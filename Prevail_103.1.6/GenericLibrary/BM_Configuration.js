_includeOnce("BM_Functions.js");
var $userLog=_readExcelFile("User_Credentials.xls","BM_Login");
var $BMConfig=_readExcelFile("User_Credentials.xls","BMConfig");
var $Run=_readExcelFile("User_Credentials.xls","Run");

//Run type
var $run=$Run[13][0];
_log($run);


//**********************************  SetAndRun  ***********************************************//

if($run == "SetAndRun")
{
BM_Login();
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));

//Click on PREVAIL Integrations link
_click(_link($userLog[2][2]));

//Checkout type
_setSelected(_select("dw-select", _in(_row("/Checkout Template Type/"))),$Run[0][0]);
var $CheckoutType=_getSelectedText(_select("dw-select", _in(_row("/Checkout Template Type/"))));

//tax service
var $taxService=_getSelectedText(_select("dw-select", _in(_row("/Tax Service/"))));

//Shipping Address Verification Service
_setSelected(_select("dw-select", _in(_row("/Shipping Address Verification Service/"))),$Run[0][1]);
var $AddrVerification=_getSelectedText(_select("dw-select", _in(_row("/Shipping Address Verification Service/"))));

//Gift Card Payment Service
_setSelected(_select("dw-select", _in(_row("/Gift Card Payment Service/"))),$Run[0][2]);
var $GiftCardPayment=_getSelectedText(_select("dw-select", _in(_row("/Gift Card Payment Service/"))));

//Credit Card Payment Service
_setSelected(_select("dw-select", _in(_row("/Credit Card Payment Service/"))),$Run[0][3]);
var $CreditCardPayment=_getSelectedText(_select("dw-select", _in(_row("/Credit Card Payment Service/"))));

//Paypal Cart
_setSelectedText(_select("dw-select", _in(_row("/Is PayPal Express Enabled/"))),$Run[0][4]);
var $paypalCart=_getSelectedText(_select("dw-select", _in(_row("/Is PayPal Express Enabled/"))));

//Paypal billing
_setSelected(_select("dw-select", _in(_row("/Is PayPal Payment Enabled/"))), $Run[0][5]);
var $paypalCheckout=_getSelectedText(_select("dw-select", _in(_row("/Is PayPal Express Enabled/"))));

//amazon payment
_setSelected(_select("dw-select", _in(_row("/Is PayWithAmazon Enabled/"))),$Run[0][6]);
var $AmazonPayment=_getSelectedText(_select("dw-select", _in(_row("/Is PayWithAmazon Enabled/"))));

//ogone
_setSelected(_select("dw-select", _in(_row("/Is Ingenico Payments Enabled/"))),$Run[0][7]);
var $OgonePayment=_getSelectedText(_select("dw-select", _in(_row("/Is Ingenico Payments Enabled/"))));

//click on apply
_click(_submit("update"));

//Order section

//click on ordering
_click(_link("Ordering"));
_click(_link("Payment Methods"));

//credit card
var $cc=$Run[8][0];
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/CREDIT_CARD Credit Card/"))));
	}
else
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/CREDIT_CARDCredit Card/"))));
	}
_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
_click(_div("/"+$cc+"/",_in(_div("x-combo-list-inner"))));
_click(_div("x-panel-header x-unselectable"));
_wait(2000);

if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))),[2]).toString();
	}
else
	{
		var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))),[2]).toString();
	}

if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		var $Visa=_getTableContents(_table("x-grid3-row-table", _in(_div("/VISA Visa/"))),[2]).toString();
	}
	else
	{
		var $Visa=_getTableContents(_table("x-grid3-row-table", _in(_div("/VISAVisa/"))),[2]).toString();
	}
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		var $MasterCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/MASTERCARDMasterCard/"))),[2]).toString();
	}
	else
	{
		var $MasterCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/MASTERCARDMasterCard/"))),[2]).toString();
	}

//gift certificate
var $gc=$Run[9][1];
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATE Gift Certificate/"))));
	}
else
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATEGift Certificate/"))));
	}
_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
_click(_div("/"+$gc+"/",_in(_div("x-combo-list-inner"))));
_click(_div("x-panel-header x-unselectable"));
_wait(2000);
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATE Gift Certificate/"))),[2]).toString();
	}
else
	{
		var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATEGift Certificate/"))),[2]).toString();
	}

//paypal
var $pp=$Run[9][2];
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/PayPal Pay Pal/"))));
	}
else
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/PayPalPay Pal/"))));
	}
_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
_click(_div("/"+$pp+"/",_in(_div("x-combo-list-inner"))));
_click(_div("x-panel-header x-unselectable"));

_wait(2000);
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		var $paypalNormal=_getTableContents(_table("x-grid3-row-table", _in(_div("/PayPal Pay Pal/"))),[2]).toString();
	}
else
	{
		var $paypalNormal=_getTableContents(_table("x-grid3-row-table", _in(_div("/PayPalPay Pal/"))),[2]).toString();
	}

//Setting Credit card type in payment processor Under payment methods
if ($CreditCardPayment==$BMConfig[0][4] && $CreditCard=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
			{
				_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
			}
		else
			{
				_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
			}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[0][5]));
		_click(_button("Apply", _in(_table("apply_payment_method_changes_button"))));
	}
else if ($CreditCardPayment==$BMConfig[1][4] && $CreditCard=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
			{
				_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
			}
		else
			{
				_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
			}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[1][5]));
		_click(_button("Apply", _in(_table("apply_payment_method_changes_button"))));
	}
else if($CreditCardPayment==$BMConfig[2][4] && $CreditCard=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari() ||_isIE())
			{
				_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
			}
		else
			{
				_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
			}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[2][5]));
		_click(_button("Apply", _in(_table("apply_payment_method_changes_button"))));
	}
//Ogone payment
if($OgonePayment=="Yes" && $CreditCard=="Yes")
{
	if (isIE11() || mobile.iPad() || _isSafari() ||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
		}
	else
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
		}
	_click(_image("s.gif", _in(_row("Payment Processor:"))));
	_click(_div($BMConfig[0][9]));
	_click(_button("Apply", _in(_table("apply_payment_method_changes_button"))));
}
if($OgonePayment=="Yes" && $Visa=="Yes")
{	
	if (isIE11() || mobile.iPad() || _isSafari() ||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/VISA Visa/"))));
		}
	else
		{
			_click(_table("x-grid3-row-table", _in(_div("/VISAVisa/"))));
		}
	_click(_image("s.gif", _in(_row("Payment Processor:"))));
	_click(_div($BMConfig[1][9]));
	_click(_button("Apply", _in(_table("apply_payment_method_changes_button"))));
}
if($OgonePayment=="Yes" && $MasterCard=="Yes")
{	
	if (isIE11() || mobile.iPad() || _isSafari() ||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/MASTERCARDMasterCard/"))));
		}
	else
		{
			_click(_table("x-grid3-row-table", _in(_div("/MASTERCARDMasterCard/"))));
		}
	_click(_image("s.gif", _in(_row("Payment Processor:"))));
	_click(_div($BMConfig[2][9]));	
	_click(_button("Apply", _in(_table("apply_payment_method_changes_button"))));
}
//Setting paypal payment type in payment processor Under payment methods
if (($paypalCart=="Yes" || $paypalCheckout=="Yes") && $paypalNormal=="Yes")
	{	
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
			{
				_click(_table("x-grid3-row-table", _in(_div("/PayPal Pay Pal/"))));
			}
		else
			{
				_click(_table("x-grid3-row-table", _in(_div("/PayPalPay Pal/"))));
			}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[0][6]));
		_click(_button("Apply", _in(_table("apply_payment_method_changes_button"))));
	}

_click(_link("Log off."));
}

//**********************************  FetchAndRun  ***********************************************//

else if($run == "FetchAndRun")
{
BM_Login();
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));

//Click on PREVAIL Integrations link
_click(_link($userLog[2][2]));

//checkout template
var $CheckoutType=_getSelectedText(_select("dw-select", _in(_row("/Checkout Template Type/"))));
//tax
var $taxService=_getSelectedText(_select("dw-select", _in(_row("/Tax Service/"))));
//paypal
var $paypalCart=_getSelectedText(_select("dw-select", _in(_row("/Is PayPal Express Enabled/"))));
//Paypal billing
var $paypalCheckout=_getSelectedText(_select("dw-select", _in(_row("/Is PayPal Express Enabled/"))));
//address verification
var $AddrVerification=_getSelectedText(_select("dw-select", _in(_row("/Shipping Address Verification Service/"))));
//gift card payment
var $GiftCardPayment=_getSelectedText(_select("dw-select", _in(_row("/Gift Card Payment Service/"))));
//credit card payment
var $CreditCardPayment=_getSelectedText(_select("dw-select", _in(_row("/Credit Card Payment Service/"))));
//amazon payment
var $AmazonPayment=_getSelectedText(_select("dw-select", _in(_row("/Is PayWithAmazon Enabled/"))));
//ogone payment
var $OgonePayment=_getSelectedText(_select("dw-select", _in(_row("/Is Ingenico Payments Enabled/"))));

//click on ordering
_click(_link("Ordering"));
_click(_link("Payment Methods"));

if (isIE11() || mobile.iPad() || _isSafari())
	{
		var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))),[2]);
		var $paypalNormal=_getTableContents(_table("x-grid3-row-table", _in(_div("/PayPal Pay Pal/"))),[2]);
		var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATE Gift Certificate/"))),[2]);
		var $Visa=_getTableContents(_table("x-grid3-row-table", _in(_div("/VISA Visa/"))),[2]).toString();
		var $MasterCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/MASTERCARDMasterCard/"))),[2]).toString();
	}
else
	{
		var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))),[2]);
		var $paypalNormal=_getTableContents(_table("x-grid3-row-table", _in(_div("/PayPalPay Pal/"))),[2]);
		var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATEGift Certificate/"))),[2]);
		var $Visa=_getTableContents(_table("x-grid3-row-table", _in(_div("/VISAVisa/"))),[2]).toString();
		var $MasterCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/MASTERCARDMasterCard/"))),[2]).toString();
	}
 
//Setting Credit card type in payment processor Under payment methods
if ($CreditCardPayment==$BMConfig[0][4] && $CreditCard=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari() || _isIE())
			{
				_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
			}
		else
			{
				_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
			}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[0][5]));
		_click(_button("Apply", _in(_table("apply_payment_method_changes_button"))));
	}
else if($CreditCardPayment==$BMConfig[1][4] && $CreditCard=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari() ||_isIE())
			{
				_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
			}
		else
			{
				_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
			}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[1][5]));
		_click(_button("Apply", _in(_table("apply_payment_method_changes_button"))));
	}
else if($CreditCardPayment==$BMConfig[2][4] && $CreditCard=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari() ||_isIE())
			{
				_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
			}
		else
			{
				_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
			}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[2][5]));
		_click(_button("Apply", _in(_table("apply_payment_method_changes_button"))));
	}
//setting Ogone payment
if($OgonePayment=="Yes" && $CreditCard=="Yes")
{
	if (isIE11() || mobile.iPad() || _isSafari() ||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
		}
	else
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
		}
	_click(_image("s.gif", _in(_row("Payment Processor:"))));
	_click(_div($BMConfig[0][9]));
	_click(_button("Apply", _in(_table("apply_payment_method_changes_button"))));
}	
if($OgonePayment=="Yes" && $Visa=="Yes")
{
	
	if (isIE11() || mobile.iPad() || _isSafari() ||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/VISA Visa/"))));
		}
	else
		{
			_click(_table("x-grid3-row-table", _in(_div("/VISAVisa/"))));
		}
	_click(_image("s.gif", _in(_row("Payment Processor:"))));
	_click(_div($BMConfig[1][9]));	
	_click(_button("Apply", _in(_table("apply_payment_method_changes_button"))));
}
if($OgonePayment=="Yes" && $MasterCard=="Yes")
{	
	if (isIE11() || mobile.iPad() || _isSafari() ||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/MASTERCARDMasterCard/"))));
		}
	else
		{
			_click(_table("x-grid3-row-table", _in(_div("/MASTERCARDMasterCard/"))));
		}
	_click(_image("s.gif", _in(_row("Payment Processor:"))));
	_click(_div($BMConfig[2][9]));	
	_click(_button("Apply", _in(_table("apply_payment_method_changes_button"))));
}
//Setting paypal payment type in payment processor Under payment methods
if (($paypalCart=="Yes" || $paypalCheckout=="Yes") && $paypalNormal=="Yes")
	{	
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
			{
				_click(_table("x-grid3-row-table", _in(_div("/PayPal Pay Pal/"))));
			}
		else
			{
				_click(_table("x-grid3-row-table", _in(_div("/PayPalPay Pal/"))));
			}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[0][6]));
		_click(_button("Apply", _in(_table("apply_payment_method_changes_button"))));
	}

_click(_link("Log off."));

}


//**********************************  DefaultRun  ***********************************************//

else
{
	var $socialSharing="NO";
	var $CheckoutType="FIVE_PAGE";
	var $paypalCart="Yes";
	var $paypalCheckout="Yes";
	var $AddrVerification="NO";
	var $GiftCardPayment="DEMANDWARE";
	var $CreditCardPayment="NO";
	var $AmazonPayment="NO";
	var $OgonePayment="NO";
	var $CreditCard="Yes";
	var $paypalNormal="Yes";
	var $giftCertificate="NO";
	var $Visa="NO";
	var $MasterCard="NO";
}