_includeOnce("BrowserSpecific.js");
_includeOnce("BM_Configuration.js");
_resource("User_Credentials.xls");

//business library Includes
_includeOnce("../BusinessLibrary/CartBusinessLibrary.js");
_includeOnce("../BusinessLibrary/CheckoutBusinessLibrary.js");
_includeOnce("../BusinessLibrary/ProfileBusinessLibrary.js");
_includeOnce("../BusinessLibrary/ShopNavBusinessLibrary.js");

var $URL=_readExcelFile("User_Credentials.xls","URL");
var $BMConfig=_readExcelFile("User_Credentials.xls","BMConfig");
var $userLog=_readExcelFile("User_Credentials.xls","BM_Login");

var $UserID=$userLog[0][0];
var $Password=$userLog[0][1];

//Function take a screen shot when a their is a script failure

function onScriptError()
{
  _log("Error log");
  _lockWindow(5000);
  _focusWindow();
  _takePageScreenShot();
  _unlockWindow();
}

function onScriptFailure()
{
  _log("Failure log");
  _lockWindow(5000);
  _focusWindow();
  _takePageScreenShot();
  _unlockWindow();
}

function SiteURLs()
{
	_navigateTo($URL[0][0]);
}

function cleanup()
 {
	//Clearing the items from cart
	ClearCartItems();
	if(isMobile())
		{
			_click(_span("Menu"));
		}
	//logout from the application
	logout();
	_click(_image("Salesforce Commerce Cloud SiteGenesis"));
}

//Checkout related functions
function round(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0)
      return Math.round(value);

    value = +value;
    exp  = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
      return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
  }



