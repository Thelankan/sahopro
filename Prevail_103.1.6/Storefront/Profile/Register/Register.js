_include("../../../GenericLibrary/GlobalFunctions.js");

//deleting the user if exists
//deleteUser();
SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);

var $t = _testcase("124584", "Verify the application behavior on click of create account link from left navigation pane of create account page");
$t.start();
try
{
	
	//click on user icon from header
	_click($HEADER_LOGIN_LINK);
	//click on register link
	_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//verify page
	_assertVisible($REGISTER_HEADING);
	//click on left nav
	_click($LEFTNAV_CREATE_ACCOUNT);
	//verify page
	_assertVisible($REGISTER_HEADING);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124585", "Verify the navigation related to 'Home' and 'My Account' links  in breadcrumb on Create account page in application as an  Anonymous user.");
$t.start();
try
{

	_assertVisible($REGISTER_PAGE_BREADCRUMB);
	if(_isIE9())
		{
		_assertEqual($Generic_Register[1][0].replace(/\s/g,''), _getText($REGISTER_PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_Register[1][0], _getText($REGISTER_PAGE_BREADCRUMB));
		}
	//Navigation related to my account
	_click($REGISTER_PAGE_BREADCRUMB_DATA);
	_assertVisible($MY_ACCOUNT_HEADING);
	_assertVisible($LOGIN_RETURNINGCUSTOMER_SECTION);

	_click($HEADER_LOGIN_LINK);
	_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//heading
	_assertVisible($REGISTER_HEADING);
	//breadcrumb
	_assertVisible($REGISTER_PAGE_BREADCRUMB);
	_assertEqual($Generic_Register[1][0], _getText($REGISTER_PAGE_BREADCRUMB));
	//Navigation related to home
	_log("Not found home link from breadcrumb");
	_assert(false);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148638", "Verify the UI  of 'Create Account' page in application as an  Anonymous user.");
$t.start();
try
{
	//click on user icon from header
	_click($HEADER_LOGIN_LINK);
	//click on register link
	_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//verify page
	_assertVisible($REGISTER_HEADING);
	//breadcrumb
	_assertVisible($REGISTER_PAGE_BREADCRUMB);
	_assertEqual($Generic_Register[1][0], _getText($REGISTER_PAGE_BREADCRUMB));
	//fields
	_assertVisible($REGISTER_FIELDSET);
	//FName
	_assertVisible($REGISTER_FIRSTNAME);
	_assertVisible(_textbox($REGISTER_FIRSTNAME_TEXTBOX));
	_assertEqual("", _getValue(_textbox($REGISTER_FIRSTNAME_TEXTBOX)));
	//Lname
	_assertVisible(_span($REGISTER_LASTNAME));
	_assertVisible($REGISTER_LASTNAME_TEXTBOX);
	_assertEqual("", _getValue($REGISTER_LASTNAME_TEXTBOX));
	//EMAIL / LOGIN INFORMATION
	_assertVisible($REGISTER_FIELDSET);
	//Email
	_assertVisible($REGISTER_EMAIL);
	_assertVisible($REGISTER_EMAIL_TEXTBOX);
	_assertEqual("", _getValue($REGISTER_EMAIL_TEXTBOX));
	//confirm email
	_assertVisible($REGISTER_CONFIRM_EMAIL);
	_assertVisible($REGISTER_CONFIRM_EMAIL_TEXTBOX);
	_assertEqual("", _getValue($REGISTER_CONFIRM_EMAIL_TEXTBOX));
	//Password
	_assertVisible($REGISTER_PASSWORD);
	_assertVisible($REGISTER_PASSWORD_TEXTBOX);
	_assertEqual("", _getValue($REGISTER_PASSWORD_TEXTBOX));
	//static text
	_assertVisible($REGISTER_PASSWORD_STATICTEXT);
	//confirm password
	_assertVisible($REGISTER_CONFIRM_PASSWORD);
	_assertVisible($REGISTER_CONFIRM_PASSWORD_TEXTBOX);
	_assertEqual("", _getValue($REGISTER_CONFIRM_PASSWORD_TEXTBOX));
	//checkbox
	_assertVisible($REGISTER_CHECKBOX);
	_assertNotTrue($REGISTER_CHECKBOX.checked);
	_assertVisible($REGISTER_CHECKBOX_STATIC_TEXT);
	//Privacy policy link
	_assertVisible($REGISTER_PRIVACY_LINK);
	//apply button
	_assertVisible($REGISTER_SUBMIT_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124592", "Verify the application behavior on click of privacy policy link in create account page of the application.");
$t.start();
try
{
	//click on user icon from header
	_click($HEADER_LOGIN_LINK);
	//click on register link
	_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//click on privacy policy from register page
	_click($REGISTER_PRIVACY_LINK);
	//verify the privacy policy overlay
	_assertVisible($REGISTER_PRIVACY_POLICY_OVERLAY);
	_assertVisible($REGISTER_PRIVACY_POLICY_OVERLAY_CLOSE);
	_assertVisible($REGISTER_PRIVACY_POLICY_OVERLAY_HEADING);
	
	//close the overlay
	_click($REGISTER_PRIVACY_POLICY_OVERLAY_CLOSE);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("148639", "Verify the navigation related to ''Privacy Policy' link  on Create account page left nav in  application as an  Anonymous user.");
$t.start();
try
{
	//click on user icon from header
	_click($HEADER_LOGIN_LINK);
	//click on register link
	_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//verify page
	_assertVisible($REGISTER_HEADING);
	//Privacy policy link
	_assertVisible($LEFTNAV_PRIVACY_POLICY);
	_click($LEFTNAV_PRIVACY_POLICY);
	//verify the navigation
	_assertVisible($REGISTER_PAGE_BREADCRUMB);
	_assertEqual($Generic_Register[0][1], _getText($REGISTER_PAGE_BREADCRUMB));
	_assertVisible($PRIVACY_POLICY_HEADING);
	_assertVisible($PRIVACY_POLICY_CONTENT);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148640", "Verify the navigation related to 'Secure Shopping' link  on Create account page left nav in  application as an  Anonymous user.");
$t.start();
try
{
	//click on user icon from header
	_click($HEADER_LOGIN_LINK);
	//click on register link
	_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//verify page
	_assertVisible($REGISTER_HEADING);
	
	//Secure Shopping Link
	_assertVisible($LEFTNAV_SECURE_SHOPPING);
	_click($LEFTNAV_SECURE_SHOPPING);
	//verify the navigation
	_assertVisible($REGISTER_PAGE_BREADCRUMB);
	if(_isIE9())
		{
		_assertEqual($Generic_Register[0][2].replace(/\s/g,''), _getText($REGISTER_PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_Register[0][2], _getText($REGISTER_PAGE_BREADCRUMB));
		}
	_assertVisible($SECURE_SHOPPING_HEADING);
	_assertVisible($SECURE_SHOPPING_CONTENT);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148641", "Verify the navigation related to 'Contact Us' link  on Create account page left nav in  application as an  Anonymous user.");
$t.start();
try
{
	//click on user icon from header
	_click($HEADER_LOGIN_LINK);
	//click on register link
	_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//verify page
	_assertVisible($REGISTER_HEADING);
	
	//Contact us
	_assertVisible($LEFTNAV_CONTACT_US);
	_click($LEFTNAV_CONTACT_US);
	//Verifying the navigation
	_assertVisible($REGISTER_PAGE_BREADCRUMB);
	if(_isIE9())
		{
		_assertEqual($Generic_Register[0][3].replace(/\s/g,''), _getText($REGISTER_PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_Register[0][3], _getText($REGISTER_PAGE_BREADCRUMB));
		}
	_assertVisible($CONTACT_HEADING);
	_assertVisible($CONTACT_CONTENT);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("148642", "Verify the functionality related to  'ACCOUNT SETTINGS' down arrow on Create account  page left Nav in application as an  Anonymous user.");
$t.start();
try
{
	//click on user icon from header
	_click($HEADER_LOGIN_LINK);
	//click on register link
	_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//verify page
	_assertVisible($REGISTER_HEADING);
	//collect the list present under the account settings
	var $list=_collectAttributes("_list","/(.*)/","sahiText",_above($LEFTNAV_SHOP_CONFIDENTLY));
	//click on account settings link
	_click($LEFTNAV_ACCOUNT_SETTING);
	//after clicking links present under account settings shouldn't visible
	for(var $i=0;$i<$list.length;$i++)
		{
		_assertNotVisible(_listItem($list[$i]));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("288270","Verify the UI of left navigation pane of create account page of the application.");
$t.start();
try
{
	//click on user icon from header
	_click($HEADER_LOGIN_LINK);
	//click on register link
	_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//verify page
	_assertVisible($REGISTER_HEADING);
	
	//verify left nav
	_assertVisible($LEFTNAV_NAVIGATION);
	//account setting data
	_assertVisible($LEFTNAV_ACCOUNT_SETTING);
	_assertVisible($LEFTNAV_CREATE_ACCOUNT);
	//shop confidently data
	_assertVisible($LEFTNAV_SHOP_CONFIDENTLY);
	_assertVisible($LEFTNAV_PRIVACY_POLICY);
	_assertVisible($LEFTNAV_SECURE_SHOPPING);
	
	//need help section
	_assertVisible($LEFTNAV_NEED_HELP);
	_assertVisible($LEFTNAV_NEED_HELP_PARAGRAPH);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124588/124589/124597/124601/125240/124599/288242", "Verify the validation related to  'First name'/'Last name'/'Email'/'Confirm Email'/'Password'/'Confirm Password' field  and  functionality related to 'Apply' button on Create account page in application as an  Anonymous user.");
$t.start();
try
{
	//click on user icon from header
	_click($HEADER_LOGIN_LINK);
	//click on register link
	_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//verify page
	_assertVisible($REGISTER_HEADING);
	
	for(var $i=0;$i<$Account_Validation.length;$i++)
		{
			if($i==10)
				{
					createAccount();
					//verify whether account got created or not
			 	   var $exp=$userData[$user][1]+" "+$userData[$user][2];
			 	   _assertEqual($exp, _extract(_getText($MY_ACCOUNT_HEADING),"/[|] (.*) [(]/",true).toString());
			 	   _click($HEADER_USERACCOUNT_LINK);
			 	   _click($MY_ACCOUNT_LOGOUT_LINK);
			 	  _click($LOGIN_CREATEACCOUNTNOW_BUTTON);
				}
			else
				{
				 _setValue($REGISTER_FIRSTNAME_TEXTBOX, $Account_Validation[$i][1]);
			 	 _setValue($REGISTER_LASTNAME_TEXTBOX, $Account_Validation[$i][2]);
			 	 _setValue($REGISTER_EMAIL_TEXTBOX, $Account_Validation[$i][3]);
			 	 _setValue($REGISTER_CONFIRM_EMAIL_TEXTBOX, $Account_Validation[$i][4]);
			 	 _setValue($REGISTER_PASSWORD_TEXTBOX, $Account_Validation[$i][5]);
			 	 _setValue($REGISTER_CONFIRM_PASSWORD_TEXTBOX, $Account_Validation[$i][6]);
			 	 if($i!=1)
			 		 {
			 		 	_click($REGISTER_SUBMIT_BUTTON); 
			 		 }	
			 	 //blank field validation
			 	 if($i==0)
			 		 {
			 		  _assertEqual($color, _style($REGISTER_FIRSTNAME_TEXTBOX, "background-color"));
			 		  _assertEqual($color, _style($REGISTER_LASTNAME_TEXTBOX, "background-color"));
			 		  _assertEqual($color, _style($REGISTER_EMAIL_TEXTBOX, "background-color"));
			 		  _assertEqual($color, _style($REGISTER_CONFIRM_EMAIL_TEXTBOX, "background-color"));
			 		  _assertEqual($color, _style($REGISTER_PASSWORD_TEXTBOX, "background-color"));
			 	  	  _assertEqual($color, _style($REGISTER_CONFIRM_PASSWORD_TEXTBOX, "background-color"));
			 		 }
			 	 //Max characters
			 	 else if($i==1)
			 		 {
			 		_assertEqual($Account_Validation[0][9],_getText($REGISTER_FIRSTNAME_TEXTBOX).length);
			 		_assertEqual($Account_Validation[1][9],_getText($REGISTER_LASTNAME_TEXTBOX).length);
			 		_assertEqual($Account_Validation[2][9],_getText($REGISTER_EMAIL_TEXTBOX).length);
			 		_assertEqual($Account_Validation[3][9],_getText($REGISTER_CONFIRM_EMAIL_TEXTBOX).length);
			 		_assertEqual($Account_Validation[4][9],_getText($REGISTER_PASSWORD_TEXTBOX).length);
			 		_assertEqual($Account_Validation[5][9],_getText($REGISTER_CONFIRM_PASSWORD_TEXTBOX).length);
			 		 }
			 	 //invalid data
			 	 else if($i==2 || $i==3 ||$i==4 || $i==5)
			 		 {
			 		 _assertVisible(_span($Account_Validation[$i][7], _in($REGISTER_EMAIL_ERROR)));
			 		 //_assertEqual($color, _style($REGISTER_EMAIL_TEXTBOX, "background-color"));	 		  
			 		 _assertVisible(_span($Account_Validation[$i][7], _in($REGISTER_EMAIL_CONFIRM_ERROR)));
			 		 //_assertEqual($color, _style($REGISTER_CONFIRM_EMAIL_TEXTBOX, "background-color"));
			 		 }	 	 
			 	 //password less than 8 characters
			 	 else if($i==6)
			 		 {
			 		_assertVisible(_div($Account_Validation[$i][7]));
			 		_assertEqual($color1,_style(_span($Account_Validation[1][10]), "color"));			 		 
			 		_assertVisible(_div($Account_Validation[$i][7], _in($REGISTER_PASSWORD_CONFIRM_ERROR)));
			 		_assertEqual($color1,_style(_span($Account_Validation[2][10]), "color"));
			 		 }
			 	 else if($i==7 || $i==8 || $i==9)
			 		 {			 		
			 		 	if($i==7)
			 		 		{
			 		 		 _assertEqual($color1, _style(_span($Account_Validation[0][10]), "color"));
			 		 		}
			 		 	if($i==8)
			 		 		{
			 		 		_assertEqual($color1, _style(_span($Account_Validation[2][10]), "color"));
			 		 		}
			 		_assertVisible(_div($Account_Validation[$i][7]));
			 		 }
				}
		}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();
//deleting the user if exists
deleteUser();