_include("../../../GenericLibrary/GlobalFunctions.js");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


//login to the application
_click($HEADER_LOGIN_LINK);
login();
DeleteCreditCard();


var $t = _testcase("125897","Verify the UI of 'My Account Payment page' when there are no payment methods added in application as a Registered user");
$t.start();
try
{
	//navigate to payment page	
	_click($LEFTNAV_PAYMENTSETTINGS_LINK);
	//verify the UI
	//Brad crumb
	_assertVisible(_link($Payment_Data[0][2], _in($PAGE_BREADCRUMB)));
	//heading
	_assertVisible(_heading1($Payment_Data[0][3]));
	_assertVisible(_div("/"+$Payment_Data[1][3]+"/"));
	_assertVisible($PAYMENT_ADDCREDITCARD);
	//left nav section
	var $Heading=HeadingCountInLeftNav();
	var $Links=LinkCountInLeftNav();
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
	       {
	              _assertVisible(_span($Payment_Data[$i][0]));                
	       }
	//checking links
	for(var $i=0;$i<$Links;$i++)
	       {
	              _assertVisible(_link($Payment_Data[$i][1]));                
	       }
	//need help section
	_assertVisible(_heading2("/"+$Payment_Data[2][3]+"/"));
	_assertVisible($LEFTNAV_CONTENTASSET);
	//contact us link
	_assertVisible(_link($Payment_Data[3][3]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124779/128723","Verify the navigation related to 'Payment Settings' menu on my Account Home  page and Verify the UI of 'My Account Payment page' when there are Multiple payment methods added in application as a Registered user");
$t.start();
try
{
	//navigate to payment page	
	_click($LEFTNAV_PAYMENTSETTINGS_LINK);
	//verify the UI
	//Brad crumb
	_assertVisible(_link($Payment_Data[0][2], _in($PAGE_BREADCRUMB)));
	//heading
	_assertVisible(_heading1($Payment_Data[0][3]));
	_assertVisible(_div("/"+$Payment_Data[1][3]+"/"));
	_assertVisible($PAYMENT_ADDCREDITCARD );
	//left nav section
	var $Heading=HeadingCountInLeftNav();
	var $Links=LinkCountInLeftNav();
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
	       {
	              _assertVisible(_span($Payment_Data[$i][0]));                
	       }
	//checking links
	for(var $i=0;$i<$Links;$i++)
	       {
	              _assertVisible(_link($Payment_Data[$i][1]));                
	       }
	//need help section
	_assertVisible(_heading2("/"+$Payment_Data[2][3]+"/"));
	_assertVisible($LEFTNAV_CONTENTASSET);
	//contact us link
	_assertVisible(_link($Payment_Data[3][3]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("124787","Verify the navigation related to 'Home'  and  'My Account' links  on   'My Account Payment page' bread crumb in application as a  Registered user");
$t.start();
try
{
	//click on home link present in the bread crumb
	//_click(_link("Home",_in($PAGE_BREADCRUMB)));
	
	//Navigate to home page
	_click($HEADER_SALESFORCE_LINK);
	//verify the navigation to home page
	_assertVisible($HOMEPAGE_SLIDES);
	_assertVisible($HOMEPAGE_SLOTS);
	//clicking on my account link
	_click($HEADER_USERACCOUNT_LINK);
	//navigating to payment settings page
	_click($PAYMENT_LINK);
	//click on my account link present in the bread crumb
	_click(_link($Payment_Data[1][2],_in($PAGE_BREADCRUMB)));
	//verify the navigation
	_assertVisible(_link($Payment_Data[1][2], _in($PAGE_BREADCRUMB)));
	_assertVisible(_heading1("/"+$Payment_Data[1][2]+"/"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124781","Verify the functionality related tothe 'ADD A CREDIT CARD' link present in the Payment section header on 'My Account Payment page' of the application as a Registered user");
$t.start();
try
{
	//navigating to Payment setting page 	
	_click($HEADER_USERACCOUNT_LINK);
	_click($LEFTNAV_PAYMENTSETTINGS_LINK);
	_click($PAYMENT_ADDCREDITCARD );
	if (_isVisible($PAYMENT_ADDCARDOVERLAY))
		{		  
		  _assert(true, "Add a Credit Card overlay is displayed");		
		}
	else
		{
		_assert(false, "Add a Credit Card overlay is not displayed");
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124782","Verify the UI of Add a credit card overlay on payment methods page in application as a Registered user");
$t.start();
try
{
	//verify the UI of Add a credit card overlay
	//Name on Card
	_assertVisible($PAYMENT_NAMETEXT);
	_assertVisible($PAYMENT_NAMETEXTBOX);
	//Tyep
	_assertVisible($PAYMENT_CARDTYPE_TEXT);
	_assertVisible($PAYMENT_CARDTYPE_DROPDOWN);
	//card number
	_assertVisible($PAYMENT_NUMBER_TEXT);
	_assertVisible($PAYMENT_NUMBER_TEXTBOX);
	//month
	_assertVisible($PAYMENT_MONTH_TEXT);
	_assertVisible($PAYMENT_MONTH_DROPDOWN);
	//Year
	_assertVisible($PAYMENT_YEAR_TEXT);
	_assertVisible($PAYMENT_YEAR_DROPDOWN);
	//checking which credit card type is enabled
	if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
		{
		//first name
		_assertVisible($PAYMENT_FIRSTNAME_TEXT);
		_assertVisible($PAYMENT_FIRSTNAME_TEXTBOX);
		//last name
		_assertVisible($PAYMENT_LASTNAME_TEXT);
		_assertVisible($PAYMENT_LASTNAME_TEXTBOX);
		//address1
		_assertVisible($PAYMENT_ADDRESS1_TEXT);
		_assertVisible($PAYMENT_ADDRESS1_TEXTBOX);
		//address2
		_assertVisible($PAYMENT_ADDRESS2_TEXT);
		_assertVisible($PAYMENT_ADDRESS2_TEXTBOX);
		//country
		_assertVisible($PAYMENT_COUNTRY_TEXT);
		_assertVisible($PAYMENT_COUNTRY_TEXTBOX);
		//city
		_assertVisible($PAYMENT_CITY_TEXT);
		_assertVisible($PAYMENT_CITY_TEXTBOX);
		//state
		_assertVisible($PAYMENT_STATE_TEXT);
		_assertVisible($PAYMENT_STATE_TEXTBOX);
		//zip code
		_assertVisible($PAYMENT_ZIPCODE_TEXT);
		_assertVisible($PAYMENT_ZIPCODE_TEXTBOX);
		//phone number
		_assertVisible($PAYMENT_PHONE_TEXT);
		_assertVisible($PAYMENT_PHONE_TEXTBOX);
		}
	_assertVisible($PAYMENT_APPLYBUTTON);
	_assertVisible($PAYMENT_CANCELBUTTON);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124783","Verify the functionality related to 'X' icon 'Add a credit card overlay payment methods page' in application as a Registered user");
$t.start();
try
{
	//click on close link
	if(_isVisible($PAYMENT_CLOSEBUTTON))
		{
		_click($PAYMENT_CLOSEBUTTON);
		}
	//overlay should not be displayed
	closedOverlayVerification();
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124784","Verify the functionality related to 'Cancel' button the Add a credit card overlay on my account payment methods page of the application as a Registered user");
$t.start();
try
{
	//open the credit card overlay
	_click($PAYMENT_ADDCREDITCARD );
	//filling details in create card fields
	if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
		{		
			CreateCreditCard(0,$Createcard, true);
		}
	else
		{
			CreateCreditCard(0,$Createcard);
		}		
	//click on cancel button
	_click($PAYMENT_CANCELBUTTON);
	//verify whether closed or not 
	closedOverlayVerification();
	//open the credit card overlay
	_click($PAYMENT_ADDCREDITCARD );
	//click on cancel button
	_click($PAYMENT_CANCELBUTTON);
	//verify whether closed or not 
	closedOverlayVerification();
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124785","Verify the functionality related to 'Apply' button the Add a credit card overlay on my account payment methods page of the application as a Registered user");
$t.start();
try
{
	//open the credit card overlay
	_click($PAYMENT_ADDCREDITCARD );
	//filling details in create card fields
	if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
		{		
			CreateCreditCard(0,$Createcard, true);
		}
	else
		{
			CreateCreditCard(0,$Createcard);
		}	
	//click on apply button
	_click($PAYMENT_APPLYBUTTON);
	//overlay got closed or not
	closedOverlayVerification();
	//verify whether credit card got saved or not and also UI
	_assertVisible($PAYMENT_PAYMENTLIST);
	var $saveInfo=_getText(_listItem("/first/")).split(" ");
	var $actualExpdate=$saveInfo[3]+""+$saveInfo[4];
	_assertEqual($Createcard[0][1],$saveInfo[0]);
	_assertEqual($Createcard[0][2],$saveInfo[1]);
	var $expDate=$Createcard[0][15]+"."+$Createcard[1][4]+"."+$Createcard[0][5];
	_assertEqual($expDate,$actualExpdate);
	_assertVisible($PAYMENT_DELETECARD_LINK);
	//left nav section
	var $Heading=HeadingCountInLeftNav();
	var $Links=LinkCountInLeftNav();
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
	       {
	              _assertVisible(_span($Payment_Data[$i][0]));                
	       }
	//checking links
	for(var $i=0;$i<$Links;$i++)
	       {
	              _assertVisible(_link($Payment_Data[$i][1]));                
	       }
	//need help section
	_assertVisible(_heading2("/"+$Payment_Data[2][3]+"/"));
	_assertVisible($LEFTNAV_CONTENTASSET);
	//contact us link
	_assertVisible(_link($Payment_Data[3][3]));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124790","Verify the functionality related tothe 'Delete card' link present in the Payment section header on 'My Account Payment page' of the application as a Registered user");
$t.start();
try
{
	//click on delete card link
	while(_isVisible($PAYMENT_DELETECARD_LINK))
		{
		_click($PAYMENT_DELETECARD_LINK);
		}
	//click on ok in confirmation overlay
	_expectConfirm("/Do you want/",true);
	//verify whether deleted or not
	_assertNotVisible($PAYMENT_PAYMENTLIST);
	_assertNotVisible($PAYMENT_DELETECARD_LINK);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("125899/148683/148684/148685/148686/148687/148688/148689/148690/148691/148692/148693/148694","Verify the validation of 'Name on Card'/'type'/number/first name/last name/address1/city/state/zip code/phone field present in the Add a credit card overlay on my account payment methods page of the application as a Registered user");
$t.start();
try
{
	if (_isIE())
	{
	_wait(2000,_isVisible($PAYMENT_ADDCREDITCARD ));
	}
	_click($PAYMENT_ADDCREDITCARD );
	//validation of credit card overlay
	for(var $i=0;$i<$Validations_Payment.length;$i++)
		{
		if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
			{		
				CreateCreditCard($i,$Validations_Payment, true);
			}
		else
			{
				CreateCreditCard($i,$Validations_Payment);
			}
		_click($PAYMENT_APPLYBUTTON);
		_log($i);
		//blank
		if($i==0)
			{
				_assertEqual($Validations_Payment[0][15],_style($PAYMENT_NAMETEXTBOX,"background-color"));
				_assertEqual($Validations_Payment[0][15],_style($PAYMENT_NUMBER_TEXTBOX,"background-color"));
				if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
				{
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_FIRSTNAME_TEXTBOX,"background-color"));
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_LASTNAME_TEXTBOX,"background-color"));
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_ADDRESS1_TEXTBOX,"background-color"));
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_COUNTRY_TEXTBOX,"background-color"));
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_CITY_TEXTBOX,"background-color"));
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_STATE_TEXTBOX,"background-color"));
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_ZIPCODE_TEXTBOX,"background-color"));
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_PHONE_TEXTBOX,"background-color"));
				}
			}
		//max length
		if($i==1)
			{
			//_assertEqual($Validations_Payment[1][15],_getText($PAYMENT_NAMETEXTBOX).length);
			//_assertEqual($Validations_Payment[1][16],_getText(MYACCOUNT_PAYMENT_NUMBER_TEXTBOX).length);
			if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
				{
					_assertEqual($Validations_Payment[1][17],_getText($PAYMENT_FIRSTNAME_TEXTBOX).length);
					_assertEqual($Validations_Payment[1][17],_getText($PAYMENT_LASTNAME_TEXTBOX).length);
					_assertEqual($Validations_Payment[1][17],_getText($PAYMENT_ADDRESS1_TEXTBOX).length);
					_assertEqual($Validations_Payment[1][17],_getText($PAYMENT_ADDRESS2_TEXTBOX).length);
					_assertEqual($Validations_Payment[1][17],_getText($PAYMENT_CITY_TEXTBOX).length);
					_assertEqual($Validations_Payment[1][18],_getText($PAYMENT_ZIPCODE_TEXTBOX).length);
					_assertEqual($Validations_Payment[1][19],_getText($PAYMENT_PHONE_TEXTBOX).length);
				}			
			}
		//invalid card number & Exp Year
		if($i==2)
			{
			//card number 
			_assertEqual($Validations_Payment[0][16],_style($PAYMENT_NUMBER_TEXT,"color"));
			_assertVisible(_div($Validations_Payment[2][15]));
			_assertEqual($Validations_Payment[0][15],_style(_div($Validations_Payment[2][15]),"background-color"));
			//Exp Year
			_assertEqual($Validations_Payment[0][16],_style($PAYMENT_MONTH_TEXT,"color"));
			_assertEqual($Validations_Payment[0][16],_style($PAYMENT_YEAR_TEXT,"color"));
			_assertVisible(_div($Validations_Payment[2][16]));
			_assertEqual($Validations_Payment[0][15],_style(_div($Validations_Payment[2][16]),"background-color"));
			}
		//invalid in zip code,phone number & number
		if($i==3)
			{
			//number
			_assertEqual($Validations_Payment[0][16],_style($PAYMENT_NUMBER_TEXT,"color"));
			_assertVisible(_div($Validations_Payment[2][15]));
			_assertEqual($Validations_Payment[0][15],_style(_div($Validations_Payment[2][15]),"background-color"));		
			}
		if($i==5)
			{
			if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
			{
				//zip code
				_assertEqual($Validations_Payment[0][15],_style($PAYMENT_ZIPCODE_TEXTBOX,"background-color"));
				_assertVisible(_span($Validations_Payment[3][15]));
				_assertEqual($Validations_Payment[0][16],_style(_span($Validations_Payment[3][15]),"color"));
			}
			}
		if($i==4)
			{	
			if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
				{
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_PHONE_TEXTBOX,"background-color"));
					_assertVisible(_span($Validations_Payment[3][16]));
					_assertEqual($Validations_Payment[0][16],_style(_span($Validations_Payment[3][16]),"color"));	
				}				
			}
		
		if($i==6)
			{
			_assertEqual($Validations_Payment[0][16],_style(_div($Validations_Payment[2][15]),"color"));
			if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
				{
					//zip code
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_ZIPCODE_TEXTBOX,"background-color"));
					_assertVisible(_span($Validations_Payment[3][15]));
					_assertEqual($Validations_Payment[0][16],_style(_span($Validations_Payment[3][15]),"color"));
				}
			
			}
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

cleanup();