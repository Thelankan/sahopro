_include("../../../GenericLibrary/GlobalFunctions.js");

var $color=$Background[0][0];

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//login
_click($HEADER_LOGIN_LINK);
login();

//Precondition
deleteAddress();

//UI without addresses
var $t = _testcase("288240", "Verify UI of addresses page of the application as a registered user not having saved addresses");
$t.start();
try
{
	//Click on home page link
	_click($HEADER_SALESFORCE_LINK);
	//click on user account link
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_MYACCOUNT_LINK);
	//Click on link addresses
	_click($MY_ACCOUNT_ADDRESSES_OPTIONS); 
	
	//heading
	_assertVisible($ADDRESSES_ADDRESS_HEADING);
	//breadcrumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Generic_Addresses[1][0], _getText($PAGE_BREADCRUMB));
	//create link
	_assertVisible($ADDRESSES_CREATENEW_ADDRESS_LINK);	
	//left nav
	_assertVisible($LEFTNAV_SECTION);
	var $Heading=HeadingCountInLeftNav();
	var $Links=LinkCountInLeftNav();
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
		{
			_assertVisible(_span($Generic_Addresses[$i][1]));			
		}
	//checking links
	for(var $i=0;$i<$Links;$i++)
		{
			_assertVisible(_link($Generic_Addresses[$i][2]));			
		}	

	//address list should not visible
	_assertNotVisible($ADDRESSES_ADDRESSES_LIST);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
 $t.end();
 
 
//Left nav navigations
 var $t = _testcase("124553", "Verify the navigation related to left nav links on Addresses page in application as a Registered user");
 $t.start();
 try
 {
	 var $Links=LinkCountInLeftNav();
 	 
		for(var $i=0;$i<$Links;$i++)
			{
				_click(_link($LeftNavLinks[$i][2]));
				_wait(3000);
				
				//verify left nav links landing page
				
				//Personal Data
			    if($i==0)
				{
			    	_assertVisible(_heading1($LeftNavLinks[3][5]));
				}
			    //Payment Settings
			    else if($i==2)
				{
			    	_assertVisible(_heading1($LeftNavLinks[4][5]));
				}
			    
			  // Wish List
				else if($i==4 || $i==5)
					{
					 _assertVisible(_heading1($LeftNavLinks[0][5]));
					}
				//Registry
				else if($i==6 || $i==7 || $i==8)
				{
					_assertVisible(_heading1($LeftNavLinks[1][5]));

				}
				//privacy polices and secure shopping
				else if($i==9 || $i==10)
				{
					if($i==9)
						{
						 _assertVisible(_heading1($LeftNavLinks[9][2]));
						}
					else if($i==10)
						{
						 _assertVisible(_heading1($LeftNavLinks[2][5]));
						}
					
					//navigate to edit account page
					_click($HEADER_USERACCOUNT_LINK);
					_click($HEADER_MYACCOUNT_LINK);
					 _click($LEFTNAV_EDITINFORMATION_LINK);

				}
				
				else
					{
					  _assertVisible(_heading1($LeftNavLinks[$i][2]));
					}
				


			}	

		

		
		
 }
 catch($e)
 {
 	_logExceptionAsFailure($e);
 }	
  $t.end();
  
 
//pre conditions to save addresses
  
	//navigate to address page
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_MYACCOUNT_LINK);
	_click($MY_ACCOUNT_ADDRESSES_OPTIONS); 
	
  for(var $i=0;$i<2;$i++)
	{
		_log($i);
		_click($ADDRESSES_CREATENEW_ADDRESS_LINK);
		_wait(2000);
		 addAddress($Valid_Address,$i);
		 _click($ADDRESSES_OVERLAY_APPLYBUTTON);
		 _wait(4000);

	}
 
//UI with addresses
var $t = _testcase("124757", "Verify the UI of Addresses page of the application as a registered user having saved addresses");
$t.start();
try
{
	//heading
	_assertVisible($ADDRESSES_ADDRESS_HEADING);
	//breadcrumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Generic_Addresses[1][0], _getText($PAGE_BREADCRUMB));
	//create link
	_assertVisible($ADDRESSES_CREATENEW_ADDRESS_LINK);	
	//left nav
	_assertVisible($LEFTNAV_SECTION);
	
	//VERIFY default address
    _assertVisible($ADDRESSES_DEFAUTADDRESS_HEADINGTEXT);
	_assertEqual($Valid_Address[0][1],_getText($ADDRESSES_MINIADDRESS_TITLE, _in($ADDRESSES_DEFAUTADDRESS)));
 	_assertEqual($Valid_Address[0][2]+" "+$Valid_Address[0][3],_getText($ADDRESSES_MINIADDRESS_NAME, _in($ADDRESSES_DEFAUTADDRESS)));
 	_assertEqual($Valid_Address[0][11], _getText($ADDRESSES_MINIADDRESS_LOCATION, _in($ADDRESSES_DEFAUTADDRESS)));
 	_assertVisible($ADDRESSES_DELETE_LINK, _in($ADDRESSES_DEFAUTADDRESS));
 	_assertVisible($ADDRESSES_EDIT_LINK, _in($ADDRESSES_DEFAUTADDRESS));
 	
 	
 	//VERIFY saved address
	_assertEqual($Valid_Address[1][1],_getText($ADDRESSES_MINIADDRESS_TITLE, _in($ADDRESSES_SAVEDADDRESS)));
 	_assertEqual($Valid_Address[1][2]+" "+$Valid_Address[1][3],_getText($ADDRESSES_MINIADDRESS_NAME, _in($ADDRESSES_SAVEDADDRESS)));
 	_assertEqual($Valid_Address[1][11], _getText($ADDRESSES_MINIADDRESS_LOCATION, _in($ADDRESSES_SAVEDADDRESS)));
 	_assertVisible($ADDRESSES_DELETE_LINK, _in($ADDRESSES_SAVEDADDRESS));
 	_assertVisible($ADDRESSES_EDIT_LINK, _in($ADDRESSES_SAVEDADDRESS));
 	_assertVisible($ADDRESSES_MAKEDEFAULT_LINK, _in($ADDRESSES_SAVEDADDRESS));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
 $t.end();
 
 
//Delete address
 var $t = _testcase("288241/124772", "Verify the functionality related to 'Delete' link present below each of the saved addresses on addresses page/application behavior on deleting default address in addresses page of the application as a registered user");
 $t.start();
 try
 {
	 deleteAddress();
 	
 }
 catch($e)
 {
 	_logExceptionAsFailure($e);
 }	
  $t.end();
  

 
 var $t = _testcase("124761", "Verify the application behavior on click of links in breadcrumb of addresses page in application as a registered user");
 $t.start();
 try
 {
 	//breadcrumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Generic_Addresses[1][0], _getText($PAGE_BREADCRUMB));

	//Bread crumb last
	_assertVisible($ADDRESSES_LINK_BREADCRUMB);
	//Navigation to My account
	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
	_click($MY_ACCOUNT_LINK_BREADCRUMB);
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Generic_Addresses[3][0], _getText($PAGE_BREADCRUMB));
	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
	//resetting to addresses Page
	_click($MY_ACCOUNT_ADDRESSES_OPTIONS);
	_assertEqual($Generic_Addresses[1][0], _getText($PAGE_BREADCRUMB));
}
catch($e)
{
	_logExceptionAsFailure($e);
}
 $t.end();
 
 var $t = _testcase("124763/124764", "Verify the application behavior/ UI of Add Address overlay  on click of create new address button in addresses page of the application as a registered user");
 $t.start();
 try
 {
	_click($HEADER_USERACCOUNT_LINK); 
	_click($HEADER_MYACCOUNT_LINK);
	_click($MY_ACCOUNT_ADDRESSES_OPTIONS);  	
	_click($ADDRESSES_CREATENEW_ADDRESS_LINK);
	_wait(5000, _isVisible($ADDRESSES_CREATEADDRESS_OVERLAY));
	 if(_isVisible($ADDRESSES_CREATEADDRESS_OVERLAY))
	 {
		 //heading
		 _assertVisible($ADDRESSES_ADD_ADDRESS_HEADING);
		// _assertVisible(_fieldset("/"+$Generic_Addresses[1][3]+"/"));  //(commented by rahul because its not visible on webpage)
		 //Close icon
		 _assertVisible($ADDRESSES_OVERLAY_CLOSEBUTTON);
		 //Checking the display of fields
		 _assertVisible($ADDRESSES_NAME_TEXT);
		 _assertVisible($ADDRESSES_ADDRESSNAME_TEXTBOX);
		 _assertVisible($ADDRESSES_FIRSTNAME_TEXT);
		 _assertVisible($ADDRESSES_FIRSTNAME_TEXTBOX);
		 _assertVisible($ADDRESSES_LASTNAME_TEXT);
		 _assertVisible($ADDRESSES_LASTNAME_TEXTBOX);
		 _assertVisible($ADDRESSES_ADDRESS1_TEXT);
		 _assertVisible($ADDRESSES_ADDRESS1_TEXTBOX);
		 _assertVisible(_div($Generic_Addresses[2][3], _in($ADDRESSES_CREATEADDRESS_OVERLAY)));
		 _assertVisible($ADDRESSES_ADDRESS2_TEXT);
		 _assertVisible($ADDRESSES_ADDRESS2_TEXTBOX);
		 _assertVisible(_div($Generic_Addresses[3][3], _in($ADDRESSES_CREATEADDRESS_OVERLAY)));
		 _assertVisible($ADDRESSES_COUNTRY_TEXT);
		 _assertVisible($ADDRESSES_COUNTRY_DROPDOWN);
		 _assertVisible($ADDRESSES_STATE_TEXT);
		 _assertVisible($ADDRESSES_STATE_DROPDOWN);
		 _assertVisible($ADDRESSES_CITY_TEXT);
		 _assertVisible($ADDRESSES_CITY_TEXTBOX);
		 _assertVisible($ADDRESSES_ZIPCODE_TEXT);
		 _assertVisible($ADDRESSES_ZIPCODE_TEXTBOX);
		 _assertVisible($ADDRESSES_PHONE_TEXT);
		 _assertVisible($ADDRESSES_PHONE_TEXTBOX);
		 _assertVisible(_div($Generic_Addresses[4][3], _in($ADDRESSES_CREATEADDRESS_OVERLAY)));
		 if (!isMobile())
			 {
			 
			 var $count=_count("_link","/tooltip/",_in($ADDRESSES_CREATEADDRESS_OVERLAY));
			 
			 for(var $i=0;$i<$count;$i++)
				 {
				 
				 	_assertVisible(_link("tooltip["+$i+"]"));
				 	_mouseOver(_link("tooltip["+$i+"]"));
				 
			        if(_getAttribute(_link("/.*/",_in(_div("form-field-tooltip["+$i+"]"))),"aria-describedby")!=null)
		              {
		                     _assert(true);
		              }
		        else
		              {
		                     _assert(false);
		              }
				 
				 }
			 
			 
	 
			 }
		 //Apply and cancel button
		 _assertVisible($ADDRESSES_OVERLAY_APPLYBUTTON);
		 _assertVisible($ADDRESSES_OVERLAY_CANCELBUTTON);
		 _click($ADDRESSES_OVERLAY_CLOSEBUTTON);
	 }
	 else
	 {
		 _assert(false,"Add address Overlay is not displaying"); 
	 }
 }
 catch($e)
 {
 	_logExceptionAsFailure($e);
 }
 $t.end();
 
 var $t = _testcase("124767", "Verify the functionality related to 'X' icon  'Add Address overlay Addresses page' in application as a  Registered user.");
 $t.start();
 try
 {
	//Click on create new address
	 _click($ADDRESSES_CREATENEW_ADDRESS_LINK);
	 //Checking close functionality
	 _assertVisible($ADDRESSES_OVERLAY_CLOSEBUTTON);
	 _click($ADDRESSES_OVERLAY_CLOSEBUTTON);
	 _assertNotVisible($ADDRESSES_CREATEADDRESS_OVERLAY);
}
catch($e)
{
	_logExceptionAsFailure($e);
}
 $t.end();
 
if (!isMobile())
	 {
 var $t = _testcase("124769", "Verify the navigation related to 'Why is this required '  tool tip  on   'Add Address overlay Addresses page' in application as a  Registered  user.");
 $t.start();
 try
 {
	 //Click on create new address
	 _click($ADDRESSES_CREATENEW_ADDRESS_LINK);
	 _assertVisible($ADDRESSES_TOOLTIP_LINK,_near($ADDRESSES_PHONE_TEXTBOX));
	 
	 //Mouse hover over text 'why is this required' 
	 _mouseOver($ADDRESSES_TOOLTIP_LINK);
	 
	 //verifications of tool tip static text
	 _assertVisible($ADDRESSES_TOOLTIP_STATICTEXT);

}
catch($e)
{
	_logExceptionAsFailure($e);
}
 $t.end();
	 }
 
 _click($HEADER_USERACCOUNT_LINK);
 _click($HEADER_MYACCOUNT_LINK);
 _click($MY_ACCOUNT_ADDRESSES_OPTIONS);
 var $t = _testcase("148666/148665", "Verify the validation related to 'Country', 'State'  drop down  on  'Add Address overlay Addresses page' in application as a  Registered user.");
 $t.start();
 try
 {
	 //Balnk field validation And valid data  of this is covered in next test script
	 //Click on create new address
	 if (isMobile())
		 {
		 _wait(2000,_isVisible($ADDRESSES_CREATENEW_ADDRESS_LINK));
		 _click($ADDRESSES_CREATENEW_ADDRESS_LINK);
		 }
	 else
		 {
	 _click($ADDRESSES_CREATENEW_ADDRESS_LINK);
		 }
	 _assertVisible($ADDRESSES_STATE_DROPDOWN);
	 _assertEqual($State_Country[0][11], _getSelectedText($ADDRESSES_STATE_DROPDOWN));
	 _assertVisible($ADDRESSES_COUNTRY_DROPDOWN);
	 _assertEqual($State_Country[0][12], _getSelectedText($ADDRESSES_COUNTRY_DROPDOWN));
	 for(var $i=0;$i<$State_Country.length-3;$i++)
		{
		 	_log($i);
			_setValue($ADDRESSES_ADDRESSNAME_TEXTBOX, $State_Country[$i][1]);  
			_setValue($ADDRESSES_FIRSTNAME_TEXTBOX, $State_Country[$i][2]);  
			_setValue($ADDRESSES_LASTNAME_TEXTBOX, $State_Country[$i][3]);  
			_setValue($ADDRESSES_ADDRESS1_TEXTBOX, $State_Country[$i][4]);  
			_setValue($ADDRESSES_ADDRESS2_TEXTBOX, $State_Country[$i][5]);  
			_setSelected($ADDRESSES_COUNTRY_DROPDOWN,$State_Country[$i][6]);  
			//Blank field validation
			if($i==0)
				{
					_setSelected($ADDRESSES_STATE_DROPDOWN, $State_Country[$i][7]);  
					_setValue($ADDRESSES_CITY_TEXTBOX, $State_Country[$i][8]);  
					_setValue($ADDRESSES_ZIPCODE_TEXTBOX, $State_Country[$i][9]);  
					_setValue($ADDRESSES_PHONE_TEXTBOX,$State_Country[$i][10]);  	
				 	_click($ADDRESSES_OVERLAY_APPLYBUTTON);
					//Checking background color
				 	_assertEqual($color, _style($ADDRESSES_STATE_DROPDOWN, "background-color"));    
				}		
			//when different Countries are selected
			else 
				{
				  _assertEqual($State_Country[$i][11], _getText($ADDRESSES_STATE_DROPDOWN).toString());
				}
		}
	 _click($ADDRESSES_OVERLAY_CLOSEBUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}
 $t.end(); 
 
 
 var $t = _testcase("128532/148661/148662/148663/148664/148667/148669/124770/124766/148681/288239", "Verify the validation related to 'Address Name','First Name','Last  Name','Address 1','Address 2','City','Phone' fields and functionality related to 'Apply' button & 'cancel' button on 'Add Address overlay Addresses page'in application as a  Registered  user/format of saved addresses displayed in address page of the application");
 $t.start();
 try
 {
	for(var $i=0;$i<$Addr_Validation.length;$i++)
		{
			_log($i);
			_click($ADDRESSES_CREATENEW_ADDRESS_LINK);
			_wait(5000, _isVisible($ADDRESSES_CREATEADDRESS_OVERLAY));
			 if(_isVisible($ADDRESSES_CREATEADDRESS_OVERLAY))
				 {
					 addAddress($Addr_Validation,$i);
					
					 //blank field validation
					 if($i==0)
						 {
						 	_click($ADDRESSES_OVERLAY_APPLYBUTTON);
							_assertEqual($color, _style($ADDRESSES_ADDRESSNAME_TEXTBOX, "background-color"));
							_assertEqual($color, _style($ADDRESSES_FIRSTNAME_TEXTBOX, "background-color"));
							_assertEqual($color, _style($ADDRESSES_LASTNAME_TEXTBOX, "background-color"));
							_assertEqual($color, _style($ADDRESSES_ADDRESS1_TEXTBOX, "background-color"));
							//_assertEqual($color, _style($ADDRESSES_COUNTRY_DROPDOWN, "background-color"));
							_assertEqual($color, _style($ADDRESSES_STATE_DROPDOWN, "background-color"));
							_assertEqual($color, _style($ADDRESSES_CITY_TEXTBOX, "background-color"));
							_assertEqual($color, _style($ADDRESSES_ZIPCODE_TEXTBOX, "background-color"));
					        _assertEqual($color, _style($ADDRESSES_PHONE_TEXTBOX, "background-color")); 
					        _click($ADDRESSES_OVERLAY_CLOSEBUTTON);
						 }
					 //cancel functionality without entering values
					 else if($i==1)
						 {
						 	
						 	_click($ADDRESSES_OVERLAY_CANCELBUTTON);
						 	_assertNotVisible($ADDRESSES_CREATEADDRESS_OVERLAY);
						 	_assertNotVisible($ADDRESSES_ADDRESSES_LIST);
						 }
					 //Special characters, Phone number inavlid format and alphanumeric value
					 else if($i==2 || $i==3 ||$i==5)
						 {
						
						 	_click($ADDRESSES_OVERLAY_APPLYBUTTON);
						 	_assertVisible(_span($Addr_Validation[$i][14]));
						 	 if(_isVisible($ADDRESSES_ERROR_MESSAGE_PHONE))
								{
									_assertEqual($Addr_Validation[$i][12],_getText($ADDRESSES_ERROR_MESSAGE_PHONE));
								}
						 	 else if(_isVisible(_span($Addr_Validation[$i][12])))
						 		 {
						 		 _assertVisible(_span($Addr_Validation[$i][12]));
						 		 }
			                else
			     
								{
									_assert(false, "Error message for Phone field is not displayed as expected");
								}
						 	
						 	
						 	 _click($ADDRESSES_OVERLAY_CLOSEBUTTON);
						 }
					 //Max characters
					 else if($i==4)
						 {
						 		
						 		 _assertEqual($Addr_Validation[0][13],_getText($ADDRESSES_ADDRESSNAME_TEXTBOX).length);  
								 _assertEqual($Addr_Validation[1][13],_getText($ADDRESSES_FIRSTNAME_TEXTBOX).length);
								 _assertEqual($Addr_Validation[2][13],_getText($ADDRESSES_LASTNAME_TEXTBOX).length);
								 _assertEqual($Addr_Validation[3][13],_getText($ADDRESSES_ADDRESS1_TEXTBOX).length);
								 _assertEqual($Addr_Validation[4][13],_getText($ADDRESSES_ADDRESS2_TEXTBOX).length);
								 _assertEqual($Addr_Validation[8][13],_getText($ADDRESSES_CITY_TEXTBOX).length);  
								 _assertEqual($Addr_Validation[6][13],_getText($ADDRESSES_PHONE_TEXTBOX).length); 
								 _click($ADDRESSES_OVERLAY_CLOSEBUTTON);
						 }
					//cancel functionality after entering values
					 else if($i==6)
						 {
						 	_click($ADDRESSES_OVERLAY_CANCELBUTTON);
						 	_assertNotVisible($ADDRESSES_CREATEADDRESS_OVERLAY);
						 	_assertNotVisible($ADDRESSES_ADDRESSES_LIST);						 		
						 }
					 
					 else if($i==8)
						 {
						 
							 _click($ADDRESSES_OVERLAY_APPLYBUTTON);
								 	 
						 	 if(_isVisible($ADDRESSES_ERROR_MESSAGE_EXISTING))
								{
									_assertEqual($Addr_Validation[$i][11],_getText($ADDRESSES_ERROR_MESSAGE_EXISTING));
								}
			                else
								{
								    _assert(false, "Error message for Address name field is not displayed as expected");
								}
							 
						 	 _click($ADDRESSES_OVERLAY_CLOSEBUTTON);
						 }
					 else
						 {
						   //verify the saved address
    					 	_click($ADDRESSES_OVERLAY_APPLYBUTTON);
						 	_assertEqual($Addr_Validation[$i][1],_getText($ADDRESSES_MINIADDRESS_TITLE));
						 	_assertEqual($Addr_Validation[$i][2]+" "+$Addr_Validation[$i][3],_getText($ADDRESSES_MINIADDRESS_NAME));
						 	_assertEqual($Addr_Validation[$i][11], _getText($ADDRESS_MINIADDRESS_LOCATION));						 
						 }					 
				 }
			 else
				 {
					 _assert(false,"Add address Overlay is not displaying"); 
				 }			
		} 
}
catch($e)
{
	_logExceptionAsFailure($e);
}
 $t.end();
 
 var $t=_testcase("124775/148670","Verify the functionality/UI of Edit Address overlay  related to 'Edit' link present below saved addresses on addresses page of the application as a Registered user");
 $t.start();
 try
 {
	 _assertVisible($ADDRESSES_EDIT_LINK);
	 if(_isVisible($ADDRESSES_EDIT_LINK))
		 {
		 	_click($ADDRESSES_EDIT_LINK);
		 	_wait(5000, _isVisible($ADDRESSES_CREATEADDRESS_OVERLAY));
		 	 if(_isVisible($ADDRESSES_CREATEADDRESS_OVERLAY))
			 {
		 		 //heading
				 _assertVisible(_heading1($Generic_Addresses[0][4]));
				// _assertVisible(_fieldset("/"+$Generic_Addresses[1][4]+"/"));  //(commented by rahul because its not present on webpage)
				 //Close icon
				 _assertVisible($ADDRESSES_OVERLAY_CLOSEBUTTON);
				 //Checking the display of fields
				 _assertVisible($ADDRESSES_NAME_TEXT);
				 _assertVisible($ADDRESSES_ADDRESSNAME_TEXTBOX);
				 _assertEqual($Addr_Validation[7][1], _getValue($ADDRESSES_ADDRESSNAME_TEXTBOX));
				 _assertVisible($ADDRESSES_FIRSTNAME_TEXT);
				 _assertVisible($ADDRESSES_FIRSTNAME_TEXTBOX);
				 _assertEqual($Addr_Validation[7][2], _getValue($ADDRESSES_FIRSTNAME_TEXTBOX));
				 _assertVisible($ADDRESSES_LASTNAME_TEXT);
				 _assertVisible($ADDRESSES_LASTNAME_TEXTBOX);
				 _assertEqual($Addr_Validation[7][3], _getValue($ADDRESSES_LASTNAME_TEXTBOX));
				 _assertVisible($ADDRESSES_ADDRESS1_TEXT);
				 _assertVisible($ADDRESSES_ADDRESS1_TEXTBOX);
				 _assertEqual($Addr_Validation[7][4], _getValue($ADDRESSES_ADDRESS1_TEXTBOX));
				 _assertVisible(_div($Generic_Addresses[2][3], _in($ADDRESSES_CREATEADDRESS_OVERLAY)));
				 _assertVisible($ADDRESSES_ADDRESS2_TEXT);
				 _assertVisible($ADDRESSES_ADDRESS2_TEXTBOX);
				 _assertEqual($Addr_Validation[7][5], _getValue($ADDRESSES_ADDRESS2_TEXTBOX));
				 _assertVisible(_div($Generic_Addresses[3][3], _in($ADDRESSES_CREATEADDRESS_OVERLAY)));
				 _assertVisible($ADDRESSES_COUNTRY_TEXT);
				 _assertVisible($ADDRESSES_COUNTRY_DROPDOWN);
				 _assertEqual($Addr_Validation[7][6], _getSelectedText($ADDRESSES_COUNTRY_DROPDOWN));
				 _assertVisible($ADDRESSES_STATE_TEXT);
				 _assertVisible($ADDRESSES_STATE_DROPDOWN);
				 _assertEqual($Addr_Validation[7][7], _getSelectedText($ADDRESSES_STATE_DROPDOWN));
				 _assertVisible($ADDRESSES_CITY_TEXT);
				 _assertVisible($ADDRESSES_CITY_TEXTBOX);
				 _assertEqual($Addr_Validation[7][8], _getValue($ADDRESSES_CITY_TEXTBOX));
				 _assertVisible($ADDRESSES_ZIPCODE_TEXT);
				 _assertVisible($ADDRESSES_ZIPCODE_TEXTBOX);
				 _assertEqual($Addr_Validation[7][9], _getValue($ADDRESSES_ZIPCODE_TEXTBOX));
				 _assertVisible($ADDRESSES_PHONE_TEXT);
				 _assertVisible($ADDRESSES_PHONE_TEXTBOX);
				 _assertEqual($Addr_Validation[7][10], _getValue($ADDRESSES_PHONE_TEXTBOX));
				 _assertVisible(_div($Generic_Addresses[4][4], _in($ADDRESSES_CREATEADDRESS_OVERLAY)));
				 if(!isMobile() || mobile.iPad())
					 {
//					 //tool tip
//					 _assertVisible($TOOLTIP,_near($ADDRESSES_PHONE_TEXTBOX));
//					 var $TooltipMsg=_extract(_getText($TOOLTIP,_near($ADDRESSES_PHONE_TEXTBOX)),"/(.*)[?]/",true).toString()+"?";
//					 _assertEqual($Generic_Addresses[5][4],$TooltipMsg);

					 
					 var $count=_count("_link","/tooltip/",_in($ADDRESSES_CREATEADDRESS_OVERLAY));
					 
					 for(var $i=0;$i<$count;$i++)
						 {
						 
						 	_assertVisible(_link("tooltip["+$i+"]"));
						 	_mouseOver(_link("tooltip["+$i+"]"));
						 
					        if(_getAttribute(_link("/.*/",_in(_div("form-field-tooltip["+$i+"]"))),"aria-describedby")!=null)
				              {
				                   _assert(true);
				              }
				        else
				              {
				                  _assert(false);
				              }

					     }
					 }
				 //Apply and cancel button
				 _assertVisible($ADDRESSES_OVERLAY_APPLYBUTTON);
				 _assertVisible($ADDRESSES_OVERLAY_CANCELBUTTON);
				 _assertVisible($ADDRESSES_DELETE_LINK);
				 _click($ADDRESSES_OVERLAY_CLOSEBUTTON);		 		 
			 }
		 	else
			 {
				 _assert(false,"Edit address Overlay is not displaying"); 
			 }		 	
		 }
	 else
		 {
		 	_assert(false, "Edit link does not exits");
		 }
 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end();
 

 var $t=_testcase("148672","Verify the functionality related to 'X' icon  'Edit Address overlay Addresses page' in application as a  Registered user.");
 $t.start();
 try
 {
	 	_click($ADDRESSES_EDIT_LINK);
	 	_wait(5000, _isVisible($ADDRESSES_CREATEADDRESS_OVERLAY));
	 	 if(_isVisible($ADDRESSES_CREATEADDRESS_OVERLAY))
		 {
	 		 //Checking close functionality
	 		 _assertVisible($ADDRESSES_OVERLAY_CLOSEBUTTON);
	 		 _click($ADDRESSES_OVERLAY_CLOSEBUTTON);
	 		 _assertNotVisible($ADDRESSES_CREATEADDRESS_OVERLAY);
	 		_assertVisible($ADDRESSES_ADDRESSES_LIST);
		 }
	 	else
		 {
			 _assert(false,"Edit address Overlay is not displaying"); 
		 }	

 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end();
 
 
if (!isMobile())
	{
 var $t=_testcase("148671","Verify the application behavior on mouse hover over text 'why is this required' in edit address overlay as a registered user having saved addresses");
 $t.start();
 try
 {
	 	_click($ADDRESSES_EDIT_LINK);
	 	_wait(5000, _isVisible($ADDRESSES_CREATEADDRESS_OVERLAY));
	 	 if(_isVisible($ADDRESSES_CREATEADDRESS_OVERLAY))
		 {
	 		if (!isMobile())
			 {
			 
			 var $count=_count("_link","/tooltip/",_in($ADDRESSES_CREATEADDRESS_OVERLAY));
			 
			 for(var $i=0;$i<$count;$i++)
				 {
				 
				 	_assertVisible(_link("tooltip["+$i+"]"));
				 	_mouseOver(_link("tooltip["+$i+"]"));
				 
			        if(_getAttribute(_link("/.*/",_in(_div("form-field-tooltip["+$i+"]"))),"aria-describedby")!=null)
		              {
		                     _assert(true);
		              }
		        else
		              {
		                     _assert(false);
		              }
				 
				 }
			 }
		 }
	 	else
		 {
			 _assert(false,"Edit address Overlay is not displaying"); 
		 }	

 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end();
	}
 
 _click($HEADER_USERACCOUNT_LINK);
 _click($HEADER_MYACCOUNT_LINK);
 _click($MY_ACCOUNT_ADDRESSES_OPTIONS); 
 var $t=_testcase("124776","Verify the functionality related to 'Delete' button in edit address overlay of addresses page as a registered user having saved addresses");
 $t.start();
 try
 {
	 _click($ADDRESSES_EDIT_LINK);
	 _wait(5000, _isVisible($ADDRESSES_CREATEADDRESS_OVERLAY));
 	 if(_isVisible($ADDRESSES_CREATEADDRESS_OVERLAY))
	 {
 		 //Checking close functionality
 		 _assertVisible($ADDRESSES_DELETE_LINK);
 		 _click($ADDRESSES_DELETE_LINK);
 	      //_wait(1000);
 	 	 _expectConfirm("/Do you want/",true);
 		 _assertNotVisible($ADDRESSES_CREATEADDRESS_OVERLAY);
 		 _assertNotVisible($ADDRESSES_ADDRESSES_LIST);
	 }
 	else
	 {
		 _assert(false,"Edit address Overlay is not displaying"); 
	 }

 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end();

 ///setup
 _click($HEADER_USERACCOUNT_LINK);
 _click($HEADER_MYACCOUNT_LINK);
 _click($MY_ACCOUNT_ADDRESSES_OPTIONS); 
 for(var $i=0;$i<$Valid_Address.length;$i++)  
	{
	
		_click($ADDRESSES_CREATENEW_ADDRESS_LINK);
		_wait(2000);
		 addAddress($Valid_Address,$i);	
		 _click($ADDRESSES_OVERLAY_APPLYBUTTON);
		 _wait(3000);
	}
 
  var $t=_testcase("124774","Verify the functionality related to 'Make Default' link present below each of the saved addresses addresses page of the application as a Registered user having saved addresses");
 $t.start();
 try
 {
	//click on make default link
	 _click($ADDRESSES_MAKEDEFAULT_LINK);
	 //comparing address after making it as default
	 var $expaddress=$Valid_Address[1][4]+" "+$Valid_Address[1][5]+" "+$Valid_Address[1][8]+", "+$Valid_Address[1][7]+" "+$Valid_Address[1][9]+" "+$Valid_Address[1][6]+" Phone: "+$Valid_Address[1][10];
	 _assertEqual($expaddress,_getText($ADDRESS_MINIADDRESS_LOCATION)).toString();
	 
	//reset back
	//click on make default link
	 _click($ADDRESSES_MAKEDEFAULT_LINK);
	 //comparing address after making it as default
	 var $expaddress=$Valid_Address[0][4]+" "+$Valid_Address[0][5]+" "+$Valid_Address[0][8]+", "+$Valid_Address[0][7]+" "+$Valid_Address[0][9]+" "+$Valid_Address[0][6]+" Phone: "+$Valid_Address[0][10];
	 _assertEqual($expaddress,_getText($ADDRESS_MINIADDRESS_LOCATION)).toString();
	 
 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end();
 

 var $t = _testcase("148678/148679", "Verify the validation related to 'Country', 'State'  drop down  on  'Edit Address overlay Addresses page' in application as a  Registered user.");
 $t.start();
 try
 {
	 //Balnk field validation And valid data  of this is covered in next test script
	 //Click on edit
	 _click($HEADER_USERACCOUNT_LINK);  
	 _click($HEADER_MYACCOUNT_LINK);
	_click($MY_ACCOUNT_ADDRESSES_OPTIONS);  	
	 _click($ADDRESSES_EDIT_LINK);
	 for(var $i=0;$i<$State_Country.length-3;$i++)
		{
			_setValue($ADDRESSES_ADDRESSNAME_TEXTBOX, $State_Country[$i][1]);  
			_setValue($ADDRESSES_FIRSTNAME_TEXTBOX, $State_Country[$i][2]);  
			_setValue($ADDRESSES_LASTNAME_TEXTBOX, $State_Country[$i][3]);  
			_setValue($ADDRESSES_ADDRESS1_TEXTBOX, $State_Country[$i][4]);  
			_setValue($ADDRESSES_ADDRESS2_TEXTBOX, $State_Country[$i][5]);  
			_setSelected($ADDRESSES_COUNTRY_DROPDOWN,$State_Country[$i][6]);  		
			//Blank field validation
			if($i==0)
				{
					_setSelected($ADDRESSES_STATE_DROPDOWN,"");  	
					_setValue($ADDRESSES_CITY_TEXTBOX, $State_Country[$i][8]);  	
					_setValue($ADDRESSES_ZIPCODE_TEXTBOX, $State_Country[$i][9]);  	
					_setValue($ADDRESSES_PHONE_TEXTBOX,$State_Country[$i][10]);  		
				 	_click($ADDRESSES_OVERLAY_APPLYBUTTON);
					//Checking background color
				 	_assertEqual($color, _style($ADDRESSES_STATE_DROPDOWN, "background-color"));    
				}		
			//when different Countries are selected
			else 
				{
				  _assertEqual($State_Country[$i][11], _getText($ADDRESSES_STATE_DROPDOWN).toString());
				}
		}
	 _click($ADDRESSES_OVERLAY_CLOSEBUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}
 $t.end(); 
 
 
 var $t = _testcase("148673/148674/148675/148676/148677/148680/148682/124777/124778/148668", "Verify the validation related to 'Address Name','First Name','Last  Name','Address 1','Address 2','City','Phone','Zip Code' fields and functionality related to 'Apply' button & 'cancel' button on 'Edit Address overlay Addresses page'in application as a  Registered  user.");
 $t.start();
 try
 {
	for(var $i=0;$i<$Edit_Addr_Validation.length;$i++)
		{
			_click($ADDRESSES_EDIT_LINK);
			_wait(5000, _isVisible($ADDRESSES_CREATEADDRESS_OVERLAY));
			 if(_isVisible($ADDRESSES_CREATEADDRESS_OVERLAY))
				 {
					 addAddress($Edit_Addr_Validation,$i);	
					
					 //blank field validation
					 if($i==0)
						 {
						 	_click($ADDRESSES_OVERLAY_APPLYBUTTON);
							_assertEqual($color, _style($ADDRESSES_ADDRESSNAME_TEXTBOX, "background-color"));
							_assertEqual($color, _style($ADDRESSES_FIRSTNAME_TEXTBOX, "background-color"));
							_assertEqual($color, _style($ADDRESSES_LASTNAME_TEXTBOX, "background-color"));
							_assertEqual($color, _style($ADDRESSES_ADDRESS1_TEXTBOX, "background-color"));
							//only one country which is pre selected so this error message will not display
							//_assertEqual($color, _style($ADDRESSES_COUNTRY_DROPDOWN, "background-color"));
							_assertEqual($color, _style($ADDRESSES_STATE_DROPDOWN, "background-color"));
							_assertEqual($color, _style($ADDRESSES_CITY_TEXTBOX, "background-color"));
							_assertEqual($color, _style($ADDRESSES_ZIPCODE_TEXTBOX, "background-color"));
					        _assertEqual($color, _style($ADDRESSES_PHONE_TEXTBOX, "background-color")); 
					        _click($ADDRESSES_OVERLAY_CLOSEBUTTON);
						 }
					 //cancel functionality without entering values
					 else if($i==1)
						 {
						 	_click($ADDRESSES_OVERLAY_CANCELBUTTON);
						 	_assertNotVisible($ADDRESSES_CREATEADDRESS_OVERLAY);
						 	_assertVisible($ADDRESSES_ADDRESSES_LIST);
						 	_assertEqual($Valid_Address[0][1],_getText($ADDRESSES_MINIADDRESS_TITLE));
						 	_assertEqual($Valid_Address[0][2]+" "+$Valid_Address[0][3], _getText($ADDRESSES_MINIADDRESS_NAME));
						 	_assertEqual($Valid_Address[0][11], _getText($ADDRESS_MINIADDRESS_LOCATION));
						 }
					 //Special characters, Phone number inavlid format and alphanumeric value
					 else if($i==2 || $i==3 ||$i==5)
						 {
						 	_click($ADDRESSES_OVERLAY_APPLYBUTTON);		 	
						 	 if(_isVisible($ADDRESSES_ERROR_MESSAGE_PHONE))
								{
									_assertEqual($Addr_Validation[$i][12],_getText($ADDRESSES_ERROR_MESSAGE_PHONE));
								}
						 	 else if(_isVisible(_span($Addr_Validation[$i][12])))
						 		 {
						 		 _assertVisible(_span($Addr_Validation[$i][12]));
						 		 }
			                else
			     
								{
									_assert(false, "Error message for Phone field is not displayed as expected");
								}
						 	
						 	 _click($ADDRESSES_OVERLAY_CLOSEBUTTON);
						 }
					 
					 //Max characters
					 else if($i==4)
						 {
						 		 _assertEqual($Edit_Addr_Validation[0][13],_getText($ADDRESSES_ADDRESSNAME_TEXTBOX).length);
								 _assertEqual($Edit_Addr_Validation[1][13],_getText($ADDRESSES_FIRSTNAME_TEXTBOX).length);
								 _assertEqual($Edit_Addr_Validation[2][13],_getText($ADDRESSES_LASTNAME_TEXTBOX).length);
								 _assertEqual($Edit_Addr_Validation[3][13],_getText($ADDRESSES_ADDRESS1_TEXTBOX).length);
								 _assertEqual($Edit_Addr_Validation[4][13],_getText($ADDRESSES_ADDRESS2_TEXTBOX).length);
								 _assertEqual($Edit_Addr_Validation[5][13],_getText($ADDRESSES_CITY_TEXTBOX).length);
								 _assertEqual($Edit_Addr_Validation[6][13],_getText($ADDRESSES_PHONE_TEXTBOX).length); 
								 _click($ADDRESSES_OVERLAY_CLOSEBUTTON);
						 }
					//cancel functionality after entering values
					 else if($i==6)
						 {
						 	_click($ADDRESSES_OVERLAY_CANCELBUTTON);
						 	_assertNotVisible($ADDRESSES_CREATEADDRESS_OVERLAY);
						 	_assertEqual($Valid_Address[0][1],_getText($ADDRESSES_MINIADDRESS_TITLE));
							_assertEqual($Valid_Address[0][2]+" "+$Valid_Address[0][3], _getText($ADDRESSES_MINIADDRESS_NAME));
						 	_assertEqual($Valid_Address[0][11], _getText($ADDRESS_MINIADDRESS_LOCATION));
						 }
					 else if($i==8)
						 {
							 _click($ADDRESSES_OVERLAY_APPLYBUTTON);
						 	 if(_isVisible($ADDRESSES_ERROR_MESSAGE_EXISTING))
								{
									_assertEqual($Edit_Addr_Validation[$i][11],_getText($ADDRESSES_ERROR_MESSAGE_EXISTING));
								}
			                else
								{
									_assert(false, "Error message for Address name field is not displayed as expected");
								}
						 	 _click($ADDRESSES_OVERLAY_CLOSEBUTTON);
						 }
					 else
						 {
						 	_click($ADDRESSES_OVERLAY_APPLYBUTTON);
						 	_assertEqual($Edit_Addr_Validation[$i][1],_getText($ADDRESSES_MINIADDRESS_TITLE));
							_assertEqual($Edit_Addr_Validation[$i][2]+" "+$Edit_Addr_Validation[$i][3], _getText($ADDRESSES_MINIADDRESS_NAME));  
						 	_assertEqual($Edit_Addr_Validation[$i][11], _getText($ADDRESS_MINIADDRESS_LOCATION));						 
						 }					 
				 }
			 else
				 {
					 _assert(false,"Add address Overlay is not displaying"); 
				 }			
		} 
}
catch($e)
{
	_logExceptionAsFailure($e);
}
 $t.end(); 
 
 cleanup();
 
 
 