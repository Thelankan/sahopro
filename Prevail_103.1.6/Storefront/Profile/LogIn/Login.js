_include("../../../GenericLibrary/GlobalFunctions.js");

var $UserEmail=$uId;
SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);

var $t = _testcase("124604", "Verify the navigation to My Account login page in application as an  Anonymous user");
$t.start();
try
{
	//click on login link
	_click($HEADER_LOGIN_LINK);
	//verify the navigation
	_assertVisible($LOGIN_HEADING);
	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124605/124606/148627", "Verify UI of 'Returning customers section'/'New customers section'/'My account login' on My account login page");
$t.start();
try
{
	//Verifying the UI of returning customer section
	//MyAccount login as heading
	_assertVisible($LOGIN_HEADING);
	//Returning customer heading
	_assertVisible($LOGIN_RETURNINGNCUSTOMER_HEADING);
	//Paragraph with static text
	_assertVisible($LOGIN_RETURNUS_TEXT);
	//email Address
	_assertVisible($LOGIN_EMAIL_ADDRESS_TEXT);
	_assertVisible($LOGIN_EMAIL_ADDRESS_TEXTBOX);
	_assertEqual("", _getValue($LOGIN_EMAIL_ADDRESS_TEXTBOX));
	//password
	_assertVisible($LOGIN_PASSWORD_TEXT);
	_assertVisible($LOGIN_PASSWORD_TEXTBOX);
	_assertEqual("", _getValue($LOGIN_PASSWORD_TEXTBOX));
	//remember me
	_assertVisible($LOGIN_REMEMBERMETEXT);
	_assertVisible($LOGIN_REMEMBEMETEXTBOX);
	_assertNotTrue($LOGIN_REMEMBEMETEXTBOX.checked);
	//login button
	_assertVisible($LOGIN_BUTTON);
	//forgot password
	_assertVisible($LOGIN_PASSWORD_RESET);
	_assertEqual($Login_Data[4][0], _getText($LOGIN_PASSWORD_RESET));
	//social links
	_assertVisible($LOGIN_SOCIALLINKS);
	_assertVisible($LOGIN_ENTIRE_LOGIN_SECTION);
	
	

	//verify the UI of New Customer section
	_assertVisible(_heading2($Login_Data[0][1]));
	//paragraph
	_assertVisible($LOGIN_NEW_CUSTOMER_PARAGRAPHTEXT);
	//create an account button
	_assertVisible($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//content assert
	_assertVisible($CONTENT_ASSET);
	_assertVisible($LOGIN_STATICTEXT1);
	_assertVisible($LOGIN_STATICTEXT2);
	_assertVisible($LOGIN_STATICTEXT3);
	_assertVisible($LOGIN_STATICTEXT4);
	_assertVisible($LOGIN_READMOREABOUTSECURITY_LINK, _in(_paragraph($Login_Data[3][8])));

	//read more about security link
	_assertVisible($LOGIN_SECURITY_LINK);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
 


var $t = _testcase("288708", "Verify the UI of check order section in my account login page of the application");
$t.start();
try
{
	//verify the UI of check an order section
	_assertVisible($LOGIN_ORDERSECTION_HEADING);
	_assertVisible($LOGIN_CHECKORDERTEXT);
	_assertVisible($LOGIN_ORDER_STATICTEXT);
	//order number
	_assertVisible($LOGIN_ORDER_NUMBERLABEL);
	_assertVisible($LOGIN_ORDERNUMBER_TEXTBOX);
	_assertEqual("", _getValue($LOGIN_ORDERNUMBER_TEXTBOX));
	//order email
	_assertVisible($LOGIN_ORDEREMAIL_LABEL);
	_assertVisible($LOGIN_ORDEREMAIL_TEXTBOX);
	_assertEqual("", _getValue($LOGIN_ORDEREMAIL_TEXTBOX));
	//postal code
	_assertVisible($LOGIN_ORDER_ZIPCODE);
	_assertVisible($LOGIN_ORDER_ZIPCODE_TEXTBOX);
	_assertEqual("", _getValue($LOGIN_ORDER_ZIPCODE_TEXTBOX));
	//submit button
	_assertVisible($LOGIN_ORDER_SUBMITBUTTON);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124608", "Verify the application behavior on click of contents displayde in breadcrumb of my account login page of the application");
$t.start();
try
{

	//Navigate to home page
	_click($HEADER_SALESFORCE_LINK);
	//verify the navigation to home page
	_assertVisible($HOMEPAGE_SLIDES);
	_assertVisible($HOMEPAGE_SLOTS);
	//click on link login
	_click($HEADER_LOGIN_LINK);
	//click on my account bread crumb link 
	_click($MY_ACCOUNT_LINK_BREADCRUMB);
	//verfiy the navigation
	_assertVisible(_heading1($LOGIN_HEADING));
	_assertVisible($LOGIN_RETURNINGCUSTOMER_SECTION);
	_assertVisible($LOGIN_NEW_CUSTOMER_SECTION);
	_assertVisible($LOGIN_ORDERSECTION);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();	

var $t = _testcase("148628", "Verify the navigation related to links in left navigation pane on my account login page of the application");
$t.start();
try
{
	
	var $leftnavlinks=_collectAttributes("_link","/(.*)/","sahiText", _in(_div("secondary")));
	for (var $i=0;$i<$leftnavlinks.length;$i++)
		{
		//click on privacy policy link
		_click(_link($leftnavlinks[$i]));
		//Heading 
		_assertVisible(_heading1($Login_Data[$i][9]));
		//navigate back to my account page
		_click($HEADER_LOGIN_LINK);
		}
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();	


var $t = _testcase("148631", "Verify the functionality related of 'Forgot Password' link on My Account login's page Returning customers section in   site as an  Anonymous user");
$t.start();
try
{	
	//navigate back to my account page
	_click($HEADER_LOGIN_LINK);
	//clicking on forgot password link
	_click($LOGIN_PASSWORD_RESET);
	//verify whether overlay got opened or not
	_assertVisible($LOGIN_DIALOG_CONTAINER);
	_assertVisible($LOGIN_DIALOG_TITLE);
	//_assertEqual($Login_Data[0][5], _getText(_span("ui-dialog-title")));
	_assertVisible($LOGIN_FORGETPASSWORD_CLOSEBUTTON);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("148632", "Verify the UI of 'Forgot Password' overlay on My Account login page in application as an  Anonymous user");
$t.start();
try
{
	//verify the UI of forgot password overlay
	_assertVisible($LOGIN_RESETPASSWORD_HEADING);	
	_assertVisible($LOGIN_FORGETPASSWORD_OVERLAY_TEXT);
	//email field
	_assertVisible($LOGIN_FORGETPASSWORD_EMAIL_LABEL);
	_assertVisible($LOGIN_FORGETPASSWORD_TEXTBOX);
	_assertVisible($LOGIN_FORGETPASSWORD_SEND_BUTTON);
	//closing the dialog
	_click($LOGIN_FORGETPASSWORD_CLOSE_BUTTON);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//var $t = _testcase("124619","Verify the functionality �Send� button related to Forgot password overlay on My Account Login page returning customer section  in application as an  Anonymous user");
//$t.start();
//try
//{
//	//clicking on forgot password link
//	_click($LOGIN_PASSWORD_RESET);
//	//enter valid email id
//	_setValue($LOGIN_FORGETPASSWORD_TEXTBOX,$Login_Data[3][5]);
//	//click on send button
//	_click($LOGIN_FORGETPASSWORD_SEND_BUTTON);
//	//verify the navigation
//	_assertVisible($LOGIN_RESETYOURPASSWORD_TEXT);
//	_assertVisible($LOGIN_FORGETPASSWORD_CLOSE_BUTTON);
//	_assertVisible($LOGIN_FORGETPASSWORD_OVERLAY_TEXT);
//	_assertVisible($HEADER_SALESFORCE_LINK);
//	//closing the dialog
//	_click($LOGIN_FORGETPASSWORD_CLOSEBUTTON);
//}	
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();


var $t = _testcase("124624", "Verify the navigation related to 'Create an account now' button My Account login page, New customers  section  in application as an  Anonymous user");
$t.start();
try
{
	//click on create an account button
	_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//verify the navigation
	_assertVisible($LOGIN_CREATEACCOUNT_HEADING);
	_assertVisible($LOGIN_CREATEACCOUNT_SECTION);
	_assertVisible($LOGIN_CREATEACCOUNT_BUTTON);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148633","Verify the navigation related to  'read more about security' social link on My Account login page, New customers  section  in application as an  Anonymous user");
$t.start();
try
{
	//navigate back to my account page
	_click($HEADER_LOGIN_LINK);	
	//click on read more about security link
	_click($LOGIN_SECURITY_LINK);
	//verify the navigation
	_assertVisible(_link($LOGIN_SECURITYPOLICY_LINK, _in($PAGE_BREADCRUMB)));
	_assertContainsText($Login_Data[2][1], $PAGE_BREADCRUMB);
	_assertVisible(_heading1($LOGIN_SECURITYPOLICY_HEADING));
	_assertVisible($CONTENT_ASSET);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124615","Verify the functionality related of 'LOGIN' button My Account login's page, Returning customers section in    site as an  Anonymous user");
$t.start();
try
{
	//navigate back to my account page
	_click($HEADER_LOGIN_LINK);	
	//login to the application
	login();
	_assertVisible($MY_ACCOUNT_LOGOUT_LINK);
	//click on link not
	_click($MY_ACCOUNT_LOGOUT_LINK);
	//verify the functionality
	_click($HEADER_USERACCOUNT_LINK);
	_assertVisible($HEADER_LOGIN_LINK);
	_click($HEADER_LOGIN_LINK);
	
	//verify my account breadcrumb
	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);


	//enter invalid data
	_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$Login_Data[0][6]);
	_setValue($LOGIN_PASSWORD_TEXTBOX,$Login_Data[1][6]);
	_click($LOGIN_BUTTON);
	//verify the error message
	_assertVisible($LOGIN_ERROR_MESSGAESECTION);
	_assertEqual($Login_Data[2][6], _getText($LOGIN_ERROR_MESSGAESECTION));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//var $t = _testcase("148634","Verify the functionality related to  'ACCOUNT SETTINGS' down arrow on My Account login's page left Nav in   site as an  Anonymous user");
//$t.start();
//try
//{
//	//collect the list present under the account settings
//	var $list=_collectAttributes("_list","/(.*)/","sahiText",_above($LEFTNAV_SHOPCONFIDENTLY_LINK));
//	//click on account settings link
//	_click($LEFTNAV_ACCOUNTSETTINGS_LINK);
//	//after clicking links present under account settings shouldn't visible
//	for(var $i=0;$i<$list.length;$i++)
//		{
//		_assertNotVisible(_listItem($list[$i]));
//		}
//}	
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();


//var $t = _testcase("148635","Verify the functionality related to 'SHOP CONFINDENTLY' down arrow on My Account login's page left Nav in   site as an  Anonymous user");
//$t.start();
//try
//{
//	//collect the list present under the SHOP CONFINDENTLY
//	var $list=_collectAttributes("_list","/(.*)/","sahiText",_under($LEFTNAV_SHOPCONFIDENTLY_LINK));
//	//click on account settings link
//	_click($LEFTNAV_SHOPCONFIDENTLY_LINK);
//	//after clicking links present under account settings shouldn't visible
//	for(var $i=0;$i<$list.length;$i++)
//		{
//		_assertNotVisible(_listItem($list[$i]));
//		}	
//}	
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();



var $t = _testcase("124610/148636","Verify the validation related to the 'Email address'/'password' field on My Account login page, Returning customers section in application as a Anonymous user");
$t.start();
try
{
	
//validating the email id & password field's
for(var $i=0;$i<$Login_Validation.length;$i++)
	{
	
	if($i==9)
		{
			login();
		}
		else if($i==5)
			{
				_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$Login_Validation[$i][1]);
				_setValue($LOGIN_PASSWORD_TEXTBOX,$Login_Validation[$i][2]);
			}
			else
				{
				_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$Login_Validation[$i][1]);
				_setValue($LOGIN_PASSWORD_TEXTBOX,$Login_Validation[$i][2]);
				_click($LOGIN_BUTTON);
				}
	
	if (isMobile())
		{
		_wait(3000);
		}
	//blank
	if($i==0)
		{
		//username
		_assertEqual($Login_Validation[$i][5],_style($LOGIN_EMAIL_ADDRESS_TEXTBOX,"background-color"));
		_assertVisible(_span($Login_Validation[$i][3]));
		//password
		_assertVisible(_span($Login_Validation[$i][4]));
		_assertEqual($Login_Validation[$i][5],_style($LOGIN_PASSWORD_TEXTBOX,"background-color"));
		}
	
	//in valid email
	else if($i==1 || $i==2 || $i==3 ||$i==6 ||$i==7 ||$i==8)
		{
		_assertEqual($Login_Validation[3][4], _getText($LOGIN_ERROR_MESSGAESECTION));
		}
	
	//invalid 
	else if($i==4)
		{
		_assertVisible(_div($Login_Validation[1][3]));
		}
	
	//More Number of characters in email field
	else if ($i==5)
		{
		_assertEqual($Login_Validation[5][3],_getText($LOGIN_EMAIL_ADDRESS_TEXTBOX).length);
		}
	//valid
	else
		{
		if (isMobile())
			{
		_wait(3000,_isVisible($MY_ACCOUNT_OPTIONS));
			}
		_assertVisible($MY_ACCOUNT_LOGOUT_LINK);
		_assertVisible(_heading1("/"+$Login_Data[1][3]+"/"));
		}
	}
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//place order
navigateToShippingPage($Data[0][0],$Data[0][1]);
//shipping details
shippingAddress($Data,3);
_click($SHIPPING_CONTINUE_BUTTON);
//verify address verification overlay
//addressVerificationOverlay();
//Billing details
BillingAddress($Data,3);
//Payment details
PaymentDetails($Data,6);
_click($BILLING_CONTINUE_BUTTON);
//Placing order
_click($ORDERSUMMARY_CONTINUE_BUTTON);
if(_isVisible($ORDERPLACING_POPUP));
	{
	_click($ORDERPLACING_POPUP);
	}
//fetch order num
var $OrderNum=_getText($ORDERCONFIRMATION_ORDERNUMBER);
_log($OrderNum);

_click($HEADER_USERACCOUNT_LINK);
_click($ORDERS_LINK);
_click($ORDER_DETAILS_BUTTON);
_wait(2000);
//var $OrderNum= _extract(_getText(_div("order-number ")),"/Order Number (.*)/", true).toString();
var $PostalCode=_getText($ADDRESS_MINIADDRESS_LOCATION).split(",")[1].split(" ")[2];
//logout from application
_click($HEADER_USERACCOUNT_LINK);
_click($MY_ACCOUNT_LOGOUT_LINK);


var $t = _testcase("124627/148637/124812/288709","Verify the validation related to the 'Order number'/email field field on My Account login page, Check an order section in application as a Anonymous user");
$t.start();
try
{
for(var $i=0;$i<$Order_Validation.length;$i++)
	{
	if($i==0)
		{
		_setValue($LOGIN_ORDERNUMBER_TEXTBOX, "");
		_setValue($LOGIN_ORDEREMAIL_TEXTBOX, "");
		_setValue($LOGIN_ORDER_ZIPCODE_TEXTBOX, "");
		_click($LOGIN_ORDER_SUBMITBUTTON);
		}
	else if($i==8)
		{
		_setValue($LOGIN_ORDERNUMBER_TEXTBOX, $OrderNum);
		_setValue($LOGIN_ORDEREMAIL_TEXTBOX, $UserEmail);
		_setValue($LOGIN_ORDER_ZIPCODE_TEXTBOX, $PostalCode);
		_click($LOGIN_ORDER_SUBMITBUTTON);
		}
		else if($i==7)
		{
		_setValue($LOGIN_ORDERNUMBER_TEXTBOX, $Order_Validation[$i][1]);
		_setValue($LOGIN_ORDEREMAIL_TEXTBOX, $Order_Validation[$i][2]);
		_setValue($LOGIN_ORDER_ZIPCODE_TEXTBOX, $Order_Validation[$i][3]);
		}
	else 
		{
		_setValue($LOGIN_ORDERNUMBER_TEXTBOX, $Order_Validation[$i][1]);
		_setValue($LOGIN_ORDEREMAIL_TEXTBOX, $Order_Validation[$i][2]);
		_setValue($LOGIN_ORDER_ZIPCODE_TEXTBOX, $Order_Validation[$i][3]);
		_click($LOGIN_ORDER_SUBMITBUTTON);
		}
	//blank
	if($i==0)
		{
	_assertEqual($Order_Validation[0][4],_style($LOGIN_ORDERNUMBER_TEXTBOX,"background-color"));
	_assertEqual($Order_Validation[0][4],_style($LOGIN_ORDEREMAIL_TEXTBOX,"background-color"));
	_assertEqual($Order_Validation[0][4],_style($LOGIN_ORDER_ZIPCODE_TEXTBOX,"background-color"));
		}		
	//valid
	else if($i==8)
		{
		_wait(4000);
		_assertVisible(_link($OrderNum, _in($PAGE_BREADCRUMB)));
		_assertVisible(_heading1($Order_Validation[2][4]+" "+$OrderNum));
		}
	//invalid zipcode
	else if($i==3 || $i==4)
		{
		_assertVisible(_div($Order_Validation[1][4]));
		_assertVisible(_div($Order_Validation[1][5]));
		}
	
	//Maximun Langth
	else if($i==7)
		{
		_assertEqual($Order_Validation[4][4],_getText($LOGIN_ORDERNUMBER_TEXTBOX).length);
		_assertEqual($Order_Validation[4][4],_getText($LOGIN_ORDEREMAIL_TEXTBOX).length);
		_assertEqual($Order_Validation[4][4],_getText($LOGIN_ORDER_ZIPCODE_TEXTBOX).length);
		}
	
	//invalid email
	else
		{
		_assertVisible(_div($Order_Validation[1][4]));
		}	
	}
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



cleanup();