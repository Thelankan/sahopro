_include("../../../GenericLibrary/GlobalFunctions.js");

var $UserEmail=$uId;

SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);


_click($HEADER_LOGIN_LINK);
_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$Account[0][0]);
_setValue($LOGIN_PASSWORD_TEXTBOX,$Account[0][1]);
_click($LOGIN_BUTTON);
_wait(3000);

var $t = _testcase("124792/124795", "Verify the navigation to 'Orders' link in the My Account - Home page and Verify the UI  of 'My Account Order history page'  when there are no order records created in application as a  Registered user.");
$t.start();
try
{
	
	_click($ORDERS_LINK);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
		_assertEqual($Generic_Myaccount_orders[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'')); 
		}
	else
		{
		_assertEqual($Generic_Myaccount_orders[0][0], _getText($PAGE_BREADCRUMB));
		}
	//heading
	_assertVisible($ORDERS_HEADING);
	//static text
	_assertVisible($ORDERS_NOORDERS_HEADING);
	_assertEqual($Generic_Myaccount_orders[2][0], _getText($ORDERS_NOORDERS_HEADING));
	//left nav
	_assertVisible($LEFTNAV_SECTION);
	var $Heading=HeadingCountInLeftNav();
	var $Links=LinkCountInLeftNav();
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
		{
			_assertVisible(_span($Generic_Myaccount_orders[$i][1]));			
		}
	//checking links
	for(var $i=0;$i<$Links;$i++)
		{
			_assertVisible(_link($Generic_Myaccount_orders[$i][2]));			
		}
	//left nav content asset(contact us)
	_assertVisible($LEFTNAV_CONTENTASSET);
	
	//logout from application
	_click($HEADER_USERACCOUNT_LINK);
	_click($MY_ACCOUNT_LOGOUT_LINK);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//Login()
_click($HEADER_LOGIN_LINK);
login();

//---------
//place order
navigateToShippingPage($Generic_Myaccount_orders[0][3],0);
//fill shipping details
shippingAddress($Valid_Data,0);
//check use this for billing check box
_check($SHIPPING_BILLING_CHECKBOX);
_click($SHIPPING_CONTINUE_BUTTON);
//UPS verification
if($AddrVerification==$BMConfig[0][2])
{
	_click($SHIPPING_ADDRESS_VERIFICATION_OVERLAY_CONTINUEBUTTON);
}

//entering Credit card Information
if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
    {
		   //PaymentDetails($Valid_Data,4);
		   PaymentDetails($creditcard_data,0); 
           $paymentType="Credit Card";
           _wait(3000,_isVisible($BILLING_CONTINUE_BUTTON));
           _click($BILLING_CONTINUE_BUTTON);
           _wait(10000,_isVisible($ORDERSUMMARY_PLACEORDER_BUTTON));
    }
else if($CreditCard=="No" && $paypalNormal=="Yes")
    {
           _click($PAYPAL_RADIO_BUTTON);
           $paymentType="Pay Pal";
           _wait(3000,_isVisible($BILLING_CONTINUE_BUTTON));
           _click($BILLING_CONTINUE_BUTTON);
           
           _setValue($PAYPAL_EMAIL_TEXTBOX, $paypal[0][0]);
           _setValue($PAYPAL_PASSWORD_TEXTBOX, $paypal[0][1]);
		   _click($PAYPAL_LOGIN_BUTTON);
		   _click($PAYPAL_CONTINUE_BUTTON);
		   _wait(10000,_isVisible($ORDERSUMMARY_PLACEORDER_BUTTON));
    }

//Placing order
_click($ORDERSUMMARY_PLACEORDER_BUTTON);
//fetch the order number from confirmation pae
var $OrderNumber=_getText($ORDERCONFIRMATION_ORDERNUMBER);

_click($HEADER_USERACCOUNT_LINK);	
_click($ORDERS_LINK);
_click($ORDERS_ORDERDETAIL_BUTTON);

if(isIE11() || _isIE10() || _isIE9())
	{
	var $OrderTotal=_extract(_getText($ORDERS_ORDERDETAIL_ORDERTOTAL), "/Order Total:(.*)/",true).toString();
	}
else
	{
	var $OrderTotal=  _extract(_getText($ORDERS_ORDERDETAIL_ORDERTOTAL), "/Order Total: (.*)/",true).toString();
	}
var $ProductName=$ORDERS_ORDERDETAIL_PRODUCTNAME;;
var $Zipcode=_getText($ADDRESS_MINIADDRESS_LOCATION).split(",")[1].split(" ")[2];
var $OrderStatus=$Generic_Myaccount_orders[0][5];
var $OrderInfo=_getText($MYACCOUNT_ORDERS_ORDERDETAIL_PAYMENTSECTION).toString();
var $prodInfo=_getText($ORDERS_ORDERDETAIL_LINEITEMDETAILS).toString();
var $ShipmentInfo=_getText($ORDERS_ORDERDETAIL_SHIPPINGSECTION).toString();
var $billingInfo=_getText($ORDERS_ORDERDETAIL_BILLINGSECTION).toString();


var $t = _testcase("124794","Verify the UI  of 'My Account Order history page'  when there are multiple order records in application as a  Registered user.");
$t.start();
try
{
	_click($LEFTNAV_ORDERHISTORY_LINK);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
		_assertEqual($Generic_Myaccount_orders[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_Myaccount_orders[0][0], _getText($PAGE_BREADCRUMB));
		}
	//heading
	_assertVisible($ORDERS_HEADING);
	//Pagination header
	_assertVisible($ORDERS_PAGINATION);
	//Pagination footer
	_assertVisible($ORDERS_PAGINATION, _under($ORDERS_SEARCHRESULT_ITEMS));
	//Checking the details of the last placed order
	//Order date
	_assertVisible($MYACCOUNT_ORDERS_ORDERDATE);
	//order status
	_assertVisible($ORDERS_ORDERSTATUS);
	if(_isIE9())
		{
			var $temp="Order Status: "+$OrderStatus;
		_assertEqual($temp.replace(/\s/g,''), _getText($ORDERS_ORDERSTATUS).replace(/\s/g,'')); 
		}
	else
		{
		_assertEqual("Order Status: "+$OrderStatus, _getText($ORDERS_ORDERSTATUS));
		}	
	//Order number
	_assertVisible($ORDERS_ORDERNUMBER);
	_assertEqual("Order Number: "+$OrderNumber, _getText($ORDERS_ORDERNUMBER));
	//Order details link
	_assertVisible($ORDERS_ORDERDETAIL_BUTTON);
	//Product name
	_assertEqual($ProductName,_getTableContents($ORDERS_ORDERTABLE,[1],[1]).toString());
	//order total
	_assertEqual($OrderTotal,_getTableContents($ORDERS_ORDERTABLE,[2],[1]).toString());	
	//left nav
	_assertVisible($LEFTNAV_SECTION);
	var $Heading=HeadingCountInLeftNav();
	var $Links=LinkCountInLeftNav();
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
		{
			_assertVisible(_span($Generic_Myaccount_orders[$i][1]));			
		}
	//checking links
	for(var $i=0;$i<$Links;$i++)
		{
			_assertVisible(_link($Generic_Myaccount_orders[$i][2]));			
		}
	//left nav content asset(contact us)
	_assertVisible($LEFTNAV_CONTENTASSET);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124797/124799","Verify the navigation related to 'Home' and 'My Account' links on my account order history page breadcrumb  in application as a Registered user.");
$t.start();
try
{
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
		_assertEqual($Generic_Myaccount_orders[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_Myaccount_orders[0][0], _getText($PAGE_BREADCRUMB));
		}
	
	_click($HEADER_USERACCOUNT_LINK);	
	_click($ORDERS_LINK);
	//Navigation related to My account
	_assertVisible($HEADER_USERACCOUNT_LINK, _in($PAGE_BREADCRUMB));
	_click($HEADER_USERACCOUNT_LINK, _in($PAGE_BREADCRUMB));
	_assertVisible($MY_ACCOUNT_HEADING);
	if(_isIE9())
		{
		_assertEqual($Generic_Myaccount_orders[6][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_Myaccount_orders[6][0], _getText($PAGE_BREADCRUMB));
		}
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("128536","Verify the functionality of Product link in the Order Summary page");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);	
	_click($ORDERS_LINK);
	//Order details link
	_assertVisible($ORDERS_ORDERDETAIL_BUTTON);
	_click($ORDERS_ORDERDETAIL_BUTTON);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("124800/124814","Verify the navigation related to 'Order Details' button  on  'My Account order history'  page in application as a  Registered user./Verify the UI  of 'My Account Order summery page' in application as a  Registered user.");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);	
	_click($ORDERS_LINK);
	//Order details link
	_assertVisible($ORDERS_ORDERDETAIL_BUTTON);
	_click($ORDERS_ORDERDETAIL_BUTTON);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
			var $temp=$Generic_Myaccount_orders[0][0]+" "+$OrderNumber;
		_assertEqual($temp.replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'')); 
		}
	else
		{
		_assertEqual($Generic_Myaccount_orders[0][0]+" "+$OrderNumber, _getText($PAGE_BREADCRUMB));
		}
	
	//heading
	_assertVisible(_heading1($Generic_Myaccount_orders[3][0]+" "+$OrderNumber));
	//Validating UI
	_assertVisible($MYACCOUNT_ORDERS_ORDERDETAIL_PAYMENTSECTION);
	_assertEqual($OrderInfo, _getText($MYACCOUNT_ORDERS_ORDERDETAIL_PAYMENTSECTION));
	_assertVisible($ORDERS_ORDERDETAIL_SHIPPINGSECTION);
	_assertEqual($ShipmentInfo, _getText($ORDERS_ORDERDETAIL_SHIPPINGSECTION));
	//billing details
	_assertVisible($ORDERS_ORDERDETAIL_BILLINGSECTION);
	_assertEqual($billingInfo,_getText($ORDERS_ORDERDETAIL_BILLINGSECTION));
	//product details
	_assertVisible($ORDERS_ORDERDETAIL_LINEITEMDETAILS);
	_assertEqual($prodInfo, _getText($ORDERS_ORDERDETAIL_LINEITEMDETAILS));
	//links
	_assertVisible($ORDERS_RETURNTO_ORDERHISTORY_LINK);
	_assertVisible($ORDERS_RETURNTOSHOPPING_LINK);
	//left nav
	_assertVisible($LEFTNAV_SECTION);
	var $Heading1=HeadingCountInLeftNav();
	var $Links1=LinkCountInLeftNav();
	//checking headings
	for(var $i=0;$i<$Heading1;$i++)
		{
			_assertVisible(_span($Generic_Myaccount_orders[$i][1]));			
		}
	//checking links
	for(var $i=0;$i<$Links1;$i++)
		{
			_assertVisible(_link($Generic_Myaccount_orders[$i][2]));			
		}
	//left nav content asset(contact us)
	_assertVisible($LEFTNAV_CONTENTASSET);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124808","Verify the navigation related to 'Return to order history'  link on  'My Account order summery' page in application as a  Registered  user.");
$t.start();
try
{
	_assertVisible($ORDERS_RETURNTO_ORDERHISTORY_LINK);
	_click($ORDERS_RETURNTO_ORDERHISTORY_LINK);
	//Validating the navigation
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
		_assertEqual($Generic_Myaccount_orders[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_Myaccount_orders[0][0], _getText($PAGE_BREADCRUMB));
		}
	//heading
	_assertVisible($ORDERS_HEADING);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124810","Verify the navigation related to 'Return to shopping'  link on  'My Account order summery' page in application as a  Registered  user.");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);	
	_click($ORDERS_LINK);
	//Order details link
	_click($ORDERS_ORDERDETAIL_BUTTON);
	//return to shopping link
	_assertVisible($ORDERS_RETURNTOSHOPPING_LINK);
	_click($ORDERS_RETURNTOSHOPPING_LINK);
	//Validating the navigation
	_assertVisible($HOMEPAGE_SLIDES);
	_assertVisible($HOMEPAGE_SLOTS);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124805","Verify the navigation related to 'Home'  and 'My Account links   on  'My Account order summery' page bread crumb in application both a Registered user.");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);	
	_click($ORDERS_LINK);
	//Order details link
	_click($ORDERS_ORDERDETAIL_BUTTON);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
			var $temp=$Generic_Myaccount_orders[0][0]+" "+$OrderNumber;
		_assertEqual($temp.replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'')); 
		}
	else
		{
		_assertEqual($Generic_Myaccount_orders[0][0]+" "+$OrderNumber, _getText($PAGE_BREADCRUMB));
		}
	
	//Validating navigation on click of order history
	_assertVisible($LEFTNAV_ORDERHISTORY_LINK, _in($PAGE_BREADCRUMB));
	_click($LEFTNAV_ORDERHISTORY_LINK, _in($PAGE_BREADCRUMB));
	//bread crumb
	if(_isIE9())
		{
		_assertEqual($Generic_Myaccount_orders[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_Myaccount_orders[0][0], _getText($PAGE_BREADCRUMB));
		}
	//heading
	_assertVisible($ORDERS_HEADING);	
	
	_click($HEADER_USERACCOUNT_LINK);	
	_click($ORDERS_LINK);
	//Order details link
	_click($ORDERS_ORDERDETAIL_BUTTON);
	//Validating navigation on click of My account
	_assertVisible($HEADER_USERACCOUNT_LINK, _in($PAGE_BREADCRUMB));
	_click($HEADER_USERACCOUNT_LINK, _in($PAGE_BREADCRUMB));
	_assertVisible($MY_ACCOUNT_HEADING);
	if(_isIE9())
		{
		_assertEqual($Generic_Myaccount_orders[6][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_Myaccount_orders[6][0], _getText($PAGE_BREADCRUMB));
		}
	
	_click($HEADER_USERACCOUNT_LINK);	
	_click($ORDERS_LINK);
	//Order details link
	_click($ORDERS_ORDERDETAIL_BUTTON);
	//click of Home page link
	_click($HEADER_SALESFORCE_LINK);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125909","Verify the navigation related to product name on  'My Account order summery' page in application as a  Registered  user.");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);	
	_click($ORDERS_LINK);
	//Order details link
	_click($ORDERS_ORDERDETAIL_BUTTON);
	//checking Product name
	_assertVisible($ORDERS_ORDERDETAIL_PRODUCTNAME);
	var $PName=_getText($ORDERS_ORDERDETAIL_PRODUCTNAME);
	_click($ORDERS_ORDERDETAIL_PRODUCTNAME);
	//Validating navigation
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertContainsText($PName,$PAGE_BREADCRUMB);
	//heading
	_assertEqual($PName, _getText($PDP_PRODUCTNAME));	
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
//logout from application
_click($HEADER_USERACCOUNT_LINK);
_click($MY_ACCOUNT_LOGOUT_LINK);



//defect in application need to remove comments once fixed
///user login
_click($HEADER_LOGIN_LINK);
_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$Account[1][0]);
_setValue($LOGIN_PASSWORD_TEXTBOX,$Account[1][1]);
_click($LOGIN_BUTTON);


//Comment below code if you are already executed this script once

for(var $i=0;$i<30;$i++)
{
		//Placing an order
		//Navigate to cart
		navigateToShippingPage($Generic_Myaccount_orders[0][3],$Generic_Myaccount_orders[0][4]);
		//Shipping
		shippingAddress($Valid_Data,0);
		//Billing
		_click($SHIPPING_CONTINUE_BUTTON);
		if(_isVisible($SHIPPING_ADDRESS_VERIFICATION_OVERLAY))
		{
		_click($SHIPPING_ADDRESS_VERIFICATION_OVERLAY_CONTINUEBUTTON);
		}
		//enter valid billing address
		BillingAddress($Valid_Data,0);
		//enter credit card details
		if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
		{
		      PaymentDetails($Valid_Data,4);
		      //click on continue
		      _click($BILLING_CONTINUE_BUTTON);   
		}
		else if($CreditCard=="No" && $paypalNormal=="Yes")
		{
		      _assert($PAYPAL_RADIO_BUTTON.checked);
		//click on continue
		_click($BILLING_CONTINUE_BUTTON);   
		//paypal
        _setValue($PAYPAL_EMAIL_TEXTBOX, $paypal[0][0]);
		_setValue($PAYPAL_PASSWORD_TEXTBOX, $paypal[0][1]);
		_click($PAYPAL_LOGIN_BUTTON);
		_click($PAYPAL_CONTINUE_BUTTON);
		_wait(10000,_isVisible($ORDERSUMMARY_PLACEORDER_BUTTON));
		}
		//Click on submit order
		_click($ORDERSUMMARY_PLACEORDER_BUTTON);
		if(_isVisible($ORDERPLACING_POPUP))
		{
		_click($ORDERPLACING_POPUP);
		}
}

var $t = _testcase("125908","Verify the functionality related to pagination  on  'My Account order history'  page in application as a  Registered user.");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);
	_click($ORDERS_LINK);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
			_assertEqual($Generic_Myaccount_orders[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,''));
		}
	else
		{
			_assertEqual($Generic_Myaccount_orders[0][0], _getText($PAGE_BREADCRUMB));
		}	
	//Pagination
	if(_isVisible($SHOPNAV_PAGINATION_LINK))
		{
			pagination();
		}
	else
		{
			_assert(false, "Pagination is not displaying");
		}
	//logout from application
	_click($HEADER_USERACCOUNT_LINK);
	_click($MY_ACCOUNT_LOGOUT_LINK);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124820","Verify the navigation related to 'Home'  and 'My Account links   on  'My Account order summery' page bread crumb in application both as an  Anonymous user.");
$t.start();
try
{
	//navigate to Login page 
	_click($HEADER_LOGIN_LINK);
	//navigating to order summary
	CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
			var $temp=$Generic_Myaccount_orders[0][0]+" "+$OrderNumber;
		_assertEqual($temp.replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'')); 
		}
	else
		{
		_assertEqual($Generic_Myaccount_orders[0][0]+" "+$OrderNumber, _getText($PAGE_BREADCRUMB));
		}
	//heading
	_assertVisible(_heading1($Generic_Myaccount_orders[3][0]+" "+$OrderNumber));
	
	//Validating navigation on click of order history
	_assertVisible($LEFTNAV_ORDERHISTORY_LINK, _in($PAGE_BREADCRUMB));
	_click($LEFTNAV_ORDERHISTORY_LINK, _in($PAGE_BREADCRUMB));
	//bread crumb
	if(_isIE9())
		{
		_assertEqual($Generic_Myaccount_orders[6][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_Myaccount_orders[6][0], _getText($PAGE_BREADCRUMB));
		}
	//heading
	_assertVisible($LOGIN_HEADING);
	_assertVisible($LOGIN_SECTION);
	
	//navigate to Login page 
	_click($HEADER_LOGIN_LINK);
	//navigating to order summary
	CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode);
	//Validating navigation on click of My account
	_assertVisible($HEADER_USERACCOUNT_LINK, _in($PAGE_BREADCRUMB));
	_click($HEADER_USERACCOUNT_LINK, _in($PAGE_BREADCRUMB));
	//bread crumb
	if(_isIE9())
		{
		_assertEqual($Generic_Myaccount_orders[6][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_Myaccount_orders[6][0], _getText($PAGE_BREADCRUMB));
		}
	//heading
	_assertVisible($LOGIN_HEADING);
	_assertVisible($LOGIN_SECTION);
	
	//navigate to Login page 
	_click($HEADER_LOGIN_LINK);
	//navigating to order summary
	CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode);
	//Validating navigation on click of Home
	_click($HEADER_SALESFORCE_LINK);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124815","Verify the navigation related to 'Return to shopping'  link on  'My Account order summery' page in application as a Anonymous user.");
$t.start();
try
{
	//navigate to Login page 
	_click($HEADER_LOGIN_LINK);
	//navigating to order summary
	CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode);
	//return to shopping link
	_assertVisible($ORDERS_RETURNTOSHOPPING_LINK);
	_click($ORDERS_RETURNTOSHOPPING_LINK);
	//Validating the navigation
	_assertVisible($HOMEPAGE_SLIDES);
	_assertVisible($HOMEPAGE_SLOTS);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();

