_include("../../../GenericLibrary/GlobalFunctions.js");

var $color=$Generic_GiftRegistry[0][6];
var $color1=$Generic_GiftRegistry[1][6];
//from util.GenericLibrary/BrowserSpecific.js
var $FirstName1=$FName;
var $LastName1=$LName;
var $UserEmail=$uId;


SiteURLs();
cleanup();
_setAccessorIgnoreCase(true); 
//Login
_click($GIFTREGISTRY_USERACCOUNT_LINK);
_click($GIFTREGISTRY_LOGIN_LINK);
login();
//Deleting addresses, which intern will delete wishlist and gift registry
deleteAddress();

var $t = _testcase("124348", "Verify the UI  on Gift registery page in application as a  Registered user.");
$t.start();
try
{
	_click($GIFTREGISTRY_MY_ACCOUNT_LINK);
	_click($GIFTREGISTRY_LINK);
	//click on new registry
	_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
	//creating the registry
	createRegistry($GiftRegistry, 0);
	//Pre/Post Event shipping info
	Pre_PostEventInfo($GiftRegistry, 4);
	//Submit button
	_click($GIFTREGISTRY_EVENTINFORMATION_CONTINUE_BUTTON);
	//Validating Navigation
	_assertVisible($GIFTREGISTRY_NOGIFTREGISTRY_HEADING);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
		}
	
	//Tabs
	_assertVisible($GIFTREGISTRY_CONTENT_TABS);
	//registry
	_assertVisible($GIFTREGISTRY_REGISTER_TAB);
	_assertEqual($Generic_GiftRegistry[1][10], _getText($GIFTREGISTRY_REGISTER_TAB));
	//event info
	_assertVisible($GIFTREGISTRY_EVENT_TAB);
	_assertEqual($Generic_GiftRegistry[2][10], _getText($GIFTREGISTRY_EVENT_TAB));
	//shipping info
	_assertVisible($GIFTREGISTRY_SHIPPINGINFO_TAB);
	_assertEqual($Generic_GiftRegistry[3][10], _getText($GIFTREGISTRY_SHIPPINGINFO_TAB));
	//purchases
	_assertVisible($GIFTREGISTRY_PURCHASES_TAB);
	_assertEqual($Generic_GiftRegistry[4][10], _getText($GIFTREGISTRY_PURCHASES_TAB));
	//gift registry details
	_assertVisible($GIFTREGISTRY_PAGE_CONTENT_TAB);
	_assertVisible(_heading2($GiftRegistry[0][1]+" - "+$GiftRegistry[0][15]));
	_assertVisible($GIFTREGISTRY_NOITEMSIN_GF_TEXT);
	//add items link
	_assertVisible($GIFTREGISTRY_ADDITEMS_LINK);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124347", "Verify the navigation related to the 'Home' and  'My account' bread crumb on Gift registery page   in application as a  Registered user.");
$t.start();
try
{
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
		}
	
	//verify the navigation to home page
	_click($HEADER_SALESFORCE_LINK);
	
	_assertVisible($HOMEPAGE_SLIDES);
	_assertVisible($HOMEPAGE_SLOTS);
	
	_click($GIFTREGISTRY_USERACCOUNT_LINK);
	_click($GIFTREGISTRY_LINK);
	//click on new registry
	_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
	//creating the registry
	createRegistry($GiftRegistry, 0);
	//Pre/Post Event shipping info
	Pre_PostEventInfo($GiftRegistry, 4);
	//Submit button
	_click($GIFTREGISTRY_EVENTINFORMATION_CONTINUE_BUTTON);
	//Validating Navigation
	_assertVisible($GIFTREGISTRY_NOGIFTREGISTRY_HEADING);
	//Navigation related to My account
	_assertVisible($GIFTREGISTRY_MY_ACCOUNT_LINK, _in($PAGE_BREADCRUMB));
	_click($GIFTREGISTRY_MY_ACCOUNT_LINK, _in($PAGE_BREADCRUMB));
	_assertVisible($GIFTREGISTRY_MY_ACCOUNT_HEADING);
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[7][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[7][0], _getText($PAGE_BREADCRUMB));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("128722", "Verify creation of Gift Registry for a logged in User");
$t.start();
try
{
	//Deleting
	Delete_GiftRegistry();
	//creating the registry
	_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
	createRegistry($GiftRegistry, 0);
	//Pre/Post Event shipping info
	Pre_PostEventInfo($GiftRegistry, 4);
	_click($GIFTREGISTRY_EVENTINFORMATION_CONTINUE_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124356", "Verify the functionality related to the 'Add Items' on Gift registery page, find someone's Gift registery list section in applicationas a  Registered user.");
$t.start();
try
{
	//verify the navigation to home page
	_click($HEADER_SALESFORCE_LINK);
	
	_click($GIFTREGISTRY_LINK);
	//Click on view 
	_click($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
	//adding items to gift registry
	addItems($Generic_GiftRegistry[0][11]);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("288236","Verify the application behavior on click of apply button in edit event info page as a Registered user having saved gift registry");
$t.start();
try
{
	
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124323/289812", "Verify the functionality related to 'View' link in giftregistery page  in application as a  Registered user./Verify the application behavior on click of view link for a gift registry in gift registry search results page as a guest or registered user");
$t.start();
try
{
	//verify the navigation to home page
	_click($HEADER_SALESFORCE_LINK);

	_click($GIFTREGISTRY_LINK);
	//Click on view 
	_click($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
		}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("124324", "Verify the UI  on Gift Registry page after clicking the �View� button in application as a  Registered user.");
$t.start();
try
{
	//Validating Navigation
	_assertVisible($GIFTREGISTRY_NOGIFTREGISTRY_HEADING);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'')); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
		}
	
	//Tabs
	_assertVisible($GIFTREGISTRY_CONTENT_TABS);
	_assertVisible($GIFTREGISTRY_MAKETHISPUBLIC_BUTTON);
	if(_isVisible($GIFTREGISTRY_MAKETHISPUBLIC_BUTTON))
		{
			_click($GIFTREGISTRY_MAKETHISPUBLIC_BUTTON);	
		}
	//make this registry private
	_assertVisible($GIFTREGISTRY_MAKETHISPRIVATE_BUTTON);
	//verifying url
	if (!_isVisible($GIFTREGISTRY_MAKETHISPUBLIC_BUTTON))
		{
		_click($GIFTREGISTRY_SHARELINK);
		_assertVisible($GIFTREGISTRY_URL_LINK);
		_assertVisible($GIFTREGISTRY_URL_SECTION);
		}
	else
		{
		_click($GIFTREGISTRY_MAKETHISPUBLIC_BUTTON);
		_click($GIFTREGISTRY_SHARELINK);
		_assertVisible($GIFTREGISTRY_URL_LINK);
		_assertVisible($GIFTREGISTRY_URL_SECTION);
		}
	//heading
	_assertVisible(_heading2($GiftRegistry[0][1]+" - "+$GiftRegistry[0][15]));
	//Item image
	_assertVisible($GIFTREGISTRY_PRODUCT_IMAGE);
	//Item name
	_assertVisible($GIFTREGISTRY_PRODUCT_NAME);
	//edit details link
	_assertVisible($GIFTREGISTRY_PRODUCT_EDIT_DETAILS);
	//product details
	_assertVisible($GIFTREGISTRY_PRODUCT_ITEMDETAILS);
	//date added
	_assertVisible($GIFTREGISTRY_PRODUCT_DATEADDED_SECTION);
	_assertVisible($GIFTREGISTRY_DATEADDED_LABLE);
	//make this item public check box
	_assertVisible($GIFTREGISTRY_MAKETHISITEMPUBLIC_CHECKBOX);
	_assert($GIFTREGISTRY_MAKETHISITEMPUBLIC_CHECKBOX.checked);
	//update
	_assertVisible($GIFTREGISTRY_PRODUCTUPDATE_LINK);
	//remove
	_assertVisible($GIFTREGISTRY_PRODUCTREMOVE_LINK);
	//quantity text box
	_assertVisible($GIFTREGISTRY_PRODUCT_QUANTITYBOX);
	//add to cart
	_assertVisible($GIFTREGISTRY_ADDTOCART_BUTTON);	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124423", "Verify the Naviagetion related to  'Edit details' link present  on Gift registery page, find someone's Gift registery list section in applicationas a  Registered user.");
$t.start();
try
{
	_assertVisible($GIFTREGISTRY_PRODUCT_EDIT_DETAILS);
	var $productName=_getText($GIFTREGISTRY_PRODUCT_NAME);
	var $productPrice=_getText($GIFTREGISTRY_PRODUCT_PRICE);
	_click($GIFTREGISTRY_PRODUCT_EDIT_DETAILS);
	//validating navigation
	_assertEqual($productName, _getText($GIFTREGISTRY_PRODUCTNAME_EDITDETAILS_OVERLAY));
	_assertEqual($productPrice, _getText($GIFTREGISTRY_PRODUCTPRICE_EDITDETAILS_OVERLAY));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124424", "Verify the functionality related to  'Update' link present  on Gift registery page, find someone's Gift registery list section in applicationas a  Registered user.");
$t.start();
try
{
	_click($GIFTREGISTRY_LINK);
	//Click on view 
	_click($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
	_setValue($GIFTREGISTRY_QUANTITYDESIRED_NUMBERBOX, $Generic_GiftRegistry[0][14]);
	//selectig value
	_setSelected($GIFTREGISTRY_PRIORITY_DROPDOWN,$Generic_GiftRegistry[1][14]);
	_uncheck($GIFTREGISTRY_MAKETHIS_ITEM_PUBLIC_CHECKBOX);
	_click($GIFTREGISTRY_PRODUCTUPDATE_LINK);
	//validating
	_assertEqual($Generic_GiftRegistry[0][14], _getValue($GIFTREGISTRY_QUANTITYDESIRED_NUMBERBOX));
	_assertEqual($Generic_GiftRegistry[0][14], _getValue($GIFTREGISTRY_PRODUCT_QUANTITYBOX));
	_assertEqual($Generic_GiftRegistry[1][14], _getSelectedText($GIFTREGISTRY_PRIORITY_DROPDOWN));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124425", "Verify the functionality related to  'Remove' link present  on Gift registery page, find someone's Gift registery list section in applicationas a  Registered user.");
$t.start();
try
{
	_assertVisible($GIFTREGISTRY_PRODUCTREMOVE_LINK);
	_click($GIFTREGISTRY_PRODUCTREMOVE_LINK);
	//Item name
	_assertNotVisible($GIFTREGISTRY_PRODUCT_NAME);
	//edit details link
	_assertNotVisible($GIFTREGISTRY_PRODUCT_EDIT_DETAILS);
	//product details
	_assertNotVisible($GIFTREGISTRY_PRODUCT_ITEMDETAILS);
	//gift registry details
	_assertVisible($GIFTREGISTRY_PAGE_CONTENT_TAB);
	_assertVisible(_heading2($GiftRegistry[0][1]+" - "+$GiftRegistry[0][15]));
	_assertVisible($GIFTREGISTRY_NOITEMSIN_GF_TEXT);
	//add items link
	_assertVisible($GIFTREGISTRY_ADDITEMS_LINK);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//adding items to gift registry
addItems($Generic_GiftRegistry[0][11]);
var $t = _testcase("124422", "Verify the functionality related to  'ADD TO CART' button present  on Gift registery page, find someone's Gift registery list section in applicationas a  Registered user.");
$t.start();
try
{
	//Click on add to cart
	_click($GIFTREGISTRY_ADDTOCART_BUTTON);
	//validating
	_assertEqual($ProductName, _getText($GIFTREGISTRY_MINICART_NAME));
	if(_isIE9())
		{
		_assertEqual($QTY, _extract(_getText($GIFTREGISTRY_MINICART_PRICE), "/Qty: (.*)[$]/", true).toString()) 
		}
	else
		{
		_assertEqual($QTY, _extract(_getText($GIFTREGISTRY_MINICART_PRICE), "/Qty: (.*) [$]/", true).toString());
		}
	_assertEqual($ProductPrice, _extract(_getText($GIFTREGISTRY_MINICART_SUBTOTAL), "/Order Subtotal (.*)/", true).toString());
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//removing items from cart
ClearCartItems()

_click($GIFTREGISTRY_LINK);
//Click on view 
_click($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
var $t = _testcase("124350", "Verify the functionality related to the ' 'Make This Registry Private' button on Gift registery page  in application as a  Registered user.' ");
$t.start();
try
{
	//click on 'Make This Registry Private'
	_click($GIFTREGISTRY_MAKETHISPRIVATE_BUTTON);
	//Make this registry public button
	_assertVisible($GIFTREGISTRY_MAKETHISPUBLIC_BUTTON);
	//checking URL
	//_assertNotVisible(_paragraph("/(.*)/", _in(_div("list-share"))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124349", "Verify the functionality related to the ' 'Make This Registry Public' button on Gift registery page in application as a  Registered user.' ");
$t.start();
try
{
	//Make this registry public button
	_assertVisible($GIFTREGISTRY_MAKETHISPUBLIC_BUTTON);
	//click on make this registry public button
	_click($GIFTREGISTRY_MAKETHISPUBLIC_BUTTON);
	//Validating navigation
	//make this registry private
	_assertVisible($GIFTREGISTRY_MAKETHISPRIVATE_BUTTON);
	//make this item public check box
	_assertVisible($GIFTREGISTRY_MAKETHISITEMPUBLIC_CHECKBOX);
	_assert($GIFTREGISTRY_MAKETHISITEMPUBLIC_CHECKBOX.checked);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124322", "Verify the UI  on Gift Registry page Non empty list of Gift Registry in application as a  Registered user.");
$t.start();
try
{
	_click($GIFTREGISTRY_USERACCOUNT_LINK);
	
	_click($GIFTREGISTRY_LINK);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
		}
	//heading
	_assertVisible($GIFTREGISTRY_FINDSOMEONES_REGISTRY_HEADING);
	//left nav
	_assertVisible($LEFTNAV_SECTION);
	var $Heading=HeadingCountInLeftNav();
	var $Links=LinkCountInLeftNav();
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
		{
			_assertVisible(_span($Generic_GiftRegistry[$i][1]));			
		}
	//checking links
	for(var $i=0;$i<$Links;$i++)
		{
			_assertVisible(_link($Generic_GiftRegistry[$i][2]));			
		}
	//left nav content asset(contact us)
	_assertVisible($LEFTNAV_ASSET);
	
	//last name
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXTBOX);
	//first name
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXTBOX);
	//email 
	_assertVisible($GIFTREGISTRY_EVENTTYPE_TEXT);
	_assertVisible($GIFTREGISTRY_EVENTTYPE_DROPDOWN);
	//find button
	_assertVisible($GIFTREGISTRY_FIND_BUTTON);
	//advanced search
	_assertVisible($GIFTREGISTRY_ADVANCE_LINK);
	//new registry button
	_assertVisible($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
	//text
	_assertVisible($GIFTREGISTRY_CREATENEW_REGISTRY_TEXT);
	//UI with gift registry's
	//heading
	_assertVisible($GIFTREGISTRY_YOURGIFTREGISTRY_HEADING);
	//table header
	_assertVisible($GIFTREGISTRY_YOURGIFTREGISTRY_ROW_ATTRIBUTES);
	//registry contents
	_assertVisible(_row($GiftRegistry[0][2]+" "+$GiftRegistry[0][1]+" "+$GiftRegistry[0][3]+" "+$GiftRegistry[0][6]+", "+$GiftRegistry[1][5]+" View Delete"));
	//view link
	_assertVisible($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
	//delete link
	_assertVisible($GIFTREGISTRY_DELETE_REGISTRY_LINK);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124357", "Verify the UI elements after clicking 'Advanced Search' link  on Gift registery page, find someone's Gift registery list section in applicationas a  Registered user.");
$t.start();
try
{
	_click($GIFTREGISTRY_ADVANCESEARCH_LINK);
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXTBOX);
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXTBOX);
	_assertVisible($GIFTREGISTRY_EVENTTYPE_DROPDOWN);
	_assertVisible($GIFTREGISTRY_EVENTTYPE_DROPDOWN);
	_assertVisible($GIFTREGISTRY_EVENTMONTH_TEXTBOX);
	_assertVisible($GIFTREGISTRY_EVENTYEAR_TEXTBOX);
	_assertVisible($GIFTREGISTRY_EVENTCITY_TEXTBOX);
	//_assertVisible($GIFTREGISTRY_STATE_DROPDOWN);
	//_assertVisible($GIFTREGISTRY_COUNTRY_DROPDOWN);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124358/148787/148788/148789/148787148790/148791", "Verify validation related to Event name/Event City/ Event Month text fields/Year drop down/State drop down/Country drop down  present  on Gift registery page, find someone's Gift registery list section in applicationas a  Registered user.");
$t.start();
try
{
	for(var $i=0;$i<$Advance.length;$i++)
	{
		_setValue($GIFTREGISTRY_LASTNAME_TEXTBOX,$LastName1);
		_setValue($GIFTREGISTRY_FIRSTNAME_TEXTBOX,$FirstName1);
		_setValue($GIFTREGISTRY_EVENTTYPE_DROPDOWN,$Advance[$i][2]);
		_setValue($GIFTREGISTRY_EVENTTYPE_DROPDOWN,$Advance[$i][3]);
		_setSelected($GIFTREGISTRY_EVENTMONTH_TEXTBOX,$Advance[$i][4]);
		_setSelected($GIFTREGISTRY_EVENTYEAR_TEXTBOX,$Advance[$i][5]);
		_setValue($GIFTREGISTRY_EVENTCITY_TEXTBOX,$Advance[$i][6]);		
		//_setSelected($GIFTREGISTRY_STATE_DROPDOWN,$Advance[$i][7]);
		//_setSelected($GIFTREGISTRY_COUNTRY_DROPDOWN,$Advance[$i][8]);
		_click($GIFTREGISTRY_FIND_BUTTON);
		_assertVisible($GIFTREGISTRY_SEARCHRESULT_HEADING);
		if($i==$Advance.length-1)
			{
				_assertVisible($GIFTREGISTRY_SEARCHRESULT_CONTENT);
				_assertVisible(_row($LastName1+" "+$FirstName1+" "+$Advance[$i][2]+" "+$Advance[0][10]+" "+$Advance[$i][6]+", "+$Advance[$i][7]+" View"));
			}
		else
			{
				_assertVisible(_paragraph($Generic_GiftRegistry[10][0]+" "+$FirstName1+" "+$LastName1+" , "+$Generic_GiftRegistry[11][0]));
			}	
		_click($GIFTREGISTRY_ADVANCESEARCH_LINK);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("288157/288158","Verify validation related to Last name text field present in find someone's gift registry section on gift registry page in application as a registered user");
$t.start();
try
{
//navigating to Gift Registry page
_click($GIFTREGISTRY_LINK);
//validating the fields last name,first name,email 
for(var $i=0;$i<$Validations_GiftRegistry.length;$i++)
  {
	_setValue($GIFTREGISTRY_LASTNAME_TEXTBOX, $Validations_GiftRegistry[$i][1]);
	_setValue($GIFTREGISTRY_FIRSTNAME_TEXTBOX, $Validations_GiftRegistry[$i][2]);
	
	//click on find button
	_click($GIFTREGISTRY_FIND_BUTTON);
	//Blank
	if($i==0)
		{
			//last name,first name fields should highlight
			_assertEqual($Validations_GiftRegistry[0][4],_style($GIFTREGISTRY_LASTNAME_TEXTBOX,"background-color"));
			_assertEqual($Validations_GiftRegistry[0][4],_style($GIFTREGISTRY_FIRSTNAME_TEXTBOX,"background-color"));
		}
	//invalid
	if($i==2 || $i==3 || $i==4 || $i==5 || $i==6)
		{
			_setSelected($GIFTREGISTRY_EVENTTYPE_DROPDOWN,$Validations_GiftRegistry[$i][3]);
			//user should be in same page
			_assertVisible(_heading1("/"+$Giftregistry_guest[8][1]+"/"));
			//verify the text message
			if(i==6)
				{
				_setValue($GIFTREGISTRY_EVENTTYPE_DROPDOWN, $Validations_GiftRegistry[$i][3]);
				_assertVisible($GIFTREGISTRY_PRODUCT_TABLES);
				_assertVisible($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
				}
			else
				{
				
				_assertVisible($GIFTREGISTRY_NOGIFTS_MESSAGE);
				}
		}
	//max characters
	if($i==1)
		{
		_assertEqual($Validations_GiftRegistry[1][4],_getText($GIFTREGISTRY_LASTNAME_TEXTBOX).length);
		_assertEqual($Validations_GiftRegistry[1][4],_getText($GIFTREGISTRY_FIRSTNAME_TEXTBOX).length);		
		}
	}	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("124325", "Verify the functionality related to 'Delete' link  in Gift Registry page  in application as a  Registered user.");
$t.start();
try
{
	_click($GIFTREGISTRY_MY_ACCOUNT_LINK);
	_click($GIFTREGISTRY_LINK);
	if(_isVisible($GIFTREGISTRY_DELETE_REGISTRY_LINK))
	  {
	  
	      _click($GIFTREGISTRY_DELETE_REGISTRY_LINK);
	      _expectConfirm("Do you want to remove this gift registry?", true); 
	      _assertNotVisible($GIFTREGISTRY_YOURGIFTREGISTRY_HEADING);
	      _assertNotVisible(_table("item-list"));
	    _log("Gift Registry deleted successfully");
	  }
	  else
	  { 
	    _assert(false, "Delete Link is not present");
	  }
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


_click($HEADER_SALESFORCE_LINK);

cleanup();