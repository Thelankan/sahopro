_include("../../../GenericLibrary/GlobalFunctions.js");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("124303/124308", "Verify 'GIFT REGISTRY' link at the Header for Guest User, Verify the UI of 'Returning customers' section Gift Registry login page in application as an Anonymous user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click($GIFTREGISTRY_LINK);
	//verify bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertVisible($GIFTREGISTRY_LINK, _in($PAGE_BREADCRUMB));
	//Verifying the UI of returning customer section
	//MyAccount login as heading
	_assertVisible($GIFTREGISTRY_LOGIN_HEADING);
	//Returning customer heading
	_assertVisible($GIFTREGISTRY_LOGIN_RETURNINGCUSTOMERS_HEADING);
	//Paragraph with static text
	_assertVisible($GIFTREGISTRY_LOGIN_TEXT);
	//email Address
	_assertVisible($GIFTREGISTRY_LOGIN_EMAILTEXT);
	_assertVisible($LOGIN_EMAIL_ADDRESS_TEXTBOX);
	_assertEqual("", _getValue($LOGIN_EMAIL_ADDRESS_TEXTBOX));
	//password
	_assertVisible($GIFTREGISTRY_LOGIN_PASSWORDTEXT);
	_assertVisible($LOGIN_PASSWORD_TEXTBOX);
	_assertEqual("", _getValue($LOGIN_PASSWORD_TEXTBOX));
	//remember me
	_assertVisible($LOGIN_REMEMBERMETEXT);
	_assertVisible($LOGIN_REMEMBEMETEXTBOX);
	_assertNotTrue($LOGIN_REMEMBEMETEXTBOX.checked);
	//login button
	_assertVisible($LOGIN_BUTTON);
	//forgot password
	_assertVisible($LOGIN_PASSWORD_RESET);
	_assertEqual($GIFTREGISTRY_FORGETPASSWORD_TEXT, _getText($LOGIN_PASSWORD_RESET));
	//social links
	//_assertVisible(_paragraph($Giftregistry_guest[10][0]));
	_assertVisible($LOGIN_ENTIRE_LOGIN_SECTION);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("124309","Verify the UI of ' New Customer's' section Gift Registry login page in application as an  Anonymous user");
$t.start();
try
{
	//verify the UI of New Customer section
	_assertVisible($$GIFTREGISTRY_CREATEACCOUNTNOW_HEADING);
	_assertVisible($CONTENT_ASSET);
	_assertVisible($GIFTREGISTRY_CREATEACCOUNTNOW_BUTTON);
	_assertVisible($GIFTREGISTRY_LOGIN_PARAGRAPH);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124310","Verify the UI of 'FIND SOMEONE'S GIFT REGISTRY section Gift Registry login page in application as an Anonymous user");
$t.start();
try
{
	//verify the UI of 'FIND SOMEONE'S GIFT REGISTRY
	_assertVisible($GIFTREGISTRY_FINDSOMEONES_REGISTRY_HEADING);
	_assertVisible($GIFTREGISTRY_FINDSOMEONES_REGISTRY_PARAGRAPH);
	//last name
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXTBOX);
	//first name
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXTBOX);
	//email 
	_assertVisible($GIFTREGISTRY_EVENTTYPE_TEXT);
	_assertVisible($GIFTREGISTRY_EVENTTYPE_DROPDOWN);
	//find button
	_assertVisible($GIFTREGISTRY_FIND_BUTTON);
	//advanced search
	_assertVisible($GIFTREGISTRY_ADVANCE_LINK);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("----","Verify the UI of 'Gift Registry login page' in application as an Anonymous user");
$t.start();
try
{
	_assertVisible($PAGE_BREADCRUMB);	
	_assertVisible($GIFTREGISTRY_LINK, _in($PAGE_BREADCRUMB));		
	_assert(_isVisible($GIFTREGISTRY_LOGIN_HEADING));
	//left nav section
	_assertVisible($LEFTNAV_ACCOUNTSETTINGS_LINK);
	_assertVisible($LEFTNAV_CREATEACCOUNT_LINK);
	_assertVisible($LEFTNAV_SHOPCONFIDENTLY_LINK);
	_assertVisible($LEFTNAV_PRIVACYPOLICY_LINK);
	_assertVisible($LEFTNAV_SECURESHOPPING_LINK);
	//need help section
	_assertVisible($GIFTREGISTRY_NEED_HELP_HEADING);
	_assertVisible($GIFTREGISTRY_LEFTNAV_CONTENTASSET);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124304","Verify the navigation related to 'Home' and 'My account' links in Gift Registry login page bread crumb in application as an Anonymous user");
$t.start();
try
{
	
	//verify the navigation to home page
	_click($HEADER_SALESFORCE_LINK);
	
	_assertVisible($HOMEPAGE_SLIDES);
	_assertVisible($HOMEPAGE_SLOTS);
	//navigating to Gift Registry page
	_click($GIFTREGISTRY_LINK);
	//click on my account link present in the bread crumb
	_click($GIFTREGISTRY_MYACCOUNT_LINK_BREADCRUMB,_in($PAGE_BREADCRUMB));
	//verify the navigation
	_assertVisible($PAGE_BREADCRUMB);	
	_assertVisible($GIFTREGISTRY_MYACCOUNT_LINK_BREADCRUMB, _in($PAGE_BREADCRUMB));
	_assertVisible($GIFTREGISTRY_MYACCOUNTLOGIN_HEADING);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124305","Verify the navigation related to 'Create an Account' link on Gift Registry login page left nav in application as an  Anonymous user");
$t.start();
try
{
	//click on link create an account
	_click($GIFTREGISTRY_CREATEACCOUNTNOW_BUTTON);
	//verify the navigation
	_assertVisible($GIFTREGISTRY_CREATEACCOUNT_HEADING);
	_assertVisible($PAGE_BREADCRUMB);	
	_assertVisible($GIFTREGISTRY_MYACCOUNT_LINK_BREADCRUMB, _in($PAGE_BREADCRUMB));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("125423","Verify the navigation related to 'Privacy Policy' link  on Gift Registry login page left nav in  application as an Anonymous user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click($GIFTREGISTRY_LINK);
	//click on privacy policy link
	_click($GIFTREGISTRY_PRIVACYPOLICY_LINK);
	//verify the navigation
	_assertVisible($GIFTREGISTRY_PRIVACYPOLICY_HEADING);
	_assertVisible($CONTENT_ASSET);
	_assertContainsText("/"+$Giftregistry_guest[4][1]+"/", $PAGE_BREADCRUMB);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148734","Verify the navigation related to 'Secure Shopping' link on Gift Registry login page left nav in  application as an  Anonymous user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click($GIFTREGISTRY_LINK);
	//click on Secure Shopping link	
	_click($GIFTREGISTRY_SECURESHOPPING_LINK);
	//verify the navigation
	_assertContainsText("/"+$Giftregistry_guest[5][1]+"/", $PAGE_BREADCRUMB);
	_assertVisible(_heading1($GIFTREGISTRY_SECURESHOPPING_HEADING));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148735","Verify the navigation related to 'Contact Us' link  on Gift Registry login page left nav in  application as an  Anonymous user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click($GIFTREGISTRY_LINK);
	//click on contact us link
	_click($GIFTREGISTRY_CONTACTUS_LINK);
	//verify the navigation
	_assertVisible($GIFTREGISTRY_CONTACTUS_HEADING);
	_assertVisible($PAGE_BREADCRUMB);	
	_assertVisible($GIFTREGISTRY_CONTACTUS_HEADING, _in($PAGE_BREADCRUMB));
	_assertVisible($GIFTREGISTRY_CONTACTUS_BUTTON);
		
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148736","Verify the UI of 'Forgot Password' overlay on Gift Registry login page in application as an Anonymous user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click($GIFTREGISTRY_LINK);
	//click on forgot password link
	_click($LOGIN_PASSWORD_RESET);
	//verify the overlay appearance
	_assertVisible($GIFTREGISTRY_PASSWORDRESET_DIALOG);
	_assertVisible($GIFTREGISTRY_RESETPASSWORD_HEADING);
	_assertVisible($GIFTREGISTRY_LOGIN_FORGETPASSWORD_TEXTBOX);
	_assertVisible($GIFTREGISTRY_FORGETPASSWORD_SEND_BUTTON);
	_assertVisible($GIFTREGISTRY_FORGETPASSWORD_CANCEL_BUTTON);
	//close the overlay
	_click($GIFTREGISTRY_FORGETPASSWORD_CANCEL_BUTTON);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124313","Verify the functionality �Send� button related to Forgot password overlay on Gift Registry login page returning customer section in application as an Anonymous user");
$t.start();
try
{
	_click($LOGIN_PASSWORD_RESET);
	//entering valid email address
	_setValue($GIFTREGISTRY_LOGIN_FORGETPASSWORD_TEXTBOX,$Giftregistry_guest[0][2]);	
	//click on send button
	_click($GIFTREGISTRY_FORGETPASSWORD_SEND_BUTTON);
	//verify the navigation
	_assertVisible($GIFTREGISTRY_RESETPASSWORD_HEADING);
	_assertVisible($HEADER_SALESFORCE_LINK);
	//close the dialog box
	_click($GIFTREGISTRY_FORGETPASSWORD_CANCEL_BUTTON);	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124314","Verify the navigation related to 'Create an account now' button Gift Registry login page create a wishlist section in application as an Anonymous user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click($GIFTREGISTRY_LINK);
	//click on link Create an account now button
	_click($GIFTREGISTRY_CREATEACCOUNTNOW_BUTTON);
	//verify the navigation
	_assertVisible($GIFTREGISTRY_CREATEACCOUNT_HEADING);
	_assertVisible($PAGE_BREADCRUMB);	
	_assertVisible($GIFTREGISTRY_MYACCOUNT_LINK_BREADCRUMB, _in($PAGE_BREADCRUMB));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124311/148738/124312","Verify validation related to the 'Email address'/'password' field's and login button on Gift Registry login page, Returning customers section in application as a Anonymous user");
$t.start();
try
{
	_click($GIFTREGISTRY_LINK);
	//validating the email address and password fields	
	for(var $i=0;$i<$GiftRegistyLogin_Validation.length;$i++)
		{
			_log($i);
			//valid
			if($i==5)
				{
				login();
				}	
			else
				{
				_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$GiftRegistyLogin_Validation[$i][1]);
				_setValue($LOGIN_PASSWORD_TEXTBOX,$GiftRegistyLogin_Validation[$i][2]);
				_click($LOGIN_BUTTON);
				}
			
			if (isMobile())
				{
				_wait(2000);
				}
			//blank
			if($i==0)
				{
				//username
				_assertEqual($GiftRegistyLogin_Validation[$i][5],_style($LOGIN_EMAIL_ADDRESS_TEXTBOX,"background-color"));
				_assertVisible(_span($GiftRegistyLogin_Validation[$i][3]));
				//password
				_assertVisible(_span($GiftRegistyLogin_Validation[$i][4]));
				_assertEqual($GiftRegistyLogin_Validation[$i][5],_style($LOGIN_PASSWORD_TEXTBOX,"background-color"));
				}
			//valid
			else if($i==5)
				{
				_assertVisible(_heading1("/"+$Giftregistry_guest[8][1]+"/"));
				}
			//invalid 
			else
				{
				_assertVisible(_div($GiftRegistyLogin_Validation[1][3]));
				}
		}
	//logout from application
	_click($GIFTREGISTRY_MY_ACCOUNT_LINK);
	_click($HEADER_LOGOUT_LINK);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148739/148740/124316","Verify validation related to the 'Last name'/'First name' field and find button functionality on Gift Registry login page, find someone's Gift Registry section in application as a Anonymous user");
$t.start();
try
{
//navigating to Gift Registry page
_click($GIFTREGISTRY_LINK);
//validating the fields last name,first name,email 
for(var $i=0;$i<$Validations_GiftRegistry.length;$i++)
  {
	_setValue($GIFTREGISTRY_LASTNAME_TEXTBOX, $Validations_GiftRegistry[$i][1]);
	_setValue($GIFTREGISTRY_FIRSTNAME_TEXTBOX, $Validations_GiftRegistry[$i][2]);
	
	//click on find button
	_click($GIFTREGISTRY_FIND_BUTTON);
	//Blank
	if($i==0)
		{
			//last name,first name fields should highlight
			_assertEqual($Validations_GiftRegistry[0][4],_style($GIFTREGISTRY_LASTNAME_TEXTBOX,"background-color"));
			_assertEqual($Validations_GiftRegistry[0][4],_style($GIFTREGISTRY_FIRSTNAME_TEXTBOX,"background-color"));
		}
	//invalid
	if($i==2 || $i==3 || $i==4 || $i==5 || $i==6)
		{
			_setSelected($GIFTREGISTRY_EVENTTYPE_DROPDOWN,$Validations_GiftRegistry[$i][3]);
			//user should be in same page
			_assertVisible(_heading1("/"+$Giftregistry_guest[8][1]+"/"));
			//verify the text message
			if(i==6)
				{
				_setValue($GIFTREGISTRY_EVENTTYPE_DROPDOWN, $Validations_GiftRegistry[$i][3]);
				_assertVisible($GIFTREGISTRY_PRODUCT_TABLES);
				_assertVisible($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
				}
			else
				{
				
				_assertVisible($GIFTREGISTRY_NOGIFTS_MESSAGE);
				}
		}
	//max characters
	if($i==1)
		{
		_assertEqual($Validations_GiftRegistry[1][4],_getText($GIFTREGISTRY_LASTNAME_TEXTBOX).length);
		_assertEqual($Validations_GiftRegistry[1][4],_getText($GIFTREGISTRY_FIRSTNAME_TEXTBOX).length);		
		}
	}	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("--","Verify advanced search functionality on Gift Registry login page, find someone's Gift Registry section in application as a Anonymous user");
$t.start();
try
{
 	//navigating to Gift Registry page
	_click($GIFTREGISTRY_LINK);	
	//click on advanced search button
	_click($GIFTREGISTRY_ADVANCESEARCH_LINK);
	//verify the UI
	//event name
	_assertVisible($GIFTREGISTRY_EVENTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_EVENTNAME_TEXTBOX);
	//event month
	_assertVisible($GIFTREGISTRY_EVENTMONTH_TEXT);
	_assertVisible($GIFTREGISTRY_EVENTMONTH_TEXTBOX);
	//event year
	_assertVisible($GIFTREGISTRY_EVENTYEAR_TEXT);
	_assertVisible($GIFTREGISTRY_EVENTYEAR_TEXTBOX);
	//event city
	_assertVisible($GIFTREGISTRY_EVENTCITY_TEXT);
	_assertVisible($GIFTREGISTRY_EVENTCITY_TEXTBOX);
	//event state
	_assertVisible($GIFTREGISTRY_EVENTCITY_TEXT);
	_assertVisible($GIFTREGISTRY_STATE_DROPDOWN);
	//event country
	_assertVisible($GIFTREGISTRY_EVENTCITY_TEXT);
	_assertVisible($GIFTREGISTRY_COUNTRY_DROPDOWN);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("289800", "Verify the UI of empty gift registry search results page in application as a guest user");
$t.start();
try
{
	_click($GIFTREGISTRY_USERACCOUNT_LINK);
	
	_click($GIFTREGISTRY_LINK);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
		}
	//heading
	_assertVisible($GIFTREGISTRY_FINDSOMEONES_REGISTRY_HEADING);
	//left nav
	_assertVisible($LEFTNAV_SECTION);
	var $Heading=HeadingCountInLeftNav();
	var $Links=LinkCountInLeftNav();
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
		{
			_assertVisible(_span($Generic_GiftRegistry[$i][1]));			
		}
	//checking links
	for(var $i=0;$i<$Links;$i++)
		{
			_assertVisible(_link($Generic_GiftRegistry[$i][2]));			
		}
	//left nav content asset(contact us)
	_assertVisible($LEFTNAV_ASSET);
	
	//last name
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXTBOX);
	//first name
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXTBOX);
	//email 
	_assertVisible($GIFTREGISTRY_EVENTTYPE_TEXT);
	_assertVisible($GIFTREGISTRY_EVENTTYPE_DROPDOWN);
	//find button
	_assertVisible($GIFTREGISTRY_FIND_BUTTON);
	//advanced search
	_assertVisible($GIFTREGISTRY_ADVANCE_LINK);
	//No gift registry heading
	_assertVisible($GIFTREGISTRY_NOGIFTS_MESSAGE);
	_assertVisible($GIFTREGISTRY_SEARCHRESULT_HEADING);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();