_include("../../../GenericLibrary/GlobalFunctions.js");

var $color=$Generic_GiftRegistry[0][6];
var $color1=$Generic_GiftRegistry[1][6];
//from util.GenericLibrary/BrowserSpecific.js
var $FirstName1=$FName;
var $LastName1=$LName;
var $UserEmail=$uId;


SiteURLs()
cleanup();
_setAccessorIgnoreCase(true); 
//Login
_click($HEADER_LOGIN_LINK);
login();

//Deleting addresses, which intern will delete wishlist and gift registry
deleteAddress();

var $t = _testcase("124318/124320", "Verify 'GIFT REGISTRY' link at the Header for Logged-in User and Verify the UI of  'Gift Registry page' in application as a  Registered user.");
$t.start();
try
{

	_click($GIFTREGISTRY_LINK);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
		}
	
	//heading
	_assertVisible(_heading1("/"+$Generic_GiftRegistry[1][0]+"/"));
	//left nav
	_assertVisible($GIFTREGISTRY_LEFTNAV_SECTION);
	var $Heading=HeadingCountInLeftNav();
	var $Links=LinkCountInLeftNav();
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
		{
			_assertVisible(_span($Generic_GiftRegistry[$i][1]));			
		}
	//checking links
	for(var $i=0;$i<$Links;$i++)
		{
			_assertVisible(_link($Generic_GiftRegistry[$i][2]));			
		}
	//left nav content asset(contact us)
	_assertVisible($GIFTREGISTRY_ACCOUNTLEFTNAV_ASSET);
	
	//last name
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXTBOX);
	//first name
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXTBOX);
	//Event Type
	_assertVisible($GIFTREGISTRY_EVENTTYPE_TEXT);
	_assertVisible($GIFTREGISTRY_EVENTTYPE_DROPDOWN);
	//find button
	_assertVisible($GIFTREGISTRY_FIND_BUTTON);
	//advanced search
	_assertVisible($GIFTREGISTRY_ADVANCE_LINK);
	//new registry button
	_assertVisible($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
	//text
	_assertVisible($GIFTREGISTRY_CREATEREGISTRY_PARAGRAPH_TEXT);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124319", "Verify the navigation related to 'Home' and 'My Account' links on  'Gift Registry  page bread crumb in application as a  Registered user.");
$t.start();
try
{
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
		}
	
	//verify the navigation to home page
	_click($HEADER_SALESFORCE_LINK);

	_assertVisible($HOMEPAGE_SLIDES);
	_assertVisible($HOMEPAGE_SLOTS);
	
	_click($HEADER_USERACCOUNT_LINK);
	_click($GIFTREGISTRY_LINK);
	//Navigation related to My account
	_assertVisible($GIFTREGISTRY_MY_ACCOUNT_LINK, _in($PAGE_BREADCRUMB));
	_click($GIFTREGISTRY_MY_ACCOUNT_LINK, _in($PAGE_BREADCRUMB));
	_assertVisible($GIFTREGISTRY_MY_ACCOUNT_HEADING);
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[7][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[7][0], _getText($PAGE_BREADCRUMB));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124334", "Verify the navigation related to left nav links on Create Gift Registry page in application as a Registered user. ");
$t.start();
try
{
	//left nav links	
	var $links=LinkCountInLeftNav();
	for(var $i=0;$i<$links;$i++)
	{
		//navigate to create gift registry page
		_click($HEADER_USERACCOUNT_LINK);
		_click($GIFTREGISTRY_LINK);
		//click on each links 
		_setAccessorIgnoreCase(true); 
		_click(_link($Generic_GiftRegistry[$i][2], _in($GIFTREGISTRY_LEFTNAV_SECTION)));
		//verify the navigation
		_assertVisible(_heading1("/"+$Generic_GiftRegistry[$i][4]+"/"));
		//For 2 links bread crumb-last element does'nt exist
		if($i==9 || $i==10)
			{
			_assertContainsText($Generic_GiftRegistry[$i][5], $PAGE_BREADCRUMB);			
			}
		else
			{
			_assertContainsText($Generic_GiftRegistry[$i][5], $PAGE_BREADCRUMB);
			_assertVisible($PAGE_BREADCRUMB);	
			_assertVisible(_link($Generic_GiftRegistry[$i][5], _in($PAGE_BREADCRUMB)));			
			}		
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148741", "Verify the functionality related to  down arrow present in left Nav on Create Gift Registry page in application a Registered user. ");
$t.start();
try
{
	//navigate to create gift registry page
	_click($HEADER_USERACCOUNT_LINK);
	_click($GIFTREGISTRY_LINK);
	//click on new registry
	_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
	var $j=1;
	//fetching the no.of headings in content assert
	var $headings=_collectAttributes("_span","/toggle/","sahiText",_in($GIFTREGISTRY_LEFTNAV_SECTION));
	for(var $i=0;$i<$headings.length;$i++)
		{
			//fetch links present in respective toggle's
			if($i==$headings.length-1)
				{
					$g=$i-1;
					var $links=_collectAttributes("_listItem","/(.*)/","sahiText",_in(_list($i,_in(_div("content-asset")))));
					_click(_span($headings[$i]));
					for(var $k=0;$k<$links.length;$k++)
						{
							if($links[$k]!="")
								{
									_assertNotVisible(_listItem($links[$k]));
								}						
						}
				}
			else
				{
					var $links=_collectAttributes("_listItem","/(.*)/","sahiText",_in(_list($i,_in(_div("content-asset")))));
					_click(_span($headings[$i]));
					for(var $k=0;$k<$links.length;$k++)
						{
							if($links[$k]!="")
								{
									_assertNotVisible(_listItem($links[$k]));
								}
						}
					$j++;
				}	
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124328", "Verify the navigation related to 'New Registry' button on Gift Registry page in application as a  Registered user.");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);
	_click($GIFTREGISTRY_LINK);
	//click on new registry
	_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);	
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
		}	
	//heading
	_assertVisible(_heading1($CREATEGIFTREGISTRY_HEADING));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124329", "Verify the UI elements of event information section on  Create Gift Registry page in application as a  Registered user.");
$t.start();
try
{
	//section heading
	_assertVisible($CREATEGIFTREGISTRY_EVENTINFORMATION_HEADING);
	//event type
	_assertVisible($CREATEGIFTREGISTRY_EVENTTYPE_TEXT);
	_assertVisible($CREATEGIFTREGISTRY_EVENTTYPE_DROPDOWN);
	//event name
	_assertVisible($CREATEGIFTREGISTRY_EVENTNAME_TEXT);
	_assertVisible($CREATEGIFTREGISTRY_EVENTNAME_TEXTBOX);
	//event date
	_assertVisible($CREATEGIFTREGISTRY_EVENTDATE_TEXT);
	_assertVisible($CREATEGIFTREGISTRY_EVENTDATE_DATEBOX);
	_assertVisible($CREATEGIFTREGISTRY_MONTHDATE_TEXT);
	//Country
	_assertVisible($CREATEGIFTREGISTRY_EVENTCOUNTRY_TEXT);
	_assertVisible($CREATEGIFTREGISTRY_EVENTCOUNTRY_TEXTBOX);
	_assertEqual($Generic_GiftRegistry[2][3], _getSelectedText($CREATEGIFTREGISTRY_EVENTCOUNTRY_TEXTBOX));
	//state
	_assertVisible($CREATEGIFTREGISTRY_EVENTSTATE_TEXT);
	_assertVisible($CREATEGIFTREGISTRY_EVENTSTATE_TEXTBOX);
	_assertEqual($Generic_GiftRegistry[2][3], _getSelectedText($CREATEGIFTREGISTRY_EVENTSTATE_TEXTBOX));
	//city
	_assertVisible($CREATEGIFTREGISTRY_EVENTCITY_TEXT);
	_assertVisible($CREATEGIFTREGISTRY_EVENTCITY_TEXTBOX);	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124330", "Verify the UI elements of  Participant Information section on  Create Gift Registry page   in application as a  Registered user.");
$t.start();
try
{
	_assertVisible($CREATEGIFTREGISTRY_PARTICIPANT_HEADING);
	//participant 1
	_assertVisible($CREATEGIFTREGISTRY_FIRST_PARTICIPANT_HEADING);
	//role
	_assertVisible($CREATEGIFTREGISTRY_ROLE_TEXT);
	_assertVisible($CREATEGIFTREGISTRY_ROLE_TEXTBOX);
	_assertEqual($Generic_GiftRegistry[2][3], _getSelectedText($CREATEGIFTREGISTRY_ROLE_TEXTBOX));
	//first name
	_assertVisible($CREATEGIFTREGISTRY_FP_FIRSTNAMETEXT);
	_assertVisible($CREATEGIFTREGISTRY_FP_FIRSTNAMETEXTBOX);
	_assertEqual($FirstName1, _getValue($CREATEGIFTREGISTRY_FP_FIRSTNAMETEXTBOX));
	//last name
	_assertVisible($CREATEGIFTREGISTRY_FP_LASTNAMETEXT);
	_assertVisible($CREATEGIFTREGISTRY_FP_LASTNAMETEXTBOX);
	_assertEqual($LastName1, _getValue($CREATEGIFTREGISTRY_FP_LASTNAMETEXTBOX));
	//email
	_assertVisible($CREATEGIFTREGISTRY_FP_EMAILTEXT);
	_assertVisible($CREATEGIFTREGISTRY_FP_EMAILTEXTBOX);
	_assertEqual($UserEmail, _getValue($CREATEGIFTREGISTRY_FP_EMAILTEXTBOX));
	
	//################# participant 2 ##############
	_assertVisible($CREATEGIFTREGISTRY_SECONDPARTICIPANT_HEADING);
	//role
	_assertVisible($CREATEGIFTREGISTRY_SECONDPARTICIPANT_ROLE_TEXT);
	_assertVisible($CREATEGIFTREGISTRY_SECONDPARTICIPANT_ROLE_DROPDOWN);
	_assertEqual($Generic_GiftRegistry[2][3], _getSelectedText($CREATEGIFTREGISTRY_SECONDPARTICIPANT_ROLE_DROPDOWN));
	//Fname
	_assertVisible($CREATEGIFTREGISTRY_SP_FIRSTNAMETEXT);
	_assertVisible($CREATEGIFTREGISTRY_SP_FIRSTNAMETEXTBOX);
	//Lname
	_assertVisible($CREATEGIFTREGISTRY_SP_LASTTNAMETEXT);
	_assertVisible($CREATEGIFTREGISTRY_SP_LASTTNAMETEXTBOX);
	//email
	_assertVisible($CREATEGIFTREGISTRY_SP_EMAILTEXT);
	_assertVisible($CREATEGIFTREGISTRY_SP_EMAILTEXTBOX);
	//Continue button
	_assertVisible($GIFTREGISTRY_PREPOST_CONTINUE_BUTTON);	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124331/124332/124333/148742/148743/148744/148745/148746/148747/148748/148749/148750/148751/148752/148753", "Verify validations related to Event Type drop down/Event name/ Event date/country/state/city/First participents role/fname/lname/email/second participants role/fname/lname/email on  Create Gift Registry page in application as a  Registered user.");
$t.start();
try
{
	for(var $i=0;$i<$CR_Validation.length;$i++)
		{
			if($i==0)
				{	
				_setValue($CREATEGIFTREGISTRY_EVENTTYPE_DROPDOWN,"");
				_setValue($CREATEGIFTREGISTRY_EVENTNAME_TEXTBOX,"");
				_setValue($CREATEGIFTREGISTRY_EVENTDATE_DATEBOX,"");
				//_setSelected($CREATEGIFTREGISTRY_EVENTCOUNTRY_TEXTBOX,"");
				//_setSelected($CREATEGIFTREGISTRY_EVENTSTATE_TEXTBOX,"");
				_setValue($CREATEGIFTREGISTRY_EVENTCITY_TEXTBOX,"");
				_setSelected($CREATEGIFTREGISTRY_ROLE_TEXTBOX,"");
				_setValue($CREATEGIFTREGISTRY_FP_FIRSTNAMETEXTBOX,"");
				_setValue($CREATEGIFTREGISTRY_FP_LASTNAMETEXTBOX,"");
				_setValue($CREATEGIFTREGISTRY_FP_EMAILTEXTBOX,"");
				_setSelected($CREATEGIFTREGISTRY_SECONDPARTICIPANT_ROLE_DROPDOWN,"");
				_setValue($CREATEGIFTREGISTRY_SP_FIRSTNAMETEXTBOX,"");
				_setValue($CREATEGIFTREGISTRY_SP_LASTTNAMETEXTBOX,"");
				_setValue($CREATEGIFTREGISTRY_SP_EMAILTEXTBOX,"");
				}
			else
				{
				_setValue($CREATEGIFTREGISTRY_EVENTTYPE_DROPDOWN,$CR_Validation[$i][1]);
				_setValue($CREATEGIFTREGISTRY_EVENTNAME_TEXTBOX,$CR_Validation[$i][2]);
				_setValue($CREATEGIFTREGISTRY_EVENTDATE_DATEBOX,$CR_Validation[$i][3]);
				//_setSelected($CREATEGIFTREGISTRY_EVENTCOUNTRY_TEXTBOX,$CR_Validation[$i][4]);
				//_setSelected($CREATEGIFTREGISTRY_EVENTSTATE_TEXTBOX,$CR_Validation[$i][5]);
				_setValue($CREATEGIFTREGISTRY_EVENTCITY_TEXTBOX,$CR_Validation[$i][6]);
				_setSelected($CREATEGIFTREGISTRY_ROLE_TEXTBOX,$CR_Validation[$i][7]);
				_setValue($CREATEGIFTREGISTRY_FP_FIRSTNAMETEXTBOX,$CR_Validation[$i][8]);
				_setValue($CREATEGIFTREGISTRY_FP_LASTNAMETEXTBOX,$CR_Validation[$i][9]);
				_setValue($CREATEGIFTREGISTRY_FP_EMAILTEXTBOX,$CR_Validation[$i][10]);
				_setSelected($CREATEGIFTREGISTRY_SECONDPARTICIPANT_ROLE_DROPDOWN,$CR_Validation[$i][11]);
				_setValue($CREATEGIFTREGISTRY_SP_FIRSTNAMETEXTBOX,$CR_Validation[$i][12]);
				_setValue($CREATEGIFTREGISTRY_SP_LASTTNAMETEXTBOX,$CR_Validation[$i][13]);
				_setValue($CREATEGIFTREGISTRY_SP_EMAILTEXTBOX,$CR_Validation[$i][14]);
				}
			if($i!=1)
			{
				_click($GIFTREGISTRY_PREPOST_CONTINUE_BUTTON);
			}
			
				if($i==6)
					{
						_setValue($CREATEGIFTREGISTRY_FP_FIRSTNAMETEXTBOX,$FirstName1);
						_setValue($CREATEGIFTREGISTRY_FP_LASTNAMETEXTBOX,$LastName1);
						_setValue($CREATEGIFTREGISTRY_FP_EMAILTEXTBOX,$UserEmail);
					}	
				//blank field
				if($i==0)
					{
						_assertEqual($color, _style($CREATEGIFTREGISTRY_EVENTTYPE_DROPDOWN, "background-color"));
						_assertEqual($color, _style($CREATEGIFTREGISTRY_EVENTNAME_TEXTBOX, "background-color"));
						_assertEqual($color, _style($CREATEGIFTREGISTRY_EVENTDATE_DATEBOX, "background-color"));
						//_assertEqual($color, _style($CREATEGIFTREGISTRY_EVENTCOUNTRY_TEXTBOX, "background-color"));
						//_assertEqual($color, _style($CREATEGIFTREGISTRY_EVENTSTATE_TEXTBOX, "background-color"));
						_assertEqual($color, _style($CREATEGIFTREGISTRY_EVENTCITY_TEXTBOX, "background-color"));
						_assertEqual($color, _style($CREATEGIFTREGISTRY_ROLE_TEXTBOX, "background-color"));
						_assertEqual($color, _style($CREATEGIFTREGISTRY_FP_FIRSTNAMETEXTBOX, "background-color"));
						_assertEqual($color, _style($CREATEGIFTREGISTRY_FP_LASTNAMETEXTBOX, "background-color"));
						_assertEqual($color, _style($CREATEGIFTREGISTRY_FP_EMAILTEXTBOX, "background-color"));
						//_assertEqual($color, _style($CREATEGIFTREGISTRY_SECONDPARTICIPANT_ROLE_DROPDOWN, "background-color"));
						//_assertEqual($color, _style($CREATEGIFTREGISTRY_SP_FIRSTNAMETEXTBOX, "background-color"));
						//_assertEqual($color, _style($CREATEGIFTREGISTRY_SP_LASTTNAMETEXTBOX, "background-color"));
						//_assertEqual($color, _style($CREATEGIFTREGISTRY_SP_EMAILTEXTBOX, "background-color"));					
					}
				//max
				else if($i==1)
					{
						_assertEqual($Generic_GiftRegistry[0][7],_getText($CREATEGIFTREGISTRY_EVENTTYPE_DROPDOWN).length);
						_assertEqual($Generic_GiftRegistry[0][7],_getText($CREATEGIFTREGISTRY_EVENTNAME_TEXTBOX).length);
						_assertEqual($Generic_GiftRegistry[1][7],_getText($CREATEGIFTREGISTRY_EVENTDATE_DATEBOX).length);
						_assertEqual($Generic_GiftRegistry[0][7],_getText($CREATEGIFTREGISTRY_EVENTCITY_TEXTBOX).length);
						_assertEqual($Generic_GiftRegistry[0][7],_getText($CREATEGIFTREGISTRY_FP_FIRSTNAMETEXTBOX).length);
						_assertEqual($Generic_GiftRegistry[0][7],_getText($CREATEGIFTREGISTRY_FP_LASTNAMETEXTBOX).length);
						_assertEqual($Generic_GiftRegistry[0][7],_getText($CREATEGIFTREGISTRY_FP_EMAILTEXTBOX).length);
						_assertEqual($Generic_GiftRegistry[0][7],_getText($CREATEGIFTREGISTRY_SP_FIRSTNAMETEXTBOX).length);
						_assertEqual($Generic_GiftRegistry[0][7],_getText($CREATEGIFTREGISTRY_SP_LASTTNAMETEXTBOX).length);
						_assertEqual($Generic_GiftRegistry[0][7],_getText($CREATEGIFTREGISTRY_SP_EMAILTEXTBOX).length);
					}
				//spl character
				else if($i==2)
					{
					//date
					_assertVisible(_span($CR_Validation[0][16],_in($CREATEGIFTREGISTRY_SP_EVENTDATE)));
					_assertEqual($color, _style($CREATEGIFTREGISTRY_EVENTDATE_DATEBOX, "background-color"));
					//email
					_assertVisible(_span($CR_Validation[0][17]));
					_assertEqual($color1,_style($CREATEGIFTREGISTRY_FP_EMAILTEXT,"color"));
					}
				//invalid date
				else if($i==3)
					{
					_assertVisible(_span($CR_Validation[0][16],_in($CREATEGIFTREGISTRY_SP_EVENTDATE)));
					_assertEqual($color, _style($CREATEGIFTREGISTRY_EVENTDATE_DATEBOX, "background-color"));
					}
				//invalid email format
				else if($i==4 ||$i==5)
					{
					_assertVisible(_span($CR_Validation[0][17]));
					_assertVisible(_span($CR_Validation[0][17], _in($CREATEGIFTREGISTRY_SP_EMAIL_DIV)));
					_assertEqual($color1,_style($CREATEGIFTREGISTRY_FP_EMAILTEXT,"color"));
					_assertEqual($color1,_style($CREATEGIFTREGISTRY_SP_EMAIL_LABEL),"color");
					}					
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("289801", "Verify the UI of empty gift registry search results page in application as a guest user");
$t.start();
try
{
	//click on user account link
	_click($GIFTREGISTRY_USERACCOUNT_LINK);
	//click on gift registry link
	_click($GIFTREGISTRY_LINK);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
		}
	//heading
	_assertVisible($GIFTREGISTRY_FINDSOMEONES_REGISTRY_HEADING);
	//left nav
	_assertVisible($LEFTNAV_SECTION);
	var $Heading=HeadingCountInLeftNav();
	var $Links=LinkCountInLeftNav();
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
		{
			_assertVisible(_span($Generic_GiftRegistry[$i][1]));			
		}
	//checking links
	for(var $i=0;$i<$Links;$i++)
		{
			_assertVisible(_link($Generic_GiftRegistry[$i][2]));			
		}
	//left nav content asset(contact us)
	_assertVisible($LEFTNAV_ASSET);
	
	//last name
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXTBOX);
	//first name
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXTBOX);
	//email 
	_assertVisible($GIFTREGISTRY_EVENTTYPE_TEXT);
	_assertVisible($GIFTREGISTRY_EVENTTYPE_DROPDOWN);
	//find button
	_assertVisible($GIFTREGISTRY_FIND_BUTTON);
	//advanced search
	_assertVisible($GIFTREGISTRY_ADVANCE_LINK);
	//No gift registry heading
	_assertVisible($GIFTREGISTRY_NOGIFTS_MESSAGE);
	_assertVisible($GIFTREGISTRY_SEARCHRESULT_HEADING);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("288129", "Verify validations related to Event Type drop down in Gift Registry login page of the application as a registered user");
$t.start();
try
{
	
//click on gift registry link
_click($GIFTREGISTRY_LINK);
//

	
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



cleanup();
