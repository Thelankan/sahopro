_include("../../../GenericLibrary/GlobalFunctions.js");

var $color=$Generic_GiftRegistry[0][6];
var $color1=$Generic_GiftRegistry[1][6];
//from util.GenericLibrary/BrowserSpecific.js
var $FirstName1=$FName;
var $LastName1=$LName;
var $UserEmail=$uId;

SiteURLs();
cleanup();
_setAccessorIgnoreCase(true); 
//Login
_click($HEADER_LOGIN_LINK);
login();

//Deleting addresses, which intern will delete wishlist and gift registry
deleteAddress();
_wait(4000,_isVisible($GIFTREGISTRY_CREATENEW_ADDRESS_LINK));
//adding addresses
for(var $i=0;$i<$Address.length;$i++)
{
	_click($GIFTREGISTRY_CREATENEW_ADDRESS_LINK);
	_wait(2000);
	if(_isVisible($GIFTREGISTRY_CREATEADDRESS_OVERLAY))
		 {
			 addAddress($Address,$i);	
			 _click($GIFTREGISTRY_OVERLAY_APPLYBUTTON);
			 _wait(4000,_isVisible($GIFTREGISTRY_CREATENEW_ADDRESS_LINK));
		 }
	else
		 {
			 _assert(false,"Add address Overlay is not displaying"); 
		 }
}

var $t = _testcase("124335", "Verify the UI  of Pre/Post Event Shipping page  in application as a  Registered user.");
$t.start();
try
{
	_click($GIFTREGISTRY_MY_ACCOUNT_LINK);
	_click($GIFTREGISTRY_LINK_MY_ACCOUNT_LANDINGPAGE);
	//click on new registry
	_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);	
	//creating the registry
	createRegistry($GiftRegistry, 0);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
		}
	//heading
	_assertVisible($GIFTREGISTRY_HEADING_PREPOST);
	//pre event shipping
	_assertVisible($GIFTREGISTRY_PREEVENTSHIPPING_HEADING);
	_assertVisible($GIFTREGISTRY_PREEVENTSHIPPING_TEXT_PARAGRAPH);
	//
	_assertVisible($GIFTREGISTRY_SELECTANADDRESS_HEADING_TEXT);
	//select address drop down
	_assertVisible($GIFTREGISTRY_ADDRESSDROPDOWN_TEXT);
	_assertVisible($GIFTREGISTRY_ADDRESSDROPDOWN);
	//first name
	_assertVisible($GIFTREGISTRY_PRE_EVENT_FIRSTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_PRE_EVENT_FIRSTNAME_TEXTBOX);
	//last name
	_assertVisible($GIFTREGISTRY_PRE_EVENT_LASTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_PRE_EVENT_LASTNAME_TEXTBOX);
	//address 1
	_assertVisible($GIFTREGISTRY_PRE_EVENT_ADDRESS1_TEXT);
	_assertVisible($GIFTREGISTRY_PRE_EVENT_ADDRESS1_TEXTBOX);
	//Street Address, P.O. Box text
	_assertVisible($GIFTREGISTRY_PRE_STREETADDRESS_TEXT);
	//tooltip
	_assertContainsText("APO/FPO", $GIFTREGISTRY_TOOLTIP_LINK);
	//address 2
	_assertVisible($GIFTREGISTRY_PRE_EVENT_ADDRESS2_TEXT);
	_assertVisible($GIFTREGISTRY_PRE_EVENT_ADDRESS2_TEXTBOX);
	_assertVisible($GIFTREGISTRY_PRE_ADDRESS2_APART_TEXT);
	//country
	_assertVisible($GIFTREGISTRY_PRE_EVENT_COUNTRY_TEXT);
	_assertVisible($GIFTREGISTRY_PRE_EVENT_COUNTRY_TEXTBOX);
	//state
	_assertVisible($GIFTREGISTRY_PRE_EVENT_STATE_TEXT);
	_assertVisible($GIFTREGISTRY_PRE_EVENT_STATE_TEXTBOX);
	//city
	_assertVisible($GIFTREGISTRY_PRE_EVENT_CITY_TEXT);
	_assertVisible($GIFTREGISTRY_PRE_EVENT_CITY_TEXTBOX);
	//zip code
	_assertVisible($GIFTREGISTRY_PRE_EVENT_ZIPCODE_TEXT);
	_assertVisible($GIFTREGISTRY_PRE_EVENT_ZIPCODE_TEXTBOX);
	//phone
	_assertVisible($GIFTREGISTRY_PRE_EVENT_PHONE_TEXT);
	_assertVisible($GIFTREGISTRY_PRE_EVENT_PHONE_TEXTBOX);
	_assertVisible(_div($Generic_GiftRegistry[6][8]));
	//tooltip
	_assertContainsText("Why is this required?",$GIFTREGISTRY_PRE_TOOLTIP);
	//Post event shipping
	//heading
	_assertVisible($GIFTREGISTRY_POSTEVENT_HEADING);
	_assertVisible($GIFTREGISTRY_POSTEVENT_PARAGRAPH);
	//use preevent shipping address button
	_assertVisible($GIFTREGISTRY_USEPRE_EVENT_SHIPPING_ADDRESS);
	//
	_assertVisible($GIFTREGISTRY_POSTEVENT_SECTION);
	//address drop down
	_assertVisible($GIFTREGISTRY_SELECTFROMSAVED_ADDRESSES_TEXT, _in($GIFTREGISTRY_POSTEVENT_SECTION));
	_assertVisible($GIFTREGISTRY_POSTEVENT_ADDRESSDROPDOWN);
	//fname
	_assertVisible($GIFTREGISTRY_POST_EVENT_FIRSTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_POST_EVENT_FIRSTNAME_TEXTBOX);
	//lname
	_assertVisible($GIFTREGISTRY_POST_EVENT_LASTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_POST_EVENT_LASTNAME_TEXTBOX);
	//address 1
	_assertVisible($GIFTREGISTRY_POST_EVENT_ADDRESS1_TEXT);
	_assertVisible($GIFTREGISTRY_POST_EVENT_ADDRESS1_TEXTBOX);
	_assertVisible($GIFTREGISTRY_POST_STREETADDRESS_TEXT);
	//tooltip
	_assertVisible($GIFTREGISTRY_POST_EVENT_TOOLTIP);
	//address 2
	_assertVisible($GIFTREGISTRY_POST_EVENT_ADDRESS2_TEXT);
	_assertVisible($GIFTREGISTRY_POST_EVENT_ADDRESS2_TEXTBOX);
	_assertVisible($GIFTREGISTRY_POST_ADDRESS2_APART_TEXT);
	//Country
	_assertVisible($GIFTREGISTRY_POST_EVENT_COUNTRY_TEXT);
	_assertVisible($GIFTREGISTRY_POST_EVENT_COUNTRY_TEXTBOX);
	//state
	_assertVisible($GIFTREGISTRY_POST_EVENT_STATE_TEXT);
	_assertVisible($GIFTREGISTRY_POST_EVENT_STATE_TEXTBOX);
	//city
	_assertVisible($GIFTREGISTRY_POST_EVENT_CITY_TEXT);
	_assertVisible($GIFTREGISTRY_POST_EVENT_CITY_TEXTBOX);
	//zipcode
	_assertVisible($GIFTREGISTRY_POST_EVENT_ZIPCODE_TEXT);
	_assertVisible($GIFTREGISTRY_POST_EVENT_ZIPCODE_TEXTBOX);
	//phone
	_assertVisible($GIFTREGISTRY_POST_EVENT_PHONE_TEXT);
	_assertVisible($GIFTREGISTRY_POST_EVENT_PHONE_TEXTBOX);
	_assertVisible(_div($Generic_GiftRegistry[6][8], _in($GIFTREGISTRY_POSTEVENT_SECTION)));
	//tool tip
	_assertVisible($GIFTREGISTRY_POST_EVENT_TOOLTIP);
	//previous
	_assertVisible($GIFTREGISTRY_PREVIOUS_LINK);
	//continue
	_assertVisible($GIFTREGISTRY_PREPOST_CONTINUE_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124336", "Verify the breadcrumb functionality of Pre/Post Event Shipping page  in application as a  Registered user.");
$t.start();
try
{
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
		}
	//Validating navigation on click of Home
	_click($HEADER_SALESFORCE_LINK);
	
	_click($GIFTREGISTRY_MY_ACCOUNT_LINK);
	_click($GIFTREGISTRY_LINK_MY_ACCOUNT_LANDINGPAGE);
	//click on new registry
	_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
	
	//creating the registry
	createRegistry($GiftRegistry, 0);
	
	//Navigation related to My account
	_assertVisible($GIFTREGISTRY_MY_ACCOUNT_LINK, _in($PAGE_BREADCRUMB));
	_click($GIFTREGISTRY_MY_ACCOUNT_LINK, _in($PAGE_BREADCRUMB));
	_assertVisible($GIFTREGISTRY_MY_ACCOUNT_HEADING);
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[7][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[7][0], _getText($PAGE_BREADCRUMB));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124338", "Verify the navigation related to left nav links on  Pre/Post Event Shipping page in application as a Registered user. ");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);
	_click($GIFTREGISTRY_LINK);
	//click on new registry
	_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
	//creating the registry
	createRegistry($GiftRegistry, 0);
	//left nav links	
	var $links=LinkCountInLeftNav();
	for(var $i=0;$i<$links;$i++)
	{
		//navigate to create gift registry page
		_click($HEADER_USERACCOUNT_LINK);
		_click($GIFTREGISTRY_LINK);
		//click on new registry
		_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
		//click on each links 
		_click(_link($Generic_GiftRegistry[$i][2]));
		//verify the navigation
		_assertVisible(_heading1("/"+$Generic_GiftRegistry[$i][4]+"/"));
		if($i==9 || $i==10)
			{
			_assertContainsText($Generic_GiftRegistry[$i][5], $PAGE_BREADCRUMB);
			}
		else
			{
			_assertContainsText($Generic_GiftRegistry[$i][5], $PAGE_BREADCRUMB);
			_assertVisible($PAGE_BREADCRUMB);
			_assertVisible(_link($Generic_GiftRegistry[$i][5], _in($PAGE_BREADCRUMB)));
			}
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148755", "Verify the functionality related to  down arrow present in left Nav on  Pre/Post Event Shipping page in application a Registered user. ");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);
	_click($GIFTREGISTRY_LINK);
	//click on new registry
	_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
	//creating the registry
	createRegistry($GiftRegistry, 0);
	
	var $heading=LeftNavHeadingText();
	var $links=LeftNavLinksText();
	
	for(var $i=0;$i<$links.length;$i++)
	{
		_assertVisible(_listItem($links[$i]));
	}
		
	for(var $i=0;$i<$heading.length;$i++)
	{
	_assertVisible(_span($heading[$i], _in($CONTENT_ASSET)));
	_click(_span($heading[$i]));
	}
	
	for(var $i=0;$i<$links.length;$i++)
	{
		_assertNotVisible(_listItem($links[$i]));
	}
	
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124340", "Verify the navigation related to the 'Previous' link on  Pre/Post Event Shipping page in application as a  Registered user.");
$t.start();
try
{
	_assertVisible($GIFTREGISTRY_PREVIOUS_LINK);
	_click($GIFTREGISTRY_PREVIOUS_LINK);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
		}	
	//Navigate to event shipping info page
	_click($GIFTREGISTRY_PREPOST_CONTINUE_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124337/124341/124342/148756/148757/148757/148757/148758/148759/148760/148761/148762/148763/148765/148766/148767/148768/148769/148770/148771", "Verify validation related to Firstname/Lastname/Address1/Address2/Country/States/City/Phone fields  present in Pre/Post Event Shipping  section on  Pre/Post Event Shipping page in application as a  Registered user.");
$t.start();
try
{
	
	_click($HEADER_USERACCOUNT_LINK);
	_click($GIFTREGISTRY_LINK);
	//click on new registry
	_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
	//creating the registry
	createRegistry($GiftRegistry, 0);
	//country validations
	var $Length=_getText($GIFTREGISTRY_PRE_EVENT_COUNTRY_TEXTBOX).length;
	for(var $j=0;$j<$Length-1;$j++)
		{
		_log($j);
		_setSelected($GIFTREGISTRY_PRE_EVENT_COUNTRY_TEXTBOX,$EventShippingInfo[$j][11]);
		_setSelected($GIFTREGISTRY_POST_EVENT_COUNTRY_TEXTBOX,$EventShippingInfo[$j][11]);
		_assertEqual($EventShippingInfo[$j][12], _getText($GIFTREGISTRY_PRE_EVENT_STATE_TEXTBOX).toString());
		_assertEqual($EventShippingInfo[$j][12], _getText($GIFTREGISTRY_POST_EVENT_STATE_TEXTBOX).toString());
		}
	
	for(var $i=0;$i<$EventShippingInfo.length;$i++)
		{
		if($i!=3 || $i!=4)
			{
		_log($i);
			_setValue($GIFTREGISTRY_PRE_EVENT_FIRSTNAME_TEXTBOX,$EventShippingInfo[$i][1]);
			_setValue($GIFTREGISTRY_PRE_EVENT_LASTNAME_TEXTBOX,$EventShippingInfo[$i][2]);
			_setValue($GIFTREGISTRY_PRE_EVENT_ADDRESS1_TEXTBOX,$EventShippingInfo[$i][3]);
			_setValue($GIFTREGISTRY_PRE_EVENT_ADDRESS2_TEXTBOX,$EventShippingInfo[$i][4]);
			_setSelected($GIFTREGISTRY_PRE_EVENT_COUNTRY_TEXTBOX,$EventShippingInfo[$i][5]);
			_setSelected($GIFTREGISTRY_PRE_EVENT_STATE_TEXTBOX,$EventShippingInfo[$i][6]);
			_setValue($GIFTREGISTRY_PRE_EVENT_CITY_TEXTBOX,$EventShippingInfo[$i][7]);
			_setValue($GIFTREGISTRY_PRE_EVENT_ZIPCODE_TEXTBOX,$EventShippingInfo[$i][8]);
			_setValue($GIFTREGISTRY_PRE_EVENT_PHONE_TEXTBOX,$EventShippingInfo[$i][9]);
			_setValue($GIFTREGISTRY_POST_EVENT_FIRSTNAME_TEXTBOX,$EventShippingInfo[$i][1]);
			_setValue($GIFTREGISTRY_POST_EVENT_LASTNAME_TEXTBOX,$EventShippingInfo[$i][2]);
			_setValue($GIFTREGISTRY_POST_EVENT_ADDRESS1_TEXTBOX,$EventShippingInfo[$i][3]);
			_setValue($GIFTREGISTRY_POST_EVENT_ADDRESS2_TEXTBOX,$EventShippingInfo[$i][4]);
			_setSelected($GIFTREGISTRY_POST_EVENT_COUNTRY_TEXTBOX,$EventShippingInfo[$i][5]);
			_setSelected($GIFTREGISTRY_POST_EVENT_STATE_TEXTBOX,$EventShippingInfo[$i][6]);
			_setValue($GIFTREGISTRY_POST_EVENT_CITY_TEXTBOX,$EventShippingInfo[$i][7]);
			_setValue($GIFTREGISTRY_POST_EVENT_ZIPCODE_TEXTBOX,$EventShippingInfo[$i][8]);
			_setValue($GIFTREGISTRY_POST_EVENT_PHONE_TEXTBOX,$EventShippingInfo[$i][9]);
			if($i!=1)
				{
				_click($GIFTREGISTRY_PREPOST_CONTINUE_BUTTON);
				}
			//blank field validation
			if($i==0)
				{
					_assertEqual($color, _style($GIFTREGISTRY_PRE_EVENT_FIRSTNAME_TEXTBOX, "background-color"));
					_assertEqual($color, _style($GIFTREGISTRY_PRE_EVENT_LASTNAME_TEXTBOX, "background-color"));
					_assertEqual($color, _style($GIFTREGISTRY_PRE_EVENT_ADDRESS1_TEXTBOX, "background-color"));
					_assertEqual($color, _style($GIFTREGISTRY_PRE_EVENT_STATE_TEXTBOX, "background-color"));
					_assertEqual($color, _style($GIFTREGISTRY_PRE_EVENT_CITY_TEXTBOX, "background-color"));
					_assertEqual($color, _style($GIFTREGISTRY_PRE_EVENT_ZIPCODE_TEXTBOX, "background-color"));
					_assertEqual($color, _style($GIFTREGISTRY_PRE_EVENT_PHONE_TEXTBOX, "background-color"));
					_assertEqual($color, _style($GIFTREGISTRY_POST_EVENT_FIRSTNAME_TEXTBOX, "background-color"));
					_assertEqual($color, _style($GIFTREGISTRY_POST_EVENT_LASTNAME_TEXTBOX, "background-color"));
					_assertEqual($color, _style($GIFTREGISTRY_POST_EVENT_ADDRESS1_TEXTBOX, "background-color"));
					_assertEqual($color, _style($GIFTREGISTRY_POST_EVENT_STATE_TEXTBOX, "background-color"));
					_assertEqual($color, _style($GIFTREGISTRY_POST_EVENT_CITY_TEXTBOX, "background-color"));
					_assertEqual($color, _style($GIFTREGISTRY_POST_EVENT_ZIPCODE_TEXTBOX, "background-color"));
					_assertEqual($color, _style($GIFTREGISTRY_POST_EVENT_PHONE_TEXTBOX, "background-color"));	
				}
			//Max characters
			else if($i==1)
				{
					_assertEqual($EventShippingInfo[0][13],_getText($GIFTREGISTRY_PRE_EVENT_FIRSTNAME_TEXTBOX).length);
					_assertEqual($EventShippingInfo[0][13],_getText($GIFTREGISTRY_PRE_EVENT_LASTNAME_TEXTBOX).length);
					_assertEqual($EventShippingInfo[0][13],_getText($GIFTREGISTRY_PRE_EVENT_ADDRESS1_TEXTBOX).length);
					_assertEqual($EventShippingInfo[0][13],_getText($GIFTREGISTRY_PRE_EVENT_ADDRESS2_TEXTBOX).length);
					_assertEqual($EventShippingInfo[1][13],_getText($GIFTREGISTRY_PRE_EVENT_CITY_TEXTBOX).length);
					_assertEqual($EventShippingInfo[3][13],_getText($GIFTREGISTRY_PRE_EVENT_ZIPCODE_TEXTBOX).length);
					_assertEqual($EventShippingInfo[2][13],_getText($GIFTREGISTRY_PRE_EVENT_PHONE_TEXTBOX).length);
					_assertEqual($EventShippingInfo[0][13],_getText($GIFTREGISTRY_POST_EVENT_FIRSTNAME_TEXTBOX).length);
					_assertEqual($EventShippingInfo[0][13],_getText($GIFTREGISTRY_POST_EVENT_LASTNAME_TEXTBOX).length);
					_assertEqual($EventShippingInfo[0][13],_getText($GIFTREGISTRY_POST_EVENT_ADDRESS1_TEXTBOX).length);
					_assertEqual($EventShippingInfo[0][13],_getText($GIFTREGISTRY_POST_EVENT_ADDRESS2_TEXTBOX).length);
					_assertEqual($EventShippingInfo[1][13],_getText($GIFTREGISTRY_POST_EVENT_CITY_TEXTBOX).length);
					_assertEqual($EventShippingInfo[3][13],_getText($GIFTREGISTRY_POST_EVENT_ZIPCODE_TEXTBOX).length);
					_assertEqual($EventShippingInfo[2][13],_getText($GIFTREGISTRY_POST_EVENT_PHONE_TEXTBOX).length);				
				}
			//spl characters in zip code
			else if($i==4)
				{
				_assertEqual($EventShippingInfo[5][10],_style($GIFTREGISTRY_PRE_EVENT_ZIPCODE_TEXT,"color"));
				}
			//valid
			else if($i==5)
				{
				_assertVisible($GIFTREGISTRY_EVENTINFORMATION_HEADING);
				_assertVisible($GIFTREGISTRY_EVENTINFO_SHIPPINGADDRESSES_HEADING);
				}
			//spl characters and invalid phone number
			else 
				{
				_assertVisible(_span($EventShippingInfo[$i][10]));
				_assertEqual($color, _style($GIFTREGISTRY_PRE_EVENT_PHONE_TEXTBOX, "background-color"));
				_assertVisible(_span($EventShippingInfo[$i][10], _in($GIFTREGISTRY_POSTEVENT_SECTION)));
				_assertEqual($color, _style($GIFTREGISTRY_POST_EVENT_PHONE_TEXTBOX, "background-color"));
				}
			}
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124344", "Verify the UI  of Event information page  in application as a  Registered user.");
$t.start();
try
{	
	/*if (_isVisible($GIFTREGISTRY_PREVIOUS_LINK))
		{
	_click($GIFTREGISTRY_PREVIOUS_LINK);
		}*/
	Pre_PostEventInfo($GiftRegistry, 4);	
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
		}
	//heading
	_assertVisible(_heading2($Generic_GiftRegistry[0][9]));
	//Static text
	_assertVisible(_paragraph($Generic_GiftRegistry[1][9]));
	//section header
	_assertVisible(_heading3($Generic_GiftRegistry[2][9]));
	//Event info
	_assertVisible(_dList("Registrant(s):"+" "+$FirstName1+" "+$LastName1+" , "+$GiftRegistry[0][12]+" "+$GiftRegistry[0][13]+" "+"Event Type:"+" "+$GiftRegistry[0][1]+" "+"Event Name:"+" "+$GiftRegistry[0][2]+" "+"Event Date:"+" "+$GiftRegistry[0][3]+" "+"Event Location:"+" "+$GiftRegistry[0][6]+", "+$GiftRegistry[1][5]+" "+$GiftRegistry[1][4]));	
	//section header
	_assertVisible(_heading3($Generic_GiftRegistry[3][9]));
	//Participants info
	_assertVisible(_dList("Registrant: "+$FirstName1+" "+$LastName1+" "+$UserEmail+" Co-Registrant: "+$GiftRegistry[0][12]+" "+$GiftRegistry[0][13]+" "+$GiftRegistry[0][14]));
	//Shipping info
	//section header
	_assertVisible(_heading3($Generic_GiftRegistry[4][9]));
	//Pre/Post Event shipping info
	_assertVisible(_dList("Pre-event Shipping: "+$GiftRegistry[4][1]+" "+$GiftRegistry[4][2]+" "+$GiftRegistry[4][3]+" "+$GiftRegistry[4][4]+" "+$GiftRegistry[4][7]+", "+$GiftRegistry[5][6]+" "+$GiftRegistry[4][8]+" Post-event Shipping: "+$GiftRegistry[4][1]+" "+$GiftRegistry[4][2]+" "+$GiftRegistry[4][3]+" "+$GiftRegistry[4][4]+" "+$GiftRegistry[4][7]+", "+$GiftRegistry[5][6]+" "+$GiftRegistry[4][8]));
	//Parevious button
	_assertVisible($GIFTREGISTRY_PREVIOUS_LINK);
	//Submit button
	_assertVisible($GIFTREGISTRY_EVENTINFORMATION_CONTINUE_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124343", "Verify the breadcrumb Navigation of Event information page  in application as a  Registered user.");
$t.start();
try
{
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
		}
	_click($HEADER_USERACCOUNT_LINK);
	_click($GIFTREGISTRY_LINK);
	//click on new registry
	_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
	//creating the registry
	createRegistry($GiftRegistry, 0);
	//Pre/Post Event shipping info
	Pre_PostEventInfo($GiftRegistry, 4);
	//Navigation related to My account
	_assertVisible($GIFTREGISTRY_MY_ACCOUNT_LINK, _in($PAGE_BREADCRUMB));
	_click($GIFTREGISTRY_MY_ACCOUNT_LINK, _in($PAGE_BREADCRUMB));
	_assertVisible($GIFTREGISTRY_MY_ACCOUNT_HEADING);
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[7][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[7][0], _getText($PAGE_BREADCRUMB));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124345", "Verify the navigation related to the 'Previous' link on Event information page  in application as a  Registered user.");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);
	_click($GIFTREGISTRY_LINK);
	//click on new registry
	_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
	//creating the registry
	createRegistry($GiftRegistry, 0);
	//Pre/Post Event shipping info
	Pre_PostEventInfo($GiftRegistry, 4);
	//Parevious button
	_assertVisible($GIFTREGISTRY_PREVIOUS_LINK);
	_click($GIFTREGISTRY_PREVIOUS_LINK);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	if(_isIE9())
		{
		_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
		}
	//heading
	_assertVisible($GIFTREGISTRY_HEADING_PREPOST);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//Navigate to Event shipping info Page
_click($HEADER_USERACCOUNT_LINK);
_click($GIFTREGISTRY_LINK);
//click on new registry
_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
//creating the registry
createRegistry($GiftRegistry, 0);
//bread crumb
_assertVisible($PAGE_BREADCRUMB);
if(_isIE9())
{
_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
}
else
{
_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
}

var $t = _testcase("148754/148764", "Verify functionality related to Select from Saved Addresses drop down present in Pre-Event Shipping and post-Event Shipping section on  Pre/Post Event Shipping page in application as a  Registered user.");
$t.start();
try
{
	_setSelected($GIFTREGISTRY_ADDRESSDROPDOWN,$Address[0][11]);
	//Validating Prepopulated data
	_assertEqual($Address[0][2],_getText($GIFTREGISTRY_PRE_EVENT_FIRSTNAME_TEXTBOX));
	_assertEqual($Address[0][3],_getText($GIFTREGISTRY_PRE_EVENT_LASTNAME_TEXTBOX));
	_assertEqual($Address[0][4],_getText($GIFTREGISTRY_PRE_EVENT_ADDRESS1_TEXTBOX));
	_assertEqual($Address[0][5],_getText($GIFTREGISTRY_PRE_EVENT_ADDRESS2_TEXTBOX));
	_assertEqual($Address[0][6],_getSelectedText($GIFTREGISTRY_PRE_EVENT_COUNTRY_TEXTBOX));
	_assertEqual($Address[0][7],_getSelectedText($GIFTREGISTRY_PRE_EVENT_STATE_TEXTBOX));
	_assertEqual($Address[0][8],_getText($GIFTREGISTRY_PRE_EVENT_CITY_TEXTBOX));
	_assertEqual($Address[0][9],_getText($GIFTREGISTRY_PRE_EVENT_ZIPCODE_TEXTBOX));
	_assertEqual($Address[0][10],_getText($GIFTREGISTRY_PRE_EVENT_PHONE_TEXTBOX));
	
	_setSelected($GIFTREGISTRY_POSTEVENT_ADDRESSDROPDOWN,$Address[1][11]);
	//Validating Prepopulated data
	_assertEqual($Address[1][2],_getText($GIFTREGISTRY_POST_EVENT_FIRSTNAME_TEXTBOX));
	_assertEqual($Address[1][3],_getText($GIFTREGISTRY_POST_EVENT_LASTNAME_TEXTBOX));
	_assertEqual($Address[1][4],_getText($GIFTREGISTRY_POST_EVENT_ADDRESS1_TEXTBOX));
	_assertEqual($Address[1][5],_getText($GIFTREGISTRY_POST_EVENT_ADDRESS2_TEXTBOX));
	_assertEqual($Address[1][6],_getSelectedText($GIFTREGISTRY_POST_EVENT_COUNTRY_TEXTBOX));
	_assertEqual($Address[1][7],_getSelectedText($GIFTREGISTRY_POST_EVENT_STATE_TEXTBOX));
	_assertEqual($Address[1][8],_getText($GIFTREGISTRY_POST_EVENT_CITY_TEXTBOX));
	_assertEqual($Address[1][9],_getText($GIFTREGISTRY_POST_EVENT_ZIPCODE_TEXTBOX));
	_assertEqual($Address[1][10],_getText($GIFTREGISTRY_POST_EVENT_PHONE_TEXTBOX));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//Navigate to Event shipping info Page
_click($HEADER_USERACCOUNT_LINK);
_click($GIFTREGISTRY_LINK);
//click on new registry
_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
//creating the registry
createRegistry($GiftRegistry, 0);
//bread crumb
_assertVisible($PAGE_BREADCRUMB);
if(_isIE9())
{
_assertEqual($Generic_GiftRegistry[0][0].replace(/\s/g,''), _getText($PAGE_BREADCRUMB).replace(/\s/g,'') ); 
}
else
{
_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
}

var $t = _testcase("124339", "Verify the  functionality related to 'Use this address for Post Event Shipping' check box   Pre-Event Shipping  section on  Pre/Post Event Shipping page in application as a  Registered user.");
$t.start();
try
{
	_setSelected($GIFTREGISTRY_ADDRESSDROPDOWN,$Address[0][11]);
	//Validating Prepopulated data
	_assertEqual($Address[0][2],_getText($GIFTREGISTRY_PRE_EVENT_FIRSTNAME_TEXTBOX));
	_assertEqual($Address[0][3],_getText($GIFTREGISTRY_PRE_EVENT_LASTNAME_TEXTBOX));
	_assertEqual($Address[0][4],_getText($GIFTREGISTRY_PRE_EVENT_ADDRESS1_TEXTBOX));
	_assertEqual($Address[0][5],_getText($GIFTREGISTRY_PRE_EVENT_ADDRESS2_TEXTBOX));
	_assertEqual($Address[0][6],_getSelectedText($GIFTREGISTRY_PRE_EVENT_COUNTRY_TEXTBOX));
	_assertEqual($Address[0][7],_getSelectedText($GIFTREGISTRY_PRE_EVENT_STATE_TEXTBOX));
	_assertEqual($Address[0][8],_getText($GIFTREGISTRY_PRE_EVENT_CITY_TEXTBOX));
	_assertEqual($Address[0][9],_getText($GIFTREGISTRY_PRE_EVENT_ZIPCODE_TEXTBOX));
	_assertEqual($Address[0][10],_getText($GIFTREGISTRY_PRE_EVENT_PHONE_TEXTBOX));
	//selecting use this address checkbox
	_click($GIFTREGISTRY_USEPRE_EVENT_SHIPPING_ADDRESS);
	//Validating Prepopulated data
	_assertEqual(_getText($GIFTREGISTRY_PRE_EVENT_FIRSTNAME_TEXTBOX),_getText($GIFTREGISTRY_POST_EVENT_FIRSTNAME_TEXTBOX));
	_assertEqual(_getText($GIFTREGISTRY_PRE_EVENT_LASTNAME_TEXTBOX),_getText($GIFTREGISTRY_POST_EVENT_LASTNAME_TEXTBOX));
	_assertEqual(_getText($GIFTREGISTRY_PRE_EVENT_ADDRESS1_TEXTBOX),_getText($GIFTREGISTRY_POST_EVENT_ADDRESS1_TEXTBOX));
	_assertEqual(_getText($GIFTREGISTRY_PRE_EVENT_ADDRESS2_TEXTBOX),_getText($GIFTREGISTRY_POST_EVENT_ADDRESS2_TEXTBOX));
	_assertEqual(_getSelectedText($GIFTREGISTRY_PRE_EVENT_COUNTRY_TEXTBOX),_getSelectedText($GIFTREGISTRY_POST_EVENT_COUNTRY_TEXTBOX));
	_assertEqual(_getSelectedText($GIFTREGISTRY_PRE_EVENT_STATE_TEXTBOX),_getSelectedText($GIFTREGISTRY_POST_EVENT_STATE_TEXTBOX));
	_assertEqual(_getText($GIFTREGISTRY_PRE_EVENT_CITY_TEXTBOX),_getText($GIFTREGISTRY_POST_EVENT_CITY_TEXTBOX));
	_assertEqual(_getText($GIFTREGISTRY_POST_EVENT_ZIPCODE_TEXTBOX),_getText($GIFTREGISTRY_POST_EVENT_ZIPCODE_TEXTBOX));
	_assertEqual(_getText($GIFTREGISTRY_PRE_EVENT_PHONE_TEXTBOX),_getText($GIFTREGISTRY_POST_EVENT_PHONE_TEXTBOX));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("148773", "Verify the functionality related to  down arrow present in left Nav on Event information page  in application as a  Registered user.");
$t.start();
try
{
		
	var $heading=LeftNavHeadingText();
	var $links=LeftNavLinksText();
	
	for(var $i=0;$i<$links.length;$i++)
	{
		_assertVisible(_listItem($links[$i]));
	}
		
	for(var $i=0;$i<$heading.length;$i++)
	{
	_assertVisible(_span($heading[$i], _in($CONTENT_ASSET)));
	_click(_span($heading[$i]));
	}
	
	for(var $i=0;$i<$links.length;$i++)
	{
		_assertNotVisible(_listItem($links[$i]));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("148772", "Verify the navigation related to left nav links on Event information page  in application as a  Registered user.");
$t.start();
try
{
	//left nav links	
	var $links=LinkCountInLeftNav()
	for(var $i=0;$i<$links;$i++)
	{
		//navigate to create gift registry page
		_click($GIFTREGISTRY_MY_ACCOUNT_LINK);
		_click($GIFTREGISTRY_LINK_MY_ACCOUNT_LANDINGPAGE);
		//click on new registry
		_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
		//creating the registry
		createRegistry($GiftRegistry, 0);
		//Pre/Post Event shipping info
		Pre_PostEventInfo($GiftRegistry, 4);
		//click on each links in event info page
		_click(_link($Generic_GiftRegistry[$i][2]))
		//verify the navigation
		_assertVisible(_heading1("/"+$Generic_GiftRegistry[$i][4]+"/"));
		_assertContainsText($Generic_GiftRegistry[$i][5], $PAGE_BREADCRUMB);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124346", "Verify the navigation related to the 'Submit' link on Event information page  in application as a  Registered user.");
$t.start();
try
{
	//navigate to create gift registry page
	_click($HEADER_USERACCOUNT_LINK);
	_click($GIFTREGISTRY_LINK);
	//click on new registry
	_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
	//creating the registry
	createRegistry($GiftRegistry, 0);
	//Pre/Post Event shipping info
	Pre_PostEventInfo($GiftRegistry, 4);
	//Click on submit
	//Submit button
	_click($GIFTREGISTRY_EVENTINFORMATION_CONTINUE_BUTTON);
	//Validating Navigation
	_assertVisible($GIFTREGISTRY_NOGIFTREGISTRY_HEADING);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


cleanup();