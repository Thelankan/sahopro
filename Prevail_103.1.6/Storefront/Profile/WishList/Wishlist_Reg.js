_include("../../../GenericLibrary/GlobalFunctions.js");;
_resource("WishList.xls");

var $Wishlist_Login=_readExcelFile("WishList.xls","Wishlist_Login");
var $Wishlist_Reg=_readExcelFile("WishList.xls","Wishlist_Reg");
var $Validations=_readExcelFile("WishList.xls","Validations");
var $addr_data=_readExcelFile("WishList.xls","Address");
var $sendtofriend=_readExcelFile("WishList.xls","Sendtofriend_Validation");
var $qunatityvalidations=_readExcelFile("WishList.xls","QuantityValidations");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//adding items to wish list for particular account
additemsToWishListAccount($Wishlist_Reg[0][0]);

//login to the application
_click($HEADER_LOGIN_LINK);
login();

//navigate to wish list page
_click($WISHLIST_LINK);
if (_isVisible($WISHLIST_SHIPPINGADDRESS_DROPDOWN))
	{
	_setSelected($WISHLIST_SHIPPINGADDRESS_DROPDOWN,0);
	}
//deleting gift registry
Delete_GiftRegistry();

//deleting if any addresses are present
//navigate address page
deleteAddress();

var $t=_testcase("289648","Verify the display of wish list shipping address drop down in wish list page as a registered user having no saved address");
$t.start();
try
{

//navigate to wish list page
_click($WISHLIST_LINK);
//Address Dropdown should not present if there are no addresses
_assertNotVisible($WISHLIST_SHIPPINGADDRESS_DROPDOWN);

}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $address=new Array();
for(var $i=0;$i<$addr_data.length;$i++)
	{
		_wait(2000);
		//create new address
		_click($ADDRESSES_CREATENEW_ADDRESS_LINK);
		//adding address
		addAddress($addr_data,$i);
		_click($ADDRESSES_OVERLAY_APPLYBUTTON);
		//$address[$i]="("+$addr_data[$i][1]+")"+" "+$addr_data[$i][4]+", "+$addr_data[$i][8]+", "+$addr_data[$i][11]+", "+$addr_data[$i][9];
		$address[$i]="("+$addr_data[$i][1]+")"+" "+$addr_data[$i][4]+" "+$addr_data[$i][8]+" "+$addr_data[$i][11]+" "+$addr_data[$i][9];
	}
	
//verifying the presence of addresses
var $boolean=_isVisible($ADDRESSES_ADDRESSES_LIST);

var $t=_testcase("148713/148714/289204","Verify the UI of empty Wish List search' page in application as an registered user");
$t.start();
try
{	
	//navigate to wish list page
	_click($WISHLIST_LINK);
	//Clear wish list
	while(_isVisible($WISHLIST_DELETE_LINK))
		{
		_click($WISHLIST_DELETE_LINK);
		}
	//verify the UI of empty Wish List search
	emptyWishlistUI();
	_assertVisible(_heading2($Wishlist_Login[12][0]));
	_assertVisible(_link($Wishlist_Login[13][0]));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124545/289203/289689","Verify the UI of non empty Wish List search' page in application as an registered user./Verify the functionality of 'Click here to start adding items.' link when no items are added to the wish list./Verify the UI of wish list page of the application as a registered user having products in wish list");
$t.start();
try
{
	//add products to wish list by clicking on link Click here to start adding items.
	_click(_link($Wishlist_Login[13][0]));
	
	//verify the navigation to home page
	_assertVisible($HOMEPAGE_SLIDES);
	_assertVisible($HOMEPAGE_SLOTS);
	
	//navigate to sub category
	_click(_link($Wishlist_Reg[0][0]));
	var $pName=_getText($SHOPNAV_PLP_NAMELINK);
	//click on name link
	_click($SHOPNAV_PLP_NAMELINK);
	//select swatches
	selectSwatch();	
	//click on link add to wish list
	_click($WISHLIST_ADDTOWISHLIST_LINK);
	//verify the UI
	emptyWishlistUI();
	//with products in wishlist
	_assertVisible($WISHLIST_LASTNAME_TEXTBOX);
	_assertEqual("", _getValue($WISHLIST_LASTNAME_TEXTBOX));
	_assertVisible($WISHLIST_FIRSTNAME_TEXTBOX);
	_assertEqual("", _getValue($WISHLIST_FIRSTNAME_TEXTBOX));
	_assertVisible($WISHLIST_EMAIL_TEXTBOX);
	_assertEqual("", _getValue($WISHLIST_EMAIL_TEXTBOX));
	//textbox labels
	_assertVisible($WISHLIST_LASTNAME_LABEL);
	_assertVisible($WISHLIST_FIRSTNAME_LABEL);
	_assertVisible($WISHLIST_EMAIL_LABEL);
	//product ID
	_assertVisible($WISHLIST_SKU_SECTION);
	_assertVisible($WISHLIST_PRODUCTID_SKU);
	//product Price
	_assertVisible($WISHLIST_PRICE);
	//image
	_assertVisible($WISHLIST_PRODUCTIMAGE);
	//name 
	_assertVisible($WISHLIST_PRODUCTNAME);
	_assertEqual($pName, _getText($WISHLIST_PRODUCTNAME));
	//product list item
	_assertVisible($WISHLIST_PRODUCT_LISTITEM);
	//edit details link
	_assertVisible($WISHLIST_EDIT_DETAILS);
	//availability
	_assertVisible($WISHLIST_INSTOCK);
	//date added
	_assertVisible($WISHLIST_DATEADDED_TEXT);
	//quantity
	_assertVisible($WISHLIST_QUNATITYDESIRED_TEXT);
	//_assertVisible(_div("/QTY:/"));
	//priority
	_assertVisible($WISHLIST_PRIORITY_TEXT);
	_assertVisible($WISHLIST_PRIORITY_DROPDOWN);
	//update
	_assertVisible($WISHLIST_UPDATE_BUTTON);
	//remove
	_assertVisible($WISHLIST_REMOVE_BUTTON);
	//quantity
	_assertVisible($WISHLIST_QUANTITY_TEXT);
	_assertVisible($WISHLIST_QUANTITY_TEXTBOX);
	//add to cart
	_assertVisible($WISHLIST_ADDTOCART_BUTTON);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124523","Verify the functionality related 'Your Wish List Shipping Address' dropdown on 'Wish List page in application as a Registered user");
$t.start();
try
{
//Your Wish List Shipping Address drop down
if($boolean)
	{
		//shipping address drop down 
		_assertVisible($WISHLIST_SHIPPINGADDRESS_DROPDOWN);
		//select any address from drop down
		var $length=_getText($WISHLIST_SHIPPINGADDRESS_DROPDOWN).length;
		var $j=0;
		_log($j);
		for(var $i=0;$i<$length;$i++)
			{
				_setSelected($WISHLIST_SHIPPINGADDRESS_DROPDOWN,$i);
				if($i==0)
					{
						_assertEqual($Wishlist_Login[11][1], _getSelectedText($WISHLIST_SHIPPINGADDRESS_DROPDOWN));
					}
				else
					{
						_log($j);
						//_assertEqual($address[$i], _getSelectedText($WISHLIST_SHIPPINGADDRESS_DROPDOWN));
						$address="("+$addr_data[$j][1]+")"+" "+$addr_data[$j][4]+", "+$addr_data[$j][8]+", "+$addr_data[$j][11]+", "+$addr_data[$j][9];
						_assertEqual($address,_getSelectedText($WISHLIST_SHIPPINGADDRESS_DROPDOWN));
						$j++;
					}
			}
	}
else
	{
		_assert(false,"addresses are not present");
	}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124525","Verify the functionality related 'Add Gift Certificate'button on'Wish List  page in application as a Registered user");
$t.start();
try
{
	//click on add gift certificate link
	_click($WISHLIST_ADDGIFTCERTIFICATE_BUTTON);
	//after clicking 'add gift certificate' button should not visible
	_assertNotVisible($WISHLIST_ADDGIFTCERTIFICATE_BUTTON);
	//verify the added gift certificate
	_assertVisible($WISHLIST_GIFTCERTIFICATE_ROW);
	//image
	_assertVisible($WISHLIST_GIFTCERTIFICATE_IMAGE);
	//link
	_assertVisible($WISHLIST_GIFTCERTIFICATE_LINK_INROW);
	_assertVisible(_div("/"+$Wishlist_Reg[0][1]+"/"));
	//date added
	_assertVisible($WISHLIST_GIFTCERTIFICATE_DATEADDED_TEXT);
	//remove link
	_assertVisible($WISHLIST_GIFTCERTIFICATE_REMOVE_LINK);
	//remove the added gift certificate
	_click($WISHLIST_GIFTCERTIFICATE_REMOVE_LINK);
	//after clicking on remove
	_assertVisible($WISHLIST_ADDGIFTCERTIFICATE_BUTTON);
	_assertNotVisible($WISHLIST_GIFTCERTIFICATE_ROW);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148717/148718","Verify the navigation related to 'Home'/'My Account' link on 'Wish List page bread crumb in application as a Registered user");//Test Case Id Need Change(Vinay)
$t.start();
try
{
	//click on home link present in the bread crumb
	//_click(_link("Home",_in($PAGE_BREADCRUMB)));
	
	//Navigate to home page
	_click($HEADER_SALESFORCE_LINK);
	
	//verify the navigation to home page
	_assertVisible($HOMEPAGE_SLIDES);
	_assertVisible($HOMEPAGE_SLOTS);
	//navigating to wish list login page
	_click($WISHLIST_LINK);
	//click on my account link present in the bread crumb
	_click(_link($Wishlist_Login[1][1],_in($PAGE_BREADCRUMB)));
	//verify the navigation
	_assertVisible(_link($Wishlist_Login[1][1], _in($PAGE_BREADCRUMB)));
	_assertVisible(_heading1("/"+$Wishlist_Login[1][1]+"/"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124527/124507","Verify the navigation related to product name present below the 'Make This List Public' button wishlist page in application as a  Registered  user");
$t.start();
try
{
	//navigating to wish list login page
	_click($WISHLIST_LINK);
	//fetch the total no.of products
	var $count=WishlistProductCount();
	
	for(var $i=0;$i<$count;$i++)
		{
		var $productName=_getText(_link("/(.*)/",_in(_div("name ["+$i+"]"))));
		//click on name link
		_click(_link("/(.*)/",_in(_div("name ["+$i+"]"))));
		//verify the navigation
		_assertVisible(_span($productName, _in($PAGE_BREADCRUMB)));
		_assertVisible($PDP_PRODUCTNAME);
		_assertEqual($productName, _getText($PDP_PRODUCTNAME));
		//navigating to wish list login page
		_click($WISHLIST_LINK);
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124547","Verify the navigation related to 'Edit details' link in on Wishlist page  nav in application as an registred user.");
$t.start();
try
{
	//navigating to wish list login page
	_click($WISHLIST_LINK);
	var $pName=_getText($WISHLIST_PRODUCTNAME);
	//click on edit details link under wishlist product
	_click($WISHLIST_EDIT_DETAILS);
	//verify the navigation to respective PDP
	_assertVisible(_span($pName, _in($PAGE_BREADCRUMB)));
	_assertVisible($PDP_PRODUCTNAME);
	_assertEqual($pName, _getText($PDP_PRODUCTNAME));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148722/148723/148724/148725","Verify the validation related to the 'Last name',First name,email id field/'Find' button functionality on wish list login page, find someone's wish list section in application as a registered user");
$t.start();
try
{
	//navigating to wish list login page
	_click($WISHLIST_LINK);
	//validating the fields last name,first name,email 
	for(var $i=0;$i<$Validations.length;$i++)
	  {
		_log($i);
		if($i==6)
			{
				_setValue($WISHLIST_LASTNAME_TEXTBOX, $LName);
				_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $FName);
				_setValue($WISHLIST_EMAIL_TEXTBOX,$uId);
			}
		else
			{
				_setValue($WISHLIST_LASTNAME_TEXTBOX, $Validations[$i][1]);
				_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $Validations[$i][2]);
				_setValue($WISHLIST_EMAIL_TEXTBOX, $Validations[$i][3]);
			}
		//Blank
		if($i==0 || $i==2 || $i==3 || $i==4 || $i==5 || $i==6)
		{
			//click on find button
			_click($WISHLIST_FIND_BUTTON);
			//user should be in same page
			_assertVisible(_heading1($Wishlist_Login[1][0]));
			//verify the text message
			if($i==6)
				{
				var $expText=$LName+" "+$FName;
				_assertVisible(_row("/"+$expText+"/"));
				_assertVisible($WISHLIST_VIEW_LINK);
				}
			else
				{
				_assertVisible($WISHLIST_NOWISHLIST_PARARAPH);
				}
		}
		//max characters
		else if($i==1)
			{
			_assertEqual($Validations[1][4],_getText($WISHLIST_LASTNAME_TEXTBOX).length);
			_assertEqual($Validations[1][4],_getText($WISHLIST_FIRSTNAME_TEXTBOX).length);
			_assertEqual($Validations[1][4],_getText($WISHLIST_EMAIL_TEXTBOX).length);
			}	
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("289166","Verify the UI of wish list details page of the application as a registered user");
try
{
	
//click on view link in wish list page
_click($WISHLIST_VIEW_LINK);
_assertVisible($WISHLIST_FINDSOMEONES_WISHLIST);
_assertVisible($PAGE_BREADCRUMB);
_assertVisible(_link($Wishlist_Login[0][0], _in($PAGE_BREADCRUMB)));
_assertVisible(_heading1($Wishlist_Login[1][0]));
var $expText=$LName+" "+$FName+" - "+"View";
_assertVisible(_row($expText));
//last names
_assertVisible($WISHLIST_SEARCH_LASTNAME_TEXT);
_assertEqual($LName, _getText($WISHLIST_SEARCH_LASTNAME_TEXT));
//first name
_assertVisible($WISHLIST_SEARCH_FIRSTNAME_TEXT);
_assertEqual($FName, _getText($WISHLIST_SEARCH_FIRSTNAME_TEXT));
_assertVisible($WISHLIST_SEARCH_CITY_TEXT);
_assertVisible($WISHLIST_SEARCH_VIEW_TEXT);
//My account link
_assertVisible($NAV_ACCOUNTSETTINGS_LINK);
_assertVisible($NAV_SHOPCONFIDENTLY_LINK);
_assertVisible($NEED_HELP_HEADING);
_assertVisible($WISHLIST_NEEDHELP_TEXT);
	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("289168/289205/289122","Verify the UI of  navigation pane in wish list details page of the application as a registered user");
$t.start();
try
{

// nav
_assertVisible($LEFTNAV_SECTION);
var $Heading=HeadingCountInNav();
var $Links=LinkCountInNav();
//checking headings
for(var $i=0;$i<$Heading;$i++)
	{
		_assertVisible(_span($Generic[$i][1]));			
	}
//checking links
for(var $i=0;$i<$Links;$i++)
	{
		_assertVisible(_link($Generic[$i][2]));			
	}
// nav content asset(contact us)
_assertVisible($LEFTNAV_CONTENTASSET);
_assertVisible($WISHLIST_NEEDHELP_TEXT);

}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("148719","Verify the functionality related to the 'Make This List Private' button on Wishlist page, find someone's wish list section in application as an registered user.");
$t.start();
try
{
	_click($WISHLIST_LINK);
	
	//_click($WISHLIST_VIEW_LINK);
	
	//verify the 'Make This List Private' functionality
	if(_isVisible($WISHLIST_MAKETHISPRIVATE_BUTTON))
		{
		_click($WISHLIST_MAKETHISPRIVATE_BUTTON);
		//_click($WISHLIST_MAKETHISPRIVATE_BUTTON);
		}
	//'Make This List public' link should be there
	_assertVisible($WISHLIST_MAKETHISPUBLIC_BUTTON);
	_assertNotVisible($WISHLIST_MAKETHISPUBLIC_TEXT);
	
	//items should not be visible
	_setValue($WISHLIST_EMAIL_TEXTBOX,$Wishlist_Login[3][4]);
	_click($WISHLIST_FIND_BUTTON  );
	//wish list results table should not display for invalid credentials
	var $expText=$LName+" "+$FName;
	_assertEqual(false,_isVisible(_row("/"+$expText+"/"))); 
	_assertNotVisible($WISHLIST_VIEW_LINK);
	_assertVisible($WISHLIST_NOWISHLIST_PARARAPH);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("140720","Verify the functionality related to the 'Make This List public' button on Wishlist page, find someone's wish list section in application as an registered user.");
$t.start();
try
{
	_click($WISHLIST_LINK);
	//_click($WISHLIST_VIEW_LINK);
	//verify the 'Make This List Public' functionality
	if (_isVisible($WISHLIST_MAKETHISPUBLIC_BUTTON))
		{
		_click($WISHLIST_MAKETHISPUBLIC_BUTTON);
		}
	//'Make This List private' link should be there
	_assertVisible($WISHLIST_MAKETHISPRIVATE_BUTTON);
	_assertVisible($WISHLIST_MAKETHISPUBLIC_TEXT);
	//items should be visible
	_setValue($WISHLIST_EMAIL_TEXTBOX,$uId);
	_click($WISHLIST_FIND_BUTTON  );
	//wish list results table should not display for invalid credentials
	var $expText=$LName+" "+$FName;
	_assertVisible(_row("/"+$expText+"/"));
	_assertVisible($WISHLIST_VIEW_LINK);
	_assertNotVisible($WISHLIST_NOWISHLIST_PARARAPH);  
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("140722/124549","Verify the functionality related to the 'Update' link on Wishlist page, find someone's wish list section in application as a registered user.");
$t.start();
try
{
	//navigating to wish list login page
	_click($WISHLIST_LINK);
	
	//_click($WISHLIST_VIEW_LINK);
	//making list as public
	if(_isVisible($WISHLIST_MAKETHISPUBLIC_BUTTON))
		{
		_click($WISHLIST_MAKETHISPUBLIC_BUTTON);
		}
	//incresing the quantity desired
	_setValue(_numberbox("dwfrm_wishlist_items_i"+$Wishlist_Reg[0][9]+"_quantity"),$Wishlist_Reg[0][3]);
	//setting priority to high
	_setSelected($WISHLIST_PRIORITY_DROPDOWN,$Wishlist_Reg[1][3]);
	//un check the public check box
	_uncheck($WISHLIST_MAKETHISPUBLIC_CHECKBOX);
	//click on update link
	_click($WISHLIST_UPDATE_BUTTON);
	//verify the functionality
	_assertEqual($Wishlist_Reg[0][3],_getText(_numberbox("dwfrm_wishlist_items_i"+$Wishlist_Reg[0][9]+"_quantity")));
	_assertEqual($Wishlist_Reg[1][3],_getSelectedText($WISHLIST_PRIORITY_DROPDOWN));
	_assertNotTrue($WISHLIST_MAKETHISPUBLIC_CHECKBOX.checked);	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124551/148721","Verify the functionality related to the 'ADD TO CART' button wish list search result page, find someone's wish list section in application as a Registered user");
$t.start();
try
{
	//fetch the total no.of products
	var $count=WishlistProductCount();
	if($count!=0)
		{
		for(var $i=0;$i<$count;$i++)
			{
			var $productName=_getText(_link("/(.*)/",_in(_div("name ["+$i+"]"))));	
			//var $cartQuantity=_extract(_getText($MINICART_TOTAL),"[(](.*)[)]",true);
			var $cartQuantity=_getText($MINICART_TOTAL);
			var $quanitytInWishlist=parseInt(_getText($WISHLIST_QUANTITY_TEXTBOX));
			//verify the functionality of add to cart button
			//_click(_submit("dwfrm_wishlist_items_i"+$i+"_addItemToCart"));  //dwfrm_wishlist_items_i0_addToCart
			
			_click(_submit("dwfrm_wishlist_items_i"+$i+"_addToCart"));
			//navigate to cart
			_click($MINICART_VIEWCART_LINK);
			//Page should navigate to cart page
			_assertVisible($CART_TABLE);
			var $prodNameInCart=_getText($PRODUCTNAMEIN_CART);
			$cartQuantity=parseInt($cartQuantity)+$quanitytInWishlist;
			//_assertEqual(parseInt($cartQuantity),parseInt(_extract(_getText($MINICART_TOTAL),"[(](.*)[)]",true)));
			_assertEqual(parseInt($cartQuantity),_getText($MINICART_TOTAL));
			
			}
		}
	else
		{
		_assert(false,"no items are there in wish list");
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("289208","Verify the validations for quantity field in wish list page of the application as a registered user");
$t.start();
try
{

//navigating to wish list login page
_click($WISHLIST_LINK);	

	//validations for quantity field
	for(var $i=0;$i<$qunatityvalidations.length;$i++)
		{
		_setValue($WISHLIST_QUANTITY_TEXTBOX,$qunatityvalidations[$i][1]);
		_click($WISHLIST_ADDTOCART_BUTTON);
		
		if ($i==0 || $i==1 || $i==2)
			{
			_assertEqual($qunatityvalidations[0][2],_getText($WISHLIST_QUANTITY_ERRORMESSAGE));
			_assertEqual($qunatityvalidations[0][3],_style($WISHLIST_QUANTITY_TEXTBOX,"background-color"))
			}
		else
			{
			_assertEqual($qunatityvalidations[3][1],_getText($WISHLIST_MINICART_QUANTITY));
			}
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("289209/289772", "Verify the navigation related to links in left navigation pane of wish list page of the application as a registered user");
$t.start();
try
{
//left nav links	
var $Links=LinkCountInLeftNav();
for(var $i=0;$i<$Links;$i++)
	{
		//navigate to my account home page
		_click($HEADER_USERACCOUNT_LINK);
		_click($HEADER_USERACCOUNT_LINK);
		_click(_link($MyAcc_Home[$i][3],_in(_listItem($MyAcc_Home[$i][3]))));
		//click on link personal data
		if($i==0)
			{
			//verify the navigation
			_assertVisible(_heading1($MyAcc_Home[$i][4]));
			_assertVisible($PAGE_BREADCRUMB);
			_assertVisible(_link($MyAcc_Home[$i][4], _in($PAGE_BREADCRUMB)));
			}
		//click on link Address
		if($i==1)
			{
			//verify the navigation
			_assertVisible(_heading1($MyAcc_Home[$i][4]));
			_assertVisible($PAGE_BREADCRUMB);
			_assertVisible(_link($MyAcc_Home[$i][4], _in($PAGE_BREADCRUMB)));
			}
		//click on link payment settings
		if($i==2)
			{
			//verify the navigation
			_assertVisible(_heading1($MyAcc_Home[$i][4]));
			_assertVisible($PAGE_BREADCRUMB);
			_assertVisible(_link($MyAcc_Home[2][3], _in($PAGE_BREADCRUMB)));
			}
		//click on link order history
		if($i==3)
			{
			//verify the navigation
			_assertVisible(_heading1($MyAcc_Home[$i][4]));
			_assertVisible($PAGE_BREADCRUMB);
			_assertVisible(_link($MyAcc_Home[$i][4], _in($PAGE_BREADCRUMB)));
			}
		//click on link modify wish list & Search Wish Lists
		if($i==4 || $i==5)
			{
			//verify the navigation
			_assertVisible(_heading1($MyAcc_Home[4][4]));
			_assertVisible($PAGE_BREADCRUMB);
			_assertVisible(_link($MyAcc_Home[4][5], _in($PAGE_BREADCRUMB)));
			}
		//click on link create registry & Search Registries & Modify Registries
		if($i==6 || $i==7 || $i==8)
			{
			//verify the navigation 
			_assertVisible(_heading1("/"+$MyAcc_Home[6][4]+"/"));
			_assertVisible($PAGE_BREADCRUMB);
			_assertVisible(_link($MyAcc_Home[6][5], _in($PAGE_BREADCRUMB)));
			}
		//click on link privacy policy
		if($i==9)
			{
			//verify the navigation 
			_assertVisible(_heading1($MyAcc_Home[$i][4]));
			_assertContainsText($MyAcc_Home[$i][4], $PAGE_BREADCRUMB);
			
			}
		//click on link Secure Shopping
		if($i==10)
			{
			//verify the navigation 
			_assertVisible(_heading1($MyAcc_Home[$i][4]));
			_assertContainsText($MyAcc_Home[$i][4], $PAGE_BREADCRUMB);
			}
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("289649","Verify the edit details functionality for a product in wish list page of the application as a registered user");
$t.start();
try
{

//Navigate to WL page
_click($HEADER_USERACCOUNT_LINK);
_click($WISHLIST_LINK);
//Navigating to PDP
_assert(false,"It should navigate to overlay but it is navigating to PDP");

}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("289657","Verify the functionality of make this item pulic checkbox displayed for a prodcut in wish list page of the application");
$t.start();
try
{

var $productName_before_public=_getText($WISHLIST_PRODUCTNAME);	
//un check the check box 
_uncheck($WISHLIST_MAKETHISPUBLIC_CHECKBOX);
//click on update button
_click($WISHLIST_PRODUCT_UPDATE_BUTTON);
//logout from the application
logout();
//click on wish list link from footer
_click($WISHLIST_LINK);
//enter valid values in text boxes
_setValue($WISHLIST_LASTNAME_TEXTBOX, $LName);
_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $FName);
_setValue($WISHLIST_EMAIL_TEXTBOX,$uId);
//click on submit button
_click($WISHLIST_FIND_BUTTON);
//click on view link in search result page 
_click($WISHLIST_VIEW_LINK);
//
if (_isVisible($WISHLIST_PRODUCTNAME))
	{
	var $productName_after_public=_getText($WISHLIST_PRODUCTNAME);
	//product name should not match
	_assertNotEquals($productName_before_public,$productName_after_public);
	}
else
	{
	_assertVisible($WISHLIST_SEARCHRESULT_PAGE_NOWISHLISTFOUND);
	}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("124550","Verify the functionality related to the 'Remove' link on Wish list page, find someone's wish list section in application as a  Registered user");
$t.start();
try
{
	//Navigate to WL page
	_click($HEADER_USERACCOUNT_LINK);
	_click($WISHLIST_LINK);
	
	//fetch the total no.of products	
	var $count=WishlistProductCount();
	if($count==1)
		{
		_click($WISHLIST_DELETE_LINK);
		//no products should be present 
		_assertVisible(_link($Wishlist_Reg[0][8]));
		_assertNotVisible($WISHLIST_REMOVE_BUTTON);
		}
	else
		{
		for(var $i=0;$i<$count;$i++)
			{	
			var $productName=_getText($WISHLIST_PRODUCTNAME);
			_click($WISHLIST_DELETE_LINK);
			//verify whether first item is removed or not
			_assertNotVisible(_link($productName,_in($WISHLIST_PRODUCTNAME_DIV)));
			}
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();



cleanup();