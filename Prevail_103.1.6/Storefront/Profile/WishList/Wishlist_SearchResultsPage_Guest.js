_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("WishList.xls");

var $Wishlist_Login=_readExcelFile("WishList.xls","Wishlist_Login");
var $Validations=_readExcelFile("WishList.xls","Validations");
var $WishListLogin_Validation=_readExcelFile("WishList.xls","Login_Validations");
var $Wishlist_Reg=_readExcelFile("WishList.xls","Wishlist_Reg");


SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//adding items to wish list for particular account
additemsToWishListAccount($Wishlist_Reg[0][0]);

var $t=_testcase("148708/148709","Verify the navigation related to 'Home'/My account link on wish list search result page bread crumb in application as an Anonymous user");
$t.start();
try
{
	//navigating to find some one's wish list page	
	navigateToFindSomeOneWishlist();
		
	//Navigate to home page
	_click($HEADER_SALESFORCE_LINK);
	
	//verify the navigation to home page
	_assertVisible($HOMEPAGE_SLIDES);
	_assertVisible($HOMEPAGE_SLOTS);
	//navigating to find some one's wish list page	
	navigateToFindSomeOneWishlist();
	//click on my account link present in the bread crumb
	_click(_link($Wishlist_Login[1][1],_in($PAGE_BREADCRUMB)));
	//verify the navigation
	_assertVisible(_link($Wishlist_Login[1][1], _in($PAGE_BREADCRUMB)));
	_assertVisible(_heading1($Wishlist_Login[3][0]));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148710","Verify the navigation related to 'Create an account' link on wish list search result page left nav in application as an Anonymous user");
$t.start();
try
{
	//navigating to find some one's wish list page	
	navigateToFindSomeOneWishlist();
	//click on link create an account
	_click(_link($Wishlist_Login[2][5]));
	//verify the navigation
	_assertVisible(_heading1($Wishlist_Login[0][5]));
	_assertVisible(_link($Wishlist_Login[1][5], _in($PAGE_BREADCRUMB)));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124514","Verify the navigation related to  product name link  on wish list search result page in application as an Anonymous user");
$t.start();
try
{
	//navigating to find some one's wish list page	
	navigateToFindSomeOneWishlist();
	if(_isVisible($WISHLIST_VIEW_LINK))
		{
		_click($WISHLIST_VIEW_LINK);
		}
	else
		{
		_assert(false,"entered account is not having any products");
		}
	//fetch the total no.of products
	var $count=WishlistProductCount();
	for(var $i=0;$i<$count;$i++)
		{
			var $productName=_getText(_link("/(.*)/",_in(_div("name ["+$i+"]"))));
			//click on name link
			_click(_link("/(.*)/",_in(_div("name ["+$i+"]"))));
			//verify the navigation	
			_assertVisible(_span($productName, _in($PAGE_BREADCRUMB)));
			_assertVisible($PDP_PRODUCTNAME);
			_assertEqual($productName, _getText($PDP_PRODUCTNAME));
			//navigating to wish list search results page
			navigateToFindSomeOneWishlist();
			_click($WISHLIST_VIEW_LINK);
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124522/148711/148712/124511","Verify the validation related to the 'Last name',First name,email id field/'Find' button functionality on wish list search results page in Laura Mercier application as a anonymous user");
$t.start();
try
{	
	//validating the fields last name,first name,email 
	for(var $i=0;$i<$Validations.length;$i++)
	  {
		_click($HEADER_SALESFORCE_LINK);
		_click($WISHLIST_LINK);
		if($i==6)
			{
				_setValue($WISHLIST_LASTNAME_TEXTBOX, $LName);
				_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $FName);
				_setValue($WISHLIST_EMAIL_TEXTBOX,$uId);
			}
		else
			{
				_setValue($WISHLIST_LASTNAME_TEXTBOX, $Validations[$i][1]);
				_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $Validations[$i][2]);
				_setValue($WISHLIST_EMAIL_TEXTBOX, $Validations[$i][3]);
			}		
		//Blank
		if($i==0 || $i==2 || $i==3 || $i==4 || $i==5 || $i==6)
			{
				//click on find button
				_click($WISHLIST_FIND_BUTTON);
				//user should be in same page
				_assertVisible(_heading1($Wishlist_Login[1][0]));
				//verify the text message
				if($i==6)
					{
					var $expText=$LName+" "+$FName+" - "+"View";
					_assertVisible(_row($expText));
					_assertVisible($WISHLIST_VIEW_LINK);
					}
				else
					{
						_assertVisible(_paragraph($Validations[$i][7]));
					}
			}
		//max characters
		if($i==1)
			{
			_assertEqual($Validations[1][4],_getText($WISHLIST_LASTNAME_TEXTBOX).length);
			_assertEqual($Validations[1][4],_getText($WISHLIST_FIRSTNAME_TEXTBOX).length);
			_assertEqual($Validations[1][4],_getText($WISHLIST_EMAIL_TEXTBOX).length);
			}
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


//login to the application
_click($HEADER_LOGIN_LINK);
login();

var $t=_testcase("124512","Verify the functionality related to the 'View' link on wish list search result page, find someone's wish list section  in application as a Anonymous user");
$t.start();
try
{
	//navigating to find some one's wish list page	
	navigateToFindSomeOneWishlist();	
	//click on view link
	if(_isVisible($WISHLIST_VIEW_LINK))
		{
		_click($WISHLIST_VIEW_LINK);
		//verify the navigation
		_assertVisible($WISHLIST_ADDTOCART_BUTTON);
		_assertVisible($WISHLIST_ITEM_LIST);
		}
	else
		{
		_assert(false,"entered account is not having any products");
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124515","Verify the functionality related to the 'ADD TO CART' button wish list search result page, find someone's wish list section in application as a Anonymous user");
$t.start();
try
{
	
/*	//click on add to cart button
	//fetch the total no.of products
	var $count=_count("_div","/name/",_in($WISHLIST_ITEM_LIST));
	for(var $i=0;$i<$count;$i++)
		{
		var $productName=_getText(_link("/(.*)/",_in(_div("name ["+$i+"]"))));	
		var $cartQuantity=_getText($MINICART_TOTAL);
		var $quanitytInWishlist=_getText($WISHLIST_QUANTITY_TEXTBOX);
		//verify the functionality of add to cart button
		_click(_submit("dwfrm_wishlist_items_i"+$i+"_addToCart"));
		//verify the mini cart fly out and quantity
		_assertVisible(_div("mini-cart-content"));
		$cartQuantity=$quanitytInWishlist;
		_assertEqual(parseInt($cartQuantity),parseInt(_getText($MINICART_TOTAL)));
		}*/
	
	//fetch the total no.of products
	var $count=WishlistProductCount();
	if($count!=0)
		{
		for(var $i=0;$i<$count;$i++)
			{
			var $productName=_getText(_link("/(.*)/",_in(_div("name ["+$i+"]"))));	
			//var $cartQuantity=_extract(_getText($MINICART_TOTAL),"[(](.*)[)]",true);
			var $cartQuantity=_getText($MINICART_TOTAL);
			var $quanitytInWishlist=parseInt(_getText($WISHLIST_QUANTITY_TEXTBOX));
			//verify the functionality of add to cart button
			//_click(_submit("dwfrm_wishlist_items_i"+$i+"_addItemToCart"));  //dwfrm_wishlist_items_i0_addToCart
			
			_click(_submit("dwfrm_wishlist_items_i"+$i+"_addToCart"));
			//navigate to cart
			_click($MINICART_VIEWCART_LINK);
			//Page should navigate to cart page
			_assertVisible($CART_TABLE);
			var $prodNameInCart=_getText($PRODUCTNAMEIN_CART);
			$cartQuantity=parseInt($cartQuantity)+$quanitytInWishlist;
			//_assertEqual(parseInt($cartQuantity),parseInt(_extract(_getText($MINICART_TOTAL),"[(](.*)[)]",true)));
			_assertEqual(parseInt($cartQuantity),_getText($MINICART_TOTAL));
			
			}
		}
	else
		{
		_assert(false,"no items are there in wish list");
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//deleting items from wishlist
deleteAddeditemsFromWishlistAccount();

cleanup();