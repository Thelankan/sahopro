_include("../../../GenericLibrary/GlobalFunctions.js");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//adding items to wish list for particular account
additemsToWishListAccount($Wishlist_Reg[0][0]);

var $t = _testcase("----/124426", "Verify the UI of 'Returning customers' section wishlist login page in application as an Anonymous user" +
		  "Verify the navigation of 'WISH LIST' link at the header as a guest user.");
$t.start();
try
{ 
	//navigating to wish list login  page
	_click($WISHLIST_LINK);
	//verify bread crumb
	_assertVisible(_link($Wishlist_Login[2][1], _in($PAGE_BREADCRUMB)));
	//Verifying the UI of returning customer section
	//MyAccount login as heading
	_assertVisible(_heading1($Wishlist_Login[3][0]));
	//Returning customer heading
	_assertVisible(_heading2($Wishlist_Login[4][0]));
	//Paragraph with static text
	_assertVisible($LOGIN_RETURNUS_TEXT);
	//email Address
	_assertVisible(_label($Wishlist_Login[6][0]));
	_assertVisible($LOGIN_EMAIL_ADDRESS_TEXTBOX);
	_assertEqual("", _getValue($LOGIN_EMAIL_ADDRESS_TEXTBOX));
	//password
	_assertVisible(_label($Wishlist_Login[7][0]));
	_assertVisible($LOGIN_PASSWORD_TEXTBOX);
	_assertEqual("", _getValue($LOGIN_PASSWORD_TEXTBOX));
	//remember me
	_assertVisible($LOGIN_REMEMBERMETEXT);
	_assertVisible($LOGIN_REMEMBEMETEXTBOX);
	_assertNotTrue($LOGIN_REMEMBEMETEXTBOX.checked);
	//login button
	_assertVisible($LOGIN_BUTTON);
	//forgot password
	_assertVisible($LOGIN_PASSWORD_RESET);
	_assertEqual($Wishlist_Login[8][0], _getText($LOGIN_PASSWORD_RESET));
	//social links
	_assertVisible($WISHLIST_ENTIRE_LOGIN_SECTION);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("124806","Verify the UI of 'Create a wishlist' section on wishlist login page in application as an anonymous user");
$t.start();
try
{
	//verify the UI of create wish list section
	_assertVisible(_heading2($Wishlist_Login[0][6]));
	_assertVisible($CONTENT_ASSET);
	_assertVisible($LOGIN_CREATEACCOUNTNOW_BUTTON);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148695","Verify the UI of  'Find Someone's Wish List'  section on wishlist login page in application as an anonymous user");
$t.start();
try
{
	//verify the UI of 'Find Someone's Wish List'
	_assertVisible(_heading2($Wishlist_Login[1][6]));
	_assertVisible($WISHLIST_TEXT_PARAGRAPH);
	//last name
	_assertVisible(_span($Wishlist_Login[2][6]));
	_assertVisible($WISHLIST_LASTNAME_TEXTBOX);
	//first name
	_assertVisible(_span($Wishlist_Login[3][6]));
	_assertVisible($WISHLIST_FIRSTNAME_TEXTBOX);
	//email 
	_assertVisible(_label($Wishlist_Login[4][6]));
	_assertVisible($WISHLIST_EMAIL_TEXTBOX);
	//find button
	_assertVisible($WISHLIST_FIND_BUTTON);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124427","Verify the UI of  'Wishlist login page' in application as an anonymous user");
$t.start();
try
{
	//verify the UI
	_assertVisible(_link($Wishlist_Login[2][1], _in($PAGE_BREADCRUMB)));
	_assert(_isVisible(_heading1($Wishlist_Login[3][0])));
	//left nav section
	_assertVisible(_span($Wishlist_Login[5][6]));
	_assertVisible(_link($Wishlist_Login[2][5]));
	_assertVisible(_span($Wishlist_Login[6][6]));
	_assertVisible(_link($Wishlist_Login[7][6]));
	_assertVisible(_link($Wishlist_Login[8][6]));
	//returning customer section
	_assertVisible($WISHLIST_RETURNINGCUSTOMER_SECTION);
	_assertVisible($WISHLIST_CREATEWISHLIST_HEADING);
	_assertVisible($WISHLIST_FINDSOMEONES_WISHLIST);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124428","Verify the navigation related to 'Home' and 'My Account' links in Wish list login page bread crumb in application as an  Anonymous user");
$t.start();
try
{
	//Navigate to home page
	_click($HEADER_SALESFORCE_LINK);
	
	//verify the navigation to home page
	_assertVisible($HOMEPAGE_SLIDES);
	_assertVisible($HOMEPAGE_SLOTS);
	//navigating to wish list login page
	_click($WISHLIST_LINK);
	//click on my account link present in the bread crumb
	_click(_link($Wishlist_Login[1][1],_in($PAGE_BREADCRUMB)));
	//verify the navigation
	_assertVisible(_link($Wishlist_Login[1][1], _in($PAGE_BREADCRUMB)));
	_assertVisible(_heading1($Wishlist_Login[3][0]));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148696","Verify the navigation related to 'Forgot Password' link on on Wishlist login page returning customer section in application as an anonymous user.");
$t.start();
try
{
	//navigate to wish list page
	_click($WISHLIST_LINK);
	//Checking Bread crumb
	_assertVisible(_link($Wishlist_Login[0][0], _in($PAGE_BREADCRUMB)));
	//wish list page
	_assertVisible(_heading1($Wishlist_Login[3][0]));
	//clicking on forgot password link
	_click($WISHLIST_FORGETPASSWORD_LINK);
	_assertVisible($LOGIN_DIALOG_CONTAINER);
	_assertVisible($WISHLIST_RESETPASSWORD_OVERLAY);
	_assertVisible(_heading1($Wishlist_Login[2][0]));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("----","Verify the functionality �Send� button related to Forgot password overlay on Wish list login page returning customer section in application as an Anonymous user");
$t.start();
try
{
	//entering valid email address
	_setValue($LOGIN_FORGETPASSWORD_TEXTBOX,$Wishlist_Login[2][4]);	
	//click on send button
	_click($WISHLIST_FORGETPASSWORD_SEND_BUTTON);
	//verify the navigation
	_assertVisible(_heading1($Wishlist_Login[12][1]));
	_assertVisible($HEADER_SALESFORCE_LINK);
	//close the dialog box
	_click($WISHLIST_RESETPASSWORD_CLOSEBUTTON_ICON);	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("124429/289770","Verify the navigation related to links in left navigation pane of wish list details page as a guest user");
$t.start();
try
{
	for(var $i=0;$i<3;$i++)
		{
			//click on wishlist
			_click($WISHLIST_LINK);
			//click on left nav as a guest user
			_click(_link($Wishlist_Login[$i][11]));
			
		if ($i==0)
			{
			//verify the navigation
			_assertVisible(_heading1($Wishlist_Login[0][5]));
			_assertVisible(_link($Wishlist_Login[1][5], _in($PAGE_BREADCRUMB)));
			}
		else if ($i==1)
			{
			//verify the navigation
			_assertVisible(_heading1($Wishlist_Login[4][1]));
			_assertVisible($CONTENT_ASSET);
			_assertVisible($PAGE_BREADCRUMB);
			_assertContainsText($Wishlist_Login[4][1], $PAGE_BREADCRUMB);
			}
		else
			{
			//verify the navigation
			_assertVisible($PAGE_BREADCRUMB);
			_assertContainsText($Wishlist_Login[5][1], $PAGE_BREADCRUMB);
			_assertVisible(_heading1($Wishlist_Login[5][1]));
			}
			
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("124504","Verify the UI of 'Forgot Password' overlay  on wishlist login page in application as an  Anonymous user");
$t.start();
try
{
	//navigate to wish list page
	_click($WISHLIST_LINK);
	//click on forgot password link
	_click($LOGIN_PASSWORD_RESET);
	//verify the overlay appearance
	_assertVisible($WISHLIST_RESETPASSWORD_DIALOG_TITLE);
	_assertVisible(_heading1($Wishlist_Login[7][1]));
	_assertVisible($LOGIN_FORGETPASSWORD_TEXTBOX);
	_assertVisible($WISHLIST_FORGETPASSWORD_SEND_BUTTON);

}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124505","Verify the navigation related to 'Create an account now' button on Wishlist login page create a wishlist section  in application as an anonymous user");
$t.start();
try
{
	_click($WISHLIST_LINK);
	//click on link Create an account now button
	_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//verify the navigation
	_assertVisible(_heading1($Wishlist_Login[0][5]));
	_assertVisible(_link($Wishlist_Login[1][5], _in($PAGE_BREADCRUMB)));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148703/148704/124506/148705","Verify the validation related to the 'Last name',First name,email id field/'Find' button functionality on wish list login page, find someone's wish list section in application as a anonymous user");
$t.start();
try
{
	_click($WISHLIST_LINK);
	//validating the fields last name,first name,email 
	for(var $i=0;$i<$Validations.length;$i++)
	  {
		if($i==6)
		{
			_setValue($WISHLIST_LASTNAME_TEXTBOX, $LName);
			_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $FName);
			_setValue($WISHLIST_EMAIL_TEXTBOX,$uId);
		}
		else
		{
			_setValue($WISHLIST_LASTNAME_TEXTBOX, $Validations[$i][1]);
			_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $Validations[$i][2]);
			_setValue($WISHLIST_EMAIL_TEXTBOX, $Validations[$i][3]);
		}	
		//Blank
		if($i==0 || $i==2 || $i==3 || $i==4 || $i==5 || $i==6)
		{
			//click on find button
			_click($WISHLIST_FIND_BUTTON);
			//user should be in same page
			_assertVisible(_heading1($Wishlist_Login[1][0]));
			//verify the text message
		if($i==6)
			{
			var $expText=$LName+" "+$FName+" - "+"View";
			_assertVisible(_row($expText));
			_assertVisible($WISHLIST_VIEW_LINK);
			}
		else
			{
			_assertVisible($WISHLIST_NOWISHLIST_PARARAPH);
			}
		}
		//max characters
		if($i==1)
			{
			_assertEqual($Validations[1][4],_getText($WISHLIST_LASTNAME_TEXTBOX).length);
			_assertEqual($Validations[1][4],_getText($WISHLIST_FIRSTNAME_TEXTBOX).length);
			_assertEqual($Validations[1][4],_getText($WISHLIST_EMAIL_TEXTBOX).length);
			}
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124430/124431/148702","Verify the validation related to the 'Email address'/'password' field/'LOGIN' button functionality on Wish list login page, Returning customers section in application as a Anonymous user");
$t.start();
try
{
	_click($HEADER_SALESFORCE_LINK);
	_click($WISHLIST_LINK);
	//validating the email address and password fields	
	for(var $i=0;$i<$WishListLogin_Validation.length;$i++)
		{
		if($i==5)
			{
			login();
			}
		else
			{
		_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$WishListLogin_Validation[$i][1]);
		_setValue($LOGIN_PASSWORD_TEXTBOX,$WishListLogin_Validation[$i][2]);
		_click($LOGIN_BUTTON);
			}
		//blank
		if($i==0)
			{
			//username
			_assertEqual($WishListLogin_Validation[$i][5],_style($LOGIN_EMAIL_ADDRESS_TEXTBOX,"background-color"));
			_assertVisible(_span($WishListLogin_Validation[$i][3]));
			//password
			_assertVisible(_span($WishListLogin_Validation[$i][4]));
			_assertEqual($WishListLogin_Validation[$i][5],_style($LOGIN_PASSWORD_TEXTBOX,"background-color"));
			}
		//valid
		else if($i==5)
			{
			_assertVisible(_heading1($Wishlist_Login[1][0]));
			}
		//invalid email id
		else if($i==1||$i==2||$i==3)
		{
			//username
			_assertEqual($WishListLogin_Validation[0][5],_style($LOGIN_EMAIL_ADDRESS_TEXTBOX,"background-color"));
			_assertVisible(_span($WishListLogin_Validation[2][3]));
		}
		//invalid 
		else
			{
			_assertVisible(_div($WishListLogin_Validation[1][3]));
			}
		}
	//logout from application
	_click($HEADER_USERACCOUNT_LINK);
	_click($MY_ACCOUNT_LOGOUT_LINK);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148706/124508","Verify the UI of empty Wish List search' page in application as an anonymous user");
$t.start();
try
{
	//navigate to wish list page
	_click($WISHLIST_LINK);
	_setValue($WISHLIST_EMAIL_TEXTBOX, $Wishlist_Login[2][4]);
	//click on find button
	_click($WISHLIST_FIND_BUTTON);
	//verify the UI of empty Wish List search
	_assertVisible($PAGE_BREADCRUMB);
	_assertVisible(_link($Wishlist_Login[0][0], _in($PAGE_BREADCRUMB)));
	_assertVisible(_heading1($Wishlist_Login[1][0]));
	_assertVisible($WISHLIST_FINDSOMEONESWISLLIST_SECTION);
	//left nav section
	_assertVisible(_span($Wishlist_Login[5][6]));
	_assertVisible(_link($Wishlist_Login[2][5]));
	_assertVisible(_span($Wishlist_Login[6][6]));
	_assertVisible(_link($Wishlist_Login[7][6]));
	_assertVisible(_link($Wishlist_Login[8][6]));
	//need help section
	_assertVisible(_heading2($Wishlist_Login[9][6]));
	_assertVisible(_div($LEFTNAV_CONTENTASSET,_in($LEFTNAV_CONTENTASSET)));
	_assertVisible($LEFTNAV_CONTACTUS_LINK);
	//fields
	_assertVisible(_label($Wishlist_Login[2][7]));
	_assertVisible(_label($Wishlist_Login[3][7]));
	_assertVisible(_label($Wishlist_Login[4][7]));
	_assertVisible($WISHLIST_LASTNAME_TEXTBOX);
	_assertEqual("", _getValue($WISHLIST_LASTNAME_TEXTBOX));
	_assertVisible($WISHLIST_FIRSTNAME_TEXTBOX);
	_assertEqual("", _getValue($WISHLIST_FIRSTNAME_TEXTBOX));
	_assertVisible($WISHLIST_EMAIL_TEXTBOX);
	_assertEqual("", _getValue($WISHLIST_EMAIL_TEXTBOX));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();	


var $t=_testcase("148707/124509/289164","Verify the UI of  non empty Wish List search' page in application as an anonymous user/Verify the UI of wish list details page of the application as a guest user");
$t.start();
try
{
	//navigate to wish list page
	_click($WISHLIST_LINK);
	_setValue($WISHLIST_LASTNAME_TEXTBOX, $LName);
	_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $FName);
	_setValue($WISHLIST_EMAIL_TEXTBOX,$uId);
	
	//click on find button
	_click($WISHLIST_FIND_BUTTON);
	//verify the UI of non empty Wish List search
	_assertVisible($PAGE_BREADCRUMB);
	_assertVisible(_link($Wishlist_Login[0][0], _in($PAGE_BREADCRUMB)));
	_assertVisible(_heading1($Wishlist_Login[1][0]));
	var $expText=$LName+" "+$FName+" - "+"View";
	_assertVisible(_row($expText));
	//last names
	_assertVisible($WISHLIST_SEARCH_LASTNAME_TEXT);
	_assertEqual($LName, _getText($WISHLIST_SEARCH_LASTNAME_TEXT));
	//first name
	_assertVisible($WISHLIST_SEARCH_FIRSTNAME_TEXT);
	_assertEqual($FName, _getText($WISHLIST_SEARCH_FIRSTNAME_TEXT));
	_assertVisible($WISHLIST_SEARCH_CITY_TEXT);
	_assertVisible($WISHLIST_SEARCH_VIEW_TEXT);
	//left nav section
	_assertVisible(_span($Wishlist_Login[5][6]));
	_assertVisible(_link($Wishlist_Login[2][5]));
	_assertVisible(_span($Wishlist_Login[6][6]));
	_assertVisible(_link($Wishlist_Login[7][6]));
	_assertVisible(_link($Wishlist_Login[8][6]));
	//need help section
	_assertVisible(_heading2($Wishlist_Login[9][6]));
	_assertVisible(_div($LEFTNAV_CONTENTASSET,_in($LEFTNAV_CONTENTASSET)));
	_assertVisible($LEFTNAV_CONTACTUS_LINK);
	//fields
	_assertVisible(_label($Wishlist_Login[2][7]));
	_assertVisible(_label($Wishlist_Login[3][7]));
	_assertVisible(_label($Wishlist_Login[4][7]));
	_assertVisible($WISHLIST_LASTNAME_TEXTBOX);
	_assertEqual("", _getValue($WISHLIST_LASTNAME_TEXTBOX));
	_assertVisible($WISHLIST_FIRSTNAME_TEXTBOX);
	_assertEqual("", _getValue($WISHLIST_FIRSTNAME_TEXTBOX));
	_assertVisible($WISHLIST_EMAIL_TEXTBOX);
	_assertEqual("", _getValue($WISHLIST_EMAIL_TEXTBOX));
	
	//click on view link in wish list page
	_click($WISHLIST_VIEW_LINK);
	_assertVisible($WISHLIST_FINDSOMEONES_WISHLIST);
	_assertVisible($PAGE_BREADCRUMB);
	_assertVisible(_link($Wishlist_Login[0][0], _in($PAGE_BREADCRUMB)));
	_assertVisible(_heading1($Wishlist_Login[1][0]));
	var $expText=$LName+" "+$FName+" - "+"View";
	_assertVisible(_row($expText));
	//last names
	_assertVisible($WISHLIST_SEARCH_LASTNAME_TEXT);
	_assertEqual($LName, _getText($WISHLIST_SEARCH_LASTNAME_TEXT));
	//first name
	_assertVisible($WISHLIST_SEARCH_FIRSTNAME_TEXT);
	_assertEqual($FName, _getText($WISHLIST_SEARCH_FIRSTNAME_TEXT));
	_assertVisible($WISHLIST_SEARCH_CITY_TEXT);
	_assertVisible($WISHLIST_SEARCH_VIEW_TEXT);
	//My account link
	_assertVisible($LEFTNAV_ACCOUNTSETTINGS_LINK);
	_assertVisible($LEFTNAV_SHOPCONFIDENTLY_LINK);
	_assertVisible($NEED_HELP_HEADING);
	_assertVisible($WISHLIST_NEEDHELP_TEXT);
	
	//textbox labels
	_assertVisible($WISHLIST_LASTNAME_LABEL);
	_assertVisible($WISHLIST_FIRSTNAME_LABEL);
	_assertVisible($WISHLIST_EMAIL_LABEL);
	//product ID
	_assertVisible($WISHLIST_SKU_SECTION);
	_assertVisible($WISHLIST_PRODUCTID_SKU);
	//product Price
	_assertVisible($WISHLIST_PRICE);
	//image
	_assertVisible($WISHLIST_PRODUCTIMAGE);
	//name 
	_assertVisible($WISHLIST_PRODUCTNAME);
	_assertEqual($pName, _getText($WISHLIST_PRODUCTNAME));
	//product list item
	_assertVisible($WISHLIST_PRODUCT_LISTITEM);
	//edit details link
	_assertVisible($WISHLIST_EDIT_DETAILS);
	//availability
	_assertVisible($WISHLIST_INSTOCK);
	//date added
	_assertVisible($WISHLIST_DATEADDED_TEXT);
	//quantity
	_assertVisible($WISHLIST_QUNATITYDESIRED_TEXT);
	//_assertVisible(_div("/QTY:/"));
	//priority
	_assertVisible($WISHLIST_PRIORITY_TEXT);
	_assertVisible($WISHLIST_PRIORITY_DROPDOWN);
	//update
	_assertVisible($WISHLIST_UPDATE_BUTTON);
	//remove
	_assertVisible($WISHLIST_REMOVE_BUTTON);
	//quantity
	_assertVisible($WISHLIST_QUANTITY_TEXT);
	_assertVisible($WISHLIST_QUANTITY_TEXTBOX);
	//add to cart
	_assertVisible($WISHLIST_ADDTOCART_BUTTON);

	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("289769","Verify the validations for quantity field in wish list details page of the application");
$t.start();
try
{

//navigating to wish list login page
//_click($WISHLIST_LINK);	

	//validations for quantity field
	for(var $i=0;$i<$qunatityvalidations.length;$i++)
		{
		_setValue($WISHLIST_QUANTITY_TEXTBOX,$qunatityvalidations[$i][1]);
		_click($WISHLIST_ADDTOCART_BUTTON);
		
		if ($i==0 || $i==1 || $i==2)
			{
			_assertEqual($qunatityvalidations[0][2],_getText($WISHLIST_QUANTITY_ERRORMESSAGE));
			_assertEqual($qunatityvalidations[0][3],_style($WISHLIST_QUANTITY_TEXTBOX,"background-color"));
			}
		else
			{
			_assertEqual($qunatityvalidations[3][1],_getText($WISHLIST_MINICART_QUANTITY));
			}
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//deleting items from wish list
deleteAddeditemsFromWishlistAccount();

cleanup();
