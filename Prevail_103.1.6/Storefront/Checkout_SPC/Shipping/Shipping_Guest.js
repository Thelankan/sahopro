_include("../../../GenericLibrary/BM_Functions.js");
_include("../../../GenericLibrary/GlobalFunctions.js");
_include("../../../GenericLibrary/BM_Configuration.js");
_include("../../../ObjectRepository/Checkout/Shipping_OR.sah");
_include("../../../ObjectRepository/Checkout/Billing_OR.sah");
_include("../../../ObjectRepository/Cart/CART_OR.sah");
_include("../../../ObjectRepository/Cart/MINICART_OR.sah");

_resource("Shipping.xls");

//_log($CheckoutType);
//_log($BMConfig[0][1]);
//
////verifies whether five page or single page checkout
//if($CheckoutType==$BMConfig[0][1])
//{
	SiteURLs();
	_setAccessorIgnoreCase(true); 
	cleanup();

	var $t = _testcase("148900","Verify the navigation to 'SHIPPING' page through shoping cart page  in Prevail application as a Anonymous user");
	$t.start();
	try
	{
		//add items to cart
		navigateToCart($Shipping_Page[0][1],1);
		_click($CART_CHECKOUT_BUTTON_BOTTOM);	
		_click($SHIPPING_GUEST);
		//verify the navigation
		verifyNavigationToShippingPage();
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();


	ClearCartItems();
	var $t = _testcase("124628","Verify the navigation to 'SHIPPING' page in Application as a guest user");
	$t.start();
	try
	{	
		//add items to cart
		navigateToCart($Shipping_Page[0][1],1);
		//click on go strait to checkout link
		_mouseOver($MINICART_LINK);
		_click($MINICART_CHECKOUT);
		_click($SHIPPING_GUEST);
		//verify the navigation
		verifyNavigationToShippingPage();	
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();

	var $t = _testcase("124470","Verify the UI of shipping address form in Application as a Anonymous user.");
	$t.start();
	try
	{	
		//verify the UI of shipping page
		//Heading
		_assertVisible($SHIPPING_PAGE_HEADER);
		//required msg
		_assertVisible($SHIPPING_REQUIRED);
		//first name
		_assertVisible($SHIPPING_FIRST_NAME);
		_assertVisible($SHIPPING_FN_TEXTBOX);
		//last name
		_assertVisible($SHIPPING_LAST_NAME);
		_assertVisible($SHIPPING_LN_TEXTBOX);
		//address1
		_assertVisible($SHIPPING_ADDRESS_ONE);
		_assertVisible($SHIPPING_ADDRESS1_TEXTBOX);
		//address2
		_assertVisible($SHIPPING_ADDRESS_TWO);
		_assertVisible($SHIPPING_ADDRESS2_TEXTBOX);
		//country
		_assertVisible($SHIPPING_COUNTRY);
		_assertVisible($SHIPPING_COUNTRY_DROPDOWN);
		//default value

		//state
		_assertVisible($SHIPPING_STATE);
		_assertVisible($SHIPPING_STATE_DROPDOWN);
		//default value

		//city
		_assertVisible($SHIPPING_CITY);
		_assertVisible($SHIPPING_CITY_TEXTBOX);
		//zip
		_assertVisible($SHIPPING_ZIPCODE);
		_assertVisible($SHIPPING_ZIPCODE_TEXTBOX);
		//phone number
		_assertVisible($SHIPPING_PHONE_NUMBER);
		_assertVisible($SHIPPING_PHONE_TEXTBOX);
		_assertVisible($SHIPPING_PHONE_EXAMPLE);
		//add to address book
		_assertNotVisible($SHIPPING_ADDTOADDRESS);
		_assertNotVisible($SHIPPING_ADDTOADDRESS_CHECKBOX);
		//use this billing address for registered user
		_assertVisible($SHIPPING_BILLING_ADDRESS);
		_assertVisible($SHIPPING_BILLING_ADDRESS_CHECKBOX);
		_assertNotTrue($SHIPPING_BILLING_ADDRESS_CHECKBOX.checked);

		if(!isMobile() || mobile.iPad())
		{
			//tooltip links
			_assertVisible($SHIPPING_ADDRESS_TOOLTIP);
			_assertVisible($SHIPPING_PHONE_TOOLTIP);
		}
		//Is this a gift section
		_assertVisible($SHIPPING_GIFT_TEXT);
		_assertVisible(_radio("true"));
		_assertNotTrue(_radio("true").checked);
		_assertVisible(_radio("false"));
		_assert(_radio("false").checked);

		//_assertVisible(_radio("is-gift-yes"));
		//_assertNotTrue(_radio("is-gift-yes").checked);
		//_assertVisible(_radio("is-gift-no"));
		//_assert(_radio("is-gift-no").checked);

		//select shipping method section
		_assertVisible($SHIPPING_METHOD_HEADING);

		var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in($SHIPPING_METHOD_HEADING));
		for(var $i=0;$i<$totalShippingMethods;$i++)
		{
			//shipping methods
			_assertVisible(_div("form-row form-indent label-inline["+$i+"]"));
		}
		//continue button
		_assertVisible($SHIPPING_SUBMIT_BUTTON);
		//summary section
		_assertVisible($SHIPPING_RIGHT_NAV_HEADING);
		_assertVisible($SHIPPING_RIGHT_NAV);	

	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();

	ClearCartItems();
	var $t = _testcase("128337","Verify the display of 'Do you want to ship to multiple addresses?' when user has added single item to the cart from Checkout Shipping page in Application as a guest  user");
	$t.start();
	try
	{
		//adding two item's to cart
		navigateToCart($Shipping_Page[0][1],2);
		//navigate to shipping page
		_click($CART_CHECKOUT_BUTTON_BOTTOM);
		_click($SHIPPING_GUEST); 
		//Ship to multiple addresses section should not be displayed
		_assertVisible($SHIPPING_MULTIPLE_ADDRESS);
		_assertVisible($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();

	ClearCartItems();
	var $t = _testcase("128338","Verify the display of 'Do you want to ship to multiple addresses?' when user has added morethan one item to the cart from Checkout Shipping page in Application as a guest  user");
	$t.start();
	try
	{
		//adding two item's to cart
		navigateToCart($Shipping_Page[0][1],1);
		//navigate to shipping page
		_click($CART_CHECKOUT_BUTTON_BOTTOM);
		_click($SHIPPING_GUEST); 
		//Ship to multiple addresses section should not be displayed
		_assertNotVisible($SHIPPING_MULTIPLE_ADDRESS);
		_assertNotVisible($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
	} 
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();


	ClearCartItems();
	var $t = _testcase("128339/289217","Verify the display of  'Do you want to ship to multiple addresses?' when user has added more than 1 item to the cart  from Checkout Shipping page in Application as a guest user");
	$t.start();
	try
	{
		//adding item to cart related to suits category
		navigateToCart($Shipping_Page[0][1],1);	
		//adding item to cart related to another category
		navigateToCart($Shipping_Page[1][1],1);	
		//navigate to shipping page
		_click($CART_CHECKOUT_BUTTON_BOTTOM);
		_click($SHIPPING_GUEST);
		//Ship to multiple addresses section should be displayed
		_assertContainsText($Shipping_Page[4][2], $SHIPPING_MULTIPLE_ADDRESS);
		//yes button
		_assertVisible($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();


	var $t = _testcase("128332","Verify the navigation of 'Yes' button/verify the is this a gift section on Do you want to ship to multiple addresses option from Checkout Shipping page in Application as a guest user");
	$t.start();
	try
	{
		//click on YES button on Do you want to ship to multiple addresses
		_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
		//verify the functionality
		_assertVisible($SHIPPING_SINGLE_ADDRESS_BUTTON);
		_assertEqual("/"+$Shipping_Page[0][2]+"/", _getText($SHIPPING_SINGLE_ADDRESS_TEXT));
		//verify the line items
		_assertVisible($SHIPPING_CART_ITEMS);
		_assertVisible($MULTISHIPPING_LINE_ITEM_FIRST);
		_assertVisible($MULTISHIPPING_LINE_ITEM_SECOND);
		var $addrCount=0;

		//adding addresses
		for(var $i=0;$i<$Address.length;$i++)
		{
			_click(_span("Add/Edit Address["+$i+"]"));
			addAddressMultiShip($Address,$i);
			$addrCount++;
		}

		//select addresses from drop down 
		var $dropDowns=_count("_select","/dwfrm_multishipping_addressSelection_quantityLineItems/",_in(_table("item-list")));
		var $c1=1;
		var $j=1;
		for(var $i=0;$i<$dropDowns;$i++)
		{
			if($c1>$addrCount)
			{
				$c1=1;
			}
			_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"),$j);
			$j++;
		}
		//navigate to shipping method page
		_click($SHIPPING_MULTIPLE_ADDRESS_SAVE_BUTTON);

		var $count=_count("_table","/item-list/",_in(_div("checkoutmultishipping")));
		for(var $i=0;$i<$count;$i++)
		{
			//verify the 'is this a gift' section 
			_assertVisible(_label($Shipping_Page[0][8]+"["+$i+"]"));
			//by default no should select
			_assertVisible(_radio("false["+$i+"]"));
			_assert(_radio("false["+$i+"]").checked);
			//selecting yes button
			_click(_radio("true["+$i+"]"));
			//verifying the message text field
			_assertVisible(_label($Shipping_Page[1][8]+"["+$i+"]"));
			_assertVisible(_textarea("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMessage"));
			//selecting no radio button
			_click(_radio("false["+$i+"]"));
			//verify the display of message box
			_assertEqual(false,_isVisible($SHIPPING_GIFT_DIV+"["+$i+"]"));
			//message field validation
			_click(_radio("true["+$i+"]"));
			//verifying the message text field
			_assertVisible(_label($Shipping_Page[1][8]));
			_assertVisible(_textarea("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMessage"));
			//enter more than 150 characters in textbox and verify the max length
			_setValue(_textarea("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMessage"),$Shipping_Page[2][8]);
			_assertEqual($Shipping_Page[3][8],_getText(_textarea("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMessage")).length);
			_assertVisible($SHIPPING_MESSAGE_TEXTAREA_STATICTEXT);
			_assertVisible($SHIPPING_MESSAGE_TEXTAREA_STATICTEXT_DIV);
		}
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();


	var $t = _testcase("128336","Verify the navigation of 'product name' in 'Do you want to ship to multiple addresses?' Section on shipping page in Prevail application as a Anonymous user");
	$t.start();
	try
	{

		//fetch the product name
		var $productName=_getText(_link("/(.*)/",_in(_div("name"))));
		//click on name link
		_click($SHIPPING_PRODUCT_NAME);
		//verify the navigation to pdp page
		if(!isMobile())
		{
			_assertVisible($SHIPPING_BREADCRUMB);
		}
		_assertContainsText($productName, $SHIPPING_BREADCRUMB);
		_assertVisible($PDP_PRODUCTNAME);
		_assertEqual($productName, _getText($PDP_PRODUCTNAME));
		//navigate to shipping page
		_click($MINICART_OVERLAY_VIEWCART_LINK);
		_click($CART_CHECKOUT_BUTTON_BOTTOM);
		_click($SHIPPING_GUEST); 
		//click on yes button to reset back
		//_click($SHIPPING_SINGLE_ADDRESS_BUTTON);
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();

	ClearCartItems();
	var $t = _testcase("124476/148946","Verify the functionality of the Checkbox 'Use this address for Billing' on shipping page in Prevail application as a guest user");
	$t.start();
	try
	{
		navigateToShippingPage($Shipping_Page[0][1],1);
		//entering the shipping address
		shippingAddress($Shipping_addr,0);
		//check the use this address for billing checkbox
		_check($SHIPPING_BILLING_ADDRESS_CHECKBOX);
		_click($SHIPPING_SUBMIT_BUTTON);
		//address verification overlay
		//addressVerificationOverlay();
		//verify the address prepopulation in billing page
		//first name
		_assertEqual($Shipping_addr[0][1],_getText($BILLING_FN_TEXTBOX));
		//last name
		_assertEqual($Shipping_addr[0][2],_getText($BILLING_LN_TEXTBOX));
		//address1
		_assertEqual($Shipping_addr[0][3],_getText($BILLING_ADDRESS1_TEXTBOX));
		//country
		_assertEqual($Shipping_addr[0][5],_getSelectedText($BILLING_COUNTRY_DROPDOWN));
		//State
		_assertEqual($Shipping_addr[0][6],_getSelectedText($BILLING_STATE_DROPDOWN));
		//City
		_assertEqual($Shipping_addr[0][7],_getText($BILLING_CITY_TEXTBOX));
		//Zipcode
		_assertEqual($Shipping_addr[0][8],_getText($BILLING_ZIPCODE_TEXTBOX));
		//Phone number
		_assertEqual($Shipping_addr[0][9],_getText($BILLING_NUMBER_TEXTBOX));
		//navigating back to shipping page
		_click($BILLING_SHIPPING_BUTTON);
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();

	var $t = _testcase("128333","Verify the functionality of 'Is this a gift?:' radio buttons on shipping page in Prevail application as a guest user");
	$t.start();
	try
	{
		//verifying the is this gift order
		_assertVisible($SHIPPING_GIFT_TEXT);

		//by default no should be selected
		_assert(_radio("false").checked);
		//selecting yes button
		_click(_radio("true"));
		//verifying the message text field
		_assertVisible($SHIPPING_MESSAGE);
		_assertVisible($SHIPPING_MESSAGE_TEXTAREA);
		//selecting no radio button
		_click(_radio("false"));
		//verify the display of message box
		_assertEqual(false,_isVisible($SHIPPING_GIFT_DIV));
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();

	var $t = _testcase("148947","Verify the validation of 'Message' field on  shipping page in Prevail application as a guest user");
	$t.start();
	try
	{
		//selecting is this gift yes button
		//selecting yes button
		_click(_radio("true"));
		//verifying the message text field
		_assertVisible($SHIPPING_MESSAGE);
		_assertVisible($SHIPPING_MESSAGE_TEXTAREA);
		//enter more than 250 characters in textbox and verify the max length
		_setValue($SHIPPING_MESSAGE_TEXTAREA,$Shipping_Page[2][8]);
		//Verify that no more than 250 characters are present in Message field
		_assertEqual($Shipping_Page[3][8],_getText($SHIPPING_MESSAGE_TEXTAREA).length);
		//No of characters displayed below the field.
		_assertVisible($SHIPPING_MESSAGE_TEXTAREA_STATICTEXT);
		_assertVisible($SHIPPING_MESSAGE_TEXTAREA_STATICTEXT_DIV);			
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();


	var $t = _testcase("148948","Verify the UI of 'SELECT SHIPPING METHOD' Section on shipping page in Prevail application as a guest user");
	$t.start();
	try
	{
		//verifying the UI of Shipping section
		_assertVisible($SHIPPING_METHOD_HEADING);
		var $totalToolTips=_count("_link","tooltip",_in($SHIPPING_METHOD_HEADING));
		var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in($SHIPPING_METHOD_HEADING));
		_assertEqual($totalShippingMethods,$totalToolTips);
		for(var $i=0;$i<$totalShippingMethods;$i++)
		{
			//shipping methods
			_assertVisible(_div("form-row form-indent label-inline["+$i+"]"));
			_assertVisible(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
			
			//mouse overing on the tooltips
			_mouseOver(_link("tooltip",_in(_div("form-row form-indent label-inline["+$i+"]"))));
			//verifying the display of tool tip
			_assertVisible($SHIPPING_METHOD_TOOLTIP);
		}
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();

	var $t = _testcase("148949","Verify the functionality of radio buttons under 'SELECT SHIPPING METHOD' Section on shipping page in Prevail application as a guest user");
	$t.start();
	try
	{

		//verify the functionality of radio buttons
		var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in($SHIPPING_METHOD_HEADING));
		var $count=0;
		for(var $i=0;$i<$totalShippingMethods;$i++)
		{
			//selecting radio buttons
			_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
			//verify the selection
			_assert(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked);
			//uncheck the checkbox
			_uncheck(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
			//verify the deselection
			_assert(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked);
			for(var $j=0;$j<$totalShippingMethods;$j++)
			{
				if(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked)
				{
					$count++;
				}
			}
		}	

	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();


	var $t = _testcase("128340","Verify the functionality of 'EDIT' link in order summary Section in right nav on shipping page in Prevail application as a guest user");
	$t.start();
	try
	{
		//navigating to shipping page
		if(!isMobile() || mobile.iPad())
		{
			var $productName=_getText($SHIPPING_SUMMARY_PRODUCT_NAME);
			//clicking on edit link present in the order Summuary section
			_click($SHIPPING_SUMMARY_EDIT_DESKTOP);
		}
		else
		{
			var $productName=_getText($SHIPPING_SUMMARY_PRODUCT_NAME);
			//clicking on edit link present in the order Summary section
			_click($SHIPPING_SUMMARY_EDIT_MOBILE);
		}
		//cart navigation verification
		_assertVisible($CART_TABLE);
		_assertVisible($CART_ACTIONBAR);
		_assertVisible($CART_PRODUCT_NAME);
		_assertEqual($productName,_getText($CART_PRODUCT_NAME));
		//fetch the price in cart
		if(_isVisible($CART_LINEITEM_PRICE))
		{
			$CItemSalesPrice=_extract(_getText($CART_LINEITEM_PRICE),"/[$](.*)/",true).toString();
        }

		else if(_isVisible($CART_LINEITEM_DISCOUNT_PRICE))
        {
			$CItemSalesPrice=_extract(_getText($CART_LINEITEM_DISCOUNT_PRICE),"/[$](.*)/",true).toString();
        }
		//updating the quantity
		_setValue($CART_QTY_DROPDOWN, _in($CART_ROW),$Shipping_Page[5][5]);
		//click on update cart
		_click($CART_UPDATE_BUTTON);
		_wait(5000,_isVisible($CART_CHECKOUT_BUTTON_BOTTOM));
		//navigate to shipping page
		_click($CART_CHECKOUT_BUTTON_BOTTOM);
		_click($SHIPPING_GUEST); 
		//verify the cart updation
		//quantity
		_assertEqual($Shipping_Page[5][5],_getText($SHIPPING_RIGHT_NAV_VALUE));
		//price
		var $ExpPrice=$Shipping_Page[5][5]*$CItemSalesPrice;
		//Price in the order summary section
		_assertEqual($ExpPrice,_getText(SHIPPING_RIGHT_NAV_PRICE));
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();

	var $t = _testcase("128341","Verify the navigation of 'product name' in order summary Section in right nav on shipping page in Prevail application as a guest user");
	$t.start();
	try
	{
		//clicking on product name in shipping page
		if(!isMobile() || mobile.iPad())
		{
			var $productName=_getText($MINICART_PRODUCT_NAME_LINK_DESKTOP);
			_click($MINICART_PRODUCT_NAME_LINK_DESKTOP);
		}
		else
		{
			var $productName=_getText($MINICART_PRODUCT_NAME_LINK_MOBILE);
			_click($MINICART_PRODUCT_NAME_LINK_MOBILE);
		}

		//verify the navigation to respective PD Page
		_assertVisible($SHIPPING_BREADCRUMB);
		if(!isMobile())
		{
			_assertVisible($SHIPPING_BREADCRUMB_TEXT);
		}
		_assertEqual($productName, _getText($SHIPPING_BREADCRUMB_TEXT));
		_assertVisible($PDP_PRODUCTNAME);
		_assertEqual($productName, _getText($PDP_PRODUCTNAME));
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();


//	navigate to shipping page
	navigateToShippingPage($Shipping_Page[0][1],1);

	if(!isMobile())
	{
		var $t = _testcase("128342/289220/289218","Verify the mouse hover link functionality for 'APO/FPO and Why is this required?' on shipping page in Prevail application as a guest user");
		$t.start();
		try
		{
			//mouse hover on apo/Fpo tool tip
			_mouseOver($SHIPPING_ADDRESS_TOOLTIP_LINK);
			//verifying the display of tool tip
			if(_getAttribute($SHIPPING_ADDRESS_TOOLTIP,"aria-describedby")!=null)
			{
				_assert(true);
				//Text inside the tooltip
				_assertEqual($Shipping_Page[12][0], _getText($SHIPPING_ADDRESS_TOOLTIP));
			}
			else
			{
				_assert(false);
			}

			//mouse hovering on why is this required tool tip
			_mouseOver($SHIPPING_PHONE_TOOLTIP_LINK);
			//verifying the display of tool tip
			if(_getAttribute($SHIPPING_PHONE_TOOLTIP,"aria-describedby")!=null)
			{
				_assert(true);
				//text inside the tooltip
				_assertEqual($Shipping_Page[13][0], _getText($SHIPPING_PHONE_TOOLTIP));
			}
			else
			{
				_assert(false);
			}
		}
		catch($e)
		{
			_logExceptionAsFailure($e);
		}	
		$t.end();
	}


//	navigate to shipping page
	navigateToShippingPage($Shipping_Page[0][1],1);
	var $t = _testcase("128344","Verify the functionality of shipping charge and country selected on shipping page in Prevail application as a guest user");
	$t.start();
	try
	{
		//changing the shipping method 
		var $totalradio=_count("_radio","/dwfrm_singleshipping_shippingAddress_shippingMethodID/");
		if($totalradio!=0)
		{
			for(var $i=0;$i<$totalradio;$i++)
			{
				_click(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
				//verify wehter added in order summuary section or not
				_assertVisible(_label("/"+$Shipping_Page[$i][10]+"/"));
			}
		}
		else
		{
			_log(false,"Need to configure Shipping Methods");
		}
		/*
	//selecting the Country as Canada
	_setSelected($SHIPPING_COUNTRY_DROPDOWN,$Shipping_Page[3][4]);
	//verifying the state field
	if(isMobile())
		{
		_assertVisible(_label("/"+$Shipping_Page[5][6]+"/"));
		}
	else
		{
	_assertVisible(_label("/"+$Shipping_Page[5][6]+"/",_leftOf($SHIPPING_STATE_DROPDOWN)));
		}*/

	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();

	var $t = _testcase("128343","Verify the display of 'Details' tool tip in select shipping option Section on shipping page in Prevail application as a guest user");
	$t.start();
	try
	{
		//verifying the tooltips
		var $totalToolTips=_count("_link","tooltip",_in($SHIPPING_METHOD_HEADING));
		var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in($SHIPPING_METHOD_HEADING));
		_assertEqual($totalShippingMethods,$totalToolTips);
		for(var $i=0;$i<$totalShippingMethods;$i++)
		{
			//mousehovering on the tooltips
			_mouseOver(_link("tooltip",_in(_div("form-row form-indent label-inline["+$i+"]"))));
			//verifying the display of tool tip
			if(_getAttribute(_link("tooltip",_in(_div("form-row form-indent label-inline["+$i+"]"))),"aria-describedby")!=null)
			{
				_assert(true);
			}
			else
			{
				_assert(false);
			}
		}	
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();

	var $t=_testcase("148950","Verify the functionality of 'CONTINUE >' button on shipping page in Prevail application as a Anonymous user");
	$t.start();
	try{

		//enter empty data in all optional fields
		_setValue($SHIPPING_FN_TEXTBOX, "");
		_setValue($SHIPPING_LN_TEXTBOX, "");
		_setValue($SHIPPING_ADDRESS1_TEXTBOX,"");
		_setSelected($SHIPPING_COUNTRY_DROPDOWN,"");
		_setSelected($SHIPPING_STATE_DROPDOWN, "");
		_setValue($SHIPPING_CITY_TEXTBOX, "");
		_setValue($SHIPPING_ZIPCODE_TEXTBOX, ""); 
		_setValue($SHIPPING_PHONE_TEXTBOX,"");
		_setValue($SHIPPING_ADDRESS2_TEXTBOX,$Validation[4][4]);
		//click on continue button
		_click($SHIPPING_SUBMIT_BUTTON);
		//verify the display
		_assert($SHIPPING_SUBMIT_BUTTON.disabled); 
		//enter all required data except optional data
		shippingAddress($Shipping_addr,0);
		//click on continue button
		_click($SHIPPING_SUBMIT_BUTTON);
		//address verification overlay
		addressVerificationOverlay();
		//verify the navigation to billing page
		_assertVisible($BILLING_PAGE_HEADING);
		_assertVisible($BILLING_SUBMIT_BUTTON);

	}catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();


	ClearCartItems(); 
	var $t = _testcase("148951","Verify the calculation functionality in Order Summary pane on shipping page in Prevail application as a Registered user");
	$t.start();
	try
	{
		//navigating to Shipping page
		navigateToShippingPage($Shipping_Page[0][1],1);
		//verifying the order summury pane
		if(!isMobile() || mobile.iPad())
		{
			_assertVisible(_div($CProductName,_in($SHIPPING_RIGHT_NAV)));
			_assertVisible($SHIPPING_RIGHT_NAV_QUANTITY);
			_assertEqual($CQuantity, _getText($SHIPPING_RIGHT_NAV_VALUE));
		}
		else
		{
			_assertVisible(_div($CProductName,_in($SHIPPING_RIGHT_NAV)));
			_assertVisible($SHIPPING_RIGHT_NAV_QUANTITY);
			_assertEqual($CQuantity, _getText($SHIPPING_RIGHT_NAV_VALUE));
		}
		_assertEqual($cOrderSubtotal,_extract(_getText($SHIPPING_RIGHT_NAV_SUBTOTAL),"/[$](.*)/",true).toString());
		if(isMobile() && !mobile.iPad())
		{
			_assertEqual($CShippingcharge,_extract(_getText($SHIPPING_RIGHT_NAV_SHIPPING_COST),"/[$](.*)/",true).toString());
		}
		else
		{
			_assertEqual($CShippingcharge,_extract(_getText($SHIPPING_RIGHT_NAV_SHIPPING_COST),"/[$](.*)/",true).toString());
		}
		//_assertEqual($tax,_extract(_getText($SHIPPING_RIGHT_NAV_SALES_TAX),"/[$](.*)/",true).toString());
		_assertEqual($COrderTotal,_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/[$](.*)/",true).toString());
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();

	var $t = _testcase("148952/148953","Verify the functionality of 'CONTINUE>' button/verify the contents of verify your shipping address page on shipping page in Prevail application as a guest user");
	$t.start();
	try
	{
		//Click on shipping guest
		_click($SHIPPING_GUEST);
		//enter the shipping address
		shippingAddress($Shipping_addr,0);	
		//click on continue button
		_click($SHIPPING_SUBMIT_BUTTON);
		//UPS verification
		if($AddrVerification==$BMConfig[0][2])
		{
			//verify the UI contents of dialog box
			_assertVisible($SHIPPING_ADDRESS_VERIFICATION_HEADING);
			_assertVisible($SHIPPING_ADDRESS_VERIFICATION);
			_assertVisible($SHIPPING_ADDRESS_VERIFICATION_PARAGRAPH);
			//original address
			_assertVisible($SHIPPING_ADDRESS_VERIFICATION_ORGINAL_ADDRESS);
			_assertVisible($SHIPPING_ADDRESS_VERIFICATION_ORGINAL_ADDRESS_BLOCK);
			_assertVisible($SHIPPING_ADDRESS_VERIFICATION_ORGINAL_ADDRESS_EDIT);
			_assertVisible($SHIPPING_ADDRESS_VERIFICATION_ORGINAL_ADDRESS_SUBMIT);
			//suggested address
			_assertVisible($SHIPPING_ADDRESS_VERIFICATION_SUGGESTED_ADDRESS);
			_assertVisible($SHIPPING_ADDRESS_VERIFICATION_SUGGESTED_ADDRESS_BLOCK);
			_assertVisible($SHIPPING_ADDRESS_VERIFICATION_SUGGESTED_ADDRESS_EDIT);
			_assertVisible($SHIPPING_ADDRESS_VERIFICATION_SUGGESTED_ADDRESS_SUBMIT);

			//click on link edit address
			_click($SHIPPING_ADDRESS_VERIFICATION_SUGGESTED_ADDRESS_EDIT);
			//again click on continue
			_click($SHIPPING_SUBMIT_BUTTON);
			if(_isVisible($SHIPPING_ADDRESS_VERIFICATION))
			{
				_click($SHIPPING_ADDRESS_VERIFICATION_ORGINAL_ADDRESS_SUBMIT);
			}
			//directly should navigate to billing page
			_assertVisible($BILLING_PAGE_HEADING);
			_assertVisible($BILLING_SUBMIT_BUTTON);
		}
		else
		{
			_log("Enable the UPS in BM");
		}
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();


	SiteURLs();
	ClearCartItems(); 
//	navigating to Shipping page
	navigateToShippingPage($Shipping_Page[0][1],1);

	var $t = _testcase("155514/155586","Verify the application behavior when country changes from United States to Germany or Canada./validation the validation for country and state fields");
	$t.start();
	try
	{
		_setSelected($SHIPPING_COUNTRY_DROPDOWN, $Shipping_Page[0][12]);
		_assertVisible($SHIPPING_STATE_TEXT);
		_assertVisible($SHIPPING_ZIPCODE_TEXT);
		_assertNotVisible($SHIPPING_OTHER_TEXT);
		_assertNotVisible($SHIPPING_POSTAL_CODE_TEXT);

		_assertEqual($Shipping_Page[0][13],_getText($SHIPPING_STATE_DROPDOWN).toString());
		/*
	_setSelected($SHIPPING_COUNTRY_DROPDOWN, $Shipping_Page[1][12]);
	_assertVisible($SHIPPING_OTHER_TEXT);
	_assertVisible($SHIPPING_POSTAL_CODE_TEXT);
	_assertNotVisible($SHIPPING_STATE_TEXT);
	_assertNotVisible($SHIPPING_ZIPCODE_TEXT);

	_assertEqual($Shipping_Page[1][13],_getText($SHIPPING_STATE_DROPDOWN));

	_setSelected($SHIPPING_COUNTRY_DROPDOWN, $Shipping_Page[2][12]);
	_assertVisible($SHIPPING_PROVINCE_TEXT);
	_assertVisible($SHIPPING_POSTAL_CODE_TEXT);
	_assertNotVisible($SHIPPING_STATE_TEXT);
	_assertNotVisible($SHIPPING_ZIPCODE_TEXT);
	_assertNotVisible($SHIPPING_OTHER_TEXT);

	_assertEqual($Shipping_Page[2][13],_getText($SHIPPING_STATE_DROPDOWN));
		 */
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();


	cleanup();



	ClearCartItems();
	var $t = _testcase("128334/128335/148938/148939/148940/148941/148943/148944/148945","Verify the field validations for 'First Name'/last name/address1/address2/city/zip code/phone number text box on shipping page in Application as a guest user");
	$t.start();
	try
	{
		//add items to cart
		navigateToCart($Shipping_Page[0][1],1);
		_click($CART_CHECKOUT_BUTTON_BOTTOM);	
		_click($SHIPPING_GUEST); 
		//verify the navigation
		_assertVisible($SHIPPING_PAGE_HEADING);
		_assertVisible($SHIPPING_PAGE_HEADER);
		//validating the fields
		for(var $i=0;$i<$Validation.length;$i++)
		{
			if($i==0)
			{
				_setValue($SHIPPING_FN_TEXTBOX,"");
				_setValue($SHIPPING_LN_TEXTBOX,"");
				_setValue($SHIPPING_ADDRESS1_TEXTBOX,"");
				_setValue($SHIPPING_ADDRESS2_TEXTBOX,"");
				_setSelected($SHIPPING_COUNTRY_DROPDOWN,"");
				_setSelected($SHIPPING_STATE_DROPDOWN, "");
				_setValue($SHIPPING_CITY_TEXTBOX,"");
				_setValue($SHIPPING_ZIPCODE_TEXTBOX, ""); 
				_setValue($SHIPPING_PHONE_TEXTBOX,"");
			}
			else
			{
				_setValue($SHIPPING_FN_TEXTBOX, $Validation[$i][1]);
				_setValue($SHIPPING_LN_TEXTBOX, $Validation[$i][2]);
				_setValue($SHIPPING_ADDRESS1_TEXTBOX, $Validation[$i][3]);
				_setValue($SHIPPING_ADDRESS2_TEXTBOX,$Validation[$i][4]);
				_setSelected($SHIPPING_COUNTRY_DROPDOWN,$Validation[$i][5]);
				_setSelected($SHIPPING_STATE_DROPDOWN, $Validation[$i][6]);
				_setValue($SHIPPING_CITY_TEXTBOX, $Validation[$i][7]);
				_setValue($SHIPPING_ZIPCODE_TEXTBOX, $Validation[$i][8]); 
				_setValue($SHIPPING_PHONE_TEXTBOX,$Validation[$i][9]);
			}
			//click on continue button
			_click($SHIPPING_SUBMIT_BUTTON);
			//with blank data
			if($i==0)
			{
				_assert($SHIPPING_SUBMIT_BUTTON.disabled);
			}
			//checking the max length
			if($i==1)
			{
				//Fname,Lname Should not highlight
				_assertNotEqual($Validation[0][10],_style($SHIPPING_FN_TEXTBOX,"background-color"));
				_assertNotEqual($Validation[0][10],_style($SHIPPING_LN_TEXTBOX,"background-color"));
				//for address label verification
				_assertVisible(_label("/"+$Shipping_Page[8][0]+"/"));
				_assertVisible(_label("/"+$Shipping_Page[9][0]+"/"));
				//verifying the length of field's first name,last name,add1,addr2,phone number,City 
				_assertEqual($Validation[1][10],_getText($SHIPPING_FN_TEXTBOX).length);
				_assertEqual($Validation[1][10],_getText($SHIPPING_LN_TEXTBOX).length);
				_assertEqual($Validation[1][10],_getText($SHIPPING_ADDRESS1_TEXTBOX).length);
				_assertEqual($Validation[1][10],_getText($SHIPPING_ADDRESS2_TEXTBOX).length);
				_assertEqual($Validation[1][10],_getText($SHIPPING_CITY_TEXTBOX).length);
				_assertEqual($Validation[0][11],_getText($SHIPPING_PHONE_TEXTBOX).length);
				//Zip Code max length
				_assertEqual($Validation[1][11],_getText($SHIPPING_ZIPCODE_TEXTBOX).length); 
			}
			//numeric,special chars,combination of both,alphabets data in city,phone number,Zip code field's
			if($i==2 || $i==3 || $i==4 || $i==5)
			{
				//Zip Code
				_assertEqual($Validation[6][10],_style($SHIPPING_ZIPCODE_TEXTBOX,"background-color")); 
				_assertEqual($Validation[4][10],_style(_span($Validation[2][11]),"color"));
				_assertVisible(_span($Validation[2][11])); 
				//Phone Number
				_assertEqual($Validation[6][10],_style($SHIPPING_PHONE_TEXTBOX,"background-color"));
				_assertEqual($Validation[4][10],_style(_span($Validation[2][10]),"color"));
				_assertVisible(_span($Validation[2][10]));
				//Continue button should be disabled
				_assert($SHIPPING_SUBMIT_BUTTON.disabled);
			}
			//state field validation
			if($i==6)
			{
				//Erro msg
//				_assertVisible(_span($Validation[7][10]));
//				//Background color
//				_assertEqual($Validation[6][10],_style(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"),"background-color"));
//				//border color
//				_assertEqual($Validation[4][10],_style(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"),"border-top-color"));
				_assert($SHIPPING_SUBMIT_BUTTON.disabled);
			}
			//with valid data
			if($i==7)
			{
				//address verification overlay
				addressVerificationOverlay();
				//Should navigate to billing page
				_assertVisible($BILLING_PAGE_HEADING);
				_assertVisible($BILLING_SUBMIT_BUTTON);
			}
		}
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();


	var $t = _testcase("148942","Verify the validation of 'Country' field on shipping page in Prevail application as a guest user");
	$t.start();
	try
	{
		//navigating back to shipping page
		_click($SHIPPING_PAGE_HEADING); 	
		//selecting US from drop down
		var $countries=_getText($SHIPPING_COUNTRY_DROPDOWN);
		for(var $i=0;$i<$countries.length;$i++)
		{
			_assertEqual($countries[$i],$Shipping_Page[$i][4]);
			//selecting US from drop down
			if($i==1)
			{
				_setSelected($SHIPPING_COUNTRY_DROPDOWN,$Shipping_Page[$i][4]);
				//verify the states present in states drop down
				for(var $j=0;$j<$Shipping_Page[5][5];$j++)
				{
					_setSelected($SHIPPING_STATE_DROPDOWN,$Shipping_Page[$j][5]);
					//verifying the selection
					_assertEqual($Shipping_Page[$j][5], _getSelectedText($SHIPPING_STATE_DROPDOWN));
				}
			}

			//select the Germany from drop down
			if($i==2)
			{
				_setSelected($SHIPPING_COUNTRY_DROPDOWN,$Shipping_Page[$i][4]);
				//verify the label changed to other or not
				_assertVisible($SHIPPING_STATE_OTHER);
				for(var $j=0;$j<$Shipping_Page[2][7];$j++)
				{
					_setSelected($SHIPPING_STATE_DROPDOWN,$Shipping_Page[1][7]);
					//verifying the selection
					_assertEqual($Shipping_Page[1][7], _getSelectedText($SHIPPING_STATE_DROPDOWN));
				}
			}

			//select the canada from drop down
			if($i==3)
			{
				_setSelected($SHIPPING_COUNTRY_DROPDOWN,$Shipping_Page[$i][4]);
				//verify the label changed to province or not
				_assertVisible($SHIPPING_PROVINCE_TEXT);
				for(var $j=0;$j<$Shipping_Page[5][5];$j++)
				{
					_setSelected($SHIPPING_STATE_DROPDOWN,$Shipping_Page[$j][6]);
					//verifying the selection
					_assertEqual($Shipping_Page[$j][6], _getSelectedText($SHIPPING_STATE_DROPDOWN));
				}
			}
		}
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();
	
	var $t=_testcase("289786","Verify if the user is able to expand the Order Review accordion when in SPC shipping page as a guest user.");
	$t.start();
	try
	{
		//Click on order review
		_click($ORDER_REVIEW_HEADING);
		//Order review section should be opened
		_assertNotVisible($ORDER_REVIEW_ITEM_ROW);
		//should be in shipping page
		_assertVisible($SHIPPING_PAGE_HEADER);
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();
	
	var $t=_testcase("289784","Verify if the user is able to expand the Payment accordion when in SPC shipping page as a guest user.");
	$t.start();
	try
	{
		//Click on payment header
		_assertVisible($BILLING_PAYMENT_HEADER);
		//Payment section should not be opened
		_assertNotVisible($BILLING_PAGE_HEADING);
		//should be in shipping page
		_assertVisible($SHIPPING_PAGE_HEADER);
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();
	
	var $t=_testcase("289178","Verify the UI of checkout 'SHIPPING' page in Application as a Anonymous user.");
	$t.start();
	try
	{
		//sign in section
		_assertVisible($CHECKOUT_SIGN_IN_SECTION);
		//shipping section
		_assertVisible($CHECKOUT_SHIPPING_SECTION);
		//Header
		_assertVisible($SHIPPING_PAGE_HEADER);
		_assertVisible($CHECKOUT_SHIPPING_CONTENT);
		//Shipping method
		_assertVisible($SHIPPING_METHOD_HEADING);
		_assertVisible($SHIPPING_METHOD_LIST);
		//Payment
		_assertVisible($CHECKOUT_PAYMENT_SECTION);
		//order review
		_assertVisible($CHECKOUT_ORDER_REVIEW_SECTION);
		//Help section
		_assertVisible($CHECKOUT_HELP_SECTION);
		//order summary
		_assertVisible($CHECKOUT_ORDER_SUMMARY_SECTION);
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();
	
	var $t=_testcase("289690/289671","Verify the UI of the Sign in section when expanded from shipping page/Sign in accordion is in active state when user is in shipping page as a guest user.");
	$t.start();
	try
	{
		//Click on Sign in heading
		_click($CHECKOUT_SIGN_IN_HEADING);
		//Sign in section should be opened
		_assertVisible($CHECKOUT_SIGN_IN_SECTION_OPENED);
		//Shipping section should be closed
		_assertNotVisible($SHIPPING_PAGE_HEADER);
		//Guest checkout
		_click($SHIPPING_GUEST);
		//Click on Sign in tab
		_click($CHECKOUT_SIGN_IN_SECTION);
		//Sign in section should be opened
		_assertVisible($CHECKOUT_SIGN_IN_SECTION_OPENED);
		//Shipping section should be closed
		_assertNotVisible($SHIPPING_PAGE_HEADER);
		//UI of Sign in section
		//checkout as guest section
		_assertVisible($SHIPPING_GUEST_SECTION);
		//Create account section
		_assertVisible($CHECKOUT_SIGN_IN_CREATE_ACC_SECTION);
		//Log in section
		_assertVisible($CHECKOUT_LOGIN_SECTION);
		//Social login section
		_assertVisible($CHECKOUT_SOCIAL_LOGIN_SECTION);
		//Help section
		_assertVisible($CHECKOUT_HELP_SECTION);
		//order summary
		_assertVisible($CHECKOUT_ORDER_SUMMARY_SECTION);
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();
//}