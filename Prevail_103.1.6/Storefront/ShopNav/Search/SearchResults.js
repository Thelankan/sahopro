_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Search_Data.xls");

SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();

var $t = _testcase("124686/125409", "Verify the navigation to search result page from home page and UI of search result page  in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic_SearchResult[0][0]);
	//bread crumb
	_assertVisible($SEARCHRESULT_BREADCRUMB_TEXT);
	_assertEqual($Generic_SearchResult[0][2]+" \""+$Generic_SearchResult[0][0]+"\"", _getText($SEARCHRESULT_BREADCRUMB_TEXT));
	//sort by
	_assertVisible($SEARCHRESULT_SORTDROPDOWN_HEADER);
	_assertVisible($SEARCHRESULT_SORTDROPDOWN_FOOTER);
	//refinement header
	_assertVisible($SEARCHRESULT_REFINEMENT_HEADING);
	//left nav
	_assertVisible($SEARCHRESULT_LEFTNAV_REFINEMENTS);
	//Pagination
	_assertVisible($SEARCHRESULT_PAGINATION_HEADER);
	_assertVisible($SEARCHRESULT_PAGINATION_FOOTER);
	//Items displayed
	_assertExists($SEARCHRESULT_SEARCHRESULT_ITEMS);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125413", "Verify the navigation related to 'Home' link on search result page breadcrumb  in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic_SearchResult[0][0]);
	//bread crumb
	_assertVisible($SEARCHRESULT_BREADCRUMB_TEXT);
	_assertEqual($Generic_SearchResult[0][2]+" \""+$Generic_SearchResult[0][0]+"\"", _getText($SEARCHRESULT_BREADCRUMB_TEXT));
	//navigation
	_assertNotVisible($SEARCHRESULT_HOMELINK_WITH_BREADCRUMB);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125410", "Verify the navigation related to 'Keyword' link on search result page breadcrumb  in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic_SearchResult[0][0]);
	//bread crumb
	_assertVisible($SEARCHRESULT_BREADCRUMB_TEXT);
	_assertEqual($Generic_SearchResult[0][2]+" \""+$Generic_SearchResult[0][0]+"\"", _getText($SEARCHRESULT_BREADCRUMB_TEXT));
	//Click on search key word
	_click(_link($Generic_SearchResult[0][0], _in($SEARCHRESULT_BREADCRUMB_TEXT)));
	//bread crumb
	_assertVisible($SEARCHRESULT_BREADCRUMB_TEXT);
	_assertEqual($Generic_SearchResult[0][2]+" \""+$Generic_SearchResult[0][0]+"\"", _getText($SEARCHRESULT_BREADCRUMB_TEXT));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125415", "Verify the UI/functionality of 'REFINE SEARCH' on search result page  left nav in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic_SearchResult[0][0]);
	var $Filters=NunberOfRefinementAttributes();
	for(var $i=0;$i<$Filters.length;$i++)
		{	
			//click on filter
			_click(_heading3($Filters[$i]));
			
			if ($Filters[$i]=="Category")
				{
				_assertNotVisible($SEARCHRESULT_CATEGORY_REFINEMENT_LEFTNAV);
				//click on filter
				_click(_heading3($Filters[$i]));
				//Filter Should Collapse
				_assertVisible($SEARCHRESULT_CATEGORY_REFINEMENT_LEFTNAV);
				
				}
			else if ($Filters[$i]=="Color")
				{
				_assertNotVisible($SEARCHRESULT_COLOR_REFINEMENT_LEFTNAV);
				//click on filter
				_click(_heading3($Filters[$i]));
				//Filter Should Collapse
				_assertVisible($SEARCHRESULT_COLOR_REFINEMENT_LEFTNAV);
				}
			else if ($Filters[$i]=="Price")
				{
				 _assertNotVisible($SEARCHRESULT_PRICE_REFINEMENT_LEFTNAV);
				//click on filter
				_click(_heading3($Filters[$i]));
				//Filter Should Collapse
				 _assertVisible($SEARCHRESULT_PRICE_REFINEMENT_LEFTNAV);
				}
			else
				{
				 _assertNotVisible(($SEARCHRESULT_REFINEMENT_NEW_LEFTNAV));
				//click on filter
				_click(_heading3($Filters[$i]));
				_assertVisible($SEARCHRESULT_REFINEMENT_NEW_LEFTNAV);
				}
			
		
		}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125416", "Verify the functionality related to  'Category' section on search result page left nav in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic_SearchResult[0][0]);
	_setAccessorIgnoreCase(true);
	//navigation of sub-category link.
	var $categories=LeftNavCategoryAttributes();
	for($i=1;$i<$categories.length;$i++)
	{
	_click(_link($categories[$i], _in($SEARCHRESULT_CATEGORY_REFINEMENTS)));
	_assertContainsText($categories[$i], $SEARCHRESULT_PAGE_BREADCRUMB);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125412", "Verify the functionality related to  'Display icon' on search result page in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic_SearchResult[0][0]);
	_assertVisible($SEARCHRESULT_SEARCHRESULT_CONTENT);
	_click($SEARCHRESULT_TOOGLE_GRID);
	_assertVisible(_byClassName("search-result-content wide-tiles", "DIV"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125417", "Verify the system's response on selection on color filter from left nav in the application both as an anonymous and a registered user");
$t.start();
try
{
	search($Generic_SearchResult[0][0]);
	var $colors = LeftNavColorAttributes();
	var $unSelectable=UnSelectableSwatchesinColorRefinement();
	for(var $k=0; $k<$colors.length; $k++)
	{
		if($unSelectable.indexOf($colors[$k])>=0)
			{
			__assert(true, "Swatch is unselectable");
			}
		else
			{
			 _click(_link($colors[$k]));
			  //clear link
			  _assertVisible($SEARCHRESULT_LEFTNAV_COLOR_REFINEMENT_CLEAR_LINK);
			  _assertEqual($colors[$k]+" x",_getText($SEARCHRESULT_BREADCRUMB_REFINEMENT_VALUE));
			 _click($SEARCHRESULT_BREADCRUMB_VALUE_CLOSE_LINK);
			 _wait(4000,!_isVisible($SEARCHRESULT_BREADCRUMB_VALUE_CLOSE_LINK));
			}  
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}		
$t.end();

var $t = _testcase("125418", "Verify the functionality related to  multiple 'Colour' section on search result page left nav in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic_SearchResult[0][0]);
	var $colors1 = LeftNavColorAttributes();
	var $unSelectable1=UnSelectableSwatchesinColorRefinement();
	var $colorCount=0;
	var $ExpColor= new Array();
	for(var $k=0; $k<$Generic_SearchResult[0][3]; $k++)
	{
		if($unSelectable1.indexOf($colors1[$k])>=0)
			{
			__assert(true, "Swatch is unselectable");
			}
		else
			{
			 _click(_link($colors1[$k]));
			  //clear link
			  _assertVisible($SEARCHRESULT_LEFTNAV_COLOR_REFINEMENT_CLEAR_LINK);
			  $colorCount++;
			$ExpColor.push($colors1[$k]);
			_wait(2000);
			} 		
	}
	var $ActualColor=SelectableSwatchesinColorRefinement();
	_assertEqualArrays($ExpColor,$ActualColor);
}
catch($e)
{
	_logExceptionAsFailure($e);
}		
$t.end();

var $t = _testcase("125419", "Verify the functionality related to  'Price' section on search result page left nav in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic_SearchResult[0][0]);
	var $PriceRange = PriceRangesInPriceRefinement();
	for ($k=0; $k< $PriceRange.length; $k++)
	{
	_click(_link($PriceRange[$k]));
	_assertEqual($PriceRange[$k]+" x", _getText($SEARCHRESULT_BREADCRUMB_REFINEMENT_VALUE)); 	
	priceCheck($PriceRange[$k]);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125420", "Verify the functionality related to  'New Arrival section on search result page left nav in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	
	search($Generic_SearchResult[0][0]);
	//bread crumb
	_assertVisible($SEARCHRESULT_BREADCRUMB_TEXT);
	_assertEqual($Generic_SearchResult[0][2]+" \""+$Generic_SearchResult[0][0]+"\"", _getText($SEARCHRESULT_BREADCRUMB_TEXT));
	//click on new arrival link
	_click($SEARCHRESULT_REFINEMENT_NEWARRIVAL_LINK);
	//verify the bread crumb
	_assertVisible($Generic_SearchResult[0][2]+" \""+$Generic_SearchResult[0][0]+"\""+" in "+$Generic_SearchResult[1][0]);
	//click on home page image
	_click($SEARCHRESULT_HOMEIMAGE);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125422", "Verify the navigation related to product image on search result page in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic_SearchResult[0][0]);
	var $productName=_getText($SEARCHRESULT_NAMELINK);
	var $productPrice = _getText($SEARCHRESULT_PLP_SALESPRICE);
	//click on name link
	_click($SEARCHRESULT_NAMELINK);
	_assertEqual($productName,_getText($SEARCHRESULT_PDP_PRODUCTNAME));
	_assertEqual($productPrice,_getText($SEARCHRESULT_PDP_PRODUCT_PRICE));
	
	search($Generic_SearchResult[0][0]);
	var $productName=_getText($SEARCHRESULT_NAMELINK);
	var $productPrice = _getText($SEARCHRESULT_PLP_SALESPRICE);
	//click on image
	_click($SEARCHRESULT_THUMBLINK);
	_assertEqual($productName,_getText($SEARCHRESULT_PDP_PRODUCTNAME));
	_assertEqual($productPrice,_getText($SEARCHRESULT_PDP_PRODUCT_PRICE));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125411", "Verify the functionality related to 'Please Select One' drop down on search result page in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic_SearchResult[0][0]);
	if(_isVisible($SEARCHRESULT_SORTDROPDOWN_HEADER) && _isVisible($SEARCHRESULT_SORTDROPDOWN_FOOTER))
	{
		var $sort=_getText($SEARCHRESULT_SORTDROPDOWN_HEADER);
		var $j=0;
		for(var $i=1;$i<$sort.length;$i++)
		{
			_setSelected($SEARCHRESULT_SORTDROPDOWN_HEADER, $sort[$i]);
			//Validating the selection
			_assertEqual($sort[$i],_getSelectedText($SEARCHRESULT_SORTDROPDOWN_HEADER),"Selected option is displaying properly");
			_assertEqual($sort[$i],_getSelectedText($SEARCHRESULT_SORTDROPDOWN_FOOTER),"Selected option is displaying properly");
			if($sort.indexOf($Generic_SearchResult[$j][1])>-1)
			{
				_setSelected($SEARCHRESULT_SORTDROPDOWN_HEADER, $Generic_SearchResult[$j][1]);
				sortBy($Generic_SearchResult[$j][1]);
				$j++;
			}
		}
	}
	else
	{
		_assert(false, "sort by drop down is not present");
	}
	_click($SEARCHRESULT_HOMEIMAGE);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

if(!isMobile())
{
var $t=_testcase("--","Verify the navigation related to Quickview overlay In the application both as an anonymous and a registered user");
$t.start();
try
{
	search($Generic_SearchResult[0][0]);
	//fetching the product name
	var $PName=_getText($SEARCHRESULT_NAMELINK);
	//mouse over on image
	_mouseOver($SEARCHRESULT_THUMBLINK);
	//Quick view button
	_click($SEARCHRESULT_QUICKVIEWBUTTON_LINK);  
	//quick view overlay
	_assertVisible($SEARCHRESULT_QUICKVIEW_OVERLAY);
	_assertEqual($PName, _getText($SEARCHRESULT_PDP_PRODUCTNAME));
	//closing overlay
	_click($SEARCHRESULT_QUICKVIEW_OVERLAY_CLOSE_BUTTON);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()
}

 var $t = _testcase("124687/125424/125429", "Verify the navigation to search no result page from home page, the UI of search no result page and the UI of 'REFINE SEARCH' section on search no result page  left nav in application both as an  Anonymous and  Registered  user.");
 $t.start();
 try
 {
	 //search a product 
	 search($Generic_SearchResult[0][4]);
	 //heading
	 _assertVisible(_heading1($Generic_SearchResult[0][5]));
	 //text
	 _assertVisible(_paragraph($Generic_SearchResult[1][5]+" "+$Generic_SearchResult[0][4]));
	 _assertVisible($NOSEARCHRESULT_CONTENTASSET);
	 _assertVisible(_label($Generic_SearchResult[2][5]));
	 //try new search 
	 _assertVisible($NOSEARCHRESULT_TRYANEW_SEARCH);
	 _assertVisible($NOSEARCHRESULT_TRYANEW_SEARCH_GOBUTTON);
	 _assertVisible($NOSEARCHRESULT_NOHITS_FOOTER_CONTENTASSET);
	 //refine search
	 _assertVisible(_heading2($Generic_SearchResult[3][5]));
	 _setAccessorIgnoreCase(true);
	 
	 //Only two categories are appearing in left nav refinements
	 //var $Expected=_collectAttributes("_link","/has-sub-menu/", "sahiText");	 
	 //var $Actual= _collectAttributes("_listItem","/expandable/", "sahiText", _in(_div("refinement Category")));
	 //_assertEqualArrays($Expected.sort(),$Actual.sort());
}
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end()

 var $t = _testcase("125426", "Verify the functionality related to  Try new search 'GO' button for valid keyword on search no result page in application both as an  Anonymous and  Registered  user.");
 $t.start();
 try
 {
	 
 //Nosearch results navigation
 search($Generic_SearchResult[0][4]);
 //try new search 
 _setValue($NOSEARCHRESULT_TRYANEW_SEARCH, $Generic_SearchResult[0][0]);
 _click($NOSEARCHRESULT_TRYANEW_SEARCH_GOBUTTON);
//bread crumb
_assertVisible($SEARCHRESULT_BREADCRUMB_TEXT);
_assertEqual($Generic_SearchResult[0][2]+" \""+$Generic_SearchResult[0][0]+"\"", _getText($SEARCHRESULT_BREADCRUMB_TEXT));

 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end()
 
 var $t = _testcase("125427", "Verify the functionality related to  Try new search 'GO' button for invalid keyword on search no result page in application both as an  Anonymous and  Registered  user.");
 $t.start();
 try
 {
	 //Nosearch results navigation
	 search($Generic_SearchResult[0][4]);
	 //try new search 
	 _setValue($NOSEARCHRESULT_TRYANEW_SEARCH,$Generic_SearchResult[1][4]);
	 _click($NOSEARCHRESULT_TRYANEW_SEARCH_GOBUTTON);
	 //heading
	 _assertVisible(_heading1($Generic_SearchResult[0][5]));
	 //text
	 _assertVisible(_paragraph($Generic_SearchResult[1][5]+" "+$Generic_SearchResult[1][4]));
 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end()

 cleanup();