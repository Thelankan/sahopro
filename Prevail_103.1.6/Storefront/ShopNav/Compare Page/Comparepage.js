_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("ComparePage.xls");


SiteURLs();
cleanup();
_setAccessorIgnoreCase(true); 

var $t = _testcase("125458/125459","Verify the display of UI elements in Compare Bucket/navigation to Compare Bucket section  in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to subcat page	
	_click($COMPARE_SUBCATEGORY_LINK);
	//add at least two items to items to compare
	_check($COMPARE_CHECKBOX);
	_check($COMPARE_CHECKBOX1);
	//verify whether compare section appeared or not
	_assertVisible($COMPARE_BUCKET_ITEMS);
	//verify the UI
	_assertVisible(_heading2($Compare[0][1]));
	_assertEqual($Compare[1][1],$totalcomparableproducts);
	//compare items button
	_assertVisible($COMPARE_ITEMS_BUTTON);
	//clear all
	_assertVisible($COMPARE_CLEARALL_BUTTON);
	var $totalCompareItems=$comparebucketaactiveproducts;
	//remove link
	for(var $i=0;$i<$totalCompareItems;$i++)
		{
		_assertVisible(_link("compare-item-remove["+$i+"]"));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125460","Verify the functionality of 'Remove(X)' image in Compare image widget box  in the application both as an anonymous and a registered user");
$t.start();
try
{
	//fetch total no.of added compare items
	var $totalCompareItems=$comparebucketaactiveproducts;
	_log("$totalCompareItems"+$totalCompareItems);
	//Total check boxes
	var $totalCheckboxes=CompareCheckboxCount();
	var $CompareItemsBeforeRemoval=$totalCompareItems;
	var $CompareItemsAfterRemoval=0;
	for(var $i=0;$i<$totalCompareItems;$i++)
		{
			if($i==$totalCompareItems-1)
			{
			//compare items button shouldn't clickable
			_assertEqual($Compare[2][1],_style($COMPARE_ITEMS_BUTTON,"color"));
			//click on compare button 
			_click($COMPARE_ITEMS_BUTTON);
			//shouldn't navigate
			_assertNotVisible($COMPARE_BACKTORESULTS_LINK);
			}
			
			//click on remove link
			_click($COMPARE_ITEM_REMOVE_ICON);
			_wait(2000);
			$CompareItemsAfterRemoval=$comparebucketaactiveproducts;
			//verify after click on remove link
			if($i==$totalCompareItems-1)
				{
					//should navigate back to sub category
					_assertNotVisible($COMPARE_BUCKET_ITEMS);
					_assertContainsText($Compare[0][0], $COMPARE_PAGE_BREADCRUMB);
				}
			else
				{
					//Verifying compare items in compare section
					_assertEqual($CompareItemsBeforeRemoval-1,$CompareItemsAfterRemoval);
					//verifying check boxes checked in Products display section
					_assertNotTrue(_checkbox("compare-check["+$i+"]").checked);
				}
		
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("287293","Verify the application behavior on removing a product in compare products page when compare bucket contains only two products");
$t.start();
try
{

	//navigate to subcat page	
	_click($COMPARE_SUBCATEGORY_LINK);
	//add at least two items to items to compare
	_check($COMPARE_CHECKBOX);
	_check($COMPARE_CHECKBOX1);
	//count of active products in compare bucket
	var $activeProductinCompareBucket=ActiveProductsInCompareBucket();
	//verify whether compare section appeared or not
	_assertVisible($COMPARE_BUCKET_ITEMS);
	//click on compare button 
	_click($COMPARE_ITEMS_BUTTON);
	//verify the navigation and UI
	_assertVisible($COMPARE_HEADING);
	//two products should be there 
	var $activeProductinCompareBucket=ProductCountInComparePage();
	//comparison of products
	_assertEqual($activeProductinCompareBucket,$activeProductinCompareBucket);
	//remove one product from compare page
	_click($COMPARE_PAGE_REMOVE_LINK);
	//Should navigate back to PLP page
	_assertVisible($COMPARE_BUCKET_ITEMS);
	_assertVisible($COMPARE_SEARCHRESULT_SEARCHRESULT_ITEMS);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125461/125464/125465/148597","Verify the functionality of 'Compare Items' button/verify the Navvigation to/UI in Compare bucket section in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to subcat page	
	_click($COMPARE_SUBCATEGORY_LINK);
	//add at least two items to items to compare
	_check($COMPARE_CHECKBOX);
	_check($COMPARE_CHECKBOX1);
	//verify whether compare section appeared or not
	_assertVisible($COMPARE_BUCKET_ITEMS);
	//fetch total no.of added compare items
	var $totalCompareItems=$comparebucketaactiveproducts;
	//click on compare items button
	_click($COMPARE_ITEMS_BUTTON);
	//verify the navigation and UI
	_assertVisible($COMPARE_HEADING);
	//back to results links
	var $totalBackToResultLinks=CompareBacktoResultLinks();
	_assertEqual($Compare[3][1],$totalBackToResultLinks);
	for(var $i=0;$i<$totalBackToResultLinks;$i++)
		{
		_assertVisible(_link("<< back to results["+$i+"]"));
		}
	//print link
	//_assertVisible(_submit("print-page"));
	//remove,name,images links
	for(var $i=0;$i<$totalCompareItems;$i++)
		{
			_assertVisible(_link("remove-link["+$i+"]"));
			_assertVisible(_link("name-link["+$i+"]"));
			//_assertVisible(_image("/(.*)/",_near(_link("name-link["+$i+"]"))));
			_assertVisible(_div("product-image", _in(_div("product-tile["+$i+"]"))));
			//add to cart
			_assertVisible(_submit("Add to Cart["+$i+"]"));
			//add to wish list
			_assertVisible(_link("Add to Wishlist["+$i+"]"));
			//add to gift registry
			_assertVisible(_link("Add to Gift Registry["+$i+"]"));	
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("125462","Verify the functionality of 'Clear All' button in Compare bucket section  in the application both as an anonymous and a registered user");
$t.start();
try
{
	//click on back results
	_click($COMPARE_BACKTORESULTS_LINK);
	//click on clear all button
	_click($COMPARE_CLEARALL_BUTTON);
	//compare section shouln't visible
	_assertNotVisible($COMPARE_BUCKET_ITEMS);
	_assertNotVisible(_heading2($Compare[0][1]));
	_assertNotVisible($COMPARE_ITEMS_BUTTON);
	_assertNotVisible($COMPARE_CLEARALL_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125463","Verify the functionality of 'Comparison Grid' by adding more then 6 products to the Compare image widget box in the application both as an anonymous and a registered user");
$t.start();
try
{	
	//add 6 items for compare
	for(var $i=0;$i<$Compare[1][1];$i++)
		{
		//check the compare check box
		_check(_checkbox("compare-check["+$i+"]"));
		}
	//try add one more product for comparision
	_check($COMPARE_CHECKBOX6);
	//pop up should come click on cancel
	_expectConfirm("/This will remove/",false);
	//should remain in same page
	_assertVisible($COMPARE_BUCKET_ITEMS);
	//add one more product for comparision
	_check($COMPARE_CHECKBOX6);
	//click on ok
	_expectConfirm("/This will remove/",true);
	//first item should replace 
	 _assertFalse($COMPARE_CHECKBOX.checked);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125466","Verify the navigation of 'Back to Results' link in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//click on compare items button
	_click($COMPARE_ITEMS_BUTTON);	
	//fetch product name
	var $pName=_getText($COMPARE_PLP_NAMELINK);
	//click on back to results link
	_click($COMPARE_BACKTORESULTS_LINK);
	//verify the navigation back to sub category page
	_assertVisible(_link($Compare[0][0], _in($COMPARE_PAGE_BREADCRUMB)));
	_assertVisible($COMPARE_ITEMS_BUTTON);
	_assertVisible($COMPARE_CLEARALL_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125467","Verify the functionality of 'Remove(X)' image in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to sub category	
	_click($COMPARE_SUBCATEGORY_LINK);	
	//click on compare items button
	_click($COMPARE_ITEMS_BUTTON);
	//fetch total no.of remove links
	var $RemoveLinks=ComparePageRemoveLink();_log($RemoveLinks); 
	for(var $i=0;$i<$RemoveLinks;$i++)
		{
			_log($i);
			var $removeLinksBeforeClick=ComparePageRemoveLink();
			if($i==$RemoveLinks-1)
				{
					//click on remove link
					_click($COMPARE_PAGE_REMOVE_LINK);
					//should navigate back to sub category
					_assertVisible($COMPARE_PAGE_BREADCRUMB);
				}
			else
				{
					//click on remove link
					_click($COMPARE_PAGE_REMOVE_LINK);
					$removeLinksBeforeClick--;
					_wait(5000);
					var $removeLinks=ComparePageRemoveLink();
					_assertEqual($removeLinksBeforeClick,$removeLinks);
				}
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("148590","Verify the mouse hover functionality if user place the mouse over on any Product image in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to sub category	
	_click($COMPARE_SUBCATEGORY_LINK);
	//add at least two items to items to compare
	_check($COMPARE_CHECKBOX);
	_check($COMPARE_CHECKBOX1);
	//fetch total no.of added compare items
	var $totalCompareItems=$comparebucketaactiveproducts;
	//click on compare items button
	_click($COMPARE_ITEMS_BUTTON);
	//verify the quick view functionality
	for(var $i=0;$i<$totalCompareItems;$i++)
		{
			_mouseOver(_div("product-image["+$i+"]"));
			var $pName=_getText(_link("name-link["+$i+"]"));
			_wait(2000);
			//click on quick view overlay
			_click(_link("quickviewbutton",_div("product-image["+$i+"]")));
			_wait(2000);
			//verify whether overlay opened or not 
			_assertVisible($COMPARE_PDP_PRODUCTNAME);
			_assertEqual($pName, _getText($COMPARE_PDP_PRODUCTNAME));
			_assertVisible($COMPARE_QUICKVIEW_OVERLAY);
			_assertVisible($COMPARE_QUICKVIEW_OVERLAY_CLOSE_BUTTON);
			//close the overlay
			_click($COMPARE_QUICKVIEW_OVERLAY_CLOSE_BUTTON);
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148591","Verify the navigation if user click on any product image or product name link in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
for(var $i=0;$i<$totalCompareItems;$i++)
	{
		var $pName=_getText(_link("name-link["+$i+"]"));
		//click on name link
		_click(_link("name-link["+$i+"]"));
		//verify the navigation
		_assertVisible($COMPARE_PAGE_BREADCRUMB);
		_assertContainsText($pName, $COMPARE_PAGE_BREADCRUMB);
		_assertVisible($COMPARE_PDP_PRODUCTNAME);
		_assertEqual($pName, _getText($COMPARE_PDP_PRODUCTNAME));
		//navigate to sub category	
		_click($COMPARE_SUBCATEGORY_LINK);
		//click on compare items button
		_click($COMPARE_ITEMS_BUTTON);
		//click on product image link
		_click(_image("/(.*)/",_in(_div("product-image["+$i+"]"))));
		//verify the navigation
		_assertVisible($COMPARE_PAGE_BREADCRUMB);
		_assertContainsText($pName, $COMPARE_PAGE_BREADCRUMB);
		_assertVisible($COMPARE_PDP_PRODUCTNAME);
		_assertEqual($pName, _getText($COMPARE_PDP_PRODUCTNAME));
		//navigate to sub category	
		_click($COMPARE_SUBCATEGORY_LINK);
		//click on compare items button
		_click($COMPARE_ITEMS_BUTTON);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148593","Verify the functionality if user click on 'ADD TO CART' button in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to sub category	
	_click($COMPARE_SUBCATEGORY_LINK);
	//click on compare items button
	_click($COMPARE_ITEMS_BUTTON);
	//click on add to cart button
	//verify the add to cart functionality
	for(var $i=0;$i<$totalCompareItems;$i++)
		{
		_mouseOver(_div("product-image["+$i+"]"));
		var $pName=_getText(_link("name-link["+$i+"]"));
		_wait(2000);
		//click on quick view overlay
		_click(_link("quickviewbutton",_div("product-image["+$i+"]")));
		_wait(2000);
		//verify whether overlay opened or not 
		_assertVisible($COMPARE_PDP_PRODUCTNAME);
		_assertEqual($pName, _getText($COMPARE_PDP_PRODUCTNAME));
		_assertVisible($COMPARE_QUICKVIEW_OVERLAY);
		_assertVisible($COMPARE_QUICKVIEW_OVERLAY_CLOSE_BUTTON);
		//close the overlay
		_click($COMPARE_QUICKVIEW_OVERLAY_CLOSE_BUTTON);
		} 
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148595","Verify the navigation if user click on 'Add to Wish list' button in Compare Products page as guest user in the application both as an anonymous user");
$t.start();
try
{
	for(var $i=0;$i<$totalCompareItems;$i++)
		{	
		//click on add to wish list link
		_click(_link("Add to Wishlist["+$i+"]"));
		//verify the navigation
		_assertVisible(_link($Compare[0][2], _in($COMPARE_PAGE_BREADCRUMB)));
		_assertVisible(_heading1($Compare[1][2]));
		//navigate to sub category	
		_click($COMPARE_SUBCATEGORY_LINK);
		//click on compare items button
		_click($COMPARE_ITEMS_BUTTON);
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("287340","Verify the application behavior on click of add to gift registry link for a product in Compare Products page as a guest user.");
$t.start();
try
{
	for(var $i=0;$i<$totalCompareItems;$i++)
		{	
		//click on add to wish list link
		_click(_link("Add to gift registry["+$i+"]"));
		//verify the navigation
		_assertVisible(_link($Compare[0][4], _in($COMPARE_PAGE_BREADCRUMB)));
		_assertVisible(_heading1($Compare[1][4]));
		//navigate to sub category	
		_click($COMPARE_SUBCATEGORY_LINK);
		//click on compare items button
		_click($COMPARE_ITEMS_BUTTON);
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148596","Verify the functionality if user select any value from 'Other Compare Items' drop down in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to other category and add some products for comparision
	_click(_link($Compare[1][0]));
	//add at least two items to items to compare
	_check($COMPARE_CHECKBOX);
	_check($COMPARE_CHECKBOX1);
	_check($COMPARE_CHECKBOX2);
	//click on compare items button
	_click($COMPARE_ITEMS_BUTTON);
	//'Other Compare Items'
	_assertVisible(_label($Compare[0][3]));
	_assertVisible(_select("compare-category-list"));
	//select the suits category from drop down
	_setSelected(_select("compare-category-list"),$Compare[0][0]);
	_assertEqual($Compare[3][1],_count("_link","/Remove/"));
	//select the suits category from drop down
	_setSelected(_select("compare-category-list"),$Compare[1][0]);
	_assertEqual($Compare[4][1],_count("_link","/Remove/"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//_______ Register User __________

//login to the application
_click($COMPARE_LOGIN_LINK);
login();
//navigate to wish list and delete all added items 
_click($COMPARE_WISHLIST_LINK);
while(_isVisible($COMPARE_WISHLIST_REMOVE_BUTTON))
	{
	_click($COMPARE_WISHLIST_REMOVE_BUTTON);
	}

var $t = _testcase("148594","Verify the navigation if user click on 'Add to Wish list' button in Compare Products page as guest user in the application both as an anonymous user");
$t.start();
try
{
	//navigate to sub category	
	_click($COMPARE_SUBCATEGORY_LINK);	
	//click on compare items button
	_click($COMPARE_ITEMS_BUTTON);
	for(var $i=0;$i<$totalCompareItems;$i++)
		{	
		var $pName=_getText(_link("name-link["+$i+"]"));
		//click on add to wish list link
		_click(_link("Add to Wishlist["+$i+"]"));
		//verify the navigation
		_assertVisible(_link($Compare[0][2], _in($COMPARE_PAGE_BREADCRUMB)));
		_assertVisible(_heading1($Compare[2][2]));
		_assertVisible(_link($pName));
		//navigate to sub category	
		_click($COMPARE_SUBCATEGORY_LINK);
		//click on compare items button
		_click($COMPARE_ITEMS_BUTTON);
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("287339","Verify the application behavior on click of add to gift registry link for a product in Compare Products page as a registered user.");
$t.start();
try
{

	//navigate to sub category	
	_click($COMPARE_SUBCATEGORY_LINK);	
	//click on compare items button
	_click($COMPARE_ITEMS_BUTTON);
	for(var $i=0;$i<$totalCompareItems;$i++)
		{	
		var $pName=_getText(_link("name-link["+$i+"]"));
		//click on add to wish list link
		_click(_link("Add to gift registry["+$i+"]"));
		//verify the navigation
		_assertVisible(_link($Compare[0][4], _in($COMPARE_PAGE_BREADCRUMB)));
		_assertVisible(_heading1($Compare[0][4]));
		_assertVisible(_link($pName));
		//navigate to sub category	
		_click($COMPARE_SUBCATEGORY_LINK);
		//click on compare items button
		_click($COMPARE_ITEMS_BUTTON);
		}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


cleanup();
