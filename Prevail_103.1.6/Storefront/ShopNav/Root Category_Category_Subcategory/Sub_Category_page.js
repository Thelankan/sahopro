_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("Category_Data.xls");

//from util.GenericLibrary/BrowserSpecific.js
var $FirstName=$FName;
var $LastName=$LName;
var $UserEmail=$uId;
SiteURLs();
var $RootCatName=$Generic_category[0][10];
var $CatName=$Generic_category[1][10];
var $SubCatName=$Generic_category[2][10];
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("125646", "Verify the UI of third category grid page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	//bread crumb
	_assertVisible($CATEGORY_PAGE_BREADCRUMB);
	_assertContainsText($SubCatName, $CATEGORY_PAGE_BREADCRUMB);
	//hero slot
	_assertVisible($CATEGORY_PAGE_PROMOTIONAL_SOLT);
	//sort by
	_assertVisible($CATEGORY_SORTDROPDOWN_HEADER);
	_assertVisible($CATEGORY_SORTDROPDOWN_FOOTER);
	//refinement header
	_assertVisible($CATEGORY_REFINEMENT_HEADING);
	//left nav
	_assertVisible($CATEGORY_LEFTNAV_REFINEMENTS);
	//result hits
	_assertVisible($CATEGORY_SHOWING_RESULTS_HEADER);
	_assertVisible($CATEGORY_SHOWING_RESULTS_FOOTER);
	//Items displayed
	_assertVisible($CATEGORY_SEARCHRESULT_ITEMS);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("125634", "Verify the display of breadcrumb and its functionality in the third category grid page  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	//bread crumb
	_assertVisible($CATEGORY_PAGE_BREADCRUMB);
	_assertContainsText($SubCatName, $CATEGORY_PAGE_BREADCRUMB);
	var $expBreadCrumb=$RootCatName+" "+$CatName+" "+$SubCatName;
	/*if(_isIE())
		{
		_assertEqual($expBreadCrumb.replace(/\s/g,""), _getText($CATEGORY_PAGE_BREADCRUMB).replace(/\s/g,""));
		}
	else
		{
		_assertEqual($expBreadCrumb, _getText($CATEGORY_PAGE_BREADCRUMB));
		}	*/
	//navigate to root category page
	_click(_link($RootCatName,_in($CATEGORY_PAGE_BREADCRUMB)));
	var $expBreadCrumb1=$RootCatName;
	if(_isIE())
	{
	_assertEqual($expBreadCrumb1.replace(/\s/g,""), _getText($CATEGORY_PAGE_BREADCRUMB).replace(/\s/g,""));
	}
	else
	{
	_assertNotVisible($CATEGORY_PAGE_BREADCRUMB);
	//Bread crumb is not visible in category landing page 
	//_assertEqual($expBreadCrumb1, _getText($CATEGORY_PAGE_BREADCRUMB));
	}
	
	navigateSubCatPage();
	//navigate to category page
	_click(_link($CatName,_in($CATEGORY_PAGE_BREADCRUMB)));
	var $expBreadCrumb2=$RootCatName+" "+$CatName;
	if(_isIE())
	{
	_assertEqual($expBreadCrumb2.replace(/\s/g,""), _getText($CATEGORY_PAGE_BREADCRUMB).replace(/\s/g,""));
	}
	else
	{
	_assertEqual($expBreadCrumb2, _getText($CATEGORY_PAGE_BREADCRUMB));
	}
	//Validating navigation from category page to home page
	navigateSubCatPage();
	_assertNotVisible(_link("Home",_in($CATEGORY_PAGE_BREADCRUMB)));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125637", "Verify the display of category links under heading Shop <cat name> in left nav in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	_setAccessorIgnoreCase(true);
	//the display of category names
	var $ExpectedCategory=$CatName;
	_assertVisible($SUBCATEGORY_LEFTNAV_REFINEMENTS);
	if(_isIE())
		{
			_assertEqual($ExpectedCategory.replace(/\s/g,""), _getText($CATEGORY_REFINEMENT_LINK_ACTIVE).replace(/\s/g,""));
		}
		else
		{
			_assertEqual($ExpectedCategory, _getText($CATEGORY_REFINEMENT_LINK_ACTIVE));
		}	
	//navigation of sub-category link.
	var $subcategories=SubCategoryLinksInLeftNav();
	for($i=1;$i<$subcategories.length;$i++)
	{
		_click(_link($subcategories[$i], _in($CATEGORY_LEFTNAV_SUBCATEGORY_LINKS_SECTION)));
		_assertContainsText($subcategories[$i], $CATEGORY_PAGE_BREADCRUMB);
		_assertEqual($subcategories[$i], _getText($CATEGORY_REFINEMENT_LINK_ACTIVE, _in($CATEGORY_LEFTNAV_SUBCATEGORY_LINKS_SECTION)));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125638", "Verify the functionality after selecting different types of filters in third category grid page  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	navigateSubCatPage();
	var $Filters=LeftNavFilterNames();
	var $ExpectedApplied= new Array();
	for(var $i=0;$i<$Filters.length;$i++)
	{
		if(_isVisible(_div("refinement "+$Filters[$i])))
			{
			
			if ($Filters[$i]=="Category")
				{
				var $FilterOptions=RootCategoryLeftNavRefinements();
				_click(_link($FilterOptions[0]));	
				$ExpectedApplied.push($FilterOptions[0]+" x");
				}
			else if ($Filters[$i]=="Color")
				{
				var $FilterOptions=SubCategoryLeftNavColorRefinements();eeeeeeeee
				_click(_link($FilterOptions[0]));	
				$ExpectedApplied.push($FilterOptions[0]+" x");
				}
			else if ($Filters[$i]=="Price")
				{
				var $FilterOptions=SubCategoryLeftNavPriceRefinements();
				_click(_link($FilterOptions[0]));	
				$ExpectedApplied.push($FilterOptions[0]+" x");
				}
			else if ($Filters[$i]=="Size")
				{
				var $FilterOptions=SubCategoryLeftNavSizeRefinements();
				_click(_link($FilterOptions[0]));	
				$ExpectedApplied.push($FilterOptions[0]+" x");
				}
			else
				{
				var $FilterOptions=SubCategoryLeftNavOtherRefinements();
				_click(_link($FilterOptions[0]));	
				$ExpectedApplied.push($FilterOptions[0]+" x");
				}
			}		
	}
	var $ActualApplied=AppliedFiltersInBreadcrumb();
	_assertEqual($ExpectedApplied.sort(),$ActualApplied.sort());
	_click($HOMEPAGE_HOMEIMAGE);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125639", "Verify the functionality of NEW ARRIVAL filter in the left nav of third category grid page  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	navigateSubCatPage();
	if(_isVisible(_heading3("NEW ARRIVAL")))
	  {
		_click(_link("Refine by:true"));
		_assertVisible($CATEGORY_REFINEMENT_CLEAR_LINK);
		_assertVisible($CATEGORY_BREADCRUMB_REFINEMENT_VALUE);
	    _click($CATEGORY_REFINEMENT_CLEAR_LINK);
	    _assertNotVisible($CATEGORY_REFINEMENT_CLEAR_LINK);
	    _assertNotVisible($CATEGORY_BREADCRUMB_REFINEMENT_VALUE);
	  }
	  else
	  {
	    _log("No New Arrival Refinement available");
	  }
	_click($HOMEPAGE_HOMEIMAGE);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125640", "Verify the functionality of COLOR filter in the left nav of third category grid page  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	navigateSubCatPage();
	var $colors = LeftNavColorAttributes();
	var $unSelectable=UnSelectableSwatchesinColorRefinement();
	for(var $k=0; $k<$colors.length; $k++)
	{
		if($unSelectable.indexOf($colors[$k])>=0)
			{
			_assert(true, "Swatch is unselectable");
			}
		else
			{
			 _click(_link($colors[$k]));
			  //clear link
			  _assertVisible($CATEGORY_LEFTNAV_COLOR_REFINEMENT_CLEAR_LINK);
			  _assertEqual($colors[$k]+" x",_getText($CATEGORY_BREADCRUMB_REFINEMENT_VALUE));
			 _click($CATEGORY_BREADCRUMB_VALUE_CLOSE_LINK);
			 _wait(4000,!_isVisible($CATEGORY_BREADCRUMB_VALUE_CLOSE_LINK));
			}  
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}		
$t.end();

var $t = _testcase("125641", "Verify the functionality of SIZE filter in the left nav of third category grid page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	var $Size =SubCategoryLeftNavSizeRefinements();
	var $SizeunSelectable=SubCategoryLeftNavSizeUnSelectableRefinements();
	for ($k=0; $k< $Size.length; $k++)
	{
			if($unSelectable.indexOf($colors[$k])>=0)
				{
				_assert(true, "Swatch is unselectable");
				}
			else
				{
				_click(_link("swatch-"+$Size[$k]));
				_assertEqual($Size[$k]+" x", _getText($CATEGORY_BREADCRUMB_REFINEMENT_VALUE)); 	
				_click($CATEGORY_BREADCRUMB_VALUE_CLOSE_LINK);
				 _wait(4000,!_isVisible($CATEGORY_BREADCRUMB_VALUE_CLOSE_LINK));
				}  
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125643", "Verify the system's response on selection on Price filter from left nav  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	var $PriceRange = PriceRangesInPriceRefinement();
	for ($k=0; $k< $PriceRange.length; $k++)
	{
	_click(_link($PriceRange[$k]));
	_assertEqual($PriceRange[$k]+" x", _getText($CATEGORY_BREADCRUMB_REFINEMENT_VALUE)); 	
	priceCheck($PriceRange[$k]);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
/*
var $t = _testcase("125644", "Verify the pagination in category landing page in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	pagination();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/
var $t = _testcase("125647/125648", "Verify the display of products in third category grid page and details displayed for each item in category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	var $productCount = ProductTileCollection();
	for ($i=0 ; $i<$productCount.length; $i++)
	 {
		 var $prod = _getText(_div("product-tile["+$i+"]"));
		 _assertVisible($CATEGORY_THUMBLINK);
		 _assertVisible($CATEGORY_PLP_PRODUCTNAME_SECTION);
		 if(_isVisible($CATEGORY_CALLOUT_MESSAGE))
		   {
		      _assert(_isVisible($CATEGORY_CALLOUT_MESSAGE));
		   }
		  else
		  {
		    _log("No promo message to display");
		  }
		if(_isVisible($CATEGORY_PLP_SWATCHLIST))
			{
			_assert(_isVisible($CATEGORY_PLP_SWATCHLIST));
			}
		else
			{
			  _log("No alternate swatches available to display");
			}
	_log($prod);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125649", "Verify the system's response on click of 'list view icon' (or) grid view icon beside pagination  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	_assertVisible($CATEGORY_SEARCHRESULT_CONTENT);
	_click($CATEGORY_TOOGLE_GRID);
	_assertVisible(_byClassName("search-result-content wide-tiles", "DIV"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125650", "Verify the display and functionality of Sort dropdown  in third category grid page in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	if(_isVisible($CATEGORY_SORTDROPDOWN_HEADER) && _isVisible($CATEGORY_SORTDROPDOWN_FOOTER))
	{
		var $sort=_getText($CATEGORY_SORTDROPDOWN_HEADER);
		var $j=0;
		for(var $i=1;$i<$sort.length;$i++)
		{
			_setSelected($CATEGORY_SORTDROPDOWN_HEADER, $sort[$i]);
			//Validating the selection
			_assertEqual($sort[$i],_getSelectedText($CATEGORY_SORTDROPDOWN_HEADER),"Selected option is displaying properly");
			_assertEqual($sort[$i],_getSelectedText($CATEGORY_SORTDROPDOWN_FOOTER),"Selected option is displaying properly");
			if($sort.indexOf($Generic_category[$j][1])>-1)
			{
				_setSelected($CATEGORY_SORTDROPDOWN_HEADER, $Generic_category[$j][1]);
				sortBy($Generic_category[$j][1]);
				$j++;
			}
		}
	}
	else
	{
		_assert(false, "sort by drop down is not present");
	}
	_click($HOMEPAGE_HOMEIMAGE);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


if(!isMobile())
{
var $t=_testcase("125859/125860","Verify the functionality of 'Compare Items ' button and display of UI elements in Compare page in Compare section  in the application both as an anonymous and a registered user");
$t.start();
try		
{
		//navigate to subcat page	
		navigateSubCatPage();
		//add 6 items for compare
		for(var $i=0;$i<$Generic_category[0][11];$i++)
			{
			//check the compare check box
			_check(_checkbox("compare-check["+$i+"]"));
			}		
		//verify whether compare section appeared or not
		_assertVisible($CATEGORY_COMPARE_BUCKET_ITEMS);
		//fetch total no.of added compare items
		var $totalCompareItems=ActiveProductsInCompareBucket();
		_assertEqual($Generic_category[0][11],$totalCompareItems);
		//click on compare items button
		_click($COMPARE_ITEMS_BUTTON);
		//verify the navigation and UI
		_assertVisible($COMPARE_HEADING);
		//back to results links
		var $totalBackToResultLinks=CompareBacktoResultLinks();
		_assertEqual($Generic_category[1][11],$totalBackToResultLinks);
		for(var $i=0;$i<$totalBackToResultLinks;$i++)
			{
			_assertVisible(_link("<< back to results["+$i+"]"));
			}
		
		//print link
		//_assertVisible(_submit("print-page"));
		//_assertVisible(_link("compare-print print-page button"));
		
		//remove,name,images links
		for(var $i=0;$i<$totalCompareItems;$i++)
			{
			_assertVisible(_link("remove-link["+$i+"]"));
			_assertVisible(_link("name-link["+$i+"]"));
			_assertVisible(_image("/(.*)/",_near(_link("name-link["+$i+"]"))));
			//add to cart
			_assertVisible(_submit("Add to Cart["+$i+"]"));
			//add to wish list
			_assertVisible(_link("Add to Wishlist["+$i+"]"));
			//add to gift registry
			_assertVisible(_link("Add to Gift Registry["+$i+"]"));
			//price label
			_assertVisible(_div("product-pricing["+$i+"]"));
			}
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()	
	
var $t = _testcase("125862","Verify the navigation of 'Back to Results' link in Compare page  in the application both as an anonymous and a registered user");
$t.start();
try
	{
		//fetch product name
		var $pName=_getText($SHOPNAV_PLP_NAMELINK);
		//click on back to results link
		_click(_link("/back to results/"));
		//verify the navigation to search results
		//bread crumb
		_assertVisible($CATEGORY_PAGE_BREADCRUMB);
		_assertContainsText($Generic_category[2][10], $CATEGORY_PAGE_BREADCRUMB);
	}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
	

//click on compare items button
_click($COMPARE_ITEMS_BUTTON);
var $t = _testcase("125861","Verify the functionality of 'Remove ' link in Compare page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	var $removeLinks=_count("_link","/action remove-link/");
	for(var $i=0;$i<$removeLinks;$i++)
		{
			var $removeLinksBeforeClick=_count("_link","/action remove-link/");
			if($i==$removeLinks-1)
				{
					//click on remove link
					_click(_link("action remove-link"));
					//should navigate back to sub category
					_assertVisible($CATEGORY_PAGE_BREADCRUMB);
				}
			else
				{
					//click on remove link
					_click(_link("action remove-link"));
					$removeLinksBeforeClick--;
					_wait(5000);
					$removeLinks=_count("_link","/action remove-link/");
					_assertEqual($removeLinksBeforeClick,$removeLinks);
				}
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();	


var $t=_testcase("125867","Verify the Quick view UI  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	//fetching the product name
	var $PName=_getText($SHOPNAV_PLP_NAMELINK);
	//mouse over on image
	_mouseOver($CATEGORY_THUMBLINK);
	//Quick view button
	_click($QUICKVIEWBUTTON_LINK); 
	//title
	_assertVisible($QUICKVIEW_OVERLAY_HEADING_TITLE);
	_assertEqual($Generic_category[0][2], _getText($QUICKVIEW_OVERLAY_HEADING_TITLE));
	//close
	_assertVisible($QUICKVIEW_OVERLAY_CLOSE_BUTTON);
	//Product name
	_assertVisible($CATEGORY_PDP_PRODUCTNAME);
	//Main image
	_assertVisible($QUICKVIEW_PRODUCTIMAGE_MAIN);
	//Product number
	_assertVisible($QUICKVIEW_PRODUCTNUMBER);
	//view details link
	_assertVisible($QUICKVIEW_VIEWFULLDETAILS_LINK);
	//Product price
	_assertVisible($QUICKVIEW_PRODUCT_PRICE);
	//Previous & next links
	_assertVisible($QUICKVIEW_PREVIOUS_NEXT_NAVIGATION);
	//stock status
	_assertVisible($QUICKVIEW_PRODUCT_AVAILIBILITY);
	//Promo messages if any Promotions are applied
	if(_isVisible($QUICKVIEW_PROMOTION_CALLOUT_MESSAGE))
	{
	  _assertVisible($QUICKVIEW_PROMOTION_CALLOUT_MESSAGE);
	  _log(_getText($QUICKVIEW_PROMOTION_CALLOUT_MESSAGE));
	}
	else
	{
	  _log("No promotional message to display");
	}
	//variations
	_assertVisible($QUICKVIEW_PRODUCT_VARIATIONS);
	//quantity
	_assertVisible($QUICKVIEW_QUANTITY_BOX);
	_assertEqual("1", _getValue($QUICKVIEW_QUANTITY_BOX));
	
	//#### Price will not display beside Add tocart button according to new functionality ###########
	//_assertVisible(_span("price-sales", _in(_fieldset("ADD TO CART OPTIONS"))));
	
	//Add to cart
	_assertVisible($QUICKVIEW_ADDTO_CART_SECTION);
	//Alternate images
	_assertVisible($QUICKVIEW_PDP_PRODUCT_THUMBNAILS);
	
	//########## Alternative images Heading is not displaying ###########
	//_assertVisible(_heading2($Generic_category[1][2]));
	
	//selecting variations
	selectSwatch();
	//add to wish list
	_assertVisible($QUICKVIEW_ADDTOWISHLIST_LINK);
	//add to gift registry
	_assertVisible($QUICKVIEW_ADD_PRODUCT_GIFTREGISTRY_LINK);
	
	//closing overlay
	_click($QUICKVIEW_OVERLAY_CLOSE_BUTTON);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


var $t=_testcase("126825","Verify 'Next' and 'Previous' links functionality in Quick view in sub category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	var $numberOfProducts = YouMightalsoLikeProductCount();
	var $prodNames= YouMightalsoLikeProducts();
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	for ($i=0; $i<$numberOfProducts-1; $i++)
	{
	  var $name = _getText($CATEGORY_PDP_PRODUCTNAME);
	  _assertEqual($prodNames[$i],$name);
	  _click($QUICKVIEW_NEXT_LINK);
	}
	for($i=$numberOfProducts-2; $i>0; $i--)
	{
	  _click($QUICKVIEW_PREVIOUS_LINK);
	  var $name = _getText($CATEGORY_PDP_PRODUCTNAME);
	  _assertEqual($prodNames[$i],$name);	  
	 }
	//closing overlay
	_click($QUICKVIEW_OVERLAY_CLOSE_BUTTON);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


var $t=_testcase("128530","Verify the 'Add to cart' functionality in Quick view of sub category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	var $Product=_getText($CATEGORY_PDP_PRODUCTNAME);
	//var $ProductPrice=_getText($QUICKVIEW_PRODUCT_PRICE);
	var $ProductPrice=_getText($CATEGORY_CART_SALES_PRICE);
	//selecting variations
	selectSwatch();
	//click in add to cart
	_click($QUICKVIEW_ADDTO_CART);
	//navigate to cart
	_click($QUICKVIEW_MINICART_LINK);
	//Checking Product name
	_assertEqual($Product, _getText($CATEGORY_CART_NAME));
	_assertEqual($ProductPrice,_getText($CATEGORY_CART_SALES_PRICE));
	//removing item from cart
	while(_isVisible($CATEGORY_CART_REMOVE_LINK))
	{
	_click($CATEGORY_CART_REMOVE_LINK);
	}
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

//'Add to wish list' as guest
var $t=_testcase("125866","Verify the functionality of  'Add to wish list' link in Quick view  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	//selecting swatches
	selectSwatch();
	//add to wish list
	_assertVisible($QUICKVIEW_ADDTOWISHLIST_LINK);
	_click($QUICKVIEW_ADDTOWISHLIST_LINK);
	//validating the navigation
	_assertVisible($CATEGORY_PAGE_BREADCRUMB);
	_assertEqual($Generic_category[0][4], _getText($CATEGORY_PAGE_BREADCRUMB));
	//heading
	_assertVisible($QUICKVIEW_LOGIN_HEADING);
	_assertVisible($QUICKVIEW_FINDSOMEONES_WISHLIST);
	//wish list section
	_assertVisible($QUICKVIEW_GUEST_FINDSOMEONES_WISHLIST);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

//'Add to Gift Registry' as guest
var $t=_testcase("125865","Verify the functionality of  'Add to Gift Registry' link in Quick view  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	//selecting swatches
	selectSwatch();
	//add to gift registry
	_assertVisible($QUICKVIEW_ADD_PRODUCT_GIFTREGISTRY_LINK);
	_click($QUICKVIEW_ADD_PRODUCT_GIFTREGISTRY_LINK);
	//validating the navigation
	_assertVisible($CATEGORY_PAGE_BREADCRUMB);
	_assertEqual($Generic_category[0][5], _getText($CATEGORY_PAGE_BREADCRUMB));
	//heading
	_assertVisible($QUICKVIEW_GIFTREGISTRY_LOGIN_HEADING);
	_assertVisible(_heading2("/"+$QUICKVIEW_GIFTREGISTRY_FINDSOMEONES_REGISTRY_HEADING+"/"));
	//gift registry section
	_assertVisible($QUICKVIEW_GUEST_FINDSOMEONES_GR_SECTION);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


//Login()
_click($QUICKVIEW_LOGIN_LINK);
login();

var $t=_testcase("125866","Verify the functionality of  'Add to wish list' link in Quick view  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	var $Pname=_getText($CATEGORY_PDP_PRODUCTNAME);
	//selecting swatches
	selectSwatch();
	//add to wish list
	_assertVisible($QUICKVIEW_ADDTOWISHLIST_LINK);
	_click($QUICKVIEW_ADDTOWISHLIST_LINK);
	//bread crumb
	_assertVisible($CATEGORY_PAGE_BREADCRUMB);
	_assertEqual($Generic_category[0][9], _getText($CATEGORY_PAGE_BREADCRUMB));
	//heading
	_assertVisible($QUICKVIEW_FINDSOMEONES_WISHLIST);
	//Product
	_assertVisible(_cell("/"+$Pname+"/"));
	//removing item
	_click($QUICKVIEW_WISHLIST_REMOVE_BUTTON, _in(_row("/"+$Pname+"/")));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

var $t=_testcase("125865","Verify the functionality of  'Add to Gift Registry' link in Quick view  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	var $Pname=_getText($CATEGORY_PDP_PRODUCTNAME);
	//selecting swatches
	selectSwatch();
	//add to gift registry
	_assertVisible($QUICKVIEW_ADD_PRODUCT_GIFTREGISTRY_LINK);
	_click($QUICKVIEW_ADD_PRODUCT_GIFTREGISTRY_LINK);
	//bread crumb
	_assertVisible($CATEGORY_PAGE_BREADCRUMB);
	_assertEqual($Generic_category[0][8], _getText($CATEGORY_PAGE_BREADCRUMB));
	//heading
	_assertVisible($QUICKVIEW_GIFTREGISTRY_HEADING); 
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()
}
cleanup();

