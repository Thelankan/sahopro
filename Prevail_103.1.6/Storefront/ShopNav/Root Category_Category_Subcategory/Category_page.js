_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("Category_Data.xls");

var $RootCatName=$Generic_category[0][0];
var $CatName=$Generic_category[1][0];
//from util.GenericLibrary/BrowserSpecific.js
var $FirstName=$FName;
var $LastName=$LName;
var $UserEmail=$uId;
SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("124406", "Verify the system response on click of category name in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_assertVisible($CATEGORY_PAGE_BREADCRUMB);
	_assertContainsText($Generic_category[1][0], $CATEGORY_PAGE_BREADCRUMB);	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124407", "Verify the UI of the category landing page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//bread crumb
	_assertVisible($CATEGORY_PAGE_BREADCRUMB);
	_assertContainsText($Generic_category[1][0], $CATEGORY_PAGE_BREADCRUMB);
	//hero slot
	_assertVisible($CATEGORY_PAGE_PROMOTIONAL_SOLT);
	//sort by
	_assertVisible($CATEGORY_SORTDROPDOWN_HEADER);
	_assertVisible($CATEGORY_SORTDROPDOWN_FOOTER);
	//refinement header
	_assertVisible($CATEGORY_REFINEMENT_HEADING);
	//left nav
	_assertVisible($CATEGORY_LEFTNAV_REFINEMENTS);
	//result hits
	_assertVisible($CATEGORY_SHOWING_RESULTS_HEADER);
	_assertVisible($CATEGORY_SHOWING_RESULTS_FOOTER);
	//Items displayed
	_assertVisible($CATEGORY_SEARCHRESULT_ITEMS);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124408", "Verify the breadcrumb in category landing page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//bread crumb
	_assertVisible($CATEGORY_PAGE_BREADCRUMB);
	_assertContainsText($Generic_category[1][0], $CATEGORY_PAGE_BREADCRUMB);
	var $expBreadCrumb=$RootCatName+" "+$CatName;
	if(_isIE())
		{
		_assertEqual($expBreadCrumb.replace(/\s/g,""), _getText($CATEGORY_PAGE_BREADCRUMB).replace(/\s/g,""));
		}
	else
		{
		_assertEqual($expBreadCrumb, _getText($CATEGORY_PAGE_BREADCRUMB));
		}
	//navigate to root category page
	_click(_link($RootCatName,_in($CATEGORY_PAGE_BREADCRUMB)));
	
	//Bread crumb should not visible in Root category landing page
	_assertNotVisible($CATEGORY_PAGE_BREADCRUMB);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124409", "Verify the display of category links under heading Shop <cat name> in left nav in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_setAccessorIgnoreCase(true);
	//the display of category names
	var $ExpectedCategory=$CatName;
	_assertVisible($CATEGORYLINKS_EXPANDED_LEFTNAV);
	_assertEqual($ExpectedCategory, _getText($CATEGORY_REFINEMENT_LINK_ACTIVE));
	//navigation of sub-category link.
	var $subcategories=SubCategoryLinksInLeftNav();
	for($i=1;$i<$subcategories.length;$i++)
	{
	_click(_link($subcategories[$i], _in($CATEGORY_LEFTNAV_SUBCATEGORY_LINKS_SECTION)));
	_assertContainsText($subcategories[$i], $CATEGORY_PAGE_BREADCRUMB);
	_assertEqual($subcategories[$i], _getText($CATEGORY_REFINEMENT_LINK_ACTIVE, _in($CATEGORY_LEFTNAV_SUBCATEGORY_LINKS_SECTION)));
	}
	//Click on any category name and verifying display of subcategory names
	navigateCatPage();
	var $ExpectedCategory1=_getText($CATEGORY_REFINEMENT_LINK_ACTIVE);
	_click($CATEGORY_REFINEMENT_LINK_ACTIVE);
	_assertVisible($CATEGORYLINKS_EXPANDED_LEFTNAV);
	_assertContainsText($ExpectedCategory1, $CATEGORYLINKS_EXPANDED_LEFTNAV);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124414", "Verify the system's response on click of 'list view icon' (or) grid view icon beside pagination after applying the filter.");
$t.start();
try
{
	navigateCatPage();
	_assertVisible($CATEGORY_SEARCHRESULT_CONTENT);
	_click($CATEGORY_TOOGLE_GRID);
	_assertVisible(_byClassName("search-result-content wide-tiles", "DIV"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124410", "Verify the system's response on selection on color filter from left nav in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	var $colors = LeftNavColorAttributes();
	var $unSelectable=UnSelectableSwatchesinColorRefinement();
	for(var $k=0; $k<$colors.length; $k++)
	{
		if($unSelectable.indexOf($colors[$k])>=0)
			{
			_assert(true, "Swatch is unselectable");
			}
		else
			{
			 _click(_link($colors[$k]));
			  //clear link
			 _assertVisible($CATEGORY_LEFTNAV_COLOR_REFINEMENT_CLEAR_LINK);
			  _assertEqual($colors[$k]+" x",_getText($CATEGORY_BREADCRUMB_REFINEMENT_VALUE));
			 _click($CATEGORY_BREADCRUMB_VALUE_CLOSE_LINK);
			 _wait(4000,!_isVisible($CATEGORY_BREADCRUMB_VALUE_CLOSE_LINK));
			}  
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}		
$t.end();

var $t = _testcase("124411", "Verify the system's response on selection on Price filter from left nav  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	var $PriceRange =PriceRangesInPriceRefinement();
	for ($k=0; $k< $PriceRange.length; $k++)
	{
	_click(_link($PriceRange[$k]));
	_assertEqual($PriceRange[$k]+" x", _getText($CATEGORY_BREADCRUMB_REFINEMENT_VALUE)); 	
	priceCheck($PriceRange[$k]);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124420", "Verify the details displayed for each item in category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	var $productCount = ProductTileCollection();
	for ($i=0 ; $i<$productCount.length; $i++)
	 {
		 var $prod = _getText(_div("product-tile["+$i+"]"));
		 _assertVisible($CATEGORY_THUMBLINK);
		 _assertVisible($CATEGORY_PLP_PRODUCTNAME_SECTION);
		 if(_isVisible($CATEGORY_CALLOUT_MESSAGE))
		   {
		      _assert(_isVisible($CATEGORY_CALLOUT_MESSAGE));
		   }
		  else
		  {
		    _log("No promo message to display");
		  }
		if(_isVisible($CATEGORY_PLP_SWATCHLIST))
			{
			_assert(_isVisible($CATEGORY_PLP_SWATCHLIST));
			}
		else
			{
			  _log("No alternate swatches available to display");
			}
	_log($prod);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124413", "Verify the system's response on click of filter names displayed in left nav");
$t.start();
try
{
	navigateCatPage();
	var $Filters=LeftNavFilterNames();
	for(var $i=0;$i<$Filters.length;$i++)
		{	
			//click on filter
			_click(_heading3($Filters[$i]));
			//filter will collapse
			//_assertNotVisible(_list("/(.*)/", _in(_div("refinement "+$Filters[$i]))));
			_assertNotVisible(_list("/(.*)/",_near(_heading3($Filters[$i]))));
			//click on filter
			_click(_heading3($Filters[$i]));
			//filter will expand
			//_assertVisible(_list("/(.*)/", _in(_div("refinement "+$Filters[$i]))));
			_assertVisible(_list("/(.*)/",_near(_heading3($Filters[$i]))));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124416", "Verify whether filter get clear when user click on selected filter value");
$t.start();
try
{
	navigateCatPage();
	var $Filters=LeftNavFilterNames();
	var $FilterOptions=_collectAttributes("_link","/(.*)/","sahiText",_in(_list("/(.*)/", _under(_heading3($Filters[0])))));
	var $unSelectableOpt=_collectAttributes("_listItem","/unselectable swatch/","sahiText",_under(_heading3($Filters[0])));
	//var $FilterOptions=_collectAttributes("_link","/(.*)/","sahiText",_in(_list("/(.*)/", _in(_div("refinement "+$Filters[0])))));
	//var $unSelectableOpt=_collectAttributes("_listItem","/unselectable swatch/","sahiText",_in(_div("refinement "+$Filters[0])));
		for(var $j=0;$j<$FilterOptions.length;$j++)
			{
				if($unSelectableOpt.indexOf($FilterOptions[$j])>=0)
				{
					_assert(true, "Swatch is unselectable");
				}
			else
				{
					_click(_link($FilterOptions[$j]));
					_assertVisible($CATEGORY_REFINEMENT_CLEAR_LINK);
					_assertVisible($CATEGORY_BREADCRUMB_REFINEMENT_VALUE);
					_assertEqual($FilterOptions[$j]+" x", _getText($CATEGORY_BREADCRUMB_REFINEMENT_VALUE));
					_click(_link($FilterOptions[$j]));
					_assertNotVisible($CATEGORY_REFINEMENT_CLEAR_LINK);
					_assertNotVisible($CATEGORY_BREADCRUMB_REFINEMENT_VALUE);
				}
			}
	_click($HOMEPAGE_HOMEIMAGE);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124419", "Verify the system's reponse when user apply multiple filters  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	var $Filter=LeftNavFilterNames();
	var $ExpectedApplied= new Array();
	for(var $i=0;$i<$Filter.length;$i++)
	{
		if(_isVisible(_div("refinement "+$Filter[$i])))
			{
			var $FilterOptions=_collectAttributes("_link","/(.*)/","sahiText",_in(_list("/(.*)/", _in(_div("refinement "+$Filter[$i])))));
			_click(_link($FilterOptions[0]));	
			$ExpectedApplied.push($FilterOptions[0]+" x");
			}		
	}
	var $ActualApplied=AppliedFiltersInBreadcrumb();
	_assertEqual($ExpectedApplied.sort(),$ActualApplied.sort());
	_click($HOMEPAGE_HOMEIMAGE);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124412", "Verify the system's response on click of 'true' link under the 'New Arrival' section in left nav  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	if(_isVisible($CATEGORY_NEWARRIVAL_HEADING))
	  {
		_click(_link("/true/",_near($CATEGORY_NEWARRIVAL_HEADING)));
		_assertVisible($CATEGORY_REFINEMENT_CLEAR_LINK);
		_assertVisible($CATEGORY_BREADCRUMB_REFINEMENT_VALUE);
	    _click($CATEGORY_REFINEMENT_CLEAR_LINK);
	    _assertNotVisible($CATEGORY_REFINEMENT_CLEAR_LINK);
	    _assertNotVisible($CATEGORY_BREADCRUMB_REFINEMENT_VALUE);
	  }
	  else
	  {
	    _log("No New Arrival Refinement available");
	  }
	_click($HOMEPAGE_HOMEIMAGE);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124421", "Verify the system's response on click of Products links in category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	var $productName=_getText($SHOPNAV_PLP_NAMELINK);
	var $productPrice = _getText($CATEGORY_PLP_SALESPRICE);
	//click on name link
	_click($SHOPNAV_PLP_NAMELINK);
	_assertEqual($productName,_getText($CATEGORY_PDP_PRODUCTNAME));
	_assertEqual($productPrice,_getText($CATEGORY_CART_SALES_PRICE));
	
	navigateCatPage();
	var $productName=_getText($SHOPNAV_PLP_NAMELINK);
	var $productPrice = _getText($CATEGORY_PLP_SALESPRICE);
	//click on image
	_click($CATEGORY_THUMBLINK);
	_assertEqual($productName,_getText($CATEGORY_PDP_PRODUCTNAME));
	_assertEqual($productPrice,_getText($CATEGORY_CART_SALES_PRICE));
	
	if(!isMobile())
	{
		var $Checkcount=0;
		//compare checkbox
		navigateCatPage();
		_check($CATEGORY_COMPARE_CHECKBOX);
		$Checkcount++;
		//validating compare 
		_assertVisible($CATEGORY_COMPARE_BUCKET_ITEMS);
		_assertEqual(1,ActiveProductsInCompareBucket());
		_uncheck($CATEGORY_COMPARE_CHECKBOX);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124415", "Verify the pagination in category landing page in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	pagination();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

if(!isMobile())
{
	var $t = _testcase("124418", "Verify the sustem's reponse on selecting value from 'Products per page' drop down  in the application both as an anonymous and a registered user");
	$t.start();
	try
	{
		navigateCatPage();
		if(_isVisible($CATEGORY_ITEMSPERPAGE_DROPDOWN_HEADER) && _isVisible($CATEGORY_ITEMSPERPAGE_DROPDOWN_FOOTER))
		{	
			var $itemsDisp=_getText($CATEGORY_ITEMSPERPAGE_DROPDOWN_HEADER);
			for(var $i=0;$i<$itemsDisp.length;$i++)
			{
				_setSelected($CATEGORY_ITEMSPERPAGE_DROPDOWN_HEADER, $itemsDisp[$i]);
				//Validating the selection
				_assertEqual($itemsDisp[$i],_getSelectedText($CATEGORY_ITEMSPERPAGE_DROPDOWN_HEADER),"Selected option is displaying properly");
				_assertEqual($itemsDisp[$i],_getSelectedText($CATEGORY_ITEMSPERPAGE_DROPDOWN_FOOTER),"Selected option is displaying properly at footer");
				ItemsperPage($itemsDisp[$i]);
			}		
		}
		else
		{
			_assert(true, "View drop down is not present");
		}
		_click($HOMEPAGE_HOMEIMAGE);
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();
}

var $t = _testcase("124417", "Verify the system's reponse on selection of any sort by value");
$t.start();
try
{
	navigateCatPage();
	if(_isVisible($CATEGORY_SORTDROPDOWN_HEADER) && _isVisible($CATEGORY_SORTDROPDOWN_FOOTER))
	{
		var $sort=_getText($CATEGORY_SORTDROPDOWN_HEADER);
		var $j=0;
		for(var $i=1;$i<$sort.length;$i++)
		{
			_setSelected($CATEGORY_SORTDROPDOWN_HEADER, $sort[$i]);
			//Validating the selection
			_assertEqual($sort[$i],_getSelectedText($CATEGORY_SORTDROPDOWN_HEADER),"Selected option is displaying properly");
			_assertEqual($sort[$i],_getSelectedText($CATEGORY_SORTDROPDOWN_FOOTER),"Selected option is displaying properly");
			if($sort.indexOf($Generic_category[$j][1])>-1)
			{
				_setSelected($CATEGORY_SORTDROPDOWN_HEADER, $Generic_category[$j][1]);
				sortBy($Generic_category[$j][1]);
				$j++;
			}
		}
	}
	else
	{
		_assert(false, "sort by drop down is not present");
	}
	_click($HOMEPAGE_HOMEIMAGE);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

if(!isMobile())
{
var $t=_testcase("148584","Verify the navigation related to Quickview overlay In the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	//fetching the product name
	var $PName=_getText($SHOPNAV_PLP_NAMELINK);
	//mouse over on image
	_mouseOver($CATEGORY_THUMBLINK);
	//Quick view button
	//_assertVisible($QUICKVIEWBUTTON_LINK);
	_click($QUICKVIEWBUTTON_LINK);  
	//quick view overlay
	_assertVisible($QUICKVIEW_OVERLAY);
	_assertEqual($PName, _getText($CATEGORY_PDP_PRODUCTNAME));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


var $t=_testcase("125620","Verify the UI of 'Quick view' In the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	//fetching the product name
	var $PName=_getText($SHOPNAV_PLP_NAMELINK);
	//mouse over on image
	_mouseOver($CATEGORY_THUMBLINK);
	//Quick view button
	_click($QUICKVIEWBUTTON_LINK); 
	//title
	_assertVisible($QUICKVIEW_OVERLAY_HEADING_TITLE);
	_assertEqual($Generic_category[0][2], _getText($QUICKVIEW_OVERLAY_HEADING_TITLE));
	//close
	_assertVisible($QUICKVIEW_OVERLAY_CLOSE_BUTTON);
	//Product name
	_assertVisible($CATEGORY_PDP_PRODUCTNAME);
	//Main image
	_assertVisible($QUICKVIEW_PRODUCTIMAGE_MAIN);
	//Product number
	_assertVisible($QUICKVIEW_PRODUCTNUMBER);
	//view details link
	_assertVisible($QUICKVIEW_VIEWFULLDETAILS_LINK);
	//Product price
	_assertVisible($QUICKVIEW_PRODUCT_PRICE);
	//Previous & next links
	_assertVisible($QUICKVIEW_PREVIOUS_NEXT_NAVIGATION);
	//stock status
	_assertVisible($QUICKVIEW_PRODUCT_AVAILIBILITY);
	//Promo messages if any Promotions are applied
	if(_isVisible($QUICKVIEW_PROMOTION_CALLOUT_MESSAGE))
	{
	  _assertVisible($QUICKVIEW_PROMOTION_CALLOUT_MESSAGE);
	  _log(_getText($QUICKVIEW_PROMOTION_CALLOUT_MESSAGE));
	}
	else
	{
	  _log("No promotional message to display");
	}
	//variations
	_assertVisible($QUICKVIEW_PRODUCT_VARIATIONS);
	//quantity
	_assertVisible($QUICKVIEW_QUANTITY_BOX);
	_assertEqual("1", _getValue($QUICKVIEW_QUANTITY_BOX));
	//Add to cart
	_assertVisible($QUICKVIEW_ADDTO_CART_SECTION);
	//Alternate images
	_assertVisible($QUICKVIEW_PDP_PRODUCT_THUMBNAILS);
	//selecting variations
	selectSwatch();
	//add to wish list
	_assertVisible($QUICKVIEW_ADDTOWISHLIST_LINK);
	//add to gift registry
	_assertVisible($QUICKVIEW_ADD_PRODUCT_GIFTREGISTRY_LINK);
	
	//closing overlay
	_click($QUICKVIEW_OVERLAY_CLOSE_BUTTON);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


var $t=_testcase("126824","Verify 'Next' and 'Previous' links functionality in Quick view in category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	var $numberOfProducts = YouMightalsoLikeProductCount();
	var $prodNames= YouMightalsoLikeProducts();
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	for ($i=0; $i<$numberOfProducts-1; $i++)
	{
	  var $name = _getText($CATEGORY_PDP_PRODUCTNAME);
	  _assertEqual($prodNames[$i],$name);
	  _click($QUICKVIEW_NEXT_LINK);
	}
	for($i=$numberOfProducts-2; $i>0; $i--)
	{
	  _click($QUICKVIEW_PREVIOUS_LINK);
	  var $name = _getText($CATEGORY_PDP_PRODUCTNAME);
	  _assertEqual($prodNames[$i],$name);	  
	 }
	//closing overlay
	_click($QUICKVIEW_OVERLAY_CLOSE_BUTTON);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


var $t=_testcase("128529","Verify the 'Add to cart' functionality in Quick view of category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	var $Product=_getText($CATEGORY_PDP_PRODUCTNAME);
	//var $ProductPrice=_getText($QUICKVIEW_PRODUCT_PRICE);
	var $ProductPrice=_getText($CATEGORY_CART_SALES_PRICE); 
	//selecting variations
	selectSwatch();
	//click in add to cart
	_click($QUICKVIEW_ADDTO_CART);
	//navigate to cart
	_click($QUICKVIEW_MINICART_LINK);
	//Checking Product name
	_assertEqual($Product, _getText($CATEGORY_CART_NAME));
	_assertEqual($ProductPrice,_getText($CATEGORY_CART_SALES_PRICE));
	//removing item from cart
	while(_isVisible($CATEGORY_CART_REMOVE_LINK))
	{
	_click($CATEGORY_CART_REMOVE_LINK);
	}
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

var $t=_testcase("148587","Verify the system response on click of color swatch displayed on 'Quick view' overlay in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	var $Product=_getText($CATEGORY_PDP_PRODUCTNAME);	
	var $colors =SwatchanchorCountColor();
	for(var $i=0;$i<$colors;$i++)
		{			
			var $a=_getAttribute(_image("/(.*)/", _in(_link("swatchanchor["+$i+"]", _in($QUICKVIEW_COLOR_LIST)))),"title");
			_click(_link($a, _in($QUICKVIEW_COLOR_LIST)));
			_assertEqual($a,_getText($QUICKVIEW_SELECTED_SWATCH,_in($QUICKVIEW_COLOR_LIST)));
			_assert(true, "Selected color is appropriate");
		}	
	//closing overlay
	_click($QUICKVIEW_OVERLAY_CLOSE_BUTTON);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


var $t=_testcase("148585","Verify 'view full details' link in 'Quick view' overlay in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	var $Product=_getText($CATEGORY_PDP_PRODUCTNAME);
	//var $ProductPrice=_getText($QUICKVIEW_PRODUCT_PRICE);
	var $ProductPrice=_getText($CATEGORY_CART_SALES_PRICE);
	//click on view details link
	_click($QUICKVIEW_VIEWFULLDETAILS_LINK);
	//validating the navigation
	_assertEqual($Product,_getText($CATEGORY_PDP_PRODUCTNAME));
	_assertEqual($ProductPrice,_getText($CATEGORY_CART_SALES_PRICE));
	//bread crumb
	_assertVisible($CATEGORY_PAGE_BREADCRUMB);
	_assertContainsText($Product, $CATEGORY_PAGE_BREADCRUMB);
	//_assertEqual($Product, _getText(_span("last", _in(_list("breadcrumb")))));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


var $t=_testcase("--","Verify the functionality of 'QTY' drop down on 'Quick view' overlay in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	//quantity
	_assertVisible($QUICKVIEW_QUANTITY_BOX);
	_assertEqual("1", _getValue($QUICKVIEW_QUANTITY_BOX));
	for(var $i=0;$i<$Quantity.length;$i++)
		{
			_setValue($QUICKVIEW_QUANTITY_BOX, $Quantity[$i][1]);
			//selecting variations
			selectSwatch();
			//click in add to cart
			_click($QUICKVIEW_ADDTO_CART);
			//navigate to cart
			_click($QUICKVIEW_MINICART_LINK);
			if($i==2)
				{
				_assertEqual($Quantity[$i][1],_getText($CATEGORY_CART_QTY_DROPDOWN));
				}
			else
				{
				_assertEqual($Quantity[0][2],_getText($CATEGORY_CART_QTY_DROPDOWN));
				}
			while(_isVisible($CATEGORY_CART_REMOVE_LINK))
			{
			_click($CATEGORY_CART_REMOVE_LINK);
			}
			navigateCatPage();
			_mouseOver($CATEGORY_THUMBLINK);
			_click($QUICKVIEWBUTTON_LINK);
		}
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

//'Add to wish list' as guest
var $t=_testcase("125621","Verify the system's response on click on 'Add to wish list' link in Quick view in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	//selecting swatches
	selectSwatch();
	//add to wish list
	_assertVisible($QUICKVIEW_ADDTOWISHLIST_LINK);
	_click($QUICKVIEW_ADDTOWISHLIST_LINK);
	//validating the navigation
	_assertVisible($CATEGORY_PAGE_BREADCRUMB);
	_assertEqual($Generic_category[0][4], _getText($CATEGORY_PAGE_BREADCRUMB));
	//heading
	_assertVisible($QUICKVIEW_LOGIN_HEADING);
	_assertVisible($QUICKVIEW_FINDSOMEONES_WISHLIST);
	//wish list section
	_assertVisible($QUICKVIEW_GUEST_FINDSOMEONES_WISHLIST_SECTION);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

//'Add to Gift Registry' as guest
var $t=_testcase("125622","Verify the system's response on click on 'Add to Gift Registry' link in Quick view  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	//selecting swatches
	selectSwatch();
	//add to gift registry
	_assertVisible($QUICKVIEW_ADD_PRODUCT_GIFTREGISTRY_LINK);
	_click($QUICKVIEW_ADD_PRODUCT_GIFTREGISTRY_LINK);
	//validating the navigation
	_assertVisible($CATEGORY_PAGE_BREADCRUMB);
	_assertEqual($Generic_category[0][5].replace(/\s/g,''), _getText($CATEGORY_PAGE_BREADCRUMB).replace(/\s/g,'')); 
	_assertEqual($Generic_category[0][5], _getText($CATEGORY_PAGE_BREADCRUMB));
	//heading
	_assertVisible($QUICKVIEW_GIFTREGISTRY_LOGIN_HEADING);
	_assertVisible(_heading2("/"+$QUICKVIEW_GIFTREGISTRY_FINDSOMEONES_REGISTRY_HEADING+"/"));
	//gift registry section
	_assertVisible($QUICKVIEW_GUEST_FINDSOMEONES_GR_SECTION);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


//Login()
_click($QUICKVIEW_LOGIN_LINK);
login();

var $t=_testcase("125621","Verify the system's response on click on 'Add to wish list' link in Quick view in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	var $Pname=_getText($CATEGORY_PDP_PRODUCTNAME);
	//selecting swatches
	selectSwatch();
	//add to wish list
	_assertVisible($QUICKVIEW_ADDTOWISHLIST_LINK);
	_click($QUICKVIEW_ADDTOWISHLIST_LINK);
	//bread crumb
	_assertVisible($CATEGORY_PAGE_BREADCRUMB);
	_assertEqual($Generic_category[0][9], _getText($CATEGORY_PAGE_BREADCRUMB));
	//heading
	_assertVisible($QUICKVIEW_FINDSOMEONES_WISHLIST);
	//Product
	_assertVisible(_cell("/"+$Pname+"/"));
	//removing item
	_click($QUICKVIEW_WISHLIST_REMOVE_BUTTON, _in(_row("/"+$Pname+"/")));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

var $t=_testcase("125622","Verify the system's response on click on 'Add to Gift Registry' link in Quick view  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	var $Pname=_getText($CATEGORY_PDP_PRODUCTNAME);
	//selecting swatches
	selectSwatch();
	//add to gift registry
	_assertVisible($QUICKVIEW_ADD_PRODUCT_GIFTREGISTRY_LINK);
	_click($QUICKVIEW_ADD_PRODUCT_GIFTREGISTRY_LINK);
	//bread crumb
	_assertVisible($CATEGORY_PAGE_BREADCRUMB);
	_assertEqual($Generic_category[0][8],_getText($CATEGORY_PAGE_BREADCRUMB));
	//heading
	_assertVisible($QUICKVIEW_GIFTREGISTRY_HEADING); 
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()
}
cleanup();
