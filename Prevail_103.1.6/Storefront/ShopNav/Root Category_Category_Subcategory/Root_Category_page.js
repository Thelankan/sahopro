_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("Category_Data.xls");

var $RootCat=$Generic_category[2][0];
//from util.GenericLibrary/BrowserSpecific.js
var $FirstName=$FName;
var $LastName=$LName;
var $UserEmail=$uId;
SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();

var $RootCatName=$Generic_category[0][0];

var $t = _testcase("124286", "Verify the UI of the root category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateRootCatPage();
	//category refinement header
	_assertVisible($CATEGORY_REFINEMENT_HEADING);
	_assertEqual("Shop "+$RootCatName, _getText($CATEGORY_REFINEMENT_HEADING));
	//Category refinement section
	_assertVisible($ROOTCATEGORY_PAGE_PROMOTION_SOLT);
	//Category banner
	_assertVisible(_heading1($RootCatName));
	_assertVisible($ROOTCATEGORY_LANDING_BANNER);
	//SubCategory content slots
	_assertVisible($ROOTCATEGORY_SECONDARY_BANNER_CONTENT);
	
	var $Rootcategory_Secondary_Category_tiles=RootCategorySecondaryCategoryTiles();
	
	for($i=0;$i<$Rootcategory_Secondary_Category_tiles;$i++)
	{
		_assertVisible(_div("category-tile["+$i+"]"));	
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124288/148574", "Verify the display and functionality of category links under heading 'Shop <root category name>' in left nav  in the application both as an anonymous and a registered user");
$t.start();
try
{	
	_setAccessorIgnoreCase(true);
	//navigation of sub-category link.
	var $categories=RootCategoryLeftNavRefinements();
	for($i=0;$i<$categories.length;$i++)
	{
	_click(_link($categories[$i], _in($ROOTCATEGORY_LEFTNAV_REFINEMENT)));
	_assertContainsText($categories[$i], $CATEGORY_PAGE_BREADCRUMB);
	_assertEqual($categories[$i], _getText($CATEGORY_REFINEMENT_LINK_ACTIVE, _in($ROOTCATEGORY_LEFTNAV_REFINEMENT)));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


cleanup();

