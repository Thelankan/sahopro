_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("Homepage.xls");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


var $t = _testcase("124676/215396/212936","Verify the UI of global header in the application as an anonymous user/Verify the application behvaior when user click on My Account icon in header");
$t.start();
try
{
	
//UI of global header(demand ware image)
_assertVisible($HOMEPAGE_HOMEIMAGE);
//login
_click($HEADER_USERACCOUNT_LINK);
//Login register heading
_assertVisible($HEADER_LOGINREGISTER_HEADING);
//Login link
_assertVisible($HEADER_LOGIN_LINK);
//Register link
_assertVisible($HEADER_REGISTER_LINK);
//cart
_assertVisible($HEADER_MINICART_ICON_SECTION);
_assertVisible($HEADER_STORELOCATOR_LINK);
_assertVisible($FOOTER_HELP_LINK);
_assertVisible($HEADER_PROMOTION_SLOT);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("148553","Verify the UI of global footer in the application both as an anonymous and a registered user");
$t.start();
try
{
	//Email textbox & Submit button
	_assertVisible($FOOTER_EMAIL_FIELD);
	_assertVisible($FOOTER_EMAIL_ICON);
	//verify the UI of global footer
	_assertVisible($HEADER_MYACCOUNT_LINK);
	_assertVisible($FOOTER_CHECKORDER_LINK);
	_assertVisible($FOOTER_HELP_LINK);
	_assertVisible($FOOTER_CONTACTUS_LINK);
	_assertVisible($FOOTER_ABOUTUS_LINK);
	_assertVisible($FOOTER_TERMS_LINK);
	_assertVisible($FOOTER_PRIVACY_LINK);
	_assertVisible($FOOTER_SITEMAP_LINK);
	_assertVisible($FOOTER_JOBS_LINK);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("148554","Verify the UI of body of the home page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//'Categories'
	_assertVisible($HEADER_CATEGORY_LEVEL_FIRST);
	//Search
	_assertVisible($HEADER_SEARCH_TEXTBOX);
	//Home slider
	_assertVisible($HOMEPAGE_SLIDES);
	_assertVisible($HOMEPAGE_SLOTS);
	// '3 promotional slots' 
	_assertVisible($HOMEPAGE_SLOT_LEFT);
	_assertVisible($HOMEPAGE_SLOT_CENTER);
	_assertVisible($HOMEPAGE_SLOT_RIGHT);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("124285/124679/212934","Verify the system response on click of root category in the application both as an anonymous and a registered user. and Verify the Navigation of Sub Category' links at home page in  the application both as an anonymous and a registered user");
$t.start();
try
{
	
	//Mouse Over on  root category
	_mouseOver($HOMEPAGE_CATEGORY_NAME_ARRIVALS);	
	//Nav to Home page
	_click($HOMEPAGE_HOMEIMAGE);
	_mouseOver($HOMEPAGE_CATEGORY_NAME_ARRIVALS);
	//Click on Subcat link
	_click($HOMEPAGE_CATEGORY_NAME_WOMENS);
	//Should Navigate to respective sub-category landing page
	_assertVisible($HOMEPAGE_CATEGORY_NAME_WOMENS, _in($PAGE_BREADCRUMB));
	
	//Nav to Home page
	_click($HOMEPAGE_HOMEIMAGE);
	//Mouse Hover on category name
	_mouseOver($HOMEPAGE_ROOTCATEGORY_NAME_WOMENS);
	//click on category name
	_click($HOMEPAGE_CATEGORY_NAME_CLOTHING);
	//Should Navigate to respective category landing page
	_assertVisible($HOMEPAGE_CATEGORYLANDINGPAGE_BREADCRUMB);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("124682/124685","Verify the navigation of links in the category menu and Verify the navigation of Gift Certificate and Top Seller link.");
$t.start();
try
{
	//Nav to Home page
	_click($HOMEPAGE_HOMEIMAGE);
	var $RootCategory= _collectAttributes("_link", "/has-sub-menu/", "sahiText", _in($HEADER_CATEGORY_LEVEL_FIRST));
	for(var $i=0;$i<$RootCategory.length;$i++)
		{
			_click(_link("has-sub-menu["+$i+"]", _in($HEADER_CATEGORY_LEVEL_FIRST)));
			_assertVisible(_span("Shop "+$RootCategory[$i]));			  
		}   
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("124683","Verify system's response when user clicks on any of the sub category links.");
$t.start();
try
{
	//Nav to Home page
	_click($HOMEPAGE_HOMEIMAGE);
	for(var $i=0;$i<$data.length;$i++)
		{
			_click(_link($data[$i][5], _in($HEADER_CATEGORY_LEVEL_FIRST)));
			_assertContainsText($data[$i][5], $PAGE_BREADCRUMB);  
		}   
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("124684","Verify the navigation of links in the child categories(sub �sub categories) .");
$t.start();
try
{
	//Nav to Home page
	_click($HOMEPAGE_HOMEIMAGE);	
	for(var $i=0;$i<$data.length;$i++)
		{
			_click(_link($data[$i][3], _in($HEADER_CATEGORY_LEVEL_FIRST)));
			_assertContainsText($data[$i][3], $PAGE_BREADCRUMB);  
		}   
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("148555/148562/212389","Verify the functionality of 'LOGIN'/'Store locator' link at global header in the application as an anonymous user/Verify the application behaviour on click of Store locator link in the header.");
$t.start();
try
{
	//Nav to Home page
	_click($HOMEPAGE_HOMEIMAGE);
	//Click on login link from header
	_click($HEADER_LOGIN_LINK );
	//Should Navigate to My Account Login Page
	_assertVisible($HOMEPAGE_LOGIN_SECTION);
	_assertVisible($HOMEPAGE_LOGIN_BREADCRUMB_MYACCOUNT, _in($PAGE_BREADCRUMB));
	//Nav to Home page
	_click($HOMEPAGE_HOMEIMAGE);
	//Click on Store Locator link
	_click($HEADER_STORELOCATOR_LINK);
	//Should navigate to Store locator page
	_assertVisible($HOMEPAGE_STORELOCATOR_HEADING);
	_assertVisible($HEADER_STORELOCATOR_LINK, _in($PAGE_BREADCRUMB));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("124694/148563/148564/148565/148566/148567/148568/148569/148570/148571","Verify the system response on click of  'Home'/'My Account'/'Check Order'/'Help'/'Contact Us'/'About Us'/'Terms'/'Privacy'/'sitemp'/'Jobs'  links at global footer in the application both as an anonymous and a registered user");
$t.start();
try
{
	for(var $i=0; $i<$footer_data.length; $i++)
		{
			//Nav to Home page
			_click($HOMEPAGE_HOMEIMAGE);
			
			//Clicking on Respective links from footer
			_click(_link($footer_data[$i][0],_in($FOOTER_CONTAINER)));
			//Verifying Breadcrumb and Heading to check whether its Navigated to respective pages or not.
			_assertVisible(_heading1("/"+$footer_data[$i][1]+"/"));
			_assertContainsText("/"+$footer_data[$i][2]+"/", $PAGE_BREADCRUMB);
		}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



_log("The Defect Need to be fixed for Email Sign up functionality in Home page");
var $t=_testcase("148572/148573","Verify the validation of email field on home page at the appliction both as an anonymous and a registered user and Verify the functionality of suscribe button at home page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//Nav to Home page
	_click($HOMEPAGE_HOMEIMAGE);
	for(var $i=0; $i<$emailsignup.length; $i++)
		{
			_setValue($FOOTER_EMAIL_FIELD,$emailsignup[$i][1]);
			_click($FOOTER_EMAIL_ICON);
			// The Defect Need to be fixed for Email Sign up functionality in Home page		
			if($i==0)
				{
					//Text box should be highlighted with red color
					_assertEqual($emailsignup[0][3],_style($FOOTER_EMAIL_FIELD,"background-color"));
				}
			else if($i==1)
				{
					//Verifying Error message and Font Color of Error message
					_assertEqual($emailsignup[$i][2],_getText($FOOTER_EMAIL_ERROR_MESSAGE));
					_assertEqual($emailsignup[1][3],_style($FOOTER_EMAIL_ERROR_MESSAGE,"color"));			
				}
			else
				{
					//Valid
					_assertNotVisible($FOOTER_EMAIL_ERROR_MESSAGE);
				}
		}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("124681","Verify the functionality of 'CART' link at global header in the application both as an anonymous and a registered user.");
$t.start();
try
{	
	//Adding item to the cart
	addItemToCart($data[1][1],0);
	_click($HOMEPAGE_ADDTOCART_BUTTON);
	_mouseOver($HEADER_MINICART_LINK);
	//Minicart Overlay Should be displayed
	_assertVisible($HEADER_MINICART_CONTENT);	
	//Clear the cart for verifying Empty cart
	ClearCartItems();
	//Mouse hover and click functionality should not happen. And products present should be 0
	_assertNotVisible($HEADER_MINICART_CONTENT);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("124677","Verify the system response on click of  'WISH LIST' link at global Footer as an anonymous  user in the application.");
$t.start();
try
{	

	//Nav to Home page
	_click($HOMEPAGE_HOMEIMAGE);
	//click on wishlist in footer
	_click($HEADER_WISHLIST_LINK);
	//wishlist heading 
	//verify bread crumb
	_assertVisible($HEADER_WISHLIST_LINK, _in($PAGE_BREADCRUMB));
	//Verifying the UI of returning customer section
	//MyAccount login as heading
	_assertVisible($HOMEPAGE_LOGIN_HEADING);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//-----Below TCs for Logged in User-------
_click($HEADER_LOGIN_LINK );
login();

var $t=_testcase("148552","Verify the UI of global header in the application as a registered user");
$t.start();
try
{
	//Nav to Home page
	_click($HOMEPAGE_HOMEIMAGE);	
	//login
	_click($HEADER_USERACCOUNT_LINK);
	_assertVisible($HEADER_MYACCOUNT_LINK);
	//Minicart Label
	_assertVisible($HEADER_MINICART_ICON_SECTION);
	_assertVisible($HEADER_STORELOCATOR_LINK);
	_assertVisible($FOOTER_HELP_LINK);
	//Promo message
	//FREE 2-Day SHIPPING FOR ORDERS OVER $300 promo message at home page
	_assertVisible($HEADER_PROMOTION_SLOT);	
	//Nav to Home page
	_click($HOMEPAGE_HOMEIMAGE);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("212943/212390","Verify the UI of the the My Account drop down as a register user./Verify the application behavior on click of links in My Account Drop down in the header as registered user");
$t.start();
try
{
	
	//click on user account link
	_click($USERACCOUNT_LINK);
	//Ui of my account drop down
	_assertVisible($HEADER_MYACCOUNT_LINK);
	_assertVisible($HEADER_CHECKORDER_LINK);
	_assertVisible($HEADER_WISHLIST_LINK);
	_assertVisible($HEADER_GIFTREGISTRY_LINK);
	
	for(var $i=0;$i>$MyAccountDropDown.length;$i++)
		{
		_click($MyAccountDropDown[$i]);
		//Navigation
		_assertVisible($MyAccountDropDownNavigations[$i]);
		}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();