_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("Minicart.xls");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("124691", "Verify the UI of mini Cart  when single product is added to bag  in the application both as an anonymous and a registered user");
$t.start();
try
{
//add item to cart
addItemToCart($Minicart[0][0],$Minicart[0][4]);
var $pName=_getText($MINICART_PDP_PRODUCTNAME);
var $price=_getText($MINI_CART_SALES_PRICE).replace("$","");
//verify the UI of Mini cart 
//mini cart overlay
_mouseOver($MINICART_LINK);
_assertVisible($MINICART_CONTENT);
_assertVisible($MINICART_IMAGE_TOOGLE);
//name link
_assertEqual($pName,_getText($MINICART_PRODUCT_NAME));
_mouseOver($MINICART_LINK);
//image
_assertVisible($MINICART_PRODUCT_IMAGE);
_assertVisible($MINICART_PRODUCT_IMAGE_SECTION);
//attributes
if($colorBoolean)
	{
		_mouseOver($MINICART_LINK);
		_assertVisible($MINICART_COLOR_LABEL);
		_assertVisible($MINICART_COLOR_VALUE);
	}
if($sizeBoolean)
	{
		_mouseOver($MINICART_LINK);
		_assertVisible($MINICART_SIZE_LABEL);
		_assertVisible($MINICART_SIZE_VALUE);
	}
if($widthBoolean)
	{
		_mouseOver($MINICART_LINK);
		_assertVisible($MINICART_WIDTH_LABEL);
		_assertVisible($MINICART_WIDTH_VALUE);
	}
_mouseOver($MINICART_LINK);
//quantity
_assertVisible($MINICART_QUANTITY_LABEL);
_assertVisible($MINICART_PRICING_SECTION);

_mouseOver($MINICART_LINK);
//sub total
_assertVisible($MINICART_ORDERSUBTITLE_LABEL);
_assertVisible($MINICART_ORDERSUBTITLE_VALUE);
_assertVisible($MINICART_SUBTOTALS_SECTION);
//slot
_assertVisible($MINICART_PROMO_SLOT);
//view cart
_assertVisible($MINICART_OVERLAY_VIEWCART_LINK);
_assertVisible($MINICART_CHECKOUT);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125605/125606/125607", "Verify the calculation of 'Order Subtotal'  & Product total/Quantity/Total In the header in the mini cart in the application both as an anonymous and a registered user");
$t.start();
try
{
//add item to cart
addItemToCart($Minicart[1][0],$Minicart[1][4]);	
//no.of items present in mini cart
var $totalSectionsInMiniCart=MiniCartProductCount();
var $minicartQuantity=new Array();
var $minicartPrice=new Array();
var $minicartName=new Array();

//featching data from minicart
for(var $i=0;$i<$totalSectionsInMiniCart;$i++)
	{
		//_mouseOver($MINICART_OVERLAY_VIEWCART_LINK);
		_mouseOver($MINICART_LINK);
		$minicartQuantity[$i]=parseInt(_extract(_getText(_div("mini-cart-pricing["+$i+"]")),"/[:](.*)/",true));
		$minicartPrice[$i]=parseFloat(_extract(_getText(_div("mini-cart-pricing["+$i+"]")),"/[$](.*)/",true));
		//verifying the quantity
		//var $qty1=parseInt(_extract(_getText(_span("mini-cart-price",_near(_link($Minicart[$i][0])))),"/[:](.*)/",true));
		var $qty1=parseInt(_getText(_span("/value/",_in(_div("mini-cart-pricing["+$i+"]")))))
		_assertEqual($qty1,$Minicart[$i][5]);
	}

var $expQuantity=$minicartQuantity[0]+$minicartQuantity[1];
var $expSubTotal=$minicartPrice[0]+$minicartPrice[1];
$expSubTotal=round($expSubTotal,2);

var $actSubTotal=parseFloat(_extract(_getText($MINICART_SUBTOTALS_SECTION),"/[$](.*)/",true));

_assertEqual($expSubTotal,$actSubTotal);

//verify the quantity in the header
var $QtyInHeader=_getText($MINICART_QUANTITY);
_assertEqual($expQuantity,$QtyInHeader);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148613", "Verify the UI of mini Cart on home page when multiple products are added to bag in the application both as an anonymous and a registered user");
$t.start();
try
{
//add item to cart
addItemToCart($Minicart[2][0],$Minicart[0][4]);	
addItemToCart($Minicart[3][0],$Minicart[0][4]);	
//verify the UI when multiple products are there
//no.of items present in mini cart
var $totalSectionsInMiniCart=MiniCartProductCount();
//fetching data from mini cart
for(var $i=0;$i<$totalSectionsInMiniCart;$i++)
	{
	//mousehover
	_mouseOver($MINICART_LINK);
	//image
	if($i==0)
		{
	_assertVisible(_image("/(.*)/",_in(_div("mini-cart-image["+$i+"]"))));
		}
	else
		{
		_assertNotVisible(_image("/(.*)/",_in(_div("mini-cart-image["+$i+"]"))));
		}
	//name link
	_assertVisible(_link("/(.*)/",_in(_div("mini-cart-name["+$i+"]"))));
	//attributes
	_assertVisible(_div("mini-cart-attributes["+$i+"]"));
	//quantity and price
	_assertVisible(_div("mini-cart-pricing["+$i+"]"));
	}
_mouseOver($MINICART_LINK);
//slot
_assertVisible($MINICART_PROMO_SLOT);
//view cart
_assertVisible($MINICART_OVERLAY_VIEWCART_LINK);
_assertVisible($MINICART_CHECKOUT);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148614", "Verify the UI of mini Cart on home page when Product description is  expanded  in the application both as an anonymous and a registered user");
$t.start();
try
{
//when product description is in expanded mode
//image should be visible
_assertVisible($MINICART_PRODUCT_IMAGE);
//name link
_assertVisible($MINICART_PRODUCT_NAME);
//attributes
_assertVisible($MINICART_ATTRIBUTES);
//quantity and price
_assertVisible($MINICART_PRICING_SECTION);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148615", "Verify the UI of mini Cart on home page when Product description is  collapsed in the application both as an anonymous and a registered user");
$t.start();
try
{
//when product description is in collapsed mode
//click on toggle
_mouseOver($MINICART_LINK);
_click($MINICART_IMAGE_TOOGLE);
//image should not be visible
_assertNotVisible($MINICART_PRODUCT_IMAGE);
//name link
_assertVisible($MINICART_PRODUCT_NAME);
//attributes
_assertVisible($MINICART_ATTRIBUTES);
//quantity and price
_assertVisible($MINICART_PRICING_SECTION);	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126806", "Verify the navigation on click of 'product name' on mini cart in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to home page
_click($HOMEPAGE_HOMEIMAGE);	
//mousehover
_mouseOver($MINICART_LINK);
//get product name
var $pName=_getText($MINICART_PRODUCT_NAME);
//click on name link
_click($MINICART_PRODUCT_NAME);
//verify the navigation

//_assertVisible(_span("last"));
//_assertEqual($pName,_getText(_span("last")));

_assertVisible(_span($pName, _in($PAGE_BREADCRUMB)));

_assertVisible($MINICART_PDP_PRODUCTNAME);
_assertEqual($pName,_getText($MINICART_PDP_PRODUCTNAME));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125592", "Verify the navigation on click of 'VIEW Cart' button in mini cart   in the application both as an anonymous and a registered user");
$t.start();
try
{
//mousehover
_mouseOver($MINICART_LINK);
//click on view cart
_click($MINICART_OVERLAY_VIEWCART_LINK);
//cart navigation verification
_assertVisible($CART_TABLE);
_assertVisible($CART_ACTIONBAR);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124593", "Verify the functionality of 'Go straight to checkout' link in Mini cart in the application as an anonymous user");
$t.start();
try
{
//mousehover
_mouseOver($MINICART_LINK);	
//click on go strait to checkout
_click($MINICART_CHECKOUT);
//verify the navigation
_assertVisible($CHECKOUT_LOGIN_GUEST_HEADING);
_assertVisible($CHECKOUT_GUEST_BUTTON); 
_assertVisible($LOGIN_RETURNINGNCUSTOMER_HEADING);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125594", "Verify the functionality of 'Go straight to checkout' link in Mini cart in the application as an registered user");
$t.start();
try
{
//login to the application
_click($LOGIN_LINK);
login();
//mousehover
_mouseOver($MINICART_LINK);	
//click on go strait to checkout
_click($MINICART_CHECKOUT);
//verify the navigation
//_assertVisible(_fieldset("/"+$Minicart[0][3]+"/"));
//_assertVisible(_div("shippingForm"));
if($CheckoutType==$BMConfig[0][1])
	{
	//Should Nav to Shipping page
	_assertVisible($SHIPPING_PAGE_HEADING);
	_assertVisible($MINICART_SHIPPING_SECTION);
	}
	if($CheckoutType==$BMConfig[1][1])
	{
	//verify the navigation
	_assertVisible($MINICART_SHIPPING_PAGE_HEADING);
	_assertVisible($SHIPPING_SUBMIT_BUTTON);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();
