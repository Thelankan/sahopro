_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("PDPage.xls");

//delete the existing user
//deleteUser();


SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


var $t = _testcase("124796","Verify the UI of Product detail page for Product set in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to product set
_click(_link($PDP_Productset[0][0]));
var $productName=_getText(_link($PDP_Productset[0][4]));
//click on name link
_click(_link($PDP_Productset[0][4]));
//verify the UI
//image
_assertVisible($PDP_PRIMARY_MAIN_IMAGE);
//name
_assertVisible($PDP_PRODUCTNAME);
_assertEqual($productName, _getText($PDP_PRODUCTNAME));
//number
_assertVisible($PDP_PRODUCT_NUMBER);
//next and previous
_assertVisible($PDP_PRIMARY_NAV_CONTAINER);
//fetch the total count present in the set
var $totalProductsInSet=PdpProductBundleCount();
for(var $i=0;$i<$totalProductsInSet;$i++)
	{
	//image
	_highlight(_div("product-set-image", _in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//name
	_assertVisible(_link("item-name",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//number
	_assertVisible(_div("product-number",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//availability
	_assertVisible(_div("availability-msg", _in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//swatches
	if(_isVisible(_heading3("COLOR",_in(_div("product-set-item product-bundle-item["+$i+"]")))))
	{
	_assertVisible(_listItem("/COLOR/",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	}
	if(_isVisible(_heading3("SIZE",_in(_div("product-set-item product-bundle-item["+$i+"]")))))
		{
		_assertVisible(_listItem("/SIZE/",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
		}
	if(_isVisible(_heading3("WIDTH",_in(_div("product-set-item product-bundle-item["+$i+"]")))))
		{
		_assertVisible(_listItem("/WIDTH/",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
		}
	//quantity
	_assertVisible(_label("QTY",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	_assertVisible(_textbox("Quantity",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//price
	//_assertVisible($PDP_PRODUCT_PRICE,_in(_div("block-add-to-cart add-sub-product",_in(_div("product-set-item product-bundle-item["+$i+"]"))))))
	//field get changed
	 _assertVisible($PDP_PRODUCT_PRICE, _in($PDP_PRODUCTSET_DETAILS_DIV, _in($PDP_PRODUCTSET_DETAILS_TILE)));
	//add to cart
	_assertVisible(_submit("button-fancy-medium sub-product-item add-to-cart",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//alternate images
	if(_isVisible($PDP_PRODUCT_THUMBNAILS))
		{
		_assertVisible($PDP_PRODUCT_THUMBNAILS_SELECTED);
		_assertVisible($PDP_PRODUCT_THUMBNAILS_SECTION);
		}
	else
		{
		_assert("alternate images are not present");
		}
	//add all to cart
	_assertVisible($PDP_PRODUCTSET_ADDALLTOCART_BUTTON);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124798","Verify the functionality of breadcrumbs on product detail page in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to root category	
var $Cat=_getText(_link($PDP_Productset[0][1]));
//navigate to sub category
_click(_link($PDP_Productset[0][0]));
//fetch the bread crumb
var $subCatBreadcrumb=_getText($PAGE_BREADCRUMB);
var $pName=_getText(_link($PDP_Productset[0][4]));
if(_isIE9())
	{
	var $breadCrumb=$subCatBreadcrumb+$pName;	
	}
else
	{
	var $breadCrumb=$subCatBreadcrumb+" "+$pName;
	}
//click on name link
_click(_link($PDP_Productset[0][4]));
//verify the presented breadcrumb
_assertEqual($breadCrumb,_getText($PAGE_BREADCRUMB));
_assertVisible(_span($pName,_in($PAGE_BREADCRUMB)));
//click on sub category link present in bread crumb
_click(_link($PDP_Productset[0][0],_in($PAGE_BREADCRUMB)));
//verify the navigation
_assertEqual($subCatBreadcrumb,_getText($PAGE_BREADCRUMB));
//navigate to PDP
_click(_link($PDP_Productset[0][4]));
//click on root category link present in the bread crumb
_click(_link($Cat,_in($PAGE_BREADCRUMB)));
//Bread crumb should not display
_assertNotVisible($PAGE_BREADCRUMB);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124801","Verify the functionality of product names on Product detail page for all products in the set in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to product set
_click(_link($PDP_Productset[0][0]));
var $productName=_getText(_link($PDP_Productset[0][4]));
//click on name link
_click(_link($PDP_Productset[0][4]));	
//fetch the total count present in the set
var $totalProductsInSet=PdpProductBundleCount();
for(var $i=0;$i<$totalProductsInSet;$i++)
	{
	var $pName=_getText(_link("item-name",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//click on name link
	_click(_link("item-name",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//verify the navigation to respective PDP
	_assertVisible(_span($pName,_in($PAGE_BREADCRUMB)));
	_assertVisible($PDP_PRODUCTNAME);
	_assertEqual($pName, _getText($PDP_PRODUCTNAME));	
	//navigate to product set
	search($PDP_Productset[0][0]);
	_click(_link($productName));	
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124804","Verify the functionality of attribute 'Select size' on product detail page for all products in the set in the application both as an anonymous and a registered user");
$t.start();
try
{
//Navigate to product set
search($PDP_Productset[0][0]);
_click(_link($PDP_Productset[0][4]));		
var $totalProductsInSet=PdpProductBundleCount();
for(var $i=0;$i<$totalProductsInSet;$i++)
 {
if(_isVisible(_heading3("SIZE",_in(_div("product-set-item product-bundle-item["+$i+"]")))))
	{
	//verify the select size functionality
	var $sizes = _count("_link","/swatchanchor/",_in(_listItem("/SIZE/",_in(_div("product-set-item product-bundle-item["+$i+"]")))));
	var $k=1;
	//only one swatchanchor is there
	if($sizes==1)
	{
		if(_isVisible(_listItem("selected",_in(_listItem("/SIZE/")))))
			  {
			  var $title=_getAttribute($PDP_SWATCHANCHOR,_in(_listItem("/SIZE/",_in(_div("product-set-item product-bundle-item["+$i+"]")),"title")));
			  //verify whether selected value is displaying or not
			  _assertVisible($PDP_SELECTED_SWATCH,_in(_listItem("/SIZE/",_in(_div("product-set-item product-bundle-item["+$i+"]")))));
			  _assertEqual($title, _getText($PDP_SELECTED_SWATCH,_in(_listItem("/SIZE/",_in(_div("product-set-item product-bundle-item["+$i+"]"))))));
			  }
		else
			  {
			  //click on swatch anchor
			  _click($PDP_SWATCHANCHOR,_in(_listItem("/SIZE/",_in(_div("product-set-item product-bundle-item["+$i+"]")))));
			  var $title=_getAttribute($PDP_SWATCHANCHOR,"title");
			  //verify whether selected value is displaying or not
			  _assertVisible($PDP_SELECTED_SWATCH);
			  _assertEqual($title, _getText($PDP_SELECTED_SWATCH));
			  }
	}
	//more than one swatchanchor is there
	else
	  {
	for (var $j=0; $j<$sizes; $j++)
	    {
		  if($j==0 || $j==1)
			{
			var $a=_getText($PDP_SELECTABLE_SWATCH, _in(_listItem("/SIZE/",_in(_div("product-set-item product-bundle-item["+$i+"]")))));
			_mouseOver(_link($a,_in(_listItem("/SIZE/",_in(_div("product-set-item product-bundle-item["+$i+"]"))))));
			var $b=_getAttribute(_link($a), "title");
			_assertEqual($a,$b);
			_click(_link($a,_in(_listItem("/SIZE/",_in(_div("product-set-item product-bundle-item["+$i+"]"))))));
			var $act=_getText($PDP_SELECTED_SWATCH, _in(_listItem("/SIZE/",_in(_div("product-set-item product-bundle-item["+$i+"]")))));
			_assertEqual($a,$act);
			}
		else
			{
			var $a=_getText(_listItem("selectable["+$k+"]",_in(_listItem("/SIZE/",_in(_div("product-set-item product-bundle-item["+$i+"]"))))));
			_mouseOver(_link($a,_in(_listItem("/SIZE/",_in(_div("product-set-item product-bundle-item["+$i+"]"))))));
			var $b=_getAttribute(_link($a), "title");
			_assertEqual($a,$b);
			_click(_link($a,_in(_listItem("/SIZE/",_in(_div("product-set-item product-bundle-item["+$i+"]"))))));
			var $act=_getText($PDP_SELECTED_SWATCH,_in(_listItem("/SIZE/",_in(_div("product-set-item product-bundle-item["+$i+"]")))));
			_assertEqual($a,$act);
			$k++;
			}	
		_assert(true, "Selected size is appropriate");	
		}
	  } 
	}
 }
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124802","Verify the functionality of attribute 'select color' on product detail page in the application both as an anonymous and a registered user");
$t.start();
try
{
//verify the select colour functionality 	
_assertVisible($PDP_COLOR_LIST);
var $totalProductsInSet=PdpProductBundleCount();
var $k=1;
for(var $i=0;$i<$totalProductsInSet;$i++)
{
if(_isVisible(_heading3("COLOR",_in(_div("product-set-item product-bundle-item["+$i+"]")))))
  {	
	var $colors=_count("_link","/swatchanchor/",_in(_listItem("/COLOR/",_in(_div("product-set-item product-bundle-item["+$i+"]")))));
	//only one swatchanchor is there
	if($colors==1)
	{
		if(_isVisible(_listItem("selected", _in(_list("swatches Color["+$i+"]")))))
			  {
			  var $title=_getAttribute($PDP_SWATCHANCHOR, _in(_list("swatches Color["+$i+"]")),"title");
			  //verify whether selected value is displaying or not
			  _assertVisible($PDP_SELECTED_SWATCH, _in(_list("swatches Color["+$i+"]")));
			  _assertEqual($title, _getText($PDP_SELECTED_SWATCH, _in(_list("swatches Color["+$i+"]"))));
			  }
		else
			  {
			  //click on swatchanchor
			  _click($PDP_SWATCHANCHOR, _in(_list("swatches Color["+$i+"]")));
			  var $title=_getAttribute($PDP_SWATCHANCHOR, _in(_list("swatches Color["+$i+"]")),"title");
			  //verify whether selected value is displaying or not
			  _assertVisible($PDP_SELECTED_SWATCH, _in(_list("swatches Color["+$i+"]")));
			  _assertEqual($title, _getText($PDP_SELECTED_SWATCH, _in(_list("swatches Color["+$i+"]"))));
			  }
	}
	//more than one swatchanchor is there
	else
	  {
	 for(var $j=0;$j<$colors;$j++)
		{
			if($j==0 || $j==1)
				{
				var $a=_getText($PDP_SELECTABLE_SWATCH, _in(_list("swatches Color["+$i+"]")));
				_mouseOver(_link($a, _in(_list("swatches Color["+$i+"]"))));
				var $b=_getAttribute(_link($a), "title");
				_assertEqual($a,$b);
				_click(_link($a, _in(_list("swatches Color["+$i+"]"))));
				var $act=_getText($PDP_SELECTED_SWATCH, _in(_list("swatches Color["+$i+"]")));
				_assertEqual($a,$act);
				}
			else
				{
				var $a=_getText(_listItem("selectable["+$k+"]", _in(_list("swatches Color["+$i+"]"))));
				_mouseOver(_link($a, _in(_list("swatches Color["+$i+"]"))));
				var $b=_getAttribute(_link($a), "title");
				_assertEqual($a,$b);
				_click(_link($a, _in(_list("swatches Color["+$i+"]"))));
				var $act=_getText($PDP_SELECTED_SWATCH, _in(_list("swatches Color["+$i+"]")));
				_assertEqual($a,$act);
				$k++;
				}
			  }
		_assert(true, "Selected color is appropriate");
		}
   }
}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124807","Verify the functionality of 'Size chart' link on product detail page for all products in the set in the application both as an anonymous and a registered user");
$t.start();
try
{
	
//Navigate to product set
search($PDP_Productset[0][0]);
_click(_link($PDP_Productset[0][4]));		
var $totalProductsInSet=PdpProductBundleCount();
for(var $i=0;$i<$totalProductsInSet;$i++)
  {	
  //verify the presence of size chart
  if(_isVisible(_link("Size Chart",_in(_div("product-set-item product-bundle-item["+$i+"]")))))
	  {
	  //click on link size chart
	  _click(_link("Size Chart",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	  _wait(2000,_isVisible(_div("ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable")));
	  if (_isVisible($PDP_SIZECHART_CONTENT))       
       {
         _assert(true, "Size chart dialogue is opened successfully");
       }
	     else  
  	  {
  	    _assert(false, "Size chart window is not opened");
  	  }
	  _assertVisible($PDP_SIZECHART_CLOSE_BUTTON);
	  _click($PDP_SIZECHART_CLOSE_BUTTON);
	  }
  else
	  {
	  _log("size chart is not present");
	  }
   }
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124809/124813","Verify the functionality of 'quantity box'/add to cart on product detail page  for all products in the set in the application both as an anonymous and a registered user");
$t.start();
try
{
	
//Navigate to product set
search($PDP_Productset[0][0]);
_click(_link($PDP_Productset[0][4]));	
var $totalProductsInSet=PdpProductBundleCount();
var $cartQuantity=0;
for(var $i=0;$i<$totalProductsInSet;$i++)
  {	
	//enter value in quantity box
	_setValue(_textbox("Quantity",_in(_div("product-set-item product-bundle-item["+$i+"]"))),$PDP_Productset[0][3]);
	//verify the entered value
	_assertEqual($PDP_Productset[0][3], _getValue(_textbox("Quantity",_in(_div("product-set-item product-bundle-item["+$i+"]")))));
	//click on add to cart
	_click(_submit("button-fancy-medium sub-product-item add-to-cart",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//verify the mini cart fly out
	_assertVisible($PDP_MINICART_CONTENT);
	$cartQuantity=$cartQuantity+parseInt($PDP_Productset[0][3]);
  }
//verify the total added quantity
var $expQuantity=$cartQuantity;
_assertEqual($cartQuantity,$expQuantity);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

ClearCartItems();
var $t = _testcase("124811/124816","Verify the validations for 'quantity box'/add to cart on product detail page for all products in the set in the application both as an anonymous and a registered user");
$t.start();
try
{

//Navigate to product set
search($PDP_Productset[0][0]);
_click(_link($PDP_Productset[0][4]));
var $totalProductsInSet=PdpProductSetCount();
//Quantity box validations	
for(var $i=0;$i<$totalProductsInSet;$i++)
  {	
	var $cartQuantity=0;
	for(var $j=0;$j<$Qty_Validations.length;$j++)
		{
		if($j==0)
			{
				_setValue(_textbox("Quantity["+$i+"]"),"");
			}
		else
			{
				_setValue(_textbox("Quantity["+$i+"]"),$Qty_Validations[$j][1]);
			}
		if($j==0)
			{
				_click($PDP_ADDTOCART_BUTTON);
				var $actQuantity=_getText($MINICART_QUANTITY);
				//verifying the quantity
				_assertEqual("1",$actQuantity);
			}
		//out of stock
		if($j==3)
			{
				//not available message
				_assertVisible($PDP_PRODUCT_NOTAVAILABLE_MESSAGE);
				_assertEqual($Qty_Validations[3][2], _getText($PDP_PRODUCT_NOTAVAILABLE_MESSAGE));
				//add to cart should disable
				_assert(_getAttribute(_submit("Add to Cart["+$i+"]"),"disabled"));
			}
		else
			{
				//fetch quantity from mini cart before clicking
				 $cartQuantity=_getText($MINICART_QUANTITY);
				_log($cartQuantity);
				//click on add to cart button
				_click(_submit("button-fancy-medium sub-product-item add-to-cart["+$i+"]"));
				//default quantity should add to cart
				var $expQuantity=parseFloat($cartQuantity)+parseInt($Qty_Validations[0][2]);
				_log($expQuantity);
				var $actQuantity=_getText($MINICART_QUANTITY);
				//verifying the quantity
				_assertEqual($expQuantity,$actQuantity);
			}
		}
	ClearCartItems();
	//Navigate to product set
	search($PDP_Productset[0][0]);
	_click(_link($PDP_Productset[0][4]));
  }
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

ClearCartItems();
var $t = _testcase("124817","Verify the functionality of 'Add all to cart' button on product detail page With set of products in the application both as an anonymous and a registered user");
$t.start();
try
{
	
//Navigate to product set
search($PDP_Productset[0][0]);
_click(_link($PDP_Productset[0][4]));	
var $totalProductsInSet=PdpProductSetCount();
var $totalQty=0;	
//fetch the quantity present in the all boxes in set 
for(var $i=0;$i<$totalProductsInSet;$i++)
  {		
	$totalQty=parseFloat($totalQty)+parseFloat(_getText(_textbox("Quantity["+$i+"]")));
  }
//fetch quantity from mini cart before clicking
var $expCartQuantity=$totalQty;
//click on add all to cart
_click($PDP_PRODUCTSET_ADDALLTOCART_BUTTON);
var $actCartQuantity=_getText($MINICART_QUANTITY);
//verify the cart quantity
_assertEqual($expCartQuantity,$actCartQuantity);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();
//delete the existing user
deleteUser();
