_resource("PDPage.xls");

var $RootCatName=$PDP_Standard[0][8];
var $CatName=$PDP_Standard[1][8];

//delete the existing user
//deleteUser();


SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


var $t = _testcase("124731","Verify the UI of Product detail page for variation product in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to variation product
_click($PDP_SUBCATEGORY_SUITS);
var $PName=_getText($SHOPNAV_PLP_NAMELINK);
//click on name link
_click($SHOPNAV_PLP_NAMELINK);
//verify the UI of variation product
_assertVisible($PDP_PAGE_BREADCRUMB);
_assertVisible(_span($PName, _in($PDP_PAGE_BREADCRUMB)));
_assertVisible($PDP_PRODUCT_IMAGE);
_assertVisible($PDP_PRODUCT_NEXT_SECTION);
_assertVisible($PDP_PRODUCTNAME);
_assertVisible($PDP_PRODUCT_NUMBER);
_assertVisible($PDP_PRODUCT_PRICE);
//review ratings
_assertVisible($PDP_PRODUCT_RATING);
_assertVisible($PDP_PRODUCT_REVIEW);
//swatches
selectSwatch();

if(_isVisible($PDP_COLOR_LABEL))
	{
	_assertVisible($PDP_COLOR_LIST);
	}
if(_isVisible($PDP_SIZE_LABEL))
	{
	_assertVisible($PDP_SIZE_LABEL);
	}
if(_isVisible($PDP_WIDTH_LABEL))
	{
	_assertVisible($PDP_WIDTH_LABEL);
	}

//quantity
_assertVisible($PDP_QUANTITY_LABEL);
_assertVisible($PDP_QUANTITY_DROPDOWN);

//alternate images
if(_isVisible($PDP_PRODUCT_THUMBNAILS))
	{
	//_assertVisible(_heading2($PDP_Standard[0][1]));
	_assertVisible($PDP_PRODUCT_THUMBNAILS_SECTION);
	}
else
	{
	_assert("alternate images are not present");
	}
//recommendations section
if(_isVisible($PDP_PRODUCT_RECOMMENDATIONS_SECTION))
	{
	_assertVisible($PDP_YOUMIGHTALSOLIKE_SECTION);
	_assertVisible($PDP_PRODUCT_RECOMMENDATIONS_CAROUSEL_SECTION);
	}

//selecting swatches
selectSwatch();
//verify the presence of add to cart
_assertVisible($PDP_ADDTOCART_BUTTON);
//add to wish list
_assertVisible($PDP_ADDTOWISHLIST_LINK);
//add to gift registry
_assertVisible($PDP_ADD_PRODUCT_GIFTREGISTRY_LINK);
//description,reviews table
_assertVisible($PDP_YOUMIGHTALSOLIKE_SECTION);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148598","Verify the UI of Product detail page for Standard product in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to standard product
_click(_link($CatName));
var $PName=_getText($SHOPNAV_PLP_NAMELINK);
_click($SHOPNAV_PLP_NAMELINK);
//verify the UI
_assertVisible($PDP_PAGE_BREADCRUMB);
_assertVisible(_span($PName, _in($PDP_PAGE_BREADCRUMB)));
_assertVisible($PDP_PRODUCT_IMAGE);
_assertVisible($PDP_PRODUCTNAME);
_assertVisible($PDP_PRODUCT_NUMBER);
_assertVisible($PDP_PRODUCT_PRICE);
//next and previous links
_assertVisible($PDP_PRODUCT_NEXT_SECTION);
//review ratings
_assertVisible($PDP_PRODUCT_RATING);
_assertVisible($PDP_PRODUCT_REVIEW);
//availability
_assertVisible($PDP_PRODUCT_AVAILABILITY);
_assertVisible($PDP_PRODUCT_AVAILABILITY_DIV);
//swatches shouldn't be visible
_assertNotVisible($PDP_COLOR_LIST);
_assertNotVisible($PDP_SIZE_LABEL);
_assertNotVisible($PDP_WIDTH_LABEL);
//quantity
_assertVisible($PDP_QUANTITY_LABEL);
_assertVisible($PDP_QUANTITY_DROPDOWN);
//verify the presence of add to cart
_assertVisible($PDP_ADDTOCART_BUTTON);
//add to wish list
_assertVisible($PDP_ADDTOWISHLIST_LINK);
//add to gift registry
_assertVisible($PDP_ADD_PRODUCT_GIFTREGISTRY_LINK);
//description,reviews table
_assertVisible($PDP_YOUMIGHTALSOLIKE_SECTION);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124733","Verify the functionality of breadcrumbs on product detail page in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to root category	
_click(_link($PDP_Standard[0][4]));
//navigate to sub category
_click($PDP_SUBCATEGORY_SUITS);
//fetch the bread crumb
var $subCatBreadcrumb=_getText($PDP_PAGE_BREADCRUMB);
var $PName=_getText($SHOPNAV_PLP_NAMELINK);
//bread crumb 
var $breadCrumb=$subCatBreadcrumb+" "+$PName;
//click on name link
_click($SHOPNAV_PLP_NAMELINK);
//verify the presented breadcrumb
_assertEqual($breadCrumb,_getText($PDP_PAGE_BREADCRUMB));
_assertVisible(_span($PName, _in($PDP_PAGE_BREADCRUMB)));
//click on sub category link present in bread crumb
_click(_link($PDP_Standard[0][0],_in($PDP_PAGE_BREADCRUMB)));
//verify the navigation
_assertEqual($subCatBreadcrumb,_getText($PDP_PAGE_BREADCRUMB));
//navigate to PDP
_click($SHOPNAV_PLP_NAMELINK);
//click on root category link present in the bread crumb
_click(_link($PDP_Standard[1][4],_in($PDP_PAGE_BREADCRUMB)));
//Bread crumb should not visible for root category
_assertNotVisible($PDP_PAGE_BREADCRUMB);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124734","Verify the navigation links previous and next on product detail page  in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to sub category
_click($PDP_SUBCATEGORY_SUITS);	
var $prodNames = new Array();
var $numberOfProducts =YouMightalsoLikeProductCount();
$prodNames = YouMightalsoLikeProducts();
$name=_getText($SHOPNAV_PLP_NAMELINK)
//navigate to PD page on click of name link
_click($SHOPNAV_PLP_NAMELINK);

  for ($i=0; $i<$numberOfProducts-1; $i++)
  {
    var $name = _getText($PDP_PRODUCTNAME);
    _assertEqual($prodNames[$i],$name);
    _click($PDP_NEXT_LINK);
    //verify the presence of previous link
    if($i!=$numberOfProducts-2)
    	{
    		_assertVisible($PDP_PREVIOUS_LINK);
    	}
  }
  for($i=$numberOfProducts-2; $i>0; $i--)
  {
	  if($i==$numberOfProducts-2)
		{
		_click($PDP_PREVIOUS_LINK);
	  	}
	else
		{
		_click($PDP_PREVIOUS_LINK);
		}
	//verify the presence of next link
	    if($i!=$numberOfProducts-2)
    	{
	    	_assertVisible($PDP_NEXT_LINK);
    	}
    var $name = _getText($PDP_PRODUCTNAME);
    _assertEqual($prodNames[$i],$name);
  }	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//navigate to variation product
_click($PDP_SUBCATEGORY_SUITS);
var $PName=_getText($SHOPNAV_PLP_NAMELINK);
//click on name link
_click($SHOPNAV_PLP_NAMELINK);

var $t = _testcase("124736","Verify the functionality of attribute 'select color' on product detail page in the application both as an anonymous and a registered user");
$t.start();
try
{
//verify the select colour functionality 	
_assertVisible($PDP_COLOR_LIST);
var $colors=$SwatchesCountColor;
var $k=1;
//only one swatch anchor is there
if($colors==1)
{
	if(_isVisible($PDP_COLOR_LISTITEM_SELECTED))
		  {
		  var $title=_getAttribute($PDP_SWATCHANCHOR, _in($PDP_COLOR_LIST),"title").split(" ")[2];
		  //verify whether selected value is displaying or not
		  _assertVisible($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST));
		  _assertEqual($title, _getText($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST)));
		  }
	else
		  {
		  //click on swatchanchor
		  _click($PDP_SWATCHANCHOR, _in($PDP_COLOR_LIST));
		  var $title=_getAttribute($PDP_SWATCHANCHOR, _in($PDP_COLOR_LIST),"title");
		  //verify whether selected value is displaying or not
		  _assertVisible($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST));
		  _assertEqual($title, _getText($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST)));
		  }
}
//When there is more than one swatch anchor
else
{
 for(var $i=0;$i<$colors;$i++)
	{
		if($i==0 || $i==1)
			{
			var $a=_getText($PDP_SELECTABLE_SWATCH, _in($PDP_COLOR_LIST));
			_mouseOver(_link($a, _in($PDP_COLOR_LIST)));
			var $b=_getAttribute(_link($a), "title");
			_assertEqual($a,$b);
			_click(_link($a, _in($PDP_COLOR_LIST)));
			var $act=_getText($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST));
			_assertEqual($a,$act);
			}
		else
			{
			var $a=_getText(_listItem("selectable["+$k+"]", _in($PDP_COLOR_LIST)));
			_mouseOver(_link($a, _in($PDP_COLOR_LIST)));
			var $b=_getAttribute(_link($a), "title");
			_assertEqual($a,$b);
			_click(_link($a, _in($PDP_COLOR_LIST)));
			var $act=_getText($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST));
			_assertEqual($a,$act);
			$k++;
			}
		  }
	_assert(true, "Selected color is appropriate");
	}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124737","Verify the functionality of attribute 'Select size' on product detail page in the application both as an anonymous and a registered user");
$t.start();
try
{
_assertVisible($PDP_SIZE_LIST);
var $sizes =SwatchanchorCountSize();
var $j=1;
//only one swatchanchor is there
if($sizes==1)
{
	if(_isVisible($PDP_SIZE_LISTITEM_SELECTED))
		  {
		  var $title=_getAttribute($PDP_SWATCHANCHOR,_in($PDP_SIZE_LIST),"title").split(" ")[2];
		  //verify whether selected value is displaying or not
		  _assertVisible($PDP_SELECTED_SWATCH,_in($PDP_SIZE_LIST));
		  _assertEqual($title, _getText($PDP_SELECTED_SWATCH,_in($PDP_SIZE_LIST)));
		  }
	else
		  {
		  //click on swatch anchor
		  _click($PDP_SWATCHANCHOR,_in($PDP_SIZE_LIST));
		  var $title=_getAttribute($PDP_SWATCHANCHOR,"title");
		  //verify whether selected value is displaying or not
		  _assertVisible($PDP_SELECTED_SWATCH);
		  _assertEqual($title, _getText($PDP_SELECTED_SWATCH));
		  }
}
//more than one swatchanchor is there
else
  {
for (var $i=0; $i<$sizes; $i++)
    {
	  if($i==0 || $i==1)
		{
		var $a=_getText($PDP_SELECTABLE_SWATCH, _in($PDP_SIZE_LIST));
		_mouseOver(_link($a, _in($PDP_SIZE_LIST)));
		var $b=_getAttribute(_link($a), "title").split(" ")[2];
		_assertEqual($a,$b);
		_click(_link($a, _in($PDP_SIZE_LIST)));
		var $act=_getText($PDP_SELECTED_SWATCH, _in($PDP_SIZE_LIST));
		_assertEqual($a,$act);
		}
	else
		{
		var $a=_getText(_listItem("selectable["+$j+"]", _in($PDP_SIZE_LIST)));
		_mouseOver(_link($a, _in($PDP_SIZE_LIST)));
		var $b=_getAttribute(_link($a), "title").split(" ")[2];
		_assertEqual($a,$b);
		_click(_link($a, _in($PDP_SIZE_LIST)));
		var $act=_getText($PDP_SELECTED_SWATCH, _in($PDP_SIZE_LIST));
		_assertEqual($a,$act);
		$j++;
		}	
	_assert(true, "Selected size is appropriate");	
	}
  }     	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124743/124749","Verify the functionality of 'quantity box'/Add to cart on product detail page for in the application both as an anonymous and a registered user");
$t.start();
try
{
//select swatches
selectSwatch();
//enter quantity 5 in quantity box
_setValue($PDP_QUANTITY_DROPDOWN,$PDP_Standard[0][5]);
//verify whether entered value is present in quantity box or not
_assertEqual($PDP_Standard[0][5], _getValue($PDP_QUANTITY_DROPDOWN));
//Add to cart
_click($PDP_ADDTOCART_BUTTON);
//fetch quntity from mini cart
var $cartQuantity=_getText($MINICART_QUANTITY);
//verify the quantity in mini cart
_assertEqual($PDP_Standard[0][5],$cartQuantity);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

ClearCartItems();
var $t = _testcase("124753/124746","Verify the validation for 'Add to cart' button on product detail page  in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to sub category
_click($PDP_SUBCATEGORY_SUITS);
//click on name link
_click($SHOPNAV_PLP_NAMELINK);
//when no variants are selected verify the add to cart button
_assertNotVisible($PDP_ADDTOCART_BUTTON);
//select the variants
selectSwatch();
//now verify the add to cart dispaly
_assertVisible($PDP_ADDTOCART_BUTTON);
//quantity box validation
var $cartQuantity=0;
for(var $j=0;$j<$Qty_Validations.length;$j++)
	{
		if($j==0)
			{
				_setValue($PDP_QUANTITY_DROPDOWN,"");
			}
		else
			{
				_setValue($PDP_QUANTITY_DROPDOWN,$Qty_Validations[$j][1]);
			}
		if($j==0)
			{
				_click($PDP_ADDTOCART_BUTTON);
				_wait(2000);
				var $actQuantity=_getText($MINICART_QUANTITY);
				//verifying the quantity
				_assertEqual("1",$actQuantity);
			}
		//out of stock
		if($j==3)
			{
				//not available message
				_assertVisible($PDP_PRODUCT_NOTAVAILABLE_MESSAGE);
				_assertEqual($Qty_Validations[3][2], _getText($PDP_PRODUCT_NOTAVAILABLE_MESSAGE));
				//add to cart should disable
				_assertNotVisible($PDP_ADDTOCART_DISABLED_BUTTON);
			}
		else
			{						
				var $cartQuantity=_getText($MINICART_QUANTITY);
				_log($cartQuantity);
				//click on add to cart button
				_click($PDP_ADDTOCART_BUTTON);
				//default quantity should add to cart
				var $expQuantity=parseFloat($cartQuantity)+parseInt($Qty_Validations[0][2]);
				var $actQuantity=_getText($MINICART_QUANTITY);
				//verifying the quantity
				_assertEqual($expQuantity,$actQuantity);
			}		
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124789","Verify the functionality of You may also like(Alternate products) on Product detail page for standard product in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to PDP
search($PDP_Standard[1][2]);
var $Product=_getText($SHOPNAV_PLP_NAMELINK);
if(_isVisible($SHOPNAV_PLP_NAMELINK, _in($SHOPNAV_PLP_PRODUCTNAME_SECTION, _in($SHOPNAV_PLP_PRODUCTTILE_SECTION))))
	{
	_click(_link($Product, _in($SHOPNAV_PLP_PRODUCTNAME_SECTION, _in($SHOPNAV_PLP_PRODUCTTILE_SECTION))));
	}
//verify the presence of You may also like Section
if(_isVisible($PDP_PRODUCT_RECOMMENDATIONS_SECTION))
	{ 
	var $count=PDPYouMightalsoLikeProductTooltipCount();
	for(var $i=0;$i<$count;$i++)
		{
		//get the product name
		var $PName=_getText($SHOPNAV_PLP_PRODUCTNAME_SECTION, _in(_div("product-tile tooltip["+$i+"]")));
		//click on product image
		_click(_image("/(.*)/",_in(_div("product-tile tooltip["+$i+"]"))));
		//verify the navigation
		verifyNavigation($PName);
		//navigate to PDP
		search($PDP_Standard[1][2]);
		//click on product name link in search page
		if(_isVisible($SHOPNAV_PLP_NAMELINK, _in($SHOPNAV_PLP_PRODUCTNAME_SECTION, _in($SHOPNAV_PLP_PRODUCTTILE_SECTION))))
			{
			_click(_link($Product, _in($SHOPNAV_PLP_PRODUCTNAME_SECTION, _in($SHOPNAV_PLP_PRODUCTTILE_SECTION))));
			}
		//click on product name link in PDP
		_click(_link("/(.*)/",_in($SHOPNAV_PLP_PRODUCTNAME_SECTION, _in(_div("product-tile tooltip["+$i+"]")))));
		//verify the navigation
		verifyNavigation($PName);
		//navigate to PDP
		search($PDP_Standard[1][2]);
		if(_isVisible($SHOPNAV_PLP_NAMELINK, _in($SHOPNAV_PLP_PRODUCTNAME_SECTION, _in($SHOPNAV_PLP_PRODUCTTILE_SECTION))))
		{
		_click(_link($Product, _in($SHOPNAV_PLP_PRODUCTNAME_SECTION, _in($SHOPNAV_PLP_PRODUCTTILE_SECTION))));
		}
		}
	//verify the carosal
	if($count>3)
		{
		_assertVisible($PDP_CAROUSEL_NEXT);
		//click on next link
		_click($PDP_CAROUSEL_NEXT);
		//previous should display
		_assertVisible($PDP_CAROUSEL_PREVIOUS);
		_click($PDP_CAROUSEL_PREVIOUS);
		//next link should display
		_assertVisible($PDP_CAROUSEL_NEXT);
		}
	}
else
	{
	_assert(false,"recomendation section is not present for this product");
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124791", "Verify the UI of product tabs on product detail page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to PDP
	search($PDP_Standard[1][2]);	
	_click($SHOPNAV_PLP_NAMELINK);		
	//verify the UI of product tab
	_assertVisible($PDP_DESCRIPTION_TAB);
	_assertVisible($PDP_PRODUCTDETAILS_TAB);
	_assertVisible($PDP_REVIEW_TAB);
	_assertVisible($PDP_YOUMIGHTALSOLIKE_SECTION);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124793", "Verify the Functionaity of product tabs on product detail page in the application both as an anonymous and a registered user");
$t.start();
try
{
	
	//navigate to PDP
	search($PDP_Standard[1][2]);	
	_click($SHOPNAV_PLP_NAMELINK);	
	
	var $BottomSection=PDPBottomTabs();
	
	for(var $i=0;$i<$BottomSection.length;$i++)
		{
		_click(_label($BottomSection[$i]));
		_assertVisible($PDP_BOTTOM_TAB_CONTENTS);
		_assertEqual($BottomSection[$i],_getText(_label("tab-label["+$i+"]", _in($PDP_TAB_SECTION))));
		}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124739", "Verify the functionality of 'Size chart' link on product detail page for in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to womens category tops subcategory
	_click(_link($PDP_Standard[2][0]));
	//click on name link
	_click($SHOPNAV_PLP_NAMELINK);
	//verify the presence of size chart
	_assertVisible($PDP_SIZECHART_LINK);
	_click($PDP_SIZECHART_LINK);
	_wait(2000, _isVisible($PDP_SIZECHART_URL));
	if (_isVisible($PDP_SIZECHART_CONTENT))       
	     {
	       _assert(true, "Size chart dialogue is opened successfully");
	     }
	   else  
		 {
		   _assert(false, "Size chart window is not opened");
		 }
	_assertVisible($PDP_SIZECHART_CLOSE_BUTTON);
	_click($PDP_SIZECHART_CLOSE_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


cleanup();
//delete the existing user
deleteUser();
