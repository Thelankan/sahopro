_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("PDPage.xls");


//delete the existing user
deleteUser();

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


var $t = _testcase("124841","Verify the UI of Product detail page for bundled products in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to bundle product page
//category
_click(_link($PDP_ProductBundle[0][3]));
//sub category
_click(_link($PDP_ProductBundle[0][4]));
//product name
_click(_link($PDP_ProductBundle[0][0]));
//verify the UI of bundle product
//image
_assertVisible($PDP_PRIMARY_MAIN_IMAGE);
//name
_assertVisible($PDP_PRODUCTNAME);
_assertEqual($PDP_ProductBundle[0][0], _getText($PDP_PRODUCTNAME));
//number
_assertVisible($PDP_PRODUCT_NUMBER);
//next and previous
_assertVisible($PDP_PRIMARY_NAV_CONTAINER);
//price
_assertVisible($PDP_PRODUCT_PRICE);
//fetch the total count present in the bundle
var $totalProductsInSet=PdpProductBundleCount();
for(var $i=0;$i<$totalProductsInSet;$i++)
	{
	//image
	_assertVisible(_div("product-set-image", _in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//name
	_assertVisible(_link("item-name",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//number
	_assertVisible(_div("product-number",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//availability
	_assertVisible(_div("availability-msg",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//quantity
	_assertVisible(_span($PDP_ProductBundle[0][1],_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	_assertVisible(_span("value",_near(_span("QUANTITY:",_in(_div("product-set-item product-bundle-item["+$i+"]"))))));
	//product options
	if(_isVisible(_div("product-options["+$i+"]")))
		{
		_assertVisible(_div("product-options["+$i+"]"));
		}
	}
//alternate images
if(_isVisible($PDP_PRODUCT_THUMBNAILS))
	{
	//_assertVisible(_heading2($PDP_Standard[0][1]));
	_assertVisible($PDP_PRODUCT_THUMBNAILS_SECTION);
	}
else
	{
	_assert("alternate images are not present");
	}

//add to wish list
_assertVisible($PDP_ADDTOWISHLIST_LINK);
//add to gift registry
_assertVisible($PDP_ADD_PRODUCT_GIFTREGISTRY_LINK);
//price at bottom
_assertVisible($PDP_PRODUCTBUNDLE_TOTALPRICE);
//add all to cart
_assertVisible($PDP_PRODUCTBUNDLE_ADDTOCART_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124846","Verify the functionality of breadcrumbs on product detail page for bundled products in the application both as an anonymous and a registered user");
$t.start();
try
{
//click on sub sub category present in breadcrumb
_click(_link($PDP_ProductBundle[0][4],_in($PAGE_BREADCRUMB)));
//verify the navigation
_assertVisible(_link($PDP_ProductBundle[0][4],_in($PAGE_BREADCRUMB)));
//navigate to bundle product page
search($PDP_ProductBundle[0][0]);
//click on sub category present in breadcrumb
_click(_link($PDP_ProductBundle[0][3],_in($PAGE_BREADCRUMB)));
//verify the navigation
_assertVisible(_link($PDP_ProductBundle[0][3],_in($PAGE_BREADCRUMB)));
//navigate to bundle product page
search($PDP_ProductBundle[0][0]);
//click on category present in breadcrumb
_click($PDP_ProductBundle[0][2],_in($PAGE_BREADCRUMB));
//verify the navigation
_assertVisible(_link($PDP_ProductBundle[0][2],_in($PAGE_BREADCRUMB)));
//navigate to bundle product page
search($PDP_ProductBundle[0][0]);
//click on home link
_assertNotVisible(_link($PDP_ProductBundle[0][7],_in($PAGE_BREADCRUMB)));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124848","Verify the links previous and next on product detail page For bundled products in the application both as an anonymous and a registered user");
$t.start();
try
{
//category
_click(_link($PDP_ProductBundle[0][3]));
//sub category
_click(_link($PDP_ProductBundle[0][4]));
//collect all the product names
var $prodNames = new Array();
var $numberOfProducts = YouMightalsoLikeProductCount();
$prodNames = YouMightalsoLikeProducts();
//product name
_click($SHOPNAV_PLP_NAMELINK);
for ($i=0; $i<$numberOfProducts-1; $i++)
{
  var $name = _getText($PDP_PRODUCTNAME);
  _assertEqual($prodNames[$i],$name);
  _click($PDP_NEXT_LINK);
  //verify the presence of previous link
  if($i!=$numberOfProducts-2)
		{
		  	_assertVisible($PDP_PREVIOUS_LINK);
		}
}
for($i=$numberOfProducts-2; $i>0; $i--)
{
	if($i==$numberOfProducts-2)
		{
		_click($PDP_PREVIOUS_LINK);
	  	}
	else
		{
		_click($PDP_PREVIOUS_LINK);
		}
	
	//verify the presence of next link
	if($i!=$numberOfProducts-2)
	  	{
			_assertVisible($PDP_NEXT_LINK);
	  	}
  var $name = _getText($PDP_PRODUCTNAME);
  _assertEqual($prodNames[$i],$name);
}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124851","Verify the functionality of product names  on  Product detail page for all products in the bundle in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to bundle product page
search($PDP_ProductBundle[0][0]);	
var $totalProductsInSet=PdpProductBundleCount();	
for(var $i=0;$i<$totalProductsInSet;$i++)
	{	
	//fetch the product name
	var $pName=_getText(_link("item-name["+$i+"]"));
	//click on name link
	_click(_link("item-name["+$i+"]"));
	//verify the navigation
	_assertVisible(_span($pName,_in($PAGE_BREADCRUMB)));
	_assertVisible($PDP_PRODUCTNAME);
	_assertEqual($pName, _getText($PDP_PRODUCTNAME));
	//navigate to bundle product page
	search($PDP_ProductBundle[0][0]);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124853","Verify the functionality of 'Extended warranty' drop dowm on product Detail page for all products in the bundle in the application both as an anonymous and a registered user");
$t.start();
try
{
//verify the presence of warranty drop down	
_assertVisible($PDP_PRODUCTBUNDLE_TOTALPRICE);	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

ClearCartItems();
var $t = _testcase("124856","Verify the functionality of 'Add to cart' button on product detail  Page for all products in the set in the application both as an anonymous and a registered user");
$t.start();
try
{
	
//navigate to bundle product page
search($PDP_ProductBundle[0][0]);
//fetch product name 
var $PName=_getText($PDP_PRODUCTNAME);
var $productsInBundle=new Array();
var $totalProductsInSet=PdpProductSetCount();	
for(var $i=0;$i<$totalProductsInSet;$i++)
	{	
	$productsInBundle[$i]=_getText(_link("item-name["+$i+"]"));
	}
//var $cartQuantity=_getText($MINICART_QUANTITY);
//click on add to cart
_click($PDP_PRODUCTBUNDLE_ADDTOCART_BUTTON);
//verify the cart quantity
var $expQuantity=$Qty_Validations[0][2];
var $actQuantity=parseInt(_getText($MINICART_QUANTITY));
_assertEqual($expQuantity,$actQuantity);
//verify weather same products are added or not
_click($PDP_MINICART_OVERLAY_VIEWCART_LINK);
_assertEqual($PName,_getText($PDP_CART_NAME_LINK));
var $actBundleproducts=PdpProductBundleCountInCartPage();
var $j=0;
for(var $i=1;$i<=$actBundleproducts;$i++)
	{
	_assertEqual($productsInBundle[$j],_getText(_link("/.*/",_in(_div("name["+$i+"]")))));
	$j++;
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124858","Verify the link 'Add to wishlist' on product detail page for bundled products in the application both as an anonymous and a registered user");
$t.start();
try
{
//click on add to wishlist link
_click($PDP_ADDTOWISHLIST_LINK);
//verify the navigation
_assertVisible(_link($PDP_ProductBundle[0][5],_in($PAGE_BREADCRUMB)));
_assertVisible(_heading1($PDP_ProductBundle[1][5]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124859","Verify the link 'Add to gift registry' on product detail page for bundled products in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to bundle product page
search($PDP_ProductBundle[0][0]);	
//click on link gift registry
_click($PDP_ADD_PRODUCT_GIFTREGISTRY_LINK);
//verify the navigation
_assertVisible(_link($PDP_ProductBundle[0][6],_in($PAGE_BREADCRUMB)));
_assertVisible(_heading1($PDP_ProductBundle[1][6]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();