_includeOnce("../ObjectRepository/Generic_OR.sah");

var $widthBoolean=false;
var $colorBoolean=false;
var $sizeBoolean=false;

//******************************Search**************************
 //search related function
 function search($product)
 {
 	if(isMobile())
 		{
 			_navigateTo(document.getElementsByName("simpleSearch")[0].getAttribute("action") + "?q=" + $product);
 		}
 	else
 		{
 			_setValue($HEADER_SEARCH_TEXTBOX, $product);	
 			_click($HEADER_SEARCH_ICON);
 		} 	
 }
  
//******************************PLP**************************
function navigateCatPage()
{
 _click(_link($RootCatName));
 _log("We are now on " + $RootCatName + " landing page");
 _wait(2000);
 _click(_link($CatName));
 _log("We are now on " + $CatName + " landing page");
 _wait(2000);
}

//******************************PDP**************************
 function selectSwatch()
 {
		//select swatches
		if(_isVisible($PDP_COLOR_SECTION))
		    {
		    $colorBoolean=true;
		    if(!_isVisible($PDP_COLOR_LISTITEM_SELECTED))
		           {
		           _click($PDP_COLOR_SWATCH);
		           }
		    }
		if(isMobile())
		    {
		    _wait(4000);
		    }
		if(_isVisible($PDP_SIZE_SECTION))
		    {
		    $sizeBoolean=true;
		    if(!_isVisible($PDP_SIZE_LISTITEM_SELECTED))
		           {
		           _click($PDP_SIZE_SWATCH);
		           }
		    }
		if(isMobile())
		{
		_wait(4000);
		}
		if(_isVisible($PDP_WIDTH_SECTION))
		    {
		    $widthBoolean=true;
		           if(!_isVisible($PDP_WIDTH_LISTITEM_SELECTED))
		           {
		           _click($PDP_WIDTH_SWATCH);
		           }
		    }
 } 
 
 function verifyNavigation($PName)
 {
 	_assertVisible($PDP_PAGE_BREADCRUMB);
 	_assertVisible(_span($PName, _in($PDP_PAGE_BREADCRUMB)));
 	_assertVisible($PDP_PRODUCTNAME);
 	_assertEqual($PName, _getText($PDP_PRODUCTNAME));	
 }
 
function addToCartSearch($Item,$qty)
{
	//search product
	search($Item);
	//selecting swatches
	selectSwatch();
	//set the quantity
	_setValue($PDP_QUANTITY_DROPDOWN,$qty);
	//click on add to cart
	_click($PDP_ADDTOCART_BUTTON);
}

function addItemToCart($subCat,$i)
{
	//navigating to sub category
	_click(_link($subCat));
	//navigate to PDP
	_click(_link("name-link"));
	//selecting swatches
	selectSwatch();
	//set the quantity
	_setValue(_textbox("Quantity"),$i);
	//click on add to cart
	_click(_submit("/add-to-cart/"));
}

function addGiftCertificate($Data, $i)
{
	//click on gift certificate
	_click($HOME_GIFTCERTIFICATE_LINK);
	//enter the gift certificate details
	addGiftCertificate($Data, $i);
	//click on add to cart
	_click($GIFTCERTIFICATE_ADDTOCART);
}

function GiftCertificate($Data, $i)
{
	_setValue($GIFTCERTIFICATE_YOURNAME, $Data[$i][1]);
	_setValue($GIFTCERTIFICATE_FRIENDSNAME, $Data[$i][2]);
	_setValue($GIFTCERTIFICATE_FRIENDSMAIL, $Data[$i][3]);
	_setValue($GIFTCERTIFICATE_CONFIRM_FRIENDSMAIL, $Data[$i][4]);
	_setValue($GIFTCERTIFICATE_MESSAGE, $Data[$i][5]);
	_setValue($GIFTCERTIFICATE_AMOUNT, $Data[$i][6]);	
}
 

 

//--------Pagination function
function pagination()
{  
	if(_getText(_div("results-hits"))==_getText(_div("/pagination/")))
		{
			_assert(true,"Single Page is displayed");		
		}
	else
		{
			if((_isVisible(_link("page-next")))&&(_isVisible(_link("page-last"))))
				{	
					_assert(_isVisible(_link("page-next")));
					_assert(_isVisible(_link("page-last")));
					_assertEqual(false,_isVisible(_link("page-previous")));
					_assertEqual(false,_isVisible(_link("page-first")));
					if(_isVisible(_link("page-last")))
						{
							_click(_link("page-last"));
						}
					_assert(_isVisible(_link("page-previous")));
					_assert(_isVisible(_link("page-first")));
					_assertEqual(false,_isVisible(_link("page-next")));
					_assertEqual(false,_isVisible(_link("page-last")));
					var $pages=_getText(_listItem("current-page"));
					if(_isVisible(_link("page-first")))
						{
							_click(_link("page-first"));
						}				
					for(var $j=1;$j<=$pages;$j++)
						{
							if($j==1)
								{
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);
								}
							else
								{
									_click(_link("page-next"));
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom)					
								}				
						}
					for(var $j=$pages;$j>=1;$j--)
						{
							if($j==$pages)
								{
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);
								}
							else
								{
									_click(_link("page-previous"));
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom)						
								}				
						}
					for(var $j=1;$j<=$pages;$j++)
						{
							if($j==1)
								{
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);
								}
							else
								{
									_click(_link("page-"+$j));
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom)							
								}				
						}
				 }
			else
				 {
					var $pageDisp=_count("_listItem","/(.*)/",_in(_div("pagination")));
					for(var $j=1;$j<=$pageDisp;$j++)
						{
							if($j==1)
								{
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);
								}
							else
								{
									_click(_link("page-"+$j));
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);					
								}				
						}				
				 }
		}
}

//--------Items per page function---------------
function ItemsperPage($itemsExpected)
{
	var $pagination=_getText(_div("pagination"));
	var $ActTotalItems=_extract($pagination, /of (.*) Results/, true).toString();
	var $ExpTotalItems=0;
	var $itemsDisplayed=0;
	if(_getText(_div("results-hits"))==_getText(_div("/pagination/")))
		{
		
			$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));//_count("_div", "/product-tile/",_in(_list("search-result-items")));
			_assertEqual($itemsDisplayed,$ActTotalItems, "Single Page is displayed");		
		}
	else
		{
			if((_isVisible(_link("page-next")))&&(_isVisible(_link("page-last"))))
			{	
				
				while(_isVisible(_link("page-last")))
					{
						_click(_link("page-last"));
					}
				var $pages=_getText(_listItem("current-page"));
				while(_isVisible(_link("page-first")))
					{
						_click(_link("page-first"));
					}
				
				for(var $j=1;$j<=$pages;$j++)
					{
						if($j==1)
							{
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								_assertEqual($itemsExpected,$itemsDisplayed);
							}
						else if($j==$pages)
							{
								_click(_link("page-"+$j));
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								var $LastPageItems=$ActTotalItems%$itemsExpected;						
								_assertEqual($LastPageItems,$itemsDisplayed);
							}
						else 
							{
								_click(_link("page-"+$j));
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								_assertEqual($itemsExpected,$itemsDisplayed);
							}
						$ExpTotalItems=$ExpTotalItems+$itemsDisplayed;
					 }				
				}
			else
				{
					var $pageDisp=_count("_listItem","/(.*)/",_in(_div("/pagination/")));
					for(var $j=1;$j<=$pageDisp;$j++)
					{
						if($j==1)
							{
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								_assertEqual($itemsExpected,$itemsDisplayed);
							}
						else if($j==$pageDisp)
							{
								_click(_link("page-"+$j));
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								var $LastPageItems=$ActTotalItems%$itemsExpected;						
								_assertEqual($LastPageItems,$itemsDisplayed);
							}
						else 
							{
								_click(_link("page-"+$j));
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								_assertEqual($itemsExpected,$itemsDisplayed);
							}
						$ExpTotalItems=$ExpTotalItems+$itemsDisplayed;
					 }				
				}
			_assertEqual($ExpTotalItems,$ActTotalItems);
		}
}


//Price filter
function priceCheck($Range)
{
	var $priceBounds = _extract(_getText(_link($Range)), "/[$](.*) - [$](.*)/",true).toString().split(",");
	var $minPrice = $priceBounds[0].toString();
	var $maxPrice = $priceBounds[1].toString();
	_log("$minPrice = " + $minPrice);
	_log("$maxPrice = " + $maxPrice);
	if(_getText(_div("results-hits"))==_getText(_div("/pagination/")))
	{
		var $prices = _collectAttributes("_span", "/product-sales-price/","sahiText");
		for (var $k = 0; $k < $prices.length; $k++) 
			{
				var $price = parseFloat(_extract($prices[$k],"/[$](.*)/",true)[0].replace(",",""));				
				_log("$price = " + $price);				
				if ($price < $minPrice) 
				{
					_assert(false, "Price " + $price + " is lesser than min price " + $minPrice);
				} 
				else if ($price > $maxPrice) 
				{
					_assert(false, "Price " + $price + " is greater than max price " + $maxPrice);
				}					
			}		
	}
	else
	{
		if((_isVisible(_link("page-next")))&&(_isVisible(_link("page-last"))))
			{
				if(_isVisible(_link("page-last")))
					{
						_click(_link("page-last"));
					}
				var $pages=_getText(_listItem("current-page"));
				if(_isVisible(_link("page-first")))
					{
						_click(_link("page-first"));
					}
			}
		else
			 {
				var $pages=_count("_listItem","/(.*)/",_in(_div("pagination")));
			 }						
		for(var $j=1;$j<=$pages;$j++)
			{
				if(!$j==1)
					{
						_click(_link("page-next"));							
					}
				var $prices = _collectAttributes("_span", "/product-sales-price/","sahiText");
				for (var $k = 0; $k < $prices.length; $k++) 
					{
						var $price = parseFloat(_extract($prices[$k],"/[$](.*)/",true)[0].replace(",",""));				
						_log("$price = " + $price);				
						if ($price < $minPrice) 
						{
							_assert(false, "Price " + $price + " is lesser than min price " + $minPrice);
						} 
						else if ($price > $maxPrice) 
						{
							_assert(false, "Price " + $price + " is greater than max price " + $maxPrice);
						}					
					}
			}
	}
}

//--------Sort By function--------------
function sortBy($opt)
{ 
	if($opt=="Price Low To High")
	{	
		sortAllPage($opt,"_span", "/product-sales-price/");
	}     
else if($opt=="Price High To Low")
	{
		sortAllPage($opt,"_span","/product-sales-price/");
	}
else if($opt=="Product Name A - Z")
	{
		sortAllPage($opt,"_link","/name-link/");            
	}
else if($opt=="Product Name Z - A")
	{
		sortAllPage($opt,"_link","/name-link/");        
	}	    
}

function sortAllPage($opt,$tag,$field)
{
	var $arr = new Array();
	var $temp =new Array();
	if(_getText(_div("results-hits"))==_getText(_div("/pagination/")))
		{
			$arr = _collectAttributes($tag,$field,"sahiText");
			assertValues($opt,$arr);		
		}
	else
		{
			if((_isVisible(_link("page-next")))&&(_isVisible(_link("page-last"))))
			{	
				
				while(_isVisible(_link("page-last")))
					{
						_click(_link("page-last"));
					}
				var $pages=_getText(_listItem("current-page"));
				while(_isVisible(_link("page-first")))
					{
						_click(_link("page-first"));
					}					
					for(var $i=1;$i<=$pages;$i++)
						{
							if($i==1)
								{
									$temp = _collectAttributes($tag,$field,"sahiText");
									$arr.push($temp);
								}
							else
								{
									_click(_link("page-"+$i));
									$temp = _collectAttributes($tag,$field,"sahiText");
									$arr.push($temp);									
								}
						}
					assertValues($opt,$arr);
				}
			else
				{
					var $pageDisp=_count("_listItem","/(.*)/",_in(_div("/pagination/")));
					for(var $i=1;$i<=$pageDisp;$i++)
						{
							if($i==1)
								{
									$temp = _collectAttributes($tag,$field,"sahiText");
									$arr.push($temp);								
								}
							else
								{
									_click(_link("page-"+$i));
									$temp = _collectAttributes($tag,$field,"sahiText");
									$arr.push($temp);																	
								}				
						}
					assertValues($opt,$arr);
				}
		}  	   
}

function assertValues($opt,$arr)
{
 	_log("Collected array-----:"+$arr);
    var $arrExp=$arr.sort();
	 if($opt=="Product Name Ascending" ||$opt=="Price Low To High")
		{
	 		$arrExp=$arr.sort();
		}
	 else if($t=="Product Name Descending" ||$opt=="Price High to Low")
		{
	  		$arrExp=$arr.sort().reverse();
		} 
	 _log("Sorted array-----:"+$arrExp);
    _assertEqualArrays($arrExp,$arr);
}

 