_includeOnce("../ObjectRepository/Generic_OR.sah");
//******************************Login**************************

function login()
{	 
	_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX, $uId);
	_setValue($LOGIN_PASSWORD_TEXTBOX, $pwd);
	if(_isVisible($LOGIN_BUTTON))
	{
		_click($LOGIN_BUTTON);
	}
	else
	{
		_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	}
}

function logout()
{
	//logout from the application
	if(_isVisible($HEADER_USERACCOUNT_LINK))
	{
		_click($HEADER_USERACCOUNT_LINK);
		if(_isVisible($HEADER_LOGOUT_LINK))
		{		
			_click($HEADER_LOGOUT_LINK);	
		}
	}	
}

function CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode)
{
	_setValue($LOGIN_ORDERNUMBER_TEXTBOX,$OrderNumber);
	_setValue($LOGIN_ORDEREMAIL_TEXTBOX,$UserEmail);
	_setValue($LOGIN_ORDER_ZIPCODE_TEXTBOX,$Zipcode);
	_click($LOGIN_ORDER_SUBMITBUTTON);
}
//******************************Create Account**************************
function createAccount()
{
	_setValue($REGISTER_FIRSTNAME_TEXTBOX, $userData[$user][1]);
	_setValue($REGISTER_LASTNAME_TEXTBOX, $userData[$user][2]);
	_setValue($REGISTER_EMAIL_TEXTBOX, $userData[$user][3]);
	_setValue($REGISTER_CONFIRM_EMAIL_TEXTBOX, $userData[$user][4]);
	_setValue($REGISTER_PASSWORD_TEXTBOX, $userData[$user][5]);
	_setValue($REGISTER_CONFIRM_PASSWORD_TEXTBOX, $userData[$user][6]); 	 
	if(_isVisible($REGISTER_SUBMIT_BUTTON))
	{
		_click($REGISTER_SUBMIT_BUTTON); 
	} 	 
} 

//******************************Addresses**************************

function deleteAddress()
{
	_click($HEADER_USERACCOUNT_LINK);
	clearWishList();
	Delete_GiftRegistry();
	_click($LEFTNAV_ADDRESS_LINK);
	//deleting the existing addresses
	
	if(_isVisible($ADDRESSES_DELETE_LINK))
	{
		var $Totalno_Address =_count("_div","/mini-address-location/", _in($ADDRESSES_ADDRESSES_LIST))
		_log($Totalno_Address);
		
		    //deleting the existing addresses
		for(var $i=1; $i<=$Totalno_Address; $i++)
				{
					_click($ADDRESSES_DELETE_LINK);
					_wait(2000);
					_expectConfirm("/Do you want/",true);
					_wait(3000);
		        }
		  _log($i);
		  	_assertNotVisible($ADDRESSES_ADDRESSES_LIST)
			 _assertNotVisible($ADDRESSES_DELETE_LINK);

	}
else
	{
	 _assertNotVisible($ADDRESSES_ADDRESSES_LIST)
	 _assertNotVisible($ADDRESSES_DELETE_LINK);
	}	
	
		
//	while(_isVisible($ADDRESSES_DELETE_LINK))
//	{
//		_click($ADDRESSES_DELETE_LINK);
//		_expectConfirm("/Do you want/",true);
//	}
}

function addAddress($validations,$r)
{
	_setValue($ADDRESSES_ADDRESSNAME_TEXTBOX, $validations[$r][1]);
	_setValue($ADDRESSES_FIRSTNAME_TEXTBOX, $validations[$r][2]);
	_setValue($ADDRESSES_LASTNAME_TEXTBOX, $validations[$r][3]);
	_setValue($ADDRESSES_ADDRESS1_TEXTBOX, $validations[$r][4]);
	_setValue($ADDRESSES_ADDRESS2_TEXTBOX, $validations[$r][5]);
	_setSelected($ADDRESSES_COUNTRY_DROPDOWN,$validations[$r][6]);
	_setSelected($ADDRESSES_STATE_DROPDOWN, $validations[$r][7]);
	_setValue($ADDRESSES_CITY_TEXTBOX, $validations[$r][8]);
	_setValue($ADDRESSES_ZIPCODE_TEXTBOX, $validations[$r][9]);
	_setValue($ADDRESSES_PHONE_TEXTBOX,$validations[$r][10]);	
}


//******************************Wish List**************************


function clearWishList()
{
	_click($HEADER_USERACCOUNT_LINK);
	_click($WISHLIST_LINK);
	if (_isVisible($WISHLIST_SHIPPINGADDRESS_DROPDOWN))
	{
		_setSelected($WISHLIST_SHIPPINGADDRESS_DROPDOWN,0);	 
	}
	while(_isVisible($WISHLIST_REMOVE_BUTTON))
	{
		_click($WISHLIST_REMOVE_BUTTON);
	} 
}

function deleteAddeditemsFromWishlistAccount()
{
	//login to the application
	_click($HEADER_LOGIN_LINK);
	//login to application for which account we have added item
	login();
	//clearing items from wishlist
	clearWishList();
	//logout from application
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_LOGOUT_LINK);
}

function additemsToWishListAccount($cat)
{
	//login to the application
	_click($HEADER_LOGIN_LINK);
	//login to application for which account we want to add item
	login();
	//clear items from wish list
	clearWishList();
	//add items
	_click(_link($cat));
	_click($SHOPNAV_PLP_NAMELINK);
	//selecting swatches
	selectSwatch();
	//click on wish list
	_click($WISHLIST_ADDTOWISHLIST_LINK);
	//navigate to wish list
	_click($WISHLIST_LINK);
	//click on make this as public if it is not having public visibility
	if(_isVisible($WISHLIST_MAKETHISPUBLIC_BUTTON))
	{
		_click($WISHLIST_MAKETHISPUBLIC_BUTTON);
	}
	//logout from application
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_LOGOUT_LINK);
}

//******************************Gift Registry**************************

function Delete_GiftRegistry()
{
	_click($GIFTREGISTRY_LINK);
	while(_isVisible($GIFTREGISTRY_DELETE_REGISTRY_LINK))
	{  
		_click($GIFTREGISTRY_DELETE_REGISTRY_LINK);
		_expectConfirm("/Do you want/",true);
		// _expectConfirm("Do you want to remove this gift registry?", true); 
		_log("Gift Registry deleted successfully");
	}
}

function createRegistry($Sheet, $i)
{
	_setValue($CREATEGIFTREGISTRY_EVENTTYPE_DROPDOWN,$Sheet[$i][1]);
	_setValue($CREATEGIFTREGISTRY_EVENTNAME_TEXTBOX,$Sheet[$i][2]);
	_setValue($CREATEGIFTREGISTRY_EVENTDATE_DATEBOX,$Sheet[$i][3]);
	//_setSelected($CREATEGIFTREGISTRY_EVENTCOUNTRY_TEXTBOX,$Sheet[$i][4]);
	//_setSelected($CREATEGIFTREGISTRY_EVENTSTATE_TEXTBOX,$Sheet[$i][5]);
	_setValue($CREATEGIFTREGISTRY_EVENTCITY_TEXTBOX,$Sheet[$i][6]);
	_setSelected($CREATEGIFTREGISTRY_ROLE_TEXTBOX,$Sheet[$i][7]);
	_setValue($CREATEGIFTREGISTRY_FP_FIRSTNAMETEXTBOX,$FirstName1);
	_setValue($CREATEGIFTREGISTRY_FP_LASTNAMETEXTBOX,$LastName1);
	_setValue($CREATEGIFTREGISTRY_FP_EMAILTEXTBOX,$UserEmail);
	_setSelected($CREATEGIFTREGISTRY_SECONDPARTICIPANT_ROLE_DROPDOWN,$Sheet[$i][11]);
	_setValue($CREATEGIFTREGISTRY_SP_FIRSTNAMETEXTBOX,$Sheet[$i][12]);
	_setValue($CREATEGIFTREGISTRY_SP_LASTTNAMETEXTBOX,$Sheet[$i][13]);
	_setValue($CREATEGIFTREGISTRY_SP_EMAILTEXTBOX,$Sheet[$i][14]);
	_click($GIFTREGISTRY_PREPOST_CONTINUE_BUTTON);	
}

function Pre_PostEventInfo($Sheet, $i)
{
	_setValue( $GIFTREGISTRY_PRE_EVENT_FIRSTNAME_TEXTBOX,$Sheet[$i][1]);
	_setValue($GIFTREGISTRY_PRE_EVENT_LASTNAME_TEXTBOX,$Sheet[$i][2]);
	_setValue($GIFTREGISTRY_PRE_EVENT_ADDRESS1_TEXTBOX,$Sheet[$i][3]);
	_setValue($GIFTREGISTRY_PRE_EVENT_ADDRESS2_TEXTBOX,$Sheet[$i][4]);
	_setSelected($GIFTREGISTRY_PRE_EVENT_COUNTRY_TEXTBOX,$Sheet[$i][5]);
	_setSelected($GIFTREGISTRY_PRE_EVENT_STATE_TEXTBOX,$Sheet[$i][6]);
	_setValue($GIFTREGISTRY_PRE_EVENT_CITY_TEXTBOX,$Sheet[$i][7]);
	_setValue($GIFTREGISTRY_PRE_EVENT_ZIPCODE_TEXTBOX,$Sheet[$i][8]);
	_setValue($GIFTREGISTRY_PRE_EVENT_PHONE_TEXTBOX,$Sheet[$i][9]);
	_setValue($GIFTREGISTRY_PRE_EVENT_ADDRESS_TEXTBOX,$Sheet[$i][10]);

	_setValue($GIFTREGISTRY_POST_EVENT_FIRSTNAME_TEXTBOX,$Sheet[$i][1]);
	_setValue($GIFTREGISTRY_POST_EVENT_LASTNAME_TEXTBOX,$Sheet[$i][2]);
	_setValue($GIFTREGISTRY_POST_EVENT_ADDRESS1_TEXTBOX,$Sheet[$i][3]);
	_setValue($GIFTREGISTRY_POST_EVENT_ADDRESS2_TEXTBOX,$Sheet[$i][4]);
	_setSelected($GIFTREGISTRY_POST_EVENT_COUNTRY_TEXTBOX,$Sheet[$i][5]);
	_setSelected($GIFTREGISTRY_POST_EVENT_STATE_TEXTBOX,$Sheet[$i][6]);
	_setValue($GIFTREGISTRY_POST_EVENT_CITY_TEXTBOX,$Sheet[$i][7]);
	_setValue($GIFTREGISTRY_POST_EVENT_ZIPCODE_TEXTBOX,$Sheet[$i][8]);
	_setValue($GIFTREGISTRY_POST_EVENT_PHONE_TEXTBOX,$Sheet[$i][9]);
	_setValue($GIFTREGISTRY_POST_EVENT_ADDRESS_TEXTBOX,$Sheet[$i][10]);

	_click($GIFTREGISTRY_PREPOST_CONTINUE_BUTTON);
}

function addItems($Item)
{
	_click($GIFTREGISTRY_ADDITEMS_LINK);
	//Verifying navigation
	_assertVisible($HOMEPAGE_SLIDES);
	_assertVisible($HOMEPAGE_SLOTS);
	//Adding the items to gift registry
	_click(_link($Item));
	//click on name link
	$ProductName=_getText($SHOPNAV_PLP_NAMELINK);
	_click($SHOPNAV_PLP_NAMELINK);
	//select swatches
	selectSwatch();
	//fetching product name
	$ProductPrice=_getText($PDP_PRODUCT_PRICE);	
	$QTY=_getText($PDP_QUANTITY_DROPDOWN);	
	$ProductNumber=_extract(_getText($PDP_PRODUCT_NUMBER), "/Item# (.*)/", true).toString();
	_click($PDP_ADDTOGIFTREGISTRY_BUTTON);	
}

//******************************Payment Information**************************
//credit card info
function CreateCreditCard($i,$Createcard, $boolean)
{

	_setValue($PAYMENT_NAMETEXTBOX,$Createcard[$i][1]);
	_setSelected($PAYMENT_CARDTYPE_DROPDOWN,$Createcard[$i][2]);
	_setValue($PAYMENT_NUMBER_TEXTBOX,$Createcard[$i][3]);
	_setSelected($PAYMENT_MONTH_DROPDOWN,$Createcard[$i][4]);
	_setSelected($PAYMENT_YEAR_DROPDOWN,$Createcard[$i][5]);
	if($boolean==true)
	{
		_setValue($PAYMENT_FIRSTNAME_TEXTBOX,$Createcard[$i][6]);
		_setValue($PAYMENT_LASTNAME_TEXTBOX,$Createcard[$i][7]);
		_setValue($PAYMENT_ADDRESS1_TEXTBOX,$Createcard[$i][8]);
		_setValue($PAYMENT_ADDRESS2_TEXTBOX,$Createcard[$i][9]);
		_setSelected($PAYMENT_COUNTRY_TEXTBOX,$Createcard[$i][10]);
		_setValue($PAYMENT_CITY_TEXTBOX,$Createcard[$i][11]);
		_setSelected($PAYMENT_STATE_TEXTBOX,$Createcard[$i][12]);
		_setValue($PAYMENT_ZIPCODE_TEXTBOX,$Createcard[$i][13]);
		_setValue($PAYMENT_PHONE_TEXTBOX,$Createcard[$i][14]);
		_setValue($PAYMENT_EMAIL_FIELD, $uId);
	}
}


function closedOverlayVerification()
{
	_assertNotVisible($PAYMENT_OVERLAY);
	//heading
	_assertVisible($PAYMENT_CREDITCARD_INFORMATION_HEADING);
	_assertVisible($PAYMENT_ADDCREDITCARD);           
}

function DeleteCreditCard()
{ 
	//verifying the address in account
	_click(_link("My Account"));
	_click(_link("Payment Settings"));
	_wait(2000)
	var $Count = _count("_div","Delete Card",_in(_div("creditcard")));
	//click on delete card link
	for(var $i=0;$i<$Count;$i++)
	{

		_byPassWaitMechanism(true);
		_expectConfirm("/Do you want to remove this credit card/",true);
		_click(_submit("Delete Card")); 
		//click on ok in confirmation overlay

		_focusWindow();
		// CTRL+R will reload this page using robot events
		_wait(3000); 
		var $robot = new java.awt.Robot();
		$robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
		_wait(500);
		$robot.keyPress(java.awt.event.KeyEvent.VK_R);
		_wait(500);
		$robot.keyRelease(java.awt.event.KeyEvent.VK_R);
		_wait(500);
		$robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
		_wait(500);


	}
	//verify whether deleted or not
	_assertNotVisible(_list("payment-list"));
	_assertNotVisible(_submit("Delete Card"));
}


function DeleteCreditCard()
{               
	//verifying the address in account
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_MYACCOUNT_LINK);
	_click($PAYMENT_LINK);
	//click on delete card link
	while(_isVisible($PAYMENT_DELETECARD_LINK))
	{
		_click($PAYMENT_DELETECARD_LINK);
	}

	//click on ok in confirmation overlay
	_expectConfirm("/Do you want/",true);
	//verify whether deleted or not
	_assertNotVisible($PAYMENT_PAYMENTLIST);
	_assertNotVisible($PAYMENT_DELETECARD_LINK);
}
/*
function addressVerificationOverlay()
 { 
	 _log("$AddrVerification"+$AddrVerification);
	//UPS verification
		if($AddrVerification==$BMConfig[0][2])
		{
			if(_isVisible(_submit("ship-to-original-address")))
			{
			_click(_submit("ship-to-original-address"));
			}
		}
		//group1 verification
		else if($AddrVerification==$BMConfig[1][2])
		{
			if(_isVisible(_submit("ship-to-original-address")))
				{
				_click(_submit("ship-to-original-address"));
				}
			if(_isVisible(_submit("ship-to-original-address[1]")))
				{
					_click(_submit("ship-to-original-address[1]"));
				}
		}
 }
 */

//############# Left Nav links and headings count ###############

function HeadingCountInLeftNav()
{
	var $countOfHeadings=_count("_span", "/(.*)/",_in(_div("secondary-navigation")));
	return $countOfHeadings;		
}

function LinkCountInLeftNav()
{
	var $countOfLinks=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
	return $countOfLinks;		
}