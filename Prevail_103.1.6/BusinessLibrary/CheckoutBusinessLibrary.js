_includeOnce("../ObjectRepository/Generic_OR.sah");
//business library Includes
_includeOnce("CartBusinessLibrary.js");
_includeOnce("ShopNavBusinessLibrary.js");

//Navigate to shipping page
function navigateToShippingPage($data,$quantity)
{
	//navigate to cart page
	navigateToCart($data,$quantity);	
	//navigating to shipping page
	_click(_submit("dwfrm_cart_checkoutCart"));
	_wait(4000);
	if(isMobile())
		{
			_wait(3000);
		}
	if(_isVisible(_div("login-box login-account")))
		{
		_click(_submit("dwfrm_login_unregistered"));
		}
	_wait(4000);
	$subTotal=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
	$tax=parseFloat(_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString());
	_log($tax);
	$shippingTax=parseFloat(_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString());
	_log($shippingTax);
}
function CalculateEstiTotal($shipDiscount,$orderDiscount)
 {
 	var $ExpOrderSubTotal=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
 	var $ExpShippingCost;
 	var $ExpTax;
 	var $ExpEstiTotal;
 	var $ActEstiTotal=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
 	
 		if(_extract(_getText(_row("order-shipping")),"/[ ](.*)/",true)=="N/A" || _extract(_getText(_row("order-shipping")),"/[ ](.*)/",true)=="-")
 		{
 		$ExpShippingCost=0;
 		}
 	else{
 		$ExpShippingCost=parseFloat(_extract(_getText(_row("order-shipping")),"/[$](.*)/",true));
 		}

 		var $ActTaxRes;
 		if(isIE11() || _isIE10())
 			{
 			$ActTaxRes=_extract(_getText(_row("order-sales-tax")),"/Tax (.*)/",true);
 			}
 		else
 			{
 			$ActTaxRes=_extract(_getText(_row("order-sales-tax")),"/Tax (.*)/",true);
 			}
 	
 	if($ActTaxRes=="N/A" || $ActTaxRes=="-")
 		{
 		$ExpTax=0;
 		}
 	else{
 		$ExpTax=parseFloat(_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true));
 	}
 	if(_isVisible(_row("order-shipping-discount discount")) || _isVisible(_row("order-discount discount")))
 	{
 		if(_isVisible(_row("order-discount discount")))
 			{
 			//calculating Order discount
 			var $OrderDisc=($orderDiscount/100)*$ExpOrderSubTotal;
 			$OrderDisc=round($OrderDisc,2);
 			//$ & - should prefixed with discount
 			_assertEqual("/- [$](.*)/",_extract(_getText(_row("order-discount discount")),"/Order Discount(.*)/",true).toString());
 			_assertEqual($OrderDisc,parseFloat(_extract(_getText(_row("order-discount discount")),"/[$](.*)/",true)));
 			$ExpEstiTotal=$ExpOrderSubTotal-$OrderDisc+$ExpShippingCost+$ExpTax;
 			$ExpEstiTotal=round($ExpEstiTotal, 2);
 			_assertEqual($ExpEstiTotal,$ActEstiTotal);
 		
 			}
 		else 
 			{
 			//calculating shipping discount
 			var $ShippingDisc=($shipDiscount/100)*$ExpShippingCost;
 			$ShippingDisc=round($ShippingDisc, 2);
 			//$ & - should prefixed with discount
 			_assertEqual("/- [$](.*)/",_extract(_getText(_row("order-shipping-discount discount")),"/Shipping Discount(.*)/",true).toString());
 			_assertEqual($ShippingDisc,parseFloat(_extract(_getText(_row("order-shipping-discount discount")),"/[$](.*)/",true)));
 			$ExpEstiTotal=$ExpOrderSubTotal+$ExpShippingCost-$ShippingDisc+$ExpTax;
 			$ExpEstiTotal=round($ExpEstiTotal, 2);
 			_assertEqual($ExpEstiTotal,$ActEstiTotal);
 			}
 	}
 	else{
 		$ExpEstiTotal=$ExpOrderSubTotal+$ExpShippingCost+$ExpTax;
 		$ExpEstiTotal=round($ExpEstiTotal, 2);
 		_assertEqual($ExpEstiTotal,$ActEstiTotal);
 	}
 	
 }


function navigateToBillingPage($data,$quantity)
{
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[1][0]);
	//shipping details
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//verifying address verification overlay
	addressVerificationOverlay();	
	$orderTotal=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());
	_log($orderTotal);
}

function shippingAddress($sheet,$i)
{
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), $sheet[$i][1]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), $sheet[$i][2]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"), $sheet[$i][3]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"),$sheet[$i][4]);
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$sheet[$i][5]);
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), $sheet[$i][6]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), $sheet[$i][7]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $sheet[$i][8]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),$sheet[$i][9]);
}
function BillingAddress($sheet,$i)
{
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"), $sheet[$i][1]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"), $sheet[$i][2]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1"), $sheet[$i][3]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address2"),$sheet[$i][4]);
	_setSelected(_select("dwfrm_billing_billingAddress_addressFields_country"),$sheet[$i][5]);
	_setSelected(_select("dwfrm_billing_billingAddress_addressFields_states_state"), $sheet[$i][6]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_city"), $sheet[$i][7]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_postal"), $sheet[$i][8]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_phone"),$sheet[$i][9]);
	
	_click(_link("user-account"));
	_wait(4000);
	
	if(!_isVisible(_link("Logout")))
		{
		_setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),$sheet[$i][10]);
		}
	
}

function verifyOrderSummary($shipmentType,$taxService,$numOfItems)
{
	var $actCount=0;
	var $actPrice=0; 
	//product name
	_assertEqual($CProductName, _getText(_link("/(.*)/",_in(_div("checkout-mini-cart")))));
	var $numOfShipments=_count("_div","/mini-cart-product/",_in(_div("checkout-mini-cart")));
	for(var $i=0;$i<$numOfShipments;$i++)
		{
		var $count=parseInt(_getText(_span("value",_in(_div("mini-cart-pricing["+$i+"]",_in(_div("checkout-mini-cart")))))));
		var $price=_extract(_getText(_span("mini-cart-price",_in(_div("checkout-mini-cart")))),"/[$](.*)/",true).toString();
		$actCount=parseFloat($actCount)+$count;
		$actPrice=parseFloat($actPrice)+parseFloat($price);		
		}
	//quantity
	_assertEqual($CQuantity,$actCount);
	//price
	_assertEqual($subTotal,$actPrice);
	//sub total
	_assertVisible(_row("order-subtotal"));
	_assertEqual($subTotal,_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString());
	//Shipping tax
	if($shipmentType=="Multishipment")
		{
		//var $multiShipmentTax=parseFloat(_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString())*numOfItems;
		for(var $i=0;$i<$numOfItems;$i++)
			{
			_assertEqual($shippingTax,parseFloat(_extract(_collectAttributes("_row","/order-shipping/","sahiText")[$i].toString(),"/[$](.*)/",true)));
			}
		//_assertEqual($shippingTax,$multiShipmentTax);
		}
	else
		{
	_assertVisible(_row("/order-shipping/"));
	_assertEqual($shippingTax,_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString());
		}
	//sales tax
	_assertVisible(_row("order-sales-tax"));
	
	if($taxService=="DEMANDWARE")
		{
			_assertEqual($tax,_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString());
			//order total
			//$actOrderTotal=$subTotal+$shippingTax+$salesTax;
			$actOrderTotal=parseFloat($COrderTotal)+parseFloat($shippingTax)+parseFloat($tax);
			_log($actOrderTotal);
			$actOrderTotal=round($actOrderTotal,2);
			_assertEqual($actOrderTotal,_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());	
		}
	else
		{
		var $boolean=_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString();
		if($boolean=="false")
			{
			_assert(false,"sales tax is not added");
			}
		else
			{
			_assert(true,"sales tax got added");
			var $salesTax=parseFloat(_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString());
			//order total
			_assertVisible(_row("order-total"));
			
				if(_isVisible(_row("/Shipping Discount/")))
				{
					var $shippingDiscount=parseFloat(_extract(_getText(_row("/Shipping Discount/")),"/[$](.*)/",true));
					$actOrderTotal=$subTotal+$shippingTax*$numOfItems+$salesTax-$shippingDiscount;
					$actOrderTotal=round($actOrderTotal,2);
					_assertEqual($actOrderTotal,_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());
				}
				else
				{
					_log($subTotal);
					_log($shippingTax);
					_log($salesTax);
					$actOrderTotal=$subTotal+$shippingTax*$numOfItems+$salesTax;
					$actOrderTotal=round($actOrderTotal,2);
					_assertEqual($actOrderTotal,_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());
				}
			}
		}
}


function billingPageUI($excel)
{
	//verify the UI of Billing page 
	//fields 
	_assertVisible(_span("/First Name/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_firstname")); 
	_assertVisible(_span("/Last Name/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_lastname"));
	_assertVisible(_span("/Address 1/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_address1"));
	_assertVisible(_span("/Address 2/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_address2"));
	_assertVisible(_span("/State/"));
	_assertVisible(_select("dwfrm_billing_billingAddress_addressFields_states_state"));
	_assertVisible(_span("/City/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_city"));
	_assertVisible(_span("/Zip Code/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_postal"));
	_assertVisible(_span("/Phone/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_phone"));
	_assertVisible(_span("/Email/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_email_emailAddress"));
	//subscribe to pandora check box
	_assertVisible(_checkbox("dwfrm_billing_billingAddress_addToEmailList"));
	
	//checkbox
	_assertVisible(_checkbox("dwfrm_billing_billingAddress_addToEmailList"));
	_assertNotTrue(_checkbox("dwfrm_billing_billingAddress_addToEmailList").checked);
	//Privacy policy
	_assertVisible(_link("See Privacy Policy"));
	//ENTER GIFT CERTIFICATE OR COUPON/DISCOUNT CODES section
	_assertVisible(_span($excel[1][0]));
	_assertVisible(_textbox("dwfrm_billing_couponCode"));
	_assertVisible(_submit("APPLY"));
	
	if($CreditCard=="Yes" && $paypalNormal=="Yes")
		{
		//Payment section
		_assertVisible(_label("Credit Card"));
		_assertVisible(_radio("Credit Card"));
		_assert(_radio("Credit Card").checked);
		_assertVisible(_label("Pay Pal"));
		_assertVisible(_radio("PayPal"));
		_assertNotTrue(_radio("PayPal").checked);
		//Payment fields
		_assertVisible(_fieldset("/"+$excel[0][1]+"/"));
		_assertVisible(_span("Name on Card"));
		_assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"));
		_assertVisible(_span("Type"));
		_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_type"));
		_assertVisible(_span("Number"));
		_assertVisible(_div($excel[1][1], _in(_div("payment-method payment-method-expanded"))));
		//_assertEqual($excel[1][1], _getText(_span("form-caption", _in(_div("PaymentMethod_CREDIT_CARD")))));
		_assertVisible(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"));
		_assertVisible(_span("/Expiration Date/"));
		_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_expiration_month"));
		_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_expiration_year"));
		_assertVisible(_span("Security Code"));
		_assertVisible(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"));
		}
	else if($CreditCard=="Yes" && $paypalNormal=="No")
		{
		//Payment section
		_assertVisible(_label("Credit Card:"));
		_assertVisible(_radio("dwfrm_billing_paymentMethods_selectedPaymentMethodID"));
		_assert(_radio("dwfrm_billing_paymentMethods_selectedPaymentMethodID").checked);
		//Payment fields
		_assertVisible(_fieldset("/"+$excel[0][1]+"/"));
		_assertVisible(_span("Name on Card"));
		_assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"));
		_assertVisible(_span("Type"));
		_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_type"));
		_assertVisible(_span("Number"));
		_assertVisible(_span($excel[1][1], _in(_div("PaymentMethod_CREDIT_CARD"))));
		//_assertEqual($excel[1][1], _getText(_span("form-caption", _in(_div("PaymentMethod_CREDIT_CARD")))));
		_assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_number"));
		_assertVisible(_span("/Expiration Date/"));
		_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_month"));
		_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_year"));
		_assertVisible(_span("Security Code"));
		_assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_cvn"));
		}
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	{
		_assertVisible(_label("Pay Pal:"));
		_assertVisible(_radio("PayPal"));
		_assert(_radio("PayPal").checked);
	}	
	_assertVisible(_submit("dwfrm_billing_save"));
	
	//Order summary section
	_assertVisible(_heading3("/ORDER SUMMARY/"));
	_assertVisible(_link("Edit"));
	
	//Click on expand link 
	if (_isVisible(_div("mini-cart-product collapsed")))
		{
		_click(_span("mini-cart-toggle fa fa-caret-right"));
		}
	_assertVisible(_div("mini-cart-image", _in(_div("checkout-mini-cart"))));
	_assertVisible(_div("mini-cart-name", _in(_div("checkout-mini-cart"))));
	_assertVisible(_row("order-subtotal"));
	_assertVisible(_row("/order-shipping/"));
	_assertVisible(_row("order-sales-tax"));
	_assertVisible(_row("order-total"));
}

function paypal($paypalUN,$paypaalPwd)
{
	if(_isVisible(_div("passwordSection")))
		{
			_setValue(_emailbox("login_email"), $paypalUN);
			_setValue(_password("login_password"), $paypaalPwd);
			_click(_submit("btnLogin"));
		}
	else
		{
			_assert(true,"Already logged in")
		}

}

function PaymentDetails($sheet,$i)
{
	  _setValue(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"), $sheet[$i][1]);
		 _setSelected(_select("dwfrm_billing_paymentMethods_creditCard_type"), $sheet[$i][2]);
		 _setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"), $sheet[$i][3]);
		 _setSelected(_select("dwfrm_billing_paymentMethods_creditCard_expiration_month"), $sheet[$i][4]);
	    _setSelected(_select("dwfrm_billing_paymentMethods_creditCard_expiration_year"), $sheet[$i][5]);
	    _setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"), $sheet[$i][6]);
}





function addAddressMultiShip($sheet,$i)
{
	  if(_isVisible(_textbox("dwfrm_profile_address_addressid")))
	  {
	    _setValue(_textbox("dwfrm_profile_address_addressid"), $sheet[$i][1]);
	  }
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_firstName")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_firstName"), $sheet[$i][2]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_lastName")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_lastName"),$sheet[$i][3]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_address1")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_address1"), $sheet[$i][4]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_address2")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_address2"), $sheet[$i][5]);
	_assert(_isVisible(_select("dwfrm_multishipping_editAddress_addressFields_country")));
	_setSelected(_select("dwfrm_multishipping_editAddress_addressFields_country"), $sheet[$i][6]);
	_assert(_isVisible(_select("dwfrm_multishipping_editAddress_addressFields_states_state")));
	_setSelected(_select("dwfrm_multishipping_editAddress_addressFields_states_state"), $sheet[$i][7]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_city")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_city"), $sheet[$i][8]);
	//_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_zip")));
	//_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_zip"), $sheet[$i][9]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_postal")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_postal"), $sheet[$i][9]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_phone")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_phone"), $sheet[$i][10]);
	if(_isVisible(_checkbox("Add to Address Book")))
	{
	_assertNotTrue(_checkbox("Add to Address Book").checked);
	_click(_checkbox("dwfrm_multishipping_editAddress_addToAddressBook"));
	}
	_click(_submit("Save"));
}

function ogonePayment($sheet,$row)
{
	_setValue(_textbox("Ecom_Payment_Card_Number"), $sheet[$row][1]);
	_setSelected(_select("Ecom_Payment_Card_ExpDate_Month"), $sheet[$row][2]);
	_setSelected(_select("Ecom_Payment_Card_ExpDate_Year"), $sheet[$row][3]);
	_setValue(_textbox("Ecom_Payment_Card_Verification"), $sheet[$row][4]);
}

function verifyNavigationToBillingPageFromOgone($sheet)
{
	_assertVisible(_fieldset("/"+$sheet[0][3]+"/"));
	_assertVisible(_submit("dwfrm_billing_save"));
	_assert(_radio("OGONE").checked);
	_assertEqual($sheet[0][1],_getSelectedText(_select("dwfrm_billing_paymentMethods_ogoneCard_type")));	
}

function verifyOCPThroughOgone($sheet,$expAmt)
{
//	_assertVisible(_heading3($sheet[0][7]));
//	if(_isVisible(_image("CloseIcon.png")))
//	{
//		_click(_image("CloseIcon.png"));
//	}
	_assertVisible(_heading1($sheet[0][2]));
	//payment type
	_assertVisible(_div("payment-type"));
	_assertEqual($sheet[0][4], _getText(_div("payment-type")));
	//verify the amount
	_assertEqual($expAmt,_extract(_getText(_span("value",_in(_div("payment-amount")))),"/[$](.*)/",true));
	_assertVisible(_div("order-confirmation-details"));
	_assertVisible(_span("value", _in(_heading1("order-number"))));
	_assertVisible(_link("Return to Shopping"));
}

function orderPlacement($dataSheet,$paypalSheet,$creditCardSheet,$tax)
{
	//navigate to billing page
	navigateToBillingPage($dataSheet[0][0],$dataSheet[1][0]);
	if($tax=="true")
		{
		//verify the tax should not ve NA or 0.00
		var $salesTax=_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true);
			if($salesTax!="N/A" && $salesTax!="0.00" && $salesTax!="false")
				{
				_assert(true);
				}
			else
				{
				_assert(false);
				}
		}
	//enter billing address
	//enter valid billing address
	BillingAddress($Valid_Data,0);
	//enter credit card details
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
		{
			  if($CreditCardPayment==$BMConfig[0][4])
			  {
			  	PaymentDetails($paypalSheet,2);
			  }
		  	  else
			  {
			  	PaymentDetails($creditCardSheet,3);
			  }
			//click on continue
			_click(_submit("dwfrm_billing_save"));	
		}
   else if($CreditCard=="No" && $paypalNormal=="Yes")
		{
	   		_assert(_radio("PayPal").checked);
			//click on continue
			_click(_submit("dwfrm_billing_save"));	
			//paypal
   			if(isMobile())
             {
             _setValue(_emailbox("login_email"), $paypalSheet[0][0]);
             }
   			else
             {
             _setValue(_textbox("login_email"), $paypalSheet[0][0]);
             }
          _setValue(_password("login_password"), $paypalSheet[0][1]);
          _click(_submit("Log In"));
          _click(_submit("Continue"));
          _wait(10000,_isVisible(_submit("submit")));
		}
	//click on submit order
	_click(_submit("submit"));
	//verify order placement
	_assertVisible(_heading1($dataSheet[0][2]));
	_assertVisible(_span("value", _near(_span("Order Placed"))));
	_assertVisible(_span("/value/",_near(_span("Order Number"))));  
	_assertVisible(_cell("order-billing"));
	_assertVisible(_cell("order-payment-instruments"));
	_assertVisible(_table("order-totals-table"));
}


function makeServiceDown($serviceName)
{
	//verify log on service down
	BM_Login();
	_click(_link("Administration"));
	_click(_link("Operations"));
	_click(_link("Services"));
	_click(_link($serviceName));
	_uncheck(_checkbox("Enabled"));
	_click(_submit("Apply"));
	_click(_link("Log off."));	
}

function makeServiceUpAndRunning($serviceName)
{
	//verify log on service down
	BM_Login();
	_click(_link("Administration"));
	_click(_link("Operations"));
	_click(_link("Services"));
	_click(_link($serviceName));
	_check(_checkbox("Enabled"));
	_click(_submit("Apply"));
	_click(_link("Log off."));	
}

function enableEndToEnd()
{
	//verify log on service down
	BM_Login();
	_click(_link("Site Preferences"));
	_click(_link("Custom Preferences"));
	//Click on PREVAIL Integrations link
	_click(_link("PREVAIL Integrations"));
	_check(_checkbox("/(.*)/",_rightOf(_row("/Is End To End Merchant/"))));
	_click(_submit("update"));
	_click(_link("Log off."));	
}

function disableEndToEnd()
{
	//verify log on service down
	BM_Login();
	_click(_link("Site Preferences"));
	_click(_link("Custom Preferences"));
	//Click on PREVAIL Integrations link
	_click(_link("PREVAIL Integrations"));
	_uncheck(_checkbox("/(.*)/",_rightOf(_row("/Is End To End Merchant/"))));
	_click(_submit("update"));
	_click(_link("Log off."));	
}

function verifyNavigationToShippingPage()
{
	_assertVisible($SHIPPING_PAGE_HEADING);
	_assertVisible($SHIPPING_PAGE_HEADER);
	_assertVisible($SHIPPING_SUBMIT_BUTTON);	
}

function addressVerificationOverlay()
{ 
	 _log("$AddrVerification"+$AddrVerification);
	//UPS verification
		if($AddrVerification==$BMConfig[0][2])
		{
			if(_isVisible(_submit("/ship-to-original-address/")))
			{
			_click(_submit("/ship-to-original-address/"));
			}
		}
		//group1 verification
		else if($AddrVerification==$BMConfig[1][2])
		{
			if(_isVisible(_submit("/ship-to-original-address/")))
				{
				_click(_submit("/ship-to-original-address/"));
				}
			if(_isVisible(_submit("ship-to-original-address[1]")))
				{
					_click(_submit("ship-to-original-address[1]"));
				}
		}
}