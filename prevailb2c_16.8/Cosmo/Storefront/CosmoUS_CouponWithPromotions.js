_include("../GenericLibrary/GlobalFunctions.js");
_resource("SanityData.xls");

//delete user
deleteUser();

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();
login_Owner();
ClearCartItems();

//Deleting Address
deleteAddress();

//click on add new address
_click(_link("Add New Address"));
//Add Address
addAddress($profileAddressData,0);


var $t = _testcase("304390","Verify the application behavior when a order is placed with Order Level coupon and order is completed using Credit card as a Registered user.");
$t.start();
try
{
	
	//navigate to cart page
	navigateToCart($generic[1][0],$generic[0][1]);
	//Entering coupon code
	_setValue(_textbox("dwfrm_cart_couponCode"), "123");
	//entering order level coupon
	_assert(false,"Should add couppon code and verify the cost");
	//navigating to shipping page
	_click(_submit("dwfrm_cart_checkoutCart"));
	if(isMobile())
		{
			_wait(3000);
		}
	if(_isVisible(_link("Account")))
		{
		_click(_submit("dwfrm_login_unregistered"));
		}
	$subTotal=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
	$tax=parseFloat(_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString());
	_log($tax);
	$shippingTax=parseFloat(_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString());
	_log($shippingTax);
	//click on use credit card button
	_click(_label("Use credit card", _in(_div("payment-method-options"))));
	//payment
	PaymentDetails($paymnet,1);
	_setValue(_textbox("dwfrm_billing_paymentMethods_creditCard_cvn"),$paymnet[$i][6]);
	//Billing details
	BillingAddress($billingAddress,0);
	//click on continue button
	_click(_submit("dwfrm_billing_save"));
		
if(mobile.iPad() || mobile.iPhone() || _isSafari())
{
	_assertVisible(_div("step-3 active chekoutsteps"));
	_assertEqual("3Review & Submit", _getText(_div("step-3 active chekoutsteps")));
	}
	else
		{
		_assertVisible(_div("step-3 active chekoutsteps"));
		_assertEqual("3 Review & Submit", _getText(_div("step-3 active chekoutsteps")));
		}
	//click on place order button
	_click(_submit("button-fancy-large chechout-button"));
    //thanku
	if(mobile.iPad() || mobile.iPhone())
	{
	_assertVisible(_bold("THANK YOU FOR YOUR ORDER"));
    }
	else
		{
		_assertVisible(_heading1("Thank you for your order"));
		}
	//_assertEqual($ProductName, _getText(_div("name")));
	_assertVisible(_div($paymnet[1][8],_in(_div("order-payment-instruments"))));

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();