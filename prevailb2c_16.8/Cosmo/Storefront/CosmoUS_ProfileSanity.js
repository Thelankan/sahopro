_include("../GenericLibrary/GlobalFunctions.js");
_resource("SanityData.xls");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();
login_Owner();
ClearCartItems();

OwnerUser();
SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();
login_Owner();
Addresspopup();
deleteList();

var $t = _testcase("227399", "[Manage users]:Verify whether the user can create new manager/child account upon providing the valid data in My Account: Settings: Manage Users page.");
$t.start();
try
{
	
	deleteUser($email);
	SiteURLs();
	login_Owner();
	Addresspopup();
	DeleteUser();
	//click on add user
	_click(_link("link-button-small primaryButtn")).click();
	AddManager();
	
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("304395/304396/316895","Verify if the registered user can edit a saved address in the application");
$t.start();
try
{
	//Deleting Address
	deleteAddress();
	//Create address
	//click on add new address
	_click(_link("Add New Address"));
	//Add Address
	addAddress($profileAddressData,0);
	//Verify added address is correct or not 
	//12345 Cosmo Prof CosmoProfLimited Chicago, IL 60706 Phone Number: 888-888-8888
	var $profileAddress=$profileAddressData[0][1]+" "+$profileAddressData[0][2]+" "+$profileAddressData[0][3]+" "+$profileAddressData[0][4]+" "+$profileAddressData[0][6]+", "+$profileAddressData[0][10]+" "+$profileAddressData[0][8]+" Phone Number: "+$profileAddressData[0][9];
	_assertEqual($profileAddress,_getText(_div("mini-address-location[1]")));
	//click on edit
	_click(_link("Edit"));
	//Edit address
	addAddress($profileAddressData,1);
	//Editing Address
	var $profileAddress=$profileAddressData[1][1]+" "+$profileAddressData[1][2]+" "+$profileAddressData[1][3]+" "+$profileAddressData[1][4]+" "+$profileAddressData[1][6]+", "+$profileAddressData[1][10]+" "+$profileAddressData[1][8]+" Phone Number: "+$profileAddressData[1][9];
	_assertEqual($profileAddress,_getText(_div("mini-address-location[1]")));
	//Deleting Address
	deleteAddress();
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("304399/304400","Verify the functionality related to 'Save' button of change name / email section in Edit Account page of the application");
$t.start();
try
{
	
	//Navigate to my account
	_click(_link("My Account", _in(_div("navigation-bar"))));
	//click on manage account
	_click(_link("MANAGE ACCOUNT"));
	//click on edit text
	_click(_link("EDIT SETTINGS"));
	//change the values and try to save 
	_setValue(_textbox("dwfrm_profile_customer_accountemail"),$generic[0][4]);
	_setValue(_textbox("dwfrm_profile_customer_accountemailconfirm"),$generic[0][4]);
	_setValue(_password("dwfrm_profile_login_changelogin"),$generic[1][4]);
	_click(_submit("dwfrm_profile_confirm"));
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("223451", "[My account-UT]: Verify the functionality of Delete User button on user's box in My Account: Settings: Manage Users page.");
$t.start();
try
{
	//click on my account
	_click(_link("My Account", _in(_div("navigation-bar"))));
	//click on manage users in left nav
	_click(_link("Manage Users", _in(_div("secondary-navigation"))));
	//click on delete user
	_click(_link("dialogify cancel secondarybtn", _in(_div("manageUsers"))));
	_assertVisible(_div("delete-box"));
    _assertEqual($Generic[0][0], _getText(_div("deleteConfirmmessage")));
    _click(_link("go-back-users secondarybtn"));
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("223458", "[My account-UT]: Verify the functionality of checkout button present in the 'Quick order' Page.");
$t.start();
try
{
	//click on my account
	_click(_link("My Account", _in(_div("navigation-bar"))));
	//click on quick oredr present in the left navigation
	_click(_link("Quick Order", _in(_div("secondary-navigation"))));
	_assertVisible(_heading1($Generic[4][0]));
    //click on checkout button in quick order page
	_click(_submit("dwfrm_quickorder_submitOrder"));
	//cart
	_assertVisible(_heading1($Generic[3][0]));
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("227398 ", "[Manage users]:Verify the newly created user (Manager) is able to access the Application in My Account: Settings: Manage Users page.");
$t.start();
try
{
	navigateToShippingPage($Generic[0][1],$Generic[0][2]);
	//click on use credit card
	_click(_label("Use credit card", _in(_div("payment-method-options"))));
	PaymentDetails($payment,0);
	BillingAddress($Billing,0);
	_click(_submit("dwfrm_billing_save"));
	//click on place order
	_click(_submit("button-fancy-large chechout-button"));
	//order placed
	if(isMobile())
		{
		_assertVisible(_bold($Generic[1][0]));
		}
	else
		{
		_assertVisible(_heading1($Generic[1][0]));

		}

}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("227400", "[Manage users]:Verify whether newly created manager account can create futher more accounts.");
$t.start();
try
{
	//click on my account
	_click(_link("My Account", _in(_div("navigation-bar"))));
	//click on manage users in left nav
	_assertNotVisible(_link("Manage Users", _in(_div("secondary-navigation"))));
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();	

var $t = _testcase("227450", "[Order History]:Veify functionality of seach button once user selects FROM and TO date and click on search button in desk top view.");
$t.start();
try
{
	//click on my account
	_click(_link("My Account", _in(_div("navigation-bar"))));
	//click on order history in left navigation
	_click(_link("Order History", _in(_div("secondary-navigation"))));
	//order history
	_assertVisible(_heading1($Generic[2][0]));
     //date entry
	_click(_button("ui-datepicker-trigger"));
	eventFromcalendar();
	_click(_button("ui-datepicker-trigger[1]"));
	eventTocalendar();
	//click on  search
	_click(_link("datePickerFind"));
	

 }
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("227501", "[View Order]:Verify the navigation of Reorder buttons in order details page in desktop view.");
$t.start();
try
{
      //click on view order button
	_click(_submit("button-fancy-small view-order"));
	_assertVisible(_heading1("Order History"));
    //click on reorder button
	_click(_link("Reorder", _in(_div("order-information"))));
	//address selection
	Addresspopup();
	//cart
	_assertVisible(_heading1($Generic[3][0]));
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();	

var $t = _testcase("230199", "[Contact US]: Verify the functionality of Submit Button- Authenticated User.");
$t.start();
try
{
	//click on contact us in the footer section
	_click(_link("Contact us via email", _in(_div("footer-content"))));
	_assertVisible(_heading1("Contact Us"));
    //select topic from dropdown
	_setSelected(_select("dwfrm_contactus_topicofinterest"), "2");
	_setValue(_textarea("dwfrm_contactus_comment"),$Generic[0][3]);
	_click(_submit("dwfrm_contactus_send"));
	//thanku popup
	_assertVisible(_paragraph($Generic[1][3]));
	_assertVisible(_paragraph($Generic[2][3], _in(_div("confirmation-message contactusdialog"))));
	_assertVisible(_submit("Close", _in(_div("confirmation-message contactusdialog"))));
    //close the dailog
	_click(_submit("Close", _in(_div("confirmation-message contactusdialog"))));
	
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("227016", "[Change Password]: Verify the navigation when user clicks on Save button in My Account: Settings: Change Password page.");
$t.start();
try
{
	//click on my account
	_click(_link("My Account", _in(_div("navigation-bar"))));
   //click on change password in left navigation
	_click(_link("Change Password", _in(_div("secondary-navigation"))));
	_assertVisible(_heading1("Change password"));
	_click(_submit("dwfrm_changepassword_apply"));
	_setValue(_password("dwfrm_changepassword_currentpassword"),$uId);
	_setValue(_password("dwfrm_changepassword_newpassword"),$Generic[0][4]);
	_setValue(_password("dwfrm_changepassword_newpasswordconfirm"),$Generic[1][4]);
	//click on save
	_click(_submit("dwfrm_changepassword_apply"));
	_assertEqual("Password change success", _getText(_span("successMsg")));
	Logout();
	login_Owner();
	_assertVisible(_div("error-form"));
	 _setValue(_textbox("dwfrm_login_username"),$uId);
	 _setValue(_password("dwfrm_login_password"),$Generic[1][4]);
	 _click(_submit("dwfrm_login_login"));
	 Addresspopup();
	//click on my account
	 _click(_link($gen[2][1], _in(_div("/navigation-bar/"))));
	 _click(_link("Change Password", _in(_div("secondary-navigation"))));
	 //change password
	 //enter current password
	 _setValue(_password("dwfrm_changepassword_currentpassword"),$Generic[0][4]);
	 //enter new password
	 _setValue(_password("dwfrm_changepassword_newpassword"), $pwd);
	 ///confirm password
	 _setValue(_password("dwfrm_changepassword_newpasswordconfirm"), $pwd);
	 //click on save
	 _click(_submit("dwfrm_changepassword_apply"));
	
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


//##################### Scripts related to Wish List ###############################

Logout();
login_Owner();
Addresspopup();

var $t = _testcase("227572", "[List Selected View]: Verify the functionality of 'Add to Cart' button in My Lists: List selected View page.");
$t.start();
try
{
	
//create list
additemsToWishListAccount($generic[0][5],$generic[0][1],$generic[0][2]);
$prodname = _getText(_div("product-name"));
 navigateToMyList();
_click(_link("Manage", _in(_div("MyListView"))));
//List name assertion
_assertEqual($generic[0][5], _getText(_heading1("content-header list-name")));
//click on add to cart
_click(_submit("Add to Cart", _in(_div("mylist-registry"))));
//click on cart
_click(_link("mini-cart-link", _in(_div("/navigation-bar/"))));
_click(_link("View Cart"));
//cart page
_assertVisible(_heading1("Cart"));
//prod name in cart
_assertEqual($prodname, _getText(_div("name", _in(_table("cart-table")))));
	 
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("304409","Verify the edit details functionality for a product in My list page of the application as a registered user");
$t.start();
try
{
	
	//Navigate to my list page
	_click(_link("My Account", _in(_listItem("my-account dd3 last"))));
	//click on wish list 
	_click(_link("My Lists", _in(_span("My Lists"))));
	//click on manage link
	_click(_link("Manage", _in(_div("PFSweb Manage"))));
	//Edit product details link
	_click(_link("Edit Details"));
	//Set the quantity as 2
	_setValue(_textbox("Quantity"),$generic[2][1]);
	//adding to list 
	_click(_link("Add to List"));
	//select created list
	_setSelected(_select("MyLists"),$generic[0][5]);
	//Click on send link
	_click(_submit("Send"));
	//quantity should update accordingly
	_assertEqual($generic[2][1], _getValue(_textbox("Quantity")));
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("304402","Verify the remove functionality for a product in My list page of the application as a registered user");
$t.start();
try
{
	
	//Products in wish list
	var $ProductCount=_count("_div","item-image",_div("mylist-registry"));
	_click(_submit("dwfrm_giftregistry_remove"));
	
	if ($ProductCount==1)
		{
		_assertVisible(_paragraph("You have no items in your list."));
		_assertVisible(_link("add-items"));
		}
		else
			{
			var $ProductCountAfter=_count("_div","item-image",_div("mylist-registry"));
			_assertEqual($ProductCountAfter,$ProductCount-1);
			}
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


//############## Related to blog page ###################
var $t = _testcase("230526","[Beauty Blog]: Verify the Filter by Blog Functionality in Beauty Blog page.");
$t.start();
try
{
	
	//navigate to blog
	 NavigateToBlog();
	 //verifying the filter by brand
	 _assertVisible(_div("refinement Brand", _in(_div("refinmentSection"))));
	 //selecting any post refinement 
	 _setSelected(_select("selectRefinements", _in(_div("refinement Brand"))), "Farouk");
	 //click on apply
	 _click(_link("Apply Filter", _in(_div("refinmentSection"))));
	 //navigating to the blog list page 
	 _assertVisible(_heading2("blog", _in(_div("blog"))));
	 //result text
	 _assertVisible(_span("Results", _in(_div("result-messages"))));
	var $noOfBlog=_count("_div","listArticle", _in(_div("Article-left")));
	 for(var $i=0;$i<$noOfBlog;$i++)
		 {
	  _assertVisible(_div("listArticle["+$i+"]", _in(_div("Article-left"))));
		 }

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var  $t = _testcase("230574","[Beauty Blog]: Verify the search functionality in beauty Blog mian page.")
$t.start();
try
{
	//navigate to blog
	 NavigateToBlog();
	//enter any invalid data in search blog box
	 _setValue(_textbox("q", _in(_div("blog-search"))), "brand");
	 //click on go button
	 _click(_submit("blogSearchBtn"));
	 // navigating to the searched blog page
	 _assertVisible(_heading2("blog"));
	 _assertVisible(_span("Results", _in(_div("result-messages"))));
	 _assertVisible(_list("Article-folder-content-list", _in(_div("search-results-content"))));
	 if(_isVisible(_div("Article-left")))
		 {//image
		 _assertVisible(_image("contentThumbnail", _in(_div("listArticle"))));
		 //article
		 _assertVisible(_div("listArticle-right", _in(_div("listArticle"))));
}
	 else
		 {
		 _assertNotVisible(_div("Article-left"));
		 }
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

/*var $t = _testcase(" "," ");
$t.start();
try
{

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();*/