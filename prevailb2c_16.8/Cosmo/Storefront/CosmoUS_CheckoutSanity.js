_include("../GenericLibrary/GlobalFunctions.js");
_resource("SanityData.xls");

//delete user
//deleteUser();

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();
login_Owner();
ClearCartItems();

//Deleting Address
deleteAddress();

//click on add new address
_click(_link("Add New Address"));
//Add Address
addAddress($profileAddressData,0);
//click on add new address
_click(_link("Add New Address"));
addAddress($profileAddressData,1);

var $t = _testcase("228771/228863/316892", "[Checkout: Step 3]: Verify the functionality of Place Order button on Checkout: Step 3 expanded page/Verify the Functionality Related to Default Shipping Address Functionality in the Address page as an registered user.");
$t.start();
try
{
	
	//navigate to cart page
	navigateToCart($generic[0][0],$generic[0][1]);	
	//navigating to shipping page
	_click(_submit("dwfrm_cart_checkoutCart"));
	//click on change link
	if (_isVisible(_link("Change")))
		{
		_click(_link("Change"));
		}
	_setSelected(_select("dwfrm_singleshipping_addressList"), "/Default/");
	//Default address should pre populate
	_assertEqual($profileAddressData[2][2],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName")));
	_assertEqual($profileAddressData[2][3],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName")));
	_assertEqual($profileAddressData[2][4],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1")));
	//_assertEqual($profileAddressData[2][5],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2")));
	_assertEqual($profileAddressData[2][6],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")));
    _assertEqual($profileAddressData[2][7],_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
    _assertEqual($profileAddressData[2][8],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_zip")));
    _assertEqual($profileAddressData[2][9],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone")));
	//click on continue button in shipping page
    _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
    //wait
    _wait(6000);
    //
    if (_isVisible(_div("address-verify-main")))
    {
    	_click(_submit("dwfrm_customeraddress_edit"));
    }
	//click on use credit card button
	_click(_label("Use credit card", _in(_div("payment-method-options"))));
	//payment
	PaymentDetails($paymnet,0);
	//Billing details
	BillingAddress($billingAddress,0);
	//click on continue button
	_click(_submit("dwfrm_billing_save"));
	
if(mobile.iPad() || mobile.iPhone() || _isSafari())
{
	_assertVisible(_div("step-3 active chekoutsteps"));
	_assertEqual("3Review & Submit", _getText(_div("step-3 active chekoutsteps")));
	}
	else
		{
		_assertVisible(_div("step-3 active chekoutsteps"));
		_assertEqual("3 Review & Submit", _getText(_div("step-3 active chekoutsteps")));
		}
	//click on place order button
	_click(_submit("button-fancy-large chechout-button"));
    //thanku
	if(mobile.iPad() || mobile.iPhone())
	{
	_assertVisible(_bold("THANK YOU FOR YOUR ORDER"));
    }
else
	{
	_assertVisible(_heading1("Thank you for your order"));

	}
	if(isMobile()|| _isSafari() || _isChrome())
	{
		_assertEqual($billingAddress[1][9], _getText(_div("order-billing")));

	}
else
	{
	_assertEqual($billingAddress[0][9], _getText(_div("order-billing")));

	}
	//UI of minicart
	var $pBrands=new Array();
	var $pNames=new Array();
	var $skus=new Array();
	var $pQuantites=new Array();
    var $pPrices=new Array();
    
    $pBrands.push($BrandName);
    $pNames.push($ProductName);
    $pQuantites.push($Qty);
    $pPrices.push($totalPrice);
    var $totalSectionsInOCP=_count("_link","/(.*)/",_in(_div("receipt-order-shipment-table item-list")));
    for(var $i=0;$i<$totalSectionsInOCP;$i++)
		{
		
	   if(_isVisible(_div("receipt-order-shipment-table item-list")))
		{
		   
		//verify product brand
        _assertEqual($pBrands[$i],_getText(_div("product-brand["+$i+"]"))); 
        //verify the qty
        _assertEqual($pQuantites[$i],_extract(_getText(_div("item-quantity column["+$i+"]")),"/[:](.*)/",true).toString());
        //verify price of product selected
        _assertEqual($pPrices[$i],_extract(_getText(_div("item-total column["+$i+"]")),"/[$](.*)/",true).toString());
        
         }
	}
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("232225", "[Checkout]: Placing an order for an already saved Valid address");
$t.start();
try
{
	navigateToShippingPage($generic[0][0],$generic[0][1]);
	//click on use credit card
	_click(_label("Use credit card"));
	//click on same as shipping address check box
	_click(_checkbox("sameasshippingadd"));
	//payment details
	PaymentDetails($paymnet,0);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	//click on continue button
	if(mobile.iPad() || mobile.iPhone() || _isSafari())
	{
	_assertVisible(_div("step-3 active chekoutsteps"));
	_assertEqual("3Review & Submit", _getText(_div("step-3 active chekoutsteps")));
	}
	else
		{
		_assertVisible(_div("step-3 active chekoutsteps"));
		_assertEqual("3 Review & Submit", _getText(_div("step-3 active chekoutsteps")));
		}
	
	//click on place order button
	_click(_submit("button-fancy-large chechout-button"));
    //thanku
	if(mobile.iPad() || mobile.iPhone())
	{
	_assertVisible(_bold("THANK YOU FOR YOUR ORDER"));
    }
else
	{
	_assertVisible(_heading1("Thank you for your order"));

	}

}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("232228", "[Checkout]: Placing an order by providing invalid credit card details on checkout page after changing the address");
$t.start();
try
{
	
	//Navigate to shipping page
	navigateToShippingPage($generic[1][0],$generic[0][1]);
	//click on use credit card
	_click(_label("Use credit card"));
	//click on same as shipping address check box
	_click(_checkbox("sameasshippingadd"));
	//payment details
	PaymentDetails($paymnet,5);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	//card expire
	_assertEqual("Invalid Credit Card Number", _getText(_span("form-caption error-message")));
	_assertEqual("Credit Card is expired", _getText(_span("form-caption error-message[1]")));
	
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

//clear cart items
ClearCartItems();

var $t = _testcase("232229", "[Checkout]: Placing an order by changing credit card details on checkout page after changing the address");
$t.start();
try
{
	navigateToShippingPage($generic[1][0],$generic[0][1]);
	//click on use credit card
	_click(_label("Use credit card"));
	//click on same as shipping address check box
	_click(_checkbox("sameasshippingadd"));
	//payment details
	PaymentDetails($paymnet,0);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	if(mobile.iPad() || mobile.iPhone() || _isSafari())
	{
	_assertVisible(_div("step-3 active chekoutsteps"));
	_assertEqual("3 Review & Submit", _getText(_div("step-3 active chekoutsteps")));
	}
else
	{
	//step2
	_assertVisible(_div("step-3 active chekoutsteps"));
	_assertEqual("3 Review & Submit", _getText(_div("step-3 active chekoutsteps")));
	}
	//click on change
	_click(_link("secondarybtn", _in(_div("selected-shipping-container"))));
	//change shipping address
	shippingAddress($shippingAddress,1);
	//click on use this address button
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	verifyyouraddress();
	
	_assertEqual($shippingAddress[1][9], _getText(_cell("left")));
	//click on use credit card button
	_click(_label("Use credit card", _in(_div("payment-method-options"))));
	//payment details
	PaymentDetails($paymnet,0);
	//Billing details
	BillingAddress($billingAddress,0);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	//billing address
	_assertEqual($billingAddress[0][10],_extract(_getText(_div("mini-billing-address  order-component-block")),"/7(.*)/",true).toString());
	//text area 
	if (_isVisible(_textarea("dwfrm_summary_comment")))
		{
		_setValue(_textarea("dwfrm_summary_comment"),"AAAAAAAAAAAAAAAAAAA");
		}
	//click on place order button
	_click(_submit("button-fancy-large chechout-button"));
    //thanku
	  if(mobile.iPad() || mobile.iPhone())
	{
	_assertVisible(_bold("THANK YOU FOR YOUR ORDER"));
    }
else
	{
	_assertVisible(_heading1("Thank you for your order"));

	}
	if(_isSafari() || _isChrome())
	{
		_assertEqual($billingAddress[1][9], _getText(_div("order-billing")));

	}
else
	{
	_assertEqual($billingAddress[0][9], _getText(_div("order-billing")));

	}
	
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


ClearCartItems();

var $t = _testcase("232237", "[Checkout]: Verify the functionality of Use Credit Card button in Checkout page.");
$t.start();
try
{
	navigateToShippingPage($generic[0][0],$generic[0][1]);
	if(mobile.iPad() || mobile.iPhone() || _isSafari())
	{
	_assertVisible(_div("step-2 active chekoutsteps"));
	_assertEqual("2Payment Method", _getText(_div("step-2 active chekoutsteps")));
	}
	else
		{
	//step2
	_assertVisible(_div("step-2 active chekoutsteps"));
	_assertEqual("2 Payment Method", _getText(_div("step-2 active chekoutsteps")));
		}
	//click on use credit card button
	_click(_label("Use credit card", _in(_div("payment-method-options"))));
	//expanded
	_assertVisible(_div("Select or Enter Payment Details"));
	_assertVisible(_div("Select or Enter Credit Card Billing Address"));
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


//Clear cart Items
ClearCartItems();

var $t = _testcase("241619", "[Checkout: Step 2]: Verify the functionality of Save this card checkbox");
$t.start();
try
{
	
	navigateToShippingPage($generic[0][0],$generic[0][1]);
	if(mobile.iPad() || mobile.iPhone() || _isSafari())
	{
	_assertVisible(_div("step-2 active chekoutsteps"));
	_assertEqual("2Payment Method", _getText(_div("step-2 active chekoutsteps")));
	}
	else
		{
		//step2
		_assertVisible(_div("step-2 active chekoutsteps"));
		_assertEqual("2 Payment Method", _getText(_div("step-2 active chekoutsteps")));
		}
	_click(_label("Use credit card", _in(_div("payment-method-options"))));
	//select payment details from the dropdown
	_setSelected(_select("dwfrm_billing_paymentMethods_creditCardList"),1);
	_setValue(_textbox("dwfrm_billing_paymentMethods_creditCard_cvn"), "123");
	//Billing address
	BillingAddress($billingAddress,0);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	//_click(_label("Use credit card", _in(_div("payment-method-options"))));
    _assertEqual($billingAddress[0][10], _extract(_getText(_div("mini-billing-address  order-component-block")), "/17(.*)/", true).toString());
     //click on change
	_click(_link("secondarybtn", _in(_div("editpayment"))));
	//enter payment details
	PaymentDetails($paymnet,0);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	//click on change
	_click(_link("secondarybtn", _in(_div("editpayment"))));
	//click on my account
	_click(_link("My Account", _in(_listItem("/my-account dd3 last/"))));
	//click on paymnet in left navigation
	_click(_link("Payment Settings", _in(_div("secondary-navigation"))));
	//Verifying in profile
	_assertVisible(_div($paymnet[0][1], _in(_div("creditcard-info"))));
	_assertVisible(_div($paymnet[0][2], _in(_div("creditcard-info"))));

}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("241615","Verify Selecting different address' on Checkout: Step 1 page as an owner/manager");
$t.start();
try
{
	

	//Navigate to shipping page
	navigateToShippingPage($generic[0][0],$generic[0][1]);
	if(mobile.iPad() || mobile.iPhone() || _isSafari())
	{
	_assertVisible(_div("step-2 active chekoutsteps"));
	_assertEqual("2Payment Method", _getText(_div("step-2 active chekoutsteps")));
	}
else
	{
	//step2
	_assertVisible(_div("step-2 active chekoutsteps"));
	_assertEqual("2 Payment Method", _getText(_div("step-2 active chekoutsteps")));
	}
	//shipping address
	//click on change
	_click(_link("secondarybtn", _in(_div("selected-shipping-container"))));
	_wait(2000);
	//select from change address dropdown
	_setSelected(_select("dwfrm_singleshipping_addressList"),"1");
	//click on change
	if (_isVisible(_submit("dwfrm_customeraddress_edit")))
		{
		_click(_submit("dwfrm_customeraddress_edit"));
		}
	//click on Use This Address button
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	_wait(5000);
	//changed shipping address
	//click on change
	if (_isVisible(_submit("dwfrm_customeraddress_edit")))
		{
		_click(_submit("dwfrm_customeraddress_edit"));
		}
	_click(_link("secondarybtn", _in(_div("selected-shipping-container"))));
	_wait(2000);
	_setSelected(_select("dwfrm_singleshipping_addressList"), "2");
	_wait(5000);
	//click on change
	if (_isVisible(_submit("dwfrm_customeraddress_edit")))
		{
		_click(_submit("dwfrm_customeraddress_edit"));
		}
	//click on Use This Address button
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//click on change
	if (_isVisible(_submit("dwfrm_customeraddress_edit")))
		{
		_click(_submit("dwfrm_customeraddress_edit"));
		}

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("241620", "[Checkout: Step 2]: Verify the functionality of Same as Shipping address checkbox");
$t.start();
try
{
	navigateToShippingPage($generic[0][0],$generic[0][1]);
	if(mobile.iPad() || mobile.iPhone() || _isSafari())
	{
	_assertVisible(_div("step-2 active chekoutsteps"));
	_assertEqual("2Payment Method", _getText(_div("step-2 active chekoutsteps")));
	}
	else
		{
		//step2
		_assertVisible(_div("step-2 active chekoutsteps"));
		_assertEqual("2 Payment Method", _getText(_div("step-2 active chekoutsteps")));
		}
	
	_click(_label("Use credit card", _in(_div("payment-method-options"))));
	//payment
	PaymentDetails($paymnet,2);
	//same as shipping address checkbox in billing page
	_check(_checkbox("sameasshippingadd"));
	_setSelected(_select("dwfrm_billing_billingAddress_addressFields_states_state"), "New York");
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_phone"), "777-777-7777");
	_click(_submit("dwfrm_billing_save"));
	if(mobile.iPad() || mobile.iPhone() || _isSafari())
	{
	_assertVisible(_div("step-3 active chekoutsteps"));
	_assertEqual("3Review & Submit", _getText(_div("step-3 active chekoutsteps")));
	}
	else
		{
		_assertVisible(_div("step-3 active chekoutsteps"));
		_assertEqual("3 Review & Submit", _getText(_div("step-3 active chekoutsteps")));
		}
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("232239", "[Checkout]: Verify the totals in checkout page.");
$t.start();
try
{
	navigateToCart($generic[0][0],$generic[0][1]);
	//order summary details which should match in cart
	if(_isVisible(_row("order-subtotal")))
	{
	$subtotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
	}
	$shippingDiscounts=_extract(_getText(_row("order-shipping  first ")),"/[$](.*)/",true).toString();
	$tax=_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString();
	$total=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
	//click on checkout
	_click(_submit("dwfrm_cart_checkoutCart"));
	//checkout order summary
	if(_isVisible(_row("order-subtotal")))
	{
	$subtotalcheckout=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
	}
	$shippingDiscountscheckout=_extract(_getText(_row("order-shipping  first ")),"/[$](.*)/",true).toString();
	$taxcheckout=_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString();
	$totalcheckout=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
	_assertEqual($subtotal,$subtotalcheckout);
	_assertEqual($shippingDiscounts,$shippingDiscountscheckout);
	_assertEqual($tax,$taxcheckout);
	_assertEqual($total,$totalcheckout);

}

catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("304397","Verify if a registered user can delete a saved credit card in the application");
$t.start();
try
{
	//Delete card
	DeleteCreditCard();
	//create credit card
	_click(_link("Add Payment Method"));
	CreateCreditCard($paymnet,4);
	_click(_submit("dwfrm_paymentinstruments_creditcards_save"));

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("304408","Verify if the registered user is able to Place Order with 16 digit credit card.");
$t.start();
try
{
	
		navigateToShippingPage($generic[1][0],$generic[0][1]);
		//click on use credit card button
		_click(_label("Use credit card", _in(_div("payment-method-options"))));
		//select payment from saved cards
		_setSelected(_select("dwfrm_billing_paymentMethods_creditCardList"), "1");
		_setValue(_textbox("dwfrm_billing_paymentMethods_creditCard_cvn"),$paymnet[4][6]);
		//Billing details
		BillingAddress($billingAddress,0);
		//click on continue button
		_click(_submit("dwfrm_billing_save"));
		
	if(mobile.iPad() || mobile.iPhone() || _isSafari())
	{
		_assertVisible(_div("step-3 active chekoutsteps"));
		_assertEqual("3Review & Submit", _getText(_div("step-3 active chekoutsteps")));
		}
		else
			{
			_assertVisible(_div("step-3 active chekoutsteps"));
			_assertEqual("3 Review & Submit", _getText(_div("step-3 active chekoutsteps")));
			}
		//click on place order button
		_click(_submit("button-fancy-large chechout-button"));
	    //thanku
		if(mobile.iPad() || mobile.iPhone())
		{
		_assertVisible(_bold("THANK YOU FOR YOUR ORDER"));
	    }
		else
			{
			_assertVisible(_heading1("Thank you for your order"));
			}

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("228774", "[Checkout: Order Confirmation] - Verify the functionality of Continue Shopping button on Checkout: Step 3 expanded page");
$t.start();
try
{
	//click on continue shopping button
	//_call(_link("continue button chechout-button").click());
	_click(_link("/continue button chechout-button/", _in(_div("order-confirmation-details"))));

	//Home page
	_assertVisible(_image("/(.*)/"));
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("304410/304411/304414/304420/304424/304425","Verify if the registered user is able to Place Order with discover/Master card/Visa/AMEX/Master credit card/16 digit credit card.");
$t.start();
try
{
	//placing order with different cards
	for(var $i=1;$i<$paymnet.length-1;$i++)
		{
		navigateToShippingPage($generic[1][0],$generic[0][1]);
		//click on use this card button
		//click on use credit card button
		_click(_label("Use credit card", _in(_div("payment-method-options"))));
		//payment
		PaymentDetails($paymnet,$i);
		_setValue(_textbox("dwfrm_billing_paymentMethods_creditCard_cvn"),$paymnet[$i][6]);
		//Billing details
		BillingAddress($billingAddress,0);
		//click on continue button
		_click(_submit("dwfrm_billing_save"));		
	if(mobile.iPad() || mobile.iPhone() || _isSafari())
	{
		_assertVisible(_div("step-3 active chekoutsteps"));
		_assertEqual("3Review & Submit", _getText(_div("step-3 active chekoutsteps")));
		}
		else
			{
			_assertVisible(_div("step-3 active chekoutsteps"));
			_assertEqual("3 Review & Submit", _getText(_div("step-3 active chekoutsteps")));
			}
		//click on place order button
		_click(_submit("button-fancy-large chechout-button"));
	    //thanku
		if(mobile.iPad() || mobile.iPhone())
		{
		_assertVisible(_bold("THANK YOU FOR YOUR ORDER"));
	    }
		else
			{
			_assertVisible(_heading1("Thank you for your order"));
			}
		_assertVisible(_div($paymnet[$i][8],_in(_div("order-payment-instruments"))));
		_assertContainsText($paymnet[$i][8],_div("order-payment-instruments"));
		
		}
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("304397/316891","Verify if a registered user can delete a saved credit card in the application/Verify the Functionality Related to Default Card Functionality in the PAYMENT SETTINGS page as an registered user.");
$t.start();
try
{
	//Delete card
	DeleteCreditCard();

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


//############################ On Account Related Scripts ##########################################
var $t = _testcase("228858", "[Order confirmation] Verify Purchase Totals in order confirmation page.");
$t.start();
try
{
	navigateToShippingPage($generic[0][0],$generic[0][1]);
	if(_isVisible(_row("order-subtotal")))
	{
	$subtotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
	}
	$shippingDiscounts=_extract(_getText(_row("order-shipping  first ")),"/[$](.*)/",true).toString();
	$tax=_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString();
	$total=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
	//click on ON Account button
	_click(_label("On Account", _in(_div("payment-method-options"))));
	
	//
	if(mobile.iPad() || mobile.iPhone() || _isSafari())
	{
	_assertVisible(_div("step-3 active chekoutsteps"));
	_assertEqual("3Review & Submit", _getText(_div("step-3 active chekoutsteps")));
	}
	else
	{
		//step3
		_assertVisible(_div("step-3 active chekoutsteps"));
		_assertEqual("3 Review & Submit", _getText(_div("step-3 active chekoutsteps")));
	}
	//click on place order button
	_click(_submit("button-fancy-large chechout-button"));
    //thanku
	if(mobile.iPad() || mobile.iPhone())
	{
		_assertVisible(_bold("THANK YOU FOR YOUR ORDER"));
        }
	else
		{
		_assertVisible(_heading1("Thank you for your order"));

		}
	//order placed page
	if(_isVisible(_row("order-subtotal")))
	{
	$subtotalocp=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
	}
	$shippingDiscountsocp=_extract(_getText(_row("order-shipping  first ")),"/[$](.*)/",true).toString();
	$taxocp=_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString();
	$totalocp=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
	_assertEqual($subtotal,$subtotalocp);
	_assertEqual($shippingDiscounts,$shippingDiscountsocp);
	_assertEqual($tax,$taxocp);
	_assertEqual($total,$totalocp);
	
}

catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


var $t = _testcase("232232 ", "[Checkout]: Verify the functionality of Change button next to Shipping address on checkout:Step 3 page");
$t.start();
try
{
	
	navigateToCart($generic[0][0],$generic[0][1]);
	//click on checkout
	_click(_submit("dwfrm_cart_checkoutCart"));
	if(mobile.iPad() || mobile.iPhone() || _isSafari())
	{
		_assertVisible(_div("step-2 active chekoutsteps"));
		_assertEqual("2Payment Method", _getText(_div("step-2 active chekoutsteps")));
		}
	else
		{
		//step2
		_assertVisible(_div("step-2 active chekoutsteps"));
		_assertEqual("2 Payment Method", _getText(_div("step-2 active chekoutsteps")));
		}
	
	//click on on account
	_click(_label("On Account", _in(_div("payment-method-options"))));
	//step3
	//checkout step 3
	if(mobile.iPad() || mobile.iPhone() || _isSafari())
	{
		_assertVisible(_div("step-3 active chekoutsteps"));
		_assertEqual("3Review & Submit", _getText(_div("step-3 active chekoutsteps")));
		}
	else{
		_assertVisible(_div("step-3 active chekoutsteps"));
		_assertEqual("3 Review & Submit", _getText(_div("step-3 active chekoutsteps")));
	}
	
	//click on change shipping address
	_click(_link("secondarybtn", _in(_div("selected-shipping-container"))));
	if(mobile.iPad() || mobile.iPhone() || _isSafari())
	{
		_assertVisible(_div("step-1 active chekoutsteps"));
		_assertEqual("1Shipping Address", _getText(_div("step-1 active chekoutsteps")));
	}
	else
		{
	//verify the changed address
	_assertVisible(_div("step-1 active chekoutsteps"));
	_assertEqual("1 Shipping Address", _getText(_div("step-1 active chekoutsteps")));
		}
	
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

//Clear cart items
ClearCartItems();

var $t = _testcase("228690", "[Checkout: Step 3 Expanded]: Verify the functionality of See Less button on Checkout: Step 3 expanded page");
$t.start();
try
{
	
	for($i=0; $i<4; $i++)
	{
	search($generic[$i][2]);
	
	if (_isVisible(_link("name-link", _in(_div("product-name")))))
		{
		_click(_link("name-link", _in(_div("product-name"))));
		}

	selectSwatchAndQuantity($generic[0][1]);
	if(_isVisible(_button("Add to Cart")))
	{
	_click(_button("Add to Cart"));
	}
else
	{
	_click(_submit("add-to-cart"));
	}
    }
_click(_click(_link("CHECKOUT")));
//click on checkout
_click(_submit("dwfrm_cart_checkoutCart"));
//click on ON Account button
_click(_label("On Account", _in(_div("payment-method-options"))));
if(mobile.iPad() || mobile.iPhone() || _isSafari())
{
_assertVisible(_div("step-3 active chekoutsteps"));
_assertEqual("3Review & Submit", _getText(_div("step-3 active chekoutsteps")));
}
else
	{
	_assertVisible(_div("step-3 active chekoutsteps"));
	_assertEqual("3 Review & Submit", _getText(_div("step-3 active chekoutsteps")));
	}
_assertVisible(_div("3. Review & Submit"));

// //product count before click of see more button
//$Prodcountbefore=_count("_div","showthisitem ",_in(_div("item-list checkout-details")));
////click on see more link
//_click(_link("transparent-button see-more"));
//$Prodcountafter=_count("_div","/showthisitem/",_in(_div("item-list checkout-details")));
//_assertNotEqual($Prodcountbefore,$Prodcountafter);

 for($i=0;$i<$Prodcountafter.length;$i++)
   {
   _assertVisible(_div("product-brand["+$i+"]"));
   _assertVisible(_div("name["+$i+"]"));
   _assertVisible(_span("Size:["+$i+"]"));
   _assertVisible(_span("SKU:["+$i+"]"));
   _assertVisible(_span("Price["+$i+"]"));
   _assertVisible(_div("item-quantity column["+$i+"]"));
   _assertVisible(_div("item-total column["+$i+"]"));
}
	
	$seemorecount=_count("_div","/showthisitem/",_in(_div("item-list checkout-details")));
	_assertEqual($Prodcountafter,$seemorecount);
	//click on see less button
	_click(_link("transparent-button see-less"));
	//$count
	$seelesscount=_count("_div","showthisitem ",_in(_div("item-list checkout-details")));
	//count after see less click
	_assertEqual($Prodcountbefore,$seelesscount);
	_assertNotEqual($seemorecount,$seelesscount);
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

//Clear cart items
ClearCartItems();

var $t = _testcase("232238", "[Checkout]: Verify On Account Button in Checkout page.");
$t.start();
try
{
	navigateToCart($generic[0][0],$generic[0][1]);
	//click on checkout
	_click(_submit("dwfrm_cart_checkoutCart"));
	if(mobile.iPad() || mobile.iPhone() || _isSafari())
	{
		_assertVisible(_div("step-2 active chekoutsteps"));
		_assertEqual("2Payment Method", _getText(_div("step-2 active chekoutsteps")));
		}
	else
		{
		//step2
		_assertVisible(_div("step-2 active chekoutsteps"));
		_assertEqual("2 Payment Method", _getText(_div("step-2 active chekoutsteps")));
		}
	
	//click on on account
	_click(_label("On Account", _in(_div("payment-method-options"))));
	//step3
	//checkout step 3
	if(mobile.iPad() || mobile.iPhone() || _isSafari())
	{
		_assertVisible(_div("step-3 active chekoutsteps"));
		_assertEqual("3Review & Submit", _getText(_div("step-3 active chekoutsteps")));
		}
	else{
		_assertVisible(_div("step-3 active chekoutsteps"));
		_assertEqual("3 Review & Submit", _getText(_div("step-3 active chekoutsteps")));
	}
	
	
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


//################################### Paypal related scripts #####################################

var $t = _testcase("321628","Verify whether user is able to place order through Paypal credit payment menthod.");
$t.start();
try
{
	
//navigate to shipping page
navigateToShippingPage($generic[0][0],$generic[0][1]);
//selecting shipping address
if(mobile.iPad() || mobile.iPhone() || _isSafari())
{
_assertVisible(_div("step-2 active chekoutsteps"));
_assertEqual("2Payment Method", _getText(_div("step-2 active chekoutsteps")));
}
else
	{
	//step2
	_assertVisible(_div("step-2 active chekoutsteps"));
	_assertEqual("2 Payment Method", _getText(_div("step-2 active chekoutsteps")));
	}

_wait(8000);

//click on paypal credit 
_click(_label("PayPal Credit"));

_wait(20000);

//if the page is in order review then click place order button
if (_isVisible(_submit("/button-fancy-large/")))
{
_click(_submit("/button-fancy-large/"));
}
else
	{
	//enter valid credentials 
	paypal($PayPal_Data[0][0],$PayPal_Data[1][0]);
	//click on place order button
	_click(_submit("/button-fancy-large/"));
	}

//thanku
if(mobile.iPad() || mobile.iPhone())
{
	_assertVisible(_bold("THANK YOU FOR YOUR ORDER"));
  }
else
	{
	_assertVisible(_heading1("Thank you for your order"));

	}
_assertEqual($ProductName, _getText(_div("name")));


}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("321631","Verify whether user able place order with paypal.");
$t.start();
try
{
	
//navigate to shipping page
navigateToShippingPage($generic[0][0],$generic[0][1]);
//selecting shipping address
if(mobile.iPad() || mobile.iPhone() || _isSafari())
{
_assertVisible(_div("step-2 active chekoutsteps"));
_assertEqual("2Payment Method", _getText(_div("step-2 active chekoutsteps")));
}
else
	{
	//step2
	_assertVisible(_div("step-2 active chekoutsteps"));
	_assertEqual("2 Payment Method", _getText(_div("step-2 active chekoutsteps")));
	}

_wait(7000);
//click on paypal credit 
_click(_label("PayPal"));

_wait(20000);

//if the page is in order review then click place order button
if (_isVisible(_submit("/button-fancy-large/")))
{
_click(_submit("/button-fancy-large/"));
}
else
	{
	//enter valid credentials 
	paypal($PayPal_Data[0][0],$PayPal_Data[1][0]);
	//click on place order button
	_click(_submit("/button-fancy-large/"));
	}

//thanku
if(mobile.iPad() || mobile.iPhone())
{
	_assertVisible(_bold("THANK YOU FOR YOUR ORDER"));
  }
else
	{
	_assertVisible(_heading1("Thank you for your order"));
	}
_assertEqual($ProductName, _getText(_div("name")));


}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();



/*var $t = _testcase(" "," ");
$t.start();
try
{

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();*/
