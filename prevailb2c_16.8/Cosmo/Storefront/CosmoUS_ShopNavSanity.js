_include("../GenericLibrary/GlobalFunctions.js");
_resource("SanityData.xls");

//SiteURLs();
//_setAccessorIgnoreCase(true); 
//cleanup();
//login_Owner();
//ClearCartItems();
RemoveFromList();

/*var $t = _testcase("227713", "Verify the functionality of 'View All Products' link in the bar on the left on Search results page");
$t.start();
try 
{
	search($generic[0][0]);
	_assertVisible(_div("breadcrumb"));
	_assertEqual($generic[0][6]+" \""+$generic[0][0]+"\"", _getText(_div("breadcrumb")));
	//expand product filter
	_click(_div("Product Line"));
	//view all
	_click(_listItem("View All Product Lines"));
	//search overlay
	_assertVisible(_div("dialog-container"));
	_assertVisible(_div("PRODUCT LINE", _in(_div("dialog-container"))));
	
	//click on close
	_click(_link("close"));
}
catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("228235 ", "[PDP - Product with Large content]: Verify the functionality of Search textbox on PDP for Products with Color Variants");
$t.start();
try 
{
	search($generic[0][7]);
	if(isMobile()|| _isSafari())
		{
		$SKU=_getText(_div("swatch-sku"));
		}
	else
		{
		$SKU=_extract(_getText(_div("swatch-sku")), "/[#] (.*)/", true);
		}
	
	//valid
	_setValue(_searchbox("/(.*)/"),$SKU);
	var $Swatches=_count("_cell", "/variantId/", _in(_table("variations_table")));
	_assertEqual("1",$Swatches);
	if(isMobile()|| _isSafari())
	{
		var $Actual=_getText(_div("swatch-sku", _in(_table("variations_table"))));
	}
else
	{
	var $Actual=_extract(_getText(_div("swatch-sku", _in(_table("variations_table")))), "/[#] (.*)/", true);
	}
	_assertEqual($SKU,$Actual);
	
	search($generic[0][7]);
	//invalid
	_setValue(_searchbox("/(.*)/"),$SKU+0);
	_assertVisible(_cell("dataTables_empty"));

	_assertEqual("No matching records found", _getText(_cell("dataTables_empty")));
}
catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end();	

//Clear cart items
ClearCartItems();

var $t = _testcase("228024", "Verify the functionality of Add to Cart button on PDP page");
$t.start();
try 
{
	NavigateToPDP($generic[0][10],$generic[1][10]);
	//set qty
	_setValue(_textbox("input-text"), "2");
	//select swatches
	if(_isVisible(_table("variations_table")))
		{
			if(!_isVisible(_row("/selected /", _in(_table("variations_table")))))
			{
				_click(_cell("/variantId/", _in(_table("variations_table"))));
			}
		}	
	//click on add to cart
	if(_isVisible(_button("add-to-cart")))
		{
		_click(_button("add-to-cart"));
		}
   else
		{
	   _click(_submit("add-to-cart"));
		}
	//verification
	_assertEqual("2",_getText(_span("mini-cart-total-label", _in(_div("/navigation-bar/")))));
	//navigate to cart
	_click(_link("mini-cart-link", _in(_div("/navigation-bar/"))));
	//click on view cart
	_click(_link("Checkout"));
	//verification
	//name
	_assertVisible(_div("name"));
	//qty
	_assertEqual("2", _getValue(_textbox("input-text", _in(_div("item-quantity column")))));
	}
catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end();

//##################### Scripts related to Wish List ###############################

var $t = _testcase("227572", "[List Selected View]: Verify the functionality of 'Add to Cart' button in My Lists: List selected View page.");
$t.start();
try
{
	
//create list
additemsToWishListAccount($generic[0][5],$generic[0][0],$generic[0][1]);
$prodname = _getText(_div("product-name"));
_log("Product Name In PDP"+$prodname);
 navigateToMyList();
_click(_link("Manage", _in(_div("MyListView"))));
//List name assertion
_assertEqual($generic[0][5], _getText(_heading1("content-header list-name")));
//click on add to cart
_click(_submit("Add to Cart", _in(_div("mylist-registry"))));
//click on cart
_click(_link("mini-cart-link", _in(_div("/navigation-bar/"))));
//click on view cart
_click(_link("Checkout"));
//cart page
_assertVisible(_heading1("Cart"));
//prod name in cart
 var $prodnamein_wl=_getText(_div("name", _in(_div("item-details"))));
 _log("Product Name In PDP"+$prodnamein_wl);
	 
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("304409","Verify the edit details functionality for a product in My list page of the application as a registered user");
$t.start();
try
{
	
	//Navigate to my list page
	_click(_link("My Account", _in(_listItem("my-account dd3 last"))));
	//click on wish list 
	_click(_link("My Lists", _in(_span("My Lists"))));
	//click on manage link
	_click(_link("Manage", _in(_div("PFSweb Manage"))));
	//Edit product details link
	_click(_link("Edit Details"));
	//Set the quantity as 2
	_setValue(_textbox("input-text"),$generic[2][1]);
	//adding to list 
	_click(_link("Add to List"));
	//select created list
	_setSelected(_select("MyLists"),$generic[0][5]);
	//Click on send link
	_click(_submit("Send"));
	 navigateToMyList();
	//quantity should update accordingly
	 _click(_link("Manage"));
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("304402","Verify the remove functionality for a product in My list page of the application as a registered user");
$t.start();
try
{
	
	//Products in wish list
	var $ProductCount=_count("_div","item-image",_div("mylist-registry"));
	_click(_submit("dwfrm_giftregistry_remove"));
	
	if ($ProductCount==1)
		{
		_assertVisible(_paragraph("You have no items in your list."));
		_assertVisible(_link("add-items"));
		}
		else
			{
			var $ProductCountAfter=_count("_div","item-image",_div("mylist-registry"));
			_assertEqual($ProductCountAfter,$ProductCount-1);
			}
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

*/

var $t = _testcase("228021", "Verify the functionality of Add to Favourites icon on PDP image");
$t.start();
try 
{
	
	NavigateToPDP($generic[0][10],$generic[1][10]);
	selectSwatchAndQuantity($generic[0][1]);
	//Add to Favorites
	_click(_link("favoriteList remove"));
	//verification
	//navigate back to favorites
	if(isMobile())
		{
			//click on hamburger
		_click(_div("visually-hidden navigation-header", _in(_div("header"))));
			//Navigate to my account
			//_click(_link("My Account"));
			//Payment
		_click(_link("Favorites "));

		}	
	else
		{
			//Navigate to my account
		_click(_link("My Account", _in(_listItem("my-account dd3 last"))));
			//Payment
		_click(_link("Favorites", _in(_div("secondary-navigation"))));
		}
	//verifying the list
	_assertVisible(_div("name"));
	//_assertEqual($ProductName, _getText(_div("name")));
	
	NavigateToPDP($generic[0][10],$generic[1][10]);
	//click on add to favorites list
	_click(_link("favoriteList added"));
	//navigate back to favorites
	if(isMobile())
		{
			//click on hamburger
		_click(_div("visually-hidden navigation-header", _in(_div("header"))));
			//Navigate to my account
			//_click(_link("My Account"));
			//Payment
			//_click(_link("Favorites", _in(_div("secondary-navigation"))));
		_click(_link("Favorites", _in(_listItem("Favorites"))));
		}	
	else
		{
			//Navigate to my account
		_click(_link("My Account", _in(_listItem("my-account dd3 last"))));
			//Payment
		_click(_link("Favorites", _in(_div("secondary-navigation"))));
		}
	//item should get removed
	_assertNotVisible(_div("item-list"));
	_assertNotVisible(_div("item-details"));
	
}
catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("228022", "[PDP Page : Simple] - iPad: Verify the functionality of QTY textbox on PDP page");
$t.start();
try 
{
	search("719612");
	//QTY Validations
	for($i=0; $i<$Qty.length; $i++)
		{
		if($i==0)
			{
			_setValue(_textbox("/input-text/"),$Qty[$i][1]);
			_click(_submit("Add to Cart"));
			$cartcount=_getText(_span("mini-cart-total-label"));
			_log($cartcount);
			_assertEqual("1",$cartcount);
			
			}
		else if($i==1)
			{
			_setValue(_textbox("/input-text/"),$Qty[$i][1]);
			var $QTY=_getValue(_textbox("/input-text/"));
			_click(_submit("Add to Cart"));
			var $updateQTY=_getText(_span("mini-cart-total-label"));
			_assertNotEqual($QTY,$updateQTY);
			}
}
	
}
catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("228262", "[PDP - Alternate State]: Verify the functionality of Create New List textbox on Add to My Lists modal");
$t.start();
try 
{
	NavigateToPDP($generic[0][10],$generic[1][10]);
	//click on add to my list
	_click(_link("Add to List"));
	
	_setSelected(_select("MyLists"),$generic[0][8]);
	//click on send
	_click(_submit("Send"));
	//add to list overlay should get closed
	_assertNotVisible(_div("add-to-mylist-dialog"));	
	//Item should get added to list
	_assertVisible(_link("Added to "+$generic[0][8]));
	if(isMobile())
		{
			//click on hamburger
		_click(_div("visually-hidden navigation-header", _in(_div("header"))));
			//Navigate to my account
		_click(_link("My Account"));
			//lists
		_click(_link("My Lists"));
			//click on list
		_click(_link($generic[0][8]));
			//product name
			_assertEqual($ProductName, _getText(_div("name")));
	
		}	
	else
		{
			//Navigate to my account
		_click(_link("My Account", _in(_listItem("my-account dd3 last"))));
			//lists
		_click(_link("My Lists", _in(_div("secondary-navigation"))));
			//click on list
		_click(_link($generic[0][8], _in(_div("secondary-navigation"))));
			//product name
			_assertEqual($ProductName,_getText(_div("name")));
		}
	
}
catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("228285", "Verify the search functionality in no search result page");
$t.start();
try 
{
	_setValue(_textbox("q"), $generic[0][9]);
	_click(_submit("go"));
	if(_isSafari() || isMobile())
		{
		_assertEqual("Your search for \"sadasded \" produced no results. Please enter a few keywords to try again.", _getText(_div("section-header")));

		}
	else
		{
		//No search result page
		_assertEqual("Your search for \"sadasded \" produced no results. Please enter a few keywords to try again.", _getText(_div("section-header")));
}
	
	_setValue(_textbox("q"), $generic[0][0]);
	_click(_submit("go"));
	_assertVisible(_div("breadcrumb"));
	_assertEqual($generic[0][6]+" \""+$generic[0][0]+"\"", _getText(_div("breadcrumb")));
}
catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("228286", "Verify the search suggestion functionality");
$t.start();
try 
{
	if(isMobile())
		{
		_click(_image("mobile_search.png", _in(_div("header-searchicon"))));
		}
	else
		{
		_click(_image("searchicon.jpg", _in(_div("header-searchicon hide-mobile"))));		
		}
	_setValue(_textbox("Search"), $generic[0][0]);	
	//search suggestion
	_assertVisible(_div("shampoo"));
	
	_click(_div("shampoo"));
	//breadcrumb
	_assertVisible(_div("breadcrumb"));
	_assertEqual($generic[0][6]+" \""+$generic[1][0]+"\"", _getText(_div("breadcrumb")));
}
catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("228702", "Verify the ADD to cart button functionality in Quick view");
$t.start();
try 
{

	NavigateToQuickView($generic[0][0],$generic[0][1]);
	//set qty
	_setValue(_textbox("Quantity"), "2");
	//click on add to cart
	_click(_submit("add-to-cart"));
	//verification
	_assertEqual("2",_getText(_span("mini-cart-total-label", _in(_div("/navigation-bar/")))));
	//navigate to cart
	_click(_link("mini-cart-link", _in(_div("/navigation-bar/"))));
	_click(_link("View Cart"));
	//verification
	//name
	_assertVisible(_div("name"));
	//_assertEqual($ProductName, _getText(_div("name")));
	//price
	_assertVisible(_div("item-price column"));
	//_assertEqual($Price,_extract(_getText(_span("price-standard")),"/[$](.*)/",true).toString());
	//qty
	_assertEqual("2", _getValue(_textbox("/input-text/", _in(_div("item-quantity column")))));
	
}
catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("228721", "Verify the ADD to cart button functionality in Quick view for the product having swatches.");
$t.start();
try 
{
	NavigateToQuickView($generic[0][0],$generic[0][1]);
	//set qty
	_setValue(_textbox("input-text"), "2");
	//select swatches
	if(!_isVisible(_row("/selected /", _in(_table("variations_table")))))
		{
			_click(_cell("/variantId/", _in(_table("variations_table"))));
		}
	//click on add to cart
	_click(_button("add-to-cart"));
	//verification
	_assertEqual("2",_getText(_span("mini-cart-total-label", _in(_div("/navigation-bar/")))));
	//navigate to cart
	_click(_link("mini-cart-link", _in(_div("/navigation-bar/"))));
    //click on view cart
	_click(_link("View Cart"));
   
	
	//_assertEqual($Price,_extract(_getText(_span("price-standard")),"/[$](.*)/",true).toString());
	//qty
	_assertEqual("2", _getValue(_textbox("/input-text/", _in(_div("item-quantity column")))));
}
catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("227716", "[Search Page]: Verify the functionality of Filters, on Search results page.");
$t.start();
try 
{
	search($generic[0][0]);
	//click on brand filter
	_click(_div("Brand"));
	//click on view all brand
	_click(_listItem("/showViewAll/", _in(_list("scrollable Brand"))));
	var $filter=_collectAttributes("_listItem","/(.*)/","sahiText", _in(_div("dialog-container")));
	var $BrandNames=new Array();
	for($i=0; $i<5; $i++)
		{
		_click(_link($filter[$i],_in(_div("dialog-container"))));
		//Respective refinement should be selected
		_assertEqual($filter[$i],_getText(_listItem("selected  ["+$i+"]", _in(_div("dialog-container")))));
		//Verify the Breadcrumb
		_assertEqual("/"+$filter[$i]+"/i", _getText(_span("breadcrumb-refinement-value["+$i+"]", _in(_div("selectedRefinements")))));
		_assertVisible(_image("Remove["+$i+"]", _in(_div("selectedRefinements"))));
		$BrandNames.push("/"+$filter[$i]+"/i");
       }
	//Verify all selected values together
	var $SelectedRefines=_collectAttributes("_listItem","/selected/","sahiText", _in(_div("dialog-container")));
	_assertEqualArrays($BrandNames,$SelectedRefines);
	
	var $RefinesInBreadCrumb=_collectAttributes("_span","/breadcrumb-refinement-value/i","sahiText", _in(_div("dialog-container")));
	_assertEqualArrays($RefinesInBreadCrumb,$SelectedRefines);
	_click(_link("Clear All", _in(_div("dialog-container"))));
	_wait(2000);
	_assertNotVisible(_div("selectedRefinements"));
	_assertNotVisible(_listItem("/selected/", _in(_div("dialog-container"))));
   //click on close
	_click(_link("close"));
	//overlay should get closed
	_assertNotVisible(_div("dialog-container"));
		
		}
catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end();

//############## Related to blog page ###################
var $t = _testcase("230526","[Beauty Blog]: Verify the Filter by Blog Functionality in Beauty Blog page.");
$t.start();
try
{
	
	//navigate to blog
	 NavigateToBlog();
	 //verifying the filter by brand
	 _assertVisible(_div("refinement Brand", _in(_div("refinmentSection"))));
	 //selecting any post refinement 
	 _setSelected(_select("selectRefinements", _in(_div("refinement Brand"))), "Farouk");
	 //click on apply
	 _click(_link("Apply Filter", _in(_div("refinmentSection"))));
	 //navigating to the blog list page 
	 _assertVisible(_heading2("blog", _in(_div("blog"))));
	 //result text
	 _assertVisible(_span("Results", _in(_div("result-messages"))));
	var $noOfBlog=_count("_div","listArticle", _in(_div("Article-left")));
	 for(var $i=0;$i<$noOfBlog;$i++)
		 {
	  _assertVisible(_div("listArticle["+$i+"]", _in(_div("Article-left"))));
		 }

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var  $t = _testcase("230574","[Beauty Blog]: Verify the search functionality in beauty Blog mian page.")
$t.start();
try
{
	//navigate to blog
	 NavigateToBlog();
	//enter any invalid data in search blog box
	 _setValue(_textbox("q", _in(_div("blog-search"))), "brand");
	 //click on go button
	 _click(_submit("blogSearchBtn"));
	 // navigating to the searched blog page
	 _assertVisible(_heading2("blog"));
	 _assertVisible(_span("Results", _in(_div("result-messages"))));
	 _assertVisible(_list("Article-folder-content-list", _in(_div("search-results-content"))));
	 if(_isVisible(_div("Article-left")))
		 {//image
		 _assertVisible(_image("contentThumbnail", _in(_div("listArticle"))));
		 //article
		 _assertVisible(_div("listArticle-right", _in(_div("listArticle"))));
}
	 else
		 {
		 _assertNotVisible(_div("Article-left"));
		 }
}
catch ($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase(" "," ");
$t.start();
try
{

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

/*var $t = _testcase(" "," ");
$t.start();
try
{

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();*/