_include("../GenericLibrary/GlobalFunctions.js");
_resource("SanityData.xls");


SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();
login_Owner();
ClearCartItems();

var $t = _testcase("228641/228631","[Cart Summary Page]: Verify the functionality of Remove link for a product on the Cart page");
$t.start();
try
{
	//navigate to cart page
	navigateToCart($generic[0][0],$generic[0][1]);
	//product name should be equal
	//_assertEqual($ProductName,$pName);
	//click on checkout
	_click(_submit("dwfrm_cart_checkoutCart"));
	//to check whether it is navigated to 
	//checkout page
	_assertVisible(_heading1("Checkout"));
	_assertEqual("1. Shipping Address", _getText(_div("page-header")));
	//checkout steps
	_assertVisible(_div("step-1 null chekoutsteps"));
	_assertVisible(_div("step-2 active chekoutsteps"));
	_assertVisible(_div("Payment Method"));
	_assertVisible(_div("step-3 inactive chekoutsteps"));

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("228641","[Cart Summary Page]: Verify the functionality of Remove link for a product on the Cart page");
$t.start();
try
{

	//click on mini cart link
	_click(_link("mini-cart-link"));
	//navigate to cart page again
	_click(_link("Checkout"));
	
	//Deleting product in cart 
	var $prodCountBeforeDelete=_count("_div","name",_in(_div("cart-table")));
	
	//Delete a product
	_click(_submit("Remove"));
	
	if ($prodCountBeforeDelete==1)
		{
		_assertEqual("Your Shopping Cart is Empty.", _getText(_div("cart-empty")));
		}
		else
			{
			var $prodCountAfterDelete=_count("_div","name",_in(_div("cart-table")));
			_assertEqual($prodCountAfterDelete,$prodCountBeforeDelete-1);
			}


}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

/*var $t = _testcase(" "," ");
$t.start();
try
{

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();*/