_setXHRReadyStatesToWaitFor("2,3");
_include("BrowserSpecific.js");
_include("BM_Functions.js");
_resource("User_Credentials.xls");
_include("../ObjectRepository/Generic_OR.sah");

var $URL=_readExcelFile("User_Credentials.xls","URL");
var $userLog=_readExcelFile("User_Credentials.xls","BM_Login");

var $UserID=$userLog[0][0];
var $Password=$userLog[0][1];
var $BrandName;
var $ProductName;
var $price;
var $SKu;
var $Qty;



//############### BM related Function ######################

function deleteUser()
{
	 data();
	BM_Login();
	_click($BM_CUSTOMER_LINK);
	_click($BM_CUSTOMER_LINK2);
	_click($BM_ADVANCE_LINK);
	_click($BM_ADVANCE_LINK);
	_setValue($BM_EMAIL_TEXTBOX,$email);
	_click($BM_FIND_BUTTON);
	if(_isVisible($BM_SELECTALL_LINK))
	{
		if(_assertEqual($email,_getText($BM_REGISTERED_EMAILID)))
			{
			_click($BM_DELETECUSTOMER_CHECKBOX);
			_click($BM_DELETECUSTOMER_BUTTON);
			_click($BM_CONFIRMOK_BUTTON);
			_log("Successfully deleted the user");
			}
	}
	else
	{
		_log("search did not match any users.");
	}
	_click($BM_LOGOUT_LINK);
}

function deleteProfileUser($Providedemail)
{
	 data();
	BM_Login();
	_click($BM_CUSTOMER_LINK);
	_click($BM_CUSTOMER_LINK2);
	_click($BM_ADVANCE_LINK);
	_click($BM_ADVANCE_LINK);
	_setValue($BM_EMAIL_TEXTBOX,$Providedemail);
	_click($BM_FIND_BUTTON);
	if(_isVisible($BM_SELECTALL_LINK))
	{
		if(_assertEqual($Providedemail,_getText($BM_REGISTERED_EMAILID)))
			{
			_click($BM_DELETECUSTOMER_CHECKBOX);
			_click($BM_DELETECUSTOMER_BUTTON);
			_click($BM_CONFIRMOK_BUTTON);
			_log("Successfully deleted the user");
			}
	}
	else
	{
		_log("search did not match any users.");
	}
	_click($BM_LOGOUT_LINK);
	
}

//Function take a screen shot when a their is a script failure

function onScriptError()
{
  _log("Error log"); 
  _lockWindow(5000);
  _focusWindow();
  _takePageScreenShot();
  _unlockWindow();
}

function onScriptFailure()
{
  _log("Failure log");
  _lockWindow(5000);
  _focusWindow();
  _takePageScreenShot();
  _unlockWindow();
}

function OwnerUser() 
{
BM_Login();
_click(_link("Customers"));
_click(_link("Customers[1]"));
_click(_link("Advanced"));
_setValue(_textbox("WFCustomerAdvancedSearch_Login"),$uId);
_click(_submit("Find", _in(_div("/Advanced Customer Search/"))));
_wait(4000,_isVisible(_row("/Select All/")));
if(_isVisible(_row("/Select All/")))
{
	 if(_isVisible(_link("table_detail_link")))
     {
  if(_checkbox("/Metabc8bciaai/"),_rightOf(_span("Is Child Account")).checked)
     {
    _uncheck(_checkbox("/Metabc8bciaai/"),_rightOf(_span("Is Child Account")));
   _check(_checkbox("/Metacddx2iaai/"));
   _click(_submit("Apply"));

     }
  _check(_checkbox("/Metacddx2iaai/"));
  _click(_submit("Apply"));
     }
  else
  {
  _log("search did not match any users.");
  }
   
	 _click(_link("Log off."));
  }
}


// ###################################### Generic to the project #############################
function SiteURLs()
{
	_navigateTo($URL[0][0]);
	if(_isVisible(_submit("Authenticate")))
	{
	_setValue(_textbox("authUser"), $userLog[0][3]);
	_setValue(_password("authPassword"), $userLog[0][4]);
	_click(_submit("Authenticate"));
	}
}

function cleanup()
{
	
	//logout
	Logout();
	//Clearing the items from cart
	ClearCartItems();
	//logout from the application
	if(isMobile())
		{
			//click on hamburger
			//_click(_heading1("visually-hidden navigation-header", _in(_div("header"))));
		_click(_div("visually-hidden navigation-header", _in(_div("navigation"))));
			//logout
			if(_isVisible(_link("/logout-btn/")))
				{
				_click(_link("/logout-btn/"));
				}
			else
				{
				//_click(_heading1("visually-hidden navigation-header active"));
				_click(_div("visually-hidden navigation-header active"));
				}
			}
			
	else
		{
			if(_isVisible(_link("My Account", _in(_list("menu-utility-user")))))
			{
				_mouseOver(_link("My Account", _in(_list("menu-utility-user"))));
				_click(_link("/logout-btn/", _in(_list("menu-utility-user"))));		
			}
		}
}


function round(value,exp) {
    if (typeof exp === 'undefined' || +exp === 0)
      return Math.round(value);

    value = +value;
    exp  = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
      return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
  }



//###################################### Profile Related Functions #############################

function login_Owner()
{
	
//	if(mobile.iPad() || mobile.iPhone() || mobile.Android())
//		{
//		_wait(2000);
//		//click on hamburger
//		_click(_div("visually-hidden navigation-header", _in(_div("navigation"))));
// 		_click(_link("user-login", _in(_list("menu-category level-1 clearfix active"))));
//		}
//	else
//		{
//		_click(_link("user-login", _in(_div("/navigation-bar/"))));
//
//		}
	 _setValue(_textbox("dwfrm_login_username"),$uId);
	 _setValue(_password("dwfrm_login_password"),$pwd);
	
   if(_isVisible(_submit("Login")))
   {
	   _click(_submit("dwfrm_login_login"));
   }
  else
   {
       _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
   }
   
   //wait till loder loads
   _wait(20000,(!_isVisible(_div("loader-indicator"))));
   
   if (_isVisible(_div("account-shipping")))
   {
	   _click(_submit("primaryButtn"));
   }
   
   //click on home page link
   //_click(_image("cosmoprof_logo.png"));
   
}

function Addresspopup()
{
	_wait(10000,_isVisible(_div("dialog-container")));
	 if(_isVisible(_div("dialog-container")))
	 {
		 _setSelected(_select("shop_address_list"),1);
		 //click on shop button
		 _click(_submit("shop"));
		 if(!isMobile())
			 {
			 _assertVisible(_link("My Account", _in(_div("navigation-bar"))));

			 }
	
		}
	 
}


function Logout()
{
	if(mobile.device()=="desktop" || mobile.AndroidTab())
	{
	    if(_isVisible(_link("My Account", _in(_list("menu-utility-user")))))
	   {
	    _click(_link("/logout-btn/", _in(_listItem("/my-account/"))));		
	    }
	}
	else
	{		

		_click(_heading1("visually-hidden navigation-header"));
	       //_click(_link("My Account"));
	       _wait(4000,_isVisible(_div("user-details")));
	       if(_isVisible(_link("/logout-btn/")))
	       {
	    	   _click(_link("/logout-btn/", _in(_list("menu-category level-1 clearfix active"))));
	    	   _wait(5000,!_isVisible(_link("/logout-btn/")));
	       }
	}

}

function addAddress($sheet,$r)
{
	
  	_setValue(_textbox("dwfrm_profile_address_addresslabel"), $sheet[$r][1]);
	_setValue(_textbox("dwfrm_profile_address_firstname"), $sheet[$r][2]);
	_setValue(_textbox("dwfrm_profile_address_lastname"), $sheet[$r][3]);
	_setValue(_textbox("dwfrm_profile_address_address1"), $sheet[$r][4]);
	_setValue(_textbox("dwfrm_profile_address_address2"), $sheet[$r][5]);
    _setValue(_textbox("dwfrm_profile_address_city"),$sheet[$r][6]);
    //_setSelected(_select("dwfrm_profile_address_country"),$sheet[$r][7]);
    _setSelected(_select("dwfrm_profile_address_states_state"), $sheet[$r][7]);
	_setValue(_textbox("dwfrm_profile_address_zip"), $sheet[$r][8]);
    _setValue(_textbox("dwfrm_profile_address_phone"),$sheet[$r][9]);

    //click on save
    _click(_submit("dwfrm_profile_address_create"));
    //wait
    _wait(4000);
    
    if (_isVisible(_div("addressvalidholder")))
    {
    	_click(_submit("dwfrm_customeraddress_edit"));
    }
    
}

function deleteAddress()
{
       if (isMobile() && !mobile.AndroidTab())
       {
       _click(_link("/(.*)/",_in(_div("hamburger_icon hide-mobile"))));
       }
       _click(_link("My Account",_in(_div("navigation-bar"))));
       _click(_link("Addresses",_in(_div("secondary-navigation"))));
       _wait(4000,_isVisible(_list("address-list")));
     //deleting the existing addresses
  /*   while (_isVisible(_link("Delete Address")))
    	 {
    	 _click(_link("Delete Address"));
    	 _click(_link("Yes[1]"));
    	 _wait(2000);
    	 }*/
       
       //deleting the existing addresses
       var $TotAddress=_count("_link","/Delete Address/");
          for(var $i=0;$i<$TotAddress;$i++)
              {
        	    //Deleting address
                _click(_link("Delete Address"));
                _click(_link("primaryButtn yes deleteaddress[1]"));
                _wait(6000);
              }
          _assertNotVisible(_div("address-info[1]"));
          //_assertNotVisible(_list("address-list"));
}

function EditAddress($validations,$r)
{
	_setValue(_textbox("dwfrm_profile_address_addresslabel"), $validations[$r][1]);
	_setValue(_textbox("dwfrm_profile_address_firstname"), $validations[$r][2]);
	_setValue(_textbox("dwfrm_profile_address_lastname"), $validations[$r][3]);
	_setValue(_textbox("dwfrm_profile_address_address1"), $validations[$r][4]);
	_setValue(_textbox("dwfrm_profile_address_address2"), $validations[$r][5]);
    _setValue(_textbox("dwfrm_profile_address_city"),$validations[$r][6]);
    //_setSelected(_select("dwfrm_profile_address_country"),$validations[$r][7]);
    _setSelected(_select("dwfrm_profile_address_states_state"), $validations[$r][7]);
	_setValue(_textbox("dwfrm_profile_address_zip"), $validations[$r][8]);
    _setValue(_textbox("dwfrm_profile_address_phone"),$validations[$r][9]);
}

function addressoverlay()
{
	//address popup
	_assertVisible(_div("/ui-dialog ui-widget ui-widget-content ui-corner-all addressrecom/"));
	//popup heading
	_assertVisible(_heading1("VERIFY YOUR ADDRESS", _in(_div("address-verify"))));
    //check original address radio button
	_check(_radio("originalAddress"));
	_click(_submit("dwfrm_customeraddress_edit"));
}
function deleteAddressconfirmation()
{
	
	 //deleting the existing addresses
	   if(_isVisible(_link("address-delete delete")));
	   	{
	   		_click(_link("address-delete delete",_rightOf(_listItem("first default"))));
	   		_assertEqual("Are You sure you want to delete this address", _getText(_div("deleteConfirmmessage[1]")));
	   	}
}


function CreateCreditCard($Createcard,$i)
{               
	_setValue(_textbox("dwfrm_paymentinstruments_creditcards_newcreditcard_owner"),$Createcard[$i][1]);
    _setSelected(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_type"),$Createcard[$i][2]);
    _setValue(_textbox("/dwfrm_paymentinstruments_creditcards_newcreditcard_number/"),$Createcard[$i][3]);
    _setSelected(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_month"),$Createcard[$i][4]);
    _setSelected(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_year"),$Createcard[$i][5]);            
}


function DeleteCreditCard()
{               
	//Navigate to my account
	_click(_link("My Account", _in(_div("navigation-bar"))));
	//Payment
	_click(_link("Payment Settings", _in(_div("secondary-navigation"))));
   //click on delete card link
   while(_isVisible(_link("Delete Card")))
       {
	   		_click(_link("Delete Card"));
	   		//click on ok in confirmation overlay
	   	   if(_isVisible(_div("deleteConfirmmessage")))
	   		   {
	   		   		_click(_link("Yes"));
	   		   }
       }	
	//verify whether deleted or not
	_assertNotVisible(_list("payment-list"));
	_assertNotVisible(_link("Delete Card"));
}

function eventTocalendar()

{
	//should select the date 
	var $currentyear1=_getText(_span("ui-datepicker-year"),_in(_div("ui-datepicker-title")));
	
	//select random date
	for (var $i=0;$i<15;$i++)
		{
		//clcik on next button
		_click(_link("Prev"));
		}
	//click link 25
	_click(_link("25",_in(_table("ui-datepicker-calendar"))));
	var $changedyear=_getText(_span("ui-datepicker-year"),_in(_div("ui-datepicker-title")));
	_assert($currentyear1>$changedyear);

}

function navigateToMyList()
{
	if(isMobile())
		{
		_click(_div("visually-hidden navigation-header", _in(_div("navigation"))));
		_click(_link("MY LISTS", _in(_list("menu-category level-1 clearfix active"))));
		}
	else
		{
		//clcik on logo
		_click(_image("/(.*)/", _in(_span("show-desktop"))));
		//click on my account
		_click(_link("My Account",_in(_div("/navigation-bar/"))));
		 //click on my list
		_click(_link("VIEW MY LISTS", _in(_div("cp_MyaccMain"))));
		
		
		}
	
	}

function deleteList()
{
	
	//Navigate to list
	if(isMobile())
		{
			//click on hamburger
		_click(_div("visually-hidden navigation-header", _in(_div("header"))));

			//Navigate to my account
			//_click(_link("My Account"));
			//lists
		_click(_link("My Lists"));
			if(_isVisible(_listItem("level-1 hide-desktop mylist")))
                 {
				var $list=_count("_link", "/ProductListID/", _in(_listItem("level-1 hide-desktop mylist")));

				for(var $i=0;$i<$list;$i++)
				{

					_click(_link("/ProductListID/"));
					_click(_link("Delete List"));
				
					_click(_button("Delete List"));
					_click(_div("visually-hidden navigation-header", _in(_div("header"))));
					_click(_link("My Lists"));

				}
				//_click(_heading1("visually-hidden navigation-header active"));
         }
			
		}	
	else
		{
			//Navigate to my account
			_click(_link("My Account", _in(_listItem("my-account dd3 last"))));
			//lists
			_click(_link("My Lists", _in(_div("secondary-navigation"))));
			if(_isVisible(_div("item-list gift-reg Mylistview")))
			{
				var $list=_count("_div", "/MyListView/", _in(_div("item-list gift-reg Mylistview")));
				for(var $i=0;$i<$list;$i++)
					{
					     _click(_link("View"));
						_click(_link("Delete List"));
					
						_click(_button("Delete List"));
					}
			}
		}
	
}


function RemoveFromList()
{
	navigateToMyList();
	//click on remove product from list
	deleteList();
	if(_isVisible(_div("event-name")))
		{
		_click(_link("Manage List", _in(_div("item-list gift-reg Mylistview"))));
		if(_isVisible(_link("Delete", _in(_div("mylist-registry")))))
			{
			
			_click(_link("Delete List", _in(_div("mylist-registry"))));
			_click(_button("Delete List"));
			}
		}
}
	
function additemsToWishListAccount($Listname,$cat,$qty)
{
	
	AddToMyList($Listname);
    //add items
	_click(_link("Add Items"));
    _click(_link($cat));
    _click(_link("name-link"));
    //selecting swatches
    selectSwatchAndQuantity($qty);
    //click on My list
    _click(_link("Add to List"));
    _setSelected(_select("input-select"),1);
    //click on send
    _click(_submit("button-fancy-small add-to-cart"));
    
}

function AddToMyList($Listname)
{
	navigateToMyList();
	_click(_submit("dwfrm_giftregistry_create"));
	_setValue(_textbox("dwfrm_giftregistry_event_name"),$Listname);
	_click(_submit("dwfrm_giftregistry_event_confirm"));
	_click(_link("View", _in(_div("item-list gift-reg Mylistview"))));
}

//#################### Blog Related Functions #################

function NavigateToBlog()
{
	//click on beauty blog
	_click(_link("BLOG", _in(_div("left-Header"))));
	//Beauty blog
	_assertVisible(_heading2("Blog"));
}
//########################## ShopNav Related Functions #####################################

function selectSwatchAndQuantity($quantity)
{
		
	if (_isVisible(_div("swatch-color-name")))
	{
	  _click(_cell("variantId"));
	  var $swatch=_extract(_getText(_div("swatch-color-name",_in(_cell("variantId")))),"/(.*) P/",true);
	}
		
	if (_isVisible(_div("swatch-size-name")))
	{
	  _click(_cell("variantId"));
	 var $swatch=_extract(_getText(_div("swatch-size-name",_in(_cell("variantId")))),"/(.*) Y/",true);
	}
	
	_setValue(_textbox("/input-text/"),$quantity);
}

//search
function search($product)
{	
	if(isMobile())
		{
		_click(_image("mobile_search.png", _in(_div("header-searchicon"))));
		}
	else
		{
		_click(_image("searchicon.jpg", _in(_div("header-searchicon hide-mobile"))));		
		}
	_setValue(_textbox("Search"), $product);
	_click(_submit("go"));
	
}

function NavigateToPDP($cat,$subCat)
{
	//navigating to sub category
	_mouseOver(_link($cat));
	_click(_link($subCat));
	$BrandName=_getText(_link("name-link"));
	$ProductName=_getText(_link("name-link", _in(_div("product-name"))));
	$price=_extract(_getText(_div("product-price")),"/[$](.*)/",true).toString();
	//navigate to PDP
	_click(_link("name-link"));	

}
//########################## Cart Related Functions ########################################

function ClearCartItems()
{	 
	if(_isVisible(_link("mini-cart-link")))
	{ 		
		_click(_link("mini-cart-link"));
		_click(_link("Checkout"));
		if(_isVisible(_row("rowcoupons")))
		 {
			_click(_submit("dwfrm_cart_coupons_i0_deleteCoupon"));
		 }		
		while(_isVisible(_submit("Remove")))
			{
			_click(_submit("Remove"));
			}
	}
}

function addItemToCart($subCat,$quantity)
{
	//navigating to sub category
	_click(_link($subCat));
	_wait(2000);
	//navigate to PDP
	//location.reload(); 
	_click(_link("name-link", _in(_div("product-name"))));
	$BrandName=_getText(_div("product-brand"));
	$ProductName=_getText(_div("product-name"));
	$price=_extract(_getText(_span("price-your")),"/[$](.*)/",true).toString();
	$SKu=_extract(_getText(_div("product-number sku")),"/SKU: (.*)/",true).toString();
	/*if(_isVisible(_div("swatch-size-name")))
		{
		 $size=_extract(_getText(_div("swatch-size-name")),"/(.*) Y/",true).toString();
		}
	else
		{
		$size=_extract(_getText(_div("simple-product-size")),"/[:](.*)/",true).toString();
		}*/
	
   
	//selecting swatches
	selectSwatchAndQuantity($quantity);
	
	$Qty=_getValue(_textbox("/input-text/"));
	//click on add to cart
	if(_isVisible(_button("Add to Cart")))
		{
		_click(_button("Add to Cart"));
		}
	else
		{
		_click(_submit("add-to-cart"));
		}
}

function navigateToCart($subCat,$i)
{
	//add item to cart
	addItemToCart($subCat,$i);
	//navigate to Cart page
	_click(_link("CHECKOUT"));
	$pName=_getText(_link("/(.*)/",_in(_div("name"))));
	$quantity=_getText(_textbox("/input-text/"));
	//$price=_extract(_getText(_span("price-standard")),"/[$](.*)/",true).toString();
		if(_isVisible(_span("price-total")))
			{
		$totalPrice=_extract(_getText(_span("price-total")),"/[$](.*)/",true).toString();
			}
		else if(_isVisible(_span("price-unadjusted")))
			{
			$totalPrice=_extract(_getText(_span("price-adjusted-total")),"/[$](.*)/",true).toString();
			}
		if(_isVisible(_row("/order-discount discount/")))
			{
		    $ShippingDiscounts=_extract(_getText(_row("/order-discount discount/")),"/[$](.*)/",true).toString();
			}
	    $shippingTax=_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString();
		$orderTotal=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
	    $salesTax=_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString();

}


//########################## Checkout related Global Functions ###############################

function navigateToShippingPage($data,$quantity)
{
	//navigate to cart page
	navigateToCart($data,$quantity);	
	//navigating to shipping page
	_click(_submit("dwfrm_cart_checkoutCart"));
	if(isMobile())
		{
			_wait(3000);
		}
	if(_isVisible(_link("Account")))
		{
		_click(_submit("dwfrm_login_unregistered"));
		}
	$subTotal=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
	$tax=parseFloat(_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString());
	_log($tax);
	$shippingTax=parseFloat(_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString());
	_log($shippingTax);
}

function shippingAddress($sheet,$i)
{

	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), $sheet[$i][1]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), $sheet[$i][2]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"), $sheet[$i][3]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"),$sheet[$i][4]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), $sheet[$i][5]);
    _setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), $sheet[$i][6]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_zip"), $sheet[$i][7]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),$sheet[$i][8]);
}

function BillingAddress($sheet,$i)
{
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"), $sheet[$i][1]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"), $sheet[$i][2]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1"), $sheet[$i][3]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address2"),$sheet[$i][4]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_city"), $sheet[$i][5]);
    _setSelected(_select("dwfrm_billing_billingAddress_addressFields_states_state"), $sheet[$i][6]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_zip"), $sheet[$i][7]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_phone"),$sheet[$i][8]);
	
}

function PaymentDetails($sheet,$i)
{
	     _setValue(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"), $sheet[$i][1]);
		 _setSelected(_select("dwfrm_billing_paymentMethods_creditCard_type"), $sheet[$i][2]);
		 _setValue(_textbox("dwfrm_billing_paymentMethods_creditCard_number"), $sheet[$i][3]);
		 _setSelected(_select("dwfrm_billing_paymentMethods_creditCard_month"), $sheet[$i][4]);
	     _setSelected(_select("dwfrm_billing_paymentMethods_creditCard_year"), $sheet[$i][5]);
	     _setValue(_textbox("dwfrm_billing_paymentMethods_creditCard_cvn"), $sheet[$i][6]);
}

function verifyyouraddress()
{
	_assertVisible(_heading1("VERIFY YOUR ADDRESS"));
    _check(_radio("originalAddress"));	
    $Address=_getText(_div("/address-verify-address/"),_under(_label("Original address")));
    _click(_submit("dwfrm_customeraddress_edit"));
}

function LeftNavigation()
{
	 if (mobile.iPad() || mobile.iPhone() || mobile.Android()) 
	 {
	  //click on logo
	  _click($mobilelogo);
	  _assertVisible(_image("/mobile_logo/", _in(_heading1("primary-logo hide-desktop"))));
	  //click on hamburger icon
	  _click(_heading1("visually-hidden navigation-header", _in(_div("header"))));
	 }
	 if(mobile.device()=="desktop" || mobile.AndroidTab())
     {
	//click on logo
	_click(_image("/(.*)/", _in(_heading1("primary-logo hide-mobile"))));
	//click on my account
	_click(_link($gen[2][1], _in(_div("navigation-bar hide-mobile"))));
	//my account page.
	_assertVisible(_heading1($gen[2][1]));
     }
	//click on leftnavigation link in order history page.
	   for($i=0;$i<4;$i++)
	  	{
		   if (mobile.iPad() || mobile.iPhone() || mobile.Android()) 
		   {
		   _doubleClick(_link($profiledata[$i][3], _in(_list("menu-category level-1 clearfix active"))));
		   //header
		   _assertVisible(_heading1($profiledata[$i][4]));
		   }
	   _click(_link($profiledata[$i][3], _in(_div("secondary-navigation"))));
	   //header
	   _assertVisible(_heading1($profiledata[$i][4]));
		   _click(_link($profiledata[$i][3], _in(_div("secondary-navigation"))));
		   //header
		   _assertVisible(_heading1($profiledata[$i][4]));
		}
}

function PaymentPageUI()
{
	_assertVisible(_div("Select or Enter Payment Details"));
	_assertVisible(_label("Select a Card"));
	_assertVisible(_select("dwfrm_billing_paymentMethods_creditCardList"));
	_assertVisible(_span("Name on Card: *"));
	_assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"));
	_assertVisible(_span("Card Type: *"));
	_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_type"));
	_assertVisible(_span("Card Number: *"));
	_assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_number"));
	_assertVisible(_span("Expiration Date*"));
	_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_month"));
	_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_year"));
	_assertVisible(_span("CVN # *"));
	_assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_cvn"));
	_assertVisible(_link("tooltip"));
	_assertVisible(_checkbox("dwfrm_billing_paymentMethods_creditCard_saveCard"));
	_assertVisible(_label("Save this card"));	
}

function BillingPageUI()
{
	 	_assertVisible(_div("Select or Enter Credit Card Billing Address"));
	    _assertVisible(_checkbox("sameasshippingadd"));
	    _assertVisible(_label("Same as Shipping Address"));
	    _assertVisible(_label("Select an Address *"));
	    _assertVisible(_select("dwfrm_billing_addressList"));
	    _assertVisible(_span("First Name *"));
	    _assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"));
	    _assertVisible(_span("Last Name *"));
	    _assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"));
	    _assertVisible(_span("Address Line 1 *"));
	    _assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_address1"));
	    _assertVisible(_span("Address Line 2"));
	    _assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_address2"));
	    _assertVisible(_span("City *"));
	    _assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_city"));
	    _assertVisible(_span("State/Province *"));
	    _assertVisible(_label("Phone Number *"));
	    _assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_phone"));
	    _assertVisible(_submit("dwfrm_billing_save"));

}

function ShippingUI()
{
	_setSelected(_select("dwfrm_singleshipping_addressList"), "Select from Saved Addresses");
	_assertVisible(_label("Select an Address*"));
	_assertVisible(_select("dwfrm_singleshipping_addressList"));
	_assertVisible(_span("First Name *"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"));
	_assertVisible(_span("Last Name *"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"));
	_assertVisible(_span("Address Line 1 *"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"));
	_assertVisible(_span("Address Line 2"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"));
	_assertVisible(_span("City *"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"));
	_assertVisible(_span("State/Province *"));
	_assertVisible(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"));
	_assertVisible(_span("Zip/Postal Code *"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_zip"));
	_assertVisible(_span("Phone Number *"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"));
	_assertVisible(_submit("dwfrm_singleshipping_shippingAddress_save"));
}

function AddManager()
{
	_setValue(_textbox("dwfrm_childprofile_customer_firstname"),$userData[$user][1]);
	_setValue(_textbox("dwfrm_childprofile_customer_lastname"),$userData[$user][2]);
	_setValue(_textbox("dwfrm_childprofile_customer_email"),$userData[$user][3]);
	_setValue(_textbox("dwfrm_childprofile_customer_emailconfirm"),$userData[$user][4]);
	if(_isVisible(_checkbox("/dwfrm_childprofile_addressbook_addresses/", _in(_div("account-multiselect")))))
		{
		_check(_checkbox("true", _in(_div("account-multiselect"))));
		}
	_click(_submit("manage-users-save primaryButtn"));

}

function EditManager($validationedit,$i)
{
	     _setValue(_textbox("dwfrm_childprofile_customer_firstname"),$Edit[$i][1]);
		_setValue(_textbox("dwfrm_childprofile_customer_lastname"),$Edit[$i][2]);
		_setValue(_textbox("dwfrm_childprofile_customer_email"),$Edit[$i][3]);
		_setValue(_textbox("dwfrm_childprofile_customer_emailconfirm"),$Edit[$i][4]);	
		if(_isVisible(_checkbox("/dwfrm_childprofile_addressbook_addresses/", _in(_div("account-multiselect")))))
		{
		_check(_checkbox("true", _in(_div("account-multiselect"))));
		}
}

 
function DeleteManager()
 {
        if (isMobile() && !mobile.AndroidTab())
        {
        _click(_link("/(.*)/",_in(_div("hamburger_icon hide-mobile"))));
        }
        _click(_link("My Account",_in(_div("/navigation-bar/"))));
        _click(_link("Manage Users", _in(_div("secondary-navigation"))));
        _wait(4000,_isVisible(_list("address-list")));
        //deleting the existing addresses
        var $TotAddress=_count("_link","/Delete User/");
           for(var $i=0; $i<$TotAddress; $i++)
               {
                 _click(_link("Delete User"));
                 
                 _click(_submit("primaryButtn[+$i+]"));
    
                 _wait(2000);
               }
           _assertNotVisible(_list("address-list"));

 }

//####################### Paypal related Function #########################

function paypal($paypalUN,$paypaalPwd)
{
	
	if (_isVisible(_div("PayPal Checkout", _in(_div("sliding-area")))))
		{
		
		if (_assertVisible($PAYPAL_PAGE_CONTINUE_BUTTON)){
			_click($PAYPAL_PAGE_CONTINUE_BUTTON);
			}
		else{
			_setValue($PAYPAL_EMAIL_TEXTBOX, $PayPal_Data[0][0]);
			_setValue($PAYPAL_PASSWORD_TEXTBOX,$PayPal_Data[1][0]);
			_click($PAYPAL_LOGIN_BUTTON);
			//click on continue
			_click($PAYPAL_PAGE_CONTINUE_BUTTON);
		}
		
		}
	
	else

		{
	try
	{
		_domain("www.sandbox.paypal.com")._getText(_link("Not you?"));
		_selectDomain("www.sandbox.paypal.com");
		//click on continue
		_click($PAYPAL_PAGE_CONTINUE_BUTTON);
		_selectDomain();
	}
	catch($e)
	{
		_selectWindow("/xcomponent__ppcheckout__4/");
		if(_isVisible($PAYPAL_EMAIL_TEXTBOX))
			{
			//paypal logo
			_assertVisible($PAYPAL_LOGO);
			//heading pay with paypal
			_assertVisible($PAYPAL_HEADING);	
			_setValue($PAYPAL_EMAIL_TEXTBOX, $PayPal_Data[0][0]);
			_setValue($PAYPAL_PASSWORD_TEXTBOX,$PayPal_Data[1][0]);
			_click($PAYPAL_LOGIN_BUTTON);
			//click on continue
			_click($PAYPAL_PAGE_CONTINUE_BUTTON);
			}
		else
			{
			_click($PAYPAL_PAGE_CONTINUE_BUTTON);
			}
		
		_selectWindow();
	}
		}
	
}


