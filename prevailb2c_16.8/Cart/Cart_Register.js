_include("../util.GenericLibrary/GlobalFunctions.js");
_include("../util.GenericLibrary/BM_Configuration.js");
_resource("ExcelData_Cart.xls");

var $keyword=_readExcelFile("ExcelData_Cart.xls","Search");
var $Generic=_readExcelFile("ExcelData_Cart.xls","Generic");
var $coupon_data=_readExcelFile("ExcelData_Cart.xls","Coupon");

SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);


var $fname=$FName.toLowerCase();
var $lname=$LName;

//Login to the Application
_click(_link("Login")); 
 login();
 
 var $t = _testcase("125256","Verify the UI of cart page with Register user.");
 $t.start();
 try
 { 	
 	//Adding Item to the cart
 		addItemToCart($keyword[0][0],1);
 		 _click(_link("mini-cart-link"));  	
 		 //cart navigation verification
 		 _assertVisible(_table("cart-table"));
 		 _assertVisible(_div("cart-actions cart-actions-top"));
 		//Verifying Bread crumb
 		 _assertNotVisible(_div("breadcrumb"));
 		
 		//Static text
 		_assertNotVisible(_heading1($Generic[0][1]));
 		var $TotCartItems=_count("_row","/cart-row/",_in(_div("primary")));
 		//Verifying all Details of line items in cart page
 		for(var $i=0; $i<$TotCartItems; $i++)
 			{			
 				_assertVisible(_row("cart-row["+$i+"]"));
 				_assertVisible(_div("name",_in(_row("cart-row["+$i+"]"))));
 				_assertVisible(_cell("item-image",_in(_row("cart-row["+$i+"]"))));
 				_assertVisible(_div("product-list-item",_in(_row("cart-row["+$i+"]"))));
 				_assertVisible(_span("Remove",_in(_row("cart-row["+$i+"]"))));
 				_assertVisible(_link("add-to-wishlist",_in((_row("cart-row["+$i+"]")))));
 				_assertNotVisible(_link("add-to-registry",_in(_row("cart-row["+$i+"]"))));
 				_assertVisible(_div("item-edit-details",_in(_row("cart-row["+$i+"]"))));
 				_assertVisible(_span("price-sales",_in(_row("cart-row["+$i+"]"))));
 				_assertVisible(_span("price-total",	_in(_row("cart-row["+$i+"]"))));
 			}
 		//LineItems
 		if(_isVisible(_row("cart-row")))
 			{
 			_assertVisible(_div("product-list-item", _in(_row("cart-row"))));
 			}
 		else
 			{
 			_log("Cart is Empty");
 			}		
 		//Coupon code section
 		_assertVisible(_div("cart-coupon-code"));
 		//Text box and Apply button
 		_assertVisible(_textbox("dwfrm_cart_couponCode"));
 		_assertVisible(_submit("add-coupon"));
 		//Order total section
 		_assertVisible(_div("cart-order-totals"));

 		//Continue shopping Button
 		_assertVisible(_submit("dwfrm_cart_continueShopping"));
 		//Checkout Button
 		_assertVisible(_submit("dwfrm_cart_checkoutCart"));
 		//Update cart Button
 		_assertVisible(_submit("update-cart"));
 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end();
 
 ClearCartItems();

 var $t=_testcase("125264/145062","Verify the navigation on click of 'Checkout' button from the cart page  in the applicationas a registered user");
 $t.start();
 try
 {
	//Searching & adding respective product to the cart to apply the coupon
	 search($coupon_data[5][0]);
	 _click(_submit("add-to-cart"));
 	//Navigating to the cart
 	 _click(_link("mini-cart-link"));  	
 	//click on checkout button
 	_click(_submit("dwfrm_cart_checkoutCart"));
 	if($CheckoutType==$BMConfig[0][1])
 		{
 			//Should Nav to Shipping page
 			_assertVisible(_heading2($Generic[3][1]));
 			_assertVisible(_div("checkout-tabs spc-shipping"));
 		}
 	else if($CheckoutType==$BMConfig[1][1])
 		{
 			//verify the navigation
 			_assertVisible(_fieldset("/"+$Generic[5][1]+" /"));
 			_assertVisible(_submit("dwfrm_singleshipping_shippingAddress_save"));
 		}
 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end();


 var $t=_testcase("125283/125285","Verify whether registered user can add item to Wish list from cart page, in the application and Verify the display of message in line item after adding item to wishlist, in the application both as an anonymous and a registered user.");
 $t.start();
 try
 {
 	//Navigating to the cart
 	 _click(_link("mini-cart-link"));  	
 	//capturing product name
 	var $prodId=_getText(_span("value", _in(_div("sku",_in(_row("cart-row"))))))
 	//Click on add to wish list in cart page
 	_click(_link("add-to-wishlist",_in(_row("cart-row"))));
 	//Verifying Success message
 	_assertVisible(_div("in-wishlist",_in(_row("cart-row"))));
 	_assertEqual($keyword[0][1], _getText(_div("in-wishlist",_in(_row("cart-row")))));
 	_click(_link("user-account"));
 	_click(_link("Wish List"));
 	//Verifying respective product is Presented in the wish list or not
 	_assertVisible(_span($prodId));	
 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end();


 if(!isMobile() || mobile.iPad())
 {
 var $t=_testcase("124786","Verify the display of Right section on cart page  in the application. as a registered user user.");
 $t.start();
 try
 {
 	ClearCartItems();
 	var $a=new Array();	
 	//Searching & Adding Multiple items to the cart because to get recently viewed products
 	search($coupon_data[0][0]);
 	$a.push(_getText(_heading1("product-name")));
 	_click(_submit("add-to-cart"));
 	search($coupon_data[1][0]);
 	$a.push(_getText(_heading1("product-name")));
 	$a.sort();
 	_click(_submit("add-to-cart"));
 	//Navigating to the cart
 	 _click(_link("mini-cart-link"));  	
 	var $b=new Array();
 	//Collecting product names from Last Visited section
 	$b=_collectAttributes("_div","/product-name/","sahiText", _in(_div("product-listing last-visited")));
 	$b.sort();	
 	//Comparing Recently Viewed products
 	_assertVisible(_div("product-listing last-visited"));
 	for(var $i=0; $i<$a.length; $i++)
 		{
 		if($b.indexOf($a[$i]>-1))
 			{
 			_assert(true);
 			}
 		else
 			{
 			_assert(false);
 			}
 		}
 	
 	
 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end();
 }

 ClearCartItems();

 var $t=_testcase("155590","Verify whether updated varainces of a line item in cart page reflecting same in Mini Cart.");
 $t.start();
 try
 {
 	search("701644059293");
 	if(_isVisible(_link("name-link")))
 		{
 		_click(_link("name-link"));
 		}
 	//select swatch
 	selectSwatch();
 	_click(_submit("add-to-cart"));
 	//Navigating to the cart
 	 _click(_link("mini-cart-link")); 
 	 
 	//click on edit details link
 		_click(_link("Edit Details"));
 		_assertVisible(_div("QuickViewDialog"));
 		var $TotalColor=_count("_link","/swatchanchor/",_in(_list("swatches color")));
 		if($TotalColor>1)
 			{
 				var $T=$TotalColor-1;
 				_click(_link("swatchanchor["+$T+"]", _in(_list("swatches color"))));
 			}
 		//total size swatches
 		var $TotalCount=_count("_link","/swatchanchor/",_in(_list("swatches size")));
 		//Change swatches
 		var $swatchName=_getText(_link("swatchanchor["+$test+"]", _in(_list("swatches size"))));
 		var $test=$TotalCount-1;
		_click(_link("swatchanchor["+$test+"]", _in(_list("swatches size"))));
 		
 		_wait(4000);
 		var $expSize=_getText(_listItem("selected-value", _in(_list("swatches size"))));
 		var $expcolor=_getText(_listItem("selected-value", _in(_list("swatches color"))));
 		_assertEqual($swatchName, $expSize);
 		//click on update
 		_click(_submit("Update"));
 		
 		//navigate to cart page
 		//selected swatches on cart 
 		var $sizeCart=_getText(_div("/size/",_in(_div("product-list-item")))).split(" ")[1].trim();
 		var $colorCart=_getText(_div("/color/",_in(_div("product-list-item")))).split(" ")[1].trim();
 		
 		//verification on cart and mini cart
 		_assertEqual($expSize, $sizeCart);
 		_assertEqual($expcolor, $colorCart);
 		
 		_mouseOver(_link("mini-cart-link"));

 		var $sizeMiniCart=_getText(_div("/size/",_in(_div("mini-cart-product")))).split(" ")[1].trim();
 		var $colorMiniCart=_getText(_div("/color/",_in(_div("mini-cart-product")))).split(" ")[1].trim();
 		_assertEqual($expSize, $sizeMiniCart);
 		_assertEqual($expcolor, $colorMiniCart);
 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end();
 
 ClearCartItems();
 
 var $t = _testcase("125289"," Verify merge cart functionality");
 $t.start();
 try
 {
    //Adding item from Tops category
 	addItemToCart($keyword[0][0],1);
 	_click(_link("mini-cart-link"));  	
 	//Capturing product name and Number in cart page As first logged -in user
 	var $ProductName=_getText(_div("name", _in(_row("cart-row"))));
 	var $itemNumberCart=_extract(_getText(_div("sku",_in(_row("cart-row")))), "/Item No.: (.*)/", true).toString();
 	//logout from the application
 	logout();
 	//adding item as an guest user
 	 //Adding item from Suites category
 	addItemToCart($keyword[1][0],1);
 	_click(_link("mini-cart-link"));  	
 	//click on Checkout
 	_click(_submit("dwfrm_cart_checkoutCart"));
 	//Log-in at intermediate login page
 	 login();
 	//Should Navigate to shipping page
 	 if($CheckoutType==$BMConfig[0][1])
 		{
 			//Should Nav to Shipping page
 			_assertVisible(_heading2($Generic[3][1]));
 			_assertVisible(_div("checkout-tabs spc-shipping"));
 		}
 	else if($CheckoutType==$BMConfig[1][1])
 		{
 			//verify the navigation
 			_assertVisible(_fieldset("/"+$Generic[5][1]+" /"));
 			_assertVisible(_submit("dwfrm_singleshipping_shippingAddress_save"));
 		}
 	//Nav to cart page
 	 _click(_link("mini-cart-link"));  	
 	var $TotCartItems=_count("_row","/cart-row/",_in(_div("primary")));
 	//Previously added element should be vanished
 	_assertNotVisible(_link($ProductName));
 	for(var $i=0; $i<$TotCartItems; $i++)
 		{
 			_assertNotEqual($ProductName,_getText(_div("name", _in(_row("cart-row["+$i+"]")))));
 			var $itemNumberCart2=  _extract(_getText(_div("sku",_in(_row("cart-row["+$i+"]")))), "/Item No.: (.*)/", true).toString();
 			_assertNotEqual($itemNumberCart,$itemNumberCart2);
 		} 	
 }
 catch($e)
 {
 	_logExceptionAsFailure($e);
 }
 $t.end();
 
 ClearCartItems();
 
 var $t = _testcase("125290","Verify the persistent cart functionality for logged in user");
 $t.start();
 try
 {
	 //Adding item from Tops category
 	 addItemToCart($keyword[0][0],1);
 	 _click(_link("mini-cart-link"));  			
 	//Capturing product name and Number in cart page As first logged -in user
 	var $PrevProductNames=new Array();
 	$PrevProductNames = _collectAttributes("_div","/name/","sahiText",_in(_table("cart-table")));
 	var $PrevProdNumbers=new Array();
 	$PrevProdNumbers=_collectAttributes("_div","/sku/","sahiText",_in(_table("cart-table")));
 	//logout from the application
 	logout();		
 	_click(_link("Login")); 
 	 login();
 	//Navigate to cart page
 	 _click(_link("mini-cart-link"));  			 
  	 var $AfterProductNames=new Array();
  	 $AfterProductNames = _collectAttributes("_div","/name/","sahiText",_in(_table("cart-table")));
 	 var $AfterProdNumbers=new Array();
 	 $AfterProdNumbers=_collectAttributes("_div","/sku/","sahiText",_in(_table("cart-table")));			 
 	 //Previously added items should persist in the cart
 	 _assertEqualArrays($PrevProductNames,$AfterProductNames);
 	 _assertEqualArrays($PrevProdNumbers,$AfterProdNumbers);
 }
 catch($e)
 {
 	  _logExceptionAsFailure($e);
 }
 	
 $t.end();
 
 
 cleanup();