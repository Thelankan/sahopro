_include("../util.GenericLibrary/GlobalFunctions.js");
_resource("ExcelData_Cart.xls");

var $coupon_data=_readExcelFile("ExcelData_Cart.xls","Coupon");
var $subcat=_readExcelFile("ExcelData_Cart.xls","Search");	

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();
	

var $t = _testcase("125267","Verify whether user can apply the valid coupon code in the cart page");
$t.start();
try
{
	//Searching & adding respective product to the cart to apply the coupon
	search($coupon_data[5][0]);
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	_click(_link("mini-cart-link"));	
	//Applying Coupon code
	_setValue(_textbox("dwfrm_cart_couponCode"),$coupon_data[5][1]);
	_click(_submit("add-coupon"));	
	//Verifying When valid coupon applied to the cart
	_assertVisible(_row("rowcoupons"));
	//Verifying the coupon code is expected or not
	_assertEqual($coupon_data[5][1],_getText(_span("value", _in(_div("cartcoupon clearfix")))));
	//Verifying "Applied" message
	_assertEqual($coupon_data[5][2], _getText(_span("bonus-item", _near(_div("cartcoupon clearfix")))));
	//parameter 10 is Discount for product coupon 
	//Verifying order Total by calculating each line item total
	orderSummaryValidation(10);//Referring function from "Total_Calculate.js"
	//Calculating Estimated Total 10 is the order discount
	CalculateEstiTotal(0,10);//Referring function from "Total_Calculate.js"
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

ClearCartItems();

var $t = _testcase("125269","Verify whether sub total / Order Total/ Discount/ Shipping gets updated after applying the coupon code.");
$t.start();
try
{
	search($coupon_data[5][0]);
	//select swatches
	selectSwatch();
	//set the quantity
	_setValue(_textbox("Quantity"),1);
	//Click on add to cart button
	_click(_submit("add-to-cart"));
	_click(_link("mini-cart-link"));  
	//Applying Coupon
	_setValue(_textbox("dwfrm_cart_couponCode"),$coupon_data[5][1]);
	_click(_submit("add-coupon"));
	//Coupon should be applied and Order sub total and estimated sub total should be updated
	_assertVisible(_span("Applied", _in(_row("rowcoupons"))));
	//Function to calculate Estimated Total (10 is the Coupon discount)
	CalculateEstiTotal(0,10);//Referring function from "Total_Calculate.js" 
	//Removing coupon
	_click(_submit("Remove", _in(_row("rowcoupons"))));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}		
$t.end();

var $t = _testcase("125277","Verify whether user can remove the line item from cart");
$t.start(); 
try
{
	//Adding Another Item to the cart
	addItemToCart($subcat[0][0],1);
	_click(_link("mini-cart-link"));  
	 var $EstiTotalBefore=_extract(_getText(_row("order-total")),"/[$](.*)/",true);
     var $ProdName=_getText(_div("name",_in(_row("cart-row"))));  
     //Click on Remove
	_click(_submit("Remove", _in(_row("cart-row"))));
	//After removing Respective product,Product name should not Visible
	_assertNotVisible($ProdName);
	//Estimated Total should Change
	_assertNotEqual($EstiTotalBefore,_extract(_getText(_row("order-total")),"/[$](.*)/",true));	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}		
$t.end();

ClearCartItems();

var $t=_testcase("125275","Verify by providing valid coupon code and hit enter key.");
$t.start();
try
{
	//Searching & adding respective product to the cart to apply the coupon
	search($coupon_data[0][0]);
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	_click(_link("mini-cart-link"));  
	//Applying Coupon code
	_setValue(_textbox("dwfrm_cart_couponCode"),$coupon_data[0][1]);
	_focusWindow();
	//Statement to hit on enter key
	_focus(_textbox("dwfrm_cart_couponCode"));
	//_keyPress(document.body, [13,13]);
	 _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	//Verifying coupon row is visible or not
	_assertVisible(_row("rowcoupons"));
	//Verifying the coupon code is expected or not
	_assertEqual($coupon_data[0][1],_getText(_span("value", _in(_div("cartcoupon clearfix")))));
	//Verifying "Applied" message
	_assertEqual($coupon_data[0][2], _getText(_span("bonus-item", _near(_div("cartcoupon clearfix")))));	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

ClearCartItems();
 	
var $t=_testcase("125279","Verify user can update the quantity of each line item in the application both as an anonymous and a registered user");
$t.start();
try
{
	search($coupon_data[0][0]);
	_click(_submit("add-to-cart"));	
	//Navigating to the cart
	_click(_link("mini-cart-link"));  
	var $ValidQty=4;
	_click(_link("mini-cart-link"));  
	//verifying by entering valid Quantity
	_setValue(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity"),$ValidQty);
	//Click on enter key from keyboard
	//Statements to hit on enter key
	_focusWindow();
	_focus(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity"));
	_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	//Verifying Qty
	_assertEqual($ValidQty,_getText(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity")));	
	//verifying by entering 0 Quantity
	var $prodID=_getText(_span("value", _in(_div("sku",_in(_row("cart-row"))))));
	_setValue(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity", _in(_row("cart-row"))),0);
	//Statements to hit on enter key
	_focusWindow();
	_focus(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity",_in(_row("cart-row"))));
	_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	_wait(2000);
	//Product Should not present
	_assertNotVisible(_span($prodID));	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

 	
 	
