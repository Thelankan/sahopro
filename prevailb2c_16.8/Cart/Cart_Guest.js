_include("../util.GenericLibrary/GlobalFunctions.js");
_include("../util.GenericLibrary/BM_Configuration.js");
_resource("ExcelData_Cart.xls");

var $keyword=_readExcelFile("ExcelData_Cart.xls","Search");
var $Generic=_readExcelFile("ExcelData_Cart.xls","Generic");
var $coupon_data=_readExcelFile("ExcelData_Cart.xls","Coupon");
var $cart_data=_readExcelFile("ExcelData_Cart.xls","Quantity");

SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);



var $t = _testcase("125257/125261/148617/148618","Verify the UI of cart page with anonymous user and Verify the display of product image in the cart page in the application both as an anonymous and a registered user and Verify the UI of Cart when products are added to bag  in the application both as an anonymous and a registered user and Verify the UI of Line Items displayed in Cart  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	//Adding Item to the cart
	addItemToCart($keyword[0][0],1);
	 _click(_link("mini-cart-link"));  	
	 //cart navigation verification
	 _assertVisible(_table("cart-table"));
	 _assertVisible(_div("cart-actions cart-actions-top"));
	//Verifying Bread crumb
	 _assertNotVisible(_div("breadcrumb"));
	
	//Static text
	_assertNotVisible(_heading1($Generic[0][1]));
	var $TotCartItems=_count("_row","/cart-row/",_in(_div("primary")));
	//Verifying all Details of line items in cart page
	if(!isMobile() || mobile.iPad())
		{
			//table Heading
			_assertVisible(_tableHeader("PRODUCT"));
			_assertVisible(_tableHeader("QTY"));
			_assertVisible(_tableHeader("PRICE"));
			_assertVisible(_tableHeader("TOTAL"));	
		}
	_assertVisible(_cell("item-image"));
	_assertVisible(_cell("item-quantity"));
	_assertVisible(_cell("item-price"));
	_assertVisible(_cell("item-total"));
	for(var $i=0; $i<$TotCartItems; $i++)
		{		
			_assertVisible(_row("cart-row["+$i+"]"));
			_assertVisible(_div("name",_in(_row("cart-row["+$i+"]"))));
			//System should display the respective product image for each item. 
			_assertVisible(_cell("item-image",_in(_row("cart-row["+$i+"]"))));
			_assertVisible(_div("product-list-item",_in(_row("cart-row["+$i+"]"))));
			_assertVisible(_span("Remove",_in(_row("cart-row["+$i+"]"))));
			_assertVisible(_link("add-to-wishlist",_in((_row("cart-row["+$i+"]")))));
			_assertNotVisible(_link("add-to-registry",_in(_row("cart-row["+$i+"]"))));
			_assertVisible(_div("item-edit-details",_in(_row("cart-row["+$i+"]"))));
			_assertVisible(_span("price-sales",_in(_row("cart-row["+$i+"]"))));
			_assertVisible(_span("price-total",	_in(_row("cart-row["+$i+"]"))));
		}
	//LineItems
	if(_isVisible(_row("cart-row")))
		{
		_assertVisible(_div("product-list-item", _in(_row("cart-row"))));
		}
	else
		{
		_log("Cart is Empty");
		}	
	//Coupon code section
	_assertVisible(_div("cart-coupon-code"));
	//Text box and Apply button
	_assertVisible(_textbox("dwfrm_cart_couponCode"));
	_assertVisible(_submit("add-coupon"));
	//Order total section
	_assertVisible(_div("cart-order-totals"));

	//Continue shopping Button
	_assertVisible(_submit("dwfrm_cart_continueShopping"));
	//Checkout Button
	_assertVisible(_submit("dwfrm_cart_checkoutCart"));
	//Update cart Button
	_assertVisible(_submit("update-cart"));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t = _testcase("125278","Verify the UI of empty cart page.");
$t.start();
try
{
	//Removing Cart Items
	ClearCartItems();
	
	//continue shopping link
	_assertVisible(_submit("dwfrm_cart_continueShopping"));
	//Reccommenadtion section
	_assertVisible(_div("/product-listing/"));
	//Verifying "Your Shopping Cart is Empty" message
	_assertVisible(_heading1($cart_data[0][1]));
	//Verifying Bread crumb
	 _assertNotVisible(_div("breadcrumb"));			
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("125258" ,"Verify user can edit details in line item in cart page");
$t.start();
try
{
	//Add item to the cart
	addItemToCart($keyword[0][0],1);
	_click(_link("mini-cart-link"));  
	//Capturing line item details
	var $EstiTotalBefore=_extract(_getText(_row("order-total")),"/[$](.*)/",true);
	//To click on the the edit details link seen in the cart page of the first line item 
    _click(_link("Edit Details"));
    //Quick view dialog should visible
    _assertVisible(_div("/ui-dialog ui-widget ui-widget-content ui-corner-all/"));
	//select swatches
    selectSwatch();
	//set the quantity
	_setValue(_textbox("Quantity"),4);
	//Update
	_click(_submit("add-to-cart"));
	//After updating Product Details
	_assertNotEqual($EstiTotalBefore,_extract(_getText(_row("order-total")),"/[$](.*)/",true));	
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}	               
$t.end();


//Removing Cart Items
ClearCartItems();
var $t = _testcase("125263"," Verify the functionality of continue shopping link when user navigates to cart page from various pages.");
$t.start();
try
{
	//Adding Item to the cart
	search($keyword[0][2]);
	_click(_submit("add-to-cart"));
	_click(_link("mini-cart-link"));  
	//Capturing Recently Visited product
	var $RecenProdName=_getText(_div("name", _in(_div("product-list-item"))));
	//To Click on continue shopping link
	_click(_submit("dwfrm_cart_continueShopping"));	
	//Verifying whether its navigated to Pd-Page or not
	_assertVisible(_div("pdp-main"));
	_assertEqual($RecenProdName, _getText(_heading1("product-name")));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();



var $t = _testcase("125277","Verify whether user can remove the line item from cart");
$t.start(); 
try
{
	//Adding Another Item to the cart
	addItemToCart($keyword[0][0],1);	
	_click(_link("mini-cart-link"));  
	 var $EstiTotalBefore=_extract(_getText(_row("order-total")),"/[$](.*)/",true);
     var $ProdName=_getText(_div("name",_in(_row("cart-row"))));  
     //Click on Remove
	_click(_submit("Remove", _in(_row("cart-row"))));
	//After removing Respective product,Product name should not Visible
	_assertNotVisible($ProdName);
	//Estimated Total should Change
	_assertNotEqual($EstiTotalBefore,_extract(_getText(_row("order-total")),"/[$](.*)/",true));	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}		
$t.end();

ClearCartItems();

var $t=_testcase("125262","Verify the display of product name in the cart page in the application both as an anonymous and a registered user.");
$t.start();
try
{
	search($keyword[0][0]);
	_click(_link("name-link"));
	//Capture Product name at PD page
	var $ProdNameAtPDP=_getText(_heading1("product-name"));
	//Select Product Variances
	//select swatches
	selectSwatch();
	//click on add to cart And Nav to cart page
	_click(_submit("add-to-cart"));
	 _click(_link("mini-cart-link"));  		
	//Comparing Product name at Cart page
	_assertEqual($ProdNameAtPDP, _getText(_div("name")));	
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("125282","Verify the display of the Line item total as a each line item in the application both as an anonymous and a registered user");  																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																											
$t.start();
try
{	
	//calculating the line item total price as: product price * Qty = Line item price  
	orderSummaryValidation(0);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("125265/148623","Verify the navigation on click of 'Checkout' button from the cart page  in the application as a anonymous user and");
$t.start();
try
{
	//click on checkout button
	_click(_submit("dwfrm_cart_checkoutCart"));
	//Should Nav to ILP page
	_assertVisible(_div("login-box-content returning-customers clearfix"));
	_assertVisible(_div("login-box login-account"));
	_assertVisible(_div("login-box"));
	_assertVisible(_submit("dwfrm_login_unregistered",_in(_div("login-box"))));
	_assertVisible(_submit("dwfrm_login_register",_in(_div("login-box"))));
	//nav to Home page
	_click(_image("Salesforce Commerce Cloud SiteGenesis"));	

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("125284","Verify whether anonymous user can add item to Wish list from cart page in the application.");
$t.start();
try
{
	 _click(_link("mini-cart-link"));  	
	//Click on add to wish list in cart page
	_click(_link("add-to-wishlist"));
	//Should Nav to Login page
	_assertVisible(_div("login-box-content returning-customers clearfix"));
	_assertVisible(_div("login-box login-account"));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("125279","Verify user can update the quantity of each line item in the application both as an anonymous and a registered user");
$t.start();
try
{
	var $ValidQty=4;
		_click(_link("mini-cart-link"));  	
		//verifying by entering valid Quantity
		_setValue(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity"),$ValidQty);
		//Click on enter key from keyboard
		//Statements to hit on enter key
		_focusWindow();
		_focus(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity"));
		_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
		//Verifying Qty
		_assertEqual($ValidQty,_getText(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity")));
		
		//verifying by entering 0 Quantity
		var $prodID=_getText(_span("value", _in(_div("sku",_in(_row("cart-row"))))));
		_setValue(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity", _in(_row("cart-row"))),0);
		//Statements to hit on enter key
		_focusWindow();
		_focus(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity",_in(_row("cart-row"))));
		_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
		_wait(5000);
		//Product Should not present
		_assertNotVisible(_span($prodID));
			
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();
ClearCartItems();

var $t=_testcase("125287","Verify the display of Featured Products section in cart page in the application both as an anonymous and a registered user.");
$t.start();
try
{
	//Navigating to Cart page
	search($keyword[0][2]);
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	 _click(_link("mini-cart-link"));  		
	//Heading
	_assertVisible(_heading2("Featured Products"));
	var $TotFeaturedProds=_count("_div","/product-image/", _in(_div("vertical-carousel")));
	for(var $i=0; $i<$TotFeaturedProds; $i++)
		{
			//Product name
			_assertVisible(_div("product-name", _in(_div("product-tile["+$i+"]"))))
			//Product image
			_assertVisible(_image("/(.*)/", _in(_div("product-tile["+$i+"]"))))
			//Product Price
			_assertVisible(_div("product-pricing", _in(_div("product-tile["+$i+"]"))));
		}	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("125288","Verify the response when user click on either product image or product name in Featured Products section in cart page");
$t.start();
try
{
	//To fetch the product name from the featured product section
	var $featuredProd= _getText(_link("name-link", _in(_div("product-tile"))));	
	//Click on Feature Product
	_click(_link("name-link", _in(_div("product-tile"))));	
	//Comparing Prod-name At Pd Page
	_assertEqual($featuredProd,_getText(_heading1("product-name")));
	_assertVisible(_div("pdp-main"));
}
catch($e)
{
 _logExceptionAsFailure($e);
}		
$t.end();


ClearCartItems();

var $t=_testcase("148620","Verify the UI of Cart when bundled product is added to Bag in the application both as an anonymous and a registered user");
$t.start();
try
{
	//Nav to Cart page
	search($keyword[0][3]);
	_click(_submit("add-to-cart"));
	 _click(_link("mini-cart-link"));  	
	if(!isMobile() || mobile.iPad())
		{
			//table Heading
			_assertVisible(_tableHeader("PRODUCT"));
			_assertVisible(_tableHeader("QTY"));
			_assertVisible(_tableHeader("PRICE"));
			_assertVisible(_tableHeader("TOTAL"));		
		}
	_assertVisible(_cell("item-image"));
	_assertVisible(_cell("item-quantity"));
	_assertVisible(_cell("item-price"));
	_assertVisible(_cell("item-total"));	
	//Verifying Bundle Products
	var $TotBundleProds=_count("_row","/rowbundle/",_in(_div("primary")));
	for(var $i=0; $i<$TotBundleProds; $i++)
		{
			//Product name
			_assertVisible(_div("name", _in(_row("rowbundle["+$i+"]"))));
			//Product number
			_assertVisible(	_div("itemnumber",_in(_row("rowbundle["+$i+"]"))));
			//image
			_assertVisible(_image("/(.*)/", _in(_row("rowbundle["+$i+"]"))));
			//Quantity
			_assertVisible(_span("bundleqtyincluded", _in(_row("rowbundle["+$i+"]"))));
			//Inventory
			_assertVisible(_div("product-availability-list", _in(_row("rowbundle["+$i+"]"))));	
		}
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

cleanup();