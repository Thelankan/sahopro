_include("util.GenericLibrary/GlobalFunctions.js");
_resource("util.GenericLibrary/User_Credentials.xls");

var $profilevalues=_readExcelFile("Sanity/Drop5_sanity/Drop5_sanity.xls","Profile");

var $payment_data=_readExcelFile("Checkout/Payment/Payment.xls","payment_data");
var $Address_data=_readExcelFile("Checkout/Payment/Payment.xls","Address_data");
var $credit_card=_readExcelFile("Checkout/Payment/Payment.xls","credit_card");
var $paymentvalidations=_readExcelFile("Checkout/Payment/Payment.xls","PaymentValidations");
var $shippingdata=_readExcelFile("Checkout/ShippingPage/ShippingPage.xls","ShippingData");

SiteURLs();

//Create account
if (isMobile())
{
 _click(_link("/mobile-show/", _in(_div("user-info"))));
}
else
	{
	 _click(_link("/desktop-show/", _in(_div("user-info"))));
	 //Visibility of overlay
	 _assertVisible(_div("dialog-container"));
	}

//click on create account button
_click(_link("/button/", _in(_div("login-box registration-btn"))));

createAccount_profile($profilevalues,0);

for(var $i=0;$i<$credit_card.length;$i++)
{
	//navigate to cart page
	navigateToCart($payment_data[1][7],$payment_data[0][8]);
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
	
	if ($i==0)
		{
		
		//shipping details
		shippingAddress($Address_data,0);
		//click on save address checkbox
		_click(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook"));
		}

					
	//Shipment date
	eventcalendar();

	//Selecting shipping method
	_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
	
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	adddressProceed();
	
	
	//Payment details
	PaymentDetails($credit_card,$i);
				
	//Placing order
	_click(_checkbox("/dwfrm_billing_confirm/"));
	_click(_submit("dwfrm_billing_save"));
	
	_assertVisible(_div("order-confirmation-details"));
	
	
	var $orderno=_getText(_span("order-number")).split(" ")[2];
	_log($orderno);
	
	var $orderkkkk=_getText(_span("order-number"))
	_log($orderkkkk);
	
	takescreenshot();
	
	//payment in fo
	_assertVisible(_div("order-payment-instruments"));
	_assertContainsText($credit_card[$i][6], _div("order-payment-instruments"));
}


function createAccount_profile($sheet,$Rowno)
{
	 _setValue(_textbox("dwfrm_profile_customer_email"),$datenowEmail);
	 var $confirmemail=_getText(_textbox("dwfrm_profile_customer_email"));
	 _log("Account is created with this emai ID"+$confirmemail);
	 _setValue(_textbox("dwfrm_profile_customer_emailconfirm"),$confirmemail);
	 _setValue(_textbox("dwfrm_profile_customer_firstname"), $sheet[$Rowno][3]);
	 _setValue(_textbox("dwfrm_profile_customer_lastname"), $sheet[$Rowno][4]);
	 _setValue(_password("/dwfrm_profile_login_password/"), $sheet[$Rowno][5]);
	 _setValue(_password("/dwfrm_profile_login_passwordconfirm/"), $sheet[$Rowno][6]);
	 //click on submit button
	 _click(_submit("dwfrm_profile_confirm"));
	 _wait(5000);
}

function shippingAddress($sheet,$i)
{
		
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), $sheet[$i][1]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), $sheet[$i][2]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"), $sheet[$i][3]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"),$sheet[$i][4]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $sheet[$i][8]);
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$sheet[$i][5]);
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), $sheet[$i][6]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), $sheet[$i][7]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),$sheet[$i][9]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email"),$uId);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail"),$uId);
}

