_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("MyAccount_Payment.xls");

var $paymentdata=_readExcelFile("MyAccount_Payment.xls","PaymentData");
var $card_data=_readExcelFile("MyAccount_Payment.xls","card_data");
var $Addresss=_readExcelFile("MyAccount_Payment.xls","Addresss");



SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


//login to the application
if (isMobile())
{
 _click(_link("/mobile-show/", _in(_div("user-info"))));
}
else
	{
	 _click(_link("/desktop-show/", _in(_div("user-info"))));
	 //Visibility of overlay
	 _assertVisible(_div("dialog-container"));
	}

login();

_wait(5000);

var $t = _testcase("219179/219181/218890/219200","Verify the UI of Account Payment Settings Landing Page/Verify the UI of User CC information displayed static text" +
		"Verify the functionality of PAYMENT SETTINGS link");
$t.start();
try
{
	
    //Navigate to account landing page & verifying the empty card
	DeleteCreditCard();
	
	//click on add new card
	_click(_submit("/add-new-paymentcard/"));
   //add card detail
   CreateCreditCard($card_data,4);
   _click(_submit("apply-button js-add-creditcard "));
  
    //verifying the newly created card
    _assertVisible(_div("account-landing-header"));
    _assertVisible(_list("payment-list"));	
	_assertVisible(_submit("Edit"));
	_assertVisible(_submit("Delete"));
	_assertVisible(_submit("Add new"));
	_assertVisible(_div("cc-type"));
	_assertVisible(_div("cc-number"));
	_assertVisible(_div("cc-exp"));
	
}

catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("219199/219212","Verify the functionality of EDIT link/Verify the UI of EDIT CREDIT CARD formfield");
$t.start();
try
{

//click on edit link	
_click(_submit("Edit"));
//payment details should be visible
_assertVisible(_div("paymentcard-form"));

//UI
_assertVisible(_textbox("/dwfrm_paymentinstruments_creditcards_newcreditcard_number/"));
_assertVisible(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_month"));
_assertVisible(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_year"));
_assertVisible(_label("Credit Card Number*"));
_assertVisible(_label("Expiration Date *"));
_assertVisible(_submit("edit-buttton js-edit-creditcard "));
_assertVisible(_link("Cancel"));

//credit card images
_assertVisible(_div("paymentcard-images"));

//click on cancel button
_click(_link("Cancel"));
_assertVisible(_div($paymentdata[2][0]));
_click(_link("Cancel"));
//payment details should not be visible
_assertNotVisible(_div("paymentcard-form"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()
//
var $t = _testcase("219202/219210/220199/219240","Verify the functionality of ADD NEW button");
$t.start();
try
{
	
//click on add new button
_click(_submit("Add new"));
//heading
_assertVisible(_heading1("NEW CREDIT CARD"));
//payment details should be visible
_assertVisible(_div("paymentcard-form"));

//UI
_assertVisible(_textbox("/dwfrm_paymentinstruments_creditcards_newcreditcard_number/"));
_assertVisible(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_month"));
_assertVisible(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_year"));
_assertVisible(_label("Credit Card Number*"));
_assertVisible(_label("Expiration Date *"));
//credit card images
_assertVisible(_div("paymentcard-images"));
//click on cancel button
_click(_link("Cancel"));
_assertVisible(_div($paymentdata[2][0]));
_click(_link("Cancel"));
//payment details should not be visible
_assertNotVisible(_div("paymentcard-form"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("12345/219209","To check that payment details is pre populating in payment page");
$t.start();
try
{
	//Navigate to account landing page & verifying the empty card
	DeleteCreditCard();
	//click on add new card
	_click(_submit("/add-new-paymentcard/"));
   //create a credit card
   CreateCreditCard($card_data,4);
   _click(_submit("apply-button js-add-creditcard "));
   
   //verifying the newly created card
   _assertVisible(_div("account-landing-header"));
   _assertVisible(_list("payment-list"));

   //add product to cart
   $product=$paymentdata[0][2]
   navigateToCart($product,1);
   
   //click on checkout button on top
   _assertVisible(_link("/mini-cart-link-checkout-link/", _in(_div("header-top"))));
   _click(_link("/mini-cart-link-checkout-link/", _in(_div("header-top"))));
   
   //verifying shipping page
   _assertVisible(_div("single-ship address-fields"));

   //enter shipping details
   shippingAddress($Addresss,0);
   
   if (_isVisible(_select("dwfrm_singleshipping_addressList")))
   {
   //select address from drop down
   _setSelected(_select("dwfrm_singleshipping_addressList"),"1");
   }

   adddressProceed();
   
 //selecting shipping method
   _check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
   
   //event calendar
   eventcalendar();
   
   _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
   
   adddressProceed();
   
   //displaying and verifying payment page
   _assertVisible(_div("Payment & Place Order"));
   _assertVisible(_div("checkout-order-total CartSummaryWrap", _in(_div("secondary"))));
   _assertVisible(_div("address-details"));
   _assertVisible(_fieldset("Select Payment Method"));

   //verifying the payment details
   
   //select existing card details
   //_setSelected(_select("creditCardList"),1);
   _wait(3000);
   
   //credit card no.
   _assertEqual($card_data[0][6], _getText(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/")));
   
   //month
   _assertEqual($card_data[4][2], _getSelectedText(_select("/dwfrm_billing_paymentMethods_creditCard_expiration_month/")));
		   
	//year
   _assertEqual($card_data[4][3],_getSelectedText(_select("/dwfrm_billing_paymentMethods_creditCard_expiration_year/")));
   
   //enter cvn no and verify it
   _setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"), $card_data[4][5]);
   _assertEqual($card_data[4][5], _getText(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/")));

		 
  //click on place order button
 //check the check box
 _check(_checkbox("/dwfrm_billing_confirm/"));
 //place order button
_click(_submit("dwfrm_billing_save"));
   
   //verifying order confirmation page
   _assertVisible(_div("confirmation-message"));
   _assertVisible(_div("order-information"));
   _assertVisible(_div("order-billing"));
   _assertVisible(_div("address"));
   _assertVisible(_div("order-payment-instruments"));
   _assertVisible(_div("order-totals-table box"));
   
   var $orderno=_getText(_span("order-number")).split(" ")[2];
   _log($orderno);

   var $orderkkkk=_getText(_span("order-number"))
   _log($orderkkkk);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("123456","verify saved card details in payment page is pre populating in payment setting page");
$t.start();
try
{

//	Navigate to account landing page & verifying the empty card
	DeleteCreditCard();
   
 //add product to cart
   $product=$paymentdata[0][2]
   navigateToCart($product,1);
   
   //click on checkout button on top
   _assertVisible(_link("/mini-cart-link-checkout-link/", _in(_div("header-top"))));
   _click(_link("/mini-cart-link-checkout-link/", _in(_div("header-top"))));
   
   //verifying shipping page
   _assertVisible(_div("single-ship address-fields"));

   //enter shipping details
   shippingAddress($Addresss,0)
   
   if (_isVisible(_select("dwfrm_singleshipping_addressList")))
   {
   //select address from drop down
   _setSelected(_select("dwfrm_singleshipping_addressList"),"1");
   }

   adddressProceed();
   
   //selecting shipping method
   _check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
   
   //event calendar
   eventcalendar();
   
   _click(_submit("dwfrm_singleshipping_shippingAddress_save"));

   adddressProceed();
   
   //displaying and verifying payment page
   _assertVisible(_div("Payment & Place Order"));
   _assertVisible(_div("checkout-order-total CartSummaryWrap", _in(_div("secondary"))));
   _assertVisible(_div("address-details"));
   _assertVisible(_fieldset("Select Payment Method"));
   
   //enter card details
   PaymentDetails($paymentdata,5);
   
   //check the check box for save card
   _assertVisible(_checkbox("input-checkbox "));
   _check(_checkbox("input-checkbox "))
   
 //check the other check box 
   _check(_checkbox("/dwfrm_billing_confirm/"));
   
   //click on place order button
   _check(_checkbox("/dwfrm_billing_confirm/"));
   //place order button
   _click(_submit("dwfrm_billing_save"));
   
   //verifying order confirmation page
   _assertVisible(_div("confirmation-message"));
   _assertVisible(_div("order-information"));
   _assertVisible(_div("order-billing"));
   _assertVisible(_div("address"));
   _assertVisible(_div("order-payment-instruments"));
   _assertVisible(_div("order-totals-table box"));
   
   var $orderno=_getText(_span("order-number")).split(" ")[2];
   _log($orderno);

   var $orderkkkk=_getText(_span("order-number"))
   _log($orderkkkk);
   
   //Navigate to account landing page
	NavigatetoAccountLanding();
	//navigate to payment setting page
  _click(_link("PAYMENT SETTINGS"));
  
  //verifying payment setting page
    _assertVisible(_link("Payment Settings", _in(_div("breadcrumb"))));
    _assertVisible(_list("payment-list"));
	_assertVisible(_heading1("Payment Settings"))
	_assertVisible(_submit("Edit"));
	_assertVisible(_submit("Delete"));
	_assertVisible(_submit("Add new"));
	_assertVisible(_div("cc-type"));
	_assertVisible(_div("cc-number"));
	_assertVisible(_div("cc-exp"));

  
  //verifying the card  details
 //credit card no.
  _assertEqual($paymentdata[5][7], _getText(_div("cc-number")));
  
 //month and year
  _assertEqual($paymentdata[5][8], _getText(_div("cc-exp")));
	   
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

//########################### Related to make a payment ####################


var $t = _testcase("220205/220206/220207","Verify the Functionality of 'Breadcrumb' in the Submit Payment Page for Desktop/ipad view /Verify the Navigation of 'Breadcrumb' in the Submit Payment Page for Desktop/ipad view" +
		"Verify the Functionality of 'Account no' in the Submit Payment Page for Desktop/ipad view");
$t.start();
try
{
	
//Navigate to account landing page
NavigatetoAccountLanding();
//click on make a payment link
_click(_link($paymentdata[1][0]));

//check the account number
_assertVisible(_heading2("/Account:/"));

//verify the bread crumb
_assertVisible(_link($paymentdata[0][1], _in(_div("breadcrumb"))));
_assertVisible(_link($paymentdata[1][1], _in(_div("breadcrumb"))));
_assertVisible(_link($paymentdata[2][1], _in(_div("breadcrumb"))));

//verifying the links
_assertVisible(_link($paymentdata[2][1], _in(_div("breadcrumb"))));
//should navigate no where
_assertVisible(_heading1("SUBMIT PAYMENT"));
//click on my account in bread crumb
_click(_link("/My Account/"));
_assertVisible(_link($paymentdata[1][1], _in(_div("breadcrumb"))));
//should navigate to my account landing page
_assertVisible(_heading1("OPTIONS"));
_assertVisible(_div("account-page"));
//click on home link in bread crumb
_click(_link($paymentdata[0][1], _in(_div("breadcrumb"))));
//verify the home page
_assertNotVisible(_div("breadcrumb"));
_assertVisible(_div("home-main-right"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()
