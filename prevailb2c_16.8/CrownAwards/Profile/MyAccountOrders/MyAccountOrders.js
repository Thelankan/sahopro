_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("MyAccountOrders.xls");

var $orderdata=_readExcelFile("MyAccountOrders.xls","Data");
var $Address_Order=_readExcelFile("MyAccountOrders.xls","Address_Order");
var $Card_order=_readExcelFile("MyAccountOrders.xls","Card_order");


SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("220016/220016/220045","Verify the Navigation of 'Forgot Password' Link in My Account Orders form in application for Desktop / ipad view /Verify the Navigation of 'Forgot Password' Link in My Account Orders form in application for Mobile View");
$t.start();
try
{

//click on your orders link 	
//_click(_link("Your Orders"));
	Mobile_view()
//navigate to orders page
_assertEqual($orderdata[3][1], _getText(_heading1("page-header")));
_assertVisible(_link($orderdata[3][1], _in(_div("breadcrumb"))));
//click on password link in your orders page in order history section
//_click(_link("/Forgot Password/", _in(_div("loginFieldbtn"))));
_click(_link("/Forgot Password/", _in(_div("loginFieldbtn order-track"))))

//should verify forget password page
_assertVisible(_heading1($orderdata[1][1]));
_assertVisible(_div("inner-container"));
//click on cancel button
_click(_button("Close"))
//pop up should not display
_assertNotVisible(_div("inner-container"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


//Place an order

//Navigation till shipping page
NavigateToCheckoutLogin($orderdata[0][6]);

//shipping detailss
shippingAddress($Address_Order,4);
			
//Shipment date
eventcalendar();
adddressProceed();
_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));

//Payment details
PaymentDetails($Card_order,0);

//Placing order
_click(_checkbox("/dwfrm_billing_confirm/"));
_click(_submit("dwfrm_billing_save"));

takescreenshot();

//fetch order number
var $myaccorderno=_extract(_getText(_span("order-number")),"/[#](.*)/",true);

var $orderemail=$uId;
var $zipcode=$Address_Order[0][8];	

var $t = _testcase("220108/220044","Verify the Functionality of 'Order Summary' in  My Account Order Details Page for Desktop/ipad view /Verify the Navigation of Find Order Button in My Account Orders Page for Mobile View");
$t.start();
try
{
//click on your orders button	
_click(_link("Your Orders"));
//enter order no and navigate to order detail page
//_assertVisible(_numberbox("searchorder"));
_setValue(_textbox("dwfrm_ordertrack_orderNumber"),$myaccorderno);
_setValue(_textbox("dwfrm_ordertrack_orderEmail"),$orderemail);
_setValue(_textbox("dwfrm_ordertrack_postalCode"),$zipcode);
_click(_submit("dwfrm_ordertrack_findorder"));
//should navigate to order detail page
_assert(false,"heading should verify");
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("220043","Verify the Navigation of Login Button in My Account Orders Page for Mobile View");
$t.start();
try
{
	
	//click on your orders button	
	_click(_link("Your Orders"));
	_wait(3000);

	//Login to the application
	if (isMobile())
	{
	   //click on login link
	   _click(_link("/mobile-show/", _in(_div("user-info"))));	
	}
	  else
		  {
		   //click on login link
		   _click(_link("/desktop-show/", _in(_div("user-info"))));
		   //Visibility of overlay
		   _assertVisible(_div("dialog-container"));
		  }
    
	//login to the app
	
	//login();
	_wait(3000);
	_setValue(_textbox("/dwfrm_login_username/"), $orderdata[5][0]);
	_setValue(_password("/dwfrm_login_password/"),$orderdata[5][1]);
	_click(_submit("dwfrm_login_login"));
	_wait(3000);

	_assertEqual($orderdata[0][5], _getText(_link("breadcrumb-element last", _in(_div("breadcrumb")))));
	_assertVisible(_heading1($orderdata[1][5]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()
_log("**************** Related to order history ******************");

var $t = _testcase("220146/220147","Verify the Functionality of 'Breadcrumb' in the My Account Order History Page for Desktop/ipad view / Verify the Navigation of 'Breadcrumb' in the My Account Order History Page for Desktop/ipad view");
$t.start();
try
{
	
//click on your orders button	
_click(_link("Your Orders"));
//should navigate to order detail page
_assertVisible(_heading1("ORDER HISTORY"));
//check the account no

if (!isMobile())
	{
	_assertVisible(_heading2("order-number desktop-show"));
	}
	else
		{
		_assertVisible(_heading2("order-number mobile-show"));
		}

_assertVisible(_div("breadcrumb"));
_assertVisible(_link($orderdata[0][3], _in(_div("breadcrumb"))));
_assertVisible(_link($orderdata[1][3], _in(_div("breadcrumb"))));
_assertVisible(_link($orderdata[2][3], _in(_div("breadcrumb"))));
//click on order details link in bread crumb
_click(_link($orderdata[2][3], _in(_div("breadcrumb"))));
//page should stay in the same page
_assertVisible(_heading1("ORDER HISTORY"));
//click on my account link in bread crumb
_click(_link($orderdata[1][3], _in(_div("breadcrumb"))));
//should navigate to my account landing page
_assertVisible(_heading1($orderdata[0][1]));
_assertVisible(_div("account-page"));
//click on home link in bread crumb
_click(_link($orderdata[0][3], _in(_div("breadcrumb"))));
_assertNotVisible(_link($orderdata[0][3]));
_assertNotVisible(_div("breadcrumb"))
//_assertNotVisible(_div("breadcrumb"));
_assertVisible(_div("home-main-right"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("220160","Verify the Navigation of  'Continue Shopping' Button in the My Account Order History Page for Desktop/ipad view");
$t.start();
try
{

//click on your orders button	
_click(_link("Your Orders"));
//verifying the visibility and functionality of continue shopping button
_assertVisible(_link("/(.*)/", _in(_span("continue-shopping"))));
_click(_link("/(.*)/", _in(_span("continue-shopping"))));
//application shpuld navigate to home page
_assertNotVisible(_div("breadcrumb"));
_assertVisible(_div("/home-main-left/"));
_assertVisible(_div("home-main-right"));
_click(_link("Log Out"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


_log("**************** Related to order details ******************");

var $t = _testcase("220096/220098","Verify the Functionality of Breadcrumb in My Account Order Details Page for Desktop/ipad view/ Verify the Navigation of Breadcrumb Link in  My Account Order Details Page for Desktop/ipad view");
$t.start();
try
{
	_click(_link($orderdata[0][9], _in(_div("user-section"))));
	_wait(3000);
	_setValue(_textbox("/dwfrm_login_username/"), $orderdata[5][0]);
	_setValue(_password("/dwfrm_login_password/"),$orderdata[5][1]);
	  _click(_submit("dwfrm_login_login"));
	  _wait(3000);

//	 deleteAddress();
//	  
//	 addAddress($orderdata,4)
//	//Place an order

	//Navigation till shipping page
	NavigateToCheckoutLogin($orderdata[0][6]);
	
	if (_isVisible(_select("dwfrm_singleshipping_addressList")))
	{
	//select address from drop down
	_setSelected(_select("dwfrm_singleshipping_addressList"),"1");
	}
	else
		{
		//shipping detailss
		shippingAddress($Address_Order,4);
		}
				
	//Shipment date
	eventcalendar();
	adddressProceed();
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));

	//Payment details
	PaymentDetails($Card_order,0);

	//Placing order
	_click(_checkbox("/dwfrm_billing_confirm/"));
	_click(_submit("dwfrm_billing_save"));
	takescreenshot();
	//fetch order number
	var $myaccorderno=_extract(_getText(_span("order-number")),"/[#](.*)/",true);

	var $orderemail=$uId;
	var $zipcode=$Address_Order[0][8];		
//click on your orders button	
_click(_link("Your Orders"));
//enter order no and navigate to order detail page
//_assertVisible(_numberbox("searchorder"));
_assertVisible(_textbox("searchorder"));

_setValue(_textbox("searchorder"),$myaccorderno);
_click(_submit("go-btn"));
//should navigate to order detail page
//_assert(false,"heading should verify");
_assertVisible(_div("breadcrumb"));
_assertVisible(_link($orderdata[0][0], _in(_div("breadcrumb"))));
_assertVisible(_link($orderdata[1][0], _in(_div("breadcrumb"))));
_assertVisible(_link("/"+$orderdata[2][0]+"/", _in(_div("breadcrumb"))));
//click on order details link in bread crumb
_click(_link($orderdata[2][0], _in(_div("breadcrumb"))));
//page should stay in the same page
//_assertVisible(_heading1("ORDER HISTORY"));
//click on my account link in bread crumb
_click(_link($orderdata[1][0], _in(_div("breadcrumb"))));
//should navigate to my account landing page
_assertVisible(_heading1($orderdata[0][1]));
_assertVisible(_div("account-page"));
//click on home link in bread crumb
_assertVisible(_link($orderdata[0][0], _in(_div("breadcrumb"))));
_assertNotVisible(_div("breadcrumb"));
_assertVisible(_div("home-main-right"));
_click(_link($orderdata[0][8]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

//Place an order

//Navigation till shipping page
NavigateToCheckoutLogin($orderdata[0][4]);

//shipping details
shippingAddress($Address_Order,4);
			
//Shipment date
eventcalendar();
adddressProceed();
_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));

//Payment details
PaymentDetails($Card_order,0);

//Placing order
_click(_checkbox("/dwfrm_billing_confirm/"));
_click(_submit("dwfrm_billing_save"));

//fetch order number
//var $myaccorderno=_extract(_getText(_span("order-number")),"/:(.*)/",true);
var $myaccorderno=_extract(_getText(_span("order-number")),"/[#](.*)/",true);


var $orderemail=$uId;
var $zipcode=$Address_Order[0][8];	

var $t = _testcase("220108/220125","Verify the Functionality of 'Order Summary' in  My Account Order Details Page for Desktop/ipad view/Verify the Navigation of the 'Track Package' link in the My Account Order Details Page for Mobile view");
$t.start();
try
{

//click on your orders button	
//_click(_link($Address_Order[0][12]));
//enter order no and navigate to order detail page
//_assertVisible(_numberbox("searchorder"));
//	_click(_link($orderdata[0][9], _in(_div("user-section"))));
//	_wait(3000);
//	_setValue(_textbox("/dwfrm_login_username/"), $orderdata[5][0]);
//	_setValue(_password("/dwfrm_login_password/"),$orderdata[5][1]);
//	  _click(_submit("dwfrm_login_login"));
//	  _wait(3000);
	  _click(_link("Your Orders"));
	  _setValue(_textbox("searchorder"),$myaccorderno);
	  _click(_submit("go-btn"));
	  _assertVisible(_heading1("Order Detail"));
	
//_setValue(_textbox("dwfrm_ordertrack_orderNumber"), "");
//should navigate to order detail page
//_assert(false,"heading should verify");
	  _click(_link($orderdata[0][8]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

//****************************************script by dhiraj

var $t = _testcase("220010,220011,220012","Verify the UI  of  'My Account Orders' form,Left NAv,functinallty 0f the My account link in the breadcrum in application for Desktop / ipad view");
$t.start();
try
{
	//click on the your orders
	_click(_link($Address_Order[0][12], _in(_div("user-info"))));
	//for non logged user
	_assertVisible(_link($orderdata[0][9], _in(_div("user-info"))));
	//checking the heading 
	_assertVisible(_heading1($orderdata[3][0]));
	_assertVisible(_submit("dwfrm_ordertrack_findorder"));
	_assertEqual($orderdata[3][2], _getText(_div("breadcrumb")));
	_assertVisible(_heading1($orderdata[0][5]));
	//checking the UI of the left  nav of the home page
	_assertVisible(_div($orderdata[4][0], _in(_div("LeftNav"))));
	_assertVisible(_div($orderdata[4][1], _in(_div("LeftNav[1]"))));
	_assertVisible(_div($orderdata[4][2], _in(_div("LeftNav[2]"))));
	//functionality  of the Bread crum to be cont...
	_click(_link($orderdata[1][3], _in(_div("breadcrumb"))));
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("220023,220024,220025","link in the left navigation");
$t.start();
try
{
	_click(_link($Address_Order[0][12]));
	  var $items=_collectAttributes("_link","/(.*)/","sahiText",_in(_div("LeftNav")));
	  for($i=0;$i<$items.length;$i++)
		  {
			  _click(_link($items[$i]));
			  _assertContainsText($items[$i], _div("breadcrumb"));
			  if($items==$orderdata[8][8])
				  {
				  _assertVisible(_div("dialog-container"));
				  
				  }
		  }
				
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("220090","");
$t.start();
try
{
	_click(_link($orderdata[0][9], _in(_div("user-section"))));
	//login page must be displayed
	_wait(3000);
	_assertVisible(_div("inner-container"));
	_setValue(_textbox("/dwfrm_login_username/"), $orderdata[5][0]);
	_setValue(_password("/dwfrm_login_password/"),$orderdata[5][1]);
	_click(_submit("dwfrm_login_login"));
	_wait(3000);
	_click(_link($orderdata[0][8], _in(_div("user-section"))));
}
 catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("220014","Verify the Functionality of 'LOGIN' Button in My Account Orders form in application for Desktop / ipad view");
$t.start();
try
{
	   _click(_link($orderdata[0][9], _in(_div("user-section"))));
	   _wait(3000);
	   _setValue(_textbox("/dwfrm_login_username/"), $orderdata[5][0]);
	   _setValue(_password("/dwfrm_login_password/"), $orderdata[5][1]);
	   _click(_submit("dwfrm_login_login"));
	   _wait(3000);
	   _assertVisible(_link($orderdata[0][8], _in(_div("user-section"))));
	   
	   _click(_link("Your Orders"));
	   _assertEqual($orderdata[0][10], _getText(_div("breadcrumb")));
	   _click(_link($orderdata[0][8], _in(_div("user-section"))));

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("220013,220014,220017,220019,220020,220021,220022","");
$t.start();
try
{

	//navigate to cart page
	navigateToCart($orderdata[0][6],$orderdata[0][7]);
	//var $totalpriceincart=_getText(_div("order-value", _in(_listItem("OrderTotalNum"))));
	var $totalpriceincart=_getText(_div("order-value", _in(_listItem("OrderTotalValue"))))
	//click mini cart link
	_click(_link("/mini-cart-link-checkout-link/"));
	//capturing price
	//var $lineitempriceincart=_collectAttributes("_span","price-sales","sahiText", _in(_div("/item-price box/"))).toString();
	//shipping form should display
	shippingAddress($Address_Order,0);
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	//Use shipping address as billing checkbox should be checked by default
	_assertEqual(true,_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked);
	//verifying price in shipping page
	_assertVisible(_div($totalpriceincart, _in(_div("box ordersumsnum"))));
	 
	//shipping method
	$ShippingMethodShippingpage=_getText(_div("ship-details",_near(_radio("input-radio"))));
	 
	//event calendar
	eventcalendar();
	 
	//_assertEqual($totalpriceincart,);
	//click on continue button in shipping page
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	adddressProceed();
	 
	//enter payment details
	PaymentDetails($Card_order,0);
	//check the check box
	_check(_checkbox("/dwfrm_billing_confirm/"));
	//place order button
	_click(_submit("dwfrm_billing_save"));
	 
	//order confirmation page
	var $orderno1=_extract(_getText(_span("order-number")),"/[#](.*)/",true);

	_log($orderno);
	var $zipcode=$Address_Order[0][8];
	var $email=$uId;
	_click(_link("Your Orders"))
	//checking all d fields
	_setValue(_textbox("dwfrm_ordertrack_orderNumber"),$Address_Order[3][0] );
	_assertVisible(_span( $orderdata[3][5]));
	_setValue(_textbox("dwfrm_ordertrack_orderEmail"),$Address_Order[3][0]);
	_assertVisible(_span( $orderdata[4][5]));
	_setValue(_textbox("dwfrm_ordertrack_postalCode"),$Address_Order[3][0] );
	_assertVisible(_span( $orderdata[5][5]));
	//validation of the order Number 
	
	//_click(_link("Your Orders"));
	_setValue(_textbox("dwfrm_ordertrack_orderNumber"),$orderno);
	_setValue(_textbox("dwfrm_ordertrack_orderEmail"), $uId);
	_setValue(_textbox("dwfrm_ordertrack_postalCode"), $zipcode);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("220159,220158");
$t.start();
try
{
	 _click(_link($orderdata[0][9], _in(_div("user-section"))));
	_wait(3000);
	_setValue(_textbox("/dwfrm_login_username/"), $Address_Order[1][0]);
	_setValue(_password("/dwfrm_login_password/"),$Address_Order[1][1]);
	_click(_submit("dwfrm_login_login"));
	_wait(3000);
	_click(_link($Address_Order[0][12]));
	
	_click(_link($Address_Order[0][13]));
	_assertVisible(_div($Address_Order[1][4]));
	_assertVisible(_link($Address_Order[1][5]));
	_click(_link($Address_Order[0][12]));
	_click(_link($Address_Order[0][14]));
	__wait(3000);
	 var $recentWinID=_getRecentWindow().sahiWinId;
	_log($recentWinID);
	_selectWindow($recentWinID);
	 
	//verify chat link popup
	_assertVisible(_image("Crown Awards"));
	_assertVisible(_bold("Support Chat"));
	_assertVisible(_table("chat_survey_table"));
	 
	_closeWindow();
	_selectWindow(); 
	_assertVisible(_table("window_header"));
	_assertVisible(_div("chat_survey_intro_div"));
	_click(_link($orderdata[0][8], _in(_div("user-section"))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("220154,220145,220148,220149,220152,220154,");
$t.start();
try
{
	//click on Crown award
	_click(_link($orderdata[2][6]));
	_click(_link($orderdata[1][10]));
	_wait(3000);
	_setValue(_textbox("/dwfrm_login_username/"),$orderdata[5][0]);
	_setValue(_password("/dwfrm_login_password/"),$orderdata[5][1]);
	_click(_submit("dwfrm_login_login"));
	_wait(3000);
	_click(_link($Address_Order[0][12]));
	//checking the pagination
	_assertVisible(_div("pagination"));
	//validation for first name and last name
	_assertVisible(_div("For "+$orderdata[8][3]));
	
	var $OrderID=_extract(_getText(_heading2("order-number desktop-show")),"/#(.*)/",true);
	_assertContainsText($orderdata[8][4],$OrderID);
	_setValue(_textbox("searchorder"), orderdata[0][2]);
	_click(_submit("go-btn"));
	if (!_isVisible(_div(orderdata[5][6])))
		{
			//checking the order details page
			//_click(_submit($orderdata[2][5]));
			_assertEqual($orderdata[0][10], _getText(_div("breadcrumb")));
			_assertVisible(_heading1($orderdata[2][3]));
			_assertVisible(_div($orderdata[8][0]));
			_assertVisible(_div($orderdata[8][1]));
			_assertVisible(_div($orderdata[8][2]));
			//UI of the order detais page
			_assertVisible(_span($Address_Order[1][2]));
			_assertVisible(_span($Address_Order[1][3]));
		}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("220021,220022","");
$t.start();
try
{
	_click(_link("Log Out"));
	//validation for the login page
		
			_click(_link($Address_Order[0][12]));
		
	_wait(3000);
	_setValue(_textbox("/dwfrm_login_username/"), $Address_Order[3][0]);
	//assertVisible(_div( $orderdata[3][5]));
	_setValue(_password("/dwfrm_login_password/"),  $Address_Order[3][0]);
	//click on the login
	_click(_submit("dwfrm_login_login"))
	//_assertVisible(_div($orderdata[3][5]));
	_assertVisible(_span($orderdata[3][6], _in(_div("form-row username required"))));
	//_assertVisible(_div( $orderdata[3][5]));
	_assertVisible(_span( $orderdata[4][6], _in(_div("form-row password required"))));
	
	
	_setValue(_textbox("/dwfrm_login_username/"),  $Address_Order[1][0]);
	//_assertNotVisible(_div( $orderdata[3][5]));
	_setValue(_password("/dwfrm_login_password/"),  $Address_Order[3][0]);
	//_assertVisible(_div($orderdata[3][5]));
	_click(_submit("dwfrm_login_login"))
	_assertNotVisible(_span($orderdata[3][6], _in(_div("form-row username required"))));
	_assertVisible(_span( $orderdata[4][6], _in(_div("form-row password required"))));
	//_assertVisible(_div($Address_Order[1][4]));

	_setValue(_textbox("/dwfrm_login_username/"), $Address_Order[3][0]);
	//_assertVisible(_div( $orderdata[3][5]));
	_setValue(_password("/dwfrm_login_password/"),  $Address_Order[1][0]);
	//_assertNotVisible(_div( $orderdata[3][5]));
	_click(_submit("dwfrm_login_login"))
	_assertVisible(_span($orderdata[3][6], _in(_div("form-row username required"))));
	//_assertNotVisible(_span( $orderdata[4][6], _in(_div("form-row password required"))));
	_assertNotVisible(_div($Address_Order[1][4]));
	
	_setValue(_textbox("/dwfrm_login_username/"),  $Address_Order[1][0]);
	_setValue(_password("/dwfrm_login_password/"),  $Address_Order[1][2]);
	_click(_submit("dwfrm_login_login"));
	_wait(3000);
	_click(_link("Log Out"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

/*
var $t = _testcase("220090/220091/220092/220094/220106/220107/220109","");
$t.start();
try
{
		_click(_link(Address_Order[0][12]));
	_wait(3000);
	_setValue(_textbox("dwfrm_ordertrack_orderNumber"),$orderdata[7][0]);
	_setValue(_textbox("dwfrm_ordertrack_orderEmail"), $uId);
	_setValue(_textbox("dwfrm_ordertrack_postalCode"), $orderdata[7][2]);
	_click(_submit("dwfrm_ordertrack_findorder"));
	
	_assertEqual($orderdata[2][1], _getText(_div("breadcrumb")));
	_assertVisible(_heading1($orderdata[2][0]));
	_assertVisible(_div($orderdata[8][0]));
	_assertVisible(_div($orderdata[8][1]));
	_assertVisible(_div($orderdata[8][2]));
	_assertVisible(_link($orderdata[8][5]));
	_assertVisible(_div($orderdata[8][6]));
	_assertVisible(_heading3($orderdata[8][7]));
	_assertVisible(_div("line-items", _in(_div("orderdetails"))));
	
	
}
   catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

*/


function Mobile_view()
{
	if(isMobile())
		{
			//navigate to menu button
			_click(_span("menuwrap"));
			_click(_span("next-level mobile-show accountArrow"));
			_click(_link("ORDERS"));
			
		}
	else
		{
			_click(_link($Address_Order[0][12]));
		
		}
}
	
	
	
	