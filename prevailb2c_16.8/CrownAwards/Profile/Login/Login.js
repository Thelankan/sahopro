_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Login.xls");

var $logindata=_readExcelFile("Login.xls","LoginData");
var $loginvalidations=_readExcelFile("Login.xls","LoginValidations");
var $resetpassword=_readExcelFile("Login.xls","ResetPasswordValidations");
var $myaccountvalidations=_readExcelFile("Login.xls","Myaccountvalidations");

deleteUser();

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


/*if (isMobile())
{
 _assertVisible(_link("/mobile-show/", _in(_div("user-info"))));
 _click(_link("/mobile-show/", _in(_div("user-info"))));
}
else
	{
	 _assertVisible(_link("/desktop-show/", _in(_div("user-info"))));
	 _click(_link("/desktop-show/", _in(_div("user-info"))));
	 //Visibility of overlay
	 _assertVisible(_div("dialog-container"));
	}*/

if (!isMobile())
	{
var $t = _testcase("216655/216656/216657/216658/216666","Verify the UI of the Login model/Verify the UI of the Returning Customer section in the Login model/Verify the UI of the New Customer section in the Login model/Verify the application behavior on click of Close icon in the Login model window.");
$t.start();
try
{
	
//visibility of login link in header
 _assertVisible(_link("/desktop-show/", _in(_div("user-info"))));
 _click(_link("/desktop-show/", _in(_div("user-info"))));
 //Visibility of overlay
 _assertVisible(_div("dialog-container"));
	
 //visibility of heading "Returning customers"
 //_assertVisible(_heading1("MY ACCOUNT LOG IN"));
 _assertVisible(_heading1($logindata[0][2]));
 _assertVisible(_textbox("/dwfrm_login_username/"));
 _assertVisible(_password("/dwfrm_login_password/"));
 _assertVisible(_label("/Email/"));
 _assertVisible(_label("/Password/"));
 //new customers heading
 _assertVisible(_heading2($logindata[0][0]));
 _log("Benefits of Creating a login ,Quicker Checkouts ,Store addresses for your convenience,Track Your Orders");
 //create a login button
 _assertVisible(_link("/button/", _in(_div("login-box registration-btn"))));
 //Privacy policy 
 _assertVisible(_link("privacy-policy"));
 //visibility of Cancel button in overlay
 _assertVisible(_submit("/cancel-btn/"));
 _assertVisible(_label("custom_checkbox", _in(_div("Remember me"))));
 //click on close overlay
 _assertVisible(_button("Close", _in(_div("/ui-dialog ui-widget/"))));
 _click(_button("Close", _in(_div("/ui-dialog ui-widget/"))));
//overlay should not visible
 _assertNotVisible(_div("dialog-container"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216660","Verify the application behavior on click of Cancel button in the Login model window");
$t.start();
try
{
	
 _click(_link("/desktop-show/", _in(_div("user-info"))));
 //Visibility of overlay
 _assertVisible(_div("dialog-container"));
//Visibility of overlay
_assertVisible(_div("dialog-container"));
 _assertVisible(_submit("/cancel-btn/"));
 _click(_submit("/cancel-btn/"));
//overlay should not visible
 _assertNotVisible(_div("dialog-container"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216661/216662","Verify the application behavior on click of Close icon in the Login model window after entering Valid Credentails./Verify the application behavior on click of Cancel button in the Login model window after entering Valid Credentails");
$t.start();
try
{
	 
 for (var $i=0;$i<2;$i++)
	 {
	 
 _click(_link("/desktop-show/", _in(_div("user-info"))));
//overlay should visible
 _assertVisible(_div("dialog-container"));
 //enter the values
 _setValue(_textbox("/dwfrm_login_username/"), $uId);
 _setValue(_password("/dwfrm_login_password/"), $pwd);
 
 if ($i==0)
	 {
	 //click on close icon
	 _click(_button("Close", _in(_div("/ui-dialog ui-widget/"))));
	 }
	 else if ($i==1)
		 {
		//click on login link
		 _click(_submit("/cancel-btn/"));
		 }
 
//overlay should not visible
 _assertNotVisible(_div("dialog-container"));
 //login link should still visible
 //_assertVisible(_link("/user-login/"));
 _assertVisible(_link("/desktop-show/", _in(_div("user-info"))));
	 
	 }

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216667/216668/216674/216676","Verify the UI of the New Customer section in the Login model after click on Create a login Button/Verify the application behavior on click of Close icon in the Login model window." +
		"Verify the display of Cancel button when create account form is displayed.");
$t.start();
try
{
	
//click on login link
//_click(_link("/user-login/"));

//click on login link
_click(_link("/desktop-show/", _in(_div("user-info"))));
//new customers heading
_assertVisible(_heading2($logindata[0][0]));
//create a login button
_assertVisible(_link("button", _in(_div("/login-box/"))));
_click(_link("button", _in(_div("/login-box/"))));
//UI of create account button
_assertVisible(_label("/First Name/", _in(_div("/login-box-content/"))));
_assertVisible(_label("/Last Name/", _in(_div("/login-box-content/"))));
_assertVisible(_label("/Email/", _in(_div("/login-box-content/"))));
_assertVisible(_label("/Confirm Email/", _in(_div("/login-box-content/"))));
_assertVisible(_label("/Password/", _in(_div("/login-box-content/"))));
_assertVisible(_label("/Confirm Password/", _in(_div("/login-box-content/"))));
//_assertVisible(_div($logindata[1][0], _in(_div("/login-box-content/"))));
_assertVisible(_textbox("dwfrm_profile_customer_firstname"));
_assertVisible(_textbox("dwfrm_profile_customer_lastname"));
_assertVisible(_textbox("dwfrm_profile_customer_email"));
_assertVisible(_textbox("dwfrm_profile_customer_emailconfirm"));
_assertVisible(_password("/dwfrm_profile_login_password/"));
_assertVisible(_password("/dwfrm_profile_login_passwordconfirm/"));
_assertVisible(_submit("dwfrm_profile_confirm"));
//visibility of cancel button in overlay
_assertVisible(_submit("/cancel-btn/", _in(_div("dialog-container"))));
//_assertVisible(_span($logindata[3][0]));
_assertVisible(_link("privacy-policy"));
//click on close icon
_click(_button("Close", _in(_div("/ui-dialog ui-widget/"))));
//overlay should not visible
_assertNotVisible(_div("dialog-container"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216669","Verify the application behavior on click of Cancel button in the Login model window after click on Create New account button");
$t.start();
try
{
	
//click on login link
//_click(_link("/user-login/"));

//click on login link
_click(_link("/desktop-show/", _in(_div("user-info"))));
//new customers heading
_assertVisible(_heading2($logindata[0][0]));
_assertVisible(_div("dialog-container"));
//create a login button
_assertVisible(_link("button", _in(_div("/login-box/"))));
_click(_link("button", _in(_div("/login-box/"))));
//click on login link
_click(_submit("/cancel-btn/"));	
//overlay should not visible
_assertNotVisible(_div("dialog-container"));
//login link should still visible
_assertVisible(_link("/desktop-show/", _in(_div("user-info"))));
//_assertVisible(_link("/user-login/"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()//216671

var $t = _testcase("216670/216671/216675","Verify the application behavior on click of Close icon in the Login model window after entering Valid Credentails./Verify the application behavior on click of Cancel button in the Login model window after entering Valid Credentails/Verify the display of Close icon when create account form is displayed");
$t.start();
try
{
	
 for (var $i=0;$i<2;$i++)
 {
	 
//click on login link
 _click(_link("/desktop-show/", _in(_div("user-info"))));
//overlay should visible
 _assertVisible(_div("dialog-container"));
 _click(_link("button", _in(_div("/login-box/"))));
 //enter the values
 _setValue(_textbox("dwfrm_profile_customer_firstname"), $User[$user][1]);
 _setValue(_textbox("dwfrm_profile_customer_lastname"),$User[$user][2]);
 _setValue(_textbox("dwfrm_profile_customer_email"),$User[$user][3]);
 _setValue(_textbox("dwfrm_profile_customer_emailconfirm"),$User[$user][4]);
 _setValue(_password("/dwfrm_profile_login_password/"),$User[$user][5]);
 _setValue(_password("/dwfrm_profile_login_passwordconfirm/"),$User[$user][6]);
 
 if ($i==0)
	 {
	 //click on close icon
	 _click(_button("Close", _in(_div("/ui-dialog ui-widget/"))));
	 }
	 else if ($i==1)
		 {
		 //click on cancel icon
		 _click(_submit("/cancel-btn/"));
		 }
 
//overlay should not visible
 _assertNotVisible(_div("dialog-container"));
 //login link should still visible
 _assertVisible(_link("/desktop-show/", _in(_div("user-info"))));
	 
 }

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

	}

var $t = _testcase("216672","Verify the display of Error Message when Login model is relaunched after click of Close Icon");
$t.start();
try
{

//click on login link
 _click(_link("/desktop-show/", _in(_div("user-info"))));
	
//validating the email id & password field's
for (var $i=0;$i<$loginvalidations.length;$i++)
	{
	
	 _setValue(_textbox("/dwfrm_login_username/"),$loginvalidations[$i][1]);
	 _setValue(_password("/dwfrm_login_password/"),$loginvalidations[$i][2]);
	 
	    if(_isVisible(_submit("Login")))
	    {
	      _click(_submit("/dwfrm_login/"));
	    }

	    if ($i==0 || $i==1 || $i==2 || $i==3 ||$i==4 ||$i==5)
	    	{
	    	_assertEqual($loginvalidations[0][3],_getText(_span("/dwfrm_login_username/")));
	    	_assertEqual($loginvalidations[0][4],_style(_textbox("/dwfrm_login_username/"),"background-color"));
	    	}
	    else if($i==6)
	    	{
	    	//_assertEqual($loginvalidations[6][3],_getText(_div("error-form")));
	    	}
	    else if ($i==7)
	    	{
	    	_assertEqual($loginvalidations[7][3],_getText(_span("/dwfrm_login_password/")));
	     	_assertEqual($loginvalidations[0][4],_style(_password("/dwfrm_login_password/"),"background-color"));
	    	}
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216690/216691/216712","Verify the application behvaior on click of Forgot Password link on the login model./Verify the UI of the Password Reset overlay");
$t.start();
try
{
	
//click on login link
	if (isMobile())
	{
	 _click(_link("/mobile-show/", _in(_div("user-info"))));
	}
	else
		{
		 _click(_link("/desktop-show/", _in(_div("user-info"))));
		 //Visibility of overlay
		 _assertVisible(_div("dialog-container"));
		}

//click on forget password link
_assertVisible(_link("/Forgot Password/"));
_click(_link("/Forgot Password/"));
//forget password dialogue should be displayed
_assertVisible(_div("inner-container"));
_assertVisible(_heading1($logindata[2][0]));
_assertVisible(_textbox("dwfrm_requestpassword_email"));
_assertVisible(_span("Email"));
_assertVisible(_submit("dwfrm_requestpassword_send"));

if (!isMobile())
{

	_assertVisible(_submit("cancel-return onlycancelbtn"));
	_assertVisible(_button("Close",_near(_div("inner-container"))));
}


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216692","Verify the display of Static text message and its format in Reset your password overlay.");
$t.start();
try
{

//click on login link
if (isMobile())
{
 _click(_link("/mobile-show/", _in(_div("user-info"))));
}
else
	{
	 _click(_link("/desktop-show/", _in(_div("user-info"))));
	 //Visibility of overlay
	 _assertVisible(_div("dialog-container"));
	}

//click on forget password link
_assertVisible(_link("/Forgot Password/"));
_click(_link("/Forgot Password/"));	 



//static text in forget password overlay	
_assertVisible(_heading1($logindata[2][0]));
_click(_button("Close",_near(_div("inner-container"))));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

if (!isMobile())
{
var $t = _testcase("216694/216695","Verify the application behavior on click of close icon in the password reset overlay./Verify the application behavior on click of Cancel Button in the password reset overlay.");
$t.start();
try
{
	
 for (var $i=0;$i<2;$i++)
 {

//click on login link
 _click(_link("/desktop-show/", _in(_div("user-info"))));
 //click on forget password link
 _click(_link("/Forgot Password/"));
 //reset password dialogue
 _assertVisible(_div("inner-container"));
 
 if ($i==0)
	 {
	 //click on close icon
	 _click(_button("Close",_near(_div("inner-container"))));
	 }
 else if ($i==1)
	 {
	 //click on cancel icon
	 _click(_submit("cancel-return onlycancelbtn"));
	 }

 //reset password dialogue
 _assertNotVisible(_div("inner-container"));
	 
 }

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

}

var $t = _testcase("216696/216697","Verify the application behvaior on entereing a valid value in the forgot Password overlay./Verify the UI of the Password reset confirmation overlay");
$t.start();
try
{

	//click on login link
	if (isMobile())
	{
	 _click(_link("/mobile-show/", _in(_div("user-info"))));
	}
	else
		{
		 _click(_link("/desktop-show/", _in(_div("user-info"))));
		 //Visibility of overlay
		 _assertVisible(_div("dialog-container"));
		}	
//click on forget password link
 _click(_link("/Forgot Password/"));
//enter valid email in forget password overlay
_setValue(_textbox("dwfrm_requestpassword_email"),$uId);	
//click on send button
_click(_submit("dwfrm_requestpassword_send"));
//success message should display
_assertVisible(_heading1($resetpassword[5][1]));

if (!isMobile())
	{
	_assertVisible(_div("dialog-container"));
	//cancel icon in password confirmation overlay
	_assertVisible(_button("Close",_near(_div("passwd-reset-header-form"))));
	//cancel button
	_assertVisible(_submit("cancel-btn"));
	}



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

if (!isMobile())
{
var $t = _testcase("216698/217031","Verify the Close icon functionality in Password confirmation overlay./Verify the Close and Return to page button functionality in Password Reset confirmation overlay.");
$t.start();
try
{

 for (var $i=0;$i<2;$i++)
 {
		 
	//click on login link
	if (isMobile())
	{
	 _click(_link("/mobile-show/", _in(_div("user-info"))));
	}
	else
		{
		 _click(_link("/desktop-show/", _in(_div("user-info"))));
		 //Visibility of overlay
		 _assertVisible(_div("dialog-container"));
			}	
	//click on forget password link
	 _click(_link("/Forgot Password/"));
	//enter valid email in forget password overlay
	_setValue(_textbox("dwfrm_requestpassword_email"),$uId);	
	//click on send button
	_click(_submit("dwfrm_requestpassword_send"));
	//success message should display
	_assertVisible(_heading1($resetpassword[5][1]));
	 
	 if ($i==0)
		 {
		 //click on close icon
		 _click(_button("Close",_near(_div("inner-container"))));
		 }
	 else if ($i==1)
		 {
		 //click on cancel icon
		 _click(_submit("cancel-btn"));
		 }
	//dialog container should not display
	 _assertNotVisible(_div("dialog-container"));
	 
 }

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216680","Verify the application behavior on click of Privacy Policy Link the Login model window.");
$t.start();
try
{

//click on login link
 _click(_link("/desktop-show/", _in(_div("user-info"))));
//click on privacy policy link
 _click(_link("privacy-policy"));
 //app should navigate to privacy policy page
 _assertVisible(_heading1($logindata[2][2]));
 _assertVisible(_link($logindata[1][2], _in(_div("breadcrumb"))));
 
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216700","Verify if the Login model window can be closed using the escape icon of the keyboard.");
$t.start();
try
{

//click on login link
 _click(_link("/desktop-show/", _in(_div("user-info"))));
//success message should display
_assertVisible(_div("dialog-container"));
_focusWindow();
//press escape key 	
 _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ESCAPE);
 //overlay shoul not display
 _assertNotVisible(_div("dialog-container"));	
 
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216701","Verify if the login model window when New customer section is expanded can be closed using the escape icon of the keyboard.");
$t.start();
try
{

//click on login link
 _click(_link("/desktop-show/", _in(_div("user-info"))));
//overlay should visible
 _assertVisible(_div("dialog-container"));
 _click(_link("button", _in(_div("/login-box/"))));
 _focusWindow();
//press escape key 	
 _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ESCAPE);
 //overlay shoul not display
 _assertNotVisible(_div("dialog-container"));	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216702","Verify if the the Password reset confirmation overlay can be closed using escape key of the keyboard.");
$t.start();
try
{

//click on login link
 _click(_link("/desktop-show/", _in(_div("user-info"))));
//click on forget password link
 _click(_link("/Forgot Password/"));
//enter valid email in forget password overlay
_setValue(_textbox("dwfrm_requestpassword_email"),$uId);	
//click on send button
_click(_submit("dwfrm_requestpassword_send"));
//success message should display
_assertVisible(_div("dialog-container"));
_focusWindow();
//press escape key 
 _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ESCAPE);
 //dialogue should not display
 _assertNotVisible(_div("dialog-container"));
 
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

}

var $t = _testcase("216673","Verify the display of Error Message when Login model is relaunched after click of Cancel Button.");
$t.start();
try
{
	
//click on login link
_click(_link("/desktop-show/", _in(_div("user-info"))));

//login to the application
_setValue(_textbox("/dwfrm_login_username/"),$loginvalidations[7][1]);
_setValue(_password("/dwfrm_login_password/"),$loginvalidations[7][2]);
_click(_submit("/dwfrm_login/"));
_assertEqual($loginvalidations[7][3],_getText(_span("/dwfrm_login_password/")));
//click on cancel button
_click(_submit("/cancel-btn/"));
//dialog container should not display
_assertNotVisible(_div("dialog-container"));

//click on login link
_click(_link("/desktop-show/", _in(_div("user-info"))));
//error message should not visible
_assertNotVisible(_span("/dwfrm_login_password/"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216689","Verify the application on click of Change Password Link in Login model");
$t.start();
try
{
	
	 for (var $i=0;$i<2;$i++)
	 {
			 
		//click on login link
		if (isMobile())
		{
		 _click(_link("/mobile-show/", _in(_div("user-info"))));
		}
		else
			{
			 _click(_link("/desktop-show/", _in(_div("user-info"))));
			 //Visibility of overlay
			 _assertVisible(_div("dialog-container"));
				}	
		//click on forget password link
		_click(_link("Change Password"));
		_assertVisible(_heading1($logindata[4][0]));
		//enter valid email in forget password overlay
		_setValue(_textbox("dwfrm_requestpassword_email"),$uId);
		//click on send button
		_click(_submit("dwfrm_requestpassword_send"));
		//success message should display
		_assertVisible(_heading1($logindata[3][1]));
		_assertVisible(_paragraph($logindata[5][1]));
		 
		 if ($i==0)
			 {
			 //click on close icon
			 _click(_button("Close",_near(_div("inner-container"))));
			 }
		 else if ($i==1)
			 {
			 //click on cancel icon
			 _click(_submit("cancel-btn"));
			 }
		//dialog container should not display
		 _assertNotVisible(_div("dialog-container"));
		 
	 }

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("216683/216684","Verify the Validation for First Name text field in new customer section.");
$t.start();
try
{

 //Create account
if (isMobile())
{
 _click(_link("/mobile-show/", _in(_div("user-info"))));
}
else
	{
	 _click(_link("/desktop-show/", _in(_div("user-info"))));
	 //Visibility of overlay
	 _assertVisible(_div("dialog-container"));
	}
 _click(_link("button", _in(_div("/login-box/"))));	
	
 
//validating the email id & password field's
for (var $i=0;$i<$myaccountvalidations.length;$i++)
	{
	
	 _setValue(_textbox("dwfrm_profile_customer_firstname"),$myaccountvalidations[$i][1]);
 	 _setValue(_textbox("dwfrm_profile_customer_lastname"),$myaccountvalidations[$i][2]);
 	 _setValue(_textbox("dwfrm_profile_customer_email"),$myaccountvalidations[$i][3]);
 	 _setValue(_textbox("dwfrm_profile_customer_emailconfirm"),$myaccountvalidations[$i][4]);
 	 _setValue(_password("/dwfrm_profile_login_password/"),$myaccountvalidations[$i][5]);
 	 _setValue(_password("/dwfrm_profile_login_passwordconfirm/"),$myaccountvalidations[$i][6]);
 	 
 	 
 	 if ($i==0)
 		 {
 		_click(_submit("dwfrm_profile_confirm"));
 		_assertEqual($myaccountvalidations[0][8], _getText(_span("dwfrm_profile_customer_email-error")));
 		_assertEqual($myaccountvalidations[1][8], _getText(_span("dwfrm_profile_customer_emailconfirm-error")));
 		_assertEqual($myaccountvalidations[0][9], _getText(_span("dwfrm_profile_customer_firstname-error")));
 		_assertEqual($myaccountvalidations[0][10], _getText(_span("dwfrm_profile_customer_lastname-error")));
 		_assertEqual($myaccountvalidations[0][11], _getText(_span("/dwfrm_profile_login_password/")));
 		_assertEqual($myaccountvalidations[1][11], _getText(_span("/dwfrm_profile_login_passwordconfirm/")));
 		_assertEqual($myaccountvalidations[0][7],_style(_textbox("/dwfrm_profile_customer_firstname/"),"background-color"));
 		_assertEqual($myaccountvalidations[0][7],_style(_textbox("/dwfrm_profile_customer_lastname/"),"background-color"));
 		_assertEqual($myaccountvalidations[0][7],_style(_textbox("/dwfrm_profile_customer_email/"),"background-color"));
 		_assertEqual($myaccountvalidations[0][7],_style(_textbox("/dwfrm_profile_customer_emailconfirm/"),"background-color"));
 		_assertEqual($myaccountvalidations[0][7],_style(_password("/dwfrm_profile_login_password/"),"background-color"));
 		_assertEqual($myaccountvalidations[0][7],_style(_password("/dwfrm_profile_login_passwordconfirm/"),"background-color"));
 		 }
 	 else if ($i==1)
 		 {
 		_click(_submit("dwfrm_profile_confirm"));
 		_assertEqual($myaccountvalidations[3][7], _getText(_textbox("/dwfrm_profile_customer_firstname/")).length);
 		_assertEqual($myaccountvalidations[3][7], _getText(_textbox("/dwfrm_profile_customer_lastname/")).length);
 		_assertEqual($myaccountvalidations[1][7], _getText(_textbox("/dwfrm_profile_customer_email/")).length);
 		_assertEqual($myaccountvalidations[1][7], _getText(_textbox("/dwfrm_profile_customer_emailconfirm/")).length);
 		_assertEqual($myaccountvalidations[2][7], _getText(_password("/dwfrm_profile_login_password/")).length);
 		_assertEqual($myaccountvalidations[2][7], _getText(_password("/dwfrm_profile_login_passwordconfirm/")).length);
 		 }
 	 else if ($i==2 || $i==3 || $i==4 ||$i==5 ||$i==6)
 		 {
 		_click(_submit("dwfrm_profile_confirm"));
 		_assertEqual($myaccountvalidations[0][7],_style(_textbox("/dwfrm_profile_customer_email/"),"background-color"));
 		_assertEqual($myaccountvalidations[0][7],_style(_textbox("/dwfrm_profile_customer_emailconfirm/"),"background-color"));
 		_assertEqual($myaccountvalidations[2][8], _getText(_span("dwfrm_profile_customer_email-error")));
 		_assertEqual($myaccountvalidations[2][8], _getText(_span("dwfrm_profile_customer_emailconfirm-error")));
 		 }
 	 else if ($i==7)
 		 {
 		_click(_submit("dwfrm_profile_confirm"));
 		_assertVisible(_div($myaccountvalidations[7][7],_near(_textbox("dwfrm_profile_customer_emailconfirm"))));
 		 }
	 else if ($i==8)
		 {
		 _click(_submit("dwfrm_profile_confirm"));
		_assertVisible(_div($myaccountvalidations[8][7],_near(_password("/dwfrm_profile_login_passwordconfirm/"))));
		 }
	 else if ($i==9)
		 {
		 _click(_submit("dwfrm_profile_confirm"));
		 _assertVisible(_span($myaccountvalidations[9][7],_near(_password("/dwfrm_profile_login_password/"))));
		 _assertVisible(_span($myaccountvalidations[9][7],_near(_password("/dwfrm_profile_login_password/"))));
		 _assertEqual($myaccountvalidations[0][7],_style(_password("/dwfrm_profile_login_password/"),"background-color"));
	 	 _assertEqual($myaccountvalidations[0][7],_style(_password("/dwfrm_profile_login_passwordconfirm/"),"background-color"));
		 }
	 else
		 {
		 createAccount()
		 //should navigate to my account landing page
		 _assertVisible(_div("account-page"));
		 _assertVisible(_link("My Account", _in(_div("breadcrumb"))));
		 }
	}

//log out from application
logout();

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216693/216705/216706/216709/216710","Verify the validation of Email text input fieldin reset Password overlay");
$t.start();
try
{

	//Create account
	if (isMobile())
	{
	 _click(_link("/mobile-show/", _in(_div("user-info"))));
	}
	else
		{
		 _click(_link("/desktop-show/", _in(_div("user-info"))));
		 //Visibility of overlay
		 _assertVisible(_div("dialog-container"));
		}
	
//click on froget password 
_click(_link("password-reset-dialog"));

//validating the email id field's
for (var $i=0;$i<$resetpassword.length;$i++)
	{
	
	_setValue(_textbox("dwfrm_requestpassword_email"),$resetpassword[$i][0]);
	_click(_submit("dwfrm_requestpassword_send"));
	
	if ($i==0|| $i==1 ||$i==2 ||$i==3 ||$i==3 ||$i==4)
		{
		_assertEqual($resetpassword[0][1], _getText(_span("dwfrm_requestpassword_email-error")));
		_assertEqual($resetpassword[0][2],_style(_textbox("dwfrm_requestpassword_email"),"background-color"));
		}
		else
			{
			_assertVisible(_heading1($resetpassword[5][1]));
			}
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216708","Verify the validation of Email text field in Change page");
$t.start();
try
{
	
	//Create account
	if (isMobile())
	{
	 _click(_link("/mobile-show/", _in(_div("user-info"))));
	}
	else
		{
		 _click(_link("/desktop-show/", _in(_div("user-info"))));
		 //Visibility of overlay
		 _assertVisible(_div("dialog-container"));
		}
	
//click on froget password 
_click(_link("/change-password/"))

//validating the email id field's
for (var $i=0;$i<$resetpassword.length;$i++)
	{
	
	_setValue(_textbox("dwfrm_requestpassword_email"),$resetpassword[$i][0]);
	_click(_submit("dwfrm_requestpassword_send"));
	
	if ($i==0|| $i==1 ||$i==2 ||$i==3 ||$i==3|| $i==4)
		{
		_assertEqual($resetpassword[0][1], _getText(_span("dwfrm_requestpassword_email-error")));
		_assertEqual($resetpassword[0][2],_style(_textbox("dwfrm_requestpassword_email"),"background-color"));
		}
		else
			{
			_assertVisible(_heading1($resetpassword[5][1]));
			}
	}	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("","");
$t.start();
try
{

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

/*var $t = _testcase("","");
$t.start();
try
{

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()*/