_include("../util.GenericLibrary/GlobalFunctions.js");
_resource("Cart.xls");

var $Minicart=_readExcelFile("Cart.xls","Minicart");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

addItemToCart($Minicart[0][0],$Minicart[0][1]);

var $t = _testcase("221800/221803/221802/221808/219354/219388","Verify the UI of the mini cart./UI of the Mini cart header./display format of the sub total price/display of Description of the product");
$t.start();
try
{
	_mouseOver(_link("/mini-cart-link/"));
	var $quantity=_extract(_getText(_span("/minicart-quantity/")),"(.*)Item",true);
	//Heading
	_assertEqual("/"+$Minicart[0][3]+"/", _getText(_div("miniCartTitle")));
	//quantity
	_assertVisible(_link("/minicart-quantity/"));
	_assertContainsText("Item(S)", _link("/minicart-quantity/"));
	//Checkout button top right
	_assertVisible(_link("/Checkout/", _in(_div("miniCartHeader"))));
	//Prod image
	_assertVisible(_div("miniCartProdimg"));
	//Prod name
	_assertVisible(_link("/(.*)/", _in(_div("miniCartProddesc"))));
	//Prod num
	_assertVisible(_div("miniCartItemnum", _in(_div("miniCartProddesc"))));
	//quantity
	_assertVisible(_span("Quantity:"));
	if($quantity>4)
	{
		_assertVisible(_link("miniCartMoreItems"));
	}
	else
	{
		_assertNotVisible(_link("miniCartMoreItems"));
	}
	//price
	_assertVisible(_span("mini-cart-each-price"));
	//total
	_assertVisible(_span("mini-cart-price"));
	//Subtotal
	_assertVisible(_div("miniCartSubtotal"));
	_assertVisible(_div("/Subtotal: [$](.*).(.*)/"));
	//viewcart
	_assertVisible(_link("/(.*)/", _in(_div("miniCartViewCart"))));
	//Checkout bottom right
	_assertVisible(_div("miniCartCheckout", _in(_div("miniCartButtons"))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("221811","Verify the display of Quantity value in the mini cart fly out.");
$t.start();
try
{
	NavigatetoHomepage();
	_mouseOver(_link("/mini-cart-link/"));
	var $miniquantity=_extract(_getText(_div("/mini-quantity/")),"[:](.*)[|]",true);
	var $mininame=_extract(_getText(_link("/(.*)/", _in(_div("miniCartProddesc")))),"(.*)[#] [#]",true);
	
	_assertEqual($prodname,$mininame);
	_assertEqual($prodquantity,$miniquantity);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("221809/219370","Verify the application behvaior on click of Product Image in the Mini Fly out cart.");
$t.start();
try
{
	_mouseOver(_link("/mini-cart-link/"));
	//product name in minicart
	var $mininame=_extract(_getText(_link("/(.*)/", _in(_div("miniCartProddesc")))),"(.*)[#] [#]",true);
	//Click on product image
	_click(_link("/(.*)/", _in(_div("miniCartProdimg"))));
	//Checking product in pdp
	_assertEqual($mininame,_getText(_heading1("product-name")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("221806/221816/221815","Verify the application behvaior on click of checkout button/View cart Button displayed at right top corner/bottom of the mini cart section.");
$t.start();
try
{
	_mouseOver(_link("/mini-cart-link/"));
	//Checkout button top right
	_click(_link("/Checkout/", _in(_div("miniCartHeader"))));
	//Checkout page
	_assertVisible(_div("checkoutlogin"));
	//Homepage
	NavigatetoHomepage();
	//viewcart
	_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));
	//Cart page
	_assertVisible(_listItem("cart-item-name"));
	//Homepage
	NavigatetoHomepage();
	//Checkout bottom right
	_click(_div("miniCartCheckout", _in(_div("miniCartButtons"))));
	//Checkout page
	_assertVisible(_div("checkoutlogin"));
	//Homepage
	NavigatetoHomepage();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("221810","Verify the application behvaior on Mouse hover of the Product Image");
$t.start();
try
{
	_mouseOver(_link("/mini-cart-link/"));
	//tooltip of product image
	var $imagetooltip=_getAttribute(_image("/(.*)/", _in(_div("miniCartProdimg"))), "title");
	//Product name in minicart
	var $mininame=_extract(_getText(_link("/(.*)/", _in(_div("miniCartProddesc")))),"(.*)[#] [#]",true);
	_assertEqual($mininame,$imagetooltip);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//Add four products to cart
for(var $i=1;$i<5;$i++)
{
	addItemToCart($Minicart[$i][0],$Minicart[$i][1]);
}

var $t = _testcase("221813/219384/219366/219382/219383/221258","Verify the application behvaior on click of More items in cart link in mini cart/Verify the application behvaior on click of More items in cart link in mini cart");
$t.start();
try
{
	_mouseOver(_link("/mini-cart-link/"));
	_assertVisible(_div("miniCartSubtotal"));
	var $quantity=_extract(_getText(_span("/minicart-quantity/")),"(.*)Item",true);
	if($quantity>4)
	{
		_assertVisible(_link("miniCartMoreItems"));
		_click(_link("miniCartMoreItems"));
		_wait(1000);
		
		for(var $i=1;$i<5;$i++)
			{
			//Cart page
			_assertVisible(_listItem("cart-item-name["+$i+"]"));
			//price
			_assertVisible(_span("mini-cart-each-price["+$i+"]"));
			//total
			_assertVisible(_span("mini-cart-price["+$i+"]"));
			}
		
		//Homepage
		NavigatetoHomepage();
	}
	else
	{
		_assert(false);
	}

//navigate to checkout page
_click(_link("/mini-cart-link-checkout-link/"));
//
for(var $i=1;$i<5;$i++)
	{
	_assertVisible(_link("/(.*)/", _in(_div("name["+$i+"]"))));
	_assertVisible(_div("item-quantity box["+$i+"]"));
	_assertVisible(_div("item-total box carttotal["+$i+"]"));
	}
	
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("221549/221549","Verify the Remove Link functionality for a Single trophy Product in cart Page/Verify the Remove Link functionality for a Single trophy Product in cart Page");
$t.start();
try
{

//click mini cart pink
_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));

var $countofproducts=_count("_div","/cart-row/",_in(_div("CartTableLeft ")));

if ($countofproducts==1)
	{
	_click(_submit("/Remove/",_in(_div("/cart-row/"))));
	_assertNotVisible(_div("cart-row box-row"));
	}
	else
		{
		_click(_submit("/Remove/",_in(_div("/cart-row/"))));
		var $countofproductsafterdelete=_count("_div","/cart-row/",_in(_div("CartTableLeft ")));
		_assertEqual($countofproducts-1,$countofproductsafterdelete);
		}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

/*var $t = _testcase("","");
$t.start();
try
{



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/