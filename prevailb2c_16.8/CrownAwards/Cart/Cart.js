_include("../util.GenericLibrary/GlobalFunctions.js");
_resource("Cart.xls");

var $Minicart=_readExcelFile("Cart.xls","Minicart");
var $cart=_readExcelFile("Cart.xls","Cart");
var $shipaddress=_readExcelFile("../Checkout/ShippingPage/ShippingPage.xls","ShipAddress");


SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("219333/219386/221003/221257","Verify the applicator behavior on click of Cart tab in progress indicator when the Cart Page is displayed/Verify the application behvaior on click of View cart Button in mini cart flyout" +
		"Verify the UI of the Progress Indicator in the Cart Page");
$t.start();
try
{
	
//navigate to cart page
navigateToCart($cart[0][0],$cart[0][1]);
//line items 
_assertVisible(_div("cart-row box-row "));
//verifying prodname,quantity,price 
_assertEqual($prodname,$pName);
_assertEqual($prodquantity,$quantity);
//shopping cart
_assertVisible(_div("/Shopping Cart/"),_in(_div("/checkout-progress-indicator/")));
_assertEqual("/Shopping Cart/", _getText(_div("step-1 active")));

//UI of progress indicator
_assertVisible(_div("Shopping Cart Review Your Cart",_in(_div("/checkout-progress-indicator/"))));
_assertVisible(_div("step-1 active"));
_assertVisible(_div("Shipping & Delivery", _in(_div("/checkout-progress-indicator/"))));
_assertVisible(_div("step-2 inactive"));
_assertVisible(_div("Payment & Place Order", _in(_div("/checkout-progress-indicator/"))));
_assertVisible(_div("step-3 inactive"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219371","Verify the application behvaior on Mouse hover of the Product Image.");
$t.start();
try
{

	_mouseOver(_image("/(.*)/", _in(_div("cartprod"))));
	//
	if (_getAttribute(_image("/(.*)/", _in(_div("cartprod")))),"title"!=null)
	 {
	     _assert(true);
	 }
	else
	{
	     _assert(false);
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("221008","Verify the UI of the Previously viewed section in the cart Page.");
$t.start();
try
{

//previously viewed	
_assertVisible(_heading2($cart[6][2]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219336/221260/221263/218460","Verify the UI of the Cart Header");
$t.start();
try
{

//click on cart link after adding the product
_click(_span("value", _in(_span("minicart-quantity desktop-show"))));
//should navigate to cart page
_assertVisible(_div("CartTableLeft "));
_assertVisible(_div("CartWrap"));

//cart header UI 
_assertVisible(_link("/(.*)/", _in(_div("name"))));
_assertVisible(_div("/item-price box/"));
_assertVisible(_div("item-quantity box"));
_assertVisible(_div("Item"));
_assertVisible(_div("Product Description"));
_assertVisible(_div("Unit Price"));
_assertVisible(_div("Qty"));
_assertVisible(_div("Edit Item"));
_assertVisible(_div("Total Price"));
_assertVisible(_div("Subtotal:"));
_assertVisible(_listItem("Shipping"));
_assertVisible(_div("Sales Tax"));
_assertVisible(_div("Estimated Total"));

//comparing unit price and total price
var $unitpricelength=_getText(_div("/item-price box cartunit/")).split(" ").length;

for (var $i=0;$i<$unitpricelength;$i++)
	{
	var $unitprice=_extract(_getText(_div("/item-price box cartunit/")).split(" ")[$i],"/[$](.*)/",true);
	var $actqty=_getText(_div("item-quantity box"));
	var $actualunitprice=$unitprice*$actqty;
	var $totalprice=_extract(_getText(_div("item-total box carttotal")).split(" ")[$i],"/[$](.*)/",true);
	_assertEqual($actualunitprice,$totalprice);
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("226526","Verify the Functionality of the Save Cart in the Cart Page as a Guest User.");
$t.start();
try
{

//visibility and click on save cart link
_assertVisible(_link("/save-cart/", _in(_div("cart-actions cartbuttons"))));
_click(_link("/save-cart/", _in(_div("cart-actions cartbuttons"))));
_wait(3000);
//login pop up should display
//Visibility of overlay
_assertVisible(_div("dialog-container"));
//visibility of heading "Returning customers"
_assertVisible(_textbox("/dwfrm_login_username/"));
_assertVisible(_password("/dwfrm_login_password/"));
_click(_submit("/cancel/", _in(_div("dialog-container"))));
//overlay should not visible
_assertNotVisible(_div("dialog-container"));
//verifying success message
_assertVisible(_div($cart[0][2],_in(_div("CartWrap"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219337","Verify the application behavior on click of Cart Header text labels");
$t.start();
try
{

_click(_div("Item"));
cartui();
_click(_div("Product Description"));
cartui();
_click(_div("Unit Price"));
cartui();
_click(_div("Qty"));
cartui();
_click(_div("Edit Item"));
cartui();
_click(_div("Total Price"));
cartui();
_click(_div("Subtotal:"));
cartui();


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219364/219365/219369","Verify the UI of the Line item section in the Mini cart flyout./Verify the display format of the sub total price in the mini cart/Verify the display of Description of the product in mini cart for each Line item");
$t.start();
try
{

//mouse hover on cart link	
_mouseOver(_link("/mini-cart-link/"));
//product count
var $countofproducts=_count("_image","/(.*)/",_in(_div("/mini-cart-products/")));
//
for (var $i=0;$i<$countofproducts;$i++)
{
	
_assertVisible(_span("Quantity:["+$i+"]"));
_assertVisible(_span("mini-cart-each-price["+$i+"]"));
_assertVisible(_div("miniCartProdimg["+$i+"]"));
_assertVisible(_div("miniCartItemnum["+$i+"]"));
_assertVisible(_link("/(.*)/",_in(_div("miniCartProddesc["+$i+"]"))));
	
}	

//mouse hover on cart link	
_mouseOver(_link("/mini-cart-link/"));

//mini cart total
_assertVisible(_div("miniCartSubtotal"));
_assertVisible(_span("/$(.*)/"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("221004/218461","Verify the UI of the Bottom Pane in the Cart Page./Verify the UI of the Right Pane in cart Page.");
$t.start();
try
{

	//Bottom cart UI
	_assertVisible(_link("/save-cart/", _in(_div("cart-actions cartbuttons"))));
	_assertVisible(_submit("dwfrm_cart_continueShopping", _in(_div("cart-actions cartbuttons"))));
	_assertVisible(_link("empty-cart button-continue-shopping", _in(_div("cart-actions cartbuttons"))));
	_assertVisible(_link("delivery-calculator", _in(_div("cart-actions cartbuttons"))));
	_assertVisible(_submit("button-checkout[1]", _in(_div("cart-actions cartbuttons"))));
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("226583/226585","Verify the functionality of Delivery Time Calculator after the user enters a valid zip code./Verify the application behavior when user click on Close and Return Page button in Delivery Calculator model before entering a Zip code.");
$t.start();
try
{

_assertVisible(_link("delivery-calculator", _in(_div("cart-actions cartbuttons"))));
_click(_link("delivery-calculator", _in(_div("cart-actions cartbuttons"))));
//check delivery calculator dialog
_assertVisible(_div("deliverycalculator"));
_assertEqual($cart[4][2], _getText(_heading2("dtc-header")));
_assertVisible(_div("display-available-methods"));
_assertVisible(_submit("dwfrm_cartcalculate_submit"));
//enter zip code
_assertVisible(_textbox("dwfrm_cartcalculate_postalcode"));
_setValue(_textbox("dwfrm_cartcalculate_postalcode"),$cart[0][2]);
_click(_submit("dwfrm_cartcalculate_submit"));
//success message
_assertVisible(_heading2($cart[5][2]));
//close the dialouge
_assertVisible(_submit("cancel-btn"));
_assertVisible(_button("Close"));
_click(_button("Close"));
//overlay should close
_assertNotVisible(_div("deliverycalculator"));
//click on cancel button
_click(_link("delivery-calculator", _in(_div("cart-actions cartbuttons"))));
_assertVisible(_div("deliverycalculator"));
_click(_submit("cancel-btn"));
//overlay should close
_assertNotVisible(_div("deliverycalculator"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("226577/226575/226576","Veirfy the display of Items in Mini after User has selected the Empty Cart Option./");
$t.start();
try
{
	
//navigate to cart
_click(_link("/mini-cart-link/"));
//items displayed in cart
_assertVisible(_div("CartTableLeft "));
//Empty cart button
_click(_link("/empty-cart/",_in(_div("cart-actions cartbuttons"))));
//empty cart overlay
_assertVisible(_paragraph($cart[1][2]));
_assertVisible(_div("dialog-container"));
_assertVisible(_div("empty-cart-dlg"));
_assertVisible(_submit("button-text"));
_assertVisible(_submit("emptycart-no-btn button-text"));
_assertVisible(_button("Close"));

//ok in modal overlay
if(_isVisible(_div("dialog-container")))
{
	_click(_submit("button-text"));
}

//no items should display in mini cart
_assertVisible(_span($cart[1][1]+" Item(s)"));
_mouseOver(_link("/mini-cart-link/"));
_assertVisible(_div($cart[2][2]));

//items should not display in cart
_assertNotVisible(_div("CartTableLeft "));
//empty cart message 
_assertVisible(_heading1($cart[2][2]));
_assertVisible(_submit("dwfrm_cart_continueShopping"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase(" 226571","Verify the Functionality of the Continue Shopping button in Cart Page");
$t.start();
try
{

//functionality of continue shopping button
_click(_submit("dwfrm_cart_continueShopping"));
//verify the home page
_assertNotVisible(_div("breadcrumb"));
_assertVisible(_div("home-main-right"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("12345","Adding and deleting product-A in cart page,shipping page,payment page and placing rorder with product-B");
$t.start();
try
{

for (var $i=0;$i<4;$i++)
	{
	
	//navigate to cart page
	navigateToCart($cart[0][0],$cart[2][1]);
	
	//verifying the quantity and price
	_assertEqual($prodquantity,$quantity);
	_assertEqual($totalprice,$orderTotal);
	//Remove item from cart
	var $countofproducts=_count("_div","/cart-row/",_in(_div("CartTableLeft ")));
	if ($i==0)
		{
	      for (var $j=0;$j<$countofproducts;$j++)
	    	  {
	    	  _click(_submit("/Remove/",_in(_div("/cart-row/"))));
	    	  }
	      //items should not display in cart
	      _assertNotVisible(_div("CartTableLeft "));
	      //empty cart message 
	      _assertVisible(_heading1($cart[2][2]));
	      _assertVisible(_submit("dwfrm_cart_continueShopping"));
		}
	else if ($i==1)
		{
		//navigate to checkout page
		_click(_link("/mini-cart-link-checkout-link/"));
		
		while(_isVisible(_link("Remove Item", _in(_div("/mini-cart-names/")))))
			{
		_click(_link("Remove Item", _in(_div("/mini-cart-names/"))))
			}
	     //empty cart message 
	      _assertVisible(_heading1($cart[2][2]));
	      _assertVisible(_submit("dwfrm_cart_continueShopping"));
		}
	else
		{
		
		   //navigate to checkout page
		   _click(_link("/mini-cart-link-checkout-link/"));
		   //shipping form should display
		   shippingAddress($shipaddress,0);
		   //event calendar
		   eventcalendar();
		 //click on continue button in shipping page
		   _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
		   adddressProceed();
		   
		   while(_isVisible(_link("Remove", _in(_div("secondary")))))
			  {
	      _click(_link("Remove", _in(_div("secondary"))));
			  }
		   
		     //empty cart message 
		   	 _assertVisible(_heading1($cart[2][2]));
		     _assertVisible(_submit("dwfrm_cart_continueShopping"));
		}
	
	}

//navigate to cart page
navigateToCart($cart[1][0],$cart[2][1]);
//price cal
var $pricecal=$totalpriceBeforeselectingQuantity*$cart[2][1];
//verifying the quantity and price
_assertEqual($prodquantity,$quantity);
_assertEqual($prodname,$pName);
_assertEqual($totalprice,$orderTotal);
_assertEqual($pricecal,$orderTotal);
//navigate to checkout page
_click(_link("/mini-cart-link-checkout-link/"));
//shipping form should display
shippingAddress($shipaddress,0);
//address proceed
adddressProceed();
//event calendar
eventcalendar();
//click on continue button in shipping page
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
adddressProceed();
//entering payment details
PaymentDetails($shipaddress,5);
//check the check box
_check(_checkbox("/dwfrm_billing_confirm/"));
//place order button
_click(_submit("dwfrm_billing_save"));
//
//_assertEqual($prodname,);
//order confirmation page
var $orderno=_getText(_span("order-number")).split(" ")[2];
_log($orderno);

var $orderkkkk=_getText(_span("order-number"))
_log($orderkkkk);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase("","");
$t.start();
try
{


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/

function cartui()
{
	//shopping cart
	_assertVisible(_div("/Shopping Cart/"),_in(_div("/checkout-progress-indicator/")));
	_assertEqual("/Shopping Cart/", _getText(_div("step-1 active")));
	_assertVisible(_div("CartTableLeft "));
	_assertVisible(_div("CartWrap"));	
}