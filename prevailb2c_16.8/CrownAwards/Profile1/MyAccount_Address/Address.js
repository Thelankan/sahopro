_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Address.xls");

var $address_data=_readExcelFile("Address.xls","Address_Data");
var $addr=_readExcelFile("Address.xls","Addr");
var $Address_Validation=_readExcelFile("Address.xls","Address_Validation");


SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//Validation for add address

function addAddressValidation($validations,$r)
{
	 _setValue(_textbox("dwfrm_profile_address_email"),$validations[$r][1]);
	 _setValue(_textbox("dwfrm_profile_address_emailcomform"),$validations[$r][2]);
	 _setValue(_textbox("dwfrm_profile_address_firstname"), $validations[$r][3]);
	 _setValue(_textbox("dwfrm_profile_address_lastname"), $validations[$r][4]);
	 _setValue(_textbox("dwfrm_profile_address_organization"), $validations[$r][5]);
	 _setValue(_textbox("dwfrm_profile_address_address1"), $validations[$r][6]);
	 _setValue(_textbox("dwfrm_profile_address_address2"), $validations[$r][7]);
	 _setValue(_textbox("dwfrm_profile_address_postal"), $validations[$r][8]);
	 
	 _setSelected(_select("dwfrm_profile_address_country"),$validations[$r][10]);
	 _wait(2000)
	 _setSelected(_select("dwfrm_profile_address_states_state"),$validations[$r][11]);
	 _wait(2000)
	 _setValue(_textbox("dwfrm_profile_address_city"), $validations[$r][9]);
	 _setValue(_textbox("dwfrm_profile_address_phone"),$validations[$r][12]);
	 _setValue(_textbox("dwfrm_profile_address_phoneext"), $validations[$r][13]);
	 _setValue(_textbox("dwfrm_profile_address_phone2"), $validations[$r][14]);

}

//login to the application
if (isMobile())
{
 _click(_link("/mobile-show/", _in(_div("user-info"))));
}
else
	{
	 _click(_link("/desktop-show/", _in(_div("user-info"))));
	 //Visibility of overlay
	 _assertVisible(_div("dialog-container"));
	}

login();
_wait(6000);
deleteAddress()
var $t = _testcase("219549/219626","Verify the navigation related to 'Home' and 'My Account' links/ UI of Bread crumb on My account Addresses page bread crumb as a Registered user");
$t.start();
try
{
	//
	NavigatetoAccountLanding();
	//click on address link
	_click(_link($address_data[0][2]));
	//Navigate to My account page
	_click(_link($address_data[1][0], _in(_div("breadcrumb"))));
	//Verify
	_assertEqual($address_data[1][0], _getText(_link("breadcrumb-element last", _in(_div("breadcrumb")))));
	//Navigate to addresses page
	//_click(_link("ADDRESSES", _in(_div("account-page"))));
	_click(_link("Addresses", _in(_div("LeftNav"))));
	//Click on Home link in breadcrumb
	_click(_link("Home", _in(_div("breadcrumb"))));
	//Verify
	_assertVisible(_div("home-promocontent"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219551","Verify the UI of 'Add Address overlay Addresses page' in application as a Registered user.");
$t.start();
try
{
     NavigatetoAccountLanding();
	//click on address link
	_click(_link($address_data[0][2]));
	//Add new button
	//_click(_submit("address-create desktop-show"));indetifiers is changing frequently
	
	//_click(_submit("address-create js-address-add desktop-show"));
	_click(_submit("address-create desktop-show"));
	//Email
	_assertVisible(_span("Email"));
	_assertVisible(_textbox("dwfrm_profile_address_email"));
	//Confirm email
	_assertVisible(_span("Confirm Email"));
	_assertVisible(_textbox("dwfrm_profile_address_emailcomform"));
	//First name
	_assertVisible(_span("First Name"));
	_assertVisible(_textbox("dwfrm_profile_address_firstname"));
	//Last name
	_assertVisible(_span("Last Name"));
	_assertVisible(_textbox("dwfrm_profile_address_lastname"));
	//Organisation
	_assertVisible(_span("Organization"));
	_assertVisible(_textbox("dwfrm_profile_address_organization"));
	//Address state
	_assertVisible(_span("Address Street"));
	_assertVisible(_textbox("dwfrm_profile_address_address1"));
	//Address 2
	_assertVisible(_span("Suite/Apt."));
	_assertVisible(_textbox("dwfrm_profile_address_address2"));
	//Zip
	_assertVisible(_span("Postal Code"));
	_assertVisible(_textbox("dwfrm_profile_address_postal"));
	//city
	_assertVisible(_span("City"));
	_assertVisible(_textbox("dwfrm_profile_address_city"));
	//Country
	_assertVisible(_span("Country"));
	_assertVisible(_select("dwfrm_profile_address_country"));
	//state
	_assertVisible(_span("State"));
	_assertVisible(_select("dwfrm_profile_address_states_state"));
	//Mobile
	_assertVisible(_span("Mobile/Day Phone"));
	_assertVisible(_textbox("dwfrm_profile_address_phone"));
	//Ext
	_assertVisible(_span("Ext."));
	_assertVisible(_textbox("dwfrm_profile_address_phoneext"));
	//Secondary phone
	_assertVisible(_span("Secondary Phone"));
	_assertVisible(_textbox("dwfrm_profile_address_phone2"));
	//Required text
	_assertVisible(_div("dialog-required"));
	//Add new button
	_assertVisible(_submit("dwfrm_profile_address_create"));
	//cancel link
	_assertVisible(_link("Cancel"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219566/219567/219568/219569/219572/219573/219574/219659/219660/219661/219663/219664/219555/219662/219678","Verify the validation related to First Name/Last name/Address1/Address2/City/Zipcode/Mobile/email/confirm email/Organization/Ext/Secondary phone field on 'Add Address form Addresses page' as a Registered user");
$t.start();
try
{
	for( var $i=0; $i<$Address_Validation.length ;$i++)
	{
		if(!_isVisible(_heading1("ADD ADDRESS")))
		{
			_click(_submit("/address-create/"));
		}
		
		addAddressValidation($Address_Validation,$i);
		
		if($i!=2)
		{
			_click(_submit("dwfrm_profile_address_create"));
		}
		
		if($i==0 || $i==1)
		{
			for(var $j=0;$j<8;$j++)
			{
				_assertEqual($Address_Validation[0][16],_style(_textbox("dwfrm_profile_address_"+$Address_Validation[$j][17]),"background-color"));
				//Error message are not yet displaying correctly
				_assertEqual($Address_Validation[$j][18], _getText(_span("error["+$j+"]")));
			}
		}
			
		else if($i==2)
		{
			_assertEqual($Address_Validation[0][15],_getText(_textbox("dwfrm_profile_address_email")).length);
			_assertEqual($Address_Validation[0][15],_getText(_textbox("dwfrm_profile_address_emailcomform")).length);
			_assertEqual($Address_Validation[3][15],_getText(_textbox("dwfrm_profile_address_firstname")).length);
			_assertEqual($Address_Validation[3][15],_getText(_textbox("dwfrm_profile_address_lastname")).length);
			_assertEqual($Address_Validation[4][15],_getText(_textbox("dwfrm_profile_address_organization")).length);
			_assertEqual($Address_Validation[5][15],_getText(_textbox("dwfrm_profile_address_address1")).length);
			_assertEqual($Address_Validation[5][15],_getText(_textbox("dwfrm_profile_address_address2")).length);
			_assertEqual($Address_Validation[5][15],_getText(_textbox("dwfrm_profile_address_city")).length);
			_assertEqual($Address_Validation[2][15],_getText(_textbox("dwfrm_profile_address_postal")).length);
			//_assertEqual($Address_Validation[2][15],_getText(_textbox("dwfrm_profile_address_phone")).length);
			_assertEqual($Address_Validation[6][15], _getValue(_textbox("dwfrm_profile_address_phone")).length);
			//_assertEqual($Address_Validation[0][15],_getText(_textbox("dwfrm_profile_address_phoneext")).length);
			_assertEqual("4", _getText(_textbox("dwfrm_profile_address_phoneext")).length);
			//_assertEqual($Address_Validation[0][15],_getText(_textbox("dwfrm_profile_address_phone2")).length);
			_assertEqual($Address_Validation[6][15], _getValue(_textbox("dwfrm_profile_address_phone2")).length);
			_wait(2000);
		}
		
		else if($i==3)
		{
			//Error for email address
			//_assert(false);
			_assertVisible(_span($Address_Validation[1][19]));
			_assertEqual($Address_Validation[0][19], _style(_div($Address_Validation[1][19]),"Color"));
			_assertVisible(_span($Address_Validation[2][19]));
			//_assertEqual($Address_Validation[13][6], _style(_div($Address_Validation[14][7]),"Color"));
			_assertEqual($Address_Validation[0][19], _style(_span("dwfrm_profile_address_emailcomform-error"), "color"));
			//Error for zipcode
			//_assert(false);
			_assertVisible(_span($Address_Validation[3][19]));
			//_assertEqual($Address_Validation[13][6], _style(span($Address_Validation[15][7]),"Color"));
			_assertEqual($Address_Validation[0][19], _style(_span("dwfrm_profile_address_postal-error"), "color"));

			//Error for phone number
			//_assert(false);
			_assertVisible(_span($Address_Validation[5][19]));
			//_assertEqual($Address_Validation[13][6], _style(_span($Address_Validation[16][7]),"Color"));
			_assertEqual($Address_Validation[0][19], _style(_span("dwfrm_profile_address_phone-error"), "color"));
			_wait(2000);

		}
		
		else if($i==4 || $i==5 || $i==6)
			{
				//Error for email address
				//_assert(false);
				
				//Error for zipcode
				//_assert(false);
				_assertVisible(_span($Address_Validation[3][19]));
				//_assertEqual($Address_Validation[13][6], _style(span($Address_Validation[15][7]),"Color"));
				_assertEqual($Address_Validation[0][19], _style(_span("dwfrm_profile_address_postal-error"), "color"));
	
				//Error for phone number
				//_assert(false);
				//_assertVisible(_span($Address_Validation[16][7]));
				_assertVisible(_span($Address_Validation[4][19]));

				//_assertEqual($Address_Validation[13][6], _style(_span($Address_Validation[16][7]),"Color"));
				_assertEqual($Address_Validation[0][19], _style(_span("dwfrm_profile_address_phone-error"), "color"));
				_wait(2000);
			}
		
		//Different combination of email
		else if($i==7)
		{
			//Error msg for confirm email
			//_assert(false);
			_assertVisible(_div($Address_Validation[1][19]));
			_assertEqual($Address_Validation[0][19], _style(_span("dwfrm_profile_address_email-error"), "color"));
			_wait(2000)

		}
		else if($i==8)
		{
		_assertVisible(_span($Address_Validation[1][19]));
		_assertEqual($Address_Validation[0][19], _style(_div($Address_Validation[1][19]),"Color"));
		_assertVisible(_span($Address_Validation[2][19]));
		//_assertEqual($Address_Validation[13][6], _style(_div($Address_Validation[14][7]),"Color"));
		_assertEqual($Address_Validation[0][19], _style(_span("dwfrm_profile_address_emailcomform-error"), "color")); 
		_wait(2000)
     	}
		//Valid
		else
		{

			if(_isVisible(_div("invalid-address js-shipping-address")))
				{
					_setValue(_textbox("dwfrm_profile_address_phone"),$Address_Validation[$i][12]);
					_setValue(_textbox("dwfrm_profile_address_phoneext"), $Address_Validation[$i][13]);
					_setValue(_textbox("dwfrm_profile_address_phone2"), $Address_Validation[$i][14]);
				_click(_link("Continue"));
				}
			_wait(5000)
			//Address in address landing
			_assertEqual("/"+$Address_Validation[9][17]+"/i", _getText(_listItem("address-tile ")));
		}
	}
}

catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219570/219571","Verify the validation related to Country/State drop down on 'Add Address form Addresses page' in application as a Registered user.");
$t.start();
try
{
	//Verify drop down values on page load
	_assertEqual("United States", _getSelectedText(_select("dwfrm_profile_address_country")));
	_assertEqual("Select...", _getSelectedText(_select("dwfrm_profile_address_states_state")));
	
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//Add address
//addAddressValidation($addr,0);
//_click(_submit("dwfrm_profile_address_create"));

var $t = _testcase("219579/219580/219581/219582/219585/219586/219587/219672/219675/219676/219681/219682/219562/219563/219564/219570","Verify the validation related to First Name/Last name/Address1/Address2/City/Zipcode/Mobile/email/confirm email/Organization/Ext/Secondary phone field on 'Edit Address form Addresses page' as a Registered user");
$t.start();
try
{
	for( var $i=0; $i<$Address_Validation.length ;$i++)
	{
		if(!_isVisible(_heading1("EDIT ADDRESS")))
		{
			_click(_link("address-edit"));
		}
		
		addAddressValidation($Address_Validation,$i);
		
		if($i!=2)
		{
			_click(_submit("dwfrm_profile_address_edit"));
		}
		
		if($i==0 || $i==1)
		{
			for(var $j=0;$j<8;$j++)
			{
				_assertEqual($Address_Validation[0][16],_style(_textbox("dwfrm_profile_address_"+$Address_Validation[$j][17]),"background-color"));
				//Error message are not yet displaying correctly
				_assertEqual($Address_Validation[$j][18], _getText(_span("error["+$j+"]")));
			}
		}
			
		else if($i==2)
		{
			_assertEqual($Address_Validation[0][15],_getText(_textbox("dwfrm_profile_address_email")).length);
			_assertEqual($Address_Validation[0][15],_getText(_textbox("dwfrm_profile_address_emailcomform")).length);
			_assertEqual($Address_Validation[3][15],_getText(_textbox("dwfrm_profile_address_firstname")).length);
			_assertEqual($Address_Validation[3][15],_getText(_textbox("dwfrm_profile_address_lastname")).length);
			_assertEqual($Address_Validation[4][15],_getText(_textbox("dwfrm_profile_address_organization")).length);
			_assertEqual($Address_Validation[5][15],_getText(_textbox("dwfrm_profile_address_address1")).length);
			_assertEqual($Address_Validation[5][15],_getText(_textbox("dwfrm_profile_address_address2")).length);
			_assertEqual($Address_Validation[5][15],_getText(_textbox("dwfrm_profile_address_city")).length);
			_assertEqual($Address_Validation[2][15],_getText(_textbox("dwfrm_profile_address_postal")).length);
			//_assertEqual($Address_Validation[2][15],_getText(_textbox("dwfrm_profile_address_phone")).length);
			_assertEqual($Address_Validation[6][15], _getValue(_textbox("dwfrm_profile_address_phone")).length);
			//_assertEqual($Address_Validation[0][15],_getText(_textbox("dwfrm_profile_address_phoneext")).length);
			_assertEqual("4", _getText(_textbox("dwfrm_profile_address_phoneext")).length);
			//_assertEqual($Address_Validation[0][15],_getText(_textbox("dwfrm_profile_address_phone2")).length);
			_assertEqual($Address_Validation[6][15], _getValue(_textbox("dwfrm_profile_address_phone2")).length);
			_wait(2000);
		}
		
		else if($i==3)
		{
			//Error for email address
			//_assert(false);
			_assertVisible(_span($Address_Validation[1][19]));
			_assertEqual($Address_Validation[0][19], _style(_div($Address_Validation[1][19]),"Color"));
			_assertVisible(_span($Address_Validation[2][19]));
			//_assertEqual($Address_Validation[13][6], _style(_div($Address_Validation[14][7]),"Color"));
			_assertEqual($Address_Validation[0][19], _style(_span("dwfrm_profile_address_emailcomform-error"), "color"));
			//Error for zipcode
			//_assert(false);
			_assertVisible(_span($Address_Validation[3][19]));
			//_assertEqual($Address_Validation[13][6], _style(span($Address_Validation[15][7]),"Color"));
			_assertEqual($Address_Validation[0][19], _style(_span("dwfrm_profile_address_postal-error"), "color"));

			//Error for phone number
			//_assert(false);
			_assertVisible(_span($Address_Validation[5][19]));
			//_assertEqual($Address_Validation[13][6], _style(_span($Address_Validation[16][7]),"Color"));
			_assertEqual($Address_Validation[0][19], _style(_span("dwfrm_profile_address_phone-error"), "color"));
			_wait(2000);

		}
		
		else if($i==4 || $i==5 || $i==6)
			{
			//Error for zipcode
			//_assert(false);
			_assertVisible(_span($Address_Validation[3][19]));
			//_assertEqual($Address_Validation[13][6], _style(span($Address_Validation[15][7]),"Color"));
			_assertEqual($Address_Validation[0][19], _style(_span("dwfrm_profile_address_postal-error"), "color"));

			//Error for phone number
			//_assert(false);
			//_assertVisible(_span($Address_Validation[16][7]));
			_assertVisible(_span($Address_Validation[4][19]));

			//_assertEqual($Address_Validation[13][6], _style(_span($Address_Validation[16][7]),"Color"));
			_assertEqual($Address_Validation[0][19], _style(_span("dwfrm_profile_address_phone-error"), "color"));
			_wait(2000);
			
			}
		
		//Different combination of email
		else if($i==7)
		{
			//Error msg for confirm email
			//_assert(false);
			_assertVisible(_div($Address_Validation[1][19]));
			_assertEqual($Address_Validation[0][19], _style(_span("dwfrm_profile_address_email-error"), "color"));
			_wait(2000)

		}
		else if($i==8)
		{
			_assertVisible(_span($Address_Validation[1][19]));
			_assertEqual($Address_Validation[0][19], _style(_div($Address_Validation[1][19]),"Color"));
			_assertVisible(_span($Address_Validation[2][19]));
			//_assertEqual($Address_Validation[13][6], _style(_div($Address_Validation[14][7]),"Color"));
			_assertEqual($Address_Validation[0][19], _style(_span("dwfrm_profile_address_emailcomform-error"), "color")); 
			_wait(2000) 
     	}
		//Valid
		else
		{
			if(_isVisible(_div("invalid-address js-shipping-address")))
			{
				_setValue(_textbox("dwfrm_profile_address_phone"),$Address_Validation[$i][12]);
				_setValue(_textbox("dwfrm_profile_address_phoneext"), $Address_Validation[$i][13]);
				_setValue(_textbox("dwfrm_profile_address_phone2"), $Address_Validation[$i][14]);
			_click(_link("Continue"));
			}
		_wait(5000)
		//Address in address landing
		_assertEqual("/"+$Address_Validation[9][17]+"/i", _getText(_listItem("address-tile ")));
		}
	}
}

catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219561","Verify the functionality related to 'Cancel' button 'Edit Address form Addresses page' in application as a Registered user.");
$t.start();
try
{
	_click(_link("address-edit"));
	_assertVisible(_div("addressform"));
	_click(_link("Cancel"));
	_assertNotVisible(_heading1("ADD ADDRESS"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219565","Verify the UI of 'Delete link' on My Account Addresses page' in application as a Registered user");
$t.start();
try
{
	_assertContainsText("Delete", _listItem("address-tile  default"));
	_assertContainsText("Edit", _listItem("address-tile  default"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219628","Verify the UI of Address Book in the Address Book Page");
$t.start();
try
{
	//Address name
	_assertVisible(_div("mini-address-name", _in(_listItem("address-tile "))));
	//Address
	_assertVisible(_div("mini-address-location", _in(_listItem("address-tile "))));
	//Edit link
	_assertVisible(_link("address-edit", _in(_listItem("address-tile "))));
	//Delete
	_assertVisible(_link("/address-delete/", _in(_listItem("address-tile "))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219627/219638/219548","Verify the UI of Account/ UI of 'My Account Addresses page' in the Address Book Page");
$t.start();
try
{
	//heading
	_assertVisible(_heading1("ADDRESS BOOK"));
	var $accno=_extract(_getText(_heading2("desktop-show")),"/#(.*)/",true).toString();
	_assertEqual("Account:#"+$accno, _getText(_heading2("desktop-show")));
	_mouseOver(_link("tooltip", _in(_listItem("address-tile  default"))));
	_assertEqual($address_data[0][3],_getText(_div("ui-tooltip-content")));
	//Add new address button
	_assertVisible(_submit("/address-create/"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("219665","Verify the UI of 'Edit Address form Addresses page' in application as a Registered user.");
$t.start();
try
{
	//Delete address
	deleteAddress();
	_wait(5000);
	_click(_link("Add new"));
	//Create a address
	addAddress($addr,0);
	_click(_submit("apply-button "));
	if(_isVisible(_div("invalid-address js-shipping-address")))
	{
		 _setValue(_textbox("dwfrm_profile_address_phone"),$addr[0][11]);
	_click(_link("Continue"));
	}
_wait(5000)
	
	_click(_link("address-edit"));
	//Email
	_assertVisible(_span("Email"));
	_assertVisible(_textbox("dwfrm_profile_address_email"));
	//Confirm email
	_assertVisible(_span("Confirm Email"));
	_assertVisible(_textbox("dwfrm_profile_address_emailcomform"));
	//First name
	_assertVisible(_span("First Name"));
	_assertVisible(_textbox("dwfrm_profile_address_firstname"));
	//Last name
	_assertVisible(_span("Last Name"));
	_assertVisible(_textbox("dwfrm_profile_address_lastname"));
	//Organisation
	_assertVisible(_span("Organization"));
	_assertVisible(_textbox("dwfrm_profile_address_organization"));
	//Address state
	_assertVisible(_span("Address Street"));
	_assertVisible(_textbox("dwfrm_profile_address_address1"));
	//Address 2
	_assertVisible(_span("Suite/Apt."));
	_assertVisible(_textbox("dwfrm_profile_address_address2"));
	//Zip
	_assertVisible(_span("Postal Code"));
	_assertVisible(_textbox("dwfrm_profile_address_postal"));
	//city
	_assertVisible(_span("City"));
	_assertVisible(_textbox("dwfrm_profile_address_city"));
	//Country
	_assertVisible(_span("Country"));
	_assertVisible(_select("dwfrm_profile_address_country"));
	//state
	_assertVisible(_span("State"));
	_assertVisible(_select("dwfrm_profile_address_states_state"));
	//Mobile
	_assertVisible(_span("Mobile/Day Phone"));
	_assertVisible(_textbox("dwfrm_profile_address_phone"));
	//Ext
	_assertVisible(_span("Ext."));
	_assertVisible(_textbox("dwfrm_profile_address_phoneext"));
	//Secondary phone
	_assertVisible(_span("Secondary Phone"));
	_assertVisible(_textbox("dwfrm_profile_address_phone2"));
	//Required text
	_assertVisible(_div("dialog-required"));
	//save button
	_assertVisible(_submit("dwfrm_profile_address_edit"));
	//cancel link
	_assertVisible(_link("Cancel"));
	
	
	
//	//prepopulated values
//	//email
//	_assertEqual($addr[0][1], _getValue(_textbox("dwfrm_profile_address_email")));
//	//FN
//	_assertEqual($addr[0][3], _getValue(_textbox("dwfrm_profile_address_firstname")));
//	//LN
//	_assertEqual($addr[0][4], _getValue(_textbox("dwfrm_profile_address_lastname")));
//	//Address1
//	_assertEqual($addr[0][5], _getValue(_textbox("dwfrm_profile_address_address1")));
//	//Zipcode
//	_assertEqual($addr[0][10], _getValue(_textbox("dwfrm_profile_address_postal")));
//	//city
//	_assertEqual($addr[0][9], _getValue(_textbox("dwfrm_profile_address_city")));
//	//Country
//	_assertEqual($addr[0][7], _getSelectedText(_select("dwfrm_profile_address_country")));
//	//state
//	_assertEqual($addr[0][8], _getSelectedText(_select("dwfrm_profile_address_states_state")));
//	//Phone
//	_assertEqual($addr[0][11], _getValue(_textbox("dwfrm_profile_address_phone")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


/*var $t = _testcase("","");
$t.start();
try
{



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/