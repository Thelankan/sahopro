_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("EditAccount.xls");

var $profile_data=_readExcelFile("EditAccount.xls","Data");
var $profile_validation=_readExcelFile("EditAccount.xls","profile_validation");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

if (isMobile())
{
 _click(_link("/mobile-show/", _in(_div("user-info"))));
}
else
	{
	 _click(_link("/desktop-show/", _in(_div("user-info"))));
	 //Visibility of overlay
	 _assertVisible(_div("dialog-container"));
	}
login();
_wait(5000,_isVisible(_link("/user-logout/")));


var $t = _testcase("219241/219247","Verify the UI of SETTINGS AND PASSWORD page");
$t.start();
try
{
	NavigatetoAccountLanding();
	//Click on settings and password
	_click(_link($profile_data[0][7]));
	var $accno=_extract(_getText(_heading2("desktop-show")),"/#(.*)/",true).toString();
	//breadcrumb
	_assertEqual("/"+$profile_data[0][8]+"/", _getText(_div("breadcrumb")));
	//Heading
	_assertVisible(_heading1($profile_data[0][7]));
	_assertEqual("Account:#"+$accno, _getText(_heading2("desktop-show")));
	//email
	_assertVisible(_span("Email"));
	_assertVisible(_textbox("dwfrm_profile_customer_email"));
	//Confirm email
	_assertVisible(_span("Confirm Email"));
	_assertVisible(_textbox("/dwfrm_profile_customer_emailconfirm/"));
	//First name
	_assertVisible(_span("First Name"));
	_assertVisible(_textbox("dwfrm_profile_customer_firstname"));
	//Last name
	_assertVisible(_span("Last Name"));
	_assertVisible(_textbox("dwfrm_profile_customer_lastname"));
	//Reset password heading
	_assertVisible(_heading2("RESET PASSWORD"));
	//Current password
	_assertVisible(_span("Current Password"));
	_assertVisible(_password("/dwfrm_profile_login_currentpassword/"));
	//New password
	_assertVisible(_span("New Password"));
	_assertVisible(_password("/dwfrm_profile_login_resetpassword/"));
	//Password rule content asset
	_assertVisible(_div("passrules"));
	//Confirm password
	_assertVisible(_span("Confirm Password"));
	_assertVisible(_password("/dwfrm_profile_login_confirmresetpassword/"));
	//Email preference heading
	_assertVisible(_heading2("SET EMAIL PREFERENCES"));
	//checkbox
	_assertVisible(_checkbox("dwfrm_profile_customer_addtoemaillist"));
	_assertTrue(_checkbox("dwfrm_profile_customer_addtoemaillist").checked);
	//text
	_assertVisible(_heading4("Sign up for exclusive offers!"));
	_assertVisible(_heading5("Uncheck To Unsubcribe"));
	//Submit button
	_assertVisible(_submit("dwfrm_profile_confirm"));
	//cancel
	_assertVisible(_link("Cancel"));
	//Privacy policy
	_assertVisible(_span("Crown Awards will never share your information. View our"));
	_assertVisible(_link("privacy-policy"));
	//Promotional content asset
	_assertVisible(_div("email-asset "));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219250/219251/219255/219256/219257/219248/219249/219239","Verify the Validation for First Name/Last name/Current Password/new Password/Confirm Password/Email/Confirm Email text field in page");
$t.start();
try
{
	for(var $i=0;$i<$profile_validation.length;$i++)
	{
		_setValue(_textbox("dwfrm_profile_customer_email"), $profile_validation[$i][1]);
		_setValue(_textbox("/dwfrm_profile_customer_emailconfirm/"), $profile_validation[$i][2]);
		_setValue(_textbox("dwfrm_profile_customer_firstname"), $profile_validation[$i][3]);
		_setValue(_textbox("dwfrm_profile_customer_lastname"), $profile_validation[$i][4]);
		_setValue(_password("/dwfrm_profile_login_currentpassword/"), $profile_validation[$i][5]);
		_setValue(_password("/dwfrm_profile_login_resetpassword/"), $profile_validation[$i][6]);
		_setValue(_password("/dwfrm_profile_login_confirmresetpassword/"), $profile_validation[$i][7]);
		
		if($i==0)
		{
			_click(_submit("dwfrm_profile_confirm"));
			//background color of each field
			_assertEqual($profile_validation[0][8],_style(_textbox("dwfrm_profile_customer_email"),"background-color"));
			_assertEqual($profile_validation[0][8],_style(_textbox("/dwfrm_profile_customer_emailconfirm/"),"background-color"));
			_assertEqual($profile_validation[0][8],_style(_textbox("dwfrm_profile_customer_firstname"),"background-color"));
			_assertEqual($profile_validation[0][8],_style(_textbox("dwfrm_profile_customer_lastname"),"background-color"));
			_assertEqual($profile_validation[0][8],_style(_password("/dwfrm_profile_login_resetpassword/"),"background-color"));
			_assertEqual($profile_validation[0][8],_style(_password("/dwfrm_profile_login_confirmresetpassword/"),"background-color"));
			
			//error message
			for(var $i=0;$i<6;$i++)
			{
				_assertEqual($profile_validation[$i][9], _getText(_span("error["+$i+"]")));
			}
		}
		
		else if($i==1)
		{
			_assertEqual($profile_validation[0][10],_getText(_textbox("dwfrm_profile_customer_email")).length);
			_assertEqual($profile_validation[0][10],_getText(_textbox("/dwfrm_profile_customer_emailconfirm/")).length);
			_assertEqual($profile_validation[0][10],_getText(_textbox("dwfrm_profile_customer_firstname")).length);
			_assertEqual($profile_validation[0][10],_getText(_textbox("dwfrm_profile_customer_lastname")).length);
			_assertEqual($profile_validation[1][10],_getText(_password("/dwfrm_profile_login_currentpassword/")).length);
			_assertEqual($profile_validation[1][10],_getText(_password("/dwfrm_profile_login_resetpassword/")).length);
			_assertEqual($profile_validation[1][10],_getText(_password("/dwfrm_profile_login_confirmresetpassword/")).length);
		}
		
		else if($i==2 || $i==3 || $i==4 ||$i==5)
		{
			_click(_submit("dwfrm_profile_confirm"));
			_assertEqual($profile_validation[6][9], _getText(_span("dwfrm_profile_customer_email-error")));
			_assertEqual($profile_validation[6][9], _getText(_span("dwfrm_profile_customer_emailconfirm-error")));
			//background color
			_assertEqual($profile_validation[0][8],_style(_textbox("dwfrm_profile_customer_email"),"background-color"));
			_assertEqual($profile_validation[0][8],_style(_textbox("/dwfrm_profile_customer_emailconfirm/"),"background-color"));
		}
		
		else if($i==6)
		{
			_assertEqual($profile_validation[7][9], _getText(_span("/dwfrm_profile_login_password/")));
			_assertEqual($profile_validation[7][9], _getText(_span("/dwfrm_profile_login_passwordconfirm/")));
			//background color
			_assertEqual($profile_validation[0][8],_style(_password("/dwfrm_profile_login_resetpassword/"),"background-color"));
			_assertEqual($profile_validation[0][8],_style(_password("/dwfrm_profile_login_confirmresetpassword/"),"background-color"));
		}
		
		else if($i==7)
		{
			_assertVisible(_div($profile_validation[8][9]));
		}
		
		else if($i==8)
		{
			_assert(false);
		}
		
		else
		{
			_assertVisible(_div($profile_validation[9][9]));
		}
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("221389","Functionality of Privacy Policy link in My account profile.");
$t.start();
try
{
	//Privacy policy link
	_click(_link("Privacy Policy"));
	//Privacy policy overlay
	_assertVisible(_div("This is Privacy Policy page"));
	_assert(false);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase("","");
$t.start();
try
{
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/