_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Account_Landing.xls");

var $accountdata=_readExcelFile("Account_Landing.xls","Data");


SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


//login to the application
if (isMobile())
{
 _click(_link("/mobile-show/", _in(_div("user-info"))));
}
else
	{
	 _click(_link("/desktop-show/", _in(_div("user-info"))));
	 //Visibility of overlay
	 _assertVisible(_div("dialog-container"));
	}

login();

_wait(5000,_isVisible(_link("/user-logout/")));

var $t = _testcase("218568","Verify the UI of Account Landing Page");
$t.start();
try
{
	
////click on your orders button	
//_click(_link("Your Orders"));
////navigate to my account page
//_click(_link("My Account", _in(_div("breadcrumb"))));
NavigatetoAccountLanding();
_wait(5000);
//UI of account landing page
_assertVisible(_link($accountdata[0][0]));
_assertVisible(_link($accountdata[1][0]));
_assertVisible(_link($accountdata[2][0]));
_assertVisible(_link($accountdata[3][0]));
_assertVisible(_link($accountdata[4][0]));
_assertVisible(_link($accountdata[5][0]));
_assertVisible(_heading2($accountdata[6][0]));

var $accountheadings=_count("_link","/(.*)/",_in(_div("account-page")));

if (_isVisible(_heading2("accountemail")))
	{
	$accountheadings+1;
	}

for (var $i=0;$i<$accountheadings;$i++)
	{
	_assertVisible(_image("icon-youtube.png["+$i+"]"));
	}


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("218779","Verify the UI of SETTINGS &amp; PASSWORD link");
$t.start();
try
{

_assertVisible(_link($accountdata[0][0]));
//UI of settings and password page
_assertVisible(_link($accountdata[0][2]));
_assertVisible(_listItem($accountdata[1][2]));
_assertVisible(_listItem($accountdata[2][2]));
_assertVisible(_listItem($accountdata[3][2]));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("218780","Verify the UI of ADDRESSES link");
$t.start();
try
{

_assertVisible(_link($accountdata[1][0]));
//UI of ADDRESSES link
_assertVisible(_listItem($accountdata[0][3]));
_assertVisible(_listItem($accountdata[1][3]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("218781","Verify the UI of MAKE A PAYMENT link");
$t.start();
try
{

_assertVisible(_link($accountdata[2][0]));
//UI of make a payment link
_assertVisible(_listItem($accountdata[0][4]));
_assertVisible(_listItem($accountdata[1][4]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("218782","Verify the UI of ORDERS link");
$t.start();
try
{
	
_assertVisible(_link($accountdata[4][0]));
//UI of orders link
_assertVisible(_listItem("Order History", _in(_div("address-order"))));
_assertVisible(_listItem("Order Details", _in(_div("address-order"))));
_assertVisible(_listItem("Track Orders", _in(_div("address-order"))));
_assertVisible(_listItem("Make a Payment", _in(_div("address-order"))));
_assertVisible(_listItem("Print an Invoice", _in(_div("address-order"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("218783","Verify the UI of PAYMENT SETTINGS link");
$t.start();
try
{
	
_assertVisible(_link($accountdata[5][0]));
//UI of payment settings link
_assertVisible(_listItem($accountdata[2][4]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("218867/218868/218869","Verify the UI of SIGN UP FOR EMAIL link/Verify the UI of SIGN UP FOR EMAIL formfield for guest user");
$t.start();
try
{

_assertVisible(_heading2($accountdata[6][0]));
_assertVisible(_listItem("Receive Exclusive Offers"));
//click on sign up for email form field
_click(_heading2($accountdata[6][0]));
_assertVisible(_span("/Email/"));
_assertVisible(_textbox("dwfrm_emailsignup_accountlanding"));
_assertVisible(_submit("/(.*)/",_in(_div("emailsignup hide"))));
_setValue(_textbox("dwfrm_emailsignup_accountlanding"),$accountdata[1][1]);
_click(_submit("signupbtn"));
_assertEqual($accountdata[0][1], _getText(_div("sucess-mesg")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

