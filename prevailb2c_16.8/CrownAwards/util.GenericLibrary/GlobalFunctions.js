_include("BrowserSpecific.js");
_include("BM_Functions.js");
_resource("User_Credentials.xls");


var $pName;
var $quantity;
var $price;
var $shippingTax;
var $tax;
var $subTotal;
var $orderTotal;
var $actOrderTotal;
var $URL=_readExcelFile("User_Credentials.xls","URL");
var $userLog=_readExcelFile("User_Credentials.xls","BM_Login");
var $Run=_readExcelFile("User_Credentials.xls","Run");
var $eventdate=_readExcelFile("User_Credentials.xls","eventdate");
var $ProductName;
var $ProductPrice;
var $ProductNumber;
var $QTY;
var $totalpriceinPDP;
var $checkbox;
var $prodname;
var $prodquantity;
var $totalprice;
var $totalpriceBeforeselectingQuantity;

var $date=Date().split(" ")[4];
var $dateNow=$date.replace(":","").replace(":","");
var $datenowEmail="generic"+"_"+$dateNow+"@gmail.com";



function SiteURLs()
{
	_navigateTo($URL[0][0]);
}

function ClearCookies()
{
       _navigateTo($URL[0][0]+"/_s_/dyn/Cookies_showAll");
       _check(_checkbox("_sahi_chooseAll"));
       _click(_button("Delete")); 
       SiteURLs();
}


function takescreenshot()
{
	  _lockWindow(5000);
	  _focusWindow();
	  _takePageScreenShot();
	  _unlockWindow();	
}

//Function take a screen shot when a their is a script failure

function onScriptError()
{
  _log("Error log"); 
  _lockWindow(5000);
  _focusWindow();
  _takePageScreenShot();
  _unlockWindow();
}

function onScriptFailure()
{
  _log("Failure log");
  _lockWindow(5000);
  _focusWindow();
  _takePageScreenShot();
  _unlockWindow();
} 

function NavigatetoHomepage()
{
	_click(_link("Crown Awards"));
}

function NavigatetoAccountLanding()
{
//	//click on your orders button	
//	_click(_link("Your Orders"));
//	//navigate to my account page
//	_click(_link("My Account", _in(_div("breadcrumb"))));
	_navigateTo("https://development-webstore-crownawards.demandware.net/on/demandware.store/Sites-MAI-Site/default/Account-Show");
}

function cleanup()
 {
	//Clearing the items from cart
	ClearCartItems();
	logout();
	NavigatetoHomepage();
}

function logout()
{
	
//click on logout link in header
if (_isVisible(_link("/user-logout/")))
	{
	_click(_link("/user-logout/"));
	}
//log out link should not be visible
_assertNotVisible(_link("/user-logout/"));

}


 function ClearCartItems()
 {	 
		//navigate to cart
		_click(_link("/mini-cart-link/"));
		//clearing cart
		if (_isVisible(_link("Empty cart")))
			{
			//Empty cart button
			_click(_link("Empty cart"));
			}
		
		//ok in modal overlay
		if(_isVisible(_div("dialog-container")))
		{
			_click(_submit("button-text "));
		}
 }
 
 function login()
 {
	 
 _setValue(_textbox("/dwfrm_login_username/"), $uId);
 _setValue(_password("/dwfrm_login_password/"), $pwd);
 
    if (_isVisible(_submit("/dwfrm_login/", _in(_div("inner-container")))))
    {
      _click(_submit("/dwfrm_login/", _in(_div("inner-container"))));
    }
   else
    {
        _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
    }
 }
 
function contactusDetails($sheet,$rowno)
{
	
//entering valid values in all the fields
_setValue(_textbox("dwfrm_contactus_email"),$sheet[$rowno][1]);
_setValue(_textbox("dwfrm_contactus_firstname"),$sheet[$rowno][2]);
_setValue(_textbox("dwfrm_contactus_lastname"),$sheet[$rowno][3]);
_setValue(_textbox("dwfrm_contactus_phone"),$sheet[$rowno][4]);
_setSelected(_select("dwfrm_contactus_yourissues"),$sheet[$rowno][5]);
_setValue(_textbox("dwfrm_contactus_ordernumbetoproof"),$sheet[$rowno][1]);
	
}
 
 function headerUI($sheet,$user)
 {
 	//Application logo
 	_assertVisible(_link($sheet[0][0], _in(_div("primary-logo"))));
 	 
 if ($user==$sheet[1][1])
 	 {
 	//login link 
	 //_assertVisible(_listItem("Hi "+$User[$user][1]+" (Log Out)"));
	 _assertVisible(_listItem("Log In Log In Hi "+$User[1][1]+" ( Log Out )"));
 	 _assertVisible(_link("/user-logout/"));
 	 }
 	 else
 		 {
	 		 //login link 
	 		 _assertVisible(_link("/desktop-show/", _in(_div("user-info"))));
	 		 //order history link
	 		 _assertVisible(_link("/order-history/"));
 		 }
 	 //payment link
    _assertVisible(_link("/paymentlink menuitems/"));
	 //chat link
	 _assertVisible(_link("Chat"));
	 //help link
	 _assertVisible(_link("/help-link/"));
	 //mini cart link
	 _assertVisible(_link("/mini-cart-link/"));
	 //checking visibility of number of products 
	 _assertVisible(_span("label", _in(_span("/minicart-quantity/"))));
	 _assertVisible(_span("/minicart-quantity/"));
	 //Price visibility checking
	 _assertVisible(_span("value", _in(_link("/mini-cart-link/"))));
	 //Checkout button
	 _assertVisible(_link("/mini-cart-link-checkout-link/"));
	 //Search bar section
	 _assertVisible(_textbox("/Search/"));
	 _assertVisible(_submit("Search"));
 	 // Sport of Activity Drop down
 	 _assertVisible(_link($sheet[1][0], _in(_div("dropdownul"))));
 	 //mega menu with 19 categories
 	 _assertEqual($sheet[3][2],_count("_link","/menuitems/", _in(_list("menu-category level-1"))));

 }
  
 function deleteAddress()
 {
	// NavigatetoAccountLanding();
	 _wait(4000);
	 _click(_link("Your Orders"));
	 _click(_link("My Account", _in(_div("main-content"))));
	 _click(_link("ADDRESSES"));
	 //deleting the existing addresses
	   while(_isVisible(_link("Delete")))
	   	{
	   	  _click(_link("Delete"));
	   	  _expectConfirm("/Do you want/",true);
	   	}
 }
 function clearWishList()
 {
	 _click(_link("user-account"));
	 _click(_link("Wish List"));
	 if (_isVisible(_select("editAddress")))
	 {
	 _setSelected(_select("editAddress"),0);	 
	 }
	 while(_isVisible(_submit("Remove")))
	 {
	  	  _click(_submit("Remove"));
	 } 
 }

 function addAddress($validations,$r)
 {
	 _setValue(_textbox("dwfrm_profile_address_email"),$validations[$r][1]);
	 _setValue(_textbox("dwfrm_profile_address_emailcomform"),$validations[$r][2]);
	 _setValue(_textbox("dwfrm_profile_address_firstname"), $validations[$r][3]);
	 _setValue(_textbox("dwfrm_profile_address_lastname"), $validations[$r][4]);
	 _setValue(_textbox("dwfrm_profile_address_address1"), $validations[$r][5]);
	 _setValue(_textbox("dwfrm_profile_address_address2"), $validations[$r][6]);
	 _setValue(_textbox("dwfrm_profile_address_postal"), $validations[$r][10]);
	 _setSelected(_select("dwfrm_profile_address_country"),$validations[$r][7]);
	 _setSelected(_select("dwfrm_profile_address_states_state"), $validations[$r][8]);
	 _setValue(_textbox("dwfrm_profile_address_city"), $validations[$r][9]);
	 _setValue(_textbox("dwfrm_profile_address_phone"),$validations[$r][11]);

 }
 

//credit card info
function CreateCreditCard($sheet,$rowno)
{
	
	_setValue(_textbox("/dwfrm_paymentinstruments_creditcards_newcreditcard_number/"),$sheet[$rowno][1]);
	_setSelected(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_month"),$sheet[$rowno][2]);
	_setSelected(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_year"),$sheet[$rowno][3]);
	
}


function closedOverlayVerification()
{
               _assertNotVisible(_div("ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable"));
               //heading
               _assertVisible(_heading1($Payment_Data[0][3]));
               _assertVisible(_div("/"+$Payment_Data[1][3]+"/"));
               _assertVisible(_link("Add a Credit Card"));           
}


function DeleteCreditCard()
{
               
//				//verifying the address in account
//				//NavigatetoAccountLanding();
//	_wait(4000);
//	_click(_link("Your Orders"));
//	_click(_link("My Account", _in(_div("main-content"))));
//	_click(_link("PAYMENT SETTINGS"));
//               //click on delete card link
//			      while(_isVisible(_submit("/Delete/")))
//	                  {
//	                  _click(_submit("/Delete/"));
//	                  }
//
////click on ok in confirmation overlay
//_expectConfirm("/Do you want/",true);
////verify whether deleted or not
//_assertNotVisible(_list("payment-list"));
//_assertNotVisible(_submit("/Delete/"));

	
	//click on delete card link
	_click(_link("Your Orders"));
	_click(_link("My Account", _in(_div("main-content"))));
	_click(_link("PAYMENT SETTINGS"));
	_wait(3000);	  

          if(_isVisible(_list("payment-list")))
			{
        	    var $Totalno_Payment =_count("_listItem","/payment-address/", _in(_list("payment-list")))
        		 _log("TOTAL ADDRESS ="+$Totalno_Payment);
        	  
        	   //deleting the existing payment methods
				for(var $i=0; $i<$Totalno_Payment; $i++)
						{
							_click(_submit("/DELETE/"));
							if(_isVisible(_div("/payment-delete-confirm/")))
							{
								_click(_submit("/DELETE/"));
								_wait(3000);
							}
				
			             }
			
			}
          
			else
			{
				_assertNotVisible(_list("payment-list"))
				_assertNotVisible(_submit("/DELETE/"));
			}

	

}
 

 function createAccount()
 {
	 _setValue(_textbox("dwfrm_profile_customer_firstname"), $userData[$user][1]);
	 _setValue(_textbox("dwfrm_profile_customer_lastname"), $userData[$user][2]);
	 _setValue(_textbox("dwfrm_profile_customer_email"), $userData[$user][3]);
	 _setValue(_textbox("dwfrm_profile_customer_emailconfirm"), $userData[$user][4]);
	 _setValue(_password("/dwfrm_profile_login_password/"), $userData[$user][5]);
	 _setValue(_password("/dwfrm_profile_login_passwordconfirm/"), $userData[$user][6]);
	 _click(_submit("dwfrm_profile_confirm"));
 }

 var $UserID=$userLog[0][0];
 var $Password=$userLog[0][1];


 
 function selectSwatch()
 {
	 
	 if (!isMobile())
	 {
	 //$checkbox=_collectAttributes("_div","box itemnum","productid",_in(_div("/ItemTableWrap/")));
	 $checkbox=_collectAttributes("_div", "box itemnum", "productid", _in(_div("product-content")));

	 _wait(2000);
	_check(_checkbox("/prdSelect"+$checkbox[0]+"/"));

	//_assertVisible(_div("/prodwrapper mobile-hide/"));
	_assertVisible(_div("prodwrapper"));

	_wait(4000);
	var $selections=_count("_div","/prodStepText/",_in(_div("prodwrapper")))-1;
	//var $selections=_count("_div", "/prodStepText/", _in(_div("/prodwrapper mobile-hide/")))-1;


	var $dropdown=0;
	var $flexdetails=0;

	for (var $i=0;$i<$selections;$i++)
	{
		
		if (_isVisible(_select("flexdetails["+$dropdown+"]")))
			{
				_setSelected(_select("flexdetails["+$dropdown+"]"),1);
				$dropdown++;
				_wait(3000);
			}
		else if (_isVisible(_radio("/radio/",_in(_div("flexdetails["+$flexdetails+"]",_in(_div("product-content")))))))
			{
			var $radio=_collectAttributes("_radio","/radio/","id",_in(_div("flexdetails["+$flexdetails+"]")));
			_click(_radio($radio[0],_in(_div("flexdetails["+$flexdetails+"]"))));
			$flexdetails++;
			}
		else if (_isVisible(_checkbox("/check/",_in(_div("flexdetails["+$flexdetails+"]",_in(_div("product-content")))))))
			{
			var $checkbox=_collectAttributes("_checkbox","/check/","id",_in(_div("flexdetails["+$flexdetails+"]")));
			_check(_checkbox($checkbox[0],_in(_div("flexdetails["+$flexdetails+"]"))));
			$flexdetails++;
			}
		
		if (_isVisible(_div("uploadPopup")))
		{
		_click(_button("Close",_near(_div("uploadPopup"))))
		}
				
		while (_isVisible(_div("dialog-container")))
		{
		  _click(_button("Close",_near(_div("dialog-container"))));
		}

	}

	$totalpriceinPDP=_extract(_getText(_div("/Total:/")),"/[$](.*)/",true);
	 }
	 
	 else
		 {
		 
		 var $p=0;
		 
				for (var $i=0;$i<10;$i++)
				{
					
				 if (_isVisible(_div("formfield",_in(_div("prodformstyle["+$p+"]")))))
					 {
					 _setSelected(_select($i),1);
					 _wait(5000);
					 $p++;
					 }
				 else
					 {
					 break;
					 }
				 
//				 if (_isVisible(_div("Step "+$i+":")));
//				 {
//					 _setSelected(_select($p),1);
//					 _wait(5000);
//					 $p++;
//				 }
				 
					if (_isVisible(_div("uploadPopup")))
					 {
					_click(_button("Close",_near(_div("uploadPopup"))))
					 }
						
					while (_isVisible(_div("dialog-container")))
					 {
					_click(_button("Close",_near(_div("dialog-container"))));
					 }
					
				 }
				
				$totalpriceinPDP=_extract(_getText(_div("/Total:/")),"/[$](.*)/",true);
				
			}
			
 }
 
 //search related function
function search($product)
{

_setValue(_textbox("/Search/"),$product);
_click(_submit("Search"));

}

 //cart related functions

function addItemToCart($subCat,$quantity)
{
	
//add product to cart
search($subCat);
//selecting product if it navigates to search result page
if (_isVisible(_div("search-result-content")))
	{
	_click(_link("/(.*)/", _in(_listItem("grid-tile new-row"))))
	}

//selecting swatches
selectSwatch();
$totalpriceBeforeselectingQuantity=_extract(_getText(_div("/Total:/")),"/[$](.*)/",true);
//selecting quantity
_setValue(_numberbox("Quantity"),$quantity);
_wait(5000);
$prodname=_getText(_heading2("product-name"));
$prodquantity=_getText(_numberbox("/(.*)/"));
$totalprice=_extract(_getText(_div("/Total:/")),"/[$](.*)/",true);
_log($totalprice);	
//click on add to cart button
_click(_submit("/sprite-addtocart/"));

while (_isVisible(_div("/sold-out-info/")))
	{
	_wait(3000);
	_click(_link("alternate-product-images",_in(_div("/sold-out-info/"))));
	//click on add to cart button
	_click(_submit("/sprite-addtocart/"));
	}

engraving();

}

function navigateTOIL($excel)
{
	//navigate to cart page
	navigateToCart($excel[0][0],$excel[0][1]);
	//click on go straight to checkout to navigate to IL Page
	_click(_link("mini-cart-link-checkout"));
}

function navigateToCart($subCat,$i)
{
	//add item to cart
	addItemToCart($subCat,$i);
	//navigate to Cart page
	_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));
	$pName=_getText(_link("/(.*)/", _in(_listItem("name"))));
	$quantity=_getText(_div("item-quantity box"));
	$orderTotal=_extract(_getText(_div("order-value")),"/[$](.*)/",true);
}


function engraving()
{

if (_isVisible(_div("headerContainer")))

	{

if ((_isVisible(_div("itemPreviewContainerAdvanced"))) || (_isVisible(_div("container itemInputContainerAdvanced"))))
	{
	
	_setSelected(_select("contentSelect_1_1"), "0");
	_setValue(_textbox("lineInputAdvanced"),$userLog[0][3]);
	
	}

	else
		{
		
		_setValue(_textbox("lineInput"),$userLog[0][3]);
	
		}

if (_isVisible(_div("Repeat On All")))
{
	_click(_checkbox("lineCheckbox"));
}

_check(_checkbox("confirmCheckbox"));
_click(_link("atc_button"));

if (_isVisible(_div("tie-In")))
	{
	_click(_button("Close"));
	}

	}

}

//Old no need to release
/*function engraving()
{

if (_isVisible(_div("headerContainer")))

	{
	
if (_isVisible(_div("itemPreviewContainerAdvanced")))
	{
	
	var $count=_count("_textbox","lineInputAdvanced",_in(_div("NDSColumnContainer")));

var $q=1;	
for (var $i=1;$i<=$count;$i++)
	{
	
	if ($i==1)
		{
		_setSelected(_select("contentSelect_1_"+$i+""), "Ornament");
		}
		else
			{
			_setSelected(_select("contentSelect_1_"+$i+""), "Text");
			_setValue(_textbox("lineInputAdvanced["+$q+"]"),$userLog[0][3]);
			_wait(1000)
			if (_isVisible(_div("Repeat On All")))
			{
				_check(_checkbox("lineCheckboxAdvanced["+$q+"]"));
//				if (!_checkbox("lineCheckboxAdvanced["+$q+"]").checked)
//					{
//					_check(_checkbox("lineCheckboxAdvanced["+$q+"]"));
//					}
			}
			
			$q++;
			
			}
	}

	}
else
	{
	
	var $count=_count("_textbox","/lineInput/",_in(_div("NDSColumnContainer")));
	for (var $j=0;$j<$count;$j++)
		{
		_setValue(_textbox("lineInput["+$j+"]",_in(_div("container itemInputContainerBasic"))),$userLog[0][3]);
		
		if (_isVisible(_div("Repeat On All")))
		{
			if (!_checkbox("lineCheckbox["+$j+"]").checked)
				{
				_click(_checkbox("lineCheckbox["+$j+"]"));
				}
		}
		
		}

	}

_check(_checkbox("confirmCheckbox"));
_click(_link("atc_button"));

if (_isVisible(_div("tie-In")))
	{
	_click(_button("Close"));
	}

	}
}*/

function shippingAddress($sheet,$i)
{
		
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), $sheet[$i][1]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), $sheet[$i][2]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"), $sheet[$i][3]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"),$sheet[$i][4]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $sheet[$i][8]);
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$sheet[$i][5]);
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), $sheet[$i][6]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), $sheet[$i][7]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),$sheet[$i][9]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email"),$uId);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail"),$uId);
}


function eventcalendar()

{
	//should select the date 
	var $currentyear=_getText(_div("/delevery-date/",_in(_div("shp-details cell")))).split("/")[2];
	_click(_textbox("dwfrm_singleshipping_datePicker_datePickerSelection"));
	//select random date
	for (var $i=0;$i<14;$i++)
		{
		//clcik on next button
		_click(_span("Next"));
		}
	//click link 25
	_click(_link("25",_in(_table("ui-datepicker-calendar"))));
	var $changedyear=_getText(_textbox("dwfrm_singleshipping_datePicker_datePickerSelection")).split(" ")[2];
	_assert($currentyear<$changedyear);

}

function adddressProceed()

{
	if (_isVisible(_div("/invalid-address/")))
	{
		_click(_link("/checkout-proceed/"));
	}	
}

function BillingAddress($sheet,$i)
{
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"), $sheet[$i][1]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"), $sheet[$i][2]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1"), $sheet[$i][3]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address2"),$sheet[$i][4]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_postal"), $sheet[$i][8]);
	_setSelected(_select("dwfrm_billing_billingAddress_addressFields_country"),$sheet[$i][5]);
	_setSelected(_select("dwfrm_billing_billingAddress_addressFields_states_state"), $sheet[$i][6]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_city"), $sheet[$i][7]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_phone"),$sheet[$i][9]);
	_setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),$uId);
	_setValue(_textbox("dwfrm_billing_billingAddress_email_confirmemail"),$uId);
	
//	if(!_isVisible(_link("/user-logout/")))
//		{
//		_setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),$uId);
//		_setValue(_textbox("dwfrm_billing_billingAddress_email_confirmemail"), $uId);
//		}
	
}


function shippingAddressUI()
{
		
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"));
	_assertVisible(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"));
	_assertVisible(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail"));
	_assertVisible(_label("Email*"));
	_assertVisible(_label("Confirm Email*"));
	_assertVisible(_label("First Name*",_in(_div("/single-ship/"))));
	_assertVisible(_label("Last Name*",_in(_div("/single-ship/"))));
	_assertVisible(_label("Organization",_in(_div("/single-ship/"))));
	_assertVisible(_label("Address Street*",_in(_div("/single-ship/"))));
	_assertVisible(_label("Suite/Apt.",_in(_div("/single-ship/"))));
	_assertVisible(_label("Zip*",_in(_div("/single-ship/"))));
	_assertVisible(_label("Ext",_in(_div("/single-ship/"))));
	_assertVisible(_label("Secondary Phone",_in(_div("/single-ship/"))));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_organization"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phoneext"));
	_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone2"));
	
}

function billingPageUI()
{
	
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_address1"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_address2"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_postal"));
	_assertVisible(_select("dwfrm_billing_billingAddress_addressFields_country"));
	_assertVisible(_select("dwfrm_billing_billingAddress_addressFields_states_state"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_city"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_phone"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_phoneext"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_phone2"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_email_emailAddress"));
	_assertVisible(_label("First Name*",_in(_div("/billing-fields/"))));
	_assertVisible(_label("Last Name*",_in(_div("/billing-fields/"))));
	_assertVisible(_label("Organization",_in(_div("/billing-fields/"))));
	_assertVisible(_label("Address Street*",_in(_div("/billing-fields/"))));
	_assertVisible(_label("Suite/Apt.",_in(_div("/billing-fields/"))));
	_assertVisible(_label("Zip*",_in(_div("/billing-fields/"))));
	_assertVisible(_label("Ext",_in(_div("/billing-fields/"))));
	_assertVisible(_label("Secondary Phone",_in(_div("/billing-fields/"))));

}

function paypal($paypalUN,$paypaalPwd)
{
	if(isMobile())
	{
		_setValue(_emailbox("login_email"), $paypalUN);
	}
	else
	{
	_setValue(_textbox("login_email"), $paypalUN);
	}

	_setValue(_password("login_password"), $paypaalPwd);
	_click(_submit("Log In"));

}

function PaymentDetails($sheet,$i)
{
	
_setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"), $sheet[$i][1]);
_setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"), $sheet[$i][2]);
_setSelected(_select("/dwfrm_billing_paymentMethods_creditCard_expiration_month/"), $sheet[$i][3]);
_setSelected(_select("/dwfrm_billing_paymentMethods_creditCard_expiration_year/"), $sheet[$i][4]);
	
}


function addItems($Item)
{
	_click(_link("Add Items"));
	//Verifying navigation
	_assertVisible(_list("homepage-slides"));
	_assertVisible(_div("home-bottom-slots"));
	//Adding the items to gift registry
	_click(_link($Item));
	//click on name link
	$ProductName=_getText(_link("name-link"));
	_click(_link("name-link"));
	//select swatches
	selectSwatch();
	//fetching product name
	$ProductPrice=_getText(_span("price-sales", _in(_div("product-price"))));	
	$QTY=_getText(_textbox("Quantity"));	
	$ProductNumber=_extract(_getText(_div("product-number")), "/Item# (.*)/", true).toString();
	_click(_link("Add to Gift Registry"));	
}

function addAddressMultiShip($sheet,$i)
{
	  if(_isVisible(_textbox("dwfrm_profile_address_addressid")))
	  {
	    _setValue(_textbox("dwfrm_profile_address_addressid"), $sheet[$i][1]);
	  }
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_firstName")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_firstName"), $sheet[$i][2]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_lastName")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_lastName"),$sheet[$i][3]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_address1")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_address1"), $sheet[$i][4]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_address2")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_address2"), $sheet[$i][5]);
	_assert(_isVisible(_select("dwfrm_multishipping_editAddress_addressFields_country")));
	_setSelected(_select("dwfrm_multishipping_editAddress_addressFields_country"), $sheet[$i][6]);
	_assert(_isVisible(_select("dwfrm_multishipping_editAddress_addressFields_states_state")));
	_setSelected(_select("dwfrm_multishipping_editAddress_addressFields_states_state"), $sheet[$i][7]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_city")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_city"), $sheet[$i][8]);
	//_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_zip")));
	//_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_zip"), $sheet[$i][9]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_postal")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_postal"), $sheet[$i][9]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_phone")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_phone"), $sheet[$i][10]);
	if(_isVisible(_checkbox("Add to Address Book")))
	{
	_assertNotTrue(_checkbox("Add to Address Book").checked);
	_click(_checkbox("dwfrm_multishipping_editAddress_addToAddressBook"));
	}
	_click(_submit("Save"));
}


//check order status
function CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode)
{
	_setValue(_textbox("dwfrm_ordertrack_orderNumber"),$OrderNumber);
	_setValue(_textbox("dwfrm_ordertrack_orderEmail"),$UserEmail);
	_setValue(_textbox("dwfrm_ordertrack_postalCode"),$Zipcode);
	_click(_submit("Check Status"));
}


//--------Pagination function
function pagination()
{  
	var $pages=_getText(_div("pagination-link float-left")).split(" ")[4];	
	
	if($pages=="1")
		{
			_assert(true,"Single Page is displayed");
			_assertVisible(_link("/Next/"));
			_assertVisible(_link("/prev/"));
			
		}
	else
		{
					for(var $j=1;$j<=$pages;$j++)
						{
						
						if ($j==1)
							{
							var $curTop=_getText(_div("pagination-link float-left")).split(" ")[2];
				  	    	_assertEqual($j,$curTop);
				  	    	var $curBottom=_getText(_div("pagination-link float-left", _in(_div("pagination")))).split(" ")[2];
				  	    	_assertEqual($j,$curBottom);
							}
							else
								{
								_click(_link("/Next/"));
								var $curTop=_getText(_div("pagination-link float-left")).split(" ")[2];
					  	    	_assertEqual($j,$curTop);
					  	    	var $curBottom=_getText(_div("pagination-link float-left", _in(_div("pagination")))).split(" ")[2];
					  	    	//_under(_list("search-result-items"))
					  	    	_assertEqual($j,$curBottom);
								}
									
							}
					for(var $j=$pages;$j>=1;$j--)
						{
							if($j==$pages)
								{	
									var $curTop= _getText(_div("pagination-link float-left")).split(" ")[2];
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=_getText(_div("pagination-link float-left", _in(_div("pagination")))).split(" ")[2];
						  	    	_assertEqual($j,$curBottom);
								}
								else
									{
									_click(_link("/prev/"));	
									var $curTop= _getText(_div("pagination-link float-left")).split(" ")[2];
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=_getText(_div("pagination-link float-left", _in(_div("pagination")))).split(" ")[2];
						  	    	_assertEqual($j,$curBottom);
									}
						}
		}
}


//--------Items per page function---------------
function ItemsperPage($itemsExpected)
{
	var $pagination=_getText(_div("pagination"));
	var $ActTotalItems=_extract(_getText(_div("pagination")), "/of (.*) </", true).toString();
	var $ExpTotalItems=0;
	var $itemsDisplayed=0;
	
	var $pages=_getText(_div("pagination-link float-left")).split(" ")[4];	
	
	for(var $j=1;$j<=$pages;$j++)
	{
		if($j==$pages)
			{
/*				$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
				_log("Products displayed per page"+$itemsDisplayed);
				//var $LastPageItems=$ActTotalItems%$itemsExpected;
				//var $noofprods=parseFloat(_getText(_div("pagesnums float-left")).split(" ")[0]);
				//var $totalnoofprods=parseFloat(_getText(_div("pagesnums float-left")).split(" ")[2]);
			
				var $totalnoofprods=_getText(_div("pagination-link float-left")).split(" ")[4];	
	
		        //last page items
				var $LastPageItems=$totalnoofprods%$itemsExpected;
				_assertEqual($LastPageItems,$itemsDisplayed);*/
			var $maxItems=parseFloat(_extract(_getText(_div("pagesnums float-left")), "/-(.*)of/", true)).toString();
			var $miniItems=parseFloat(_extract(_getText(_div("pagesnums float-left")), "/(.*)-/", true)).toString();

		      $TotalProduct_Perpage=$maxItems-$miniItems
			var $totalitemsdisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")))-1;
			_assertEqual($TotalProduct_Perpage,$totalitemsdisplayed);

			}
		
			else if($j==1)
				{
				
					$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
					_log("Products displayed per page"+$itemsDisplayed);
					_assertEqual($itemsExpected,$itemsDisplayed);
				
				}
		
				else
					{
					
						_click(_link("/Next/"));
						$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
						_log("Products displayed per page"+$itemsDisplayed);
						_assertEqual($itemsExpected,$itemsDisplayed);
					
					}	
	}
	
}


//Price filter
function priceCheck($Range)
{
	var $priceBounds = _extract(_getText(_link($Range)), "/[$](.*)-[$](.*)/",true).toString().split(",");
	var $minPrice = $priceBounds[0].toString();
	var $maxPrice = $priceBounds[1].toString();
	_log("$minPrice = " + $minPrice);
	_log("$maxPrice = " + $maxPrice);
	
var $pages=_getText(_div("/pagination-link/")).split(" ")[4];	
	
	if($pages=="1")
	{
		var $prices = _collectAttributes("_div", "/product-pricing/","sahiText");
		for (var $k = 0; $k < $prices.length; $k++) 
			{
				var $price = parseFloat(_extract($prices[$k],"/[$](.*)/",true)[0].replace(",",""));				
				_log("$price = " + $price);				
				if ($price < $minPrice) 
				{
					_assert(false, "Price " + $price + " is lesser than min price " + $minPrice);
				} 
				else if ($price > $maxPrice) 
				{
					_assert(false, "Price " + $price + " is greater than max price " + $maxPrice);
				}					
			}		
	}
	else
	{
		
		for(var $j=1;$j<=$pages;$j++)
		{
			if(!$j==1)
				{
				_click(_link("next"));						
				}
			var $prices = _collectAttributes("_div", "/product-pricing/","sahiText");
			for (var $k = 0; $k < $prices.length; $k++) 
				{
					var $price = parseFloat(_extract($prices[$k],"/[$](.*)/",true)[0].replace(",",""));				
					_log("$price = " + $price);				
					if ($price < $minPrice) 
					{
						_assert(false, "Price " + $price + " is lesser than min price " + $minPrice);
					} 
					else if ($price > $maxPrice) 
					{
						_assert(false, "Price " + $price + " is greater than max price " + $maxPrice);
					}					
				}
		}
		
	}
}

//--------Sort By function--------------
function sortBy($opt)
{

if($opt=="Type")
{	
	sortAllPage($opt,"_div", "/product-pricing/");
}

else if($opt=="Popularity")
{
	sortAllPage($opt,"_div", "/product-pricing/");
}

else if($opt=="Price")
{
	sortAllPage($opt,"_div", "/product-pricing/");
}
	
//	if($opt=="Price Low to High")
//	{	
//		sortAllPage($opt,"_div", "/product-pricing/");
//	}     
//else if($opt=="Price High to Low")
//	{
//		sortAllPage($opt,"_div", "/product-pricing/");
//	}
//else if($opt=="Product Name A - Z")
//	{
//		sortAllPage($opt,"_link","/name-link/");            
//	}
//else if($opt=="Product Name Z - A")
//	{
//		sortAllPage($opt,"_link","/name-link/");        
//	}

}

function sortAllPage($opt,$tag,$field)
{
	var $arr = new Array();
	var $temp =new Array();

		var $pages=_getText(_div("pagination-link float-left")).split(" ")[4];
		
		for(var $j=1;$j<=$pages;$j++)
		{
			
		if ($j==1)
			{
			
			$arr = _collectAttributes($tag,$field,"sahiText");
			assertValues($opt,$arr);	
			
			}
			else
				{
				
				_click(_link("/Next/"));
				$temp = _collectAttributes($tag,$field,"sahiText");
				$arr.push($temp);
				assertValues($opt,$arr);	
				
				}
		}
		
}


function assertValues($opt,$arr)
{
  	_log("Collected array-----:"+$arr);
     var $arrExp=$arr.sort();
	 if($opt=="Product Name Ascending" ||$opt=="Price Low To High")
		{
	 		$arrExp=$arr.sort();
		}
	 else if($t=="Product Name Descending" ||$opt=="Price High to Low")
		{
	  		$arrExp=$arr.sort().reverse();
		} 
	 _log("Sorted array-----:"+$arrExp);
     _assertEqualArrays($arrExp,$arr);
}

function round(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0)
      return Math.round(value);

    value = +value;
    exp  = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
      return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
  }

function orderPlacement($dataSheet,$paypalSheet,$creditCardSheet,$tax)
{
	//navigate to billing page
	navigateToBillingPage($dataSheet[0][0],$dataSheet[1][0]);
	if($tax=="true")
		{
		//verify the tax should not ve NA or 0.00
		var $salesTax=_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true);
			if($salesTax!="N/A" && $salesTax!="0.00" && $salesTax!="false")
				{
				_assert(true);
				}
			else
				{
				_assert(false);
				}
		}
	//enter billing address
	//enter valid billing address
	BillingAddress($Valid_Data,0);
	//enter credit card details
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
		{
			  if($CreditCardPayment==$BMConfig[0][4])
			  {
			  	PaymentDetails($paypalSheet,2);
			  }
		  	  else
			  {
			  	PaymentDetails($creditCardSheet,3);
			  }
			//click on continue
			_click(_submit("dwfrm_billing_save"));	
		}
   else if($CreditCard=="No" && $paypalNormal=="Yes")
		{
	   		_assert(_radio("PayPal").checked);
			//click on continue
			_click(_submit("dwfrm_billing_save"));	
			//paypal
   			if(isMobile())
             {
             _setValue(_emailbox("login_email"), $paypalSheet[0][0]);
             }
   			else
             {
             _setValue(_textbox("login_email"), $paypalSheet[0][0]);
             }
          _setValue(_password("login_password"), $paypalSheet[0][1]);
          _click(_submit("Log In"));
          _click(_submit("Continue"));
          _wait(10000,_isVisible(_submit("submit")));
		}
	//click on submit order
	_click(_submit("submit"));
	//verify order placement
	_assertVisible(_heading1($dataSheet[0][2]));
	_assertVisible(_span("value", _near(_span("Order Placed"))));
	_assertVisible(_span("/value/",_near(_span("Order Number"))));  
	_assertVisible(_cell("order-billing"));
	_assertVisible(_cell("order-payment-instruments"));
	_assertVisible(_table("order-totals-table"));
}


/*  Shopnav Functions   */

function navigateToPdp($productid)
{
	_setValue(_textbox("q"), $productid);
	//Click on search icon
	_click(_submit("Search"));
	
	//selecting product if it navigates to search result page
	if (_isVisible(_div("search-result-content")))
		{
		_click(_link("/(.*)/", _in(_listItem("grid-tile new-row"))))
		}

	//_click(_link("name-link", _in(_list("search-result-items"))));
}



function NavigatetoSubcat($prod)
{

//_click(_link("/Select Sport/", _in(_div("dropdownul"))));
_click(_link($prod, _in(_list("dropdown-menu"))));
	
}

function NavigateToCheckoutLogin($productid)
{
	navigateToPdp($productid);
	selectSwatch();
	
	var $prodname=_getText(_heading2("product-name"));
	var $prodquantity=_getText(_numberbox("/(.*)/"));
	var $prodprice=_extract(_getText(_div("/Total:/")),"/[$](.*)/",true);
	var $totalprice=$prodprice*$prodquantity;
	
	//click on add to cart button
	_click(_submit("/sprite-addtocart/"));
	//Engraving
	engraving();
	//Click checkout button
	_click(_link("/mini-cart-link-checkout-link/"));
}

/*function emptycart()
{
	//navigate to cart
	_click(_link("/mini-cart-link/"));
	//Empty cart button
	_click(_link("Empty cart"));
	//ok in modal overlay
	if(_isVisible(_div("dialog-container")))
	{
		_click(_submit("button-text "));
	}
}*/