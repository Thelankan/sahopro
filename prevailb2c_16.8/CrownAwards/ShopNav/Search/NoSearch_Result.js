_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Search.xls");

var $No_Search=_readExcelFile("Search.xls","NoSearch");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

function NavigateToNoSearch($productID)
{
	//Enter keyword
	_setValue(_textbox("q"), $productID);
	//Click on search icon
	_click(_submit("Search"));
}

var $t = _testcase("216348/215835","Verify the navigation to search no result page from home page in application both as an Anonymous and Registered user./display of 'You can also shop..' title");
$t.start();
try
{
	NavigatetoHomepage();
	NavigateToNoSearch($No_Search[0][0]);
	//No search result
	_assertVisible(_heading3("No Products Matched Your Search For \""+$No_Search[0][0]+"\""));
	//Display of you can also shop products
	//_assertVisible(_heading3($No_Search[0][3]));
	_assertVisible(_div("category-slot"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("215599","Verify the navigation of 'Recently Viewed' of left nav in No Search results page in Desktop/tablet");
$t.start();
try
{
	navigateToPdp($No_Search[0][1]);
	NavigateToNoSearch($No_Search[0][0]);
	//Recently viewed heading
	_assertVisible(_heading3("Recently Viewed"));
	//Recently viewed product
	var $productname=_getText(_link("name-link", _in(_list("/last-visited-result-items/"))));
	//Navigate to PDP
	_click(_link("name-link", _in(_list("/last-visited-result-items/"))));
	//Verify the product name
	_assertEqual($productname, _getText(_heading1("product-name", _in(_div("pdp-main")))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("215821","Verify the functionality Search box for search again in No Search results page in Desktop/tablet");
$t.start();
try
{
	//Navigate to search no rsult page
	NavigateToNoSearch($No_Search[0][0]);
	//Search using try another search
	_setValue(_textbox("q", _in(_div("Try Another Search!"))), $No_Search[1][0]);
	_click(_submit("btSearch", _in(_div("Try Another Search!"))));
	//Search result page should be displayed
	_assertVisible(_list("search-result-items"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("215825","Verify the functionality of static Toll free number in Need more we can help! section in No Search results page in Desktop/tablet");
$t.start();
try
{
	//Navigate to search no rsult page
	NavigateToNoSearch($No_Search[0][0]);
	//Click on Phone number
	_click(_span($No_Search[0][2]));
	_assertVisible(_heading3("No Products Matched Your Search For \""+$No_Search[0][0]+"\""));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("215837/215838/215839","Verify the display of 'Awards Categories' grid of No Search results page in Desktop/tablet/Verify the functionality of any category link in 'Awards Categories' grid of No Search results page in Desktop/tablet");
$t.start();
try
{

	//Navigate to search no rsult page
	NavigateToNoSearch($No_Search[0][0]);
	//display of categories
	_assertVisible(_div("category-slot"));
	_assertVisible(_div("/categorybox/"));
	
	//click on any of the category in search result page
	var $category=_getText(_link("/(.*)/", _in(_div("categorybox"))));
	_click(_link("/(.*)/", _in(_div("categorybox"))));
	//verifying category landing page
	_assertContainsText($category,_getText(_heading1("/(.*)/",_near(_div("breadcrumb")))));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

/*
var $t = _testcase("","");
$t.start();
try
{


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()
*/