_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Search.xls");

var $searchsugg=_readExcelFile("Search.xls","SearchSuggestions");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


var $t = _testcase("215922/215956/216069","Verify the UI of the search suggestions Overlay as a guest User displayed from the Global Header./Verify the UI of the product tile in search suggestions drop down as a guest user./Verify the maximum number of products displayed in the search suggestions overlay as a Guest user.");
$t.start();
try
{

	_assertVisible(_textbox("q", _in(_div("header-test-search"))));
	//Setting value in search field
	_setValue(_textbox("q", _in(_div("header-test-search"))),$searchsugg[0][0]);
	//checking the visibility of left pane
	_assertVisible(_link("hit", _in(_div("phrase-suggestions"))));
	//count of left nav pane
	_set($count,_count("_link","hit", _in(_div("phrase-suggestions"))));
	
	for(var $i=0;$i<$count;$i++)
		{
		_assertVisible(_link("hit["+$i+"]", _in(_div("phrase-suggestions"))));
		}
	//checking right pane
	_assertVisible(_link("product-link"));
	_assertVisible(_image("/(.*)/", _in(_div("product-image"))));
	_set($prodcount,_count("_link","product-link",_in(_div("product-suggestions"))));
	
	_assertEqual($searchsugg[0][1],$prodcount);
	
	if ($prodcount==$searchsugg[0][1])
		{
			for(var $j=0;$j<$prodcount;$j++)
			{
				_assertVisible(_link("product-link["+$j+"]"));
				_assertVisible(_image("/(.*)/", _in(_div("product-image["+$j+"]"))));
			}
		}
	else
		{
		_assert(false,"more than or lessthan 2 products are displaying");
		}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216071","Verify the application behvaior on click of Product name in the search suggestions drop down as a Guest User");
$t.start();
try
{
	
//Setting value in search field
_setValue(_textbox("q", _in(_div("header-test-search"))),$searchsugg[0][0]);
	
for(var $j=0;$j<2;$j++)
{
	
	_assertVisible(_link("product-link["+$j+"]"));
	var $productName=_getText(_link("product-link["+$j+"]"));
	_assertVisible(_image("/(.*)/", _in(_div("product-image["+$j+"]"))));
	//click on productimage
	_click(_link("product-link["+$j+"]"));
	//verification
	_assertVisible(_span($productName, _in(_div("breadcrumb"))));
	_assertEqual($productName, _getText(_heading1("product-name")));
	//Setting value in search field
	_setValue(_textbox("q", _in(_div("header-test-search"))),$searchsugg[0][0]);
	
}


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216075","Verify the application behavior on click of product image in the Search suggestion drop down as a Guest user.");
$t.start();
try
{
	
	//Setting value in search field
	_setValue(_textbox("q", _in(_div("header-test-search"))),$searchsugg[0][0]);
		
	for(var $j=0;$j<2;$j++)
	{
		
		_assertVisible(_link("product-link["+$j+"]"));
		var $productName=_getText(_link("product-link["+$j+"]"));
		_assertVisible(_image("/(.*)/", _in(_div("product-image["+$j+"]"))));
		//click on productimage
		_click(_image("/(.*)/", _in(_div("product-image["+$j+"]"))));
		//verification
		_assertVisible(_span($productName, _in(_div("breadcrumb"))));
		_assertEqual($productName, _getText(_heading1("product-name")));
		//Setting value in search field
		_setValue(_textbox("q", _in(_div("header-test-search"))),$searchsugg[0][0]);
		
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

/*var $t = _testcase("","");
$t.start();
try
{

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()*/

_log("******************** Search Suggestions Register user flow *******************")

if (isMobile())
{
 _click(_link("/mobile-show/", _in(_div("user-info"))));
}
else
	{
	 _click(_link("/desktop-show/", _in(_div("user-info"))));
	 //Visibility of overlay
	 _assertVisible(_div("dialog-container"));
	}
login();

var $t = _testcase("215957/215958/216070","Verify the UI of the search suggestions Overlay as a Registerd  User displayed from the Global Header./Verify the UI of the product tile in search suggestions drop down as a Reg user./Verify the maximum number of products displayed in the search suggestions overlay as a REG user.");
$t.start();
try
{
	
	NavigatetoHomepage();

	_assertVisible(_textbox("q", _in(_div("header-test-search"))));
	//Setting value in search field
	_setValue(_textbox("q", _in(_div("header-test-search"))),$searchsugg[0][0]);
	//checking the visibility of left pane
	_assertVisible(_link("hit", _in(_div("phrase-suggestions"))));
	//count of left nav pane
	_set($count,_count("_link","hit", _in(_div("phrase-suggestions"))));
	
	for(var $i=0;$i<$count;$i++)
		{
		_assertVisible(_link("hit["+$i+"]", _in(_div("phrase-suggestions"))));
		}
	//checking right pane
	_assertVisible(_link("product-link"));
	_assertVisible(_image("/(.*)/", _in(_div("product-image"))));
	_set($prodcount,_count("_link","product-link",_in(_div("product-suggestions"))));
	
	_assertEqual($searchsugg[0][1],$prodcount);
	
	if ($prodcount==$searchsugg[0][1])
		{
			for(var $j=0;$j<$prodcount;$j++)
			{
				_assertVisible(_link("product-link["+$j+"]"));
				_assertVisible(_image("/(.*)/", _in(_div("product-image["+$j+"]"))));
			}
		}
	else
		{
		_assert(false,"more than or lessthan 2 products are displaying");
		}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("216073","Verify the application behvaior on click of Product name in the search suggestions drop down as a REG user");
$t.start();
try
{
	
	//Setting value in search field
	_setValue(_textbox("q", _in(_div("header-test-search"))),$searchsugg[0][0]);
		
	for(var $j=0;$j<2;$j++)
	{
		
		_assertVisible(_link("product-link["+$j+"]"));
		var $productName=_getText(_link("product-link["+$j+"]"));
		_assertVisible(_image("/(.*)/", _in(_div("product-image["+$j+"]"))));
		//click on productimage
		_click(_link("product-link["+$j+"]"));
		//verification
		_assertVisible(_span($productName, _in(_div("breadcrumb"))));
		_assertEqual($productName, _getText(_heading1("product-name")));
		//Setting value in search field
		_setValue(_textbox("q", _in(_div("header-test-search"))),$searchsugg[0][0]);
		
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216076","Verify the application behavior on click of product image in the Search suggestion drop down as a REG user.");
$t.start();
try
{

	//Setting value in search field
	_setValue(_textbox("q", _in(_div("header-test-search"))),$searchsugg[0][0]);
		
	for(var $j=0;$j<2;$j++)
	{
		
		_assertVisible(_link("product-link["+$j+"]"));
		var $productName=_getText(_link("product-link["+$j+"]"));
		_assertVisible(_image("/(.*)/", _in(_div("product-image["+$j+"]"))));
		//click on productimage
		_click(_image("/(.*)/", _in(_div("product-image["+$j+"]"))));
		//verification
		_assertVisible(_span($productName, _in(_div("breadcrumb"))));
		_assertEqual($productName, _getText(_heading1("product-name")));
		//Setting value in search field
		_setValue(_textbox("q", _in(_div("header-test-search"))),$searchsugg[0][0]);
		
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

/*var $t = _testcase("","");
$t.start();
try
{


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()*/

