//including external files
_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Search.xls");


//declaring excel files
var $Search=_readExcelFile("Search.xls","Search");


SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


var $t=_testcase("216155/216156","Verify the Navigation of CYA/Sport page search in desktop/tablet");
$t.start();
try
{
	
	//click on drop down
	_click(_link("drop-downul"));
	_click(_link($Search[0][0]));
	//verify navigation
	if(!isMobile())
		{
		//shan
		//_assertVisible(_heading3($Search[0][0]));
		_assertContainsText($Search[0][0], _heading1("/cya-title bigtext-line/"));
		
		
		}
	_assertVisible(_span("titleaccent"));
	var $title=$Search[0][0]+" "+$Search[0][3]+" I "+$Search[0][0]+" "+$Search[1][3]+" I "+$Search[0][0]+" "+$Search[2][3];
	_assertVisible(_span($title));
	//bread crumb
	_assertVisible(_div("main-content"));
	var $expBreadCrumb=$Search[0][1]+" "+$Search[1][1]+" "+$Search[0][0];
	_assertEqual($expBreadCrumb, _getText(_div("main-content")));
	//grid page
	_assertVisible(_list("search-result-items"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("216179","Verify the display of product grid in CYA/Sport page search in desktop/tablet");
$t.start();
try
{
	//shan
	//verify the display
	//_assertVisible(_heading1("cya-title"));
	_assertContainsText($Search[0][0], _heading1("/cya-title bigtext-line/"));
	
	if(!isMobile())
		{
		//shan
	//	_assertVisible(_paragraph("newcya2 desktop-show"));
		_assertVisible(_paragraph("newcya2 hbarbn2 desktop-show"));
		
		_assertVisible(_div("dwt-box-images"));
		for(var $i=0;$i<2;$i++)
			{
			//total products 
			_assertVisible(_div("results-hits["+$i+"]"));
			//pagination
			_assertVisible(_div("pagination-link float-left["+$i+"]"));
			//sort by
			_assertVisible(_label($Search[0][2]+"["+$i+"]"));
			_assertVisible(_select("grid-sort-header["+$i+"]"));
			//products per page
			_assertVisible(_span($Search[1][2]+"["+$i+"]"));
			_assertVisible(_select("grid-paging-header["+$i+"]"));
			}
		//product per row
		var $newRowProds=_count("_listItem","/grid-tile new-row/",_in(_list("search-result-items")));
		var $toaProds=_count("_listItem","/grid-tile/",_in(_list("search-result-items")));
		var $count=$toaProds%5;
		var $expCount=parseInt($toaProds/5);
		if($count==0)
			{
			_assertEqual($newRowProds,$expCount);
			}
		else
			{
			_assertEqual($newRowProds,$expCount+1);
			}
		}
	else
		{
		for(var $i=0;$i<2;$i++)
			{
			_assertVisible(_div("pagination-link float-left["+$i+"]"));
			}
		_assertVisible(_link("REFINE SEARCH"));
		_assertVisible(_span("VIEW"));
		_assertVisible(_label("sort by:"));
		_assertVisible(_select("grid-sort-header[1]"));
		}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


if(!isMobile())
{
var $t=_testcase("216216","Verify the Navigation of search results landing desktop/tablet");
$t.start();
try
{
	
	//navigate to home page
	NavigatetoHomepage();
	//enter valid key word in search box
	_setValue(_textbox("q"),$Search[0][0]);
	//hit on enter key
	_focusWindow();
	_focus(_textbox("q"));
	_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	//verify navigation
	if(!isMobile())
	{
	//shan
	//_assertVisible(_heading3($Search[0][0]));
		//_assertContainsText($Search[0][0], _heading1("cya-title"));
		_assertContainsText($Search[0][0], _heading1("/cya-title bigtext-line/"));
		
	}
	_assertVisible(_span("titleaccent"));
	//shan
	var $title=$Search[0][6]+" "+$Search[0][0]+" "+$Search[0][7]+" "+$Search[0][8]+" "+$Search[0][6]+" "+$Search[0][0]+" "+$Search[0][9]+" "+$Search[0][8]+" "+$Search[0][6]+" "+$Search[0][0]+" "+$Search[0][10];
	_assertVisible(_span($title));

	//bread crumb shouldn't display
	_assertVisible(_div("main-content"));
	_assertEqual("", _getText(_div("main-content")));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();
}


var $t=_testcase("216345","Verify the navigation to search result page from home page in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	
	//navigate to home page
	NavigatetoHomepage();
	//enter valid key word in search box
	_setValue(_textbox("q"),$Search[1][0]);
	_click(_submit("Search"));
	//verify navigation
	if(!isMobile())
	{
	_assertVisible(_heading3($Search[1][0]));
		
	
	}
	_assertVisible(_span("titleaccent"));
	var $title=$Search[1][0]+" "+$Search[0][3]+" I "+$Search[1][0]+" "+$Search[1][3]+" I "+$Search[1][0]+" "+$Search[2][3];
	_assertVisible(_span($title));
	//_assertContainsText
	
	//bread crumb shouldn't display
	_assertVisible(_div("main-content"));
	_assertEqual("", _getText(_div("main-content")));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("216723","Verify the navigation of non-sport related search page in Desktop");
$t.start();
try
{

	//navigate to home page
	NavigatetoHomepage();
	//enter valid key word in search box
	_setValue(_textbox("q"),$Search[2][0]);
	_click(_submit("Search"));
	//verify navigation
	if(!isMobile())
	{
	//_assertVisible(_heading3($Search[2][0]));
		//_assertContainsText($Search[2][0], _heading1("cya-title"));
		_assertContainsText($Search[2][0], _heading1("/cya-title bigtext-line/"));
	}
	_assertVisible(_span("titleaccent"));
	var $title=$Search[2][0]+" "+$Search[0][3]+" I "+$Search[2][0]+" "+$Search[1][3]+" I "+$Search[2][0]+" "+$Search[2][3];
	_assertVisible(_span($title));
	//bread crumb shouldn't display
	_assertVisible(_div("main-content"));
	_assertEqual("", _getText(_div("main-content")));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("216214","Verify the display of Bottom category level text in CYA/Sport page search in desktop/tablet");
$t.start();
try
{
	_assert(false); // need text configuration in bottom of the page 
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("216159/216161","Verify the displayof Page title in CYA/Sport page search in desktop/tablet/Verify the displayof Searched term description in CYA/Sport page search in desktop/tablet");
$t.start();
try
{

//navigate to search results page	
search($Search[0][0]);

//display of Searched term description in CYA/Sport page
//_assertContainsText($Search[0][0], _paragraph("newcya2 desktop-show"));
_assertContainsText($Search[0][0],_paragraph("newcya2 hbarbn2 desktop-show"));



//Search key should be displayed with originally Grouped top level category
var $Searchkey=_getText(_span("titleaccent")).split("I").length;

for ($i=0;$i<$Searchkey;$i++)
	{

	//_assertContainsText($Search[0][0],_getText(_span("titleaccent")).split("I")[$i])
	
	
//	var $expectedkeyword=new Array();
//	var $keyword=_getText(_span("titleaccent")).split("I")[$i]
//	var $originalkeyword=$keyword.split
//	$expectedkeyword.push($keyword);
	
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216163/216399","Verify the functionality of Category images content slot in CYA/Sport page search in desktop/tablet/ Verify the navigation of Search results - dynamically created page");
$t.start();
try
{

_assert(false,"Content slot is not configured but updated in testopia manually passed i should ask them to fail");
_assert(false,"doubt should ask with sneha");

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216166","Verify the display of pagination links in CYA/Sport page search in desktop/tablet");
$t.start();
try
{

//navigate to sub cat page
search($Search[0][0]);
//verification of pagination 
pagination();

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216170/222317","Verify the display of options in Show dropdown/display of # of search results in CYA/Sport page search in desktop/tablet");
$t.start();
try
{

//navigate to sub cat page
search($Search[0][0]);
	
if (_isVisible(_div("items-per-page")) && _isVisible(_div("items-per-page",_under(_list("search-result-items")))))
	{
	
	//items per page in header		
	_assertVisible(_div("items-per-page"));
	//Items per page in footer	
	_assertVisible(_div("items-per-page", _under(_list("search-result-items"))));
	
	//options in show drop down
	_assertVisible(_select("page-show"));	
	var $itemsdisplayed=_getText(_select("page-show"));

	for (var $i=0;$i<$itemsdisplayed.length;$i++)
	{
	_setSelected(_select("page-show"),$itemsdisplayed[$i]);
	_assertEqual($itemsdisplayed[$i],_getSelectedText(_select("page-show")),"Selected option is displaying properly");
	_assertEqual($itemsdisplayed[$i],_getSelectedText(_select("page-show",_under(_list("search-result-items")))),"Selected option is displaying properly at footer");
	ItemsperPage($itemsdisplayed[$i]);
	}
	
	}

else
	{
	
	_log("Single page displayed");
	var $totalitems=_extract(_getText(_div("pagination")), "/of (.*) </", true).toString();
	var $totalitemsdisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
	_assertEqual($totalitems,$totalitemsdisplayed);
	
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("222328","Verify the application behavior on search for product by the item #");
$t.start();
try
{

//navigate to PDP
search($Search[0][4]);
//verification of navigation
//PDP
_assertVisible(_heading1("product-name", _in(_div("pdpMain"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//Arjun
var $t = _testcase("222355/222357","Verify the functionality of Recently Viewed carousel/'More like this' link in left navigation refinement pane for desktop/tablet");
$t.start();
try
{
	//navigate to any of the product PDP

	var $prodnamelist=new Array()     
	       
	for (var $i=0;$i<3;$i++)
	       {

	       search($Search[$i][5]);
	       //_click(_link("/name-link/",_in(_listItem("grid-tile new-row"))));
	       var $productName=_getText(_heading1("product-name"));
	       $prodnamelist.push($productName);
	       
	       }

	NavigatetoHomepage();
	search($Search[0][0]);
	

	//count of products in recently viewed
	var $count=_count("_link","name-link",_in(_div("slick-track",_in(_list("/last-visited-result-items/")))));
	_assertEqual($Search[3][5],$count);

	var $recentlyviewedprods=_collectAttributes("_link","name-link","sahiText",_in(_div("slick-track",_in(_list("/last-visited-result-items/")))));
	_assertEqual($prodnamelist.sort(),$recentlyviewedprods.sort());

	//click on recently viewed products on home page should navigate to PD page
	for (var $j=0;$j<$count;$j++)
	       {
	       _click(_link($recentlyviewedprods[$j]));
	       var $productName=_getText(_heading1("product-name"));
	       _assertVisible(_image("/primary-image/"));
	       _assertVisible(_div("pdpMain"));
	       _assertVisible(_div("/product-detail/"));
	       _assertEqual($recentlyviewedprods[$j],$productName);
	       NavigatetoHomepage();
	       search($Search[0][0]);
	       }
	//Click more like this link
	_click(_link("more-like-this"));
	//Category landing
	_assertEqual($Search[2][1], _getText(_div("breadcrumb")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("216179","Verify the display of product grid in CYA/Sport page search in desktop/tablet");
$t.start();
try
{
	//navigate to home page
	NavigatetoHomepage();
	//enter valid key word in search box
	_setValue(_textbox("q"),$Search[3][0]);
	//hit on enter key
	_focusWindow();
	_focus(_textbox("q"));
	_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	//verifying the heading
	_assertVisible(_div("dwt-box-images"));
	_assertVisible(_div("/dwt-box/"));
var $prod=_count("_div","/dwt-content/", _in(_div("dwt-box-content desktop-show")));
_assertEqual($Search[0][11],$prod);
		
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();
