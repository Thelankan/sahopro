_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("PDP.xls");
var $pdp=_readExcelFile("PDP.xls","pdp");
var $Address_data=_readExcelFile("../../Checkout/Payment/Payment.xls","Address_data");
var $credit_card=_readExcelFile("../../Checkout/Payment/Payment.xls","credit_card");
var $shipaddress=_readExcelFile("../../Checkout/ShippingPage/ShippingPage.xls","ShipAddress");
var $shippingdata=_readExcelFile("../../Checkout/ShippingPage/ShippingPage.xls","ShippingData");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("227203","Verify the UI related to the Checkout-View Engraving (on payment page) in Crown Awards application as an anonymous and registered user.");
$t.start();
try
{

//navigate to cart page with single 
navigateToCart($pdp[3][0],$pdp[0][1]);

//click on checkout link in mini cart
_mouseOver(_link("/mini-cart-link/"));
//click link on view cart link in mini cart section
_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));

_assertVisible(_submit("/view-engrave/"));
_click(_submit("/view-engrave/"));
//engraving details
_assertVisible(_div("engrave-bold"));
_assertVisible(_span($userLog[0][3], _in(_div("engrave-bold"))));

//click on checkout link
_click(_link("/mini-cart-link-checkout-link/"));

//shipping form should display
shippingAddress($shipaddress,1);

//address proceed
adddressProceed();

//Selecting shipping method
_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));

//event calendar
eventcalendar();

//click on view engraving in payment page
_click(_link("/checkout-view-engraving/"));
_wait(3000);

//engraving content
_assertVisible(_div("engrave-content"));
_assertVisible(_div("review-engraving-details"));
_assertVisible(_span($userLog[0][3],_in(_div("engrave-content"))));
//click on close link
_click(_button("Close"));
_wait(4000);
_assertNotVisible(_div("review-engraving-details"));

//clean up the cart page
cleanup();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()



var $t = _testcase("227240","Verify the UI. functionality related to standard engraving product or engine for multiple products on PDP in Crown Awards application as an anonymous and registered user.");
$t.start();
try
{
	
//add item to cart
addItemToCart($pdp[0][0],$pdp[0][1]);
//click on checkout button in PDP
_click(_link("Checkout"));
//added engraving line
_assertEqual($userLog[0][3],_getText(_span("engrave-normal")));

_assert(false,"need to add onw more scenario");

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("227161","Verify the functionality related to Product Page Crystal product on PDP in Crown Awards application as an anonymous and registered user.");
$t.start();
try
{
cleanup();	
//navigate to PDP
navigateToPdp($pdp[0][2]);

$checkbox=_collectAttributes("_div","box itemnum","productid",_in(_div("/ItemTableWrap/")));

_check(_checkbox("/prdSelect"+$checkbox[0]+"/"));
_assertVisible(_div("/prodwrapper/"));

//select option
_setSelected(_select("flexdetails"),$pdp[0][3]);
//select quantity
_setValue(_numberbox("Quantity"),$pdp[1][1]);
//click on add to cart button
_click(_submit("/add-to-cart/"));
//engraving
engravingPDP();
//click on view cart button
_mouseOver(_link("/mini-cart-link/"));
_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));
//gift card option should not display
_assertVisible(_span($pdp[1][4]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("12345/227161","glpk6 product order placing in different scenarios");
$t.start();
try
{
	for (var $t=0;$t<2;$t++)
	{	
		_log("******************************"+$t+"**********************************")
		//navigate to PDP
		navigateToPdp($pdp[$t][5]);

		$checkboxmain=_collectAttributes("_div", "box itemnum", "productid", _in(_div("product-content")));
		$checkboxsize = _collectAttributes("_div", "box-row", "sahiText", _in(_div("product-content"))).length
		_log($checkboxmain);
		
		for (var $p=0;$p<$checkboxsize;$p++)
		{
			_check(_checkbox("/prdSelect"+$checkboxmain[$p]+"/"));
			_check(_checkbox("/prdSelect"+$checkboxmain[$p]+"/"));
			if (!isMobile() || mobile.iPad())
			{	
				_assertVisible(_div("/prodwrapper/"));
				_wait(4000);
				var $selections=_count("_div", "/prodStepText/", _in(_div("/prodwrapper mobile-hide/")))-1;
				var $dropdown=0;
				var $flexdetails=0;
				var $swatecinc=1;
				
				for (var $i=0;$i<$selections;$i++)
				{
					if (_isVisible(_select("flexdetails["+$dropdown+"]")))
					{
						_setSelected(_select("flexdetails["+$dropdown+"]"),$swatecinc);
						$dropdown++;
						$swatecinc++;
						_wait(3000);
					}
					else if (_isVisible(_radio("/radio/",_in(_div("flexdetails["+$flexdetails+"]",_in(_div("product-content")))))))
					{
						var $radio=_collectAttributes("_radio","/radio/","id",_in(_div("flexdetails["+$flexdetails+"]")));
						_click(_radio($radio[0],_in(_div("flexdetails["+$flexdetails+"]"))));
						$flexdetails++;
					}
					else if (_isVisible(_checkbox("/check/",_in(_div("flexdetails["+$flexdetails+"]",_in(_div("product-content")))))))
					{
						var $checkbox=_collectAttributes("_checkbox","/check/","id",_in(_div("flexdetails["+$flexdetails+"]")));
						_check(_checkbox($checkbox[0],_in(_div("flexdetails["+$flexdetails+"]"))));
						$flexdetails++;
					}
					
					if (_isVisible(_div("uploadPopup")))
					{
						_click(_button("Close",_near(_div("uploadPopup"))))
					}
			
					while (_isVisible(_div("dialog-container")))
					{
						_click(_button("Close",_near(_div("dialog-container"))));
					}
				}
				//selecting quantity
				_setValue(_numberbox("Quantity"),10);
			}
			else
			{
				var $p=0;
				for (var $i=0;$i<10;$i++)
				{
					if (_isVisible(_div("formfield",_in(_div("prodformstyle["+$p+"]")))))
					{
						_setSelected(_select($i),1);
						_wait(5000);
						$p++;
					}
					else
					{
						break;
					}
					if (_isVisible(_div("uploadPopup")))
					{
						_click(_button("Close",_near(_div("uploadPopup"))))
					}
					while (_isVisible(_div("dialog-container")))
					{
						_click(_button("Close",_near(_div("dialog-container"))));
					}
				}
				//selecting quantity
				_setValue(_numberbox("Quantity"),10);
			}
		}
		_wait(3000)
		//add all to engrave
		//_click(_link("with-engraving"));
		_click(_link("/add-all-engrave/"));
		//engraving
		engravingPDP();
		//navigate to checkout page
		_click(_link("/mini-cart-link-checkout-link/"));
		//shipping details
		shippingAddress($Address_data,$t);
		_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
		//address proceed
		adddressProceed();
		//Shipment date
		eventcalendar();
		_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
		adddressProceed();
		//Payment details
		PaymentDetails($credit_card,$t);
		//Placing order
		_click(_checkbox("/dwfrm_billing_confirm/"));
		_click(_submit("dwfrm_billing_save"));

		//Checking order is placed or not
		_assertVisible(_heading1($pdp[0][6]));
		_assertVisible(_div("order-confirmation-details"));


		var $orderno=_getText(_span("order-number")).split(" ")[2];
		_log($orderno);

		var $orderkkkk=_getText(_span("order-number"))
		_log($orderkkkk);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()




function engravingPDP()
{

while (_isVisible(_div("headerContainer")))

	{
	
if ((_isVisible(_div("itemPreviewContainerAdvanced"))) || (_isVisible(_div("container itemInputContainerAdvanced"))))
	{
	
	var $count=_count("_textbox","lineInputAdvanced",_in(_div("NDSColumnContainer")));

var $q=1;	
for (var $i=1;$i<=$count;$i++)
	{
	
	if ($i==1)
		{
		_setSelected(_select("contentSelect_1_"+$i+""), "Ornament");
		}
		else
			{
			_setSelected(_select("contentSelect_1_"+$i+""), "Text");
			_setValue(_textbox("lineInputAdvanced["+$q+"]"),$pdp[0][4]);
			_wait(1000)
			if (_isVisible(_div("Repeat On All")))
			{
				_check(_checkbox("lineCheckboxAdvanced["+$q+"]"));
//				if (!_checkbox("lineCheckboxAdvanced["+$q+"]").checked)
//					{
//					_check(_checkbox("lineCheckboxAdvanced["+$q+"]"));
//					}
			}
			
			$q++;
			
			}

	}

	}
else
	{
	
	var $count=_count("_textbox","/lineInput/",_in(_div("NDSColumnContainer")));
	for (var $j=0;$j<$count;$j++)
		{
		_setValue(_textbox("lineInput["+$j+"]",_in(_div("container itemInputContainerBasic"))),$pdp[0][4]);
		
		if (_isVisible(_div("Repeat On All")))
		{
			if (!_checkbox("lineCheckbox["+$j+"]").checked)
				{
				_click(_checkbox("lineCheckbox["+$j+"]"));
				}
		}
		
		_check(_checkbox("confirmCheckbox"));
		_click(_link("atc_button"));
		
		}

	}


_check(_checkbox("confirmCheckbox"));
_click(_link("atc_button"));

if (_isVisible(_div("tie-In")))
	{
	_click(_button("Close"));
	}

	}
}


