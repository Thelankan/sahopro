_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Header&Footer.xls");

var $header=_readExcelFile("Header&Footer.xls","Header");
var $footer=_readExcelFile("Header&Footer.xls","Footer");
var $emailvalidation=_readExcelFile("Header&Footer.xls","FooterEmailValidation");
var $breadcrumbheadings=_readExcelFile("Header&Footer.xls","BreadCrumbsHeadings");


SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("216135/216224/216383/216469","Verify the UI of footer in the application as a guest user/Verify the UI of the Email sign up section in the global Footer./Verify the display of Rights reserved text message in the global footer./Verify the UI of the Email sign up section in global footer in mobile view.");
$t.start();
try
{

//footer customer rating section	
_assertVisible(_div("Customer Satisfaction Ratings", _in(_div("footer-container"))));
//_assertVisible(_paragraph("Customer Satisfaction Ratings",_in(_div("footer-container"))));DH
//email section
_assertVisible(_textbox("dwfrm_emailsignup_emailid"));
_assertVisible(_submit("/signupbtn/"));
//_assertVisible(_submit("dwfrm_emailsignup_signup"));DJ
//_assertVisible(_div($footer[0][0]));
//_assertVisible(_div($footer[0][0]));
_assertEqual($footer[8][3], _getText(_div("/footerPromoHead/")));
_assertVisible(_div("/email-signup/"));

//promotions checkbox
_assert(_checkbox("dwfrm_emailsignup_footeraddtomaillist").checked);
//footer link headings
//var $footerlink_headings=_count("_div","/footerLinksHead/",_in(_div("footer-item-list")));
var $footerlink_headings=_count("_link", "/(.*)/", _in(_div("footer-item")));
var $footerlinks=_collectAttributes("_link", "/(.*)/","sahiText", _in(_div("footer-item")));

for(var $i=0;$i<$footerlink_headings;$i++)
{
//footer configured links
//_assertVisible(_link($footer[$i][2]));
//_assertVisible(_link( $footerlinks[$i]));
_assertVisible(_link($footerlinks[$i], _in(_div("footer-item"))));
//_assertEqual($footer[$i][2], _getText(_div("footerLinksHead["+$i+"]")));	
}

//social icon section
_assertVisible(_div("footer-social"));
_assertVisible(_div("footer-images"));
_assertVisible(_span("footer-keycode"));
//_assertVisible(_listItem("Privacy Policy", _in(_div("footer-policy"))));
_assertVisible(_link("Privacy Policy", _in(_div("footer-privacy"))));
//_assertVisible(_link("Terms & Condition", _in(_div("footer-policy"))));
_assertVisible(_link("Terms & Conditions", _in(_div("footer-privacy"))));

//copyright
_assertVisible(_listItem($footer[2][0], _in(_div("footer-privacy"))));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216222","Verify the UI of the Customer Review section in the global Footer");
$t.start();
try
{

	//_assert(false,"has to be configured in BM test case also bloked");
	_assertVisible(_div($footer[8][3]));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216253","Verify the UI of the Crown Awards Section in the global Footer");
$t.start();
try
{
	var $footerlinks=_collectAttributes("_link", "/(.*)/","sahiText", _in(_div("footer-item")));
for(var $i=0;$i<9;$i++)
{
	//footer configured links
	//_assertVisible(_link($footer[$i][2], _near(_div("Crown Awards"))));
	_assertVisible(_link($footerlinks[$i], _in(_div("footer-item"))));
	
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216254/216255","Verify the UI of the Questions section in the Global Footer./Verify the UI of the Questions section in the Global Footer");
$t.start();
try
{
	
	//display of question section in footer	
	//_assertVisible(_div($footer[1][1], _in(_div("crown_awards"))));
	_assertVisible(_div($footer[1][1], _in(_div("/footer-item-list/"))));
	
	//static links
	_assertVisible(_listItem($footer[0][3]));
	_assertVisible(_listItem($footer[1][3]));
	_assertVisible(_list("crownhours"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("216288","Verify the UI of the about Crown Awards in the global Footer");
$t.start();
try
{
	
for(var $i=0;$i<4;$i++)
{
	//footer configured links
	_assertVisible(_link($footer[$i][4], _near(_div("About Crown Awards"))));
	
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216289","Verify the UI of the Customer Service Section in the Global footer.");
$t.start();
try
{
	
for(var $i=0;$i<5;$i++)
{
	//footer configured links
	_assertVisible(_link($footer[$i][5], _near(_div("Customer Service"))));
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("216294","Verify the UI of the Footer images section in the global");
$t.start();
try
{
	
//checking card links in footer
//_assertVisible(_image("footer-satisfaction-icon.png"));
	_assertVisible(_image("footer_satisfaction_NEW.png"));
_assertVisible(_image("footer-norton-icon.png"));
_assertVisible(_image("footer-cc-icon.png"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216349/216490","Verify the validation of Email address text field in the Global footer/Verify the Validation of Email input field in global Footer in mobile view.");
$t.start();

try

{

for(var $i=0;$i<$emailvalidation.length;$i++)
	{
		_setValue(_textbox("dwfrm_emailsignup_emailid"),$emailvalidation[$i][0]);
		
		_click(_submit("/signupbtn/"));
		
		if ($i==0)
		{
			_assertEqual($emailvalidation[2][1], _getText(_div("field-wrapper")));
		}
		
		else if ($i==1 || $i==2 || $i==3 || $i==4 || $i==5 || $i==6 || $i==7)
			{
			 _assertEqual($emailvalidation[0][1], _getText(_div("field-wrapper")));
			
			}
		else
			{
			
			_assertVisible(_div($emailvalidation[1][1]));
			
			}
	}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216355","Verify the application navigation on click of links in Crown awards section in the global footer.");
$t.start();
try
{

//_assertEqual(false,"still 3 links need to be configured");	
	
for(var $i=0;$i<9;$i++)
{
	//footer configured links
	_assertVisible(_link($footer[$i][2],_near(_div("Crown Awards"))));
	_click(_link($footer[$i][2], _near(_div("Crown Awards"))));

	if($i==3)
		{
			//checking bread crumb
			_assertVisible(_span($breadcrumbheadings[$i][0], _in(_div("breadcrumb"))));
			_assertEqual($breadcrumbheadings[$i][0], _getText(_span("breadcrumb-element last")));
			_assertVisible(_div("Search For Award By sport or Activity"));
			_assertVisible(_textbox("categorySearch", _in(_div("search-box-main"))));		
		}
	
	if($i==8)
	 {
		_assertVisible(_link("Customer Service", _in(_div("breadcrumb"))));
	
	 }
		else
		   {
				//checking bread crumb
				_assertVisible(_span($breadcrumbheadings[$i][0], _in(_div("breadcrumb"))));
				_assertEqual($breadcrumbheadings[$i][0], _getText(_span("breadcrumb-element last")));
				//checking heading
				_assertVisible(_heading1($breadcrumbheadings[$i][1]));
		    }

}	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216358","Verify the application behavior on click of Order History and Order Details link in Global footer as a Guest User.");
$t.start();
try
{
	
//click on Order Status/Order History link in footer link	
_assertVisible(_link($footer[2][5], _near(_div("Customer Service"))));
_click(_link($footer[2][5], _near(_div("Customer Service"))));
//login section should display
_assertVisible(_textbox("/dwfrm_login_username/", _near(_span("Email"))));
_assertVisible(_password("/dwfrm_login_password/", _near(_span("Password"))));
//track order
_assertVisible(_textbox("dwfrm_ordertrack_orderNumber", _near(_span("Order Number"))));
_assertVisible(_textbox("dwfrm_ordertrack_orderEmail", _near(_span("Billing Email"))));
_assertVisible(_textbox("dwfrm_ordertrack_postalCode", _near(_span("Billing ZIP Code"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216381","Verify the application behavior on click of privacy policy link in the global footer");
$t.start();
try
{
	
//click on privacy policy in footer
	_click(_link("Privacy Policy", _in(_div("footer-privacy"))));
//
//_assert(false,"link has to configure");
	_assertVisible(_link("Our Policies - Privacy Policy", _in(_div("breadcrumb"))));
	_assertVisible(_heading1("Privacy Policy"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216382","Verify the application behavior on click of Terms and Condition link in global Footer.");
$t.start();
try
{
	
//click on privacy policy in footer
//_click(_link("Terms & Condition", _in(_div("footer-policy"))));
	_click(_link("Terms & Conditions", _in(_div("footer-privacy"))));
//
//_assert(false,"link has to configure");

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("","");
$t.start();
try
{

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

_click(_link("/user-login/"));
login();

var $t = _testcase("216224","Verify the UI of the Email sign up section in the global Footer.");
$t.start();
try
{
	
//footer customer rating section	
//	_assertVisible(_div("Customer Satisfaction Ratings", _in(_div("footer-rating "))));
	_assertVisible(_div("Customer Satisfaction Ratings", _in(_div("footer-rating desktop-show "))));


//email section
_assertVisible(_textbox("dwfrm_emailsignup_emailid"));
//_assertVisible(_submit("dwfrm_emailsignup_signup"));
_assertVisible(_submit("/signupbtn/"));
//_assertVisible(_div($footer[0][0]));
_assertEqual($footer[4][0], _getText(_div("/footerPromoHead/")));
_assertVisible(_div("/email-signup/"));
//sign button in footer
//_assertVisible(_submit("dwfrm_emailsignup_signup"));
_assertVisible(_submit("/signupbtn/"));


//footer link headings
var $footerlink_headings=_count("_div","/footerLinksHead/", _in(_div("footer-container")))

for(var $i=0;$i<$footerlink_headings;$i++)
{
//footer configured links
_assertVisible(_div($footer[$i][1]));	
_assertEqual($footer[$i][1], _getText(_div("footerLinksHead["+$i+"]")));	
}

//social icon section
_assertVisible(_div("footer-social"));
_assertVisible(_div("footer-images"));
_assertVisible(_span("footer-keycode"));
_assertVisible(_listItem("Privacy Policy",_in(_div("footer-privacy"))));
_assertVisible(_listItem("Terms & Condition", _in(_div("footer-privacy"))));
_assertVisible(_listItem($footer[2][0], _in(_div("footer-privacy"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216294","Verify the UI of the Footer images section in the global");
$t.start();
try
{
	
//checking card links in footer
//_assertVisible(_image("footer-satisfaction-icon.png"));
_assertVisible(_image("footer_satisfaction_NEW.png"));
_assertVisible(_image("footer-norton-icon.png"));
_assertVisible(_image("footer-cc-icon.png"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216359","Verify the application behavior on click of Order History and Order Details link in Global footer as a REG User.");
$t.start();
try
{
	//click on login link
	if (isMobile())
	{
	 _click(_link("/mobile-show/", _in(_div("user-info"))));
	}
	else
		{
		 _click(_link("/desktop-show/", _in(_div("user-info"))));
		 //Visibility of overlay
		 _assertVisible(_div("dialog-container"));
		}

//login to the application
login();

//click on Order Status/Order History link in footer link	
_assertVisible(_link($footer[2][5], _near(_div("Customer Service"))));
_click(_link($footer[2][5], _near(_div("Customer Service"))));

//Order detail page
_assertVisible(_link("Order History", _in(_div("breadcrumb"))));
//order tables
_assertVisible(_div("CartTableLeft"));
//search order
_assertVisible(_textbox("searchorder"));
//question
_assertVisible(_div("questions"));
_assertVisible(_link("continue shopping"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("216290/216361/216360/216362","Verify the UI of the Social Share Icons in the Global Footer/Verify the application behavior on click of Printrest icon in global footer./Verify the application behavior on click of Facebook link in global footer./Verify the application behavior on click of Blog icon in the global footer in global footer.");
$t.start();
try
{

//Social icon links
for(var $i=0;$i<3;$i++)
{

	//click on socila icons
	_click(_image($footer[$i][6]));
	
	_wait(5000);
	var $recentWinID=_getRecentWindow().sahiWinId;
	_log($recentWinID);
	_selectWindow($recentWinID);
	
	//verifyUrl
	var $url=window.location.href;
	
	//var $url=_getRecentWindow().windowURL;
	//var $url=window.document.location.href;

	
	if ($i==0)
		{
		_log($i);
		//Verifying URL
		_assertEqual($footer[$i][7],$url);
		_assertVisible(_link("Facebook logo"));
		}
	else if ($i==1)
		{
		_log($i);
		//Verifying URL
		_assertEqual($footer[$i][7],$url);
		_assertVisible(_image("awardLogo.png"));
		}
	else if ($i==2)
		{
		_log($i);
		//Verifying URL
		_assertEqual($footer[$i][7],$url);
		_assertVisible(_link("fa-pinterest-square"));	
		}
		else
			{
			_log($i);
			_popup("Norton Secured Seal")._assertVisible(_image("footer-norton-icon.png"));
			}

	
	_closeWindow();
	_selectWindow();
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

/*var $t = _testcase("","");
$t.start();
try
{

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()*/