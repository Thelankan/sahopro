_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Header&Footer.xls");

var $header=_readExcelFile("Header&Footer.xls","Header");
var $footer=_readExcelFile("Header&Footer.xls","Footer");


SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

var $t = _testcase("215960","Verify the Functionality of the Logo in the Global header as a Guest User");
$t.start();
try
{
	//Application logo
	_assertVisible(_image("Crown Awards", _in(_div("primary-logo"))));
	// _assertVisible(_heading1("primary-logo"));
	// _assertEqual($header[0][0], _getText(_heading1("primary-logo")));
	// _assertContainsText($header[0][0], _heading1("primary-logo"));
	_assertContainsText("Crown Awards", _link("Crown Awards", _in(_div("primary-logo"))));
	 
	 //Navigate to category landing page
	 _click(_link("Trophies"));
	 _assertVisible(_list("/search-result-items/"));
	 
	 //Click on logo
	 NavigatetoHomepage();
	 _assertNotVisible(_list("/search-result-items/"));
	 _assertVisible(_div("home-main-right"));
	 
	//content slots
	 _assertVisible(_div("html-slot-container"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();;

var $t = _testcase("215874/215909/215875","Verify the UI of the Header in Desktop View as a Guest User/Verify the UI of the Logo section in the Global header as a Guest user/Verify the UI of the Cart section in the Global header as a guest User.");
$t.start();
try
{

//Verifying headerUI for guest user 
headerUI($header,$header[0][1]);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();;

var $t = _testcase("215910/215911","Verify the UI of the Search Section in the Global Header as a Guest User./Verify the UI of the Select Sport or Activity section in the Global header as a Guest User.");
$t.start();
try
{
	
	//Search bar section
	_assertVisible(_textbox("/Search/"));
	_assertVisible(_submit("Search"));
	//Text should display
	//_assertEqual($header[2][0], _getValue(_textbox("Search Catalog"))); not ableto to fetch the text change by dh
	
	//Sport of Activity Drop down
	_assertVisible(_div("dropdownul"));
	_assertVisible(_link("drop-downul"));
	_assertVisible(_link($header[1][0], _in(_div("dropdownul"))));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();;

var $t = _testcase("215912","Verify the UI of the global Menu in the Global header as a Guest User");
$t.start();
try
{
	
//Stock categories
var $stockcategories=_count("_link","/menuitems/", _in(_listItem("/stockcategories/")));
//Custom categories
var $customcategories=_count("_link","/menuitems/", _in(_listItem("/customcategories/")));
_assertEqual($header[1][2],$stockcategories);
_assertEqual($header[2][2],$customcategories);
var $total=$stockcategories+$customcategories;
_assertEqual($header[0][2],$total);
var $totalcategories=_count("_link","/menuitems/", _in(_list("menu-category level-1")))-1;
_assertEqual($header[0][2],$totalcategories);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();;

var $t = _testcase("215976","Verify the Chat link functionality in the global header as a guest user.");
$t.start();
try
{
	
//Verifying 
//_assertVisible(_link("/bold-chat-link/"));
	_assertVisible(_link("/bold-chat-link/", _in(_div("header-right"))));

_mouseOver(_link("/bold-chat-link/", _in(_div("header-right"))));

 if (_getAttribute(_link("/bold-chat-link/"),"title")!=null)
 {
     _assert(true);
 }
else
{
     _assert(false);
}
 
//click on chat link
_click(_link("/bold-chat-link/"));
_wait(3000);
var $recentWinID=_getRecentWindow().sahiWinId;
_log($recentWinID);
_selectWindow($recentWinID);

//verify chat link popup
_assertVisible(_image("Crown Awards"));
_assertVisible(_bold("Support Chat"));
_assertVisible(_table("chat_survey_table"));

_closeWindow();
_selectWindow();

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();;

var $t = _testcase("215985","Verify the application behvaior on click of Your Orders link in the header as a Guest user.");
$t.start();
try
{
 //click on orders link
 _click(_link("/order-history/"));
 
 _assertVisible(_heading1("ORDERS"));
 _assertVisible(_link("Orders", _in(_div("breadcrumb"))));
 _assertVisible(_heading1("Order Status/Track Order"));
 _assertVisible(_heading2("Review a specific order"));
 _assertVisible(_textbox("dwfrm_ordertrack_orderNumber"));
 _assertVisible(_textbox("dwfrm_ordertrack_orderEmail"));
 _assertVisible(_textbox("dwfrm_ordertrack_postalCode"));
 _assertVisible(_submit("dwfrm_ordertrack_findorder"));
 _assertVisible(_label("Order Number*"));
// _assertVisible(_label("Order Email*"));
 _assertVisible(_span("Billing Email"));
 _assertVisible(_label("Billing ZIP Code*"));
 _assertVisible(_submit("dwfrm_ordertrack_findorder"));
 
 _assertVisible(_heading1("Order History"));
 _assertVisible(_heading2("See your previous orders"));
 
 _assertVisible(_textbox("/dwfrm_login_username/"));
 _assertVisible(_password("/dwfrm_login_password/"));
// _assertVisible(_label("Email:*"));
 _assertVisible(_label("Email*"));
//_assertVisible(_label("Password:*"));
 _assertVisible(_label("Password*"));
 _assertVisible(_submit("/dwfrm_login/"));
 
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();;

var $t = _testcase("216022","Verify if the can trigger the Select Sports and Activities in the Global header as a Guest user.");
$t.start();
try
{
	
 // Sport of Activity Drop down
 _assertVisible(_link($header[1][0], _in(_div("dropdownul"))));
 //click on drop down to see the links
 _click(_link("drop-downul", _in(_div("dropdownul"))));
 //Checking configured links
 _assertVisible(_listItem("dropdownleft"));
 _assertVisible(_listItem("dropdownright"));
 _log("********** Not verifying each and every link as it is a content assert ****************");

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("216061","Verify the navigation to search suggestion page from the global header as a Guest user");
$t.start();
try
{

//Searching a product
search($header[0][3]);
//page should navigate to particular category page
//_assertContainsText($header[0][3], _heading1("cya-title"));dhiraj
//_assertContainsText($header[0][3], _heading1("cya-title bigtext-line0"));
_assertEqual($header[0][3], _getText(_heading1("bigtext-line0")));

_assertVisible(_list("/search-result-items/"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("216064","Verify if the user can navigate to the search result page through the Enter key as a guest user");
$t.start();
try
{
	
//Searching a product
_setValue(_textbox("/Search/"),$header[0][3]);
_focus(_submit("Search", _in(_div("/Search/"))));
_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
//page should navigate to particular category page
//_assertContainsText($header[0][3], _heading1("cya-title"));
//_assertContainsText($header[0][3], _heading1("cya-title bigtext-line0"));
_assertContainsText($header[0][3], _heading1("TROPHIES"));
_assertVisible(_list("/search-result-items/"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("216089","Verify the application behavior on Mouse hover of Payment link in the global header as a Guest user.");
$t.start();
try
{

//mouse hover on payment link	
 _mouseOver(_link("/paymentlink/"));	
 if (_getAttribute(_link("/paymentlink/"),"title")!=null)
 {
     _assert(true);
 }
	else
	{
	     _assert(false);
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("216095","Verify the application behavior on click of a link value  in the Select a sport or activity drop down as a guest user.");
$t.start();
try
{
	
//Click on select sport dropdown
_click(_link($header[1][0], _in(_div("dropdownul"))));
//click on any link Select sport of Activity drop down
_click(_link($header[1][3]));
//Should navigate to bass ball page
_assertVisible(_span($header[1][3], _in(_div("breadcrumb"))));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("216104","Verify the application behavior on click of cart icon or link in the global header as a guest user.");
$t.start();
try
{
	
//click on cart link when it has no product
_click(_span("value", _in(_span("minicart-quantity desktop-show"))));
//empty cart heading
_assertVisible(_heading1($header[0][4]));
_assertVisible(_submit("dwfrm_cart_continueShopping"));

//add product to cart
addItemToCart($header[0][7],$header[0][8]);

//click on cart link after adding the product
_click(_span("value", _in(_span("minicart-quantity desktop-show"))));
//should navigate to cart page
_assertVisible(_div("CartTableLeft "));
_assertVisible(_div("CartWrap"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//clear cart
ClearCartItems();

var $t = _testcase("216106/216102","Verify the application navigation on click of View Shopping Cart link in the application as a Guest user in the Global header.");
$t.start();
try
{
	
//click on View Shopping Cart link when it has no product
_mouseOver(_span("value", _in(_span("minicart-quantity desktop-show"))));
_assertVisible(_div("Your Shopping Cart is currently empty."));
_assertVisible(_link("View Shopping Cart (0 Items)"));

//navigate to Cart page
//_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));
_click(_link("/(.*)/", _in(_div("minicart"))));
//empty cart heading
_assertVisible(_heading1($header[0][4]));
_assertVisible(_submit("dwfrm_cart_continueShopping"));

//add product to cart
addItemToCart($header[0][7],$header[0][8]);
//click on View Shopping Cart link when it has no product
_mouseOver(_span("value", _in(_span("/minicart-quantity/"))));
//_assertEqual("Your Cart 1 Item(s)", _getText(_div("miniCartTitle")));
_assertEqual("Your Cart 2 Items", _getText(_div("miniCartTitle")));
//_assertVisible(_link("Cart"));
_assertVisible(_link("mini-cart-link productpresent"));

//_assertVisible(_link("Go to Cart"));


//navigate to Cart page
//_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));
_click(_link("/(.*)/", _in(_div("minicart"))));
//should navigate to cart page
_assertVisible(_div("CartTableLeft "));
_assertVisible(_div("CartWrap"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("216110","Verify the application behavior on click of checkout button when cart has a product as a guest user");
$t.start();
try
{
	//_click(_link("/(.*)/", _in(_div("miniCartCheckout"))));
	//_click(_link("/(.*)/", _in(_div("minicart"))));
		_assertVisible(_link("/mini-cart-link/", _in(_div("navmenu"))));
		_click(_link("/mini-cart-link-checkout-link/"));

		//should navigate to shipping page
		_assertVisible(_heading2("Shipping Address"));
		_assertVisible(_div("Have an Account? Log In(optional)"));

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("216114/216117","Verify the application behavior on click of FAQ link in the Global header as a guest user.");
$t.start();
try
{
	//FAQ link
	_click(_link($header[0][5]));
	//respective page heading and breadcrumb verification
	//_assertVisible(_span($header[0][5], _in(_div("breadcrumb"))));
	//_assertVisible(_link($header[0][5], _in(_div("breadcrumb"))));
	_assertVisible(_link("breadcrumb-element last", _in(_div("breadcrumb"))));
	_assertVisible(_heading1($header[0][6]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("","Verify the application behavior on click of Email Us link in the Global header as a guest user.");
$t.start();
try
{
	_click(_link($header[3][5] ,_in(_div("help-tooltip hovermenu hide"))));
	//respective page heading and breadcrumb verification
	_assertVisible(_link($header[4][5], _in(_div("breadcrumb"))));
	_assertVisible(_heading1($header[3][6]));	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("216976","Verify if the user can enter text in the Select Sport of Activity Drop down for a Guest user.");
$t.start();
try
{

//Try to set value in Select Sport Or Activity dropdown	
//_setValue(_link($header[1][0]),$header[0][3]);
	_click(_link("Trophies", _in(_list("dropdown-menu"))));
//value should not enter 
// var $existingvalue=_getText(_link("drop-downul", _in(_div($header[1][0]))));
 var $existingvalue=_getText(_link($header[1][0], _in(_div("dropdownul"))))


_assertNotEqual($header[0][3],$existingvalue);
_assertEqual($header[1][0],$existingvalue);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


/*var $t = _testcase("","");
$t.start();
try
{

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/

_log("******************** Header Register user flow *******************")

	if (isMobile())
	{
	   //click on login link
	   _click(_link("/mobile-show/", _in(_div("user-info"))));	
	}
	  else
		  {
		   //click on login link
		   _click(_link("/desktop-show/", _in(_div("user-info"))));
		   //Visibility of overlay
		   _assertVisible(_div("dialog-container"));
		  }
login();

var $t = _testcase("215967","Verify the display of User name in header as a Reg user in the global header.");
$t.start();
try
{

//var $name=_getText(_listItem("Hi Crown ( Log Out )")).split(" ")[1];
//_assertEqual($User[$user][1],$name);
//	_assertNotVisible(_link("/desktop-show/", _in(_div("user-info"))));
//	_assertVisible(_link("/user-logout/"));
	
_wait(3000);
_assertVisible(_listItem("Log In Log In Hi "+$User[$user][1]+" ( Log Out )"));

_assertNotVisible(_link("/user-login/"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("215962","Verify the Functionality of the Logo in the Global header as a Registered User");
$t.start();
try
{
	
 //Application logo
 _assertVisible(_image("Crown Awards", _in(_div("primary-logo"))));
 //_assertVisible(_heading1("primary-logo"));
 //_assertContainsText($header[0][0], _heading1("primary-logo"));
 _assertVisible(_link($header[0][0], _in(_div("primary-logo"))));
 
 //Navigate to category landing page
 _click(_link("Trophies"));
 _assertVisible(_list("/search-result-items/"));
 
 //Click on logo
 NavigatetoHomepage();
 _assertNotVisible(_list("/search-result-items/"));
 _assertVisible(_div("home-main-right"));
 
 var $count=_count("_list","horizontal-carousel", _in(_div("home-product-carousel ipad-desktop-show")));
 
for(var $i=0;$i<$count;$i++)
{
 _assertVisible(_list("horizontal-carousel["+$i+"]", _in(_div("home-product-carousel ipad-desktop-show"))));
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("215913/215914/215915","Verify the UI of the Header in Desktop View as a Registered User./Verify the UI of the Cart section in the Global header as a Registered user./Verify the UI of the Logo section in the Global header as a Registered User.");
$t.start();
try
{
	//Verifying headerUI for reg user 
	headerUI($header,"REGISTER");
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("215916/215918","Verify the UI of the Search Section in the Global Header as a Registered User/Verify the UI of the Select Sport or Activity section in the Global header as a Registered User.");
$t.start();
try
{
	
//Search bar section
_assertVisible(_textbox("/Search/"));
_assertVisible(_submit("Search"));
//Text should display
//_assertEqual($header[2][0], _getValue(_textbox("Search Catalog")));

//Sport of Activity Drop down
_assertVisible(_div("dropdownul"));
_assertVisible(_link("drop-downul"));
_assertVisible(_link($header[1][0], _in(_div("dropdownul"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("215920","Verify the UI of the global Menu in the Global header as a Registered User");
$t.start();
try
{
	
//Stock categories
var $stockcategories=_count("_link","/menuitems/", _in(_listItem("/stockcategories/")));
//Custom categories
var $customcategories=_count("_link","/menuitems/", _in(_listItem("/customcategories/")));
_assertEqual($header[1][2],$stockcategories);
_assertEqual($header[2][2],$customcategories);
var $total=$stockcategories+$customcategories;
_assertEqual($header[0][2],$total);
var $totalcategories=_count("_link","/menuitems/", _in(_list("menu-category level-1")))-1;
_assertEqual($header[0][2],$totalcategories);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("215984","Verify the application behavior on click of chat link as a Registered user.");
$t.start();
try
{
	//Verifying 
	_assertVisible(_link("bold-chat-link menuitems", _in(_div("navmenu"))));

	_mouseOver(_link("/bold-chat-link/"));

	 if (_getAttribute(_link("/bold-chat-link/"),"title")!=null)
	 {
	     _assert(true);
	 }
	else
	{
	     _assert(false);
	}
	 
	//click on chat link
	_click(_link("/bold-chat-link/"));
	_wait(3000);
	var $recentWinID=_getRecentWindow().sahiWinId;
	_log($recentWinID);
	_selectWindow($recentWinID);

	//verify chat link popup
	_assertVisible(_image("Crown Awards"));
	_assertVisible(_bold("Support Chat"));
	_assertVisible(_table("chat_survey_table"));

	_closeWindow();
	_selectWindow();

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("215986","Verify if the application behavior on click of Your Orders link in global header as a REG user.");
$t.start();
try
{
	 //click on orders link
	 _click(_link("/order-history/"));
	 //should navigate to order history page
	 _assertVisible(_heading1("Order History"));
	 _assertVisible(_link("Order History", _in(_div("breadcrumb"))));
	 //order tables
	 _assertVisible(_div("order-hostory"));
	 //search order
	 _assertVisible(_textbox("searchorder"));
	 //question
	 _assertVisible(_div("questions"));
	 _assertVisible(_link("continue shopping"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("216022","Verify if the can trigger the Select Sports and Activities in the Global header as a Guest user.");
$t.start();
try
{
	
 // Sport of Activity Drop down
 _assertVisible(_link($header[1][0], _in(_div("dropdownul"))));
 //click on drop down to see the links
 _click(_link("drop-downul", _in(_div("dropdownul"))));
 //Checking configured links
 _assertVisible(_listItem("dropdownleft"));
 _log("********** Not verifying each and every link as it is a content assert ********************");

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("216062","Verify the navigation to search suggestion page from the global header as a REG user");
$t.start();
try
{
_click(_link("Crown Awards"));
_wait(3000);
//Searching a product
search($header[0][3]);
_wait(5000);
//page should navigate to particular category page
//breadcrumb
_assertEqual($header[0][3], _getText(_span("breadcrumb-element last")));
//result page
_assertVisible(_list("/search-result-items/"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("216066","Verify if the user can navigate to the search result page through the Enter key as a REG user.");
$t.start();
try
{
	
//Searching a product
_setValue(_textbox("/Search/"),$header[0][3]);
_focus(_submit("Search", _in(_div("/Search/"))));
_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
//page should navigate to particular category page
//breadcrumb
_assertEqual($header[0][3], _getText(_span("breadcrumb-element last")));
//result page
_assertVisible(_list("/search-result-items/"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("216095","Verify the application behavior on click of a link value  in the Select a sport or activity drop down as a guest user.");
$t.start();
try
{
//Click on select sport dropdown
_click(_link($header[1][0], _in(_div("dropdownul"))));
//click on any link Select sport of Activity drop down
_click(_link($header[1][3]));
//Should navigate to bass ball page
_assertVisible(_span($header[1][3], _in(_div("breadcrumb"))));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("216109","Verify the application behavior on click of Checkout button in global header when no product list part of cart as a REG user.");
$t.start();
try
{

//Navigate to home page
NavigatetoHomepage();	
//click on checkout link in header with  	
_click(_link("/mini-cart-link-checkout-link/"));
//Handling popup
_expectConfirm("/Shopping Cart is currently empty,cannot Checkout/", true); 

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("216105/16107/216099","Verify the application behavior on click of cart icon or link in the global header as a REG user.");
$t.start();
try
{
	addItemToCart($header[0][7],$header[0][8]);
	_wait(2000);
	_click(_link("/mini-cart-link/"));
	_assertVisible(_div("cart-row box-row "));
	_assertVisible(_div("Shopping Cart", _in(_div("step-1 active"))));
	_assertVisible(_italic("sprite sprite-cart_bottom_checkout"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("216111","Verify the application behavior on click of checkout button when cart has a product as a REG user.");
$t.start();
try
{

	_click(_link("/(.*)/", _in(_div("miniCartCheckout"))));
	//should navigate to shipping page
	_assertVisible(_heading2("Shipping Address"));
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("216115/216118","Verify the application behavior on click of FAQ link in the Global header as a REG user.");
$t.start();
try
{

_click(_link($header[0][5]));
//respective page heading and breadcrumb verification
_assertVisible(_link("Crown Frequently Asked Questions: How To Order", _in(_div("breadcrumb"))));
_assertVisible(_heading1($header[0][6]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();;

var $t = _testcase("","Verify the application behavior on click of 'Email us' link in the Global header as a REG user.");
$t.start();
try
{
	
	_click(_link($header[3][5] ,_in(_div("help-tooltip hovermenu hide"))))
	//respective page heading and breadcrumb verification
	_assertVisible(_link($header[4][5], _in(_div("breadcrumb"))));
	_assertVisible(_heading1($header[3][6]));	
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("216977","Verify if the user can enter text in the Select Sport of Activity Drop down for a REG user.");
$t.start();
try
{

	//Try to set value in Select Sport Or Activity dropdown	
	_setValue(_link($header[1][0]),$header[0][3]);
	//value should not enter 
	//var $existingvalue=_getText(_link("drop-downul", _in(_div($header[1][0]))));
	var $existingvalue=_getText(_link($header[1][0], _in(_div("dropdownul"))));
	_assertNotEqual($header[0][3],$existingvalue);
	_assertEqual($header[1][0],$existingvalue);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();;



//************************************************REmoved test cases********************************************


//var $t=_testcase("216112","Verify the application behvaior on click of Help link in the global header as a Guest User.");
//$t.start();
//try
//{
//	//click on help link
//	_click(_link("/help-link/"));
//	//FAQ, Chat Live, Call Us , Email US links should be displayed 
//	_assertVisible(_link($header[0][5]));
//	_assertVisible(_link($header[1][5]));
//	_assertVisible(_link($header[2][5]));
//	_assertVisible(_link($header[3][5], _in(_div("help-tooltip hovermenu hide"))));
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();



//var $t = _testcase("216113","Verify the application behvaior on click of Help link in the global header as a REG User.");
//$t.start();
//try
//{
//
////visibility of help link in header	
//_assertVisible(_link("/help-link/"));
////click on help link
//_click(_link("/help-link/"));
////FAQ, Chat Live, Call Us , Email US links should be displayed 
//_assertVisible(_link($header[0][5]));
//_assertVisible(_link($header[1][5]));
//_assertVisible(_link($header[2][5]));
////_assertVisible(_link($header[3][5]));
//_assertVisible(_link($header[3][5], _in(_div("help-tooltip hovermenu hide"))));
//
//
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();
