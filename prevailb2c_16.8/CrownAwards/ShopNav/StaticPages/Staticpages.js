_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Staticpages.xls");

var $static_data=_readExcelFile("Staticpages.xls","Data");
var $contactvalidations=_readExcelFile("Staticpages.xls","ContactValidations");
var $Catalog_Val=_readExcelFile("Staticpages.xls","Catalog_Val");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("218506","Verify the UI of about us left nav in Desktop/tablet");
$t.start();
try
{
	//breadcrumb
	_assertVisible(_link("Customer Service", _in(_div("breadcrumb"))));
	//heading
	_assertEqual("Contact Crown Awards", _getText(_heading1("content-header")));
	//call us section
	_assertVisible(_div("call-us-module"));
	//chat section
	_assertVisible(_div("chat-module-section"));
	//email us section
	_assertVisible(_div("email-us-section"));
	//left nav
	_assertVisible(_div("About Crown", _in(_div("secondary-navigation"))));
	_assertVisible(_div("FAQ", _in(_div("secondary-navigation"))));
	_assertVisible(_link("/Shipping & Delivery/", _in(_div("secondary-navigation"))));
	_assertVisible(_link("/Our Policies/", _in(_div("secondary-navigation"))));
	_assertVisible(_div("Crown Requests", _in(_div("secondary-navigation"))));
	_assertVisible(_heading3("Contact Crown", _in(_div("secondary-navigation"))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("218935/218937","Verify the display of continue shopping button in About pages for desktop/tablet/Verify the functionality of 'Continue Shopping' button in the about pages for desktop/tablet");
$t.start();
try
{
	
	_click(_link($static_data[0][0]));
	//click on continue shopping link
	_assertVisible(_link("/(.*)/", _in(_div("continue-shopping"))));
	_click(_link("/(.*)/", _in(_div("continue-shopping"))));
	//verify the home page
	_assertNotVisible(_div("breadcrumb"));
	_assertVisible(_div("home-main-right"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("219191","Verify the UI of 'CALL US' module in contact us page for desktop/tablet");
$t.start();
try
{

	//click on contact us in footer  	
	_click(_link($static_data[1][0],_near(_div("Customer Service"))));
	//heading
	_assertEqual("Call Us", _getText(_div("/module-header/")));
	//day time section
	_assertVisible(_div("day-timings"));
	//customer service
	_assertVisible(_div("customer-service-section"));
	//custom department
	_assertVisible(_div("customer-department-section"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("219261/219261/219263/219267/219270/219271/219274/219275/220457/219205","Verify the contents displayed in 'Your issue' drop down in the Email Us section-contact us page for desktop/tablet");
$t.start();
try
{

var $contactusOptions=_getText(_select("dwfrm_contactus_yourissues"));
_assertEqual($static_data[0][1],$contactusOptions.length);

var $orderno=0;
var $itemQuantity=0;

for (var $i=1;$i<$contactusOptions.length;$i++)
{

_setSelected(_select("dwfrm_contactus_yourissues"),$contactusOptions[$i]);
_assertEqual($contactusOptions[$i],_getSelectedText(_select("dwfrm_contactus_yourissues")));

if ($i==1)
	{
	_assertVisible(_textbox("dwfrm_contactus_ordernumbertostatus"));
	_assertVisible(_textbox("dwfrm_contactus_billingzipcode"));
	_assertVisible(_textbox("dwfrm_contactus_billingemail"));
	_assertVisible(_link("check-email-status"));
	_assertVisible(_submit("email-us-submit"));
	_assertVisible(_div("dialog-required"));
	_assertVisible(_label("Billing Zipcode*"));
	_assertVisible(_label("Billing Email*"));
	
	if (_isVisible(_label("Order Number*["+$orderno+"]")))
		{
		_assertVisible(_label("Order Number*["+$orderno+"]"));
		$orderno++;
		}
	}
	else if ($i==2)
		{
		_assertVisible(_textarea("dwfrm_contactus_itemnumberandqty"));
		//_assertVisible(_textbox("dwfrm_contactus_ordernumbertotime"));
		_assertVisible(_textbox("dwfrm_contactus_address"));
		_assertVisible(_textbox("dwfrm_contactus_city"));
		_assertVisible(_select("dwfrm_contactus_states_state"));
		_assertVisible(_label("Enter the item numbers and quantities of each product:*["+$itemQuantity+"]"));
		_assertVisible(_label("Address*"));
		_assertVisible(_label("City*"));
		_assertVisible(_label("State*"));
		
		if (_isVisible(_label("Enter the item numbers and quantities of each product:*["+$itemQuantity+"]")))
		{
		_assertVisible(_label("Enter the item numbers and quantities of each product:*["+$itemQuantity+"]"));
		$itemQuantity++;
		}
		
		if (_isVisible(_label("Order Number*["+$orderno+"]")))
		{
		_assertVisible(_label("Order Number*["+$orderno+"]"));
		$orderno++;
		}
		
		}
	else if ($i==3)
		{
		_assertVisible(_textbox("dwfrm_contactus_ordernumbetoproof"));
		if (_isVisible(_label("Order Number*["+$orderno+"]")))
		{
		_assertVisible(_label("Order Number*["+$orderno+"]"));
		$orderno++;
		}
		
		}
	else if ($i==4)
		{
		_assertVisible(_textbox("dwfrm_contactus_ordernumbertorefund"));
		if (_isVisible(_label("Order Number*["+$orderno+"]")))
		{
		_assertVisible(_label("Order Number*["+$orderno+"]"));
		$orderno++;
		}
		
		}
	else if ($i==5)
		{
		_assertVisible(_textarea("dwfrm_contactus_orderitemnumberandqty"));
		_assertVisible(_textbox("dwfrm_contactus_ordernumbertoadditems"));
		if (_isVisible(_label("Enter the item numbers and quantities of each product:*["+$itemQuantity+"]")))
		{
		_assertVisible(_label("Enter the item numbers and quantities of each product:*["+$itemQuantity+"]"));
		$itemQuantity++;
		}
		}
	else if ($i==6)
		{
		_assertVisible(_textarea("dwfrm_contactus_representativecall"));
		_assertVisible(_label("What do you need us to call you about?*"));
		}
	else 
		{
		_assertVisible(_textarea("dwfrm_contactus_yourquestion"));
		_assertVisible(_label("your Question*"));
		}

}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("219308/220274/219244/219245/219246/219259/219265/219265/219308/219315/219312/219320/219323/219328/219606/219607/219316","Verify the validation of 'Enter the item numbers and quantities of each product' text field in the Email Us form for desktop/tablet");
$t.start();
try
{ 
var $contactusOptions=_getText(_select("dwfrm_contactus_yourissues"));
_assertEqual($static_data[0][1],$contactusOptions.length);
		
	for (var $i=1;$i<$contactusOptions.length;$i++)
	{

	//entering valid values in all the fields
	_setValue(_textbox("dwfrm_contactus_email"),$static_data[0][5]);
	_setValue(_textbox("dwfrm_contactus_firstname"),$static_data[0][6]);
	_setValue(_textbox("dwfrm_contactus_lastname"),$static_data[0][7]);
	_setValue(_textbox("dwfrm_contactus_phone"),$static_data[0][8]);
	_setSelected(_select("dwfrm_contactus_yourissues"),$contactusOptions[$i]);

	if ($i==1)
		{
		
		for (var $j=0;$j<3;$j++)
			{
			//entering valid values in all the fields
			_setValue(_textbox("dwfrm_contactus_email"),$static_data[0][5]);
			_setValue(_textbox("dwfrm_contactus_firstname"),$static_data[0][6]);
			_setValue(_textbox("dwfrm_contactus_lastname"),$static_data[0][7]);
			_setValue(_textbox("dwfrm_contactus_phone"),$static_data[0][8]);
			_setSelected(_select("dwfrm_contactus_yourissues"),$contactusOptions[$i]);
			
			_setValue(_textbox("dwfrm_contactus_ordernumbertostatus"),$contactvalidations[$j][2]);
			_setValue(_textbox("dwfrm_contactus_billingzipcode"),$contactvalidations[$j][3]);
			_setValue(_textbox("dwfrm_contactus_billingemail"),$contactvalidations[$j][4]);
			//click on submit button
			_click(_link("check-email-status"));
			
			if ($j==0)
				{
					_assertEqual("Please enter your Order Number.", _getText(_span("dwfrm_contactus_ordernumbertostatus-error")));
					_assertEqual("Please enter your billing ZIP code.", _getText(_span("dwfrm_contactus_billingzipcode-error")));
					_assertEqual("Please enter your billing email.", _getText(_span("dwfrm_contactus_billingemail-error")));
				}
			else if ($j==1)
				{
				  _assertEqual("Please enter at least 5 characters.", _getText(_span("dwfrm_contactus_billingzipcode-error")));
				}
			else
				{
					_assert(false,"Should navigate to respective page if order no is correct");
				}
			
			}
		}
		else if($i==2)
		{
		
		for (var $l=4;$l<7;$l++)
			{
			//entering valid values in all the fields
			_setValue(_textbox("dwfrm_contactus_email"),$static_data[0][5]);
			_setValue(_textbox("dwfrm_contactus_firstname"),$static_data[0][6]);
			_setValue(_textbox("dwfrm_contactus_lastname"),$static_data[0][7]);
			_setValue(_textbox("dwfrm_contactus_phone"),$static_data[0][8]);
			_setSelected(_select("dwfrm_contactus_yourissues"),$contactusOptions[$i]);
			
			_setValue(_textarea("dwfrm_contactus_itemnumberandqty"),$contactvalidations[$l][2]);
			_setValue(_textbox("dwfrm_contactus_ordernumbertotime"),$contactvalidations[$l][2]);
			_setValue(_textbox("dwfrm_contactus_address"),$contactvalidations[$l][2]);
			_setValue(_textbox("dwfrm_contactus_city"),$contactvalidations[$l][2]);
			_setValue(_textbox("dwfrm_contactus_state"),$contactvalidations[$l][2]);
			//click on submit button
			_click(_submit("submit-button"));
			
			if ($l==4)
				{
				_assertVisible(_span("This field is required.",_near(_textarea("dwfrm_contactus_itemnumberandqty"))));
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_contactus_ordernumbertotime"))));
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_contactus_address"))));
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_contactus_city"))));
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_contactus_state"))));
				}
			else if ($l==5)
				{
				_assertVisible(_span("This field is required.",_near(_textarea("dwfrm_contactus_itemnumberandqty"))));
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_contactus_ordernumbertotime"))));
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_contactus_address"))));
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_contactus_city"))));
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_contactus_state"))));
				}
			else
				{
				_assert(false,"Should navigate to respective page");
				}
			
			}
		}
		else if($i==3)
		{
		
		for (var $m=8;$m<11;$m++)
			{
			//entering valid values in all the fields
			_setValue(_textbox("dwfrm_contactus_email"),$static_data[0][5]);
			_setValue(_textbox("dwfrm_contactus_firstname"),$static_data[0][6]);
			_setValue(_textbox("dwfrm_contactus_lastname"),$static_data[0][7]);
			_setValue(_textbox("dwfrm_contactus_phone"),$static_data[0][8]);
			_setSelected(_select("dwfrm_contactus_yourissues"),$contactusOptions[$i]);
			
			_setValue(_textbox("dwfrm_contactus_ordernumbetoproof"),$contactvalidations[$m][2]);
			//click on submit button
			_click(_submit("submit-button"));
			
			if ($m==8)
				{
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_contactus_ordernumbetoproof"))));
				}
			else if ($m==9)
				{
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_contactus_ordernumbetoproof"))));
				}
			else
				{
				_assert(false,"Should navigate to respective page");
				}
			
			}
		}

		else if($i==4)
		{
		
		for (var $n=12;$n<15;$n++)
			{
			//entering valid values in all the fields
			_setValue(_textbox("dwfrm_contactus_email"),$static_data[0][5]);
			_setValue(_textbox("dwfrm_contactus_firstname"),$static_data[0][6]);
			_setValue(_textbox("dwfrm_contactus_lastname"),$static_data[0][7]);
			_setValue(_textbox("dwfrm_contactus_phone"),$static_data[0][8]);
			_setSelected(_select("dwfrm_contactus_yourissues"),$contactusOptions[$i]);
			
			_setValue(_textbox("dwfrm_contactus_ordernumbertorefund"),$contactvalidations[$n][2]);
			//click on submit button
			_click(_submit("submit-button"));
			
			if ($n==12)
				{
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_contactus_ordernumbertorefund"))));
				}
			else if ($n==13)
				{
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_contactus_ordernumbertorefund"))));
				}
			else
				{
				_assert(false,"Should navigate to respective page");
				}
			
			}
		}

		else if($i==5)
		{
		
		for (var $p=17;$p<20;$p++)
			{
			//entering valid values in all the fields
			_setValue(_textbox("dwfrm_contactus_email"),$static_data[0][5]);
			_setValue(_textbox("dwfrm_contactus_firstname"),$static_data[0][6]);
			_setValue(_textbox("dwfrm_contactus_lastname"),$static_data[0][7]);
			_setValue(_textbox("dwfrm_contactus_phone"),$static_data[0][8]);
			_setSelected(_select("dwfrm_contactus_yourissues"),$contactusOptions[$i]);
			
			_setValue(_textarea("dwfrm_contactus_orderitemnumberandqty"),$contactvalidations[$p][2]);
			_setValue(_textbox("dwfrm_contactus_ordernumbertoadditems"),$contactvalidations[$p][2]);
			//click on submit button
			_click(_submit("submit-button"));
			
			if ($p==17)
				{
				_assertVisible(_span("This field is required.",_near(_textarea("dwfrm_contactus_orderitemnumberandqty"))));
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_contactus_ordernumbertoadditems"))));
				}
			else if ($p==18)
				{
				_assertVisible(_span("This field is required.",_near(_textarea("dwfrm_contactus_orderitemnumberandqty"))));
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_contactus_ordernumbertoadditems"))));
				}
			else
				{
				_assert(false,"Should navigate to respective page");
				}
			
			}
		}
		else if($i==6)
		{
		
		for (var $r=21;$r<23;$r++)
			{
			//entering valid values in all the fields
			_setValue(_textbox("dwfrm_contactus_email"),$static_data[0][5]);
			_setValue(_textbox("dwfrm_contactus_firstname"),$static_data[0][6]);
			_setValue(_textbox("dwfrm_contactus_lastname"),$static_data[0][7]);
			_setValue(_textbox("dwfrm_contactus_phone"),$static_data[0][8]);
			_setSelected(_select("dwfrm_contactus_yourissues"),$contactusOptions[$i]);
			
			_setValue(_textarea("dwfrm_contactus_representativecall"),$contactvalidations[$r][2]);
			//click on submit button
			_click(_submit("submit-button"));
			
			if ($r==21)
				{
				_assertVisible(_span("This field is required.",_near(_textarea("dwfrm_contactus_representativecall"))));
				}
			else if ($r==22)
				{
				_assertVisible(_span("This field is required.",_near(_textarea("dwfrm_contactus_representativecall"))));
				}
			else
				{
				_assert(false,"Should navigate to respective page");
				}
			
			}
		}
		else
			{
			
			for (var $s=24;$s<27;$s++)
			{
				//entering valid values in all the fields
				_setValue(_textbox("dwfrm_contactus_email"),$static_data[0][5]);
				_setValue(_textbox("dwfrm_contactus_firstname"),$static_data[0][6]);
				_setValue(_textbox("dwfrm_contactus_lastname"),$static_data[0][7]);
				_setValue(_textbox("dwfrm_contactus_phone"),$static_data[0][8]);
				_setSelected(_select("dwfrm_contactus_yourissues"),$contactusOptions[$i]);
			
			_setValue(_textarea("dwfrm_contactus_yourquestion"),$contactvalidations[$s][2]);
			//click on submit button
			_click(_submit("submit-button"));
			
			if ($s==24)
				{
				_assertVisible(_span("This field is required.",_near(_textarea("dwfrm_contactus_yourquestion"))));
				}
			else if ($s==25)
				{
				_assertVisible(_span("This field is required.",_near(_textarea("dwfrm_contactus_yourquestion"))));
				}
			else
				{
				_assert(false,"Should navigate to respective page");
				}
			
			}
			
			}

	}
	
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("218534/220335/220336/220337/220333","Verify the navigation of links in the 'about crown' section in left nav in desktop/tablet in Contact Us Page/Verify the appication navigaion on click on Breadcrumb links in About Crown Pages." +
		"Verify the application behavior on mouse hover of the Links in breadcrumb displayed in the About CROWN  page");
$t.start();
try
{

	//click on contact us in footer  	
	_click(_link($static_data[1][0],_near(_div("Customer Service"))));
	var $leftnavcrown=_collectAttributes("_link","/(.*)/","sahiText",_in(_div("LeftNav",_near(_div("About Crown", _in(_div("LeftNav")))))));

	for (var $s=0;$s<$leftnavcrown.length;$s++)
	{
		
		_click(_link($leftnavcrown[$s], _in(_div("LeftNav"))));
		
		//bread crumb
		_assertVisible(_link($static_data[$s][9], _in(_div("breadcrumb"))));
		
		//mouse hover on home link in bread crumb
		_mouseOver(_link($static_data[$s][9], _in(_div("breadcrumb"))));	
		
		if (_getAttribute(_link($static_data[$s][9],_in(_div("breadcrumb"))),"title")!=null)
		{
		    _assert(true);
		}
			else
			{
			    _assert(false);
			}
		
		//mouse hover on home link in bread crumb
		_mouseOver(_link("Home", _in(_div("breadcrumb"))));	

		if (_getAttribute(_link("Home"),"title")!=null)
		{
		    _assert(true);
		}
			else
			{
			    _assert(false);
			}
		
		//click on last element in bread crumb
		_click(_link($static_data[$s][9], _in(_div("breadcrumb"))));
		//still application should be in same page
		_assertVisible(_link($static_data[$s][9], _in(_div("breadcrumb"))));
		//click on home link in breadcrumb
		_click(_link("Home", _in(_div("breadcrumb"))));
		//application should be in home page
		_assertVisible(_div("home-promocontent"));
		//verify the home page
		_assertNotVisible(_div("breadcrumb"));
		_assertVisible(_div("home-main-right"));	
		
		//click on contact us in footer  	
		_click(_link($static_data[1][0],_near(_div("Customer Service"))));
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()



var $t = _testcase("218535/219341","Verify the navigation of links in the 'FAQ'' section in left nav in desktop/tablet in Contact Us Page");
$t.start();
try
{
	
	//click on contact us in footer  	
	_click(_link($static_data[1][0],_near(_div("Customer Service"))));
	var $leftnavfaq=_collectAttributes("_link","/(.*)/","sahiText",_in(_div("LeftNav",_near(_div("FAQ", _in(_div("LeftNav[1]")))))));



	for (var $s=0;$s<$leftnavfaq.length;$s++)
	{
	    _click(_link($leftnavfaq[$s], _in(_div("LeftNav[1]"))));	
	    _assertVisible(_link("Crown Frequently Asked Questions: "+$leftnavfaq[$s], _in(_div("breadcrumb"))));

		//click on contact us in footer  	
		_click(_link($static_data[1][0],_near(_div("Customer Service"))));
		
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("218536/218537","Verify the navigation of  'Shipping and Delivery ' link in left nav in desktop/tablet in Contact Us Page");
$t.start();
try
{
	
//click on contact us in footer  	
_click(_link($static_data[1][0],_near(_div("Customer Service"))));
//_assert(false,"data not configured");
//vishal
//click on link shipping and delivery link
_click(_link("/"+$static_data[2][4]+"/", _in(_div("secondary-navigation"))));
_assertVisible(_link("/"+$static_data[6][4]+"/", _in(_div("breadcrumb"))));

_assertVisible(_heading1($static_data[2][4]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("219178/220338/220402","Verify the UI of about crown pages template for desktop/tablet/Verify the application behavior on mouse hover of Continue Shopping button/Verify the UI of the Page Title Section in the About Crown Page");
$t.start();
try
{
	
//click on our history
_click(_link($static_data[0][9], _near(_div("About Crown Awards"))));
//bread crumb
_assertVisible(_link($static_data[0][9], _in(_div("breadcrumb"))));
_assertVisible(_heading1($static_data[0][11]));
//mouse hover on continue shopping button
_mouseOver(_link("/(.*)/", _in(_div("continue-shopping"))));

if (_getAttribute(_link("/(.*)/", _in(_div("continue-shopping"))),"title")!=null)
	{
	    _assert(true);
	}
		else
		{
		    _assert(false);
		}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("220456/219190/219190/219197/220458/221274","Verify the UI of the Live chat Section in the Contact Us page/Verify the UI of Contact us page for desktop/tablet");
$t.start();
try
{
//click on contact us in footer  	
_click(_link($static_data[1][0],_near(_div("Customer Service"))));
//ui of lie chat section
_assertVisible(_div("chat-module-section"));
_assertVisible(_link("Chat",_div("chat-module-section")));
_assertVisible(_heading1("CONTACT CROWN AWARDS"));
_assertVisible(_link("Customer Service", _in(_div("breadcrumb"))));
_assertVisible(_div("call-us-module-header"));
_assertVisible(_div("call-us-module"));
_assertVisible(_div("email-us-section"));

//functionality of bread crumb
_click(_link("Customer Service", _in(_div("breadcrumb"))));
//remains in the same page
_assertVisible(_heading1("CONTACT CROWN AWARDS"));
//functionality of bread crumb
_click(_link("Home", _in(_div("breadcrumb"))));
//verify the home page
_assertNotVisible(_div("breadcrumb"));
_assertVisible(_div("home-main-right"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("221284/221285","Verify the functionality of catalogs images/view catalog links in online catalog landing page");
$t.start();
try
{    //vishal
	_click(_link($static_data[2][0], _near(_div("footer-icons"))));
	//_click(_link($static_data[2][0]));
     _assertVisible(_div($static_data[10][9], _in(_div("breadcrumb"))));
	//View catalog link below the image
	_assertVisible(_link($static_data[3][0], _in(_div("/CROWN AWARDS CATALOGS/"))));
	_click(_link("/(.*)/", _in(_div("/CROWN AWARDS CATALOGS/"))));
	//_click(_link("View 2015 Catalog", _in(_div("/CROWN AWARDS CATALOGS/"))));
	//breadcrumb
	_assertVisible(_div($static_data[9][9], _in(_div("breadcrumb"))));
	//Heading
	_assertVisible(_heading1("CROWN AWARDS CATALOG"));
	//image
	_assertVisible(_div("onlinecatalogmain"));
	_assertVisible(_div("getcatalog"));

	//_click(_image("/(.*)/", _in(_div("/CROWN AWARDS CATALOGS/"))));
	//Catalog page
	//_assertVisible(_heading1("CROWN AWARDS CATALOG"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("221329/221280/221282","Verify the application behavior on click of breadcrumb in online catalog page for desktop/tablet");
$t.start();
try
{   //vishal
	_click(_link($static_data[2][0], _near(_div("footer-icons"))));
	//_click(_link($static_data[2][0]));
     _assertVisible(_div($static_data[10][9], _in(_div("breadcrumb"))));
	//click on Home
	_click(_link("breadcrumb-element"));
	//breadcrumb
	_assertNotVisible(_div("breadcrumb"));
	//_click(_link($static_data[2][0]));
	//click on view our catalog
	_click(_link($static_data[2][0], _near(_div("footer-icons"))));
	//breadcrumb
	_assertVisible(_div($static_data[10][9], _in(_div("breadcrumb"))));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("221283","Verify the contents displayed in catalog landing page");
$t.start();
try
{    ////vishal
	//View online catalog link in footer
	_click(_link($static_data[2][0], _near(_div("footer-icons"))));
	//breadcrumb
	_assertVisible(_div($static_data[10][9], _in(_div("breadcrumb"))));
	//_click(_link($static_data[2][0]));
	//View catalog link below the image
	_assertVisible(_link($static_data[3][0], _in(_div("/CROWN AWARDS CATALOGS/"))));
	_click(_link("/(.*)/", _in(_div("/CROWN AWARDS CATALOGS/"))));
	//_click(_link("View 2015 Catalog", _in(_div("/CROWN AWARDS CATALOGS/"))));
	//breadcrumb
	_assertVisible(_div($static_data[9][9], _in(_div("breadcrumb"))));
	//Heading
	_assertVisible(_heading1("CROWN AWARDS CATALOG"));
	//image
	_assertVisible(_div("onlinecatalogmain"));
	_assertVisible(_div("getcatalog"));

	//_click(_image("/(.*)/", _in(_div("/CROWN AWARDS CATALOGS/"))));
	//Catalog page
	//_assertVisible(_heading1("CROWN AWARDS CATALOG"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("221332","Verify the functionality of request catalog button in online catalog page for desktop/tablet.");
$t.start();
try
{
	 ////vishal
	//View online catalog link in footer
	_click(_link($static_data[2][0], _near(_div("footer-icons"))));
	//breadcrumb
	_assertVisible(_div($static_data[10][9], _in(_div("breadcrumb"))));
	//_click(_link($static_data[2][0]));
	//View catalog link below the image
	_assertVisible(_link($static_data[3][0], _in(_div("/CROWN AWARDS CATALOGS/"))));
	_click(_link("/(.*)/", _in(_div("/CROWN AWARDS CATALOGS/"))));
	//_click(_link("View 2015 Catalog", _in(_div("/CROWN AWARDS CATALOGS/"))));
	//breadcrumb
	_assertVisible(_div($static_data[9][9], _in(_div("breadcrumb"))));
	//Heading
	_assertVisible(_heading1("CROWN AWARDS CATALOG"));
	//image
	_assertVisible(_div("onlinecatalogmain"));
	//click on request ctalog button
	_click(_link("catalog-req"));
	//breadcrumb
	_assertVisible(_div($static_data[9][9], _in(_div("breadcrumb"))));
	_assertVisible(_div("request-catalog-header"));
	_assertVisible(_div("catalog-image-section"));
	_assertVisible(_div("footer-container"));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("221279","Verify the different ways to reach online catalog landing page for desktop/tablet.");
$t.start();
try
{
	//View online catalog link in footer
	_click(_link($static_data[2][0], _near(_div("footer-icons"))));
	//breadcrumb
	_assertVisible(_div($static_data[10][9], _in(_div("breadcrumb"))));
	//_click(_link($static_data[2][0]));
	//View catalog link below the image
	_assertVisible(_link($static_data[3][0], _in(_div("/CROWN AWARDS CATALOGS/"))));
	//Link in left anv
	_click(_link($static_data[2][0], _in(_div("static-page-left-nav"))));
	//breadcrumb
	_assertVisible(_div($static_data[10][9], _in(_div("breadcrumb"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("219330/218541","Verify the UI of FAQ page for desktop/tablet");
$t.start();
try
{
	//FAQ link in footer
	_click(_link($static_data[1][4],_near(_div("Customer Service"))));
	//breadcrumb
	_assertVisible(_div($static_data[11][9], _in(_div("breadcrumb"))));
	//heading
	_assertVisible(_heading1($static_data[12][9]));
	//Search
	_assertVisible(_div("searched-text"));
	_assertVisible(_textbox("faqsearchfield"));
	_assertVisible(_submit("faqsearchbutton"));
	//How to order heading
	_assertVisible(_listItem("order-list "));
	_assertVisible(_div("faq-answers"));
	//top link
	_assertVisible(_div("faq-link"));
	//Continue shopping
	//vishal
	//_assertVisible(_image("continue-shopping"));
	_assertVisible(_link("/(.*)/", _in(_div("continue-shopping"))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("219601/219602/220277/220278/220262/220265/220266","FN/LN/Addr1/Addr2/Zip/Country/State/city/phone/email/confirm email/Organization");
$t.start();
try
{    //vishal
	_click(_link("/Request A FREE Catalog/", _near(_div("footer-icons"))))
	for (var $i=0;$i<$Catalog_Val.length;$i++) 
	{
	
		if ($i==0)
			{
				_setValue(_textbox("dwfrm_catalogrequest_firstname"),"");
				_setValue(_textbox("dwfrm_catalogrequest_lastname"),"");
				_setValue(_textbox("dwfrm_catalogrequest_address1"),"");
				_setValue(_textbox("dwfrm_catalogrequest_address2"),"");
				_setValue(_textbox("dwfrm_catalogrequest_zipcode"),"");
				_setSelected(_select("dwfrm_catalogrequest_country"),"");
				_setSelected(_select("dwfrm_catalogrequest_states_state"),"");
				_setValue(_textbox("dwfrm_catalogrequest_city"),"");
				_setValue(_textbox("dwfrm_catalogrequest_phone"),"");
				_setValue(_textbox("dwfrm_catalogrequest_email"),"");
				_setValue(_textbox("dwfrm_catalogrequest_emailconfirm"),"");
				_setValue(_textbox("dwfrm_catalogrequest_organization"),"");
			}
		else
			{
			   RequestCatalogData($Catalog_Val,$i);
			}
		
		//click on submit button
		_click(_submit("dwfrm_catalogrequest_requestcatalog"));
		
		if ($i==0)
			{
			//_assertEqual(true,(_submit("dwfrm_catalogrequest_requestcatalog").disabled));
			_assertVisible(_textbox("dwfrm_catalogrequest_email", _near(_span("This field is required."))));
			_assertVisible(_textbox("dwfrm_catalogrequest_emailconfirm", _near(_span("This field is required."))));
			_assertVisible(_textbox("dwfrm_catalogrequest_firstname", _near(_span("This field is required."))));
			_assertVisible(_textbox("dwfrm_catalogrequest_lastname", _near(_span("This field is required."))));
			_assertVisible(_textbox("dwfrm_catalogrequest_address1", _near(_span("This field is required."))));
			_assertVisible(_textbox("dwfrm_catalogrequest_zipcode", _near(_span("This field is required."))));
			_assertVisible(_textbox("dwfrm_catalogrequest_city", _near(_span("This field is required."))));
			_assertVisible(_select("dwfrm_catalogrequest_states_state", _near(_span("This field is required."))));
			_assertVisible(_textbox("dwfrm_catalogrequest_phone", _near(_span("This field is required."))));
			_assertEqual("rgb(248, 233, 233)",_style(_textbox("dwfrm_catalogrequest_email"),"background-color"))
			_assertEqual("rgb(248, 233, 233)",_style(_textbox("dwfrm_catalogrequest_emailconfirm"),"background-color"))
			_assertEqual("rgb(248, 233, 233)",_style(_textbox("dwfrm_catalogrequest_firstname"),"background-color"))
			_assertEqual("rgb(248, 233, 233)",_style(_textbox("dwfrm_catalogrequest_lastname"),"background-color"))
			_assertEqual("rgb(248, 233, 233)",_style(_textbox("dwfrm_catalogrequest_address1"),"background-color"))
			_assertEqual("rgb(248, 233, 233)",_style(_textbox("dwfrm_catalogrequest_zipcode"),"background-color"))
			_assertEqual("rgb(248, 233, 233)",_style(_textbox("dwfrm_catalogrequest_city"),"background-color"))
			_assertEqual("rgb(255, 255, 255)", _style(_select("dwfrm_catalogrequest_states_state"), "background-color"))
			_assertEqual("rgb(248, 233, 233)",_style(_textbox("dwfrm_catalogrequest_phone"),"background-color"))
			}

		else if ($i==1)
			{
				_assertEqual($Catalog_Val[0][12],_getText(_textbox("dwfrm_catalogrequest_firstname")).length);
				_assertEqual($Catalog_Val[0][12],_getText(_textbox("dwfrm_catalogrequest_lastname")).length);
				_assertEqual($Catalog_Val[1][12],_getText(_textbox("dwfrm_catalogrequest_address1")).length);
				_assertEqual($Catalog_Val[1][12],_getText(_textbox("dwfrm_catalogrequest_address2")).length);
				_assertEqual($Catalog_Val[1][12],_getText(_textbox("dwfrm_catalogrequest_city")).length);
				_assertEqual($Catalog_Val[0][14],_getText(_textbox("dwfrm_catalogrequest_phone")).length);
				_assertEqual($Catalog_Val[0][13],_getText(_textbox("dwfrm_catalogrequest_zipcode")).length);
				_assertEqual($Catalog_Val[0][12],_getText(_textbox("dwfrm_catalogrequest_email")).length);
				_assertEqual($Catalog_Val[0][12],_getText(_textbox("dwfrm_catalogrequest_emailconfirm")).length);
				_assertEqual($Catalog_Val[2][12],_getText(_textbox("dwfrm_catalogrequest_organization")).length);
			}
		else if ($i==2 || $i==3)
			{
				_assertEqual("rgb(248, 233, 233)",_style(_textbox("dwfrm_catalogrequest_phone"),"background-color"))
				_assert(true, "Zip code is taking less than 3 values");
			}
			else if ($i==4 || $i==5)
			{
				_assertEqual("Please check and re-enter your email address.", _getText(_span("dwfrm_catalogrequest_email-error")));
				_assertEqual("Please check and re-enter your email address.", _getText(_span("dwfrm_catalogrequest_emailconfirm-error")));
				_assertEqual("Please specify a valid phone number.", _getText(_span("dwfrm_catalogrequest_phone-error")));
				_assertEqual("This field is required.", _getText(_span("dwfrm_catalogrequest_sportactivivty-error")));
		
	    	}
			else if ($i==6)
				{
				
				_assertVisible(_select("dwfrm_catalogrequest_states_state", _near(_span("This field is required."))));
				_assertEqual("rgb(255, 255, 255)", _style(_select("dwfrm_catalogrequest_states_state"), "background-color"))
	
				}
			else
			{
				//form should be submitted
				_assertVisible(_div("requestcat"));
			}
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219615","Verify the validation of 'Would you like a call from a representative?' radio button in the Request a catalog page for desktop/tablet");
$t.start();
try
{
	_click(_link("/Request A FREE Catalog/", _near(_div("footer-icons"))))
	//Would you like a call from representative
	//Yes radio  button
	_assert(_radio("true", _in(_div("form-row representative required"))).checked);
	//No radio button
	_assertNotTrue(_radio("false", _in(_div("form-row representative required"))).checked);
	//Enter valid data
	RequestCatalogData($Catalog_Val,"7");
	//click on submit button
	_click(_submit("dwfrm_catalogrequest_requestcatalog"));
	//form should be submitted
	_assertVisible(_div("requestcat"));
	_click(_link("/Request A FREE Catalog/", _near(_div("footer-icons"))))
	//Click on No radio button
	_click(_radio("false", _in(_div("form-row representative required"))));
	RequestCatalogData($Catalog_Val,"7");
	//click on submit button
	_click(_submit("dwfrm_catalogrequest_requestcatalog"));
	//form should be submitted
	_assertVisible(_div("requestcat"));
	_assertVisible(_div("requestcat"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219612","Verify the validation of 'Award quantity you would like to order' drop down in the Request a catalog page for desktop/tablet");
$t.start();
try
{  
	
	//vishal
	_click(_link("/Request A FREE Catalog/", _near(_div("footer-icons"))))
	var $quantity=_getText(_select("dwfrm_catalogrequest_quantity")).toString().split(",");
	for(var $i=0;$i<$quantity.length;$i++)
	{
		RequestCatalogData($Catalog_Val,"7");
		_setSelected(_select("dwfrm_catalogrequest_quantity"), $quantity[$i]);
		//click on submit button
		_click(_submit("dwfrm_catalogrequest_requestcatalog"));
		//form should be submitted
		_assertVisible(_div("requestcat"));
		_click(_link("/Request A FREE Catalog/", _near(_div("footer-icons"))))
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219610","Verify the validation of 'Sport or Activity that Interests you' drop down in the Request a catalog page for desktop/tablet");
$t.start();
try
{    //click on Request A FREE Catalog
	_click(_link("/Request A FREE Catalog/", _near(_div("footer-icons"))))

	var $sport=_getText(_select("dwfrm_catalogrequest_sportactivivty"));
	for(var $i=1;$i<$sport.length;$i++)
	{
		_setValue(_textbox("dwfrm_catalogrequest_firstname"), $Catalog_Val[7][1]);
		_setValue(_textbox("dwfrm_catalogrequest_lastname"), $Catalog_Val[7][2]);
		_setValue(_textbox("dwfrm_catalogrequest_address1"), $Catalog_Val[7][3]);
		_setValue(_textbox("dwfrm_catalogrequest_address2"),$Catalog_Val[7][4]);
		_setValue(_textbox("dwfrm_catalogrequest_zipcode"), $Catalog_Val[7][8]);
		_setSelected(_select("dwfrm_catalogrequest_country"),$Catalog_Val[7][5]);
		_setSelected(_select("dwfrm_catalogrequest_states_state"), $Catalog_Val[7][6]);
		_setValue(_textbox("dwfrm_catalogrequest_city"), $Catalog_Val[7][7]);
		_setValue(_textbox("dwfrm_catalogrequest_phone"),$Catalog_Val[7][9]);
		_setValue(_textbox("dwfrm_catalogrequest_email"),$Catalog_Val[7][10]);
		_setValue(_textbox("dwfrm_catalogrequest_emailconfirm"),$Catalog_Val[7][10]);
		_setValue(_textbox("dwfrm_catalogrequest_organization"),$Catalog_Val[7][11]);
		//select the sport
		_setSelected(_select("dwfrm_catalogrequest_sportactivivty"), $sport[$i]);
		//click on submit button
		_click(_submit("dwfrm_catalogrequest_requestcatalog"));
		//form should be submitted
		_assertVisible(_div("requestcat"));
		_click(_link("/Request A FREE Catalog/", _near(_div("footer-icons"))))
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("218539","Verify the navigation of links in the 'contact crown' section in left nav in desktop/tablet");
$t.start();
try
{
	
//click on contact us in footer  	
_click(_link($static_data[1][0],_near(_div("Customer Service"))));
_assertVisible(_heading1("/Contact/"));

var $count=_count("_div","/content-asset/",_in(_div("static-page-left-nav")))-1;
var $test=_collectAttributes("_link","/(.*)/","sahiText",_in(_div("content-asset["+$count+"]",_in(_div("static-page-left-nav")))))

for (var $i=1;$i<$test.length-1;$i++)
	{
	_click(_link($test[$i],_near(_heading3("Contact Crown"))));
	
	
		if($i==1 || $i==2)
		{
			_assertVisible(_div("main-content", _in(_div("breadcrumb"))));
			_assertVisible(_heading1("content-header", _near(_div("breadcrumb"))));


		}
		else if($i==4)
		{
			_assertVisible(_div("main-content", _in(_div("breadcrumb"))));
			_assertVisible(_heading1($static_data[2][12]));

		}
		else
			{
			_wait(3000);
			var $recentWinID=_getRecentWindow().sahiWinId;
			_log($recentWinID);
			_selectWindow($recentWinID);
			 
			//verify chat link popup
			_assertVisible(_image("Crown Awards"));
			_assertVisible(_bold("Support Chat"));
			_assertVisible(_table("chat_survey_table"));
			 
			_closeWindow();
			_selectWindow();
			}
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()



//Functions

function RequestCatalogData($sheet,$i)
{
	_setValue(_textbox("dwfrm_catalogrequest_firstname"), $sheet[$i][1]);
	_setValue(_textbox("dwfrm_catalogrequest_lastname"), $sheet[$i][2]);
	_setValue(_textbox("dwfrm_catalogrequest_address1"), $sheet[$i][3]);
	_setValue(_textbox("dwfrm_catalogrequest_address2"),$sheet[$i][4]);
	_setValue(_textbox("dwfrm_catalogrequest_zipcode"), $sheet[$i][8]);
	_setSelected(_select("dwfrm_catalogrequest_country"),$sheet[$i][5]);
	_setSelected(_select("dwfrm_catalogrequest_states_state"), $sheet[$i][6]);
	_setValue(_textbox("dwfrm_catalogrequest_city"), $sheet[$i][7]);
	_setValue(_textbox("dwfrm_catalogrequest_phone"),$sheet[$i][9]);
	_setValue(_textbox("dwfrm_catalogrequest_email"),$sheet[$i][10]);
	_setValue(_textbox("dwfrm_catalogrequest_emailconfirm"),$sheet[$i][10]);
	_setValue(_textbox("dwfrm_catalogrequest_organization"),$sheet[$i][11]);
	_setSelected(_select("dwfrm_catalogrequest_sportactivivty"),$sheet[$i][12]);
}
