_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Category_landing.xls");

var $City_data=_readExcelFile("Category_landing.xls","CityPages");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

function NavigateToCityPage()
{
	_navigateTo($City_data[0][0]);
}

var $t = _testcase("","");
$t.start();
try
{
	NavigateToCityPage();
	//Left nav
	_assertVisible(_div("home-main-left"));
	//Product corousel section 
	_assertVisible(_div("home-scroll-right single-prodect-slide"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


/*var $t = _testcase("","");
$t.start();
try
{



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/