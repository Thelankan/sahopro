_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Category_landing.xls");

var $awardbysport = _readExcelFile("Category_landing.xls", "AwardBySport");
var $insertfinder = _readExcelFile("Category_landing.xls", "Insert_finder");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("216286","Verify the navigation of the Crown nav category");
$t.start();
try
{

//navigate to basket ball page	
NavigatetoSubcat($awardbysport[5][0]);	
//_assertVisible(_div("left-refinement"));chnage by dhiraj
_div("refinement category-refinement");

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216223","Verify the navigation of Custom cateory landing page in desktop/tablet");
$t.start();
try
{

//navigate to basket ball page	
NavigatetoSubcat($awardbysport[0][4]);	
//_assertVisible(_div("left-refinement"));change by dhiraj
_div("refinement category-refinement");
_assertVisible(_heading1($awardbysport[1][4]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("218394/218396/218398/218415/218416 ", "Verify the navigation of Trophies category landing page,Verify the display of page title in Trophies category landing page,Verify the display of middle Choose Your Sport dropdown in Trophies category" +
 " landing page,Verify the functionality of Zoom button in Trophies category landing page");
$t.start();
try {

 //logo
 _click(_image("/crown-awards-logo/"));
 //click on trophies
 _click(_link($insertfinder[2][0]));
 //category landing page
 _assertEqual($insertfinder[2][0], _getText(_span("breadcrumb-element last")));
 //_assertEqual("Home+" + ">" + "+Trophies", _getText(_link("breadcrumb-element")) + " " + _getText(_span("breadcrumb-element last")));chnage by dhiraj
 _assertContainsText("Home Trophies", _div("breadcrumb"));
 //page title
 _assertVisible(_heading1($insertfinder[0][3]));
 // choose your sport dropdown
 //_assertVisible(_link("drop-downul", _in(_div("text-codes desktop-show"))));//dropdown is not avilable in this page by dhiraj
 //_assertEqual($insertfinder[1][3], _getText(_link("drop-downul", _in(_div("text-codes desktop-show")))));//dropdown is not avilable in this page by dhiraj
 //click on any product shop now button
 //_click(_image("/shop-now-btn/"));change by dhiraj
 _link("show-now-link[1]");
 //PDP
 //_assertVisible(_div("pdpMain"));
 //click on zoom button in the PDP
 //_click(_link("Zoom", _in(_div("product-attributes zoom"))));
 //new window
 var $recentWinID = _getRecentWindow().sahiWinId;
 _log($recentWinID);
 _selectWindow($recentWinID);
 //verifyUrl
 var $url = window.location.href;
_assert("do some operations")

 _closeWindow();
 _selectWindow();


} 

catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end()


var $t = _testcase("218376/218377/218375/218374", "Verify the functionality of breadcrumb in CategoryFeatures 3 Products products page in Desktop/tablet,Verify the display of page title in CategoryFeatures" +
 " 3 Products products page in Desktop/tablet,Verify the display of breadcrumb in CategoryFeatures 3 Products products page in Desktop/tablet,Verify the navigation CategoryFeatures 3 Products " +
 "products page in Desktop/tablet");
$t.start();
try {
 //logo
 _click(_image("/crown-awards-logo/"));
 //click on Ring category
 _click(_link("Rings"));
 //PlP
 _assertVisible(_div("primary-content ribbon-head"));
 //breadcrumb
 //_assertEqual("Home+" + ">" + "+Rings", _getText(_link("breadcrumb-element")) + " " + _getText(_span("breadcrumb-element last")));change by  dhiraj
 _assertContainsText("Home Rings", _div("breadcrumb"));
 _click(_span("breadcrumb-element last"));
 _assertVisible(_div("primary-content ribbon-head"));
 //heading
 _assertVisible(_heading1("CHAMPIONSHIP RINGS AWARDS"));
 //click on home in the breadcrumb
 _click(_link("breadcrumb-element"));
 //naviagtion to home page
 _assertVisible(_div("home-main-right"));

}
catch ($e) 
{
 _logExceptionAsFailure($e);
}
$t.end()
//chage by change by dhiraj
var $t = _testcase("218385", "Verify the display of Shop Now button in CategoryFeatures 3 Products products page in Desktop/tablet");
$t.start();
try {
 //logo
 _click(_image("/crown-awards-logo/"));
 //click on Ring category
 //_click(_link("Rings"));
 _click(_link("Trophies"));
 //PlP
 _assertVisible(_div("primary-content ribbon-head"));
 //breadcrumb
 //_assertEqual(Home + " +" > "+ " + Rings, _getText(_link("breadcrumb-element")) + " " + _getText(_span("breadcrumb-element last")));change by dhiraj
 //_assertContainsText("Home Rings", _div("breadcrumb"));
 _assertContainsText("Trophies", _div("breadcrumb"));
 
 //shop now come under image
 //_assertVisible(_image("null"), _under(_image("/shop-now-btn/")));
 //_assertVisible(_image("null"), _div("/product-image no-quickview/"));//change by dhiraj
 //click on shopnow button
// _click(_image("/shop-now-btn/"));change by dhiraj
 _click(_image("Gift Certificate[1]"));
 //PDP
 _assertVisible(_div("pdpMain"));//not going to Pdp page error page is displayed

} 
catch ($e) 
{
 _logExceptionAsFailure($e);
}
$t.end()

var $t = _testcase("218391", "Verify the display of price range in CategoryFeatures 3 Products");
$t.start();
try {
 //logo
 _click(_image("/crown-awards-logo/"));
//click on Ring category
 _click(_link("Rings"));
 
$pricerange=_collectAttributes("_div","/product-attributes product-pricing/","sahiText",_in(_list("search-result-items tiles-container clearfix hide-compare mobile-grid-width")))

for ($i = 0; $i <$pricerange.lengh; $i++) 
{
  var $minprice = _getText(_div("product-attributes product-pricing[$i]")).split(" ")[0]
  var $maxprice = _getText(_div("product-attributes product-pricing[$i]")).split(" ")[2]
  _assert($minprice < $maxprice)

 }
}
catch ($e) 
{
 _logExceptionAsFailure($e);
}
$t.end()

var $t = _testcase("218392/218381", "Verify the display of price range in CategoryFeatures 3 Products,Verify the display and functionality of" +
 " Pagination in CategoryFeatures 3 Products products page in Desktop/tablet");
$t.start();
try {
 //logo
 _click(_image("/crown-awards-logo/"));
 //click on Ring category
 _click(_link("Rings"));
 //pagination
 pagination();
} 
catch ($e) 
{
 _logExceptionAsFailure($e);
}
$t.end()



/*var $t = _testcase("218393 ","Verify the display of Botton text content in CategoryFeatures 3 Products");
$t.start();
try
{
	
	
	
	
	
	
	
}

catch($e)
{
		_logExceptionAsFailure($e);
}	
$t.end()
*/


//----------------------------------------City pages category testcases----------------------------------------------------------

var $t = _testcase("218420","Verify the display of Page title in City Pages");
$t.start();
try
{
	
	
	
	
	
	
	
}

catch($e)
{
		_logExceptionAsFailure($e);
}	
$t.end()

/*var $t = _testcase("","");
$t.start();
try
{



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()*/