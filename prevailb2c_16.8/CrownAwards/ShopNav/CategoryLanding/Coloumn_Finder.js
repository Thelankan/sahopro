_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Category_landing.xls");

var $columnfinder=_readExcelFile("Category_landing.xls","ColumnFinder");


//SiteURLs();
_setAccessorIgnoreCase(true); 
//cleanup();

/*
var $t = _testcase("216094/216096/216101/216103","Verify the navigation of Column finder product listing page in desktop/tablet/Verify the display of Message below the heading of Column finder product listing page in desktop/tablet" +
		"Verify the display of heading for carousel below the heading of Column finder product listing page in desktop/tablet");
$t.start();
try
{

//Navigate to homepage
NavigatetoHomepage();
//Click on Insert finder link in the left nav
_click(_link($columnfinder[0][0], _in(_listItem($columnfinder[0][0]))));
_assertVisible(_div("breadcrumb"));
_assertEqual($columnfinder[1][0], _getText(_div("column-finder-heading")));
_assertVisible(_div("columnfinder-carousel"));
_assertVisible(_heading2($columnfinder[2][0]));
_assertEqual($columnfinder[3][0], _getText(_div("carousel-heading")));

	//Display of breadcrumb
	if(isMobile())
	{
		_assertEqual($columnfinder[1][1], _getText(_div("breadcrumb")));
	}
	else
		{
			_assertEqual($columnfinder[0][1], _getText(_div("breadcrumb")));
		}	
_assertContainsText($columnfinder[0][0], _span("breadcrumb-element"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216098/216100","Verify the functionality of bread crumb in Column finder product listing page in desktop/tablet / Verify the display of heading in Column finder product listing page in desktop/tablet");
$t.start();
try
{

//Click on last element in bread crumb	
_click(_span("breadcrumb-element last"));
//Should not navigate to any where
_assertVisible(_div("breadcrumb"));
_assertEqual($columnfinder[1][0], _getText(_div("column-finder-heading")));
_assertVisible(_div("columnfinder-carousel"));

	//Display of breadcrumb
	if(isMobile())
	{
		_assertEqual($columnfinder[1][1], _getText(_div("breadcrumb")));
	}
	else
		{
			_assertEqual($columnfinder[0][1], _getText(_div("breadcrumb")));
		}
	
//functionality of bread crumb
_click(_link("Home", _in(_div("breadcrumb"))));
//verify the home page
_assertNotVisible(_div("breadcrumb"));
_assertVisible(_div("home-main-right"));	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216116","Verify the display of Column carousel in Column finder product listing page in desktop/tablet");
$t.start();
try
{

//Click on Insert finder link in the left nav
_click(_link($columnfinder[0][0], _in(_listItem($columnfinder[0][0]))));
//display of column carousel
_assertVisible(_list("horizontal-carousel"));
_assertVisible(_div("columnfinder-carousel"));
_assertVisible(_button("slick-next",_in(_div("columnfinder-carousel"))));
_assertVisible(_button("slick-prev slick-disabled",_in(_div("columnfinder-carousel"))));

//display of column ID's in carousal section
var $count=_assertVisible(_button("slick-next",_in(_div("columnfinder-carousel"))));

for (var $i=0;$i<$count.length;$i++)
{
	_assertVisible(_div("product-ID["+$i+"]",_in(_div("columnfinder-carousel"))));
	_assert(false,"column description we can ignore this also but for safe side should confirm");
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216136/216137","Verify the display of pagination  links in Coulmn finder Product listing in desktop/tablet / Verify the navigation of pagination  links in Coulmn finder Product listing in desktop/tablet");
$t.start();
try
{

//Navigate to homepage
NavigatetoHomepage();
//Click on Insert finder link in the left nav
_click(_link($columnfinder[0][0], _in(_listItem($columnfinder[0][0]))));
//verifying pagination
pagination();

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()
*/

var $t = _testcase("216141/216143/216145","Verify the display of options in Show dropdown in Column finder product listing page in desktop/tablet / Verify the functionality of selecting different show options in Show dropdown in Column finder product listing page in desktop/tablet" +
		"Verifying the default option in Show dropdown on page load in Coulmn finder Product listing in desktop/tablet");
$t.start();
try
{
	
	//Click on Insert finder link in the left nav
	_click(_link($columnfinder[0][0], _in(_listItem($columnfinder[0][0]))));

	if (_isVisible(_div("items-per-page")) && _isVisible(_div("items-per-page",_in(_div("pagination", _under(_list("search-result-items")))))))
	{
	
	//items per page in header		
	_assertVisible(_div("items-per-page"));
	//Items per page in footer	
	_assertVisible(_div("items-per-page",_in(_div("pagination", _under(_list("search-result-items"))))));
	//Default option in items per page drop down
	_assertEqual("25",_getSelectedText(_select("page-show")),"false Need to confirm default value in dropdown");
	
	//options in show drop down
	_assertVisible(_select("page-show"));	
	var $itemsdisplayed=_getText(_select("page-show"));

	for (var $i=0;$i<$itemsdisplayed.length;$i++)
	{
	_setSelected(_select("page-show"),$itemsdisplayed[$i]);
	_assertEqual($itemsdisplayed[$i],_getSelectedText(_select("page-show")),"Selected option is displaying properly");
	_assertEqual($itemsdisplayed[$i],_getSelectedText(_select("page-show",_in(_div("pagination", _under(_list("search-result-items")))))),"Selected option is displaying properly at footer");
	ItemsperPage($itemsdisplayed[$i]);
	}
	
	}

else
	{
	
	_log("Single page displayed");
	var $totalitems=_extract(_getText(_div("pagination")), "/of (.*) </", true).toString();
	var $totalitemsdisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
	
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216138/216139/216140/216144","Verify the display of options in Sort By dropdown in Coulmn finder Product listing in desktop/tablet Verifying the default option in Sort by dropdown on page load in Coulmn finder Product listing in desktop/tablet " +
		"Verify the display of Show dropdown in Column finder product listing page in desktop/tablet");
$t.start();
try
{

	//Click on Insert finder link in the left nav
	_click(_link($columnfinder[0][0], _in(_listItem($columnfinder[0][0]))));
	
	if(_isVisible(_select("grid-sort-header")))
	{
		var $sort=_getText(_select("grid-sort-header"));
		var $j=0;
		for(var $i=1;$i<$sort.length;$i++)
		{
			_setSelected(_select("grid-sort-header"), $sort[$i]);
			//Validating the selection
			_assertEqual($sort[$i],_getSelectedText(_select("grid-sort-header")),"Selected option is displaying properly");
			//_assertEqual($sort[$i],_getSelectedText(_select("grid-sort-footer")),"Selected option is displaying properly");
			sortBy($sort[$i]);
		}
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216148/216149/216147/216542-R/216523/216524","Verify the display/functionality of SHOP NOW button/display of Column images for column trophy image/navigation/display of bread crumb in the Column finder product listing page desktop/tablet");
$t.start();
try
{
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($columnfinder[0][0], _in(_listItem($columnfinder[0][0]))));
	var $prodname=_getText(_link("name-link"));
	//Display of Shop now button
	_assertVisible(_link("show-now-link",_in(_list("search-result-items"))));
	//Product image
	_assertVisible(_image("/(.*)/", _in(_list("search-result-items"))));
	//product price
	_assertVisible(_div("/product-pricing/", _in(_list("search-result-items"))));
	//breadcrumb
	_assertEqual($columnfinder[2][1], _getText(_div("breadcrumb")));
	//Click Shop now button
	_click(_link("show-now-link",_in(_list("search-result-items"))));
	//Prod name in PDP
	_assertEqual($prodname, _getText(_heading1("product-name")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("","");
$t.start();
try
{



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


/*var $t = _testcase("","");
$t.start();
try
{



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()*/