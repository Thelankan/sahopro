_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Category_landing.xls");

var $Football=_readExcelFile("Category_landing.xls","Football_insert");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

var $t = _testcase("215917/215917/215921/215961","Verify the display of Product/category image in the Product tile search in Desktop/tablet,Verify the display of Product/category image in the Product tile search in Desktop/tablet,Verify the functionalityof Shop Now button in the Product tile " +
		" search in Desktop/tablet,Verify the display of Category name in the Product tile search in Desktop/tablet");
$t.start();
try
{
	//search for product
	_setValue(_textbox("q"),$Football[0][0]);
	//enter by keyboard
	_focusWindow();
	_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	//search result page
	_assertVisible(_list("search-result-items"));
	_assertEqual($Football[0][0], _getText(_heading3("refinement-heading")));
    //product image should appear
	_assertVisible(_image("/product image/"));
	//shop now button
	_assertVisible(_image("/shop-now-btn/"));
	//shopnav button under image
	_assertVisible(_link(_image("/shop-now-btn/"), _under(_image("/product image/"))));
	//product title below shop now button
	_assertVisible(_link("name-link"),_under(_image("/shop-now-btn/")));
	//on click of shop now button
	_click(_image("/shop-now/"));
	//PDP
	_assertVisible(_div("pdp-main"));
	_assertVisible(_div("breadcrumb"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("215969","Verify the display of 'Quick View' button in the Product tile search in Desktop/tablet");
$t.start();
try
{
	//search for product
	_setValue(_textbox("q"),$Football[0][0]);
	//enter by keyboard
	_focusWindow();
	_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	//search result page
	_assertVisible(_list("search-result-items"));
	//mouse over on image
	_mouseOver(_image("/product image/"));
	//quickview
	_assertvisible("");
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()