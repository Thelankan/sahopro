_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Category_landing.xls");

var $insert=_readExcelFile("Category_landing.xls","Insert_finder");
var $searchvalidation=_readExcelFile("Category_landing.xls","SearchValidation");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();



var $t = _testcase("215581/215540","Verify the navigation of 'Insert finder page' in Desktop/tablet/ UI of the Breadcrumb of the 'Color insert finder' page");
$t.start();
try
{
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//Display of breadcrumb
	if(isMobile())
	{
		_assertEqual($insert[2][1], _getText(_div("breadcrumb")));
	}
	else
	{
		_assertEqual($insert[0][1], _getText(_span("breadcrumb-element last")));
	}
	//_assertContainsText($insert[0][0], _span("breadcrumb-element"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("217377/215580","Verify the functionality of clicking search icon in the Insert finder page /Verify the functionality of 'Search box'  in  'Color insert finder' page in desktop/tablet");
$t.start();
try
{
	//Category level searchbox
	if(isMobile())
	{
		_setValue(_textbox("categorySearch",_in(_div("/mobile-search/"))), $insert[0][2]);
		//Click on search icon
		_click(_image("/(.*)/", _in(_div("/mobile-search/"))));
	//	_click(_submit("Search", _in(_div("Search Catalog Search"))));
	}
	else
	{
		//_setValue(_textbox("categorySearch", _in(_div("search-box-main"))), $insert[0][2]);
		_setValue(_textbox("Search Catalog", _in(_div("header-test-search"))), $insert[0][2]);
		//Click on search icon
	//	_click(_image("/(.*)/", _in(_div("search-box-main"))));
		_click(_submit("Search", _in(_div("Search Catalog Search"))));
	}
	
	//Breadcrumb/refinement heading
	//_assertEqual($insert[0][2], _getText(_heading3("refinement-heading")));
	_assertContainsText($insert[0][2], _heading1($insert[7][3]));
	
	//Dispay of products
	_assertVisible(_div("product-tile"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("215582/217377","Verify the validation of Search box in Color insert finder page in Desktop/tablet /Verify the functionality of clicking search icon in the Insert finder page");
$t.start();
try
{

for(var $i=0;$i<$searchvalidation.length;$i++)
	{
	
	//Category level searchbox
	if(isMobile())
	{
		_setValue(_textbox("categorySearch",_in(_div("/mobile-search/"))),$searchvalidation[$i][1]);
		//Click on search icon
		_click(_image("/(.*)/", _in(_div("/mobile-search/"))));
	}
	else
		{
			
		//_setValue(_textbox("categorySearch", _in(_div("search-box-main"))),$searchvalidation[$i][1]);
		_setValue(_textbox("q", _in(_div("Search Catalog Search"))),$searchvalidation[$i][1]);
		//Click on search icon
		//	_click(_image("/(.*)/", _in(_div("search-box-main"))));
			_click(_submit("Search", _in(_div("Search Catalog Search"))));
		}
	
	if ($i==0)
		{
		
		_assert(false,"TBD in test case");
		
		}
	else if ($i==1)
		{
		
		//No search result
		_assertVisible(_heading3("No Products Matched Your Search For \""+$searchvalidation[1][3]+"\""));
		//Display of you can also shop products
		_assertVisible(_heading3($searchvalidation[0][2]));
		_assertVisible(_div("category-slot"));
		_assertVisible(_div("/categorybox/"));
		
		}
		else
			{
			
			//_assertEqual($searchvalidation[2][1], _getText(_heading3("refinement-heading")));
			_assertContainsText($searchvalidation[2][1], _heading1("/cya-title bigtext-line/"));
			
			}
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("215549","Verify the navigation of the left nav link 'Cup trophies' in 'Color insert finder' page in desktop/tablet");
$t.start();
try
{
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//click on the cup trophies in left nav

	//_click(_link($insert[1][0], _in(_div("refinements"))));
	_click(_link($insert[1][0], _in(_div("LeftNav"))));
	//breadcrumb
	//_assertEqual($insert[1][1], _getText(_span("breadcrumb-element")));
	_assertEqual($insert[1][1], _getText(_span("Cup Trophies", _in(_div("breadcrumb")))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("215541","Verify the functionality of the Breadcrumb of the 'Color insert finder' page in Desktop/tablet");
$t.start();
try
{

//Navigate to homepage
NavigatetoHomepage();
//Click on Insert finder link in the left nav
_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
//click on our trophies link in left nav
_click(_link("Home", _in(_div("breadcrumb"))));
//verify the home page
_assertNotVisible(_div("breadcrumb"));
_assertVisible(_div("home-main-right"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("216831/215584","Verify the Ui of the 'Alphabets search' link in the color insert finder page in Desktop/tablet");
$t.start();
try
{

//click on insert finder link
_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
//count of the links
var $countoflinks=_count("_link","/(.*)/", _in(_div("list")));
var $alphanumericlinksinheading=_collectAttributes("_link","/(.*)/","sahiText", _in(_div("list")));
var $alphanumericlinksincatlanding=_collectAttributes("_div","item-name","id",_in(_div("list-container"))).toString().replace(",","").replace(/,,/g,",");
_assertEqualArrays($alphanumericlinksinheading.sort(),$alphanumericlinksincatlanding.sort());

for (var $i=0;$i<$countoflinks;$i++)
{
	_call(window.scrollTo(0,0));
	var $current;
	_set($current, window.document.body.scrollTop);	
	//_click(_link($alphanumericlinks[$i],_in(_div("list"))));
	_click(_link($alphanumericlinksinheading[$i],_in(_div("list"))));
	
	var $changed;
	_set($changed, window.document.body.scrollTop);
	
	if($current<$changed)
		{
		_log($current);
		_log($changed);
		_assert(true,"Page Scrolled down Successfully");       
		}
		else
		    {
		    _assert(false,"Page did not Scroll down");
		    }
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase(" ","All left nav links");
$t.start();
try
{

	_assert(false,"Modifications should be done");
	//collecting refinements
	var $AwardByFilter=_collectAttributes("_link","/Refine by/","sahiText",_in(_div("refinement topLevelCategory")));
	
	for (var $i=0;$i<$AwardByFilter.length;$i++)
	{
		//selecting the refinement's
		_click(_link($AwardByFilter[$i],_in(_div("refinement topLevelCategory"))));
		_assertVisible(_span($AwardByFilter[$i]+" x", _in(_div("breadcrumb"))));
		//De selecting the refinement's
		_click(_link($AwardByFilter[$i], _in(_listItem("selected",_in(_div("refinement topLevelCategory"))))));
		_assertNotVisible(_span($AwardByFilter[$i]+" x", _in(_div("breadcrumb"))));
		//navigate to sub category page
		search($awardbysport[5][0]);
	}


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("215544","Verify the Position of the 'Search for insert' section in the Color insert finder page");
$t.start();
try
{

	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


NavigatetoHomepage();
_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));

var $t = _testcase("215545/215547/215560/215561","Verify the navigation of the left nav link Economy/Participation trophies/Baseball trophies/Basketball trophies in 'Color insert finder' page in desktop/tablet");
$t.start();
try
{
	for(var $i=3;$i<11;$i++)
	{
		_click(_link($insert[$i][1], _in(_div("home-main-left"))));
		//breadcrumb
		_assertEqual($insert[$i][1], _getText(_span("breadcrumb-element last")));
		NavigatetoHomepage();
		_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
		_log($insert[$i][1]);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();





//shantanu

var $t = _testcase("215538","Verify the UI of the left nav of CROWN- Finder: Inserts page / Color insert finder page in desktop/tablet");
$t.start();
try
{
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//verifying the UI of the left nav
	//_assertVisible(_div("LeftNavHdr", _in(_div("LeftNav"))));
	_assertVisible(_div("/LeftNavHdr/", _in(_div("LeftNav"))));
	_assertVisible(_link($insert[12][0]));
	_assertVisible(_div($insert[9][0], _in(_div("home-main-left"))));
	//_assert(false,"content slot is not there");
	//verify the display of crown catalog
	_assertVisible(_div($insert[10][0], _in(_div("home-main-left"))));
	//verify the display of build your trophy in content asset
	//_assertVisible(_link($insert[11][0], _in(_div($insert[0][5]))));
	_assertVisible(_link($insert[11][0], _in(_div("leftTrophyimgs"))));

	}

catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("215542","Verify the position of Insert finder Image/Content slot in the 'Color insert finder' page");
$t.start();
try
{
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//Verify the position of Insert finder image/content slot
	_assertVisible(_image("Color Insert Finder", _in(_div("finderImg"))))
	_assertVisible(_image("/(.*)/",_under(_div("breadcrumb"))));
	_assertVisible(_image("/(.*)/",_rightOf(_div("LeftNav"))));
	
	}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("215543","Verify the functionality of Insert finder image/slot in the 'Color insert finder' page");
$t.start();
try
{
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//Verify the position of Insert finder image/content slot
	_assertVisible(_image("Color Insert Finder", _in(_div("finderImg"))))
	 //Verify the functionality of Insert finder image/content slot by clicking on it
	_click(_image("Color Insert Finder", _in(_div("finderImg"))));
	_assert(false,"cant click the image");
	

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("215544","Verify the Position of the 'Search for insert' section in the Color insert finder page");
$t.start();
try
{
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//Verify the position of Insert finder image/content slot
	_assertVisible(_image("Color Insert Finder", _in(_div("finderImg"))))
	 //Verifying the position of the 'Search for insert' section
	_assertVisible(_submit("/(.*)/",_rightOf(_textbox("Search Catalog"))));
    //Search icon should be displayed in red color
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("215546","Verify the navigation of the left nav link 'NEW for 2016'  in  'Color insert finder' page in desktop/tablet");
$t.start();
try
{
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//Verify the position of Insert finder image/content slot
	_assertVisible(_image("Color Insert Finder", _in(_div("finderImg"))))
	//Click on the 'NEW for 2016' left navigation pane link
	_click(_link($insert[13][0]));
	//verifying that user navigate to respective category landing page/product listing page
	
	}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();





var $t = _testcase("215553/215557/215563/215568/215569","Verify the navigation of the left nav  in  'Color insert finder' page in desktop/tablet");
$t.start();
try
{
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//Verify the position of Insert finder image/content slot
	_assertVisible(_image("Color Insert Finder", _in(_div("finderImg"))))
	leftnav($insert[3][0],$insert[11][1],$insert[2][3]);
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//click on sale link in left nav
	_click(_link($insert[5][0], _in(_div("LeftNav"))));
	_assertVisible(_span($insert[12][1], _in(_div("breadcrumb"))));
	_assertVisible(_heading1("SALE AWARDS"));
	
	//leftnav($insert[5][0],$insert[12][1],$insert[3][3]);
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	
	leftnav($insert[6][0],$insert[13][1],$insert[4][3]);
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	
	leftnav($insert[7][0],$insert[14][1],$insert[5][3]);
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	
	leftnav($insert[8][0],$insert[15][1],$insert[6][3]);
	
	}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("215559/215562","Verify the navigation of the links present  in  'Color insert finder' page in desktop/tablet");
$t.start();
try
{
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//Verify the position of Insert finder image/content slot
	_assertVisible(_image("Color Insert Finder", _in(_div("finderImg"))))
	//click on animal trophies
	_click(_link($insert[14][0]));
	//verifying navigation to the animals page
	_assertVisible(_span($insert[16][1], _in(_div("breadcrumb"))));
	_assertVisible(_heading1("insert-crown-head"));
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//click on the beauty awards in the page
	_click(_link($insert[15][0]));
	//verifying navigation to the beauty page
	_assertVisible(_span($insert[17][1], _in(_div("breadcrumb"))));
	//_assertVisible(_span("BEAUTY", _in(_div("breadcrumb"))));
	_assertVisible(_heading1("insert-crown-head"));
	
	}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("216771","Verifythe UI of the content slot in 'Color insert finder' page");
$t.start();
try
{
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//Verify the position of Insert finder image/content slot
	//_assertVisible(_image("Color Insert Finder", _in(_div("finderImg"))))
	_assertVisible(_image("Color Insert Finder", _in(_div("finderImg"))))
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("216832","Verify the UI of color insert finder page in Desktop/tablet");
$t.start();
try
{
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//Verify the position of Insert finder image/content slot
	_assertVisible(_image("Color Insert Finder", _in(_div("finderImg"))))

	//verify the display of insert finder image
	_assertVisible(_image("/(.*)/",_under(_div("breadcrumb"))));
	
	_assertVisible(_div("/(.*)/"));

	}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("216772","Verify the UI of the 'Search for insert' section in the Color insert finder page");
$t.start();
try
{
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//Verify the position of Insert finder image/content slot
	_assertVisible(_image("Color Insert Finder", _in(_div("finderImg"))))
	//verify the UI of insert finder image
	_assertVisible(_image("/(.*)/",_under(_div("breadcrumb"))));
	_assertVisible(_div("/(.*)/"));
	//verifying the insert section heading
	_assertVisible(_textbox("q"));
	_assertVisible(_submit("Search"));
    //VERIFYING the placeholder
	_assertVisible(_div("search-box-main"));
	_assertVisible(_textbox("categorySearch", _in(_div("search-box-main"))));



	}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("216832","Verify the UI of color insert finder page in Desktop/tablet");
$t.start();
try
{
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//verify the home link in breadcrumb
	_assertVisible(_span("breadcrumb-element last", _in(_div("breadcrumb"))));
	//Verify the position of Insert finder image/content slot
	_assertVisible(_image("Color Insert Finder", _in(_div("finderImg"))))
//verify the insert section heading
	_assertVisible(_image("/(.*)/",_under(_div("breadcrumb"))));
	_assertVisible(_div("/(.*)/"));
	//verifying the insert section heading
	_assertVisible(_textbox("q"));
	_assertVisible(_submit("Search"));
    //VERIFYING the placeholder
	_assertVisible(_div("search-box-main"));
	_assertVisible(_textbox("categorySearch", _in(_div("search-box-main"))));
	_assertVisible(_span($insert[1][5], _in(_div("searched-text"))));
    //verify the alphabet search links Alphabet search links and all inserts template should be displayed with the subcategoires list per alphabet or numeric 
	//var $countoflinks=_count("_link","/(.*)/", _in(_div("list")));
	var $alphanumericlinksinheading=_collectAttributes("_link","/(.*)/","sahiText", _in(_div("list")));
	for(var $i=0;$i<$alphanumericlinksinheading.length;$i++)
		{
		
		_assertVisible(_link($alphanumericlinksinheading[$i], _in(_div("list"))));
		_click(_link(_link($alphanumericlinksinheading[$i], _in(_div("list")))));
		//_assertVisible(_div($alphanumericlinksinheading[$i], _in(_div("/items/"))));
		_assertVisible(_div($alphanumericlinksinheading[$i], _in(_div("list-container"))));

            }

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("217935","Veirfy the total number of Columns displayed in the Insert finder Page in desktop/ipad view");
$t.start();
try
{
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//verify the home link in breadcrumb
	_assertVisible(_link("Home", _in(_div("Back Home Color Insert Finder"))));
	//Verify the position of Insert finder image/content slot
	_assertVisible(_image("Color Insert Finder", _in(_div("finderImg"))))

	//verify the color insert finder link in breadcrumb
	_assertVisible(_span("Color Insert Finder", _in(_div("Back Home Color Insert Finder"))));
//verify the insert section heading
	_assertVisible(_image("/(.*)/",_under(_div("breadcrumb"))));
	_assertVisible(_div("/(.*)/"));
	//verifying the insert section heading
	_assertVisible(_textbox("q"));
	_assertVisible(_submit("Search"));
   // counting the total number of columns in insert finder page
	var $columns=_count("_div","/items/", _in(_div("list-container")));
//verifying the no of columns in the color insert finder page
	_assertEqual(3,$columns);
	//number of links in page
	var $act_links_in_page=_count("_link","/(.*)/", _in(_div("list-container")));
	//number of links in each column
	var $expect= $act_links_in_page/$columns;	
	
	for(var $i=0;$i<$columns;$i++)
		{
	//no of links actually in a column
		var $Act_col=_count("_link","/(.*)/", _in(_div("items["+$i+"]")));
	
	
		_assertEqual($expect,$Act_col);
		
		//_assert(false,"number of links are not divided equally");
		}
    
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();








var $t = _testcase("217937","Verify the display of Place holder text in Search box when out of focus in insert finder category laning page");
$t.start();
try
{
	//Navigate to homepage
	NavigatetoHomepage();
	//Click on Insert finder link in the left nav
	_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	//verify the home link in breadcrumb
	_assertVisible(_link("Home", _in(_div("Back Home Color Insert Finder"))));
	//Verify the position of Insert finder image/content slot
	_assertVisible(_image("Color Insert Finder", _in(_div("finderImg"))))
	
	//verify the color insert finder link in breadcrumb
	_assertVisible(_span("Color Insert Finder", _in(_div("Back Home Color Insert Finder"))));
//verify the insert section heading
	_assertVisible(_image("/(.*)/",_under(_div("breadcrumb"))));
	_assertVisible(_div("/(.*)/"));
	//verifying the insert section heading
	_assertVisible(_textbox("q"));
	_assertVisible(_submit("Search"));
	//verifying the placeholder in searchbox
	_assertVisible(_div("search-box-main"));
	_assertVisible(_textbox("categorySearch", _in(_div("search-box-main"))));
	_assertVisible(_span($insert[1][5], _in(_div("searched-text"))));
  
   
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("215545/215539/215577/215578","Verify the navigation of the left nav link Custom Logo Awards/Our Trophies/Custom medals/Corporate trophies in 'Color insert finder' page in desktop/tablet");
$t.start();
try
{
	var $leftNavLinks=_collectAttributes("_link","/(.*)/","sahiText" , _in(_div("LeftNav")))
	for(var $i=0;$i<$leftNavLinks.length;$i++)
	{
		_click(_link($leftNavLinks[$i], _in(_div("LeftNav"))));
		//_assertEqual($insert[$i][4], _getText(_span("breadcrumb-element last")));
	 _assertContainsText($leftNavLinks[$i], _heading1("/cya-title bigtext-line/"));
		NavigatetoHomepage();
		//Insert Finder page
		_click(_link($insert[0][0], _in(_listItem($insert[0][0]))));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
