_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Category_landing.xls");

var $Football=_readExcelFile("Category_landing.xls","Football_insert");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();




var $t = _testcase("216384/216388/216395/216397/216398/222211","Verify the navigation of Productlisting_Football insert finder page,Verify the display of 'Choose insert preview' text in Productlisting_Football insert finder page,Verify the display of trophies in Productlisting_Football insert finder page,Verify the functionality of suggested categories in Productlisting_Football insert finder page,Verify the functionality of Trophy in Productlisting_Football insert finder page");
$t.start();
try
{
	//click on home logo
	_click(_image($Football[2][4]));
	//insert finder in left nav
	//_click(_link($Football[0][1], _in(_div("home-main-left"))));
	_click(_link($Football[0][1], _in(_listItem($Football[0][4]))));
	//colour insert finder page
	_assertEqual($Football[0][2], _getText(_span("breadcrumb-element last")));
	_assertVisible(_div("breadcrumb"));
    //click of foot ball comes under F family
	//_click(_link($Football[1][1], _in(_div("/F FAMILY/"))));
	_click(_link($Football[1][1], _in(_div("list-container"))));
	//insert crown head page
	_assertVisible(_heading1("insert-crown-head"));
	_assertEqual($Football[0][3], _getText(_heading1("insert-crown-head")));
	//insert to preview text 
//
//	_assertVisible(_div($Football[1][3], _in(_div("primary-content ribbon-head"))));//configuration issue
//	_assertVisible(_heading1($Football[1][3], _in(_div("primary"))));
	//'Insert Carousel
	_assertVisible(_div("slick-track"));
	//_assertVisible(_div("search-result-content finder-carousal"));
   //count in entire carousal
   $totalcount=_count("_div","/product-tile/", _in(_div("slick-list draggable")));
	//
	_assert(false,"its not possible to take total count of 12 products in 1 carousal and not possible to take count of 6 products in each row");
   //suggested categories
	_assertVisible(_heading2("trophies-header"));
   //count of suggseted categories
	_assert(false,"not able to fine count of trophies categories by row wise");
   //suggested trophies should come just below the carousal
	//_assertVisible(_div("search-result-content finders-block finderresult"), _under(_div("search-result-content finder-carousal")));
	_assertVisible(_div("/search-result-content finders-block finderresult js/"), _under(_div("slick-track")));
	
	//click on any suggested trophies
	//_click(_link("name-link", _in(_div("search-result-content finders-block finderresult"))));
	_click(_link("name-link", _in(_listItem("grid-tile"))));
	//PDP
	//_assertVisible(_div("pdpMain"));
	_assertVisible(_div("pdp-main"));
	_assert(false,"216397 Tc's is pending");

 }

catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216386","Verify the functionality of breadcrumb in Productlisting_Football insert finder page");
$t.start();
try
{
	//click on homelogo
	_click(_image("crown-awards-logo-white.png"));
	//insert finder in left nav
	//_click(_link($Football[0][1], _in(_div("home-main-left"))));
	_click(_link($Football[0][1], _in(_listItem($Football[0][4]))));

	//colour insert finder page
	_assertEqual($Football[0][2], _getText(_span("breadcrumb-element last")));
	//breadcrumb
	//_assertEqual("Home"+">"+"Color Insert Finder", _getText(_div("breadcrumb")).replace(" ","").replace(" ",""));
	_assertVisible(_span($Football[0][2], _in(_div("breadcrumb"))));

	//click on color insert finder should not navigate to any page
	//_click(_span("breadcrumb-element last"));
	//should be in same page
	//_assertVisible(_div("primary-content ribbon-head"));
	//under line for home
	_assertEqual($Football[1][2],_style(_link("breadcrumb-element"),"text-decoration"));
	//underline fr colour insert finder
	_assertEqual($Football[2][2],_style(_span("breadcrumb-element last"),"text-decoration"));
	//click on home
	_click(_link("breadcrumb-element"));
	//home page
	_assertVisible(_div("home-main-right"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

//SHAN
var $t = _testcase("216391/216392/216393","Verify the functionality of Insert carousel in Productlisting_Football insert finder page");
$t.start();
try
{
	//click on the home icon
	_click(_image($Football[2][4]));
	//insert finder in left nav
	//_click(_link($Football[0][1], _in(_div("home-main-left"))));
	_click(_link($Football[0][1], _in(_listItem($Football[0][4]))));
	//colour insert finder page
	_assertEqual($Football[0][2], _getText(_span("breadcrumb-element last")));
	_assertVisible(_div("breadcrumb"));
    //click of foot ball comes under F family
	//_click(_link($Football[1][1], _in(_div("/F FAMILY/"))));
	_click(_link($Football[1][1], _in(_div("list-container"))));
	//insert crown head page
	_assertVisible(_heading1("insert-crown-head"));
	_assertEqual($Football[0][3], _getText(_heading1("insert-crown-head")));
	//insert to preview text 
//	_assertVisible(_div($Football[1][3], _in(_div("primary-content ribbon-head"))));
	//_assertVisible(_heading1($Football[1][3], _in(_div("primary"))));
	//'Insert Carousel
//_assertVisible(_div("search-result-content finder-carousal"));
	_assertVisible(_div("slick-track"));
	
	//suggested trophies should come just below the carousal	
//	_assertVisible(_div("slick-list draggable"));
//_assertVisible(_div("search-result-content finders-block finderresult"), _under(_div("search-result-content finder-carousal")));
	_assertVisible(_div("/search-result-content finders-block finderresult js/"), _under(_div("slick-track")));
	
	//verifying the name of carousal

//	_assertVisible(_div($Football[2][3]));
//	_assertVisible(_div($Football[2][3], _in(_div("product-tile"))));//based on configuration cant fetch the name

	
	//counting the no of carousal
	$totalcount=_count("_div","/product-tile/", _in(_div("slick-list draggable")));
   //count in entire carousal
	//
	_assert(false,"its not possible to take total count of 12 products in 1 carousal and not possible to take count of 6 products in each row")
    //click on any football in carousal
    _click(_image("ImageCompositionServlet", _in(_div("product-tile"))));
	_assert(false,"Its not possible to verify the selectedfootball in all trophies preview page");
	_assert(false,"Its not possible to verify the selected  insert football category name should displayed");
	
	
	//verifying the price of item displayed in the carousal
	_assert(false,"Its not possible to verify the price of item displayed in the carousal");
	
//	_assertVisible(_heading2("/trophies-header/"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()




