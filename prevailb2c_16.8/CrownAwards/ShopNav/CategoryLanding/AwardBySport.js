_log("*********************** Award By Sport ********************");

_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Category_landing.xls");

var $awardbysport=_readExcelFile("Category_landing.xls","AwardBySport");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();



var $t = _testcase("215972/215974/215977/215978","Verify the UI of Bread crumb of the Category landing Award by sport page/Verify the functionality of Bread crumb of the Category landing Award by sport page/Verify the navigation for 'Award by sport' page in Desktop" +
		"Verify the display of Featuring cheer image/slot in the 'Award by sport' page in Desktop");
$t.start();
try
{

//Navigate to category landing page	
NavigatetoSubcat($awardbysport[0][0]);
//verifying bread crumb
_assertVisible(_div("breadcrumb"));
//_assertEqual($awardbysport[0][1], _getText(_div("breadcrumb")));
_assertContainsText("Home Award By Sport", _div("breadcrumb"));// change by dhiraj
//functionality of bread crumb
_click(_link("Home", _in(_div("breadcrumb"))));
//verify the home page
_assertNotVisible(_div("breadcrumb"));
_assertVisible(_div("home-main-right"));
//Navigate to category landing page	
NavigatetoSubcat($awardbysport[0][0]);
//Featuring cheer image/slot
_log("Featuring cheer image/slot");
_assertVisible(_div("dwt-box-images"));
_assertVisible(_textbox("categorySearch", _in(_div("search-box-main"))));
_assertVisible(_div($awardbysport[1][0]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("215979","Verify the functionality of Featuring cheer image/slot in the 'Award by sport' page in Desktop");
$t.start();
try
{
	_click(_link("Award By Sport"));
//click on image/slot in the 'Award by sport' 	
_click(_image("/(.*)/", _in(_div("dwt-box-images"))));
//navigate to PLP page
_assertVisible(_div("breadcrumb"));
_assertVisible(_div("/pagination-link/"));
_assertVisible(_div("pagination"));
_assertVisible(_div("search-result-options"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


_log("*********************** Category landing page trophies ********************");

var $t = _testcase("217883/217883/217884/217916/217882","Verify the UI and display of  breadcrumb in Trophies category landing page in the main nav category in Desktop/tablet/Verify the display of  breadcrumb in Trophies category landing page in the main nav category in Desktop/tablet" +
		"Verify the navigation of Breadcrumb in Trophies Category landing Page in Desktop/tablet");
$t.start();
try
{

//click on trophies category	
_click(_link($awardbysport[2][0],_listItem("/stockcategories/")));
_assertVisible(_heading1($awardbysport[3][0]));
_assertVisible(_div("/dwt-box-content/"));
//_assert(false,"choose your dropdown is missing should confirm with sneha");
//_assertVisible(_div("dropdownul[1]"));
//category landing page
_assertVisible(_div("breadcrumb"));
//_assertEqual($awardbysport[1][1], _getText(_div("breadcrumb")));
_assertEqual("Home Trophies", _getText(_div("Home Trophies")));
//click on home link in breadcrumb
_click(_link("Home", _in(_div("breadcrumb"))));
//verify the home page
_assertNotVisible(_div("breadcrumb"));
_assertVisible(_div("home-main-right"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("217885","Verify the display of Title/heading in Trophies category landing page in Desktop/tablet");
$t.start();
try
{

//click on trophies category	change by dhiraj	
_click(_link($awardbysport[2][0],_listItem("/stockcategories/")));	
	
//fetching color 	
var $bluecolor=_style(_heading1("TROPHIES"),"color");
_assertEqual($awardbysport[1][2],$bluecolor);

//var $redcolor=_style(_span("titleaccent red"),"color");
//_assertEqual($awardbysport[0][2],$redcolor);//done by dhiraj
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end() 

var $t = _testcase("217917","Verify the functionality of Choose Your sport drop down in the Trophies category landing page");
$t.start();
try
{
	//_assertVisible(_div("dropdownul[1]"));
	//_assertContainsText("Choose Your Sport Sports", _div("dropdownul[1]"));
	//_assert(false,"Not updated in testopia and should clarify with sneha");

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("217918","Verify the display of 'Search Our Trophies' in the left nav of Trophies category landing page");
$t.start();
try
{
	_assertVisible(_div("LeftNavHdr"));
	//_assert(false,"Not updated in testopia and should clarify with sneha");change by dhiraj

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("217920/217922/217921","Verify the navigation of CYA basket ball page in Desktop/tablet/Verify the navigation of Basketball Trophies page in Desktop/tablet" +
		"Verify the UI of CYA Basketball page");
$t.start();
try
{

//navigate to basket ball page	
NavigatetoSubcat($awardbysport[5][0]);
//to verify the bread crumb for basketball change by dhiraj
_assertContainsText($awardbysport[2][1],_div("breadcrumb"));
//verifying UI of the page
_assertVisible(_heading1($awardbysport[0][3]));
//content assert
_assertVisible(_div("dwt-box-images"));
//verification of content slot
//_assert(false,"content slot is not configured");
//_assertVisible(_div($awardbysport[1][3]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("217917","Verify the functionality of Choose Your sport drop down in the Trophies category landing page");
$t.start();
try
{
	
//click on trophies category	
//_click(_link($awardbysport[2][0],_listItem("/stockcategories/")));
	_click(_link("Award By Sport", _in(_listItem("stockcategories ipad-height-cal"))));
//'Choose Your Sport' drop down in trophies landing page
//_assert(false,"CYA dropdown is missing");change by dhiraj
//_assertVisible(_div("dropdownul[1]"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("217918","Verify the display of 'Search Our Trophies' in the left nav of Trophies category landing page");
$t.start();
try
{

//click on trophies category	
_click(_link($awardbysport[2][0],_listItem("/stockcategories/")));
//_assert(false,"Search Our Trophies links are not configured in left nav");change by dhiraj
_assertVisible(_div("LeftNav"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

if (isMobile())
	{

_log("*********************** Award By sport scripts for mobile ********************");

var $t = _testcase("216471/216472/216474/216475","Verify the navigation of productlisting_award by sport in Mobile/Verify the display of cross reference categories of productlisting_award by sport in Mobile" +
		"Verify the display of Next/previous link in productlisting_award by sport in Mobile");
$t.start();
try
{
	
//click on Hamburger button
_click(_submit("menu-toggle"));
//click on award by sport
_click(_link($awardbysport[0][0],_near(_submit("menu-toggle"))));
//should navigate to award by sport landing page
_assertEqual($awardbysport[3][1], _getText(_div("breadcrumb")));
//click on any category in  drop down
_click(_link($awardbysport[5][0],_near(_textbox("categorySearch"))));
//verifying bread crumb
_assertEqual($awardbysport[5][0], _getText(_span("breadcrumb-element last")));
//verifying UI of the page
_assertVisible(_heading1($awardbysport[0][3]));

//verification of next and previous links
//_assertVisible(_link("next"));
//_assertVisible(_link("prev"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216477","Verify the display of no. of pages in productlisting_award by sport in Mobile");
$t.start();
try
{
	
//click on Hamburger button
_click(_submit("menu-toggle"));
//click on award by sport
_click(_link($awardbysport[0][0],_near(_submit("menu-toggle"))));
//verifying no of pages
var $startpage=_getText(_div("/pagination-link/")).split(" ")[2];
var $endpage=_getText(_div("/pagination-link/")).split(" ")[4];
_assertVisible(_div("Previous page "+$startpage+" of "+$endpage+" Next"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()
	
	
	}





_log("*********************** Related to refinements ********************");

var $t = _testcase("216350","Verify the display of Refine Search in left nav category search refinements");
$t.start();
try
{

	//navigate to sub category page	
	NavigatetoSubcat($awardbysport[5][0]);
	//collecting refinements
	//var $refinements=_collectAttributes("_heading3","/toggle/","sahiText",_in(_div("refinements")));by dhiraj
	var $refinements=_collectAttributes("_heading3", "/toggle refinement-heading/", "sahiText");
	for (var $i=0;$i<$refinements.length;$i++)
	{
		//_assertVisible(_heading3($refinements[$i],_in(_div("left-refinement"))));by dhiraj
		_assertVisible(_heading3("By Type",_in(_div("refinement topLevelCategory"))));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216351/216356","Verify the UI of deselecting refinements in By Award type  left nav - category search - refinements/Verify the display of Refine Search - By Award type in left nav category search refinements");
$t.start();
try
{
	
	//navigate to sub category page	
	NavigatetoSubcat($awardbysport[5][0]);
	//collecting refinements
	var $AwardByFilter=_collectAttributes("_link","/Refine by/","sahiText",_in(_div("refinement topLevelCategory")));
	
	for (var $i=0;$i<$AwardByFilter.length;$i++)
	{
		//selecting the refinement's
		_click(_link($AwardByFilter[$i],_in(_div("refinement topLevelCategory"))));
		//_assertVisible(_span($AwardByFilter[$i], _div("seoText")));
		_assertContainsText($AwardByFilter[$i], _div("ttt bigtext"));
		//De selecting the refinement's
		_click(_link($AwardByFilter[$i], _in(_listItem("selected",_in(_div("refinement topLevelCategory"))))));
		_assertNotVisible(_span($AwardByFilter[$i],_in(_div("seoText"))));
		//navigate to sub category page
		search($awardbysport[5][0]);
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

/*var $t = _testcase("216352","");
$t.start();
try
{
	
	//navigate to sub category page	
	NavigatetoSubcat($awardbysport[5][0]);
	//collecting refinements
	//var $PriceFilters=_collectAttributes("_link","/Refine by/","sahiText",_in(_div("refinement ")));by dhiraj
	var $PriceFilters=_collectAttributes("_link","/Refine by/","sahiText",_in(_div("refinement topLevelCategory")));
	
	for (var $i=0;$i<$PriceFilters.length;$i++)
	{
		//selecting the refinement's
		_click(_link($PriceFilters[$i], _in(_div("refinement topLevelCategory"))));
		_assertVisible(_span($PriceFilters[$i]+" x", _in(_div("breadcrumb"))));
		//De selecting the refinement's
		_click(_link($PriceFilters[$i], _in(_listItem("selected",_in(_div("refinement "))))));
		_assertNotVisible(_span($PriceFilters[$i]+" x", _in(_div("breadcrumb"))));
		//navigate to sub category page
		search($awardbysport[5][0]);
	}
breadcrum is not visible 
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216353","");
$t.start();
try
{
	
	//navigate to sub category page	
	NavigatetoSubcat($awardbysport[5][0]);
	//collecting refinements
	var $weightFilters=_collectAttributes("_link","/Refine by/","sahiText",_in(_div("refinement weight")));
	
	for (var $i=0;$i<$weightFilters.length;$i++)
	{
		//selecting the refinement's
		_click(_link($weightFilters[$i],_in(_div("refinement weight"))));
		_assertVisible(_span($weightFilters[$i]+" x", _in(_div("breadcrumb"))));
		//De selecting the refinement's
		_click(_link($weightFilters[$i], _in(_listItem("selected",_in(_div("refinement weight"))))));
		_assertNotVisible(_span($weightFilters[$i]+" x", _in(_div("breadcrumb"))));
		//navigate to sub category page
		search($awardbysport[5][0]);
	}
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("","");
$t.start();
try
{
	_assert(false,"size is not configured");
	//navigate to sub category page	
	NavigatetoSubcat($awardbysport[5][0]);
	//collecting refinements
	var $weightFilters=_collectAttributes("_link","/Refine by/","sahiText",_in(_div("refinement size")));
	
	for (var $i=0;$i<$weightFilters.length;$i++)
	{
		//selecting the refinement's
		_click(_link($weightFilters[$i],_in(_div("refinement weight"))));
		_assertVisible(_span($weightFilters[$i]+" x", _in(_div("breadcrumb"))));
		//De selecting the refinement's
		_click(_link($weightFilters[$i], _in(_listItem("selected",_in(_div("refinement weight"))))));
		_assertNotVisible(_span($weightFilters[$i]+" x", _in(_div("breadcrumb"))));
		//navigate to sub category page
		search($awardbysport[5][0]);
	}
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()
*/
_log("*********************** Related to price display ********************");


var $t = _testcase("217974","Verify the display of price in Product Listing page");
$t.start();
try
{
	//click on trophies category	
	_click(_link($awardbysport[2][0],_listItem("/stockcategories/")));
	//collecting refinements
	var $prodtileCount=_count("_div","/product-tile/",_in(_list("/search-result-items/")));
	
	for (var $i=0;$i<$prodtileCount;$i++)
	{
		//_assertVisible(_div("product-tile["+$i+"]", _in(_list("search-result-items"))));
		//_assertVisible(_div("product-pricing["+$i+"]", _in(_list("search-result-items"))));
		_assertVisible(_div("product-tile["+$i+"]", _in(_list("/search-result-items/"))));
		_assertVisible(_div("product-attributes product-pricing["+$i+"]", _in(_list("/search-result-items/"))));
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("217975","Verify the display of price in CYA pages");
$t.start();
try
{
	
	//navigate to sub category page
	NavigatetoSubcat($awardbysport[5][0]);
	//collecting refinements
	var $prodtileCount=_count("_div","/product-tile/",_in(_list("search-result-items")));
	
	for (var $i=0;$i<$prodtileCount;$i++)
	{
		_assertVisible(_div("product-tile["+$i+"]", _in(_list("search-result-items"))));
		_assertVisible(_div("product-pricing["+$i+"]", _in(_list("search-result-items"))));
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("217976","Verify the display of price in Search Result Page.");
$t.start();
try
{
	
	//navigate to sub category page
	search($awardbysport[5][0]);
	//collecting refinements
	var $prodtileCount=_count("_div","/product-tile/",_in(_list("search-result-items")));
	
	for (var $i=0;$i<$prodtileCount;$i++)
	{
		_assertVisible(_div("product-tile["+$i+"]", _in(_list("search-result-items"))));
		_assertVisible(_div("product-pricing["+$i+"]", _in(_list("search-result-items"))));
	}


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

//*******************************************************************************by dhiarj

var $t = _testcase("222351","Verify the application behavior on click of see all sports link in the left navigation pane");
$t.start();
try
{
	_click(_link("Trophies"));
	//check the link should be present in left navigation 
	_assertVisible(_paragraph("...See All Sports!", _in(_div("LeftNav"))));
	//check the functionality of the See All Sports
	_click(_link("...See All Sports!"));
	//checking the navigation to the award by sport
	_assertVisible(_div("awardbysport"));
	var $items=_collectAttributes("_link","/(.*)/","sahiText",_in(_div("list-container")));
		for($i=0;$i<$items.length;$i++)
			{
					_click(_link($items[$i]));
					//_assertVisible(_div("search-result-content"));
					_assertContainsText("Home"+" "+"Award By Sport"+" "+$items[$i], _div("breadcrumb"));
					//verify the functionality of bread and crumb
					_click(_link("Award By Sport[2]"));
			}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("216344","Verify the display of OUR TROPHIES category in Crown nav desktop/tablet");
$t.start();
try
{	
	_click(_link("Trophies"));
	_assertVisible(_div("OUR TROPHIES"));
	
	var $items=_collectAttributes("_link","/(.*)/","sahiText",_above(_div("Sport Trophies")));
	for($i=0;$i<$items.length;$i++)
		{
				_click(_link($items[$i]));
				_assertContainsText($items[$i],_div("breadcrumb"));
				
				_click(_link("Trophies"));
				_assertVisible(_div("OUR TROPHIES"));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216343","Verify the navigation of SPORT TROPHIES category links in Crown nav desktop/tablet");
$t.start();
try
{
	_click(_link("Trophies"));
	_assertVisible(_div("Sport Trophies"));
	var $items=_collectAttributes("_link","/(.*)/","sahiText",_above(_paragraph("/See All Sports/")));
		for($i=0;$i<$items;$i++)
			{
			_assertVisible(_link($items[$i]));
			_click(_link($items[$i]));
			_assertVisible(_div("subcategory-tiles"));
			_click(_link("Trophies"));
			}
}
catch($e)
{
	_logExceptionAsFailure($e);
	
}	
$t.end()


var $t = _testcase("216342","Verify the display of SPORT TROPHIES category in Crown nav desktop/tablet");
$t.start();
try
{
	_click(_link("Trophies"));
	_assertVisible(_div("Sport Trophies"));
	var $items=_collectAttributes("_link","/(.*)/","sahiText",_above(_paragraph("/See All Sports/")));
	log($items);
		for(var $i=0;$i<$items;$i++)
			{
			log($items[$i]);
			_assertVisible(_link($items[$i]));
			//_click(_link($items[$i]);
			//_assertVisible(_div("subcategory-tiles"));
			_click(_link("Trophies"));
			}
}
catch($e)
		{
			_logExceptionAsFailure($e);
		}	
		$t.end()

var $t = _testcase("216341","Verify the display of OUR TROPHIES category in Crown nav desktop/tablet");
$t.start();
try
{	
	_click(_link("Trophies"));
	_assertVisible(_div("OUR TROPHIES"));
	
	var $items=_collectAttributes("_link", "/(.*)/", "sahiText", _above(_div("Sport Trophies"))).toString().split(",");
	for(var $i=2;$i<$items.length;$i++)
		{
				_log($item);
				_click(_link($items[$i]));
				_assertContainsText($items[$i],_div("breadcrumb"));
				_assertVisible(_div("OUR TROPHIES"));
				_click(_link("Trophies"));
				//_assertVisible(_div("OUR TROPHIES"));
		}
}
	
catch($e)
{
	_logExceptionAsFailure($e);
	
}	
$t.end()

var $t = _testcase("216287","Verify the UI of Crown Nav category");
$t.start();
try
{
	
	_click(_link("Trophies"));
	_assertVisible(_div("OUR TROPHIES"));
	_assertVisible(_div("Sport Trophies"));
	_assertVisible(_div("Trophy Finders"));
	_assertVisible(_div("Crown Catalogs"));
	_assertVisible(_div("breadcrumb"));
	_assertVisible(_div("recent view catalog"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
	
}	
$t.end()

var $t = _testcase("216285,216284,216283,216249,216250,216251,Verify the navigation of the subcategories in Medal sport valley category in Custom nav,Verify the navigation of the subcategories set2 in Medal sport valley category in Custom nav");
$t.start();
try
{
	
	_click(_link("Custom Medals", _in(_div("Custom Medals Custom Logo Awards"))));
	//_assertVisible(_div("Medals Sport Gallery", _in(_div("LeftNav"))));
	//_assertVisible(_link("Skating Medals", _in(_div("LeftNav"))));
	var $items=_collectAttributes("_link","/Medals/","sahiText",_above(_div("Custom Logo Awards[1]")));
	for(var $i=0;$i<$items.length;$i++)
		{
		//_assertVisible(_div($items[$i], _in(_div("LeftNav"))));
		_click(_link($items[$i]));
		_assertContainsText($items[$i], _heading1($items[$i], _in(_div("bottomText"))));
		_click(_link("Custom Medals", _in(_div("Custom Medals Custom Logo Awards"))));
		}
}

catch($e)
{
	_logExceptionAsFailure($e);
	
}	
$t.end()

var $t =_testcase("216282,216252,216248,Verify the navigation of  Crown Catalogs category in the Custom left nav in desktop/tablet,Verify the navigation of Custom logo awards subcategories in custom nav desktop/tablet")
	$t.start();
try
{
	
	_click(_link("Custom Medals", _in(_div("Custom Medals Custom Logo Awards"))));
	//_assertVisible(_div("Medals Sport Gallery", _in(_div("LeftNav"))));
	var $items=_collectAttributes("_link","/(.*)/","sahiText",_under(_div("Custom Logo Awards[1]")));
	for(var $i=0;$i<$items.length;$i++)
	{
	_click(_link($items[$i]));
	//_assertVisible(_heading1($items[$i], _in(_div("ttt bigtext"))));
	_assertContainsText($items[$i], _heading1($items[$i], _in(_div("bottomText"))));
	_click(_link("Custom Medals", _in(_div("Custom Medals Custom Logo Awards"))));
	}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
	
}	
$t.end()
var $t =_testcase("216234,Verify the UI Custom left nav in desktop/tablet")
$t.start();
try
{
	_click(_link("Custom Medals", _in(_div("Custom Medals Custom Logo Awards"))));
	_assertVisible(_div("Medals Sport Gallery", _in(_div("LeftNav"))));
	_assertVisible(_div("CUSTOM MEDALS", _in(_div("LeftNav"))));
	_assertVisible(_div("Custom Logo Awards", _in(_div("LeftNav"))));
	_assertVisible(_div("Trophy Finders", _near(_div("LeftNav"))));
	_assertVisible(_div("Crown Catalogs", _near(_div("LeftNav"))));
	_assertVisible(_image("/(.*)/", _in(_div("leftPartyFavors"))));
	_assertVisible(_image("/(.)/", _near(_div("leftPartyFavors"))));
	
	
}
catch($e)
{
	_logExceptionAsFailure($e);
	
}	
$t.end()



var $t =_testcase("217923,Verify the display of heading of the Basketball trophies page");
$t.start();
try
{
	_click(_link("Select Sport Or Activity"));
	_click(_link("Basketball"));
	_click(_link("Basketball Cup Trophies"));
	_assertVisible(_heading1("Basketball Cup Trophies"));
	_assertVisible(_list("search-result-items"));
}
catch($e)
{
	_logExceptionAsFailure($e);
	
}	
$t.end()



var $t =_testcase("216451,Verify the navigation of Award by Sport _mobile category landing page");
$t.start();
try
{
	_assertVisible(_link("Award By Sport", _in(_listItem("stockcategories ipad-height-cal"))));
	_click(_link("Award By Sport", _in(_listItem("stockcategories ipad-height-cal"))));
	_assertVisible(_div("awardbysport", _in(_div("main"))));
	_assertVisible(_div("breadcrumb"));
	_assertVisible(_textbox("categorySearch", _in(_div("search-box-main"))));
	//_assertEqual($AwardsBySport[3][1], _getText(_div("breadcrumb")));
	_assertEqual("Back Home Award By Sport", _getText(_div("breadcrumb")));
	_assertVisible(_div("Search Catalog Search"));
	_setValue(_textbox("q", _in(_div("Search Catalog Search"))), "Basketball Trophies");
	_click(_submit("Search"));
	_assertVisible(_div("Basketball", _in(_div("home-main-left"))));
	_assertVisible(_heading1("Basketball Trophies Trophies", _in(_div("search-result-content"))));
	_click(_link("/Xtreme Basketball Trophy/",_in(_listItem("grid-tile new-row"))));
	_click(_link("Home", _in(_div("breadcrumb"))));
}
catch($e)
{
	_logExceptionAsFailure($e)
	
}

