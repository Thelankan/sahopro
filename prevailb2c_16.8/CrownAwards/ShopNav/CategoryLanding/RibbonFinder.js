_log("*********************** Ribbon Finder ********************");
_resource("Category_landing.xls");

_include("../../util.GenericLibrary/GlobalFunctions.js");

var $ribbon_finder=_readExcelFile("Category_landing.xls","RibbonFinder");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("215982","Verify the functionality of bread crumb of Finder ribbon product listing page in desktop/tablet");
$t.start();
try
{
	
//Navigate to homepage
NavigatetoHomepage();
//Click on Insert finder link in the left nav
_click(_link($ribbon_finder[0][0], _in(_listItem($ribbon_finder[0][0]))));
_assertVisible(_div("breadcrumb"));
_assertVisible(_link("Home",_div("breadcrumb")));
_assertVisible(_link("Ribbon Finder",_div("breadcrumb")));

//Display of breadcrumb
if(isMobile())
{
	//verifying functionality of bread crumb
	_assertEqual($ribbon_finder[1][1], _getText(_div("breadcrumb")));
}
else
	{
	//verifying functionality of bread crumb
	_assertEqual($ribbon_finder[0][1], _getText(_div("breadcrumb")));
	}	

//click on home link in bread crumb
_click(_link("Home", _in(_div("breadcrumb"))));
//verify the home page
_assertNotVisible(_div("breadcrumb"));
_assertVisible(_div("home-main-right"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("","");
$t.start();
try
{

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

/*var $t = _testcase("","");
$t.start();
try
{

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()*/