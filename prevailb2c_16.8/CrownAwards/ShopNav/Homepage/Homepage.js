_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Homepage.xls");

var $homepagedata=_readExcelFile("Homepage.xls","HomepageData");



//SiteURLs();
_setAccessorIgnoreCase(true); 
//cleanup();

var $productnameinhomepage;
var $image="/thumb-link/";
var $shopnow="show-now-link";
var $namelink="/name-link/";
var $slidetrack="slick-track"
var $slidetrack1="slick-track[1]"

/*function ClickofproductsinHomepage($clickbleitem,$track)
{
	_click(_link($clickbleitem,_in(_div($track,_in(_div("home-main-right"))))));
	var $productnameinPDP=_getText(_heading1("product-name")).toString();
	_setAccessorIgnoreCase(true); 
	_assertEqual($productnameinhomepage,$productnameinPDP);
	_setAccessorIgnoreCase(true); 
	_assertVisible(_span($productnameinhomepage, _in(_div("breadcrumb"))));
}*/

var $t = _testcase("216531/216534","Verify the display of Home Page Left navigation Pane/Verify the application navigation on click of links in the left navigation pane.");
$t.start();
try
{

	var $leftNavs=_count("_div","/LeftNavHdr/",_in(_div("secondary")));

	for (var $i=0;$i<$leftNavs;$i++)
	{
	var $leftnavlinks=_collectAttributes("_link","/(.*)/","sahiText", _in(_div("LeftNav["+$i+"]")));

		for (var $j=0;$j<3;$j++)
			{
				//visibility of links
				_assertVisible(_link($leftnavlinks[$j],_in(_div("LeftNav["+$i+"]"))));
				//click on left nav links
				_click(_link($leftnavlinks[$j],_in(_div("LeftNav["+$i+"]"))));
				//should navigate to respective page
				_assertVisible(_span($leftnavlinks[$j], _in(_div("breadcrumb"))));
				//navigate to home page
				NavigatetoHomepage();
			    _wait(2000);

			}
	}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()



var $t = _testcase("216526","Verify the Position of the Promotion content in the Home Page");
$t.start();
try
{
	
//promo slot
_assertVisible(_div("home-promocontent"));
_assertVisible(_image("/(.*)/", _in(_div("hp-valuebanner"))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()*/

	
//*************************************Removed test cases**********************************************************
	

//var $t = _testcase("216618/216623","Verify the application navigation on click of product name in the product carrosil section in the home Page body." +
//"Verify the application behvaior on click of links n the product tile of the Scrolling section in home Page body.");
//$t.start();
//try
//{
//
////product name on slide track
//$productnameinhomepage=_getText(_link("/name-link/",_in(_div("slick-track",_in(_div("home-main-right"))))));
////verifying action after on click of image in home page slide track
//ClickofproductsinHomepage($image,$slidetrack);
////navigate to home page
//NavigatetoHomepage();
////verifying action after on click of namelink in home page slide track
//ClickofproductsinHomepage($shopnow,$slidetrack);
////navigate to home page
//NavigatetoHomepage();
////verifying action after on click of shopnow link in home page slide track
//ClickofproductsinHomepage($namelink,$slidetrack);
////navigate to home page
//NavigatetoHomepage();
//
////for slide track's below
//$productnameinhomepage=_getText(_link("/name-link/",_in(_div("slick-track[1]",_in(_div("home-main-right"))))));
////verifying action after on click of image in home page slide track
//ClickofproductsinHomepage($image,$slidetrack1);
////navigate to home page
//NavigatetoHomepage();
////verifying action after on click of namelink in home page slide track
//ClickofproductsinHomepage($shopnow,$slidetrack1);
////navigate to home page
//NavigatetoHomepage();
////verifying action after on click of shopnow link in home page slide track
//ClickofproductsinHomepage($namelink,$slidetrack1);
////navigate to home page
//NavigatetoHomepage();
//
//}
//catch($e)
//{
//_logExceptionAsFailure($e);
//}	
//$t.end()

//var $t = _testcase("216636","Verify the application behavior when only 4 or less than 4 products are configured for scrolling section of Home Page");
//$t.start();
//try
//{
//
//	//_assertVisible(_button("slick-next",_in(_list("horizontal-carousel[5]",_in(_div("home-main-right"))))))      
//	var $SlideTrackcount=_count("_div","slick-track",_in(_div("/home-product-carousel/")));
//
//	for (var $j=1;$j<=$SlideTrackcount;$j++)
//	{
//
//	var $countofProducts=_count("_link","name-link",_in(_div("slick-track["+$j+"]")));
//	
//		_log($countofProducts);
//		
//		if ($countofProducts<4)
//			{
//			_assertVisible(_button("slick-prev slick-disabled["+$j+"]"));
//			_assertVisible(_button("slick-next slick-disabled["+$j+"]"));
//			}
//	
//	}
//	
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end()


//	var $t = _testcase("216543","Verify the Mouse hover functionality on left navigation links in left Navigation Pane.");
//$t.start();
//try
//{
//	
//var $leftNavs=_count("_div","/LeftNavHdr/",_in(_div("home-main-left")));
//
//for (var $i=0;$i<$leftNavs;$i++)
//{
//var $leftnavlinks=_collectAttributes("_link","/(.*)/","sahiText", _in(_div("LeftNav["+$i+"]")));
//
//	for (var $j=0;$j<$leftnavlinks.length;$j++)
//		{
//		//mouse hover on payment link	
//		 _mouseOver(_link($leftnavlinks[$j],_in(_div("LeftNav["+$i+"]"))));
//		 
//		 if (_getAttribute(_link($leftnavlinks[$j],_in(_div("LeftNav["+$i+"]"))),"title")!=null)
//		 {
//		     _assert(true);
//		 }
//		else
//		{
//		     _assert(false);
//		} 
//		}
//}	
//
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end()
//
//var $t = _testcase("216548/216620/216620/216619","Verify the Active and Inactive state of the Product carrosil in home Page/Verify the UI of the Scrolling section in the Home Page body/Verify the source of the products in product carrosil widget of the Home Page");
//$t.start();
//try
//{
//
////display of see all links
//_assertVisible(_link($homepagedata[0][0], _in(_div("content-header"))));	
//	
//var $ProductCountinCarrosil=_count("_link","thumb-link", _in(_div("slick-track",_in(_div("home-main-right")))));	
//
//if ($ProductCountinCarrosil==1)
//	{
//	_assertVisible(_button("slick-next slick-disabled",_in(_div("home-main-right"))));
//	_assertVisible(_button("slick-prev slick-disabled",_in(_div("home-main-right"))));
//	}
//else
//	{
//	
//	//previous link should be disabled
//	_assertVisible(_button("slick-prev slick-disabled",_in(_div("home-main-right"))));
//	//next link should be enabled
//	_assertVisible(_button("slick-next",_in(_div("home-main-right"))));
//	
//	while (_isVisible(_button("slick-next",_in(_div("home-main-right")))))
//	{
//		_click(_button("slick-next",_in(_div("home-main-right"))));
//	}
//	_assertVisible(_button("slick-next slick-disabled",_in(_div("home-main-right"))));
//	
//	while (_isVisible(_button("slick-prev",_in(_div("home-main-right")))))
//	{
//		_click(_button("slick-prev",_in(_div("home-main-right"))));
//	}
//	//previous link should be disabled
//	_assertVisible(_button("slick-prev slick-disabled",_in(_div("home-main-right"))));
//	
//	}
//
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end()

//var $t = _testcase("216617/216625/216625/216635","Verify the UI of the product Carrosil section in Home Page body./Verify the Active and Inactive state of the Carrosil arrows in scrolling section in the Home Page body.");
//$t.start();
//try
//{
//
//         
////_assertVisible(_button("slick-next",_in(_list("horizontal-carousel[5]",_in(_div("home-main-right"))))))      
//var $SlideTrackcount=_count("_div","slick-track",_in(_div("home-main-right")));
//
//for (var $j=0;$j<$SlideTrackcount;$j++)
//{
//
//var $countofProducts=_count("_link","name-link",_in(_div("slick-track["+$j+"]")));
//
//	for (var $i=0;$i<$countofProducts;$i++)
//	{
//	
//	if (_isVisible(_button("slick-next",_in(_list("horizontal-carousel["+$j+"]",_in(_div("home-main-right")))))) || (_isVisible(_button("slick-prev",_in(_list("horizontal-carousel["+$j+"]",_in(_div("home-main-right"))))))))
//	{
//		_assertVisible(_link("name-link["+$i+"]",_in(_div("slick-track["+$j+"]",_in(_div("home-main-right"))))));
//		_assertVisible(_div("product-pricing["+$i+"]",_in(_div("slick-track["+$j+"]",_in(_div("home-main-right"))))));
//		_assertVisible(_div("product-image ["+$i+"]",_in(_div("slick-track["+$j+"]",_in(_div("home-main-right"))))));
//		_assertVisible(_link("show-now-link["+$i+"]",_in(_div("slick-track["+$j+"]",_in(_div("home-main-right"))))));
//		_click(_button("slick-next["+$j+"]",_in(_div("home-main-right"))));
//	}		
//
//	}
//	
//	for (var $k=$countofProducts-1;$k>=0;$k--)
//	{
//		
//	if (_isVisible(_button("slick-next",_in(_list("horizontal-carousel["+$j+"]",_in(_div("home-main-right")))))) || (_isVisible(_button("slick-prev",_in(_list("horizontal-carousel["+$j+"]",_in(_div("home-main-right"))))))))
//	{
//		_assertVisible(_link("name-link["+$k+"]",_in(_div("slick-track["+$j+"]",_in(_div("home-main-right"))))));
//		_assertVisible(_div("product-pricing["+$k+"]",_in(_div("slick-track["+$j+"]",_in(_div("home-main-right"))))));
//		_assertVisible(_div("product-image ["+$k+"]",_in(_div("slick-track["+$j+"]",_in(_div("home-main-right"))))));
//		_assertVisible(_link("show-now-link["+$k+"]",_in(_div("slick-track["+$j+"]",_in(_div("home-main-right"))))));
//		_click(_button("slick-prev["+$j+"]",_in(_div("home-main-right"))));
//	}
//
//	}
//}
//
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end()

