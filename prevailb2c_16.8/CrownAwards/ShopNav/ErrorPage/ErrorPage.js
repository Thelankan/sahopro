//including external files
_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("ErrorPage.xls");


//declaring excel files
var $ErrorPage=_readExcelFile("ErrorPage.xls","ErrorPage");


SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();



var $t=_testcase("216642/216644","Verify the navigation of 404 page in Desktop");
$t.start();
try
{		
	
	//navigate to 404 error page
	_navigateTo($ErrorPage[0][1]);
	//verify navigation
	_assertVisible(_span($ErrorPage[0][2]));
	_assertVisible(_image("/(.*)/",_in(_div("sorryheader"))));
	_assertVisible(_listItem($ErrorPage[0][3]));
	_assertVisible(_listItem($ErrorPage[1][3]));
	_assertVisible(_listItem($ErrorPage[2][3]));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("216645/216649","Verify the display of Search again box in 404 page in Desktop");
$t.start();
try
{	
	
	//verify search again
	_assertVisible(_heading2($ErrorPage[0][4]));
	//need more we can help section
	_assertVisible(_paragraph($ErrorPage[2][2]));
	_assertVisible(_div("content-asset",_in(_div("searchbox"))));
	_assertVisible(_textbox("input-text"));
	_assertVisible(_submit("simplesearch"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("216651","Verify the display of 'You can also shop..' title in 404 page in Desktop");
$t.start();
try
{
	
	//You can shop also section
	_assertVisible(_heading3($ErrorPage[1][2]));
	_assertVisible(_div("category-slot"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("216646","Verify the functionality of Search again box in 404 page in Desktop");
$t.start();
try
{
	

	//navigate to 404 error page
	_navigateTo($ErrorPage[0][1]);
	//verify navigation
	_assertVisible(_span($ErrorPage[0][2]));
	_assertVisible(_image("/(.*)/",_in(_div("sorryheader"))));
	_assertVisible(_listItem($ErrorPage[0][3]));
	_assertVisible(_listItem("/"+$ErrorPage[1][3]+"/"));
	//verify search box
	//enter the value in 404 text field and hit enter
	_setValue(_textbox("input-text searchinput valid"), $ErrorPage[0][0]);
	_focusWindow();
	_focus(_textbox("input-text searchinput valid"));
	_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);

	//verify search result
	_assertContainsText($ErrorPage[0][0], _div("cat-headings desktop-show"));
	_assertVisible(_div("main-content"));

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("216654","Verify the functionality of any category link in 'Awards and Categories' grid  in 404 page in Desktop");
$t.start();
try
{
	
	//navigate to 404 error page
	_navigateTo($ErrorPage[0][1]);
	//verify navigation
	var $prodName=_getText(_div("text-bottom")).replace("Shop","").trim();
	//click on product name
	_click(_div("text-bottom"));
	//verify navigation
	_assertVisible(_span("breadcrumb-element"));
	_assertEqual($prodName, _getText(_span("breadcrumb-element")));
	_assertVisible(_heading1($prodName));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("216652","Verify the display of 'Awards and Categories' grid in 404 page in Desktop");
$t.start();
try
{

	//navigate to 404 error page
	_navigateTo($ErrorPage[0][1]);
	//verify navigation
	var $prodName=_getText(_div("text-bottom")).replace("Shop","").trim();
	//click on product name
	_click(_div("text-bottom"));
	_assert(false,"last 1 step did not automated")
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("222374/222373","Verify the application behavior on click of bread crumb/display of bread crumb in the 404 page for desktop/tablet");
$t.start();
try
{
	//navigate to 404 error page
	_navigateTo($ErrorPage[0][1]);
	//verify navigation
	_assertEqual($ErrorPage[0][4], _getText(_div("breadcrumb")));
	//Home link
	_click(_link("Home", _in(_div("breadcrumb"))));
	//Home page
	_assertVisible(_div("/home-main/"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();