_include("../GenericLibrary/GlobalFunctions.js");
_resource("SanityData.xls");

//ClearCookies();
SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("304805","Verify the edit engarving functionality on cart page");
$t.start();
try
{
	
//navigate to cart page with single product 
navigateToCart($genericData[3][0],$genericData[0][1]);
//engravement view button
_assertVisible($VIEWENGRAVEMENT_LINK);
//click on edit link in cart page
_click($VIEWENGRAVEMENT_LINK);
//engravement bold
_assertVisible($VIEWENGRAVEMENT_LINK);
//Text in engravement
_assertEqual($userLog[0][3],_getText($VIEWENGRAVEMENT_TEXTIN_CART_PAGE));
//engravement 
if (_isVisible($ENGRAVEMENT_HEADING))

	{
	
	if ((_isVisible($ENGRAVEMENT_DROPDOWN_SECTIONONE)) || (_isVisible($ENGRAVEMENT_DROPDOWN_SECTION)))
	{
	
	var $count=EngravementDropDownCount();

var $q=1;	
for (var $i=1;$i<=$count;$i++)
	{
	
	if ($i==1)
		{
		_setSelected(_select("contentSelect_1_"+$i+""), "Ornament");
		}
		else
			{
			_setSelected(_select("contentSelect_1_"+$i+""), "Text");
			_setValue(_textbox("lineInputAdvanced["+$q+"]"),$genericData[0][5]);
			_wait(1000)
			if (_isVisible($ENGRAVEMENT_REPEAT_ALL_CHECKBOX))
			{
				_check(_checkbox("lineCheckboxAdvanced["+$q+"]"));
			}
			
			$q++;
			
			}
	}
else
	{
	
	var $count=EngravementTextBoxCount();
	for (var $j=0;$j<$count;$j++)
		{
		_setValue(_textbox("lineInput["+$j+"]",_in($ENGRAVEMENT_TEXTBOX_CONTAINER)),$genericData[0][5]);
		
		if (_isVisible($ENGRAVEMENT_REPEAT_ALL_CHECKBOX))
		{
			if (!_checkbox("lineCheckbox["+$j+"]").checked)
				{
				_click(_checkbox("lineCheckbox["+$j+"]"));
				}
		}
		
		}

	}

_check($ENGRAVEMENT_CONFIRM_CHECKBOX);
_click($ADDTOCART_BUTTON_ENGRAVEMENT_PAGE);

if (_isVisible($ENGRAVEMENT_TIE_IN_POPUP))
	{
	_click($ENGRAVEMENT_TIE_IN_POPUP_CLOSE_BUTTON);
	}

	}
}

//click on edit link in cart page
_click($VIEWENGRAVEMENT_LINK);
//Text in engravement
_assertEqual($genericData[0][5],_getText($VIEWENGRAVEMENT_TEXTIN_CART_PAGE));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("304787","Verify the Remove Link functionality for a Single trophy Product in cart Page");
$t.start();
try
{
	

//click mini cart pink
_click($VIEWCART_LINK_IN_MINICART);

var $countofproducts=ProductCountInCart();

if ($countofproducts==1)
	{
	_click($REMOVE_LINK_INCARTPAGE);
	_assertNotVisible($CART_PRODUCT_ROW);
	}
	else
		{
		_click($REMOVE_LINK_INCARTPAGE);
		var $countofproductsafterdelete=ProductCountInCart();
		_assertEqual($countofproducts-1,$countofproductsafterdelete);
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("304806","Verify the empty cart functionality.");
$t.start();
try
{
	
//navigate to cart page with single product 
navigateToCart($genericData[2][0],$genericData[0][1]);
//verify empty cart functionality
//clearing cart
if (_isVisible($EMPTYCART_BUTTON_CART_PAGE))
	{
	//Empty cart button
	_click($EMPTYCART_BUTTON_CART_PAGE);
	}
//ok in modal overlay
if(_isVisible($EMPTYYOUR_CART_OVERLAY))
	{
		_click($YES_BUTTON_IN_EMPTYCART_OVERLAY);
	}
//empty cart message
_assertVisible($CART_YOUR_CART_IS_EMPTY_MESSAGE);


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

