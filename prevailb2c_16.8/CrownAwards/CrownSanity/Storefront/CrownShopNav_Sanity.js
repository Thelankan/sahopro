_include("../GenericLibrary/GlobalFunctions.js");
_resource("SanityData.xls");

//ClearCookies();
SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

var $t = _testcase("304757","Verify the application behavior on click of Checkout button in global header when no product list part of cart as a Guest user.");
$t.start();
try
{
	
//click on checkout button in header with out adding products
_click($CHECKOUT_BUTTON_BESIDE_MINI_CART);
//Handling popup
_expectConfirm("/Shopping Cart is currently empty,cannot Checkout/", true); 

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("304754","Verify the display of search suggestions drop down for a guest user as per characters entered.");
$t.start();
try
{
	
for (var $l=0;$l<5;$l++)
	{
_setValue($SEARCH_TEXTBOX,$genericData[$l][4]);

if($l==4)
	{
	_wait(4000);
	_assertVisible($SEARCH_SUGGESTION_SECTION);
	_assertVisible($SEARCH_PRODUCT_LINK_SUGGESTION);
	_assertVisible($SEARCH_PRODUCT_LINK_SUGGESTION_WITH_IMAGE);
	_assertVisible($SEARCH_IMAGE_SUGGESTION);
	}
else
	{
	_wait(3000);
	_assertNotVisible($SEARCH_SUGGESTION_SECTION);
	}

	}

}

var $t = _testcase("304755/304756","Verify the Mega Menu behavior in the global header as a Guest User.");
$t.start();
try
{
	
//Stock categories
var $stockcategoriesCount=CountofRootCategories();
var $stockcategoriesNames=RootCategoryNames();

for (var $j=0;$j<$stockcategoriesCount;$j++)
{
	_click(_link($stockcategoriesNames[$j]));
	if($j==5)
		{
		//Verifying category landing page
		_assertVisible(_span("Sculpture Trophies", _in($PAGE_BREADCRUMB)));
		}
	else
		{
		//Verifying category landing page
		_assertVisible(_span($stockcategoriesNames[$j], _in($PAGE_BREADCRUMB)));
		}
	
}

//Custom categories
var $customcategoriesCount=CountofCustomCategories();
var $customcategoriesNames=CustomCategoryNames();

for (var $j=0;$j<$customcategoriesCount;$j++)
{
	_click(_link($customcategoriesNames[$j]));
	//Verifying category landing page
	_assertVisible(_span($customcategoriesNames[$j], _in($PAGE_BREADCRUMB)));
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("304758","Verify the display of SHOP NOW button for column trophy image in the Column finder product listing page desktop/tablet");
$t.start();
try
{
	
//Navigate to homepage
NavigatetoHomepage();
_click($COLUMN_LINK_HOMEPAGE_LEFTNAV);
//Display of Shop now button
_assertVisible($COLUMN_FINDER_PRODUCT_SHOPNOW_BUTTON);
//Product image
_assertVisible($COLUMN_FINDER_PRODUCT_IMAGE);
//product price
_assertVisible($COLUMN_FINDER_PRODUCT_PRICING);
//breadcrumb
_assertEqual($columnfinder[2][1], _getText($PAGE_BREADCRUMB));
//Click Shop now button
_click($COLUMN_FINDER_PRODUCT_SHOPNOW_BUTTON);
//Prod name in PDP
_assertEqual($prodname, _getText($PRODUCTNAME_IN_PDP));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("304763/304770/304762","Verify the functionality related to EDIT link on PDP in Crown Awards application as an anonymous and registered user./Verify the functionality related to the Repeats on all check boxes on engraving engine modal for multiple products on PDP in Crown Awards application as an anonymous and registered user." +
		"Verify the functionality related to ADD TO CART and Engrave button on PDP in Crown Awards application as an anonymous and registered user.");
$t.start();
try
{

//Navigate to PDP
navigateToPdp($genericData[2][0]);
//select swatch 
selectSwatch();
//selecting quantity
_setValue($QUANTITY_TEXTBOX_IN_PDP,"2");
//click on add to cart button and add to engrave 
_click($ADDTOCART_ENGRAVE_IN_PDP);

//check the repeat all button
if (_isVisible($ENGRAVEMENT_HEADING))
{
if ((_isVisible($ENGRAVEMENT_DROPDOWN_SECTIONONE)) || (_isVisible($ENGRAVEMENT_DROPDOWN_SECTION)))
{
var $count=EngravementDropDownCount();

var $q=1;	
for (var $i=1;$i<=$count;$i++)
{

if ($i==1)
	{
	_setSelected(_select("contentSelect_1_"+$i+""), "Ornament");
	}
	else
		{
		_setSelected(_select("contentSelect_1_"+$i+""), "Text");
		_setValue(_textbox("lineInputAdvanced["+$q+"]"),$userLog[0][3]);
		_wait(1000)
		if (_isVisible($ENGRAVEMENT_REPEAT_ALL_CHECKBOX))
		{
			_check(_checkbox("lineCheckboxAdvanced["+$q+"]"));
		}
		
		$q++;
		
		}
}

}

else
{

var $count=EngravementTextBoxCount();
for (var $j=0;$j<$count;$j++)
	{
	_setValue(_textbox("lineInput["+$j+"]",_in($ENGRAVEMENT_TEXTBOX_CONTAINER)),$userLog[0][3]);
	
	if (_isVisible($ENGRAVEMENT_REPEAT_ALL_CHECKBOX))
	{
		if (!_checkbox("lineCheckbox["+$j+"]").checked)
			{
			_click(_checkbox("lineCheckbox["+$j+"]"));
			}
	}
	
	}

}

//Check the repeat all button text pre-populating
_assertEqual($userLog[0][3],_getText($ENGRAVEMENT_TEXTBOX1_WITH_POPULATED_DATA));
_assertEqual($userLog[0][3],_getText($ENGRAVEMENT_TEXTBOX2_WITH_POPULATED_DATA));
_assertEqual($userLog[0][3],_getText($ENGRAVEMENT_TEXTBOX3_WITH_POPULATED_DATA));

//click on add to check box and add to cart
_check($ENGRAVEMENT_CONFIRM_CHECKBOX);
_click($ADDTOCART_BUTTON_ENGRAVEMENT_PAGE);

if (_isVisible($ENGRAVEMENT_TIE_IN_POPUP))
{
_click($ENGRAVEMENT_TIE_IN_POPUP_CLOSE_BUTTON);
}

}
//click on edit link in cart page
_click($EDIT_ITEM_LINK_IN_CART);
//should navigate to PDP page
_assertVisible($PDP_MAIN_LINK);
//comparing retain flexvalues
_assertEqual(_getSelectedText($PDP_FLEXDETAILS_DROPDOWN),$firstFlexValue);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("304764/304760","Verify the functionality related to Add Another button on PDP in Crown Awards application as an anonymous and registered user./Verify the functionality related to Social icons on PDP in Crown Awards application as an anonymous and registered user.");
$t.start();
try
{

//add product to cart
search($genericData[0][0]);
//selecting product if it navigates to search result page
if (_isVisible($SEARCHRESULT_PRODUCTS))
	{
	_click($SEARCHRESULT_PRODUCT_LINK);
	}

//selecting swatches
selectSwatch();
$totalpriceBeforeselectingQuantity=_extract(_getText($PRICE_IN_PDP),"/[$](.*)/",true);
//Should verify error Message
_setValue($QUANTITY_TEXTBOX_IN_PDP,$genericData[1][1]);
//click on add to cart button
_click($ADDTOCART_ENGRAVE_IN_PDP);
//Error message verification
_assertVisible($REDUCE_QUANTITY_POPUP);
_assertVisible($REDUCE_QUANTITY_BUTTON);
_assertVisible($SOLDOUT_MESSAGE_IN_POPUP);
_assertVisible($REDUCE_QUANTITY_ERROR_MESSAGE_POPUP);
_click($REDUCE_QUANTITY_POPUP_CLOSE_BUTTON);
//selecting quantity
_setValue($QUANTITY_TEXTBOX_IN_PDP,$genericData[0][1]);
_wait(5000);
$prodname=_getText($PRODUCTNAME_IN_PDP);
$prodquantity=_getText($QUANTITY_TEXTBOX_IN_PDP);
$totalprice=_extract(_getText($PRICE_IN_PDP),"/[$](.*)/",true);
_log($totalprice);	
//click on add to cart button
_click($ADDTOCART_ENGRAVE_IN_PDP);

while (_isVisible($SOLDOUT_INFO_POPUP))
	{
	_wait(3000);
	_click($ALTERNATE_IMAGES_IN_SOLDOUT_POPUP);
	//click on add to cart button
	_click($ADDTOCART_ENGRAVE_IN_PDP);
	}

//engravement
engraving();

//closing "Added item to cart" Product
if (_isVisible($ADDANOTHER_ITEM_TO_CART_POPUP))
	{
	_click($ADDANOTHER_ITEM_BUTTON_TO_CART_IN_POPUP);
	}

//comparing flex values should not retain
_assertNotEqual(_getSelectedText($PDP_FLEXDETAILS_DROPDOWN),$firstFlexValue);
		
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("304768","Verify the functionality related to ADD ALL AWARDS AND ENGRAVE button when multiple products trophies are present on 'PDP in Crown Awards application as an anonymous or registered user when product option is selected.");
$t.start();
try
{
	//Navigate to PDP
	navigateToPdp($genericData[4][0]);

	$checkboxmain=FlexlistCount();
	_log($checkboxmain);
	
	var $flexdetailsmobile_hide=0;
	
	for (var $p=0;$p<$checkboxmain.length;$p++)
	{
			_check(_checkbox("/prdSelect"+$checkboxmain[$p]+"/"));
			_wait(3000)
			if (_isVisible(_select("flexdetails", _in(_div("prodwrapper["+$p+"]")))))
			{
				_setSelected(_select("flexdetails", _in(_div("prodwrapper["+$p+"]"))),4);
				_wait(3000);
			}
			else if (_isVisible(_checkbox("/check/", _in(_div("flexdetails mobile-hide["+$flexdetailsmobile_hide+"]", _in(_div("prodwrapper["+$p+"]")))))))
			{
				_check(_checkbox("/check/", _in(_div("flexdetails mobile-hide["+$flexdetailsmobile_hide+"]", _in(_div("prodwrapper["+$p+"]"))))))
				_wait(3000);
				$flexdetailsmobile_hide++;
			}
			else if (_isVisible(_radio("/radio/", _in(_div("flexdetails mobile-hide["+$flexdetailsmobile_hide+"]", _in(_div("prodwrapper["+$p+"]")))))))
			{
				_check(_radio("/radio/", _in(_div("flexdetails mobile-hide["+$flexdetailsmobile_hide+"]", _in(_div("prodwrapper["+$p+"]"))))))
				_wait(3000);
				$flexdetailsmobile_hide++;
			}
			
			if (_isVisible($IMAGE_UPLOAD_POPUP))
			{
				_click($CLOSE_BUTTON_IN_IMAGE_UPLOAD_POPUP)
			}
	
			while (_isVisible($IMAGE_UPLOAD_POPUP_DIALOGUE))
			{
				_click($IMAGE_UPLOAD_POPUP_DIALOGUE_CLOSE_BUTTON);
			}
			//selecting quantity
			_setValue(_numberbox("Quantity", _in(_div("prodwrapper["+$p+"]"))), "1");
			//verify the values
			_assertEqual("1", _getValue(_numberbox("Quantity", _in(_div("prodwrapper["+$p+"]")))));
			//add to cart button should enable
			_assertEqual(false, _submit("Add to Cart & Engrave["+$p+"]").disabled);
	}
	
	//add all to engrave
	_click($ADD_ALLAWARDSTO_CART_AND_ENGRAVE);
	_wait(5000);
	//engraving
	engraving();
	//navigate to checkout page
	_click($CHECKOUT_BUTTON_BESIDE_MINI_CART);
	//shipping details
	shippingAddress($Address_data,$t);
	_check($SHIPPING_ADDRESS_SAME_AS_BILLING_CHECKBOX);
	//address proceed
	adddressProceed();
	
	//selecting shipping method
	_check($SHIPPING_METHOD_ID);
	
	//Shipment date
	eventcalendar();
	
	_click($SHIPPING_CONTINUE_BUTTON);
	
	adddressProceed();
	//Payment details
	PaymentDetails($credit_card,$t);
	//Placing order
	//_check(_checkbox("/dwfrm_billing_confirm/"));
	
	_click($BILLING_PLACE_ORDER_BUTTON);

	//Checking order is placed or not
	_assertVisible(_heading1($pdp[0][6]));
	_assertVisible($ORDER_CONFIRMATION_DETAILS);


	var $orderno=_getText($ORDER_CONFIRMATION_NUMBER).split(" ")[2];
	_log($orderno);

	var $orderkkkk=_getText($ORDER_CONFIRMATION_NUMBER);
	_log($orderkkkk);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase(" "," ");
$t.start();
try
{
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/