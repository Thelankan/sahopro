_include("../GenericLibrary/GlobalFunctions.js");
_resource("SanityData.xls");

//ClearCookies();
SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("304798/304799/304759","Verify if a guest user can create an account in the application");
$t.start();
try
{
	
//click on login link
 _click($LOGINLINK);
//click on create account link
_click($CREATELOGIN_BUTTON);	
//Create account from the cart page
createAccount();
//Logout link should be visible
_assertVisible($LOGOUTLINKTEXT);
//fetching first name from the header
var $firstName=_extract(_getText($HEADERHITEXT),"/Hi (.*)/",true);
//assert Equal
_assertEqual($userData[$user][1],$firstName);
//Logout from the application
logout();
//click on login link
_click($LOGINLINK);
//login with created account
login( $userData[$user][3],$userData[$user][5]);
//Logout link should be visible
_assertVisible($LOGOUTLINKTEXT);
//click on delete card link
_click($HEADERYOURORDERSLINK);
_click($MYACCOUNTLINK_BREADCRUMB);   
//Navigate to settings and password page 
_click($SETTINGSANDPASSWORDLINK);
//changing password
_setValue($CURRENTPASSWORDTEXTBOX,$userData[$user][5]);
_setValue($CURRENTPASSWORDCONFIRMTEXTBOX,$userData[$user][5]);
_setValue($RESETPASSWORDTEXTBOX,$genericData[0][2]);
_setValue($RESETPASSWORDCONFIRMTEXTBOX,$genericData[0][2]);
_click($PASSWORDUPDATEBUTTON);
_wait(2000);

//logout form the application
logout();
//click on login link
_click($LOGINLINK);
//login with created account
login( $userData[$user][3],$genericData[0][2]);
//Logout link should be visible
_assertVisible($LOGOUTLINKTEXT);
//click on delete card link
_click($HEADERYOURORDERSLINK);
_click($MYACCOUNTLINK_BREADCRUMB);   
//Navigate to settings and password page 
_click($SETTINGSANDPASSWORDLINK);
//changing password and resetting back to original one
_setValue($CURRENTPASSWORDTEXTBOX,$genericData[0][2]);
_setValue($CURRENTPASSWORDCONFIRMTEXTBOX,$genericData[0][2]);
_setValue($RESETPASSWORDTEXTBOX,$userData[$user][5]);
_setValue($RESETPASSWORDCONFIRMTEXTBOX,$userData[$user][5]);
_click($PASSWORDUPDATEBUTTON);

//logout form the application
logout();

//Login to BM
BM_Login();
//Navigate to customers page
_click($BM_CUSTOMERSLINK);

_click($BM_CUSTOMERLINK_ONE);
_click($BM_ADVANCE_LINK);

_setValue($BM_ADVANCECUSTOMERSEARCH_BUTTON,$userData[$user][3]);
_click($BM_ADVANCECUSTOMERSEARCH_BUTTON);

//Should match the user created
_assertVisible(_cell($userData[$user][3]));
//log out from BM
_click($BM_LOGOFF_LINK);


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("304795/304782","Verify if the registered user can edit a saved address in the application/Verify the functionality related to 'CANCEL' button in the 'Add Address Form' Addresses page' in application as a  Registered user.");
$t.start();
try
{
	
//navigate to my account landing page
NavigatetoAccountLanding();
//click on address link
_click($ADDRESSESLINK);
//Navigate to My account page
_click($MYACCOUNTLINK_BREADCRUMB);
//Verify
_assertEqual("My Account",_getText($BREADCRUMB_LAST_IN_MYACCOUNTLANDINGPAGE));
//Delete address
deleteAddress();
//Click on add new address in addresses page
_click($ADDNEW_ADDRESS_LINK);
//address form should be visible
_assertVisible($ADDRESS_FORMFIELDS);
//click on cancel button
_click($ADDRESS_CANCEL_LINK);
//address form should not be visible
_assertNotVisible($ADDRESS_FORMFIELDS);
//Click on add new address in addresses page
_click($ADDNEW_ADDRESS_LINK);
//Create a address
addAddress($address,0);
//click on create address button
_click($ADDADDRESS_BUTTON);
_wait(4000);
//address comparison
var $comparingAddress=ComparingAddressFromAccountLandingPage(0);
_assertEqual($comparingAddress,_getText($MINIADDRESS_LOCATION));
//click on edit address link
_click($EDITADDRESS_LINK);
//change the values
addAddress($address,1);
//click on create address button
_click($ADDADDRESS_BUTTON);
_wait(4000);
//Changed address comparison
var $comparingAddress=ComparingAddressFromAccountLandingPage(0);
_assertEqual($comparingAddress,_getText($MINIADDRESS_LOCATION));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("304796","Verify if the registered user can delete a saved address in the application");
$t.start();
try
{

var $addressCountBeforeDelete=AddressCount();
//Delete Address
_click($ADDRESS_DELETE);
//Deleting one adderss
var $addressCountAfterDelete=AddressCount();
//comparison of deleted addresses
_assertEqual($addressCountAfterDelete,$addressCountBeforeDelete-1);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("304797/304785","Verify if a registered user can delete a saved credit card in the application/Verify the Navigation of 'Detail' Link in the Submit Payment Page for Desktop/ipad view");
$t.start();
try
{
	
//navigate to my account landing page
NavigatetoAccountLanding();
//click on make a payment link
_click($MAKE_A_PAYMENT_LINK);
//make a payment heading
_assertVisible($SUBMITPAYMENT_HEADING);
//checking in bread crumb
_assertEqual("Submit Payment",_getText($BREADCRUMB_LAST_TEXT));
_assertEqual("We have no order records for this account.", _getText($NOORDERS_MESSAGE));

//Navigate to account landing page & verifying the empty card
DeleteCreditCard();

//is visible payment list
if (_isVisible($PAYMENTLIST))
	{
	
		var $paymentCountBeforeDelete=PaymentListCount();
		
		//click on delete link
		_click($PAYMENT_DELETE_BUTTON);
		_assertVisible($REDFONT_SECTION_IN_PAYMENTLIST);
		//click on delete link
		_click($PAYMENT_DELETE_BUTTON);
		if(_isVisible($PAYMENTDELETE_CONFIRM_POPUP))
		{
			_click($PAYMENT_DELETE_BUTTON);
			_wait(3000);
		}
		
			if (_isVisible($PAYMENTLIST))
				{
				var $paymentCountAfterDelete=PaymentListCount();
				//deleting one card from the list
				_assertEqual($paymentCountAfterDelete,$paymentCountBeforeDelete-1);
				}
			else
				{
				_assertNotVisible($PAYMENTLIST)
				_assertVisible($NOCARDS_SAVED_MESSAGE);
				}
	}
else
	{
	//click on add new card
	_click($ADDNEW_PAYMENT_BUTTON);
    //add card detail
    CreateCreditCard($card_data,4);
    //card should be created
    _assertVisible($PAYMENTLIST);
	//click on delete link
	_click($PAYMENT_DELETE_BUTTON);
	_assertVisible($REDFONT_SECTION_IN_PAYMENTLIST);
	//click on delete link
	_click($PAYMENT_DELETE_BUTTON);
	if(_isVisible($PAYMENTDELETE_CONFIRM_POPUP))
		{
			_click($PAYMENT_DELETE_BUTTON);
			_wait(3000);
		}
	_assertNotVisible($PAYMENTLIST)
	_assertVisible($NOCARDS_SAVED_MESSAGE);
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("304771","Verify the functionality of 'START CHAT' button in Contact Us page for desktop/tablet");
$t.start();
try
{
	//click on contact us link in footer
	_click($FOOTERLINK_CONTACTUS);
	//breadcrumb   
	_assertVisible($HOME_CUSTOMERSERVICE_BREADCRUMB, _in($PAGE_BREADCRUMB));
	//content-header
	_assertVisible($CONTACTUS_HEADING);

	//display live chat
	_assertVisible($CHATMODULE_SECTION);
	_assertVisible($LIVECHAT_HEADING, _in($CHATMODULE_SECTION));
	_assertVisible(_link($START_CHART_BUTTON, _in($CHATMODULE_SECTION)));

	//Click Start chat
	_click(_link($START_CHART_BUTTON, _in($CHATMODULE_SECTION)));

	_wait(3000);
	var $recentWinID=_getRecentWindow().sahiWinId;
	_log($recentWinID);
	_selectWindow($recentWinID);

	//verify chat link popup
	_assertVisible($START_CHART_NEWWINDOW_HEADING);
	_assertVisible($START_CHART_SUPPORT_CHAT);
	_assertVisible($START_CHART_CHAT_SURVEY_TABEL);

	_closeWindow();
	_selectWindow();

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("304772","Verify the functionality of 'SUBMIT' button in Email Us section in Contact us page for desktop/tablet");
$t.start();
try
{

//click on contact us link in footer
_click($FOOTERLINK_CONTACTUS);
//entering details
contactusDetails($genericData,10);
//click on submit button
_click($CONTACTUS_SEND_BUTTON);
//thank you message should display
_assertVisible($CONTACTUS_CONFIRMATION_MESSAGE);
_assertVisible(_heading3($genericData[10][7]));
_assertEqual($genericData[10][8], _getText($CONTACTUS_CONFIRMATION_MESSAGE));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("304777","Verify the functionality of 'SUBMIT REQUEST' button in request a catalog page for desktop/tablet");
$t.start();
try
{
	
//click on request a catalog link
_click($REQUESTA_FREE_CATALOG_LINK);

//verifying request a catalog link
_assertVisible($REQUESTA_FREE_CATALOG_HEADING);
_assertVisible($HOME_CUSTOMERSERVICE_BREADCRUMB, _in($PAGE_BREADCRUMB));
_assertVisible($REQUESTA_FREE_CATALOG_IMAGE_SECTION);
_assertVisible($REQUESTA_FREE_CATALOG_SUBMIT_BUTTON, _in($REQUESTA_FREE_CATALOG_IMAGE_SECTION)));


//filling data in request a catalog field
_setValue($REQUESTA_FREE_CATALOG_FIRSTNAME_TEXTBOX,$drop6_data[0][4]);
_setValue($REQUESTA_FREE_CATALOG_LASTNAME_TEXTBOX,$drop6_data[1][4]);
_setValue($REQUESTA_FREE_CATALOG_ORGANIZATION_TEXTBOX,$drop6_data[2][4]);
_setValue($REQUESTA_FREE_CATALOG_ADDRESS1_TEXTBOX,$drop6_data[3][4]);
_setValue($REQUESTA_FREE_CATALOG_ADDRESS2_TEXTBOX,$drop6_data[4][4]);
_setValue($REQUESTA_FREE_CATALOG_ZIPCODE_TEXTBOX,$drop6_data[5][4]);
_setValue($REQUESTA_FREE_CATALOG_CITY_TEXTBOX,$drop6_data[6][4]);
_setSelected($REQUESTA_FREE_CATALOG_COUNTRY_TEXTBOX,$drop6_data[7][4]);
_setSelected($REQUESTA_FREE_CATALOG_STATE_TEXTBOX,$drop6_data[8][4]);
_setValue($REQUESTA_FREE_CATALOG_EMAIL_TEXTBOX,$drop6_data[9][4]);
_setValue($REQUESTA_FREE_CATALOG_CONFIRMEMAIL_TEXTBOX,$drop6_data[9][4]);
_setValue($REQUESTA_FREE_CATALOG_PHONE_TEXTBOX,$drop6_data[10][4]);
_setSelected($REQUESTA_FREE_CATALOG_SPORTACTIVITY_TEXTBOX,$drop6_data[11][4]);
_setSelected($REQUESTA_FREE_CATALOG_QUANTITY_TEXTBOX,"0");
_check($REQUESTA_FREE_CATALOG_REQUEST_PATHES_CHECKBOX);
_click($REQUESTA_FREE_CATALOG_SUBMIT_BUTTON);

//verifying the submit request page
_assertVisible($HOME_CUSTOMERSERVICE_BREADCRUMB,_in($PAGE_BREADCRUMB)));
_assertVisible($REQUESTA_FREE_CATALOG_HEADING);
_assertVisible($REQUESTA_FREE_CATALOG_RIGHT_IMAGE);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("304778","Verify the functionality of continue shopping button in testimonials page for desktop/tablet");
$t.start();
try
{
	
//static data
_click($FOOTER_TESTIMONIALS_LINK)
//click on continue shopping link
_assertVisible($FOOTER_TESTIMONIALS_CONTINUESHOPPING_LINK);
_click($FOOTER_TESTIMONIALS_CONTINUESHOPPING_LINK);
//verify the home page
_assertNotVisible($PAGE_BREADCRUMB);
_assertVisible($HOMEPAGE_CONTENT_SLOT);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("304774","Verify the functionality of 'CONTINUE SHOPPING' button displayed in the FAQ page for desktop/tablet/Verify the functionality of 'CONTINUE SHOPPING' button displayed in the FAQ page for mobile");
$t.start();
try
{

//click on faq link in footer 	
_assertVisible($FOOTER_FAQ_LINK);
_click($FOOTER_FAQ_LINK);
//verifying FAQ link
_assertVisible($HOME_FAQ_BREADCRUMB,_in($PAGE_BREADCRUMB));
_assertVisible($FAQ_HEADING);
//click on continue shopping link
_click($FOOTER_FAQ_CONTINUESHOPPING_LINK);
//verify the home page
_assertNotVisible($PAGE_BREADCRUMB);
_assertVisible($HOMEPAGE_CONTENT_SLOT);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

/*var $t = _testcase(" "," ");
$t.start();
try
{
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/