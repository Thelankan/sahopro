_include("../GenericLibrary/GlobalFunctions.js");
_resource("SanityData.xls");

//ClearCookies();
SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

var $currentmonth;
//Ashley Phillips 817-478-1802 CrownTrophiesE@gmail.com VERSANTE DRIVE 6287 RENWOOD DR FORT WORTH, TX 76140-9403 United States
var $shippingaddress=$checkoutData[0][1]+" "+$checkoutData[0][2]+" "+$checkoutData[0][12]+" "+$uId+" "+$checkoutData[0][4]+" "+$checkoutData[0][3]+" "+$checkoutData[0][7]+", "+$checkoutData[0][11]+" "+$checkoutData[0][8]+" "+$checkoutData[0][5];
//Ashley Phillips VERSANTE DRIVE 6287 RENWOOD DR FORT WORTH, TX 76140-9403 United States
var $shippingaddressComp=$checkoutData[0][1]+" "+$checkoutData[0][2]+" "+$checkoutData[0][4]+" "+$checkoutData[0][3]+" "+$checkoutData[0][7]+", "+$checkoutData[0][11]+" "+$checkoutData[0][8]+" "+$checkoutData[0][5];
//vinay lanka 12456 street aveneue WHITE PLAINS, NY 10603 United States
var $billingaddress=$checkoutData[0][1]+" "+$checkoutData[0][2]+" "+$checkoutData[0][3]+" "+$checkoutData[0][4]+" "+$checkoutData[0][7]+", "+$checkoutData[0][11]+" "+$checkoutData[0][8]+" "+$checkoutData[0][5];	
var $ShippingMethodShippingpage;

var $comparingShippingAddressFromPayment=ComparingShippingAddressFromPaymentPage(0);
var $comparingBillingAddressFromPayment=ComparingBillingAddressFromPaymentPage(1);

//################# Guest User ########################

var $t = _testcase("304790/304792","Verify if a guest user is able to Place Order Using Amex and Discover as a Payment method.");
$t.start();
try
{

for(var $i=1;$i<3;$i++)
	{
//Navigate to Payment Page
NavigateToPaymentPage();

//Payment details with visa 16 digits 
PaymentDetails($cardDetails,$i);
                    
//Placing order
_click($BILLING_PLACE_ORDER_BUTTON);

//Order confirmation verification
orderConfirmationVerification();

//Card Details
_assertContainsText($cardDetails[$i][6], $ORDER_CONFIRMATION_PAYMENT_DETAILS);
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("304791/304776","Verify the Guest User is able to Place order using Master Card as a payment type with Standard Product as part of Line item");
$t.start();
try
{

//navigate to cart page with single product 
navigateToCart($genericData[0][0],$genericData[0][1]);
navigateToCart($genericData[1][0],$genericData[0][1]);

//click mini cart link
_click($VIEWCART_LINK_IN_MINICART);
//click on edit item in cart page
_click($EDIT_ITEM_LINK_IN_CART);
//should navigate to PDP page
_assertVisible($PRODUCTNAME_IN_PDP);
//click on checkout link in mini cart
_click($MINI_CART_LINK);
//click on checkout button in cart page
_click($CHECKOUT_LINK_IN_CART_PAGE);
//should navigate to shipping page
_assertVisible($SHIPPING_ADDRESS_HEADING);
//Login optional section
_assertVisible($SHIPPING_LOGIN_OPTIONAL_SECTION);
//comparing product name in shipping page
var $pnameshippingpage=_getText($SHIPPING_ORDER_SUMMARY_PRODUCT_NAME).toString();

if($pName.indexOf($pnameshippingpage)>=0)                            
{
      _assert(true);
}

//shipping form should display
shippingAddress($checkoutData,0);

//un checking the check box to enter billing address
if ($SHIPPING_ADDRESS_SAME_AS_BILLING_CHECKBOX.checked)
	{
	_uncheck($SHIPPING_ADDRESS_SAME_AS_BILLING_CHECKBOX)
	}

//reload the page 
window.location.href("https://development-webstore-crownawards.demandware.net/s/MAI/checkout");

//enter billing address
BillingAddress($checkoutData,1);

//address proceed
adddressProceed();

//event calendar
eventcalendar();

//Selecting first shipping method
_check($SHIPPING_METHOD_ID);

//click on continue button
_click($SHIPPING_CONTINUE_BUTTON);

//address proceed
adddressProceed();

var $priceInPaymentPage=_extract(_getText($PAYMENT_PAGE_PRICE),"/[$](.*)/",true);

//pre-populate Shipping and billing details
_assertEqual($comparingShippingAddressFromPayment,_getText($PAYMENT_PAGE_SHIPPING_ADDRESS_SECTION));
_assertEqual($comparingBillingAddressFromPayment,_getText($PAYMENT_PAGE_BILLING_ADDRESS_SECTION));

//click on edit link in payment method
_click($SHIPPING_ADDRESS_EDIT_LINK);

//pre populate shipping 
prepopulateShipping($checkoutData,0);

//shipping form should display
shippingAddress($checkoutData,3);

//change the shipping address pre populate 
prepopulateShipping($checkoutData,2);

//click on continue button
_click($SHIPPING_CONTINUE_BUTTON);

//address proceed
adddressProceed();

//Comparing new shipping address
_assertEqual($comparingShippingAddressFromPayment,_getText($PAYMENT_PAGE_SHIPPING_ADDRESS_SECTION));

//enter master card payment details
PaymentDetails($cardDetails,3);

//Placing order
_click($BILLING_PLACE_ORDER_BUTTON);

//Order confirmation verification
orderConfirmationVerification();

_assertContainsText($cardDetails[3][6], $ORDER_CONFIRMATION_PAYMENT_DETAILS);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//click on checkout link in mini cart
var $t = _testcase("304789/304773","Verify the Order details in track your order  section when the order is placed using Visa card with same billing and shipping address as a guest user./Verify the Functionality of 'CHECK STATUS' button in the Email Us section-contact us page as registered user for deskto");
$t.start();
try
{

//Navigate to Payment Page
NavigateToPaymentPage();

//Payment details with visa 16 digits 
PaymentDetails($cardDetails,1);
                    
//Placing order
_click($BILLING_PLACE_ORDER_BUTTON);

//Order confirmation verification
orderConfirmationVerification();

_assertContainsText($cardDetails[1][6], $ORDER_CONFIRMATION_PAYMENT_DETAILS);

//verifying the order details
var $orderNumberInOcp=_extract(_getText($ORDER_CONFIRMATION_NUMBER),"/[#](.*)/",true);
var $billingEmailInOcp=_getText($ORDER_CONFIRMATION_BILLING_EMAIL);
var $zipcodeInOcp=_extract(_getText($ORDER_CONFIRMATION_BILLING_ADDRESS_LOCATION),"/Y(.*)U/",true);

//storing the order no from order confirmation page 
var $orderNumberInOcp=_extract(_getText($ORDER_CONFIRMATION_NUMBER),"/[#](.*)/",true).toString()
_log($orderNumberInOcp);

var $IDinocpPage=_getText($ORDER_CONFIRMATION_BILLING_ADDRESS_LOCATION).split(" ")[2];
var $billaddress=_getText($ORDER_CONFIRMATION_BILLING_ADDRESS_LOCATION);
var $shipaddress=_getText($ORDER_CONFIRMATION_SHIPPING_ADDRESS_LOCATION);
var $price=_extract(_getText($ORDER_CONFIRMATION_TOTAL_PRICE),"/: (.*)/",true);
var $shippingmethod=_getText($ORDER_CONFIRMATION_SHIPPING_METHOD);
var $orderprice=_getText($ORDER_CONFIRMATION_ORDER_TOTAL);
//card number
var $verifyingcardno=_getText($ORDER_CONFIRMATION_CARD_NUMBER);

//delivery date
var $deliverydate=_extract(_getText($ORDER_CONFIRMATION_ORDER_DATE),"/: (.*)/",true);

//############ click on your orders link in profile ##################
_click($HEADERYOURORDERSLINK);
//_assertVisible(_numberbox("searchorder"));
_setValue($ORDERSPAGE_ORDER_NUMBER_TEXTBOX,$orderNumberInOcp);
_setValue($ORDERSPAGE_BILLINGEMAIL_TEXTBOX,$billingEmailInOcp);
_setValue($ORDERSPAGE_BILLINGZIPCODE_TEXTBOX,$zipcodeInOcp);
_click($ORDERSPAGE_FIND_ORDER_BUTTON);

//###### Fetching the values ###################
var $ordernoinDetailspage=_extract(_getText($ORDER_CONFIRMATION_NUMBER),"/[#](.*)/",true);
var $emailIdinDetailspage=_getText($ORDER_CONFIRMATION_BILLING_ADDRESS_LOCATION).split(" ")[2];

//address verification 
var $billaddressinDetailspage=_getText($ORDER_CONFIRMATION_BILLING_ADDRESS_LOCATION);
var $shipaddressinDetailspage=_getText($ORDER_CONFIRMATION_SHIPPING_ADDRESS_LOCATION);

//price comparing in details page
var $priceinDetailspage=_extract(_getText($ORDER_CONFIRMATION_TOTAL_PRICE),"/: (.*)/",true);
var $orderpriceinDetailspage=_getText($ORDER_CONFIRMATION_ORDER_TOTAL);

//shipping method
var $shippingmethodinDetailspage=_getText($ORDER_CONFIRMATION_SHIPPING_METHOD);

//card number
var $verifyingcardnoinDetailpage=_extract(_getText($ORDER_CONFIRMATION_PAYMENT_DETAILS),"/: (.*)/",true);

//delivery date
var $deliverydateinDetailpage=_extract(_getText($ORDER_CONFIRMATION_ORDER_DATE),"/: (.*)/",true);

_assertEqual($orderNumberInOcp,$ordernoinDetailspage);
_assertEqual($IDinocpPage,$emailIdinDetailspage);
_assertEqual($billaddress,$billaddressinDetailspage);
_assertEqual($shipaddress,$shipaddressinDetailspage);
_assertEqual($price,$priceinDetailspage);
_assertEqual($orderprice,$orderpriceinDetailspage);
_assertEqual($shippingmethod,$shippingmethodinDetailspage);
_assertEqual($verifyingcardno,$verifyingcardnoinDetailpage);
_assertEqual($deliverydate,$deliverydateinDetailpage);

//_assert(false,"heading should verify");


//click on contact us link in footer
_click($FOOTERLINK_CONTACTUS);
//breadcrumb   
_assertVisible($HOME_CUSTOMERSERVICE_BREADCRUMB, _in($PAGE_BREADCRUMB));
//content-header
_assertVisible($CONTACTUS_HEADING);
//display email us
_assertVisible($CONTACTUS_EMAILUS_HEADING);
_assertVisible($CONTACTUS_SUBMIT_BUTTON);

//entering valid values in all the fields
_setValue($CONTACTUS_EMAIL_TEXTBOX,$checkoutData[0][10]);
_setValue($CONTACTUS_FIRSTNAME_TEXTBOX,$checkoutData[0][1]);
_setValue($CONTACTUS_LASTNAME_TEXTBOX,$checkoutData[0][2]);
_setValue($CONTACTUS_PHONE_TEXTBOX,$checkoutData[0][9]);
_setSelected($CONTACTUS_YOURISSUES_DROPDOWN,$genericData[0][3]);

//verify the check status button
_assertVisible($CONTACTUS_EMAILUS_BUTTON);


_setValue($CONTACTUS_ORDERNUMBER_TEXTBOX,$orderNumberInOcp);
_setValue($CONTACTUS_BILLINGZIPCODE_TEXTBOX,$checkoutData[0][8]);
_setValue($CONTACTUS_BILLINGEMAIL_TEXTBOX,$checkoutData[0][10]);

//click on check status button
_click($CONTACTUS_CHECKEMAIL_STATUS_BUTTON);

//verification in order details page
_assertVisible($ORDER_DETAIL_HEADING);
_assertVisible($PRINT_PAGE_BUTTON);
_assertVisible($EMAIL_PAGE_LINK);

takescreenshot();

//storing the order no from order confirmation page 
var $ordernoinDetailspage=_extract(_getText($ORDER_CONFIRMATION_NUMBER),"/[#](.*)/",true);
var $emailIdinDetailspage=_getText($ORDER_CONFIRMATION_BILLING_ADDRESS_LOCATION).split(" ")[2];

//address verification 
var $billaddressinDetailspage=_getText($ORDER_CONFIRMATION_BILLING_ADDRESS_LOCATION);
var $shipaddressinDetailspage=_getText($ORDER_CONFIRMATION_SHIPPING_ADDRESS_LOCATION);

//price comparing in details page
var $priceinDetailspage=_extract(_getText($ORDER_CONFIRMATION_TOTAL_PRICE),"/: (.*)/",true);
var $orderpriceinDetailspage=_getText($ORDER_CONFIRMATION_ORDER_TOTAL);

//shipping method
var $shippingmethodinDetailspage=_getText($ORDER_CONFIRMATION_SHIPPING_METHOD);

//card number
var $verifyingcardnoinDetailpage=_extract(_getText($ORDER_CONFIRMATION_PAYMENT_DETAILS),"/: (.*)/",true);

//delivery date
var $deliverydateinDetailpage=_extract(_getText($ORDER_CONFIRMATION_ORDER_DATE),"/: (.*)/",true);

_assertEqual($orderNumberInOcp,$ordernoinDetailspage);
_assertEqual($IDinocpPage,$emailIdinDetailspage);
_assertEqual($billaddress,$billaddressinDetailspage);
_assertEqual($shipaddress,$shipaddressinDetailspage);
_assertEqual($price,$priceinDetailspage);
_assertEqual($orderprice,$orderpriceinDetailspage);
_assertEqual($shippingmethod,$shippingmethodinDetailspage);
_assertEqual($verifyingcardno,$verifyingcardnoinDetailpage);
_assertEqual($deliverydate,$deliverydateinDetailpage);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("304780","Verify the functionality of 'EDIT' link in Cart summary Section in right nav on payment  page in Application as a Guest user.");
$t.start();
try
{

//Navigate to Payment Page
NavigateToPaymentPage();

//product name in payment page
var $productNamein_PaymentPage=_getText($PAYMENT_PAGE_PRODUCT_NAME);
var $lineItempriceInPaymentPage=LineItemPriceInShipping();
_assertEqual($priceLineItemsInCart.sort(),$lineItempriceInPaymentPage.sort());

//click on edit cart link in order summary section in payment page
_assertVisible($EDITCARTLINKIN_PAYMENTPAGE);
_click($EDITCARTLINKIN_PAYMENTPAGE);

//application should navigate to cart page
_assertVisible($CART_PRODUCT_ROW);

//Product Name should be same
if($productNamein_PaymentPage.indexOf($pName)>=0)                         
{
      _assert(true);
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("304812","Verify the functionality of remove product link on shipping page.");
$t.start();
try
{

//Navigate to shipping page
_click($CHECKOUT_BUTTON_BESIDE_MINI_CART);
//click on product remove button 
_click($SHIPPING_ORDER_SUMMARY_PRODUCT_REMOVE_LINK);
//application should re-direct to cart page
_assertVisible($CART_PRODUCT_ROW);
//Product count in cart page
var $productCountInCartPage=ProductCountInCart();

//after deleting product in cart page
if ($productCountInCartPage==1)
	{
	//click on remove link in cart page
	_click($REMOVE_LINK_INCARTPAGE);
    _assertNotVisible($CART_PRODUCT_TABEL);
    //empty cart message 
    _assertVisible($CART_YOUR_CART_IS_EMPTY_MESSAGE);
	}
	else
		{
		//click on remove link in cart page
		_click($REMOVE_LINK_INCARTPAGE);
		//products in cart
		var $productCountInCartPageAfterDeletion=ProductCountInCart();
	    //items should display in cart
	    _asserVisible($CART_PRODUCT_TABEL);
	    //product 
	    _assertEqual($productCountInCartPageAfterDeletion,$productCountInCartPage-1);
		}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("304813","Verify the functionality of remove product link on payment page.");
$t.start();
try
{
	
//Navigate to Payment Page
NavigateToPaymentPage();

//click on remove link in payment page
_click($PAYMENT_PAGE_PRODUCT_REMOVE_LINK);

//product name in payment page
var $lineItempriceInPaymentPage=LineItemPriceInShipping();
_assertEqual($priceLineItemsInCart.sort(),$lineItempriceInPaymentPage.sort());

//application should re-direct to cart page
_assertVisible($CART_PRODUCT_ROW);
//Product count in cart page
var $productCountInCartPage=ProductCountInCart();

//after deleting product in cart page
if ($productCountInCartPage==1)
	{
	//click on remove link in cart page
	_click($REMOVE_LINK_INCARTPAGE);
    //items should not display in cart
    _assertNotVisible($CART_PRODUCT_TABEL);
    //empty cart message 
    _assertVisible($CART_YOUR_CART_IS_EMPTY_MESSAGE);
	}
else
	{
	//click on remove link in cart page
	_click($REMOVE_LINK_INCARTPAGE);
	//products in cart
	var $productCountInCartPageAfterDeletion=ProductCountInCart();
    //items should display in cart
    _assertNotVisible($CART_PRODUCT_TABEL);
    //product 
    _assertEqual($productCountInCartPageAfterDeletion,$productCountInCartPage-1);
	}


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//********* Register User ************

var $t = _testcase("304802","Verify if the user is able to Place order using discover card as a registered user");
$t.start();
try
{

//click on creare account link
 _click($LOGINLINK);
 //Visibility of overlay
 _assertVisible($CREATE_ACCOUNT_DIALOGUE);

//click on create account button
_click($CREATELOGIN_BUTTON);
	
//Create account for the application
createAccount_profile($genericData,6);

//Logout link should be visible
_assertVisible($LOGOUTLINK);

//navigate to cart page with single 
navigateToCart($genericData[0][0],$genericData[0][1]);
//click on checkout link in mini cart
_mouseOver($MINICART_LINK);
_assertVisible($MINICART_LINK_RIGHTTOP_CHECKOUT_LINK);
_click($MINICART_LINK_RIGHTTOP_CHECKOUT_LINK);
//should navigate to shipping page
_assertVisible($SHIPPING_ADDRESS_HEADING);
//Login optional section
_assertVisible($SHIPPING_LOGIN_OPTIONAL_SECTION);

//comparing product name in shipping page
var $pnameshippingpage=_getText($SHIPPING_ORDER_SUMMARY_PRODUCT_NAME).toString();

if($pName.indexOf($pnameshippingpage)>=0)                            
{
      _assert(true);
}

//shipping form should display
shippingAddress($checkoutData,0);

//un checking the check box to enter billing address
if ($SHIPPING_ADDRESS_SAME_AS_BILLING_CHECKBOX.checked)
	{
	_uncheck($SHIPPING_ADDRESS_SAME_AS_BILLING_CHECKBOX)
	}

//reload the page 
window.location.href("https://development-webstore-crownawards.demandware.net/s/MAI/checkout");

//enter billing address
BillingAddress($checkoutData,1);

//address proceed
adddressProceed();

//event calendar
eventcalendar();

//click on save shipping address check box
_check($SHIPPING_ADDTOADDRESSBOOK_CHECKBOX);
//Selecting first shipping method
_check($SHIPPING_METHOD_ID);

_wait(4000);
//address proceed
adddressProceed();

//click on continue button
_click($SHIPPING_CONTINUE_BUTTON);

//address proceed
adddressProceed();

var $priceInPaymentPage=_extract(_getText($PAYMENT_PAGE_PRICE),"/[$](.*)/",true);

//enter master card payment details
PaymentDetails($cardDetails,2);

//Placing order
_click($BILLING_PLACE_ORDER_BUTTON);

//Order confirmation verification
orderConfirmationVerification();

_assertContainsText($cardDetails[2][6], $ORDER_CONFIRMATION_PAYMENT_DETAILS);

//navigate to address page in profile
NavigatetoAccountLanding();

//click on address link
_click($ADDRESSESLINK);

//comparing saved address in my account landing page
_assertEqual(ComparingAddressFromAccountLandingPage(0),_getText($ORDER_CONFIRMATION_BILLING_ADDRESS_LOCATION));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("304801","Verify if the registered user is able to Place Order with 16 digit visa credit card.");
$t.start();
try
{

//add new address
//click on add new address button
 _click($ADDNEW_ADDRESS_LINK);
//address form should display
_assertVisible($ADDRESS_FORMFIELDS);
//entering values in address form
addAddress($address,0);
//navigate to cart page with single 
navigateToCart($genericData[0][0],$genericData[0][1]);
//click on checkout link in mini cart
_mouseOver($MINICART_LINK);
_assertVisible($MINICART_LINK_RIGHTTOP_CHECKOUT_LINK);
_click($MINICART_LINK_RIGHTTOP_CHECKOUT_LINK);
//should navigate to shipping page
_assertVisible($SHIPPING_ADDRESS_HEADING);
//Login optional section
_assertVisible($SHIPPING_LOGIN_OPTIONAL_SECTION);

//comparing product name in shipping page
var $pnameshippingpage=_getText($SHIPPING_ORDER_SUMMARY_PRODUCT_NAME).toString();

if($pName.indexOf($pnameshippingpage)>=0)                            
{
      _assert(true);
}
//selected newly saved address
_setSelected($SHIPPING_ADDRESS_DROPDOWN, 2);

//comparing shipping values
prepopulateShipping($checkoutData,3);

//un checking the check box to enter billing address
if ($SHIPPING_ADDRESS_SAME_AS_BILLING_CHECKBOX.checked)
	{
	_uncheck($SHIPPING_ADDRESS_SAME_AS_BILLING_CHECKBOX)
	}

//reload the page 
window.location.href("https://development-webstore-crownawards.demandware.net/s/MAI/checkout");

//address proceed
adddressProceed();

_check($SHIPPING_METHOD_ID);
_wait(4000);

//click on continue button
_click($SHIPPING_CONTINUE_BUTTON);

//address proceed
adddressProceed();

var $priceInPaymentPage=_extract(_getText($PAYMENT_PAGE_PRICE),"/[$](.*)/",true);

//enter master card payment details
PaymentDetails($cardDetails,0);

//Placing order
_click($BILLING_PLACE_ORDER_BUTTON);

//Order confirmation verification
orderConfirmationVerification();

_assertContainsText($cardDetails[0][6], $ORDER_CONFIRMATION_PAYMENT_DETAILS);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("304803","Verify the registered user can place order using Master card.");
$t.start();
try
{
	
//Navigate to Payment Page
NavigateToPaymentPage();

//Payment details with visa 16 digits 
PaymentDetails($cardDetails,3);
                    
//Placing order
_click($BILLING_PLACE_ORDER_BUTTON);

//Order confirmation verification
orderConfirmationVerification();
_assertContainsText($cardDetails[3][6], $ORDER_CONFIRMATION_PAYMENT_DETAILS);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("304794","Verify if the user can place an order after logging in from cart page.");
$t.start();
try
{
	
//navigate to cart page with single 
navigateToCart($genericData[0][0],$genericData[0][1]);

//click on creare account link
_click($LOGINLINK);
//Visibility of overlay
_assertVisible($CREATE_ACCOUNT_DIALOGUE);

//Login to the application
login();

//Navigate to shipping page
//click on checkout link in mini cart
_mouseOver($MINICART_LINK);
_assertVisible($MINICART_LINK_RIGHTTOP_CHECKOUT_LINK);
_click($MINICART_LINK_RIGHTTOP_CHECKOUT_LINK);
//should navigate to shipping page
_assertVisible($SHIPPING_ADDRESS_HEADING);
//Login optional section
_assertVisible($SHIPPING_LOGIN_OPTIONAL_SECTION);

//comparing product name in shipping page
var $pnameshippingpage=_getText($SHIPPING_ORDER_SUMMARY_PRODUCT_NAME).toString();

if($pName.indexOf($pnameshippingpage)>=0)                            
{
      _assert(true);
}

//to check whether to enter the shipping address or not 
var $boolean=_assertContainsText("@",$SHIPPING_EMAIL_TEXTBOX);

if (!$boolean=="true")
	{
	//shipping form should display
	shippingAddress($checkoutData,0);

	//un checking the check box to enter billing address
	if ($SHIPPING_ADDRESS_SAME_AS_BILLING_CHECKBOX.checked)
		{
		_uncheck($SHIPPING_ADDRESS_SAME_AS_BILLING_CHECKBOX)
		}

	//reload the page 
	window.location.href("https://development-webstore-crownawards.demandware.net/s/MAI/checkout");

	//enter billing address
	BillingAddress($checkoutData,1);
	}


//address proceed
adddressProceed();

//event calendar
eventcalendar();

//Selecting first shipping method
_check($SHIPPING_METHOD_ID);

//address proceed
adddressProceed();

//Payment details with visa 16 digits 
PaymentDetails($cardDetails,3);
                    
//Placing order
_click($BILLING_PLACE_ORDER_BUTTON);

//Order confirmation verification
orderConfirmationVerification();
_assertContainsText($cardDetails[3][6], $ORDER_CONFIRMATION_PAYMENT_DETAILS);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//logout from the application
logout();

var $t = _testcase("304793","Verify if user is able to Place order after creating an account in cart  Page.");
$t.start();
try
{
	
//navigate to cart page with single 
navigateToCart($genericData[0][0],$genericData[0][1]);

//click on creare account link
_click($LOGINLINK);
//Visibility of overlay
_assertVisible($CREATE_ACCOUNT_DIALOGUE);

//Create account from the cart page
createAccount_profile($genericData,6);

//application should re-direct to cart page only
_assertVisible($CART_PRODUCT_ROW);

//fetching first name from the header
var $firstName=_extract(_getText($HEADERHITEXT),"/Hi (.*)/",true);

//assert Equal
_assertEqual($genericData[6][3],$firstName);

//click on checkout link in mini cart
_mouseOver($MINICART_LINK);
_assertVisible($MINICART_LINK_RIGHTTOP_CHECKOUT_LINK);
_click($MINICART_LINK_RIGHTTOP_CHECKOUT_LINK);
//should navigate to shipping page
_assertVisible($SHIPPING_ADDRESS_HEADING);
//Login optional section
_assertVisible($SHIPPING_LOGIN_OPTIONAL_SECTION);

//comparing product name in shipping page
var $pnameshippingpage=_getText($SHIPPING_ORDER_SUMMARY_PRODUCT_NAME).toString();

//shipping form should display
shippingAddress($checkoutData,0);

//un checking the check box to enter billing address
if ($SHIPPING_ADDRESS_SAME_AS_BILLING_CHECKBOX.checked)
	{
	_uncheck($SHIPPING_ADDRESS_SAME_AS_BILLING_CHECKBOX)
	}

//reload the page 
window.location.href("https://development-webstore-crownawards.demandware.net/s/MAI/checkout");

//enter billing address
BillingAddress($checkoutData,1);

//address proceed
adddressProceed();

//event calendar
eventcalendar();

//address proceed
adddressProceed();

//Payment details with visa 16 digits 
PaymentDetails($cardDetails,1);
//Placing order
_click($BILLING_PLACE_ORDER_BUTTON);

//Order confirmation verification
orderConfirmationVerification();
_assertContainsText($cardDetails[1][6], $ORDER_CONFIRMATION_PAYMENT_DETAILS);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("304779","Verify the functionality of 'EDIT' link in Cart summary Section in right nav on shipping page in Application as a Registered user.");
$t.start();
try
{
	
//navigate to cart page with single 
navigateToCart($genericData[0][0],$genericData[0][1]);

var $lineitempriceinshipping=LineItemPriceInShipping();

//click on checkout link
_click($MINICART_LINK_RIGHTTOP_CHECKOUT_LINK);
//should navigate to shipping page
_assertVisible($SHIPPING_ADDRESS_HEADING);
//click on edit link in cart summary in shipping page
_click($SHIPPING_EDIT_CART_LINK);
//tempcode needs to be deleted
_expectConfirm("/Are you sure you want to leave this page/", true);
//application should re-direct to cart page only
_assertVisible($CART_PRODUCT_ROW);

var $productnameincart=_extract(_getText($PRODUCT_NAME_IN_CART),"/(.*) -/",true);
var $lineitempriceincart=LineItemPriceInCart();

//_assertEqual($productnameincartsummary,$productnameincart);
_assertEqualArrays($lineitempriceincart.sort(),$lineitempriceinshipping.sort());

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase(" "," ");
$t.start();
try
{
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/