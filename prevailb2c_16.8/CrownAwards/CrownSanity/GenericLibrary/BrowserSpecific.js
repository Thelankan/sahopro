var $userAgent;
_set($userAgent, window.navigator.userAgent);

_resource("User_Credentials.xls");
var $User=_readExcelFile("User_Credentials.xls","User");
var $userData=_readExcelFile("User_Credentials.xls","UserAccount");
//var $UserName=_readExcelFile("User_Credentials.xls","Register");

var mobile = {
                                
      Android: function() {
          return  $userAgent.match(/Android/i);
      },
      iPhone: function() {
          return  $userAgent.match(/iPhone/i);
      },
      iPad: function() {
          return   $userAgent.match(/iPad/i);
      },
      any: function() {
          if(mobile.Android() || mobile.iPhone() || mobile.iPad())
          {
            return true;
          }
          else
          {
          return false;
          }
      },
      device: function() {
          return (mobile.Android() || mobile.iPhone() || mobile.iPad()||"desktop");
      }
  }

function isMobile()
{
                return mobile.any();
}
function getDevice()
{
                return mobile.device();
}
function getBrowser(){

   var $N, $ua, $tem, $appVersion;
                 
    _set($N, window.navigator.appName);
    
    _set($ua, window.navigator.userAgent);
    
                _set($appVersion, window.navigator.appVersion);
   
    
    var $M= $ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i); //firefox
    if($M && ($tem= $ua.match(/version\/([\.\d]+)/i))!= null) $M[2]= $tem[1];
    $M=$M? [$M[1], $M[2]]: [$N, $appVersion, '-?'];
           
    return $M[0];
    }
function getBrowserVersion(){

                var $N, $ua, $tem, $appVersion;
                
    _set($N, window.navigator.appName);
    
    _set($ua, window.navigator.userAgent);
    
                _set($appVersion, window.navigator.appVersion);
                
    var $M=$ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i); //firefox
    if($M && ($tem= $ua.match(/version\/([\.\d]+)/i))!= null) $M[2]= $tem[1];
    $M=$M? [$M[1], $M[2]]: [$N, $appVersion, '-?'];
    var version=$M[1];
    versionOffset=version.indexOf('.');
     return version.substring(0,versionOffset);
    }
function getOS()
{
                
                var $appVersionWin7;
                _set($appVersionWin7, window.navigator.appVersion.indexOf("Windows NT 6.1"));
                
                var $appVersionWin8;
                _set($appVersionWin8, window.navigator.appVersion.indexOf("Windows NT 6.3"));
                
                var $appVersionWin10;
                _set($appVersionWin10, window.navigator.appVersion.indexOf("Windows NT 10.0"));
                
                var $appVersionMac;
                _set($appVersionMac, window.navigator.appVersion.indexOf("Mac"));
                
                if($appVersionWin7==-1 && $appVersionWin8==-1 && $appVersionWin10==-1  && $appVersionMac==-1)
                	{
                	 return "Windows7";
                	}
                
                if ($appVersionWin7!=-1)
                {
                                return "Windows7";
                }
                if ($appVersionWin8!=-1)
                {
                                return "Windows8";
                }
                if ($appVersionWin10!=-1)
                {
                                return "Windows10";
                }
                if ($appVersionMac!=-1)
                {
                                return "Mac";
                }
}

function isIE11()
{
       var $testIE11;
       _set($testIE11, window.navigator.userAgent);
       _log($testIE11);
       if(!!$testIE11.match(/Trident.*rv[ :]*11\./))
              {
                     return true;
              }
       else
              {
                     return false;
              }
       
}


var $GC;
var $user;
function data()
{
                
var $mobile1=isMobile();
var $device1=getDevice();
_log("isMobile"+$mobile1);
_log("$device1"+$device1);
	if (isMobile())
       {
            if(getDevice().indexOf('Android')>-1)
	            {
	               $user=0;
	            }
            else if(getDevice().indexOf('iPad')>-1)
	            {
	            	$user=1;
	            }
            else if(getDevice().indexOf('iPhone')>-1)
	            {
	            	$user=2;
	            }
        }
    else                                
        {

            if (getOS() == "Windows7")
            {
	               var $browser = getBrowser();                                               
		            if ($browser == "Firefox")
			            {
		            		$user=3;
		                }
	                else if ($browser == "Chrome")
		                {
	                		$user=4;
		                }
	                else if ($browser == "Safari")
		                {
	                		$user=5;
		                }
	                else if ($browser == "MSIE")
	                { 
	                   var $browserVersion = getBrowserVersion();                                                                    
	                   if ($browserVersion == '9')
		                    {
	                	   		$user=6;
		                    }
	                   else if ($browserVersion == '10')
		                    {
	                	   		$user=7;
		                    }
	                }
	                else if(isIE11())
		                {
	                		$user=8;
		                }
              }
            else if (getOS() == "Mac")
                {
            		$user=9;
	            }
            else if (getOS() == "Windows8")
            	{
              if ($browser == "MSIE")
                { 
                   var $browserVersion = getBrowserVersion();                                                                    
                   if ($browserVersion == '9')
	                    {
                	   		$user=10;
	                    }
                   else if ($browserVersion == '10')
	                    {
                	   		$user=11;
	                    }
                }
                else if(isIE11())
	                {
                		$user=12;
	                }
            	}
            else if (getOS() == "Windows10")
            	{
            	
                if ($browser == "MSIE")
                { 
                   var $browserVersion = getBrowserVersion();                                                                    
                    if ($browserVersion == '10')
	                    {
                	   		$user=13;
	                    }
                }
            	
                else if(isIE11())
	                {
                		$user=14;
	                }
            	}
        }
}
data();

var $uId=$User[$user][3];
var $pwd=$User[$user][4];
var $FName=$User[$user][1];
var $LName=$User[$user][2];
var $email=$userData[$user][3];

/*
for(var $i=0;$i<$User.length;$i++)
	{
	 //Create account
	if (isMobile())
	{
	 _click(_link("/mobile-show/", _in(_div("user-info"))));
	}
	else
		{
		 _click(_link("/desktop-show/", _in(_div("user-info"))));
		 //Visibility of overlay
		 _assertVisible(_div("dialog-container"));
		}
	 _click(_link("button", _in(_div("/login-box/"))));	
	 
	 _setValue(_textbox("dwfrm_profile_customer_firstname"), $User[$user][1]);
	 _setValue(_textbox("dwfrm_profile_customer_lastname"), $User[$user][2]);
	 _setValue(_textbox("dwfrm_profile_customer_email"), $User[$user][3]);
	 _setValue(_textbox("dwfrm_profile_customer_emailconfirm"), $User[$user][3]);
	 _setValue(_password("/dwfrm_profile_login_password/"), $User[$user][4]);
	 _setValue(_password("/dwfrm_profile_login_passwordconfirm/"), $User[$user][4]);
	 _click(_submit("dwfrm_profile_confirm"));
	 
	 //should navigate to my account landing page
	 _assertVisible(_div("account-page"));
	 _assertVisible(_link("My Account", _in(_div("breadcrumb"))));
	 
	 _click(_link("/user-logout/"))
	
	}*/