_include("BrowserSpecific.js");
_include("BM_Functions.js");
_include("../ObjectRepository/CrownAwards_OR.sah");
_resource("User_Credentials.xls");

//generic email
var $date=Date().split(" ")[4];
var $dateNow=$date.replace(":","").replace(":","");
var $datenowEmail="generic"+"_"+$dateNow+"@gmail.com";

//product name in shipping page
var $pnameshippingpage;
var $priceInShippingBillingPage;
var $priceInPaymentPage;
var $pnameinocppage;
var $priceinOCP;
var $ordertotalOCP;
var $priceLineItemsInCart;

//********************** Generic Global Functions ****************************
function SiteURLs()
{
	_navigateTo($URL[0][0]);
}

function NavigatetoHomepage()
{
	 _click($CROWNAWARDS_HOMEPAGE_LINK);
}

function takescreenshot()
{
	  _lockWindow(5000);
	  _focusWindow();
	  _takePageScreenShot();
	  _unlockWindow();	
}

//Function take a screen shot when a their is a script failure

function onScriptError()
{
  _log("Error log"); 
  _lockWindow(5000);
  _focusWindow();
  _takePageScreenShot();
  _unlockWindow();
}

function onScriptFailure()
{
  _log("Failure log");
  _lockWindow(5000);
  _focusWindow();
  _takePageScreenShot();
  _unlockWindow();
}

function cleanup()
{
	//Clearing the items from cart
	ClearCartItems();
	logout();
	NavigatetoHomepage();
}

//********************** Profile Global Functions ****************************
function login()
{
	 
_setValue($LOGIN_USERNAME_TEXTBOX, $uId);
_setValue($LOGIN_PASSWORD_TEXTBOX, $pwd);

   if (_isVisible($LOGIN_BUTTON))
   {
     _click($LOGIN_BUTTON);
   }
	  else
	   {
	       _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	   }
}

function login($userName,$password)
{
	 
_setValue($LOGIN_USERNAME_TEXTBOX,$userName);
_setValue($LOGIN_PASSWORD_TEXTBOX,$password);

   if (_isVisible($LOGIN_BUTTON))
   {
     _click($LOGIN_BUTTON);
   }
	  else
	   {
	       _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	   }
}

function logout()
{
	
//click on logout link in header
if (_isVisible($LOGOUTLINK))
	{
	_click($LOGOUTLINK);
	}
//log out link should not be visible
_assertNotVisible($LOGOUTLINK);

}

function NavigatetoAccountLanding()
{
	_navigateTo("https://development-webstore-crownawards.demandware.net/on/demandware.store/Sites-MAI-Site/default/Account-Show");
}

function createAccount_profile($sheet,$Rowno)
{
	 _setValue($CREATEACCOUNT_EMAIL_TEXTBOX,$datenowEmail);
	 _setValue($CREATEACCOUNT_EMAIL_CONFIRM_TEXTBOX,$datenowEmail);
	 _setValue($CREATEACCOUNT_FIRSTNAME_TEXTBOX, $sheet[$Rowno][3]);
	 _setValue($CREATEACCOUNT_LASTNAME_TEXTBOX, $sheet[$Rowno][4]);
	 _setValue($CREATEACCOUNT_PASSWORD_TEXTBOX, $sheet[$Rowno][5]);
	 _setValue($CREATEACCOUNT_PASSWORD_CONFIRM_TEXTBOX, $sheet[$Rowno][6]);
	 //click on submit button
	 _click($CREATEACCOUNT_SUBMIT_BUTTON);
	 _wait(5000);
}

function createAccount()
{
	 _setValue($CREATEACCOUNT_FIRSTNAME_TEXTBOX, $userData[$user][1]);
	 _setValue($CREATEACCOUNT_LASTNAME_TEXTBOX, $userData[$user][2]);
	 _setValue($CREATEACCOUNT_EMAIL_TEXTBOX, $userData[$user][3]);
	 _setValue($CREATEACCOUNT_EMAIL_CONFIRM_TEXTBOX, $userData[$user][4]);
	 _setValue($CREATEACCOUNT_PASSWORD_TEXTBOX, $userData[$user][5]);
	 _setValue($CREATEACCOUNT_PASSWORD_CONFIRM_TEXTBOX, $userData[$user][6]);
	 _click($CREATEACCOUNT_SUBMIT_BUTTON);
}

var $UserID=$userLog[0][0];
var $Password=$userLog[0][1];

function addAddress($sheet,$r)
{
	 _setValue($ADDRESS_EMAIL_TEXTBOX,$sheet[$r][1]);
	 _setValue($ADDRESS_EMAIL_CONFIRM_TEXTBOX,$sheet[$r][2]);
	 _setValue($ADDRESS_FIRSTNAME_TEXTBOX, $sheet[$r][3]);
	 _setValue($ADDRESS_LASTNAME_TEXTBOX, $sheet[$r][4]);
	 _setValue($ADDRESS_ADDRESS1_TEXTBOX, $sheet[$r][5]);
	 //_setValue($ADDRESS_ADDRESS2_TEXTBOX, $sheet[$r][6]);
	 _setValue($ADDRESS_POSTAL_TEXTBOX, $sheet[$r][10]);
	 _setSelected($ADDRESS_COUNTRY_TEXTBOX,$sheet[$r][7]);
	 _setSelected($ADDRESS_STATE_TEXTBOX, $sheet[$r][8]);
	 _setValue($ADDRESS_CITY_TEXTBOX, $sheet[$r][9]);
	 _setValue($ADDRESS_PHONE_TEXTBOX,$sheet[$r][11]);

}

function CreateCreditCard($sheet,$rowno)
{
	
	_setValue($PAYMENT_NEW_CREDITCARD_TEXTBOX,$sheet[$rowno][1]);
	_setSelected($PAYMENT_EXPIRATION_MONTH_DROPDOWN,$sheet[$rowno][2]);
	_setSelected($PAYMENT_EXPIRATION_YEAR_DROPDOWN,$sheet[$rowno][3]);
	
}

function DeleteCreditCard()
{
               
	//click on delete card link
	_click($HEADERYOURORDERSLINK);
	_click($MYACCOUNTLINK_BREADCRUMB);
	_click($PAYMENT_LINK);
	_wait(3000);	  

          if(_isVisible($PAYMENTLIST))
			{
        	    var $Totalno_Payment =PaymentListCount();
        		 _log("TOTAL ADDRESS ="+$Totalno_Payment);
        	  
        	   //deleting the existing payment methods
				for(var $i=0; $i<$Totalno_Payment; $i++)
						{
							//click on delete link
							_click($PAYMENT_DELETE_BUTTON);
							_assertVisible($REDFONT_SECTION_IN_PAYMENTLIST);
							//click on delete link
							_click($PAYMENT_DELETE_BUTTON);
							
							if(_isVisible($PAYMENTDELETE_CONFIRM_POPUP))
							{
								_click($PAYMENT_DELETE_BUTTON);
								_wait(3000);
							}
				
			             }
			}
			else
			{
				_assertNotVisible($PAYMENTLIST)
				_assertNotVisible($PAYMENT_DELETE_BUTTON);
			}
}

//********************** ShopNav Global Functions ****************************

function navigateToPdp($productid)
{
	_setValue($SEARCH_TEXTBOX, $productid);
	//Click on search icon
	_click($SEARCH_BUTTON);
	
	//selecting product if it navigates to search result page
	if (_isVisible($SEARCHRESULT_CONTENT)))
		{
		_click($PRODUCTNAME_IN_SEARCHRESULT_CONTENT)
		}

	//_click(_link("name-link", _in(_list("search-result-items"))));
}


function selectSwatch()
{
	 
	if (!isMobile())
	 {
	 //$checkbox=_collectAttributes("_div","box itemnum","productid",_in(_div("/ItemTableWrap/")));
	 $checkbox=SwatchsCount();

	 _wait(2000);
	_check(_checkbox("/prdSelect"+$checkbox[0]+"/"));

	//_assertVisible(_div("/prodwrapper mobile-hide/"));
	_assertVisible($PDP_PRODUCTWRAPPER);

	_wait(4000);
	//var $selections=_count("_div","/prodStepText/",_in($PDP_PRODUCTWRAPPER))-1;
	var $selections=0;
	var $j=1;
	for(var $i=0;$i<20;$i++)
		{
			if(_isVisible(_div("prodStepText["+$i+"]")))
			{
				$selections++;
				$j++;
			}
			else
				{
				break;
				}
		}
	_log($selections);
	_log($j);
	//var $selections=_count("_div", "/prodStepText/", _in(_div("/prodwrapper mobile-hide/")))-1;


	var $dropdown=0;
	var $flexdetails=0;
	var $firstFlexValue;

	for (var $i=0;$i<$selections;$i++)
	{
		
		if (_isVisible(_select("flexdetails["+$i+"]")))
			{
				_setSelected(_select("flexdetails["+$i+"]"),4);
				$firstFlexValue=_getSelectedText($PDP_FLEXDETAILS_DROPDOWN);
				
				$dropdown++;
				_wait(3000);
			}
		else if (_isVisible(_radio("/radio/",_in(_div("flexdetails mobile-hide["+$flexdetails+"]",_in(_div("product-content")))))))
			{
			var $radio=_collectAttributes("_radio","/radio/","id",_in(_div("flexdetails mobile-hide["+$flexdetails+"]")));
			_click(_radio($radio[0],_in(_div("flexdetails mobile-hide["+$flexdetails+"]"))));
			$flexdetails++;
			}
		else if (_isVisible(_checkbox("/check/",_in(_div("flexdetails mobile-hide["+$flexdetails+"]",_in(_div("product-content")))))))
			{
			var $checkbox=_collectAttributes("_checkbox","/check/","id",_in(_div("flexdetails mobile-hide["+$flexdetails+"]")));
			_check(_checkbox($checkbox[0],_in(_div("flexdetails mobile-hide["+$flexdetails+"]"))));
			$flexdetails++;
			}
		
		if (_isVisible($IMAGE_UPLOAD_POPUP))
		{
		_click($UPLOAD_POPUP_CLOSE_BUTTON,_near($IMAGE_UPLOAD_POPUP))
		}
				
		while (_isVisible($IMAGE_UPLOAD_POPUP_DIALOGUE))
		{
		  _click($UPLOAD_POPUP_CLOSE_BUTTON,_near($IMAGE_UPLOAD_POPUP_DIALOGUE));
		}

	}

	$totalpriceinPDP=_extract(_getText($PRICE_IN_PDP),"/[$](.*)/",true);
	 }
	 
	 else
		 {
		 
		 var $p=0;
		 
				for (var $i=0;$i<10;$i++)
				{
					
				 if (_isVisible(_div("formfield",_in(_div("prodformstyle["+$p+"]")))))
					 {
					 _setSelected(_select($i),1);
					 _wait(5000);
					 $p++;
					 }
				 else
					 {
					 break;
					 }
				 
				 	//Handling Popups
					if (_isVisible($IMAGE_UPLOAD_POPUP))
					 {
					_click($UPLOAD_POPUP_CLOSE_BUTTON,_near($IMAGE_UPLOAD_POPUP))
					 }
						
					while (_isVisible($DIALOG_CONTAINER_POPUP))
					 {
					_click($UPLOAD_POPUP_CLOSE_BUTTON,_near($DIALOG_CONTAINER_POPUP));
					 }
					
				 }
				
				$totalpriceinPDP=_extract(_getText($PRICE_IN_PDP),"/[$](.*)/",true);
			}		
			
}

function engraving()
{

if (_isVisible($ENGRAVEMENT_HEADING))

	{
	
	if ((_isVisible($ENGRAVEMENT_DROPDOWN_SECTIONONE)) || (_isVisible($ENGRAVEMENT_DROPDOWN_SECTION)))
	{
	
	var $count=EngravementDropDownCount();

var $q=1;	
for (var $i=1;$i<=$count;$i++)
	{
	
	if ($i==1)
		{
		_setSelected(_select("contentSelect_1_"+$i+""), "Ornament");
		}
		else
			{
			_setSelected(_select("contentSelect_1_"+$i+""), "Text");
			_setValue(_textbox("lineInputAdvanced["+$q+"]"),$userLog[0][3]);
			_wait(1000)
			if (_isVisible($ENGRAVEMENT_REPEAT_ALL_CHECKBOX))
			{
				_check(_checkbox("lineCheckboxAdvanced["+$q+"]"));
			}
			
			$q++;
			
			}
	}

	}
else
	{
	
	var $count=EngravementTextBoxCount();
	for (var $j=0;$j<$count;$j++)
		{
		_setValue(_textbox("lineInput["+$j+"]",_in($ENGRAVEMENT_TEXTBOX_CONTAINER)),$userLog[0][3]);
		
		if (_isVisible($ENGRAVEMENT_REPEAT_ALL_CHECKBOX))
		{
			if (!_checkbox("lineCheckbox["+$j+"]").checked)
				{
				_click(_checkbox("lineCheckbox["+$j+"]"));
				}
		}
		
		}

	}

_check($ENGRAVEMENT_CONFIRM_CHECKBOX);
_click($ADDTOCART_BUTTON_ENGRAVEMENT_PAGE);

if (_isVisible($ENGRAVEMENT_TIE_IN_POPUP))
	{
	_click($ENGRAVEMENT_TIE_IN_POPUP_CLOSE_BUTTON);
	}

	}
}

function search($product)
{

_setValue($SEARCH_TEXTBOX,$product);
_click($SEARCH_BUTTON);

}


//********************** Cart Global Functions ****************************
function ClearCartItems()
{	 
//navigate to cart
_click($MINI_CART_LINK);
//clearing cart
if (_isVisible($EMPTYCART_BUTTON_CART_PAGE))
	{
	//Empty cart button
	_click($EMPTYCART_BUTTON_CART_PAGE);
	}
//ok in modal overlay
if(_isVisible($EMPTYYOUR_CART_OVERLAY))
	{
		_click($YES_BUTTON_IN_EMPTYCART_OVERLAY);
	}
}

function addItemToCart($subCat,$quantity)
{
	
//add product to cart
search($subCat);
//selecting product if it navigates to search result page
if (_isVisible($SEARCHRESULT_CONTENT)))
	{
	_click($PRODUCTNAME_IN_SEARCHRESULT_CONTENT)
	}

//selecting swatches
selectSwatch();
$totalpriceBeforeselectingQuantity=_extract(_getText($PRICE_IN_PDP),"/[$](.*)/",true);
//selecting quantity
_setValue($QUANTITY_TEXTBOX_IN_PDP,$quantity);
_wait(5000);
$prodname=_getText($PRODUCTNAME_IN_PDP);
$prodquantity=_getText($PRODUCT_QUANTITY_IN_CART);
$totalprice=_extract(_getText($PRICE_IN_PDP),"/[$](.*)/",true);
_log($totalprice);	
//click on add to cart button
_click($ADDTOCART_ENGRAVE_IN_PDP);

while (_isVisible($PRODUCT_SOLDOUT_INFO))
	{
	_wait(3000);
	_click($ALTERNATE_PRODUCT_IMAGES_IN_POPUP,_in($PRODUCT_SOLDOUT_INFO));
	//click on add to cart button
	_click($ADDTOCART_ENGRAVE_IN_PDP);
	}

engraving();

//closing "Added item to cart" Product
if (_isVisible($ADDANOTHER_ITEM_TO_CART_POPUP))
	{
	_click($CLOSEBUTTON_IN_ADDOTHER_ITEM_POPUP);
	}

}

function navigateToCart($subCat,$i)
{
	//add item to cart
	addItemToCart($subCat,$i);
	//navigate to Cart page
	_click($VIEWCART_LINK_IN_MINICART);
	//Product Name
	$pName=_getText($PRODUCT_NAME_IN_CART);
	//Price line items
	$priceLineItemsInCart=LineItemPriceInCart();
	$quantity=_getText($CART_QUANTITY_TEXTBOX);
	$orderTotal=_extract(_getText($CART_ORDER_VALUE),"/[$](.*)/",true);
}

//********************** Checkout global Functions *************************
function navigateTOIL($excel)
{
	//navigate to cart page
	navigateToCart($excel[0][0],$excel[0][1]);
	//click on go straight to checkout to navigate to IL Page
	_click($CHECKOUT_BUTTON_BESIDE_MINI_CART);
}


function shippingAddress($sheet,$i)
{
		
	_setValue($SHIPPING_FIRSTNAME_TEXTBOX, $sheet[$i][1]);
	_setValue($SHIPPING_LASTNAME_TEXTBOX, $sheet[$i][2]);
	_setValue($SHIPPING_LASTNAME_TEXTBOX, $sheet[$i][3]);
	//_setValue($SHIPPING_ADDRESS2_TEXTBOX,$sheet[$i][4]);
	_setValue($SHIPPING_POSTAL_TEXTBOX, $sheet[$i][8]);
	_setSelected($SHIPPING_COUNTRY_TEXTBOX,$sheet[$i][5]);
	_setSelected($SHIPPING_STATE_TEXTBOX, $sheet[$i][6]);
	_setValue($SHIPPING_CITY_TEXTBOX, $sheet[$i][7]);
	_setValue($SHIPPING_PHONE_TEXTBOX,$sheet[$i][9]);
	_setValue($SHIPPING_EMAIL_TEXTBOX,$sheet[$i][10]);
	_setValue($SHIPPING_CONFIRMEMAIL_TEXTBOX,$sheet[$i][10]);
}


function eventcalendar()

{
	//should select the date 
	var $currentyear=_getText($SHIPPING_METHOD_DELEVERY_DATE).split("/")[2];
	_click($EVENTCALENDAR_DATE_PICKER_SELECTION_TEXTBOX);
	//select random date
	for (var $i=0;$i<14;$i++)
		{
		//clcik on next button
		_click(var $EVENTCALENDAR_DATE_PICKER_SELECTION_TEXTBOX);
		}
	//click link 25
	_click(var $EVENTCALENDAR_NEXT_ARROW=);
	var $changedyear=_getText($EVENTCALENDAR_DATE_PICKER_SELECTION_TEXTBOX).split(" ")[2];
	_assert($currentyear<$changedyear);

}

function adddressProceed()

{	
	while (_isVisible($SHIPPING_INVALID_ADDRESS_POPUP))
	{
		_click($SHIPPING_INVALID_ADDRESS_POPUP_PROCEED_BUTTON);
	}
}

function BillingAddress($sheet,$i)
{
	_setValue($BILLING_FIRSTNAME_TEXTBOX, $sheet[$i][1]);
	_setValue($BILLING_LASTNAME_TEXTBOX, $sheet[$i][2]);
	_setValue($BILLING_ADDRESS1_TEXTBOX, $sheet[$i][3]);
	//_setValue($BILLING_ADDRESS2_TEXTBOX,$sheet[$i][4]);
	_setValue($BILLING_POSTAL_TEXTBOX, $sheet[$i][8]);
	_setSelected($BILLING_COUNTRY_TEXTBOX,$sheet[$i][5]);
	_setSelected($BILLING_STATE_TEXTBOX, $sheet[$i][6]);
	_setValue($BILLING_CITY_TEXTBOX, $sheet[$i][7]);
	_setValue($BILLING_PHONE_TEXTBOX,$sheet[$i][9]);
	_setValue($BILLING_EMAIL_TEXTBOX,$sheet[$i][10]);
	_setValue($BILLING_CONFIRMEMAIL_TEXTBOX,$sheet[$i][10]);
	
}

function prepopulateShipping($excelSheet,$i)
{
       //verify the prepopulated values
       //checkout login
       _assertEqual($datenowEmail,_getText($SHIPPING_EMAIL_TEXTBOX));
       _assertEqual($datenowEmail,_getText($SHIPPING_CONFIRMEMAIL_TEXTBOX));
       
       //Shipping section
       _assertEqual($excelSheet[$i][1],_getText($SHIPPING_FIRSTNAME_TEXTBOX));
       _assertEqual($excelSheet[$i][2],_getText($SHIPPING_LASTNAME_TEXTBOX));
       _assertEqual($excelSheet[$i][3],_getText($SHIPPING_ADDRESS1_TEXTBOX));
       //_assertEqual($excelSheet[$i][4],_getText($SHIPPING_ADDRESS2_TEXTBOX));
       _assertEqual($excelSheet[$i][5],_getSelectedText($SHIPPING_COUNTRY_TEXTBOX));
       _assertEqual($excelSheet[$i][6],_getSelectedText($SHIPPING_CITY_TEXTBOX));
       _assertEqual($excelSheet[$i][7],_getText($SHIPPING_CITY_TEXTBOX));
       _assertEqual($excelSheet[$i][8],_getText($SHIPPING_POSTAL_TEXTBOX));
       _assertEqual($excelSheet[$i][9],_getText($SHIPPING_PHONE_TEXTBOX));
}

function prepopulateBilling($excelSheet,$i)
{
       //verify the prepopulated values
       //checkout login
       _assertEqual($datenowEmail,_getText($BILLING_EMAIL_TEXTBOX));
       _assertEqual($datenowEmail,_getText($BILLING_CONFIRMEMAIL_TEXTBOX));
       
       //Billing section
       _assertEqual($excelSheet[$i][1],_getText($BILLING_FIRSTNAME_TEXTBOX));
       _assertEqual($excelSheet[$i][2],_getText($BILLING_LASTNAME_TEXTBOX));
       _assertEqual($excelSheet[$i][3],_getText($BILLING_ADDRESS1_TEXTBOX));
       //_assertEqual($excelSheet[$i][4],_getText($BILLING_ADDRESS2_TEXTBOX));
       _assertEqual($excelSheet[$i][5],_getSelectedText($BILLING_COUNTRY_TEXTBOX));
       _assertEqual($excelSheet[$i][6],_getSelectedText($BILLING_STATE_TEXTBOX));
       _assertEqual($excelSheet[$i][7],_getText($BILLING_CITY_TEXTBOX));
       _assertEqual($excelSheet[$i][8],_getText($BILLING_POSTAL_TEXTBOX));
       _assertEqual($excelSheet[$i][9],_getText($BILLING_PHONE_TEXTBOX));
}

function NavigateToPaymentPage()
{
//navigate to cart page with single 
navigateToCart($genericData[0][0],$genericData[0][1]);
//click on checkout link in mini cart
_mouseOver($MINICART_LINK);
_assertVisible($MINICART_LINK_RIGHTTOP_CHECKOUT_LINK);
_click($MINICART_LINK_RIGHTTOP_CHECKOUT_LINK);
//should navigate to shipping page
_assertVisible($SHIPPING_ADDRESS_HEADING);
//Login optional section
_assertVisible($SHIPPING_LOGIN_OPTIONAL_SECTION);

if($pName.indexOf($pnameshippingpage)>=0)                            
{
      _assert(true);
}

//comparing product name in shipping page
$pnameshippingpage=_getText($SHIPPING_ORDER_SUMMARY_PRODUCT_NAME).toString();

//shipping form should display
shippingAddress($checkoutData,0);

//un checking the check box to enter billing address
if ($SHIPPING_ADDRESS_SAME_AS_BILLING_CHECKBOX.checked)
	{
	_uncheck($SHIPPING_ADDRESS_SAME_AS_BILLING_CHECKBOX)
	}

//reload the page 
window.location.href("https://development-webstore-crownawards.demandware.net/s/MAI/checkout");

//enter billing address
BillingAddress($checkoutData,1);

//address proceed
adddressProceed();

//event calendar
eventcalendar();

//address proceed
adddressProceed();

//Selecting first shipping method
_check($SHIPPING_METHOD_ID);
_wait(4000);
//click on continue button
_click($SHIPPING_CONTINUE_BUTTON);

//address proceed
adddressProceed();

$priceInPaymentPage=_extract(_getText($PAYMENT_PAGE_PRICE),"/[$](.*)/",true);
}

//payment details
function PaymentDetails($sheet,$i)
{
	
_setValue($PAYMENT_CREDIT_CARDNUMBER_TEXTBOX, $sheet[$i][1]);
_setValue($PAYMENT_CREDITCART_CVN_TEXTBOX, $sheet[$i][2]);
_setSelected($PAYMENT_EXPIRATION_MONTH_TEXTBOX, $sheet[$i][3]);
_setSelected($PAYMENT_EXPIRATION_YEAR_TEXTBOX, $sheet[$i][4]);
	
}

function orderConfirmationVerification()
{
	//Checking order is placed or not
	_assertVisible($ORDER_CONFIRMATION_HEADING);
	_assertVisible($ORDER_CONFIRMATION_DETAILS);
	
	$pnameinocppage=_getText($ORDER_CONFIRMATION_PRODUCT_NAME).toString();
	if($pnameshippingpage.indexOf($pnameinocppage)>=0)                         
	{
	      _assert(true);
	}

	_assertEqual($prodquantity,_getText($ORDER_CONFIRMATION_PRODUCT_QUANTITY));


	$priceinOCP=_extract(_getText($ORDER_CONFIRMATION_TOTAL_PRICE),"/: [$](.*)/",true);
	$ordertotalOCP=_extract(_getText($PAYMENT_PAGE_PRICE),"/[$](.*)/",true);

	//price verification
	_assertEqual($priceInPaymentPage,$priceinOCP);
	_assertEqual($priceinOCP,$ordertotalOCP);


	var $orderno=_getText($ORDER_CONFIRMATION_NUMBER).split(" ")[2];
	_log($orderno);

	var $orderkkkk=_getText($ORDER_CONFIRMATION_NUMBER)
	_log($orderkkkk);

	takescreenshot();

	//payment in formation in order confirmation page
	_assertVisible($ORDER_CONFIRMATION_PAYMENT_DETAILS);
}

//no need of creating objects for the cookie function
function ClearCookies()
{
       _navigateTo($URL[0][0]+"/_s_/dyn/Cookies_showAll");
       _check(_checkbox("_sahi_chooseAll"));
       _click(_button("Delete")); 
       SiteURLs();
}

//Billing Address comparison in payment page
function ComparingBillingAddressFromPaymentPage($i)
{
//vinay lanka 6379432404 vinay@gmail.com 12456 street aveneue WHITE PLAINS, NY 10603 United States	
var $billingaddress=$checkoutData[$i][1]+" "+$checkoutData[$i][2]+" "+$checkoutData[$i][9]+" "+$checkoutData[$i][10]+" "+$checkoutData[$i][3]+" "+$checkoutData[$i][7]+", "+$checkoutData[$i][11]+" "+$checkoutData[$i][8]+" "+$checkoutData[$i][5];
return $billingaddress;
}

function ComparingShippingAddressFromPaymentPage($i)
{
//vinay lanka 12456 street aveneue WHITE PLAINS, NY 10603 United States
var $shippingaddress=$checkoutData[$i][1]+" "+$checkoutData[$i][2]+" "+$checkoutData[$i][3]+" "+$checkoutData[$i][7]+", "+$checkoutData[$i][11]+", "+$checkoutData[$i][8]+" "+$checkoutData[$i][5];
return $shippingaddress;
}

function ComparingAddressFromAccountLandingPage($i)
{
//12456 STREET AVENEUE NEW YORK, NY 10258-0001 United States (888) 888-8888 VINAY@GMAIL.COM
var $shippingaddressInMyaccount=$checkoutData[$i][3]+" "+$checkoutData[$i][7]+", "+$checkoutData[$i][11]+" "+$checkoutData[$i][8]+" "+$checkoutData[$i][5]+" "+$checkoutData[$i][13]+" "+$datenowEmail;
return $shippingaddressInMyaccount;
}