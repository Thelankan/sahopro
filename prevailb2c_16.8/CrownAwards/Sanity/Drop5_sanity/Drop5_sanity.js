

_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Drop5_sanity.xls");

var $drop5_data=_readExcelFile("Drop5_sanity.xls","Data");
var $profilevalues=_readExcelFile("Drop5_sanity.xls","Profile");
var $address=_readExcelFile("Drop5_sanity.xls","Address");
var $Address_payment=_readExcelFile("Drop5_sanity.xls","Address_payment");
var $carddata=_readExcelFile("Drop5_sanity.xls","CardData");

ClearCookies();

deleteUser_profile();
SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


if (_isChrome())
	{
	// manual testing. We are not able to delete user from BM due to assist.
		
var $t = _testcase("12345","checking custom object is created in bm or not");
$t.start();
try
{

	 //Create account
	if (isMobile())
	{
	 _click(_link("/mobile-show/", _in(_div("user-info"))));
	}
	else
		{
		 _click(_link("/desktop-show/", _in(_div("user-info"))));
		 //Visibility of overlay
		 _assertVisible(_div("dialog-container"));
		}

	//click on create account button
	_click(_link("/button/", _in(_div("login-box registration-btn"))));
	 
	//create account	
	createAccount_profile($profilevalues,0);

	//should navigate to my account landing page
	//_assertVisible(_heading1($address[1][13]));
	//_assertVisible(_div("account-page"));
	_assertVisible(_link("Log Out"));
	
	//logout from the application
	logout();

	//checking custom object is created in BM or not 
	BM_Login();
	_click(_link("Customers"));

	//Need to change these two steps once we got the access
	//_click(_link("overview_subtitle", _in(_cell("Import & Export"))));
	//_click(_submit("Export",_near(_cell("Customers"))));

	_click(_link("Customers[1]"));
	_click(_link("Advanced"));

	_setValue(_textbox("WFCustomerAdvancedSearch_Email"),$profilevalues[0][1]);
	_click(_submit("Find", _in(_div("/Advanced Customer Search/"))));

	//Should match the user created
	_assertVisible(_cell($profilevalues[0][1]));
	//log out from BM
	_click(_link("Log off."));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()
	
	
	
	var $t = _testcase("233223","To verify User should able to update password");
	$t.start();
	try
	{
		deleteUser_profile();
		SiteURLs();

		 //Create account
		if (isMobile())
		{
		 _click(_link("/mobile-show/", _in(_div("user-info"))));
		}
		else
			{
			 _click(_link("/desktop-show/", _in(_div("user-info"))));
			 //Visibility of overlay
			 _assertVisible(_div("dialog-container"));
			}
		
		 _click(_link("/button/", _in(_div("login-box registration-btn"))));
		
		 createAccount_profile($profilevalues,0);
		 _wait(3000)
		 
		 _setValue(_textbox("topSearch"), "my account");
		 _click(_submit("home-search-btn"));
		
		////NavigatetoAccountLanding();
		 _click(_link($drop5_data[1][2]));
		 _setValue(_password("/dwfrm_profile_login_currentpassword/"), $profilevalues[0][5]);
		 _setValue(_password("/dwfrm_profile_login_resetpassword/",_near(_heading2("Reset Password"))), $profilevalues[1][5]);
		 _setValue(_password("/dwfrm_profile_login_confirmresetpassword/",_near(_heading2("Reset Password"))), $profilevalues[1][5]);
		// _click(_submit("/dwfrm_profile_confirm/",_near(_checkbox("dwfrm_profile_customer_addtoemaillist"))));
		 _click(_submit("desktop-show update-password"));
		 
		 _wait(5000);
		 logout();
		 _wait(5000);
		 
		 _click(_link("Log In", _in(_div("user-section"))));
		 _setValue(_textbox("/dwfrm_login_username/"), $profilevalues[0][1]);
		 _setValue(_password("/dwfrm_login_password/"), $profilevalues[1][5]);
		 _click(_submit("/dwfrm_login/"));
		 _wait(5000);
		//NavigatetoAccountLanding();
		 _assertVisible(_link("Log Out"));
		 _click(_link("Your Orders"));
		 _click(_link("My Account", _in(_div("main-content"))));
		 logout();
		 
//		 _click(_link($drop5_data[1][2]));
//		 _setValue(_textbox("dwfrm_profile_customer_email"), $profilevalues[0][1]);
//		 _setValue(_textbox("dwfrm_profile_customer_emailconfirmupdate"),  $profilevalues[0][1]);
//		 _setValue(_password("/dwfrm_profile_login_currentpassword/"),$profilevalues[1][5]);
//		 _setValue(_password("/dwfrm_profile_login_resetpassword/"), $profilevalues[0][5]);
//		 _setValue(_password("/dwfrm_profile_login_confirmresetpassword/"),  $profilevalues[0][5]);
//		 click on submit button
//		 _click(_submit("dwfrm_profile_confirm",_near(_checkbox("/input-checkbox/"))))
//		 _click(_submit("/dwfrm_profile_confirm/",_near(_checkbox("dwfrm_profile_customer_addtoemaillist"))));
//		 _wait(5000);
//		 logout();
	}
			

	catch($e)
	{
		_logExceptionAsFailure($e);
	}
	$t.end();

var $t = _testcase("24681","create account with existing user");
$t.start();
try
{

	 //site urls	
	 SiteURLs();
	 
	 //Create account
	if (isMobile())
	{
	 _click(_link("/mobile-show/", _in(_div("user-info"))));
	}
	else
		{
		 _click(_link("/desktop-show/", _in(_div("user-info"))));
		 //Visibility of overlay
		 _assertVisible(_div("dialog-container"));
		}
	
	//click on create account button
	 _click(_link("/button/", _in(_div("login-box registration-btn"))));
	 
	//create account	
	createAccount_profile($profilevalues,2);
	//error message should display
	_assertVisible(_div($drop5_data[0][6]));
	//close button
	_click(_button("Close"));
	_assertNotVisible(_div("/login-box-content/"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()
}


//click on login link
if (isMobile())
{
 _click(_link("/mobile-show/", _in(_div("user-info"))));
}
else
	{
	 _click(_link("/desktop-show/", _in(_div("user-info"))));
	 //Visibility of overlay
	 _assertVisible(_div("dialog-container"));
	}

//login to the application
login();
_wait(4000);
_click(_link("Your Orders"));
_click(_link("My Account", _in(_div("main-content"))));


var $t = _testcase("220208","Verify the Navigation of 'Detail' Link in the Submit Payment Page for Desktop/ipad view");
$t.start();
try
{

//navigate to ay account landing
//NavigatetoAccountLanding();	

//click on payment link
_click(_link("Make a Payment", _in(_div("account-page"))));
//should navigate to payment page
_assertEqual($drop5_data[1][3], _getText(_link("breadcrumb-element last", _in(_div("breadcrumb")))));
//_assertVisible(_heading1($drop5_data[1][3]));
_assertVisible(_heading1($drop5_data[1][3], _in(_div("primary-content"))));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()



//delete address
deleteAddress();

var $t = _testcase("219560","Verify the functionality related tothe 'Edit'  link present below each of the addresses on my account Addresses page of the application as a Registered user.");
$t.start();
try
{
	
//click on add new address button
 _click(_link("/New Address/"));
//address form should display
_assertVisible(_div("addressform"));
//entering values in address form
addAddress($address,0);	

_click(_submit("dwfrm_profile_address_create"));

_wait(9000, _isVisible(_div($address[0][3]+" "+$address[0][4])));

//click on continue in address
if (_isVisible(_div("/invalid-address/")))
	 {
	 _click(_link("Continue"));
	 }


_click(_link("address-edit"));
_wait(2000);
//verifying the pre-populate values
_assertEqual("/"+$address[0][1]+"/i",_getText(_textbox("dwfrm_profile_address_email")));
_assertEqual("/"+$address[0][3]+"/i",_getText(_textbox("dwfrm_profile_address_firstname")));
_assertEqual("/"+$address[0][4]+"/i",_getText(_textbox("dwfrm_profile_address_lastname")));
_assertEqual("/"+$address[0][6]+"/i",_getText(_textbox("dwfrm_profile_address_address1")));
_assertEqual("/"+$address[0][10]+"/i",_getText(_textbox("dwfrm_profile_address_postal")));
_assertEqual("/"+$address[0][9]+"/i",_getText(_textbox("dwfrm_profile_address_city")));
_assertEqual("/"+$address[0][7]+"/i",_getSelectedText(_select("dwfrm_profile_address_country")));
_assertEqual("/"+$address[0][8]+"/i",_getSelectedText(_select("dwfrm_profile_address_states_state")));
_assertEqual("/"+$address[0][11]+"/i",_getText(_textbox("dwfrm_profile_address_phone")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("219547/219558/219552","Verify the navigation of 'Addresses' link in the My Account home page/Verify the functionality related to 'CANCEL' button  'Add Address overlay Addresses page' in application as a  Registered user.");
$t.start();
try
{

	//NavigatetoAccountLanding();
	_click(_link("Your Orders"));
	_click(_link("My Account", _in(_div("main-content"))));
	//click on address link
	_click(_link($drop5_data[0][2]));
	//address book
	_assertVisible(_heading1($drop5_data[1][0]));
	//checking address link in address page bread crumb 
	_assertVisible(_link($drop5_data[1][1], _in(_div("breadcrumb"))));
	//click on add new address button
	 _click(_link("/New Address/"));
	//_click(_submit("address-create desktop-show"));
	//address form should display
	_assertVisible(_div("addressform"));
	//entering values in address form
	addAddress($address,0);
	//Click on cancel button
	_click(_link("Cancel"));
	//add address heading should Not display
	//_assertNotVisible(_div("addressform"));
	_assertNotVisible(_heading1("Add Address"));
	_wait(4000);
	
	//****** verifying with out data *********
	
	//click on add new address button
	 _click(_link("/New Address/"));
	//_click(_submit("address-create desktop-show"));
	//address form should display
	_assertVisible(_div("addressform"));
	//Click on cancel button
	_click(_link("Cancel"));
	// address heading should Not display
	_assertNotVisible(_heading1("Add Address"));
	_wait(4000);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("220147","Verify the Navigation of 'Breadcrumb' in the My Account Order History Page for Desktop/ipad view");
$t.start();
try
{

	//click on order history link
	_click(_link("/order-history/"));
	//verifying bread crumb
	_assertVisible(_link($address[0][12], _in(_div("breadcrumb"))));
	_assertVisible(_link($address[1][12], _in(_div("breadcrumb"))));
	_assertVisible(_link($address[2][12], _in(_div("breadcrumb"))));
	//click on order history in bread crumb
	_click(_link($address[2][12], _in(_div("breadcrumb"))));
	_assertVisible(_heading1($address[0][13]));
	//click on order history in bread crumb
	_click(_link($address[1][12], _in(_div("breadcrumb"))));
	//should navigate to my account landing page
	_assertEqual($address[1][13], _getText(_heading1("Options")));
	//click on order history in bread crumb
	_click(_link($address[0][12], _in(_div("breadcrumb"))));
	//should navigate to home page
	_assertNotVisible(_div("breadcrumb"));
	_assertVisible(_div("home-main-right"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("220126/220150","Verify the Functionality of the 'Order Details' Section in the My Account Order Details Page for/Verify the Functionality of  'Search by Order No' in the My Account Order History Page for Desktop");
$t.start();
try
{
	navigateToCart($drop5_data[0][7],$drop5_data[0][8]);
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
	//navigate to shipping page
	_assertVisible(_heading2("Shipping Address"));
	
	//select any address
	if (_isVisible(_select("dwfrm_singleshipping_addressList")))
	{
		//select address from drop down
		_setSelected(_select("dwfrm_singleshipping_addressList"),"1");
	}
	
	//proceed to next page
	adddressProceed();
	
	//selecting shipping method
	_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
	
	//event calendar
	eventcalendar();
	
	_click(_submit("/continue-button/"));
	
	//entering the payment details
	PaymentDetails($Address_payment,3);
	
	//checking the checkbox
	// _check(_checkbox("/dwfrm_billing_confirm/"));
	// _assertEqual(true, _checkbox("/dwfrm_billing_confirm/").checked);
	 
	 //_click(_div("/Payment & Place Order/"));
	 _click(_submit("/continue-button/"));
	 
	 
	//storing the order no from order confirmation page 
	 var $orderno=_extract(_getText(_span("order-number")),"/[#](.*)/",true);
	 _log($orderno);
	 
	 var $IDinocpPage=_getText(_div("mini-address-location")).split(" ")[2];
	 var $billaddress=_getText(_div("mini-address-location"));
	 var $shipaddress=_getText(_div("address"));
	 var $price=_extract(_getText(_span("order-price")),"/: (.*)/",true);
	 var $shippingmethod=_getText(_div("value",_in(_div("shipping-method"))));
	 var $orderprice=_getText(_div("order-value",_in(_listItem("OrderTotalNum"))));
	//card number
	 var $verifyingcardno=_getText(_div("cc-number"));
	 
	 //delivery date
	 var $deliverydate=_extract(_getText(_div("order-date")),"/: (.*)/",true);
	 
	 takescreenshot();
		
	//click on order link in header
	_click(_link("Your Orders"));
	_wait(5000);
	
	while(_isVisible(_textbox("searchorder")))
		{
		//enter vaild details in order no field
		_setValue(_textbox("searchorder"),$orderno);
		//click on search order button
		_click(_submit("go-btn"));
		_wait(5000);		
		}

	//need ot verify below mentioned things
	//storing the order no from order confirmation page 
	var $ordernoinDetailspage=_extract(_getText(_span("order-number")),"/[#](.*)/",true);
	var $emailIdinDetailspage=_getText(_div("mini-address-location")).split(" ")[2];
	
	//address verification 
	var $billaddressinDetailspage=_getText(_div("mini-address-location"));
	var $shipaddressinDetailspage=_getText(_div("address"));
	
	//price comparing in details page
	var $priceinDetailspage=_extract(_getText(_span("order-price")),"/: (.*)/",true);
	var $orderpriceinDetailspage=_getText(_div("order-value",_in(_listItem("OrderTotalNum"))));
	
	//shipping method
	var $shippingmethodinDetailspage=_getText(_div("value",_in(_div("shipping-method"))));
	
	//card number
	var $verifyingcardnoinDetailpage=_extract(_getText(_div("order-payment-instruments")),"/: (.*)/",true);
	
	//delivery date
	var $deliverydateinDetailpage=_extract(_getText(_div("order-date")),"/: (.*)/",true);
	
	_assertEqual($orderno,$ordernoinDetailspage);
	_assertEqual($IDinocpPage,$emailIdinDetailspage);
	_assertEqual($billaddress,$billaddressinDetailspage);
	_assertEqual($shipaddress,$shipaddressinDetailspage);
	_assertEqual($price,$priceinDetailspage);
	_assertEqual($orderprice,$orderpriceinDetailspage);
	_assertEqual($shippingmethod,$shippingmethodinDetailspage);
	_assertEqual($verifyingcardno,$verifyingcardnoinDetailpage);
	_assertEqual($deliverydate,$deliverydateinDetailpage);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()



var $t = _testcase("12345/219209","To check that payment details is pre populating in payment page");
$t.start();
try
{
//	Navigate to account landing page & verifying the empty card
	DeleteCreditCard();
	//click on add new card
	_click(_submit("/add-new-paymentcard/"));
   //create a credit card
   CreateCreditCard($carddata,0);
   _click(_submit("apply-button js-add-creditcard "));
   
   //verifying the newly created card
   _assertVisible(_div("account-landing-header"));
   _assertVisible(_list("payment-list"));

   //add product to cart
   $product=$drop5_data[0][7];
   navigateToCart($product,1);
   
    //click mini cart link
   _click(_link("/mini-cart-link-checkout-link/"));
   
   //verifying shipping page
   _assertVisible(_div("step-2 active"));
   _assertVisible(_div("single-ship address-fields"));

   //enter shipping details
   //shippingAddress($Addresss,0)
   
   if (_isVisible(_select("dwfrm_singleshipping_addressList")))
   {
   //select address from drop down
   _setSelected(_select("dwfrm_singleshipping_addressList"),"1");
   }
   
 //proceed to next page
   adddressProceed();

  _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
 //Selecting shipping method
 _check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
   
   //event calendar
   eventcalendar();
   
   _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
   
   
   //displaying and verifying payment page
   _assertVisible(_div("Payment & Place Order"));
   _assertVisible(_div("checkout-order-total CartSummaryWrap", _in(_div("secondary"))));
   _assertVisible(_div("address-details"));
   _assertVisible(_fieldset("Select Payment Method"));

   //verifying the payment details
   
   //select existing card details
   //_setSelected(_select("creditCardList"),1);
   
   if (_isVisible(_setSelected(_select("creditCardList"))))
	   {
	   _setSelected(_select("creditCardList"),1);
	   }
   
   _wait(3000);
   
   //credit card no.
   _assertEqual($carddata[0][6], _getText(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/")));
   
   //month
   _assertEqual($carddata[0][2], _getSelectedText(_select("/dwfrm_billing_paymentMethods_creditCard_expiration_month/")));
		   
	//year
   _assertEqual($carddata[0][3],_getSelectedText(_select("/dwfrm_billing_paymentMethods_creditCard_expiration_year/")));
   
   //enter cvn no and verify it
   _setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"), $carddata[0][5]);
   _assertEqual($carddata[0][5], _getText(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/")));

		 
  //click on place order button
 //check the check box
 //_check(_checkbox("/dwfrm_billing_confirm/"));
 //place order button
_click(_submit("dwfrm_billing_save"));
   
   //verifying order confirmation page
   _assertVisible(_div("confirmation-message"));
   _assertVisible(_div("order-information"));
   _assertVisible(_div("order-billing"));
   _assertVisible(_div("address"));
   _assertVisible(_div("order-payment-instruments"));
   _assertVisible(_div("order-totals-table box"));
   
   var $orderno=_getText(_span("order-number")).split(" ")[2];
   _log($orderno);

   var $orderkkkk=_getText(_span("order-number"))
   _log($orderkkkk);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("123456","verify saved card details in payment page is pre populating in payment setting page");
$t.start();
try
{

//	Navigate to account landing page & verifying the empty card
	DeleteCreditCard();
   
 //add product to cart
   $product=$drop5_data[0][7];
   navigateToCart($product,1);
   
   //click mini cart link
  _click(_link("/mini-cart-link-checkout-link/"));
   
   //verifying shipping page
   _assertVisible(_div("step-2 active"));
   _assertVisible(_div("single-ship address-fields"));

   //enter shipping details
   //shippingAddress($Addresss,0)
   
   if (_isVisible(_select("dwfrm_singleshipping_addressList")))
   {
   //select address from drop down
   _setSelected(_select("dwfrm_singleshipping_addressList"),"1");
   }
   
 //proceed to next page
   adddressProceed();
   _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
   //Selecting shipping method
   _check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
   
   //event calendar
   eventcalendar();
   
   _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
   
   //displaying and verifying payment page
   _assertVisible(_div("Payment & Place Order"));
   _assertVisible(_div("checkout-order-total CartSummaryWrap", _in(_div("secondary"))));
   _assertVisible(_div("address-details"));
   _assertVisible(_fieldset("Select Payment Method"));
   
   //enter card details
   PaymentDetails($Address_payment,3);
   
   //check the check box for save card
   _assertVisible(_checkbox("input-checkbox "));
   _check(_checkbox("input-checkbox "))
   
 //check the other check box 
  // _check(_checkbox("/dwfrm_billing_confirm/"));
   
   //click on place order button
   //_check(_checkbox("/dwfrm_billing_confirm/"));
   //place order button
   _click(_submit("dwfrm_billing_save"));
   
   //verifying order confirmation page
   _assertVisible(_div("confirmation-message"));
   _assertVisible(_div("order-information"));
   _assertVisible(_div("order-billing"));
   _assertVisible(_div("address"));
   _assertVisible(_div("order-payment-instruments"));
   _assertVisible(_div("order-totals-table box"));
   
   var $orderno=_getText(_span("order-number")).split(" ")[2];
   _log($orderno);

   var $orderkkkk=_getText(_span("order-number"))
   _log($orderkkkk);
   
   //Navigate to account landing page
	//NavigatetoAccountLanding();
	 _click(_link("Your Orders"));
	 _click(_link("My Account", _in(_div("main-content"))));
	//navigate to payment setting page
  _click(_link("PAYMENT SETTINGS"));
  
  //verifying payment setting page
    _assertVisible(_link("Payment Settings", _in(_div("breadcrumb"))));
    _assertVisible(_list("payment-list"));
	_assertVisible(_heading1("Payment Settings"))
	_assertVisible(_submit("Edit"));
	_assertVisible(_submit("Delete"));
	_assertVisible(_submit("Add new"));
	_assertVisible(_div("cc-type"));
	_assertVisible(_div("cc-number"));
	_assertVisible(_div("cc-exp"));

  
  //verifying the card  details
 //credit card no.
  _assertEqual($Address_payment[3][5], _getText(_div("cc-number")));
  
 //month and year
  _assertEqual($Address_payment[3][7], _getText(_div("cc-exp")));
	   
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


//################ Functions ##############

function ComparingValues($sheet,$Rowno)
{
	_assertEqual($sheet[$Rowno][1],_getText(_textbox("dwfrm_profile_customer_email")));
	//_assertEqual($sheet[$Rowno][3],_getText(_textbox("dwfrm_profile_customer_firstname")));
	//_assertEqual($sheet[$Rowno][4],_getText(_textbox("dwfrm_profile_customer_lastname")));
}

function createAccount_profile($sheet,$Rowno)
{
	 _setValue(_textbox("dwfrm_profile_customer_email"), $sheet[$Rowno][1]);
	 _setValue(_textbox("dwfrm_profile_customer_emailconfirm"), $sheet[$Rowno][2]);
	 _setValue(_textbox("dwfrm_profile_customer_firstname"), $sheet[$Rowno][3]);
	 _setValue(_textbox("dwfrm_profile_customer_lastname"), $sheet[$Rowno][4]);
	 _setValue(_password("/dwfrm_profile_login_password/"), $sheet[$Rowno][5]);
	 _setValue(_password("/dwfrm_profile_login_passwordconfirm/"), $sheet[$Rowno][6]);
	 //click on submit button
	 _click(_submit("dwfrm_profile_confirm"));
	 _wait(5000);
}

function createAccount_profileUpdate($sheet,$Rowno)
{
//	_setValue(_textbox("dwfrm_profile_customer_email"), $sheet[$Rowno][1]);
//	_setValue(_textbox("dwfrm_profile_customer_emailconfirmupdate"), $sheet[$Rowno][2]);
//	//_setValue(_textbox("dwfrm_profile_customer_firstname"), $sheet[$Rowno][3]);
//	//_setValue(_textbox("dwfrm_profile_customer_lastname"), $sheet[$Rowno][4]);
//	 _setValue(_password("/dwfrm_profile_login_currentpassword/"),$sheet[0][5]);
//	 _setValue(_password("/dwfrm_profile_login_resetpassword/"), $sheet[$Rowno][5]);
//	 _setValue(_password("/dwfrm_profile_login_confirmresetpassword/"),  $sheet[$Rowno][6]);
//	 //click on submit button
//	 _click(_submit("dwfrm_profile_confirm",_in(_div("Update profile Cancel"))));
//	 _wait(5000);
	 _setValue(_password("/dwfrm_profile_customer_currentpassword/"), $sheet[0][5]);
	 _setValue(_password("/dwfrm_profile_login_currentpassword/"), $sheet[0][5]);
	 _setValue(_password("/dwfrm_profile_login_resetpassword/"), $sheet[$Rowno][5]);
	 _setValue(_password("/dwfrm_profile_login_confirmresetpassword/"), $sheet[$Rowno][5]);
	 _click(_submit("desktop-show update-password"));
	 _wait(2000);
}

function deleteUser_profile()
{
	BM_Login();
_click(_link("Customers"));
_click(_link("Customers[1]"));
_click(_link("Advanced"));
_setValue(_textbox("WFCustomerAdvancedSearch_Email"),$profilevalues[0][1]);
_click(_submit("Find", _in(_div("/Advanced Customer Search/"))));
if(_isVisible(_row("/Select All/")))
{
	if(_assertEqual($profilevalues[0][1],_getText(_cell("table_detail e s[4]"))))
		{
		_click(_checkbox("DeleteCustomer"));
		_click(_submit("Delete"));
		_click(_submit("OK"));
		_log("Successfully deleted the user")
		}
}

else
{
	_log("search did not match any users.");
}
_click(_link("Log off."));

}

function ProfileValues($sheet,$Rowno)
{
	 _setValue(_textbox("dwfrm_profile_customer_email"),$sheet[$Rowno][1]);
	 _setValue(_textbox("dwfrm_profile_customer_emailconfirm"),$sheet[$Rowno][2]);
	 _setValue(_textbox("dwfrm_profile_customer_firstname"), $sheet[$Rowno][3]);
	 _setValue(_textbox("dwfrm_profile_customer_lastname"),$sheet[$Rowno][4]);
	 _setValue(_password("/dwfrm_profile_login_password/"),$sheet[$Rowno][5]);
	 _setValue(_password("/dwfrm_profile_login_passwordconfirm/"),$sheet[$Rowno][6]);
	 _click(_submit("dwfrm_profile_confirm")); 	 
}

var $t = _testcase("219258","Verify the functionality of UPDATE PROFILE BUTTON  in SETTINGS AND PASSWORD page");
$t.start();
try
{
	
	//navigate to my account landing page
	_click(_link("Your Orders"));
	_click(_link("My Account", _in(_div("main-content"))));
	//click on settings and password
	_click(_link("/Settings(.*)Password/"));
	
	//current pass
	_setValue(_password("/dwfrm_profile_customer_currentpassword/"), $pwd);
	_setValue(_password("/dwfrm_profile_login_currentpassword/"), $pwd);
	
	//new pass
	_setValue(_password("/dwfrm_profile_login_resetpassword/"), "Revtech123@");
	_setValue(_password("/wfrm_profile_login_confirmresetpassword/"), "Revtech123@");
	
	//update button
	_click(_submit("desktop-show update-password"));
	
	//logout from the application
	logout();
	
	//login to the application
	if (isMobile())
	{
	 _click(_link("/mobile-show/", _in(_div("user-info"))));
	}
	else
		{
			 _click(_link("/desktop-show/", _in(_div("user-info"))));
			 //Visibility of overlay
			 _assertVisible(_div("dialog-container"));
		}
	
	//login to the app with previously updated credentials
	_setValue(_textbox("/dwfrm_login_username/"),$uId);
	_setValue(_password("/dwfrm_login_password/"),"Revtech123@");
	_click(_submit("/dwfrm_login/", _in(_div("inner-container"))));
	_wait(3000);
	
	//navigate to my account landing page
	_click(_link("Your Orders"));
	_click(_link("My Account", _in(_div("main-content"))));
	//click on settings and password
	_click(_link("/Settings(.*)Password/"));
	
	   
	
	//current pass
	_setValue(_password("/dwfrm_profile_customer_currentpassword/"), "Revtech123@");
	_setValue(_password("/dwfrm_profile_login_currentpassword/"), "Revtech123@");
	
	//new pass
	_setValue(_password("/dwfrm_profile_login_resetpassword/"), $pwd);
	_setValue(_password("/wfrm_profile_login_confirmresetpassword/"), $pwd);
	
	//update button
	_click(_submit("desktop-show update-password"));
	
	//logout from the application
	logout();
	
	//click on login link
	if (isMobile())
	{
	 _click(_link("/mobile-show/", _in(_div("user-info"))));
	}
	else
		{
		 _click(_link("/desktop-show/", _in(_div("user-info"))));
		 //Visibility of overlay
		 _assertVisible(_div("dialog-container"));
		}

	//login with updated credentials
	login();
	
	//verify login page
	_assertVisible(_link("Your Orders"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

