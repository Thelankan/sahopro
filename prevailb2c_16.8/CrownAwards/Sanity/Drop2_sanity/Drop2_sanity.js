_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Drop2_sanity.xls");
_resource("../../ShopNav/ErrorPage/ErrorPage.xls");

var $details=_readExcelFile("Drop2_sanity.xls","Details");
var $ErrorPage=_readExcelFile("../../ShopNav/ErrorPage/ErrorPage.xls","ErrorPage");

ClearCookies();
_setAccessorIgnoreCase(true); 
cleanup();

var $t = _testcase("215580","Verify the functionality of 'Search box'  in  'Color insert finder' page in desktop/tablet");
$t.start();
try
{
	//Click on Insert finder link in the left nav
	_click(_link($details[0][0], _in(_listItem($details[0][0]))));
	//Category level searchbox
	_setValue(_textbox("categorySearch", _in(_div("search-box-main"))),$details[0][1]);
	//click on searh button
	_click(_submit("btSearch", _in(_div("search-box-main"))));
	
	//_focusWindow();
	//Click on Enter key
	//_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	
	//Breadcrumb/refinement heading
	//_assertContainsText($details[0][1], _heading1("insert-crown-head"));
	_assertContainsText($details[0][1],_heading1("insert-crown-head"));
	//_assertContainsText($details[0][1], _heading1("bigtext-line0"));
	//Dispay of products
	//_assertVisible(_div("product-tile"));
	//_assertVisible(_div("dwt-boxes-block"));
	_assertExists(_div("search-result-content1 finder-carousel"));
	_assertVisible(_div("product-tile", _in(_listItem("grid-tile"))));
	//_assertVisible(_div("product-tile", _in(_div("slick-track"))))
	//_assertVisible(_div("/product-tile/"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("215921","Verify the functionality of Shop Now button in the Product tile search in Desktop/tablet");
$t.start();
try
{
	//navigate to search result page	
	_setValue(_textbox("/Search/"),$details[1][0]);
	
	//search submit
	_click(_submit("Search"));
	
	//prod name in PLP
	var $prodNameinPLP=_getText(_link("name-link"));
	//click on shop now link
	_click(_link("show-now-link"));
	//Prod Name in PDP
	var $prodNameinPDP=_getText(_heading2("product-name"));
	
	//checking same name is coming or not 
	if($prodNameinPLP.indexOf($prodNameinPDP)>=0)				
	{
		_assert(true);
	}
	
	_assertVisible(_image("/(.*)/", _in(_div("product-primary-image"))));
	_assertVisible(_div("pdpMain"));
	_assertVisible(_div("product-col-2 product-detail"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("12345","checking zoom functionality");
$t.start();
try
{
	
	//search product	
	search($details[2][0]);
	
	//check the zoom functionality
	_click(_link("product-zoom"));
	_assertVisible(_div("/product-zoomimage/"));
	_assertVisible(_button("Close"));
	_click(_button("Close"));
	_assertNotVisible(_div("/product-zoomimage/"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("216143","Verify the functionality of selecting different show options in Show dropdown in Column finder product listing page in desktop/tablet");
$t.start();
try
{
	NavigatetoHomepage()
	//click on insert finder
	//_click(_link("Column Finder", _in(_div("/LeftNav/"))));
	NavigatetoSubcat($details[5][0]);
	
	if(_isVisible(_select("grid-sort-header")))
	{
		var $sort=_getText(_select("grid-sort-header"));
		var $j=0;
		for(var $i=1;$i<$sort.length;$i++)
		{
			_setSelected(_select("grid-sort-header"), $sort[$i]);
			//Validating the selection
			_assertEqual($sort[$i],_getSelectedText(_select("grid-sort-header")),"Selected option is displaying properly");
			//_assertEqual($sort[$i],_getSelectedText(_select("grid-sort-footer")),"Selected option is displaying properly");
			sortBy($sort[$i]);
		}
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()



var $t = _testcase("216315","Verify the functionality of Recently Viewed carousel in Crown nav desktop/tablet");
$t.start();
try
{

//navigate to any of the product PDP

var $prodnamelist=new Array()	
	
for (var $i=0;$i<3;$i++)
	{

	search($details[$i][4]);
//	if (_isVisible(_link("name-link", _in(_listItem("grid-tile slick-slide slick-active[1]")))))
//		{
//		//_click(_link("/name-link/",_in(_listItem("grid-tile new-row"))));
//		_click(_link("name-link", _in(_listItem("grid-tile slick-slide slick-active[1]"))))
//		}
	var $productName=_getText(_heading2("product-name"));
	$prodnamelist.push($productName);
	_log($productName);
	
	}

NavigatetoHomepage();

//count of products in recently viewed
var $count=_count("_link","name-link",_in(_div("slick-track",_in(_list("/last-visited-result-items/")))));
_assertEqual($details[3][4],$count);

var $recentlyviewedprods=_collectAttributes("_link","name-link","sahiText",_in(_div("slick-track",_in(_list("/last-visited-result-items/")))));
_assertEqualArrays($prodnamelist.sort(),$recentlyviewedprods.sort());

//click on recently viewed products on home page should navigate to PD page
for (var $j=0;$j<$count;$j++)
	{
		_click(_link($recentlyviewedprods[$j]));
		var $productName=_getText(_heading2("product-name"));
		_assertVisible(_image("/(.*)/", _in(_div("product-primary-image"))));
		_assertVisible(_div("pdpMain"));
		_assertVisible(_div("product-col-2 product-detail"));
		_assertEqual($recentlyviewedprods[$j],$productName);
		NavigatetoHomepage();
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216340","Verify the functionality of 'More like this' link in Recently Viewed carousel in Crown nav desktop/tablet");
$t.start();
try
{
	
//click on more like this link in recently viewed section
_click(_link("more-like-this",_in(_list("/last-visited-result-items/"))));
//should navigate to category landing page
_assertVisible(_div("subcategory-tiles"));



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

if (isMobile())
	{
var $t = _testcase("216462","Verify the functionality of Choose your sport dropdown in Award by sport category page mobile");
$t.start();
try
{

//click on award by sport link
_click(_link($details[1][2]));
_assertVisible(_div("Select Sport Or Activity"));
_click(_link($details[0][2],_near(_div("Select Sport Or Activity"))));
//should navigate to base ball category
_assertVisible(_span($details[0][2], _in(_div("breadcrumb"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()
	}

var $t = _testcase("216505","Verify the functionality links in  'All inserts' template in Insert Finder _mobile category landing page");
$t.start();
try
{

//click on insert finder	
_click(_link("INSERT FINDER"));
_assertContainsText("Insert Finder", _div("breadcrumb"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216646","Verify the functionality of Search again box in 404 page in Desktop");
$t.start();
try
{

//navigate to 404 error page
//_navigateTo($ErrorPage[0][1]);
	NavigatetoHomepage()
//enter invalid text
  search("mm*ana98");

//verify navigation
  _assertVisible(_heading3("No products Matched Your search for \"mm*ana98\""));
  _assertVisible(_div("/Try Another Search/"));
//verify search box
//enter the value in 404 text field and hit enter
_setValue(_textbox("q", _in(_div("Try Another Search!"))), "baseball");
_click(_submit("simplesearch", _in(_div("Try Another Search!"))));
_wait(4000);

//verify search result
_assertVisible(_heading1("BASEBALL TROPHIES", _in(_div("ttt bigtext"))));
_assertVisible(_div("main-content"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216354/216629/216633","Verify the functionality of deselecting refinements in By Award type  left nav - category search - refinements/Verify the functionality of 'REFINE SEARCH' button in Search results page in mobile/Verify the functionality of Award type refinement in Refine Search in Mobile");
$t.start();
try
{

////navigate to sub category page	
//NavigatetoSubcat($details[0][2]);
//
//var $ExpectedApplied=new Array();
//
//var $refinements=_collectAttributes("_heading3","/toggle/","sahiText",_in(_div("refinements")));
//
//for (var $i=0;$i<$refinements.length;$i++)
//	{
//	
//	if ($refinements[$i]=="BY TYPE")
//		{
//		var $Filters=_collectAttributes("_link","/Refine by/","sahiText",_in(_div("refinement topLevelCategory")));
//		_click(_link($Filters[0]));	
//		$ExpectedApplied.push($Filters[0]+" x");
//		}
//	else if ($refinements[$i]=="BY PRICE")
//		{
//		var $Filters=_collectAttributes("_link","/Refine by/","sahiText",_in(_div("refinement ")));
//		_click(_link($Filters[0]));	
//		$ExpectedApplied.push($Filters[0]+" x");
//		}
//	else if ($refinements[$i]=="BY WEIGHT")
//		{
//		var $Filters=_collectAttributes("_link","/Refine by/","sahiText",_in(_div("refinement weight")));
//		_click(_link($Filters[0]));	
//		$ExpectedApplied.push($Filters[0]+" x");	
//		}
//	else
//		{
//		_assert(false,"to verify for size refinement");
//		}
//	}
//
//	var $ActualApplied=_collectAttributes("_span","/breadcrumb-refinement-value/","sahiText",_in(_div("breadcrumb"))).toString();
//	_assertEqualA($ExpectedApplied.sort(),$ActualApplied.sort());
	
	
	//navigate to sub category page	
	NavigatetoSubcat($details[0][2]);
	
var $refinements=_collectAttributes("_div","/toggle/","sahiText",_in(_div("refinements")))

for (var $i=1;$i<=$refinements.length;$i++)
	{
	
	if ($refinements[$i]=="By Type")
		{
		var $Filters=_collectAttributes("_link","/Refine by/","sahiText",_in(_div("refinement topLevelCategory")));
		for (var $j=0;$j<3;$j++)
			    {
		                _click(_link($Filters[$j]));	
		                _wait(2000);
				       _click(_link("Clear"));
		        }
		}
	
	else if ($refinements[$i]=="By Size")
		{
		var $Filters=_collectAttributes("_link","/swatch/","sahiText",_in(_list("clearfix swatches size")));
		for (var $k=0;$k<3;$k++)
		     {
	                _click(_link($Filters[$k]));	
	                _wait(2000);
			        _click(_link("Clear"));
	        }
		}
	
	else if ($refinements[$i]=="By Price")
	{
		var $Filters=_collectAttributes("_link","/Refine by/","sahiText",_in(_div("refinement ")));
		for (var $k=0;$k<3;$k++)
		     {
	                _click(_link($Filters[$k]));	
	                _wait(2000);
			        _click(_link("Clear"));
	        }
	}
	
	else if ($refinements[$i]=="By Min Price")
		{
			var $Filters=_collectAttributes("_link","/Refine by/","sahiText",_in(_div("refinement categoryMinPrice")));
			for (var $l=0;$l<3;$l++)
			{
	                _click(_link($Filters[$l]));	
	                _wait(2000);
			        _click(_link("Clear"));
		     }
		}
		
else if ($refinements[$i]=="By Max Price")
		{
		var $Filters=_collectAttributes("_link","/Refine by/","sahiText",_in(_div("refinement categoryMaxPrice")));
		for (var $m=0;$m<3;$m++)
	     	{
                _click(_link($Filters[$m]));	
                _wait(2000);
		        _click(_link("Clear"));
	        }
		}
	
else if ($refinements[$i]=="By Trophies Price Refinement")
		{
		   var $Filters=_collectAttributes("_link","/Refine by/","sahiText",_in(_div("refinement ")));
			for (var $n=0;$n<3;$n++)
			    {
	                _click(_link($Filters[$n]));	
	                _wait(2000);
		     	    _click(_link("Clear"));
		        }
		}
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("216167","Verify the navigation of pagination links in CYA/Sport page search in desktop/tablet");
$t.start();
try
{
	//navigate to sub cat page
	search("medals");
	_wait(5000);
	//verification of pagination 
	pagination();

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("216170/222317","Verify the display of options in Show dropdown/display of # of search results in CYA/Sport page search in desktop/tablet");
$t.start();
try
{

     //navigate to sub cat page
     NavigatetoSubcat($details[0][2
                                  ]);
	_click(_link("Baseball Trophies", _in(_div("refinements refinement"))));
	
if (_isVisible(_div("items-per-page")) && _isVisible(_div("items-per-page",_under(_list("search-result-items")))))
	{
	
	//items per page in header		
	_assertVisible(_div("items-per-page"));
	//Items per page in footer	
	_assertVisible(_div("items-per-page", _under(_list("search-result-items"))));
	
	//options in show drop down
	_assertVisible(_select("page-show"));	
	var $itemsdisplayed=_getText(_select("page-show"));

	for (var $i=0;$i<$itemsdisplayed.length;$i++)
	{
		_setSelected(_select("page-show"),$itemsdisplayed[$i]);
		_assertEqual($itemsdisplayed[$i],_getSelectedText(_select("page-show")),"Selected option is displaying properly");
		_assertEqual($itemsdisplayed[$i],_getSelectedText(_select("page-show",_under(_list("search-result-items")))),"Selected option is displaying properly at footer");
		ItemsperPage($itemsdisplayed[$i]);
	}
	
	}

else
	{
	
	_log("Single page displayed");
	var $totalitems=_extract(_getText(_div("pagination")), "/of (.*) </", true).toString();
	var $totalitemsdisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
	_assertEqual($totalitems,$totalitemsdisplayed);
	
	var $maxItems=parseFloat(_extract(_getText(_div("pagesnums float-left")), "/-(.*)of/", true)).toString();
	var $miniItems=parseFloat(_extract(_getText(_div("pagesnums float-left")), "/(.*)-/", true)).toString();

      $TotalProduct_Perpage=$maxItems-$miniItems
	var $totalitemsdisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")))-1;
	_assertEqual($TotalProduct_Perpage,$totalitemsdisplayed);
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()