_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Drop4_sanity.xls");

var $drop4_data=_readExcelFile("Drop4_sanity.xls","Data");
var $drop4_address=_readExcelFile("Drop4_sanity.xls","Address");
var $payment_details=_readExcelFile("Drop4_sanity.xls","Payment");
var $shippingdata=_readExcelFile("../../Checkout/ShippingPage/ShippingPage.xls","ShippingData");

ClearCookies();
_setAccessorIgnoreCase(true); 
cleanup();

var $currentmonth;
//Ashley Phillips 817-478-1802 CrownTrophiesE@gmail.com VERSANTE DRIVE 6287 RENWOOD DR FORT WORTH, TX 76140-9403 United States
var $shippingaddress=$drop4_address[0][1]+" "+$drop4_address[0][2]+" "+$drop4_address[0][12]+" "+$uId+" "+$drop4_address[0][4]+" "+$drop4_address[0][3]+" "+$drop4_address[0][7]+", "+$drop4_address[0][11]+" "+$drop4_address[0][8]+" "+$drop4_address[0][5];
//Ashley Phillips VERSANTE DRIVE 6287 RENWOOD DR FORT WORTH, TX 76140-9403 United States
var $shippingaddressComp=$drop4_address[0][1]+" "+$drop4_address[0][2]+" "+$drop4_address[0][4]+" "+$drop4_address[0][3]+" "+$drop4_address[0][7]+", "+$drop4_address[0][11]+" "+$drop4_address[0][8]+" "+$drop4_address[0][5];
//Ashley Phillips District 1 1656 Union Street Eureka, CA 95502 United States
var $billingaddress=$drop4_address[0][1]+" "+$drop4_address[0][2]+" "+$drop4_address[0][3]+" "+$drop4_address[0][4]+" "+$drop4_address[0][7]+", "+$drop4_address[0][11]+" "+$drop4_address[0][8]+" "+$drop4_address[0][5];	
var $ShippingMethodShippingpage;

_log("*********************** For Register User *******************************");

//click on login link
	if (isMobile())
	{
	 _click(_link("/mobile-show/", _in(_div("user-info"))));
	}
	else
		{
		 _click(_link("/desktop-show/", _in(_div("user-info"))));
		 //Visibility of overlay
		 _assertVisible(_div("dialog-container"));
		}

//login to the application
login();
_wait(5000);
//navigate to my account landing page
//NavigatetoAccountLanding()
_click(_link("Your Orders"));
_click(_link("My Account", _in(_div("main-content"))));
//create address
_click(_link("ADDRESSES"));
//delete existing address
deleteAddress();
//click on add new address 
 _click(_link("/New Address/"));
//Add address
addAddress($shippingdata,10);
//click on create address
_click(_submit("dwfrm_profile_address_create"));
//click on continue in address
if (_isVisible(_div("/invalid-address/")))
	 {
	 _click(_link("Continue"));
	 }
_wait(10000)

var $t = _testcase("219513","Verify the calculation functionality in Order Summary pane on shipping page in Application as a Registered user.");
$t.start();
try
{
	NavigatetoHomepage();
	_click(_image("Crown Awards"));
	_wait(5000);
	//navigate to cart page
	navigateToCart($drop4_data[0][0],$drop4_data[0][1]);
	//price in cart page
	var $totalpriceincart=_getText(_div("order-value", _in(_listItem("OrderTotalValue"))));
	//capturing price
	var $lineitempriceincart=_collectAttributes("_span","price-sales","sahiText", _in(_div("/item-price box/")));
	//click mini cart pink
	_click(_link("/mini-cart-link-checkout-link/"));
	//should navigate to shipping page
	_assertVisible(_heading2("Shipping Address"));
	//verifying the order summary section 
	_assertVisible(_heading3($drop4_data[3][2]));
	_assertVisible(_div($drop4_data[4][2]));
	_assertVisible(_div($drop4_data[5][2]));
	//_assertVisible(_listItem($drop4_data[6][2]));
	_assertVisible(_div($drop4_data[7][2]));
	//capturing price
	var $lineitempriceinshipping=_collectAttributes("_span","price-sales","sahiText", _in(_div("box cartunit")));
	_assertEqualArrays($lineitempriceincart.sort(),$lineitempriceinshipping.sort());
	//price in shipping page
	var $priceinshippingpage=_getText(_div("order-value", _in(_listItem("OrderTotalNum"))));
	_assertEqual($totalpriceincart,$priceinshippingpage);	


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("219484","Verify the navigation to  'SHIPPING' page through Global HEader  in Application as a Register user.");
$t.start();
try
{

NavigatetoHomepage();
//tempcode
_expectConfirm("/Are you sure you want to leave this page/", true);
//click on checkout button in global header
_click(_link("/mini-cart-link-checkout-link/"));
//should navigate to shipping page
_assertVisible(_heading2("Shipping Address"));
//_assertVisible(_heading2($drop4_data[0][2], _in(_div("login-box"))));
//_assertVisible(_heading2($drop4_data[1][2], _in(_div("login-box login-account"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("219485","Verify the functionality of 'EDIT' link in Cart summary Section in right nav on shipping page in Application as a Registered user.");
$t.start();
try
{

//var $productnameincartsummary=_getText(_link("/(.*)/", _in(_div("/mini-cart-names/"))));
var $lineitempriceinshipping=_collectAttributes("_span","price-sales","sahiText", _in(_div("box cartunit")));

//click on edit cart link in global header
_click(_link("section-header-note"));
//tempcode needs to be deleted
_expectConfirm("/Are you sure you want to leave this page/", true);
//should navigate to shopping cart page
_assertVisible(_div("Shopping Cart"));
var $productnameincart=_extract(_getText(_link("/(.*)/", _in(_listItem("name")))),"/(.*) -/",true);
var $lineitempriceincart=_collectAttributes("_span","price-sales","sahiText", _in(_div("/item-price box/")));


//_assertEqual($productnameincartsummary,$productnameincart);
_assertEqualArrays($lineitempriceincart.sort(),$lineitempriceinshipping.sort());


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("219488/219514/219526","Verify the functionality of 'CONTINUE' button on shipping page in Application as a Registered user./Verify the functionality of the Checkbox 'Use this address for Billing' on shipping page in Application as a Anonymous user.");
$t.start();
try
{

NavigatetoHomepage();
//tempcode
_expectConfirm("/Are you sure you want to leave this page/", true);

//navigate back to shipping page by clicking checkout button in global header
_click(_link("/mini-cart-link-checkout-link/"));	
	
if (_isVisible(_select("dwfrm_singleshipping_addressList")))
{
//select address from drop down
_setSelected(_select("dwfrm_singleshipping_addressList"),"1");
}

//address proceed
adddressProceed();

_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
//use same as billing address check box should be checked by default
_assertEqual(true,_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked);
//billing fields should not visible
_assertNotVisible(_div("billing-fields"));

////un checking the check box to enter billing address
//if (_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked)
//	{
//	_uncheck(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"))
//	}
//
////billing fields should visible
//_assertVisible(_div("/billing-fields/"));
//
////enter billing address
//BillingAddress($drop4_address,1);

//selecting shipping method
_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));

//event calendar
eventcalendar();

//click on continue in shipping page
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));

adddressProceed();

//should navigate to billing/payment page
_assertEqual($drop4_data[2][2], _getText(_div("address-header-main")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("219482/219528","Verify the Functionality of the continue Shopping button in Order Confirmation page./Verify the functionality of check box under 'SELECT SHIPPING METHOD' Section on shipping page in Application as a Registered user.");
$t.start();
try
{

//navigate back to shipping page
_click(_div("/Shipping & Delivery/",_near(_div("step-3 active"))))
	
////shipping form should display
//shippingAddress($drop4_address,0);
//
//if (!_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked)
//	{
//	_uncheck(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"))
//	}
////enter billing address
//BillingAddress($drop4_address,1);

//shipping method selection verification
var $shippingmethods=_collectAttributes("_div","ship-details","sahiText",_in(_div("ship-table")));

for (var $i=0;$i<$shippingmethods.length;$i++)
	{
	
	_check(_radio("input-radio shipping-methods-length",_near(_label($shippingmethods[$i]))));
	_wait(5000);
	_assertEqual(true,_radio("input-radio shipping-methods-length",_near(_label($shippingmethods[$i]))).checked);
	_wait(10000);
	//selected shipping method should reflect in order summary section in shipping page  
	_assertVisible(_listItem($shippingmethods[$i]+" "+"Shipping:", _in(_div("box ordersums"))));
	}

//reseting the first shipping value
_check(_radio("input-radio shipping-methods-length",_near(_label($shippingmethods[0]))));

//event calendar
eventcalendar();

//click on continue in shipping page
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
adddressProceed();

//enter payment details
PaymentDetails($payment_details,0);

//check the check box
//_check(_checkbox("/dwfrm_billing_confirm/"));

//place order button
_click(_submit("dwfrm_billing_save"));

//order confirmation heading
_assertVisible(_heading1($drop4_data[0][3]));

var $orderno=_getText(_span("order-number")).split(" ")[2];
_log($orderno);

var $orderkkkk=_getText(_span("order-number"))
_log($orderkkkk);

//click on continue shopping button in ORDER CONFIRMATION page
_click(_link("/(.*)/",_in(_span("continue-shopping desktop-show"))));
//should navigate to home page
//verify the home page
_assertNotVisible(_div("breadcrumb"));
_assertVisible(_div("home-main-right"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


//clearing cart items
ClearCartItems();

var $t = _testcase("219512/219512/222568","Verify the functionality of the Checkbox 'Use this address for Billing' on shipping page in Application as a Registered user.//Verify the Selection of Use as Billing address check box on shipping page load");
$t.start();
try
{

//navigate to cart page
navigateToCart($drop4_data[0][0],$drop4_data[0][1]);
var $totalpriceincart=_getText(_div("order-value", _in(_listItem("OrderTotalValue"))));
//capturing price
var $lineitempriceincart=_collectAttributes("_span","price-sales","sahiText", _in(_div("/item-price box/")));
//click mini cart link
_click(_link("/mini-cart-link-checkout-link/"));

if (_isVisible(_select("dwfrm_singleshipping_addressList")))
{
//select address from drop down
_setSelected(_select("dwfrm_singleshipping_addressList"),"1");
}

//address proceed
adddressProceed();

////shipping form should display
//shippingAddress($drop4_address,0);
_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
//Use shipping address as billing checkbox should be checked by default
_assertEqual(true,_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked);

//selecting shipping method
_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));

//event calendar
eventcalendar();
//price in shipping page
var $priceinshippingpage=_getText(_div("order-value", _in(_listItem("OrderTotalNum"))));

//click on continue button in shipping page
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
adddressProceed();
//should navigate to billing/payment page
_assertEqual($drop4_data[2][2], _getText(_div("address-header-main")));
////comparing address
//_assertEqual($shippingaddress,_getText(_div("address-content", _in(_div("billing-address-section")))));
//_assertEqual($shippingaddressComp,_getText(_div("address-content", _in(_div("shipping-address-section")))));
//line item total in payment page
var $lineitemtotalinpaymentpage=_collectAttributes("_span","price-sales","sahiText", _in(_div("box cartunit")));
//comparing arrays
_assertEqualArrays($lineitempriceincart.sort(),$lineitemtotalinpaymentpage.sort());
//entering payment details
//enter payment details
PaymentDetails($payment_details,0);
//check the check box
//_check(_checkbox("/dwfrm_billing_confirm/"));
//place order button
_click(_submit("dwfrm_billing_save"));
//order confirmation page
_assertVisible(_heading1($drop4_data[0][3]));
_assertVisible(_div($drop4_data[1][3]));
_assertVisible(_span($drop4_data[2][3]));
_assertVisible(_span($drop4_data[3][3]));
_assertVisible(_span($drop4_data[4][3]));

var $orderno=_getText(_span("order-number")).split(" ")[2];
_log($orderno);

var $orderkkkk=_getText(_span("order-number"));
_log($orderkkkk);

//capturing price
var $TotalpriceinOCP=_getText(_span("order-price")).split(" ")[1];

//comparing price
_assertEqual($priceinshippingpage,$TotalpriceinOCP);


////ashley phillips versante drive 6287 renwood dr fort worth, TX 76140-9403 United States
//var $shippingaddressinOCPpage=$drop4_address[0][1]+" "+$drop4_address[0][2]+" "+$drop4_address[0][4]+" "+$drop4_address[0][3]+" "+$drop4_address[0][7]+", "+$drop4_address[0][11]+" "+$drop4_address[0][8]+" "+$drop4_address[0][5];
////(817) 478-1802 crowntrophiese@gmail.com versante drive 6287 renwood dr FORT WORTH, TX 76140-9403 United States
//var $billingaddressinOCPpage=$drop4_address[0][13]+" "+$uId+" "+$drop4_address[0][4]+", "+$drop4_address[0][3]+" "+$drop4_address[0][7]+", "+$drop4_address[0][11]+" "+$drop4_address[0][8]+" "+$drop4_address[0][5];
//
//
////comparing addresses
//_assertEqual($shippingaddressinOCPpage,_getText(_div("address")));
//_assertEqual($billingaddressinOCPpage,_getText(_div("mini-address-location")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


//clean up function
cleanup();

//############ placing order from checkout login page ##############################

var $t = _testcase("221548","Verify if the user can Login through checkout Login section.");
$t.start();
try
{

//navigate to cart page
navigateToCart($drop4_data[0][0],$drop4_data[0][1]);
var $totalpriceincart=_getText(_div("order-value", _in(_listItem("OrderTotalValue"))));
//capturing price
var $lineitempriceincart=_collectAttributes("_span","price-sales","sahiText", _in(_div("/item-price box/")));
//click mini cart link
_click(_link("/mini-cart-link-checkout-link/"));

//verifying loginn section
//_assertVisible(_heading2("LOGIN"));
//_assertVisible(_div("/returning-customers clearfix/"));
//_assertVisible(_heading2("CHECKOUT AS GUEST"));
//_assertVisible(_div("login-box-content clearfix"));

//login from checkout shipping page
//_setValue(_textbox("/dwfrm_login_checkout_username/"),$uId);
//_setValue(_password("/dwfrm_login_checkout_password/"),$pwd);
//_click(_submit("/dwfrm_login/"));

_assertVisible(_div("checkloglabel"));
_assertVisible(_link("Log In", _in(_div("user-section checkoutlogin"))));
//click
_click(_link("Log In", _in(_div("user-section checkoutlogin"))));

//login
_setValue(_textbox("/dwfrm_login_username/"), $uId);
_setValue(_password("/dwfrm_login_password/"), $pwd);

   if (_isVisible(_submit("/dwfrm_login/", _in(_div("inner-container")))))
   {
     _click(_submit("/dwfrm_login/", _in(_div("inner-container"))));
   }
  else
   {
       _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
   }

//login section should not visible
_assertNotVisible(_div("/returning-customers clearfix/"));

//shipping form should display
//shippingAddress($drop4_address,0);

if (_isVisible(_select("dwfrm_singleshipping_addressList")))
{
//select address from drop down
_setSelected(_select("dwfrm_singleshipping_addressList"),"1");
}

//address proceed
adddressProceed();

_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
//Use shipping address as billing checkbox should be checked by default
_assertEqual(true,_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked);

//price in shipping page
var $priceinshippingpage=_getText(_div("order-value", _in(_listItem("OrderTotalNum"))));

//Selecting shipping method
_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));

//event calendar
eventcalendar();

//click on continue button in shipping page
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
adddressProceed();
//should navigate to billing/payment page
_assertEqual($drop4_data[2][2], _getText(_div("address-header-main")));
////comparing address
//_assertEqual($shippingaddress,_getText(_div("address-content", _in(_div("billing-address-section")))));
//_assertEqual($shippingaddressComp,_getText(_div("address-content", _in(_div("shipping-address-section")))));
//line item total in payment page
var $lineitemtotalinpaymentpage=_collectAttributes("_span","price-sales","sahiText", _in(_div("box cartunit")));
//comparing arrays
_assertEqualArrays($lineitempriceincart.sort(),$lineitemtotalinpaymentpage.sort());
//entering payment details
//enter payment details
PaymentDetails($payment_details,0);
//check the check box
//_check(_checkbox("/dwfrm_billing_confirm/"));
//place order button
_click(_submit("dwfrm_billing_save"));
//order confirmation page
_assertVisible(_heading1($drop4_data[0][3]));
_assertVisible(_div($drop4_data[1][3]));
_assertVisible(_span($drop4_data[2][3]));
_assertVisible(_span($drop4_data[3][3]));
_assertVisible(_span($drop4_data[4][3]));

var $orderno=_getText(_span("order-number")).split(" ")[2];
_log($orderno);

var $orderkkkk=_getText(_span("order-number"));
_log($orderkkkk);

//capturing price
var $TotalpriceinOCP=_getText(_span("order-price")).split(" ")[1];

////comparing price
//_assertEqual($priceinshippingpage,$TotalpriceinOCP);
////ashley phillips versante drive 6287 renwood dr fort worth, TX 76140-9403 United States
//var $shippingaddressinOCPpage=$drop4_address[0][1]+" "+$drop4_address[0][2]+" "+$drop4_address[0][4]+" "+$drop4_address[0][3]+" "+$drop4_address[0][7]+", "+$drop4_address[0][11]+" "+$drop4_address[0][8]+" "+$drop4_address[0][5];
////(817) 478-1802 crowntrophiese@gmail.com versante drive 6287 renwood dr FORT WORTH, TX 76140-9403 United States
//var $billingaddressinOCPpage=$drop4_address[0][13]+" "+$uId+" "+$drop4_address[0][4]+", "+$drop4_address[0][3]+" "+$drop4_address[0][7]+", "+$drop4_address[0][11]+" "+$drop4_address[0][8]+" "+$drop4_address[0][5];
//
////comparing addresses
//_assertEqual($shippingaddressinOCPpage,_getText(_div("address")));
//_assertEqual($billingaddressinOCPpage,_getText(_div("mini-address-location")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()
/*
//################## Placing order with deleviry date equal to event date ###############
var $t = _testcase("12345","checking event calender scenarios");
$t.start();
try
{

//navigate to cart page
navigateToCart($drop4_data[0][0],$drop4_data[0][1]);
//click mini cart link
_click(_link("/mini-cart-link-checkout-link/"));
//shipping form should display
//shippingAddress($drop4_address,0);

if (_isVisible(_select("dwfrm_singleshipping_addressList")))
{
//select address from drop down
_setSelected(_select("dwfrm_singleshipping_addressList"),"1");
}
else
	{
	//shipping form should display
	shippingAddress($drop4_address,0);
	}


//address proceed
adddressProceed();
_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
//Use shipping address as billing checkbox should be checked by default
_assertEqual(true,_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked);
//selecting shipping method
_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
//should select the date 
var $Monthinshippingmethod=parseInt(_getText(_div("/delevery-date/", _in(_div("shp-details cell")))).split("-")[0]);
//var $dateinshippingmethod=_extract(_getText(_div("/delevery-date/", _in(_div("shp-details cell")))).split("-")[1],"/[0](.*)/",true).toString();
var $dateinshippingmethod=parseInt(_getText(_div("/delevery-date/", _in(_div("shp-details cell")))).split("/")[1],"/[0](.*)/").toString();
_log("Date in shipping method:"+$dateinshippingmethod);
	
	for (var $j=0;$j<$eventdate.length;$j++)
		{
		
		 _log($j);
		 if ($Monthinshippingmethod==$j)
			{
				$j=$j-1;
				$currentmonth=$eventdate[$j][0];
				_log($currentmonth)
				break;
			}
		
		}
	
//click on event date text box
_mouseOver(_textbox("dwfrm_singleshipping_datePicker_datePickerSelection"));
var $monthToBeSelected=_getText(_span("/(.*)/", _in(_div("ui-datepicker-title"))));
_log($monthToBeSelected);

while ($currentmonth!=$monthToBeSelected)
{
	_click(_span("Next"));
	$monthToBeSelected=_getText(_span("/(.*)/", _in(_div("ui-datepicker-title"))));
	_log("Month to be selected:"+$monthToBeSelected);
}

_assertEqual($currentmonth,$monthToBeSelected);
//click current date
_click(_link($dateinshippingmethod,_in(_table("ui-datepicker-calendar"))));

//fetching event date
var $eventdate=_getText(_textbox("dwfrm_singleshipping_datePicker_datePickerSelection"));
_log($eventdate);

//click on continue button in shipping page
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	
adddressProceed();

//enter payment details
PaymentDetails($payment_details,0);
//check the check box
//_check(_checkbox("/dwfrm_billing_confirm/"));
//place order button
_click(_submit("dwfrm_billing_save"));

//order confirmation page
_assertVisible(_heading1($drop4_data[0][3]));
_assertVisible(_div($drop4_data[1][3]));
_assertVisible(_span($drop4_data[2][3]));
_assertVisible(_span($drop4_data[3][3]));
_assertVisible(_span($drop4_data[4][3]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()
*/

_log("**********************Guest USer***********************************************")

cleanup();

var $t = _testcase("221272","Verify the EDIT link Functionlity in the Cart Page for the Single Trohy Line item .");
$t.start();
try
{

//navigate to cart page with single 
navigateToCart($drop4_data[0][4],$drop4_data[0][1]);
//click mini cart link
_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));
//product name should be same
//_assertEqual($prodname,$pName);
//click on edit item in cart page
_click(_link("Edit Item[1]"));
//should navigate to PDP page
_assertVisible(_heading2("product-name"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("221549/221549","Verify the Remove Link functionality for a Single trophy Product in cart Page/Verify the Remove Link functionality for a Single trophy Product in cart Page");
$t.start();
try
{

//click mini cart pink
_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));

var $countofproducts=_count("_div","/cartprod/",_in(_div("CartTableLeft ")));

if ($countofproducts==1)
	{
	_click(_submit("Remove[1]"));
	//_click(_submit("/Remove/",_in(_div("/cart-row/"))));
	_assertNotVisible(_div("cart-row box-row"));
	}
	else
		{
		_click(_submit("Remove[1]"));
		//_click(_submit("/Remove/",_in(_div("/cart-row/"))));
		var $countofproductsafterdelete=_count("_div","/cartprod/",_in(_div("CartTableLeft ")));
		_assertEqual($countofproducts-1,$countofproductsafterdelete);
		}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("219340/219531","Verify the application behavior on click of Checkout button in the cart Page displayed in the Right Pane./Verify the calculation functionality in Cart Summary pane on shipping page in Application as a Anonymous user.");
$t.start();
try
{

//navigate to cart page
navigateToCart($drop4_data[0][0],$drop4_data[0][1]);
//capturing price
var $lineitempriceincart=_collectAttributes("_span","price-sales","sahiText", _in(_div("/item-price box/")));
//click mini cart link
_click(_link("/mini-cart-link-checkout-link/"));
//should navigate to shipping page
_assertVisible(_heading2("Shipping Address"));
//_assertVisible(_heading2($drop4_data[0][2], _in(_div("login-box"))));
//_assertVisible(_heading2($drop4_data[1][2], _in(_div("/login-box login-account/"))));
_assertVisible(_div("checkloglabel"));

//verifying the order summary section  in shipping page
_assertVisible(_heading3($drop4_data[3][2]));
_assertVisible(_div($drop4_data[4][2]));
_assertVisible(_div($drop4_data[5][2]));
//_assertVisible(_listItem($drop4_data[6][2]));
_assertVisible(_div($drop4_data[7][2]));
//capturing price
var $lineitempriceinshipping=_collectAttributes("_span","price-sales","sahiText", _in(_div("box cartunit")));
_assertEqualArrays($lineitempriceincart,$lineitempriceinshipping);
//_assertEqual($orderTotal,);
//should verify cost
_assertVisible(_div("$"+$orderTotal, _in(_div("box ordersumsnum"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("219530/219898","Verify the functionality of 'CONTINUE &gt;' button on shipping page in Application as a Anonymous user./Veirfy the Application navigattion to Payment Page");
$t.start();
try
{

//shipping form should display
shippingAddress($drop4_address,0);

//un checking the check box to enter billing address
if (_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked)
	{
	_uncheck(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"))
	}

//reload the page 
window.location.href("https://development-webstore-crownawards.demandware.net/s/MAI/checkout");

//enter billing address
BillingAddress($drop4_address,1);

//address proceed
adddressProceed();

//event calendar
eventcalendar();

//Selecting shipping method
_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));

//click on continue in shipping page
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));

//should navigate to billing/payment page
_assertEqual($drop4_data[2][2], _getText(_div("address-header-main")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

//navigateToCart($drop4_data[0][4],$drop4_data[0][1]);

var $t = _testcase("219697/219367/219387","Veirfy the navigation to Shipping page using the mini cart/Verify the application behvaior on click of checkout button displayed at right top corner of the mini cart  section./Verify the application behavior on click of Checkout button displayed at the bottom in mini cart fly out");
$t.start();
try
{

//navigate to home page
NavigatetoHomepage();

//tempcode needs to be deleted
_expectConfirm("/Are you sure you want to leave this page/", true);

//click on checkout link in mini cart
_mouseOver(_link("/mini-cart-link/"));
//click on top checkout button in mini cart overlay
_assertVisible(_link("/(.*)/", _in(_div("miniCartCheckout"))));
_click(_link("/(.*)/", _in(_div("miniCartCheckout"))));

//should navigate to shipping page
_assertVisible(_heading2("Shipping Address"));
//_assertVisible(_heading2($drop4_data[0][2], _in(_div("login-box"))));
//_assertVisible(_heading2($drop4_data[1][2], _in(_div("/login-box login-account/"))));

_assertVisible(_div("checkloglabel"));

//navigate to home page
NavigatetoHomepage();

//tempcode needs to be deleted
_expectConfirm("/Are you sure you want to leave this page/", true);

//click on checkout link in mini cart
_mouseOver(_link("/mini-cart-link/"));
//click on bottom checkout button in mini cart overlay
_assertVisible(_link("/(.*)/", _in(_div("miniCartCheckout",_near(_div("miniCartViewCart"))))));
_click(_link("/(.*)/", _in(_div("miniCartCheckout",_near(_div("miniCartViewCart"))))));

//should navigate to shipping page
_assertVisible(_heading2("Shipping Address"));
//_assertVisible(_heading2($drop4_data[0][2], _in(_div("login-box"))));
//_assertVisible(_heading2($drop4_data[1][2], _in(_div("/login-box login-account/"))));
_assertVisible(_div("checkloglabel"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("219880","Verify the application navigation to shipping page from cart page");
$t.start();
try
{

//navigate to home page
NavigatetoHomepage();

//tempcode needs to be deleted
_expectConfirm("/Are you sure you want to leave this page/", true);

//click on checkout link in mini cart
_mouseOver(_link("/mini-cart-link/"));
//click link on view cart link in mini cart section
_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));
//click on checkout button in cart page
_click(_submit("dwfrm_cart_checkoutCart", _in(_div("/rightcartcol/"))));

//should navigate to shipping page
_assertVisible(_heading2("Shipping Address"));
//_assertVisible(_heading2($drop4_data[0][2], _in(_div("login-box"))));
//_assertVisible(_heading2($drop4_data[1][2], _in(_div("/login-box login-account/"))));
_assertVisible(_div("checkloglabel"));

//navigate to home page
NavigatetoHomepage();

//tempcode needs to be deleted
_expectConfirm("/Are you sure you want to leave this page/", true);

//click checkout button displayed at right top corner of the mini cart  section
_click(_link("/(.*)/",_in(_div("miniCartCheckout"))));

//should navigate to shipping page
_assertVisible(_heading2("Shipping Address"));
//_assertVisible(_heading2($drop4_data[0][2], _in(_div("login-box"))));
//_assertVisible(_heading2($drop4_data[1][2], _in(_div("/login-box login-account/"))));
_assertVisible(_div("checkloglabel"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("219500","Verify the functionality of 'EDIT' link in Cart summary Section in right nav on payment  page in Application as a Guest user.");
$t.start();
try
{

NavigatetoHomepage();	
//click mini cart link
_click(_link("/mini-cart-link-checkout-link/"));
	
//var $productnameincartsummary=_getText(_link("/(.*)/", _in(_div("/mini-cart-names/"))));
var $lineitempriceinshipping=_collectAttributes("_span","price-sales","sahiText", _in(_div("box cartunit")));

//click on edit cart link in global header
_click(_link("section-header-note"));
//tempcode needs to be deleted
_expectConfirm("/Are you sure you want to leave this page/", true);
//should navigate to shopping cart page
_assertVisible(_div("Shopping Cart"));
var $productnameincart=_extract(_getText(_link("/(.*)/", _in(_listItem("name")))),"/(.*) -/",true);
var $lineitempriceincart=_collectAttributes("_span","price-sales","sahiText", _in(_div("/item-price box/")));
//comparing product name and price in cart and order summary section in shipping page
//_assertContainsText($productnameincartsummary,_link("/(.*)/", _in(_div("name"))));
_assertEqualArrays($lineitempriceincart.sort(),$lineitempriceinshipping.sort());

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("219894/219529","Verify the Functionality of the continue Shopping button in Order Confirmation page./Verify the functionality of checkbox  under 'SELECT SHIPPING METHOD' Section on shipping page in Application as a Anonymous user.");
$t.start();
try
{

//click mini cart link
_click(_link("/mini-cart-link-checkout-link/"));	
	
//shipping form should display
shippingAddress($drop4_address,0);
//
//if (!_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked)
//	{
//	_uncheck(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"))
//	}

//uncheck the checkbox
_uncheck(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));

//enter billing address
BillingAddress($drop4_address,1);

//address proceed
adddressProceed();

//shipping method selection verification
var $shippingmethods=_collectAttributes("_div","ship-details","sahiText",_in(_div("ship-table")));

for (var $i=0;$i<$shippingmethods.length;$i++)
	{
	
	_check(_radio("input-radio shipping-methods-length",_near(_label($shippingmethods[$i]))));
	_wait(5000);
	_assertEqual(true,_radio("input-radio shipping-methods-length",_near(_label($shippingmethods[$i]))).checked);
	_wait(5000);
//	//fetching price in shipping method
//	var $shippingmethodpriceWithoutextract=_getText(_div("ship-price",_near(_label($shippingmethods[$i]))));
//	_log($shippingmethodpriceWithoutextract);
//	//fetching price in shipping method with extract
//	var $shippingmethodprice=_extract(_getText(_div("ship-price",_near(_label($shippingmethods[$i])))),"/[$](.*)/",true);
//	_log("shipping method price="+$shippingmethodprice);
	//selected shipping method should reflect in order summary section in shipping page  
	_assertVisible(_listItem($shippingmethods[$i]+" "+"Shipping:", _in(_div("box ordersums"))));
	//fetching price in order summary section
	//var $shippingmethodpriceinOSS=_extract(_getText(_div("order-shipping  first ")),"/[$](.*)/",true);
	//total price in shipping page
//	var $priceinshippingpage=_extract(_getText(_div("order-value", _in(_listItem("OrderTotalNum")))),"/[$](.*)/",true);
//	var shippingmethodpriceinOSS;
//	
//	if (_isVisible($shippingmethodpriceWithoutextract,_in(_list("order-details-value"))))
//		{
//		var $priceinshippingpage=shippingmethodpriceinOSS;
//		}
//	
//	_log("shipping method price in ordr summary section="+$shippingmethodpriceinOSS);
//	_assertEqual($shippingmethodprice,$shippingmethodpriceinOSS);
//	
//	_log("total price in shipping page="+$priceinshippingpage);
//	var $totalshipprice=parseFloat($totalprice) + parseFloat($shippingmethodpriceinOSS);
//	round($totalshipprice,2);
//	_log("cart total price="+$totalprice);
//	_log("Calculating total shipping price="+$totalshipprice);
//	_assertEqual($totalshipprice,$priceinshippingpage)
	
	}

//reseting the first shipping value
_check(_radio("input-radio shipping-methods-length",_near(_label($shippingmethods[0]))));

var $ShippingMethodInShippingpage=_getText(_div("ship-details",_near(_radio("input-radio shipping-methods-length"))));

//event calendar
eventcalendar();

//click on continue in shipping page
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//enter payment details
PaymentDetails($payment_details,0);

//check the check box
//_check(_checkbox("/dwfrm_billing_confirm/"));

//place order button
_click(_submit("dwfrm_billing_save"));

var $orderno=_getText(_span("order-number")).split(" ")[2];
_log($orderno);

var $orderkkkk=_getText(_span("order-number"))
_log($orderkkkk);

//order confirmation page heading
_assertVisible(_heading1($drop4_data[9][2]));

//shipping method verification in order confirmation page
var $ShippingMethodInOcpPage=_getText(_div("value", _in(_div("shipping-method"))));

_assertEqual($ShippingMethodInShippingpage+" Shipping",$ShippingMethodInOcpPage);

//click on continue shopping button in ORDER CONFIRMATION page
_click(_link("/(.*)/",_in(_span("continue-shopping desktop-show"))));
//should navigate to home page
//verify the home page
_assertNotVisible(_div("breadcrumb"));
_assertVisible(_div("home-main-right"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

//clearing cart items
ClearCartItems();

var $t = _testcase("220088/219476/222571/222570","Verify the Functionality PLACE ORDER button Functrionality In Cart Page as Guest USER./Verify the functionality of the Checkbox 'Use this address for Billing' on shipping page in Application as a Anonymous user./Verify the Billing address update is seen in shipping page.");
$t.start();
try
{

//navigate to cart page
navigateToCart($drop4_data[0][0],$drop4_data[0][1]);
var $totalpriceincart=_getText(_div("order-value", _in(_listItem("OrderTotalValue"))));
//click mini cart link
_click(_link("/mini-cart-link-checkout-link/"));
//capturing price
var $lineitempriceinshipping=_collectAttributes("_span","price-sales","sahiText", _in(_div("box cartunit")));

//shipping form should display
shippingAddress($drop4_address,0);
//address proceed
adddressProceed();
_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
//Use shipping address as billing checkbox should be checked by default
_assertEqual(true,_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked);
//selecting shipping method
_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
//event calendar
eventcalendar();

//price in shipping page
var $priceinshippingpage=_getText(_div("order-value", _in(_listItem("OrderTotalNum"))));

//click on continue button in shipping page
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
adddressProceed();

//should navigate to billing/payment page
_assertEqual($drop4_data[2][2], _getText(_div("address-header-main")));
////comparing address
//_assertEqual($shippingaddress,_getText(_div("address-content", _in(_div("billing-address-section")))));
//_assertEqual($shippingaddressComp,_getText(_div("address-content", _in(_div("shipping-address-section")))));
//line item total in payment page
var $lineitemtotalinpaymentpage=_collectAttributes("_span","price-sales","sahiText", _in(_div("box cartunit")));
//comparing arrays
_assertEqualArrays($lineitempriceinshipping.sort(),$lineitemtotalinpaymentpage.sort());
//entering payment details
if(_isVisible(_div("gift-cert-used form-indent")))
	{
	_check(_checkbox("input-checkbox  required"));
	_click(_submit("continue-button handledmixedpopup"));
	}
else
	{
	//enter payment details
	PaymentDetails($payment_details,0);
	//check the check box
	//_check(_checkbox("/dwfrm_billing_confirm/"));
	//place order button
	_click(_submit("dwfrm_billing_save"));
	}


//order confirmation page
_assertVisible(_heading1($drop4_data[0][3]));
_assertVisible(_div($drop4_data[1][3]));
_assertVisible(_span($drop4_data[2][3]));
_assertVisible(_span($drop4_data[3][3]));
_assertVisible(_span($drop4_data[4][3]));

var $orderno=_getText(_span("order-number")).split(" ")[2];
_log($orderno);

var $orderkkkk=_getText(_span("order-number"))
_log($orderkkkk);

//capturing price
var $TotalpriceinOCP=_getText(_span("order-price")).split(" ")[1];

//comparing price
_assertEqual($priceinshippingpage,$TotalpriceinOCP);

////ashley phillips versante drive 6287 renwood dr fort worth, TX 76140-9403 United States
//var $shippingaddressinOCPpage=$drop4_address[0][1]+" "+$drop4_address[0][2]+" "+$drop4_address[0][4]+" "+$drop4_address[0][3]+" "+$drop4_address[0][7]+", "+$drop4_address[0][11]+" "+$drop4_address[0][8]+" "+$drop4_address[0][5];
////(817) 478-1802 crowntrophiese@gmail.com versante drive 6287 renwood dr FORT WORTH, TX 76140-9403 United States
//var $billingaddressinOCPpage=$drop4_address[0][13]+" "+$uId+" "+$drop4_address[0][4]+", "+$drop4_address[0][3]+" "+$drop4_address[0][7]+", "+$drop4_address[0][11]+" "+$drop4_address[0][8]+" "+$drop4_address[0][5];
//
////comparing addresses
//_assertEqual($shippingaddressinOCPpage,_getText(_div("address")));
//_assertEqual($billingaddressinOCPpage,_getText(_div("mini-address-location")));

//navigato to home page
NavigatetoHomepage();
//handling leave pop up
_expectConfirm("/Are you sure you want to leave this page/", true); 

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()



//######################### Functions #####################
function tempcode()
{
	
	var $nofoproducts=_count("_div","/mini-cart-product-list/",_in(_div("CartTableLeft")));
	
	for (var $i=0;i<$nofoproducts;$i++)
		{
		
		if (_isVisible(_div("N/A N/A",_in(_div("mini-cart-product-list box-row["+$i+"]")))))
			{
			_click(_link("Remove Item["+$i+"]"));
			//click on checkout button
			_expectConfirm("/Are you sure you want to leave this page/", true); 
			}
		
		}

}
