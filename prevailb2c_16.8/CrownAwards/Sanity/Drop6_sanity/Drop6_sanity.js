_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Drop6_sanity.xls");

var $drop6_data=_readExcelFile("Drop6_sanity.xls","Data");
var $contactus_sanity=_readExcelFile("Drop6_sanity.xls","Contactus");

ClearCookies();
_setAccessorIgnoreCase(true); 
cleanup();

var $t = _testcase("219208/219307","Verify the functionality of 'SUBMIT' button in Email Us section in Contact us page for desktop/tablet/Verify the UI of Thank you message after submitting the Email Us form for desktop/tablet");
$t.start();
try
{

//click on contact us link in footer
//_click(_link("Contact Us", _in(_div("footer-item-list"))));
_click(_link($drop6_data[2][0], _near(_div("Customer Service"))));
//entering details
contactusDetails($contactus_sanity,0);
//click on submit button
_click(_submit("dwfrm_contactus_send"))
//thank you message should display
_assertVisible(_div("confirmation-message"));
_assertVisible(_heading3($contactus_sanity[0][7]));
_assertEqual($contactus_sanity[0][8], _getText(_div("confirmation-message")));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("219334/219335","Verify the functionality of 'CONTINUE SHOPPING' button displayed in the FAQ page for desktop/tablet/Verify the functionality of 'CONTINUE SHOPPING' button displayed in the FAQ page for mobile");
$t.start();
try
{

//click on faq link in footer 	
_assertVisible(_link($drop6_data[0][0],_near(_div("Customer Service"))));
_click(_link($drop6_data[0][1],_near(_div("Customer Service"))));
//verifying FAQ link
_assertVisible(_div($drop6_data[2][1], _in(_div("breadcrumb"))));
_assertVisible(_heading1($drop6_data[0][2]));
//click on continue shopping link
_click(_link("/(.*)/", _in(_div("continue-shopping"))));
//verify the home page
_assertNotVisible(_div("breadcrumb"));
_assertVisible(_div("home-main-right"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("219339/219338","Verify the functionality of SEARCH FAQs search field in the FAQ page for mobile/Verify the functionality of SEARCH FAQs search field in the FAQ page for desktop/tablet");
$t.start();
try
{
	
//click on faq link in footer 	
_click(_link($drop6_data[0][1],_near(_div("Customer Service"))));
//visibility of search text field in faq page
_assertEqual($drop6_data[1][2], _getText(_div("searched-text")));
_assertVisible(_div("search-box-container"));
_assertVisible(_textbox("/q/",_in(_div("search-box-container"))));
_assertVisible(_submit("faqsearchbutton"));
//enter valid value in search field
_setValue(_textbox("/q/",_in(_div("search-box-container"))),$drop6_data[0][3]);
_click(_submit("faqsearchbutton"));
//should display question and answers according to search term
var $questions=_collectAttributes("_heading3","/q0/","sahiText",_in(_div("faq-data")));

for (var $i=0;$i<2;$i++)
	{
	_assertContainsText($drop6_data[0][3],_heading3($questions[$i]));
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("219347/219348","Verify the functionality of 'SUBMIT REQUEST' button in request a catalog page for desktop/tablet /Verify the functionality of 'SUBMIT REQUEST' button in request a catalog page for mobile");
$t.start();
try
{

//click on request a catalog link
_assertVisible(_link($drop6_data[3][0], _near(_div("footer-icons"))));
_click(_link($drop6_data[3][0], _near(_div("footer-icons"))));

//verifying request a catalog link
_assertVisible(_heading1($drop6_data[2][2]));
_assertVisible(_div($drop6_data[1][1], _in(_div("breadcrumb"))));
_assertVisible(_div("catalog-image-section"));
_assertVisible(_submit("dwfrm_catalogrequest_requestcatalog", _in(_div("catalog-image-section"))));


//filling data in request a catalog field
_setValue(_textbox("dwfrm_catalogrequest_firstname"),$drop6_data[0][4]);
_setValue(_textbox("dwfrm_catalogrequest_lastname"),$drop6_data[1][4]);
_setValue(_textbox("dwfrm_catalogrequest_organization"),$drop6_data[2][4]);
_setValue(_textbox("dwfrm_catalogrequest_address1"),$drop6_data[3][4]);
_setValue(_textbox("dwfrm_catalogrequest_address2"),$drop6_data[4][4]);
_setValue(_textbox("dwfrm_catalogrequest_zipcode"),$drop6_data[5][4]);
_setValue(_textbox("dwfrm_catalogrequest_city"),$drop6_data[6][4]);
_setSelected(_select("dwfrm_catalogrequest_country"),$drop6_data[7][4]);
_setSelected(_select("dwfrm_catalogrequest_states_state"),$drop6_data[8][4]);
_setValue(_textbox("dwfrm_catalogrequest_email"),$drop6_data[9][4]);
_setValue(_textbox("dwfrm_catalogrequest_emailconfirm"),$drop6_data[9][4]);
_setValue(_textbox("dwfrm_catalogrequest_phone"),$drop6_data[10][4]);
_setSelected(_select("dwfrm_catalogrequest_sportactivivty"),$drop6_data[11][4]);
_setSelected(_select("dwfrm_catalogrequest_quantity"),"0");
_check(_checkbox("dwfrm_catalogrequest_patches"));
_click(_submit("dwfrm_catalogrequest_requestcatalog"));

//verifying the submit request page
_assertVisible(_div($drop6_data[1][1], _in(_div("breadcrumb"))));
_assertVisible(_div("request-catalog-header"));
_assertVisible(_div("catalog-success"));
_assertVisible(_image("/(.*)/", _in(_div("/catalog-right-images-section/"))));



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("219325/219326/2201278","Verify the Functionality of 'CHECK STATUS' button in the Email Us section-contact us page as registered user for desktop/Verify the Functionality of 'CHECK STATUS' button in the Email Us section-contact us page as registered user for mobile");
$t.start();
try
{
	
navigateToCart($contactus_sanity[8][0],$contactus_sanity[8][1]);
//navigate to checkout page
_click(_link("/mini-cart-link-checkout-link/"));
//navigate to shipping page
_assertVisible(_heading2("Shipping Address"));

//enter shipping details
shippingAddress($contactus_sanity,5)
_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
//selecting shipping method
_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));

//event calendar
eventcalendar();

_click(_submit("/continue-button/"));

//proceed to next page
adddressProceed();
//entering the payment details
PaymentDetails($contactus_sanity,11);

//checking the checkbox
//_check(_checkbox("/dwfrm_billing_confirm/"));
//_assertEqual(true, _checkbox("/dwfrm_billing_confirm/").checked);
 
//_click(_div("/Payment & Place Order/"));
_click(_submit("/continue-button/"));
 
 
//storing the order no from order confirmation page 
 var $orderno=_extract(_getText(_span("order-number")),"/[#](.*)/",true).toString()
 _log($orderno);
 
 var $IDinocpPage=_getText(_div("mini-address-location")).split(" ")[2];
 var $billaddress=_getText(_div("mini-address-location"));
 var $shipaddress=_getText(_div("address"));
 var $price=_extract(_getText(_span("order-price")),"/: (.*)/",true);
 var $shippingmethod=_getText(_div("value",_in(_div("shipping-method"))));
 var $orderprice=_getText(_div("order-value",_in(_listItem("OrderTotalNum"))));
//card number
 var $verifyingcardno=_getText(_div("cc-number"));
 
 //delivery date
 var $deliverydate=_extract(_getText(_div("order-date")),"/: (.*)/",true);
 
 takescreenshot();
 
//click on contact us link in footer
_click(_link($drop6_data[2][0], _near(_div("Customer Service"))));
//breadcrumb   
_assertVisible(_div($drop6_data[1][1], _in(_div("breadcrumb"))));
//content-header
_assertVisible(_heading1("content-header"));
//display email us
_assertVisible(_div("email-us-header", _in(_div("email-us-section"))));
_assertVisible(_submit("dwfrm_contactus_send", _in(_div("email-us-section"))));

//entering valid values in all the fields
_setValue(_textbox("dwfrm_contactus_email"),$uId);
_setValue(_textbox("dwfrm_contactus_firstname"),$contactus_sanity[5][1]);
_setValue(_textbox("dwfrm_contactus_lastname"),$contactus_sanity[5][2]);
_setValue(_textbox("dwfrm_contactus_phone"),$contactus_sanity[5][9]);
_setSelected(_select("dwfrm_contactus_yourissues"),$contactus_sanity[2][1]);

//verify the check status button
_assertVisible(_link("check-email-status1", _in(_div("email-us-section"))));


_setValue(_textbox("dwfrm_contactus_ordernumbertostatus"),$orderno);
_setValue(_textbox("dwfrm_contactus_billingzipcode"),$contactus_sanity[5][8]);
_setValue(_textbox("dwfrm_contactus_billingemail"),$uId);

//click on check status button
_click(_link("check-email-status", _in(_div("email-us-section"))));

//verification in order details page
_assertVisible(_heading1("Order Detail"));
_assertVisible(_submit("/print-page/"));
_assertVisible(_link("/email-page/"));

takescreenshot();

//storing the order no from order confirmation page 
var $ordernoinDetailspage=_extract(_getText(_span("order-number")),"/[#](.*)/",true);
var $emailIdinDetailspage=_getText(_div("mini-address-location")).split(" ")[2];

//address verification 
var $billaddressinDetailspage=_getText(_div("mini-address-location"));
var $shipaddressinDetailspage=_getText(_div("address"));

//price comparing in details page
var $priceinDetailspage=_extract(_getText(_span("order-price")),"/: (.*)/",true);
var $orderpriceinDetailspage=_getText(_div("order-value",_in(_listItem("OrderTotalNum"))));

//shipping method
var $shippingmethodinDetailspage=_getText(_div("value",_in(_div("shipping-method"))));

//card number
var $verifyingcardnoinDetailpage=_extract(_getText(_div("order-payment-instruments")),"/: (.*)/",true);

//delivery date
var $deliverydateinDetailpage=_extract(_getText(_div("order-date")),"/: (.*)/",true);

_assertEqual($orderno,$ordernoinDetailspage);
_assertEqual($IDinocpPage,$emailIdinDetailspage);
_assertEqual($billaddress,$billaddressinDetailspage);
_assertEqual($shipaddress,$shipaddressinDetailspage);
_assertEqual($price,$priceinDetailspage);
_assertEqual($orderprice,$orderpriceinDetailspage);
_assertEqual($shippingmethod,$shippingmethodinDetailspage);
_assertEqual($verifyingcardno,$verifyingcardnoinDetailpage);
_assertEqual($deliverydate,$deliverydateinDetailpage);


}
catch($e)
{
_logExceptionAsFailure($e);	
}	
$t.end()


var $t = _testcase("219207/219242/219329","Verify the functionality of 'START CHAT' button in Contact Us page for desktop/tablet /Verify the functionality of 'START CHAT' button in Contact Us page for mobile");
$t.start();
try
{
	
//click on contact us link in footer
_click(_link("Contact Us", _in(_listItem("Contact Us"))));
_click(_link($drop6_data[2][0], _near(_div("Customer Service"))));
//breadcrumb   
_assertVisible(_div($drop6_data[1][1], _in(_div("breadcrumb"))));
//content-header
_assertVisible(_heading1("content-header"));

//display live chat
_assertVisible(_div("chat-module-section"));
_assertVisible(_div("chat-header", _in(_div("chat-module-section"))));
_assertVisible(_link("Chat", _in(_div("chat-module-section"))));

//Click Start chat
_click(_link("Chat", _in(_div("chat-module-section"))));

_wait(3000);
var $recentWinID=_getRecentWindow().sahiWinId;
_log($recentWinID);
_selectWindow($recentWinID);

//verify chat link popup
_assertVisible(_image("Crown Awards"));
_assertVisible(_bold("Support Chat"));
_assertVisible(_table("chat_survey_table"));

_closeWindow();
_selectWindow();
}

catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

/*var $t = _testcase("","");
$t.start();
try
{

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()*/