_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("PDP.xls");
var $pdp=_readExcelFile("PDP.xls","pdp");
var $Address_data=_readExcelFile("../../Checkout/Payment/Payment.xls","Address_data");
var $credit_card=_readExcelFile("../../Checkout/Payment/Payment.xls","credit_card");
var $shipaddress=_readExcelFile("../../Checkout/ShippingPage/ShippingPage.xls","ShipAddress");
var $shippingdata=_readExcelFile("../../Checkout/ShippingPage/ShippingPage.xls","ShippingData");
var $RE = "0";
var $checkboxmain;
ClearCookies();
_setAccessorIgnoreCase(true); 
cleanup();

var $t = _testcase("227203","Verify the UI related to the Checkout-View Engraving (on payment page) in Crown Awards application as an anonymous and registered user.");
$t.start();
try
{
	
	//navigate to cart page with single 
	navigateToCart($pdp[3][0],$pdp[0][1]);
	
	//click on checkout link in mini cart
	_mouseOver(_link("/mini-cart-link/"));
	//click link on view cart link in mini cart section
	_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));
	
	_assertVisible(_submit("/view-engrave/"));
	_click(_submit("/view-engrave/"));
	//engraving details
	_assertVisible(_div("engrave-bold"));
	_assertVisible(_span($userLog[0][3]+""+"["+$RE+"]", _in(_div("engrave-bold"))));
	_assertEqual($userLog[0][3],_getText(_span("engrave-normal", _in(_div("engrave-bold")))));
	
	//click on checkout link
	_click(_link("/mini-cart-link-checkout-link/"));
	
	//shipping form should display
	shippingAddress($shipaddress,1);
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	
	//address proceed
	adddressProceed();
	
	//event calendar
	eventcalendar();
	
	//Selecting shipping method
	_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
	
	//click on view engraving in payment page
	_click(_link("/checkout-view-engraving/"));
	_wait(3000);
	
	//engraving content
	_assertVisible(_div("engrave-content"));
	_assertVisible(_div("review-engraving-details"));
	_assertVisible(_span($userLog[0][3]+""+"["+$RE+"]",_in(_div("engrave-content"))));
	_assertEqual($userLog[0][3],_getText(_span("engrave-normal", _in(_div("engrave-content")))));
	//click on close link
	//_click(_button("Close"));
	_click(_button("Close", _in(_div("ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-draggable"))));
	_assertNotVisible(_div("review-engraving-details"));
	
	
	//click on continue in shipping page
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//address proceed
	adddressProceed();
	
	//click on view engraving in payment page
	_click(_link("/checkout-view-engraving/"));
	
	//engraving content
	_assertVisible(_div("engrave-content"));
	_assertVisible(_div("review-engraving-details"));
	_assertVisible(_span($userLog[0][3]+""+"["+$RE+"]",_in(_div("engrave-content"))));
	//click on close link
	_click(_button("Close"));
	_assertNotVisible(_div("review-engraving-details"));
	
	//enter payment details
	PaymentDetails($shipaddress,5);
	
	//check the check box
	//_check(_checkbox("/dwfrm_billing_confirm/"));
	//place order button
	_click(_submit("dwfrm_billing_save"));
	//order confirmation page
	_assertVisible(_heading1($shippingdata[0][5]));
	_assertVisible(_div($shippingdata[1][5]));
	
	var $orderno=_getText(_span("order-number")).split(" ")[2];
	_log($orderno);
	
	var $orderkkkk=_getText(_span("order-number"))
	_log($orderkkkk);
	
	//engraving link on order confirmation page
	_click(_submit("/view-engrave/"));
	_assertVisible(_span($userLog[0][3]+""+"["+$RE+"]",_in(_div("engrave-bold"))));
	//details should be visible
	_assertVisible(_submit("Hide Engraving"));
	_click(_submit("Hide Engraving"));
	_assertNotVisible(_submit("Hide Engraving"));

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("227161","Verify the functionality related to Product Page Crystal product on PDP in Crown Awards application as an anonymous and registered user.");
$t.start();
try
{
	
	//navigate to PDP
	navigateToPdp($pdp[3][0]);
	
	$checkbox=_collectAttributes("_div", "/box itemnum/", "sahiText", _in(_div("/ItemTableWrap main-pdp-grid/")));
	
	//_check(_checkbox("prdSelect"+$checkbox[0]+""));
	_check(_checkbox("/"+"prdSelect"+$checkbox[0]+"/"))
	_assertVisible(_div("prodwrapper"));
	
	//select option
	_setSelected(_select("flexdetails"),$pdp[0][3]);
	//select quantity
	_setValue(_numberbox("Quantity"),$pdp[1][1]);
	//click on add to cart button
	_click(_submit("/add-to-cart/"));
	//engraving
	engravingPDP();
	//click on view cart button
	_mouseOver(_link("/mini-cart-link/"));
	_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


_log("*********************************glpk6 and lucbb4 product order placing in different scenarios**********************************")

//var $t = _testcase("12345/227240","glpk6 and lucbb4 product order placing in different scenarios");
//$t.start();
//try
//{
	for (var $t=0;$t<=1;$t++)
	{	
		_log("******************************"+$t+"**********************************")
		//navigate to PDP
		navigateToPdp($pdp[$t][5]);

		$checkboxmain=_collectAttributes("_div", "/box itemnum/", "sahiText", _in(_div("/ItemTableWrap main-pdp-grid/")));
		//$checkboxsize = _collectAttributes("_div", "box-row", "sahiText", _in(_div("product-content"))).length
		_log($checkboxmain);
		
		for (var $p=0;$p<$checkboxmain.length;$p++)
		{
				_check(_checkbox("/prdSelect"+$checkboxmain[$p]+"/"));
				_wait(3000)
				if (_isVisible(_select("flexdetails", _in(_div("prodwrapper["+$p+"]")))))
				{
					_setSelected(_select("flexdetails", _in(_div("prodwrapper["+$p+"]"))),1);
					_wait(3000);
				}
				else if (_isVisible(_checkbox("/check/", _in(_div("/flexdetails/", _in(_div("prodwrapper["+$p+"]")))))))
				{
					_check(_checkbox("/check/", _in(_div("/flexdetails/", _in(_div("prodwrapper["+$p+"]"))))))
					_wait(3000);
				}
				else if (_isVisible(_radio("/radio/", _in(_div("/flexdetails/", _in(_div("prodwrapper["+$p+"]")))))))
				{
					_check(_radio("/radio/", _in(_div("/flexdetails/", _in(_div("prodwrapper["+$p+"]"))))))
					_wait(3000);
				}
				
				if (_isVisible(_div("uploadPopup")))
				{
					_click(_button("Close",_near(_div("uploadPopup"))))
				}
		
				while (_isVisible(_div("dialog-container")))
				{
					_click(_button("Close",_near(_div("dialog-container"))));
				}
				//selecting quantity
				_setValue(_numberbox("Quantity", _in(_div("prodwrapper["+$p+"]"))), "10");
				//verify the values
				_assertEqual("10", _getValue(_numberbox("Quantity", _in(_div("prodwrapper["+$p+"]")))));
				//add to cart button should enable
				_assertEqual(false, _submit("Add to Cart & Engrave["+$p+"]").disabled);
			
		}
		//add all to engrave
		_click(_link("/add-all-engrave/"));
		_wait(5000);
		//engraving
		engravingPDP();
		//navigate to checkout page
		_click(_link("/mini-cart-link-checkout-link/"));
		//shipping details
		shippingAddress($Address_data,$t);
		_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
		//address proceed
		adddressProceed();
		
		//selecting shipping method
		_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
		
		//Shipment date
		eventcalendar();
		
		_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
		
		adddressProceed();
		//Payment details
		PaymentDetails($credit_card,$t);
		//Placing order
		//_check(_checkbox("/dwfrm_billing_confirm/"));
		
		_click(_submit("dwfrm_billing_save"));

		//Checking order is placed or not
		_assertVisible(_heading1($pdp[0][6]));
		_assertVisible(_div("order-confirmation-details"));


		var $orderno=_getText(_span("order-number")).split(" ")[2];
		_log($orderno);

		var $orderkkkk=_getText(_span("order-number"));
		_log($orderkkkk);
	}

//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end()



function engravingPDP()
{

while (_isVisible(_div("headerContainer")))

	{
	
if ((_isVisible(_div("itemPreviewContainerAdvanced"))) || (_isVisible(_div("container itemInputContainerAdvanced"))))
	{
	
	var $count=_count("_textbox","lineInputAdvanced",_in(_div("NDSColumnContainer")));

var $q=1;	
for (var $i=1;$i<=parseInt($count);$i++)
	{
	
	if ($i==1)
		{
		_setSelected(_select("contentSelect_1_"+$i+""), "Ornament");
		}
		else
			{
			_setSelected(_select("contentSelect_1_"+$i+""), "Text");
			_setValue(_textbox("lineInputAdvanced["+$q+"]"),$pdp[0][4]);
			_wait(1000)
			if (_isVisible(_div("Repeat On All")))
			{
				_check(_checkbox("lineCheckboxAdvanced["+$q+"]"));
//				if (!_checkbox("lineCheckboxAdvanced["+$q+"]").checked)
//					{
//					_check(_checkbox("lineCheckboxAdvanced["+$q+"]"));
//					}
			}
			
			$q++;
			
			}

	}

	}
else
	{
	
	var $count=_count("_textbox","/lineInput/",_in(_div("NDSColumnContainer")));
	for (var $j=0;$j<parseInt($count);$j++)
		{
		_setValue(_textbox("lineInput["+$j+"]",_in(_div("container itemInputContainerBasic"))),$pdp[0][4]);
		
		if (_isVisible(_div("Repeat On All")))
		{
			if (!_checkbox("lineCheckbox["+$j+"]").checked)
				{
				_click(_checkbox("lineCheckbox["+$j+"]"));
				}
		}
		
		_check(_checkbox("confirmCheckbox"));
		_click(_link("atc_button"));
		
		}

	}


_check(_checkbox("confirmCheckbox"));
_click(_link("atc_button"));

if (_isVisible(_div("tie-In")))
	{
	_click(_button("Close"));
	}

	}
}
