_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Drop1_sanity.xls");

var $data_drop1=_readExcelFile("Drop1_sanity.xls","Data");



//if (_isChrome())
//{

var $t = _testcase("216679","Verify if a Customer object is created in BM after a user account is created in application.");
$t.start();
try
{
	
	 BM_Login();
	_click(_link("Customers"));
	
	//Need to change these two steps once we got the access
	//_click(_link("overview_subtitle", _in(_cell("Import & Export"))));
	//_click(_submit("Export",_near(_cell("Customers"))));
	
	_click(_link("Customers[1]"));
	_click(_link("Advanced"));
	
	_setValue(_textbox("WFCustomerAdvancedSearch_Email"),$uId);
	_click(_submit("Find", _in(_div("/Advanced Customer Search/"))));
	
	//Should match the user created
	//_assertVisible(_cell($uId));
	if(!_isVisible(_cell($uId)))
	{
	   _assert(false,"********User login crendials are not valid please create new user and execute script again************")
	}
	
	//log out from BM
	_click(_link("Log off."));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

//}

//Site url's and clean up function
ClearCookies();
_setAccessorIgnoreCase(true); 
cleanup();

_log("*********** Register Flow for drop1 sanity **********");

var $t = _testcase("215965/216445","Verify the application behvaior on login link in the global header as a Guest User./Verify the application behvaior on click of Login link in the global header in mobile view.");
$t.start();
try
{

if (isMobile())
	{
	 _assertVisible(_link("/mobile-show/", _in(_div("user-info"))));
	 _mouseOver(_link("/mobile-show/", _in(_div("user-info"))));
	 
	 if (_getAttribute(_link("/mobile-show/", _in(_div("user-info"))),"title")!=null)
	 {
	     _assert(true);
	 }
		else
		{
		     _assert(false);
		}
		 
	}
	else
		{
		 _assertVisible(_link("/desktop-show/", _in(_div("user-info"))));
		 _mouseOver(_link("/desktop-show/", _in(_div("user-info"))));
		 
		 if (_getAttribute(_link("/desktop-show/", _in(_div("user-info"))),"title")!=null)
		 {
		     _assert(true);
		 }
			else
			{
			     _assert(false);
			}
		}
 

if (isMobile())
{
   //click on login link
   _click(_link("/mobile-show/", _in(_div("user-info"))));	
}
  else
	  {
	   //click on login link
	   _click(_link("/desktop-show/", _in(_div("user-info"))));
	   //Visibility of overlay
	   _assertVisible(_div("dialog-container"));
	  }

//visibility of heading "Returning customers"
_assertVisible(_heading1($data_drop1[1][0]));
_assertVisible(_textbox("/dwfrm_login_username/"));
_assertVisible(_password("/dwfrm_login_password/"));
//_assertVisible(_label("Email:*"));
_assertVisible(_label("/Email/"));
_assertVisible(_label("/Password/"));
//new customers heading
_assertVisible(_heading2($data_drop1[0][0]));
//create a login button
_assertVisible(_link("/button/", _in(_div("login-box registration-btn"))));
//Privacy policy 
_assertVisible(_link("privacy-policy"));
//visibility of Cancel button in overlay
//_assertVisible(_submit("/cancel-btn/"));
_assertVisible(_submit("cancel-btn", _in(_div("popup_buttons"))));
//click on close overlay
_click(_button("Close", _in(_div("/ui-dialog ui-widget/"))));
//overlay should not visible
_assertNotVisible(_div("dialog-container"));

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


if (isMobile())
{
var $t = _testcase("216446","Verify the application behavior on click of Hamburger menu in the global header in mobile view.");
$t.start();
try
{

//checking visibility of header in mobile	
_assertVisible(_div("top-banner headerTable"));
//click on Hamburger menu
_click(_submit("menu-toggle"));
//verify category names are displaying or not
var $CatCount=_count("_link","/menuitems/", _in(_list("menu-category level-1")));
var $CatNames=_collectAttributes("_link","/menuitems/","sahiText",_in(_list("/menu-category level-1/")));

for (var $i=0;$i<$CatCount;$i++)
{
	_assertVisible(_link(" menuitems["+$i+"]"));
	_assertVisible(_link($CatNames[$i]));
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()	
}	

var $t = _testcase("215975/216677/218567","Verify the Login Functionlaity in the Global Header/Verify the Login functionality using Login model window/Verify the navigation to Account Landing Page");
$t.start();
try
{

NavigatetoHomepage();

//should re direct to home page
_assertVisible(_div("home-main-right"));

	if (isMobile())
	{
	   //click on login link
	   _click(_link("/mobile-show/", _in(_div("user-info"))));	
	}
	  else
		  {
		   //click on login link
		   _click(_link("/desktop-show/", _in(_div("user-info"))));
		   //Visibility of overlay
		   _assertVisible(_div("dialog-container"));
		  }

//login to the app
login();
//login link 
//_assertVisible(_listItem("Hi "+$User[$user][1]+" ( Log Out )"));
_assertVisible(_listItem("Log In Log In Hi "+$User[$user][1]+" ( Log Out )"));

_assertNotVisible(_link("/user-login/"));

if (isMobile())
{
   //click on login link
	_assertNotVisible(_link("/mobile-show/", _in(_div("user-info"))));	
}
  else
	  {
	   //click on login link
	  _assertNotVisible(_link("/desktop-show/", _in(_div("user-info"))));
	  }


_assertVisible(_link("/user-logout/"));

//should re direct to home page
_assertVisible(_div("home-main-right"));



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("216027","Verify the display of search suggestions drop down for a REG user as per characters entered.");
$t.start();
try
{
	
for (var $l=0;$l<5;$l++)
	{
_setValue(_textbox("q", _in(_div("header-test-search"))),$data_drop1[$l][1]);

if($l==4)
	{
	_wait(4000);
	_assertVisible(_div("search-suggestion-wrapper full"));
	_assertVisible(_link("hit", _in(_div("phrase-suggestions"))));
	_assertVisible(_link("product-link"));
	_assertVisible(_image("/(.*)/", _in(_div("product-image"))));
	}
else
	{
	_wait(3000);
	_assertNotVisible(_div("search-suggestion-wrapper full"));
	}

	}

}

catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216083","Verify the Mega Menu behavior in the global header as a REG User.");
$t.start();
try
{
	
//Stock categories
var $stockcategoriesCount=_count("_link","/menuitems/", _in(_listItem("/stockcategories/")));
var $stockcategoriesNames=_collectAttributes("_link","/menuitems/","sahiText",_in(_listItem("/stockcategories/")));

for (var $j=0;$j<$stockcategoriesCount;$j++)
{
	_click(_link($stockcategoriesNames[$j]));
	if($j==5)
		{
		//Verifying category landing page
		_assertVisible(_span("Sculpture Trophies", _in(_div("breadcrumb"))));
		}
	else
		{
		//Verifying category landing page
		_assertVisible(_span($stockcategoriesNames[$j], _in(_div("breadcrumb"))));
		}
	
}

//Custom categories
var $customcategoriesCount=_count("_link","/menuitems/", _in(_listItem("/customcategories/")));
var $customcategoriesNames=_collectAttributes("_link","/menuitems/","sahiText",_in(_listItem("/toprightcategory/")));

for (var $j=0;$j<$customcategoriesCount;$j++)
{
	_click(_link($customcategoriesNames[$j]));
	//Verifying category landing page
	_assertVisible(_span($customcategoriesNames[$j], _in(_div("breadcrumb"))));
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("215973","Verify the application behvaior on click of Log out link in the Global header");
$t.start();
try
{
	
//click on logout link in header
if (_isVisible(_link("/user-logout/")))
	{
	_click(_link("/user-logout/"));
	}
//log out link should not be visible
_assertNotVisible(_link("/user-logout/"));
//should re direct to home page
_assertVisible(_div("home-main-right"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


_log("************** Guest Flow for drop1 sanity*************")
var $t = _testcase("216026","Verify the display of search suggestions drop down for a guest user as per characters entered.");
$t.start();
try
{
	
	for (var $l=0;$l<5;$l++)
	{
_setValue(_textbox("q", _in(_div("header-test-search"))),$data_drop1[$l][1]);

if($l==4)
	{
	_wait(4000);
	_assertVisible(_div("search-suggestion-wrapper full"));
	_assertVisible(_link("hit", _in(_div("phrase-suggestions"))));
	_assertVisible(_link("product-link"));
	_assertVisible(_image("/(.*)/", _in(_div("product-image"))));
	}
else
	{
	_wait(3000);
	_assertNotVisible(_div("search-suggestion-wrapper full"));
	}

	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("216082","Verify the Mega Menu behavior in the global header as a Guest User.");
$t.start();
try
{
	
//Stock categories
var $stockcategoriesCount=_count("_link","/menuitems/", _in(_listItem("/stockcategories/")));
var $stockcategoriesNames=_collectAttributes("_link","/menuitems/","sahiText",_in(_listItem("/stockcategories/")));

for (var $j=0;$j<$stockcategoriesCount;$j++)
{
	_click(_link($stockcategoriesNames[$j]));
	if($j==5)
	{
	//Verifying category landing page
	_assertVisible(_span("Sculpture Trophies", _in(_div("breadcrumb"))));
	}
else
	{
	//Verifying category landing page
	_assertVisible(_span($stockcategoriesNames[$j], _in(_div("breadcrumb"))));
	}
}

//Custom categories
var $customcategoriesCount=_count("_link","/menuitems/", _in(_listItem("/customcategories/")));
var $customcategoriesNames=_collectAttributes("_link","/menuitems/","sahiText",_in(_listItem("/toprightcategory/")));

for (var $j=0;$j<$customcategoriesCount;$j++)
{
	_click(_link($customcategoriesNames[$j]));
	//Verifying category landing page
	_assertVisible(_span($customcategoriesNames[$j], _in(_div("breadcrumb"))));
}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("216108","Verify the application behavior on click of Checkout button in global header when no product list part of cart as a Guest user.");
$t.start();
try
{
	
//click on checkout button in header with out adding products
_click(_link("/mini-cart-link-checkout/"))
//Handling popup
_expectConfirm("/Shopping Cart is currently empty,cannot Checkout/", true); 

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()

var $t = _testcase("12345","Checking all the footer links and its navigation");
$t.start();
try
{

var $CrownCategory=_collectAttributes("_link","/(.*)/","sahiText",_in(_div("footerLinksColumns footerCategory")));	
	
for(var $i=0;$i<$CrownCategory.length;$i++)
{
	_log($CrownCategory[$i]);
	//footer configured links
	_assertVisible(_link($CrownCategory[$i],_in(_div("footerLinksColumns footerCategory"))));
	_click(_link($CrownCategory[$i], _in(_div("footerLinksColumns footerCategory"))));
	if(_isVisible(_link($data_drop1[$i][3], _in(_div("breadcrumb")))))
		{
		//checking bread crumb
		_assertVisible(_link($data_drop1[$i][3], _in(_div("breadcrumb"))));
		_assertEqual($data_drop1[$i][3], _getText(_link("breadcrumb-element last")));
		}
	else
		{
		//checking bread crumb
		_assertVisible(_span($data_drop1[$i][3], _in(_div("breadcrumb"))));
		_assertEqual($data_drop1[$i][3], _getText(_span("breadcrumb-element last")));
		}

	if(!_isVisible(_span("Award By Sport", _in(_div("breadcrumb")))))
		{
			//checking heading
			_assertVisible(_heading1($data_drop1[$i][4]));
		}
	
	//_assertVisible(_heading1($CrownCategory[$i]));
	if($i==7 || $i==8)
		{
		_log($CrownCategory[$i]);
		//footer configured links
		_assertVisible(_link($CrownCategory[$i],_in(_div("footerLinksColumns footerCategory"))));
		_click(_link($CrownCategory[$i], _in(_div("footerLinksColumns footerCategory"))));
		//checking bread crumb
		_assertVisible(_link($data_drop1[$i][3], _in(_div("breadcrumb"))));
		_assertEqual($data_drop1[$i][3], _getText(_link("breadcrumb-element last")));
		//checking heading
		_assertVisible(_heading1($data_drop1[$i][4]));
		
		}
}	

var $AboutCrownAwards=_collectAttributes("_link","/(.*)/","sahiText",_in(_div("footerLinksColumns footerAbout")));	

for(var $i=0;$i<$AboutCrownAwards.length;$i++)
{
	//footer configured links
	_assertVisible(_link($AboutCrownAwards[$i],_in(_div("footerLinksColumns footerAbout"))));
	_click(_link($AboutCrownAwards[$i], _in(_div("footerLinksColumns footerAbout"))));
	//checking bread crumb
	_assertVisible(_link($data_drop1[$i][5], _in(_div("breadcrumb"))));
	_assertEqual($data_drop1[$i][5], _getText(_link("breadcrumb-element last")));
	//checking heading
	_assertVisible(_heading1($data_drop1[$i][6]));
}

var $CustomerService=_collectAttributes("_link","/(.*)/","sahiText",_in(_div("footerLinksColumns footerCustServ")));	

for(var $i=0;$i<$CustomerService.length;$i++)
{
	//footer configured links
	_assertVisible(_link($CustomerService[$i],_in(_div("footerLinksColumns footerCustServ"))));
	_click(_link($CustomerService[$i], _in(_div("footerLinksColumns footerCustServ"))));
	//checking bread crumb
	_assertVisible(_link($data_drop1[$i][7], _in(_div("breadcrumb"))));
	_assertEqual($data_drop1[$i][7], _getText(_link("breadcrumb-element last")));
	//checking heading
	_assertVisible(_heading1($data_drop1[$i][8]));
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


//
//var $t = _testcase("12345","Checking all the footer links and its navigation");
//$t.start();
//try
//{
//
//var $CrownCategory=_collectAttributes("_link","/(.*)/","sahiText",_in(_div("footerLinksColumns footerCategory")));	
//	
//for(var $i=0;$i<$CrownCategory.length;$i++)
//{
//	_log($CrownCategory[$i]);
//	//footer configured links
//	_assertVisible(_link($CrownCategory[$i],_in(_div("footerLinksColumns footerCategory"))));
//	_click(_link($CrownCategory[$i], _in(_div("footerLinksColumns footerCategory"))));
//	//checking bread crumb
//	_assertVisible(_link($data_drop1[$i][3], _in(_div("breadcrumb"))));
//	_assertEqual($data_drop1[$i][3], _getText(_link("breadcrumb-element last")));
//	//checking heading
//	_assertVisible(_heading1($data_drop1[$i][4]));
//	//_assertVisible(_heading1($CrownCategory[$i]));
//}	
//
//var $AboutCrownAwards=_collectAttributes("_link","/(.*)/","sahiText",_in(_div("footerLinksColumns footerAbout")));	
//
//for(var $i=0;$i<$AboutCrownAwards.length;$i++)
//{
//	//footer configured links
//	_assertVisible(_link($AboutCrownAwards[$i],_in(_div("footerLinksColumns footerAbout"))));
//	_click(_link($AboutCrownAwards[$i], _in(_div("footerLinksColumns footerAbout"))));
//	//checking bread crumb
//	_assertVisible(_link($data_drop1[$i][5], _in(_div("breadcrumb"))));
//	_assertEqual($data_drop1[$i][5], _getText(_link("breadcrumb-element last")));
//	//checking heading
//	_assertVisible(_heading1($data_drop1[$i][6]));
//}
//
//var $CustomerService=_collectAttributes("_link","/(.*)/","sahiText",_in(_div("footerLinksColumns footerCustServ")));	
//
//for(var $i=0;$i<$CustomerService.length;$i++)
//{
//	//footer configured links
//	_assertVisible(_link($CustomerService[$i],_in(_div("footerLinksColumns footerCustServ"))));
//	_click(_link($CustomerService[$i], _in(_div("footerLinksColumns footerCustServ"))));
//	//checking bread crumb
//	_assertVisible(_link($data_drop1[$i][7], _in(_div("breadcrumb"))));
//	_assertEqual($data_drop1[$i][7], _getText(_link("breadcrumb-element last")));
//	//checking heading
//	_assertVisible(_heading1($data_drop1[$i][8]));
//}
//
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end()

/*var $t = _testcase("","");
$t.start();
try
{

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()*/