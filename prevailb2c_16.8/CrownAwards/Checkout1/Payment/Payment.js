_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Payment.xls");

var $payment_data=_readExcelFile("Payment.xls","payment_data");
var $Address_data=_readExcelFile("Payment.xls","Address_data");
var $credit_card=_readExcelFile("Payment.xls","credit_card");
var $paymentvalidations=_readExcelFile("Payment.xls","PaymentValidations");
var $shippingdata=_readExcelFile("../ShippingPage/ShippingPage.xls","ShippingData");

if(mobile.device()=="desktop")
       {
       var $paymentcard="paymentcard-images desktop-only";
       }
else if(mobile.iPad())
       {
       var $paymentcard="paymentcard-images ipad-only";
       }


SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

function OrderSummary()
{
       //Verify the Order Summary
       //Edit cart link
       _assertVisible(_link("Edit Cart", _in(_div("/nav summary/"))));
       //Edit item lik
       _assertVisible(_link("Edit Item", _in(_div("/nav summary/"))));
       //Remove item link
       _assertVisible(_link("Remove Item", _in(_div("/nav summary/"))));
       if(_isVisible(_div("order-shipping")))
       {
              var $summaryShipping=_extract(_getText(_div("order-shipping  first ")),"[$](.*)",true);
       }
       //Product name
       var $pname= _getText(_link("/(.*)/", _in(_div("/cartdesc/"))));
       if($prodname.indexOf($pname)>=0)                       
       {
              _assert(true);
       }
       _assertEqual($prodquantity, _extract(_getText(_div("/mini-cart-attribute box cartqty/")),"/: (.*)/",true));
       if(_isVisible(_div("order-shipping")))
       {
              _assertEqual($shippingcharge,$summaryShipping);
       }
       //Order Total
       _assertEqual($ordertotal,_extract(_getText(_div("order-value", _in(_listItem("OrderTotalNum")))),"/[$](.*)/",true));
}

function guestlogin($emailid)
{
       _setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email"), $emailid);
       _setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail"), $emailid);
}

//Need to remove once issue is fixed
function templogin($emailid,$pswd)
{
       _setValue(_textbox("/dwfrm_login_checkout_username/"), $emailid);
       _setValue(_password("/dwfrm_login_checkout_password/"), $pswd);
}

function prepopulateShipping()
{
       //verify the prepopulated values
       //checkout login
       _assertEqual($uId,_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email")));
       _assertEqual($uId,_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail")));
       
       //Shipping section
       _assertEqual("/"+$Address_data[0][1]+"/i",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName")));
       _assertEqual("/"+$Address_data[0][2]+"/i",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName")));
       _assertEqual("/"+$Address_data[0][3]+"/i",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1")));
       _assertEqual($Address_data[0][4],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2")));
       _assertEqual("/"+$Address_data[0][5]+"/i",_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_country")));
       _assertEqual("/"+$Address_data[0][6]+"/i",_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
       _assertEqual("/"+$Address_data[0][7]+"/i",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")));
       _assertEqual($Address_data[0][8],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")));
       _assertEqual($Address_data[0][9],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone")));
}

function prepopulateBilling()
{
       //verify the prepopulated values
       //checkout login
       _assertEqual($uId,_getText(_textbox("dwfrm_billing_billingAddress_email_emailAddress")));
       _assertEqual($uId,_getText(_textbox("dwfrm_billing_billingAddress_email_confirmemail")));
       
       //Billing section
       _assertEqual("/"+$Address_data[1][1]+"/i",_getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
       _assertEqual("/"+$Address_data[1][2]+"/i",_getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
       _assertEqual("/"+$Address_data[1][4]+"/i",_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
       _assertEqual("/"+$Address_data[1][3]+"/i",_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address2")));
       _assertEqual("/"+$Address_data[1][5]+"/i",_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
       _assertEqual("/"+$Address_data[1][6]+"/i",_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
       _assertEqual("/"+$Address_data[1][7]+"/i",_getText(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
       _assertEqual($Address_data[1][8],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_postal")));
       _assertEqual($Address_data[1][9],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_phone")));
}

var $t = _testcase("219898/219899/220085/220086/220087/219943/219928/219942/220082/220082/222343/221840/226645/226647/226652","Verify the Application navigation to Payment Page/UI of the Payment Page./UI of the Review confirmation section/UI of the Apply Promotion Code Section/if the review section checkbox selection on Payment page load");
$t.start();
try
{
       addItemToCart($payment_data[0][7],$payment_data[0][8]);
       //navigate to cart
       _click(_link("/mini-cart-link/"));
       
       var $subtotal=_extract(_getText(_div("/(.*)/", _in(_div("box ordersumsnum")),_rightOf(_div("Subtotal:")))),"[$](.*)",true);
       if(_isVisible(_div("order-shipping")))
       {
              var $shippingcharge=_extract(_getText(_div("order-shipping")),"[$](.*)",true);
       }
       
       var $ordertotal=_extract(_getText(_div("order-value")),"/[$](.*)/",true);

              
       //Click on checkout link
       _click(_link("/mini-cart-link-checkout-link/"));
       //Verify order summary
       OrderSummary();
       //Enter values in guest login
       guestlogin($uId);    
       //enter shipping address
       shippingAddress($Address_data,0);
       //Uncheck the checkbox
       _uncheck(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
       BillingAddress($Address_data,1);
       
       //address proceed
       adddressProceed();
       
       //selecting shipping method
       _check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
       
       //event calendar
       eventcalendar();
       
       //click on continue button
       _click(_submit("/continue-button/"));
       
       //address proceed
       adddressProceed();
       
       //Patyment section UI
       var $shippingaddress=$Address_data[0][1]+" "+$Address_data[0][2]+" "+$Address_data[0][3]+" "+$Address_data[0][4]+" "+$Address_data[0][7]+", "+$Address_data[0][11]+" "+$Address_data[0][8]+" "+$Address_data[0][5];
       var $billingaddress=$Address_data[1][1]+" "+$Address_data[1][2]+" "+$Address_data[1][12]+" "+$uId+" "+$Address_data[1][3]+" "+$Address_data[1][4]+" "+$Address_data[1][7]+", "+$Address_data[1][11]+" "+$Address_data[1][8]+" "+$Address_data[1][5];  
      //should navigate to billing/payment page
       _assertEqual($payment_data[1][0], _getText(_div("address-header-main")));
       _assertContainsText("Review Address And Delivery", _div("address-block handledmixedpopup"));
       //comparing address
       _assertEqual($Address_data[3][0]+" "+$uId+" "+$Address_data[3][1],_getText(_div("address-content", _in(_div("billing-address-section")))));
       _assertEqual($Address_data[4][0],_getText(_div("address-content", _in(_div("shipping-address-section")))));
       
       //Verify order summary
       //Verify the Order Summary
       //Edit cart link
       _assertVisible(_link("Edit Cart", _in(_div("/nav summary/"))));
       //Edit item lik
       _assertVisible(_link("Edit Item", _in(_div("/nav summary/"))));
       //Remove item link
       _assertVisible(_link("Remove Item", _in(_div("/nav summary/"))));
       _assertEqual($prodquantity, _extract(_getText(_div("/mini-cart-attribute box cartqty/")),"/: (.*)/",true));
       
       //radio button
       //Credit card
       _assertVisible(_label("Credit Card"));
       _assertVisible(_radio("dwfrm_billing_paymentMethods_selectedPaymentMethodID"));
       _assert(_radio("dwfrm_billing_paymentMethods_selectedPaymentMethodID").checked);
       //Purchase order
       //_assertVisible(_label("Purchase Order"));
       //_assertVisible(_submit("Place Order", _in(_div("continue-button-checkout"))));
//
//       _assertVisible(_radio("is-PURCHASE_ORDER"));
//       _assertNotTrue(_radio("is-PURCHASE_ORDER").checked);
       //credit card number
       _assertVisible(_span("Credit Card Number"));
       _assertVisible(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"));
       //Security
       _assertVisible(_span("Security Code (CID#)"));
       _assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_cvn"));
       //tooltip
       _assertVisible(_link("tooltip", _rightOf(_textbox("dwfrm_billing_paymentMethods_creditCard_cvn"))))
       //expiration date
       _assertVisible(_span("Expiration Date:"));
       _assertVisible(_select("/dwfrm_billing_paymentMethods_creditCard_expiration_month/"));
       _assertEqual($payment_data[0][4],_getSelectedText(_select("/dwfrm_billing_paymentMethods_creditCard_expiration_month/")));
       _assertVisible(_select("/dwfrm_billing_paymentMethods_creditCard_expiration_year/"));
       _assertEqual($payment_data[0][5], _getSelectedText(_select("/dwfrm_billing_paymentMethods_creditCard_expiration_year/")));
       _assertEqual(true,_radio("input-radio", _in(_div("Credit Card"))).checked);
       //required
       _assertVisible(_div("dialog-required"));
       //checkbox
       _assertVisible(_span($payment_data[1][1]));
       _assertVisible(_checkbox("/dwfrm_billing_confirm/"));
       _assertNotTrue(_checkbox("/dwfrm_billing_confirm/").checked);
       //Promotion
//       _assertVisible(_span("Apply Promotion code"));
//       _assertVisible(_textbox("dwfrm_billing_couponCode"));
//       _assertVisible(_submit("dwfrm_billing_applyCoupon"));
       //
       _assertVisible(_checkbox("/dwfrm_billing_confirm/"));

       
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();


var $t = _testcase("219900/219901/220004","Verify the UI of the Progress Indicator in Payment Page/Veirfy the data displayed in the Delivery Information section in Review Address and Delivery section in Payment Page.");
$t.start();
try
{
       
       //UI of progress indicator
       _assertVisible(_div("Shopping Cart Review Your Cart", _in(_div("/ checkout-progress-indicator/"))));
       _assertVisible(_div("step-1 inactive"));
       _assertVisible(_div("Shipping & Delivery", _in(_div("/ checkout-progress-indicator/"))));
       _assertVisible(_div("step-2 inactive"));
       _assertVisible(_div("Payment & Place Order", _in(_div("/ checkout-progress-indicator/"))));
       _assertVisible(_div("step-3 active"));
       
       
       //click on payment and place order in indicator
       _click(_div("Payment & Place Order", _in(_div("/checkout-progress-indicator/"))));
       //should navigate to billing/payment page
       _assertEqual($payment_data[1][0], _getText(_div("address-header-main")));
       
//       click on payment and place order in indicator
//       _click(_div("Shopping Cart Review your order", _in(_div("/checkout-progress-indicator/"))));
//       _click(_div("Review Your Cart", _in(_div("breadcrumb handledmixedpopup checkout-progress-indicator"))));
       //should navigate to billing/payment page
       _assertEqual($payment_data[1][0], _getText(_div("address-header-main")));
       
       //delevery information
       _assertVisible(_div("Delivery Information"));
       _assertVisible(_div("/delivary-date/"));
       
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();

var $t = _testcase("219945/219944/220005","Verify the EDIT link functionality in the Billing address/Shipping address in Review Address and Delivery section in Payment Page.");
$t.start();
try
{
       //Edit link in shipping section
       _click(_link("Edit", _in(_div("billing-address-section"))));
       prepopulateShipping();
       _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
       adddressProceed();
       //Edit link in Billing
       _click(_link("Edit", _in(_div("shipping-address-section"))));
       //prepopulateBilling(); needs to fix
       _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
       adddressProceed();
       //Edit link in Deliveer information
       _click(_link("Edit", _in(_div("delivery-information-section"))));
       //_check(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook"));
       prepopulateShipping();
       prepopulateBilling();
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();

ClearCartItems();
var $t=_testcase("221857/221893/221894/221895","Verify whether user is able to place order using VISA, MASTERCARD, AMERICAN EXPRESS, DISCOVER as credit card type  and Credit card alone on billing page in Application as a Anonymous user.");
$t.start();
try
{
	//$credit_card.length
       for(var $i=0;$i<$credit_card.length;$i++)
              {
    	   _log("************************************"+$i+"********************************")
                     //navigate to cart page
                     navigateToCart($payment_data[1][7],$payment_data[0][8]);
                     //navigate to checkout page
                     _click(_link("/mini-cart-link-checkout-link/"));
                     
                     //comparing product name in shipping page
                     var $pnameshippingpage=_getText(_link("/(.*)/", _in(_div("/mini-cart-names box/")))).toString();
                     
                     if($pName.indexOf($pnameshippingpage)>=0)                            
                     {
                           _assert(true);
                     }
                     
                     //shipping details
                     shippingAddress($Address_data,0);
                     
                     //address proceed
                     adddressProceed();
                     
                     _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"))
                       //Selecting shipping method
                     _check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));                                      
                     //Shipment date
                     eventcalendar();
                    
                     
                     var $priceinshippingpage=_getText(_div("order-value", _in(_listItem("OrderTotalNum"))));
                                 
                     _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
                     
                     //address proceed
                     adddressProceed();
                     
                     var $billingprice=_getText(_div("order-value", _in(_listItem("OrderTotalNum"))));
                     //comparing shipping and billing price
                     _assertEqual($priceinshippingpage,$billingprice);
                     
                     //Payment details
                     PaymentDetails($credit_card,$i);
                                         
                     //Placing order
                     _click(_checkbox("/dwfrm_billing_confirm/"));
                     _click(_submit("dwfrm_billing_save"));
                     
                     //Checking order is placed or not
                     _assertVisible(_heading1($payment_data[1][3]));
                     _assertVisible(_div("order-confirmation-details"));
                     
                     var $pnameinocppage=_getText(_link("/(.*)/", _in(_div("product-list-item")))).toString();
                     if($pnameshippingpage.indexOf($pnameinocppage)>=0)                         
                     {
                           _assert(true);
                     }

                     _assertEqual($prodquantity,_getText(_div("/cartqty/", _in(_div("line-item box-row")))));
                     
                     
                     var $priceinOCP=_extract(_getText(_span("order-price")),"/: (.*)/",true);
                     var $ordertotalOCP=_getText(_div("order-value", _in(_listItem("OrderTotalNum"))));
                     
                     //price verification
                     _assertEqual($priceinshippingpage,$priceinOCP);
                     _assertEqual($billingprice,$priceinOCP);
                     _assertEqual($priceinOCP,$ordertotalOCP);
                     
                     
                     var $orderno=_getText(_span("order-number")).split(" ")[2];
                     _log($orderno);
                     
                     var $orderkkkk=_getText(_span("order-number"))
                     _log($orderkkkk);
                     
                     takescreenshot();
                     
                     //payment in fo
                     _assertVisible(_div("order-payment-instruments"));
                     _assertContainsText($credit_card[$i][6], _div("order-payment-instruments"));
              }
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

if (isMobile())
{
_click(_link("/mobile-show/", _in(_div("user-info"))));
}
else
       {
       _click(_link("/desktop-show/", _in(_div("user-info"))));
       //Visibility of overlay
       _assertVisible(_div("dialog-container"));
       }

_wait(5000);

var $t = _testcase("221897/221898/221899/221900","Verify whether user is able to place order using VISA, MASTERCARD, AMERICAN EXPRESS, DISCOVER as credit card type  and Credit card alone on billing page in Application as a Registered user.");
$t.start();
try
{

	//_click(_link("/user-logout /"));
	_wait(4000);
	_click(_link("/user-login /"));
	_setValue(_textbox("/dwfrm_login_username/"), "nvaradam@pfsweb.com");
	_setValue(_password("/dwfrm_login_password/"), "Crown@123");
	_click(_submit("dwfrm_login_login"));
	_wait(5000);
       //delete existing address
      deleteAddress()
		
       //click on add new address 
	   _click(_link("Addresses", _in(_div("LeftNav"))));
       _click(_link("/ADD NEW/"));
       //Add address
       addAddress($shippingdata,10);
       //click on create address
       _click(_submit("dwfrm_profile_address_create"));
       //click on continue in address
       if (_isVisible(_div("/invalid-address/")))
              {
              _click(_link("Continue"));
              }
       //$credit_card.length
       for(var $i=0;$i<$credit_card.length;$i++)
       {
    	   _log("********************************"+$i+"*******************************")
              //navigate to cart page
              navigateToCart($payment_data[1][7],$payment_data[0][8]);
              //navigate to checkout page
              _click(_link("/mini-cart-link-checkout-link/"));
              
              //comparing product name in shipping page
              var $pnameshippingpage=_getText(_link("/(.*)/", _in(_div("/mini-cart-names box/")))).toString();
              
              if($pName.indexOf($pnameshippingpage)>=0)                            
              {
                     _assert(true);
              }
              
              if (_isVisible(_select("dwfrm_singleshipping_addressList")))
              {
              //select address from drop down
              _setSelected(_select("dwfrm_singleshipping_addressList"),"1");
              }
              
              //address proceed
              adddressProceed();
              
              //Selecting shipping method
             // _check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
              
              //Shipment date
              eventcalendar();
              
              _wait(5000);
              _check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
              var $priceinshippingpage=_getText(_div("order-value", _in(_listItem("OrderTotalNum"))));
              
              _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
              adddressProceed();
              
              var $billingprice=_getText(_div("order-value", _in(_listItem("OrderTotalNum"))));
              //comparing shipping and billing price
              _assertEqual($priceinshippingpage,$billingprice);
              
              //Payment details
              PaymentDetails($credit_card,$i);
                                  
              //Placing order
              _click(_checkbox("/dwfrm_billing_confirm/"));
              _click(_submit("dwfrm_billing_save"));
              
              //Checking order is placed or not
              _assertVisible(_heading1($payment_data[1][3]));
              _assertVisible(_div("order-confirmation-details"));
              
              var $pnameinocppage=_getText(_link("/(.*)/", _in(_div("product-list-item")))).toString();
              if($pnameshippingpage.indexOf($pnameinocppage)>=0)                         
              {
                     _assert(true);
              }

              _assertEqual($prodquantity,_getText(_div("/cartqty/", _in(_div("line-item box-row")))));
              
              
              var $priceinOCP=_extract(_getText(_span("order-price")),"/: (.*)/",true);
              var $ordertotalOCP=_getText(_div("order-value", _in(_listItem("OrderTotalNum"))));
              
              //price verification
              _assertEqual($priceinshippingpage,$priceinOCP);
              _assertEqual($billingprice,$priceinOCP);
              _assertEqual($priceinOCP,$ordertotalOCP);
              
              
              var $orderno=_getText(_span("order-number")).split(" ")[2];
              _log($orderno);
              
              var $orderkkkk=_getText(_span("order-number"))
              _log($orderkkkk);
              
              takescreenshot();
              
              //payment in fo
              _assertVisible(_div("order-payment-instruments"));
              _assertContainsText($credit_card[$i][6], _div("order-payment-instruments"));
              _wait(5000);
       }
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();

/*NavigatetoHomepage();

var $t = _testcase("221852/221854","Verify the Application navigation to Payment Page/UI of the Payment Page./UI of the Review confirmation section/UI of the Apply Promotion Code Section/if the review section checkbox selection on Payment page load");
$t.start();
try
{
//       addItemToCart($payment_data[2][7],$payment_data[0][1]);
//       //navigate to cart
//       _click(_link("/mini-cart-link/"));
//       
//       var $subtotal=_extract(_getText(_div("/(.*)/", _in(_div("box ordersumsnum")),_rightOf(_div("Subtotal:")))),"[$](.*)",true);
//       if(_isVisible(_div("order-shipping")))
//       {
//              var $shippingcharge=_extract(_getText(_div("order-shipping")),"[$](.*)",true);
//       }
//       var $ordertotal=_extract(_getText(_div("order-value")),"/[$](.*)/",true);
//       //Click on checkout link
//       _click(_link("/mini-cart-link-checkout-link/"));
//       //Verify order summary
//       OrderSummary();
//       //Enter values in guest login
//       guestlogin($uId);
//       //Enter values in checkout login
//       //templogin($payment_data[0][2],$payment_data[0][3]);
//       
//       //enter shipping address
//       shippingAddress($Address_data,0);
//       //Uncheck the checkbox
//       _uncheck(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
//       BillingAddress($Address_data,1);
//       eventcalendar();
			 //navigate to cart page
		    navigateToCart($payment_data[1][7],$payment_data[0][8]);
		    //navigate to checkout page
		    _click(_link("/mini-cart-link-checkout-link/"));
		    
		    //comparing product name in shipping page
		    var $pnameshippingpage=_getText(_link("/(.*)/", _in(_div("/mini-cart-names box/")))).toString();
		    
		    if($pName.indexOf($pnameshippingpage)>=0)                            
		    {
		           _assert(true);
		    }
		    
		    if (_isVisible(_select("dwfrm_singleshipping_addressList")))
		    {
		    //select address from drop down
		    _setSelected(_select("dwfrm_singleshipping_addressList"),"1");
		    }
		    
		    //address proceed
		    adddressProceed();
		    
		    //Selecting shipping method
		   // _check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
		    
		    //Shipment date
		    eventcalendar();
		    _wait(3000);
       var $deliveryDate=_getText(_div("/delevery-date/")).replace(/[/]/g,"-");
       //navigate to billing page
       _check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));

       _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
       adddressProceed();
       //delivery date
       var $newdeliverydate=_extract(_getText(_div("delivary-date address-content")),"[:][ ](.*)",true);
       _assertEqual($deliveryDate,$newdeliverydate);
       
       //Patyment section UI
       var $shippingaddress=$Address_data[0][1]+" "+$Address_data[0][2]+" "+$Address_data[0][3]+" "+$Address_data[0][4]+" "+$Address_data[0][7]+", "+$Address_data[0][11]+" "+$Address_data[0][8]+" "+$Address_data[0][5];
       var $billingaddress=$Address_data[1][1]+" "+$Address_data[1][2]+" "+$Address_data[1][12]+" "+$uId+" "+$Address_data[1][3]+" "+$Address_data[1][4]+" "+$Address_data[1][7]+", "+$Address_data[1][11]+" "+$Address_data[1][8]+" "+$Address_data[1][5];  
       
       //should navigate to billing/payment page
       _assertEqual($payment_data[1][0], _getText(_div("address-header-main")));
       //comparing address
       _assertEqual($billingaddress,_getText(_div("address-content", _in(_div("billing-address-section")))));
       _assertEqual($shippingaddress,_getText(_div("address-content", _in(_div("shipping-address-section")))));
       //Verify order summary
       //OrderSummary();
       
       //radio button
       //Credit card
       _assertVisible(_label("Credit Card"));
       _assertVisible(_radio("dwfrm_billing_paymentMethods_selectedPaymentMethodID"));
       _assert(_radio("dwfrm_billing_paymentMethods_selectedPaymentMethodID").checked);
       //Purchase order
       _assertVisible(_label("Purchase Order"));
       _assertVisible(_radio("is-PURCHASE_ORDER"));
       _assertNotTrue(_radio("is-PURCHASE_ORDER").checked);
       //credit card number
       _assertVisible(_span("Credit Card Number"));
       _assertVisible(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"));
       //Security
       _assertVisible(_span("Security Code (CID#)"));
       _assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_cvn"));
       //tooltip
       _assertVisible(_link("tooltip", _rightOf(_textbox("dwfrm_billing_paymentMethods_creditCard_cvn"))))
       //expiration date
       _assertVisible(_span("Expiration Date:"));
       _assertVisible(_select("/dwfrm_billing_paymentMethods_creditCard_expiration_month/"));
       _assertEqual($payment_data[0][4],_getSelectedText(_select("/dwfrm_billing_paymentMethods_creditCard_expiration_month/")));
       _assertVisible(_select("/dwfrm_billing_paymentMethods_creditCard_expiration_year/"));
       _assertEqual($payment_data[0][5], _getSelectedText(_select("/dwfrm_billing_paymentMethods_creditCard_expiration_year/")));
       _assertEqual(true,_radio("input-radio", _in(_div("Credit Card"))).checked);
       //required
       _assertVisible(_div("dialog-required"));
       //checkbox
       _assertVisible(_span($payment_data[1][1]));
       _assertVisible(_checkbox("/dwfrm_billing_confirm/"));
       _assertNotTrue(_checkbox("/dwfrm_billing_confirm/").checked);
       //Promotion
       _assertVisible(_span("Apply Promotion code"));
       _assertVisible(_textbox("dwfrm_billing_couponCode"));
       _assertVisible(_submit("dwfrm_billing_applyCoupon"));
       //
       _assertVisible(_label($payment_data[0][6]));
       _assertVisible(_checkbox("/dwfrm_billing_confirm/"));

       
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();

if(!isMobile())
{
var $t = _testcase("222332/222334/222335/222336/222360","Veirfy the Visa/Master/AMEX/Discover icon behvaior/tooltip in payment section as a Registered user");
$t.start();
try
{      
       //DIsplay of tool tip link in the payment section
       _assertVisible(_link("tooltip", _in(_fieldset("SELECT PAYMENT METHOD"))));
       //ouse hover
       _mouseOver(_link("tooltip", _in(_fieldset("SELECT PAYMENT METHOD"))));
       //display of tool tip
       _assertVisible(_div("ui-tooltip-content"));
       
       
              //Valid Card numbers
              for(var $i=0;$i<$credit_card.length;$i++)
              {
                     
                     _setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"), $credit_card[$i][1]);
              
                     if($i==0 || $i==2)
                     {
                           _assertVisible(_listItem("VS visa-card visa active", _in(_div($paymentcard))));
                     }
                     
                     else if($i==1 || $i==4)
                     {
                           _assertVisible(_listItem("MC master-card mastercard active", _in(_div($paymentcard))));
                     }
                     
                     else if($i==3 || $i==6)
                     {
                           _assertVisible(_listItem("AM amex-card amex active", _in(_div($paymentcard))));
                     }
                     
                     else
                     {
                           _assertVisible(_listItem("DS discover-card discover active", _in(_div($paymentcard))));
                     }
              }
              
              //Invalid numbers
              for(var $i=0;$i<$credit_card.length;$i++)
              {      
                     _setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"), $credit_card[$i][5]);
                     _assertNotVisible(_listItem("VS visa-card visa active", _in(_div($paymentcard))));
                     _assertNotVisible(_listItem("MC master-card mastercard active", _in(_div($paymentcard))));
                     _assertNotVisible(_listItem("AM amex-card amex active", _in(_div($paymentcard))));
                     _assertNotVisible(_listItem("DS discover-card discover active", _in(_div($paymentcard))));
              }      
       
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();
}

var $t = _testcase("222341","Verify the functionality of Review section checkbox as a registered user in Payment Page.");
$t.start();
try
{
       //checkbox being unchecked
       _assertNotTrue(_checkbox("/dwfrm_billing_confirm/").checked);
       //checkbox being checked
       _click(_checkbox("/dwfrm_billing_confirm/"));
       //Place order button should be enabled
       _assertNotTrue(_submit("dwfrm_billing_save").disabled);
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();

var $t = _testcase("222417","Verify the display of Save card option displayed in the Payment section of the Payment page as a Reg user");
$t.start();
try
{
       //Save card text
       _assertVisible(_span("Save Card"));
       //Save card checkbox
       _assertVisible(_checkbox("dwfrm_billing_paymentMethods_creditCard_saveCard"));
       //unchecked by default
       _assertNotTrue(_checkbox("dwfrm_billing_paymentMethods_creditCard_saveCard").checked);
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();

cleanup();

var $t = _testcase("221856","Verify the Delivery Date value displayed in the Payment page as a Guest user");
$t.start();
try
{
       //Navigation till shipping page
       NavigateToCheckoutLogin($payment_data[2][7]);
       //templogin($payment_data[0][2],$payment_data[0][3]);
       
       //shipping details
       shippingAddress($Address_data,0);
       adddressProceed();

       //Delivary date
       var $deliveryDate=_getText(_div("/delevery-date/")).replace(/[/]/g,"-");
       eventcalendar();
       adddressProceed();

       //Navigate to Payment page
       _check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
       _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
     
       var $newdeliverydate=_extract(_getText(_div("delivary-date address-content")),"[:][ ](.*)",true);
       _assertEqual($deliveryDate,$newdeliverydate);
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();

var $t = _testcase("222244/222300/222330/222331","Veirfy the Visa/Master/AMEX/Discover icon behvaior in payment section as a Guest user");
$t.start();
try
{
       if(!isMobile())
       {
              //Valid Card numbers
              for(var $i=0;$i<$credit_card.length;$i++)
              {
                     
                     _setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"), $credit_card[$i][1]);
              
                     if($i==0 || $i==2)
                     {
                           _assertVisible(_listItem("VS visa-card visa active", _in(_div($paymentcard))));
                     }
                     
                     else if($i==1 || $i==4)
                     {
                           _assertVisible(_listItem("MC master-card mastercard active", _in(_div($paymentcard))));
                     }
                     
                     else if($i==3 || $i==6)
                     {
                            _assertVisible(_listItem("AM amex-card amex active", _in(_div($paymentcard))));
                     }
                     
                     else
                     {
                           _assertVisible(_listItem("DS discover-card discover active", _in(_div($paymentcard))));
                     }
              }
              
              //Invalid numbers
              for(var $i=0;$i<$credit_card.length;$i++)
              {      
                     _setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"), $credit_card[$i][5]);
                     _assertNotVisible(_listItem("VS visa-card visa active", _in(_div($paymentcard))));
                     _assertNotVisible(_listItem("MC master-card mastercard active", _in(_div($paymentcard))));
                     _assertNotVisible(_listItem("AM amex-card amex active", _in(_div($paymentcard))));
                     _assertNotVisible(_listItem("DS discover-card discover active", _in(_div($paymentcard))));
              }      
       }
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();

var $t = _testcase("222345","Verify the functionality of Review section checkbox as a Guest user in Payment Page.");
$t.start();
try
{
       //Valid data in all fields
       PaymentDetails($credit_card,0);
       //checkbox being unchecked
       _assertNotTrue(_checkbox("/dwfrm_billing_confirm/").checked);
       //checkbox being checked
       _click(_checkbox("/dwfrm_billing_confirm/"));
       //Place order button should be enabled
       _assertNotTrue(_submit("dwfrm_billing_save").disabled);
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();

var $t = _testcase("222356","Verify the functionality Tool tip icon in payment section of the Payment page as a Guest user.");
$t.start();
try
{
       //DIsplay of tool tip link in the payment section
       _assertVisible(_link("tooltip", _in(_fieldset("SELECT PAYMENT METHOD"))));
       //mouse hover
       _mouseOver(_link("tooltip", _in(_fieldset("SELECT PAYMENT METHOD"))));
       //display of tool tip
       _assertVisible(_div("ui-tooltip-content"));
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();

var $t = _testcase("219902","Verify the application behavior on click of Shipping and Delivery section of the Progress indicator on Payment Page");
$t.start();
try
{
       
//click on Shipping and Delivery section
_click(_link("/(.*)/", _in(_div("step-2 inactive"))));
_assertVisible(_heading2("SHIPPING ADDRESS"));
       
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();

var $t = _testcase("220089","Verify the Validation of the Credit card Text field in Payment Page as a guest user.");
$t.start();
try
{
	_click(_submit("/continue-button /"));
for(var $i=0;$i<$paymentvalidations.length;$i++)
       {
       
       PaymentDetails($paymentvalidations,$i);
       //checkbox being checked
       _check(_checkbox("/dwfrm_billing_confirm/"));
       _click(_submit("dwfrm_billing_save"));
       
       if ($i==0)
              {
              _assertEqual($paymentvalidations[0][5],_getText(_span("/dwfrm_billing_paymentMethods_creditCard_number/")));
              _assertEqual($paymentvalidations[0][6], _getText(_div("form-row cvn required")));

              }
       else if ($i==1 || $i==2 ||$i==3 ||$i==4 || $i==5)
              {
              _assertEqual($paymentvalidations[1][5], _getText(_div("error-form")));
              }
            else if($i==6 || $i==7 || $i==8)
		       {
    	   		_assertEqual($paymentvalidations[1][6], _getText(_div("error-form")));

		       }
       else 
    	   {
    	   _assertEqual($paymentvalidations[2][6], _getText(_div("error-form")));

    	   }
       }


}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();*/

//Add credit card
/*var $t = _testcase("","");
$t.start();
try
{
       //Navigation till shipping page
       NavigateToCheckoutLogin($payment_data[1][2]);
       //guestlogin($uId);
       //shipping details
       shippingAddress($Address_data,0);
       eventcalendar();
       _check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));

       _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
       //select card drop down
       _assertVisible(_label("Select Credit Card"));
       _assertVisible(_select("dwfrm_billing_paymentMethods_creditCardList"));
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();
*/

/*var $t = _testcase("","");
$t.start();
try
{



}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();*/

