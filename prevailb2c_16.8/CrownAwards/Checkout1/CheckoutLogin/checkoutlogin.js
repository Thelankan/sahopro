_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("checkoutlogin.xls");

var $Checkout_data=_readExcelFile("checkoutlogin.xls","Checkout_data");
var $Sheet1=_readExcelFile("checkoutlogin.xls","Sheet1");
var $validation=_readExcelFile("checkoutlogin.xls","validation");



deleteUser_checkoutlogin();
SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

NavigateToCheckoutLogin($Checkout_data[0][0]);

var $t = _testcase("219389","Veirfy the UI of the checkout Login section in the shipping Page");
$t.start();
try
{
	//Guest checkout
	//Heading
	//_assertVisible(_heading2($Checkout_data[0][1]));
	_assertVisible(_div("checkloglabel"));
	_assertEqual("Have an Account? Log In(optional)", _getText(_div("checkloglabel")));
	_click(_link("Log In", _in(_div("checkloglabel"))));
	_click(_link("button", _in(_div("passwd-reset-header-content"))));
	
	//_assertVisible(_link("Create a Login"));
	_assertVisible(_submit("dwfrm_profile_confirm"));

	//Log in section
	//vishal
	_assertVisible(_heading2($Checkout_data[3][1], _in(_div("dialog-container"))));
	_assertVisible(_heading1("Returning Customers"));
	_assertVisible(_div($Checkout_data[6][1], _in(_div("dialog-container"))));
	//Email
	_assertVisible(_span("/"+$Checkout_data[4][1]+"/", _in(_div("dialog-container"))));
	//vishal
	_assertVisible(_textbox("/dwfrm_login_username/", _in(_div("dialog-container"))));
	//Password
	_assertVisible(_password("/dwfrm_login_password/", _in(_div("dialog-container"))));
	//Forgot password
	
	if(isMobile())
		{
		_assertVisible(_link("Forgot Password?", _in(_div("Forgot Password?[1]"))));
		}
	else
		{
		_assertVisible(_link("Forgot Password?", _in(_div("dialog-container"))));
		}
	//Log in button
	_assertVisible(_submit("dwfrm_login_login", _in(_div("dialog-container"))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219408/219409","Verify the application behvaior on click of Forgot Password link in returning customer section/UI of the Password Reset overlay of the shipping Page");
$t.start();
try
{    //Forgot Password
	//vishal
	//_click(_link("Forgot Password", _in(_div("dialog-container"))));
	_click(_link("Forgot Password?", _in(_div("dialog-container"))));
	//Heading
	_assertVisible(_heading1($Checkout_data[0][2]));
	//Paragraph
	_assertVisible(_paragraph($Checkout_data[1][2]));
	//Email
	_assertVisible(_span("Email", _in(_div("passwd-reset-header-form"))));
	_assertVisible(_textbox("dwfrm_requestpassword_email", _in(_div("passwd-reset-header-form"))));
	//close icon
	_assertVisible(_button("Close"));
	//Submit
	_assertVisible(_submit("dwfrm_requestpassword_send"));
	//Cancel button
	//_assertVisible(_submit("cancel-return "));
	_assertVisible(_submit("cancel-return onlycancelbtn"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219428","Verify the UI of the Password reset confirmation overlay after Password reset when launched through shipping Page");
$t.start();
try
{
	_setValue(_textbox("dwfrm_requestpassword_email", _in(_div("passwd-reset-header-form"))), $uId);
	_click(_submit("dwfrm_requestpassword_send"));
	//Reset password confirmation
	if(isMobile())
		{
		_assertVisible(_heading2($Checkout_data[2][2]));
		}
	_assertVisible(_heading1($Checkout_data[2][2]));
	//close icon
	_assertVisible(_button("Close"));
	//Static text
	_assertVisible(_paragraph($Checkout_data[3][2]));
	//Cancel
	//_assertVisible(_submit("cancel-return "));
	//_assertVisible(_submit("cancel-btn"));
	_assertVisible(_submit("cancel-btn", _in(_div("dialog-container"))));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219412","Verify the application behavior on click of close icon in the password reset overlay when launched through shipping Page.");
$t.start();
try
{
	//close icon
	_click(_button("Close"));
	_assertNotVisible(_heading1($Checkout_data[2][2]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("221524","Verify if the user can Continue as a guest through Checkout Login section ");
$t.start();
try
{
	
    //Email
    _setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email"), $uId);
    //Confirm email
    _setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail"), $uId);
    //Enter shipping values
    //First name
    _setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), $Checkout_data[13][0]);
    //Last name
    _setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), $Checkout_data[13][1]);
    //address 1
    _setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"), $Checkout_data[13][2]);
    //Zip code
    _setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $Checkout_data[13][3]);
    //City
    _setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), $Checkout_data[13][5]);
    //Country
    _setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"), $Checkout_data[13][6]);
    //State
    _setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), $Checkout_data[13][7]);
    //Phone
    _setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"), $Checkout_data[13][4]);
    
    _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	//event calendar
	eventcalendar();
	
	//Selecting shipping method
	_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
	
	//Submit
	_click(_submit("/continue-button/"));
	
	adddressProceed();
	
	//displaying and verifying payment page
   _assertVisible(_div("Payment & Place Order"));
   _assertVisible(_div("/checkout-mini-cart CartSummaryWrap/", _in(_div("secondary"))));
   _assertVisible(_div("checkout-order-total CartSummaryWrap", _in(_div("secondary"))));
   _assertVisible(_div("address-details"));
   _assertVisible(_fieldset("Select Payment Method"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end()


var $t = _testcase("219390,219402","Verify the UI of the Checkout as guest section in shipping Page as a guest user.");
$t.start();
try
{
	
	navigateToCart($Sheet1[0][5],$Sheet1[6][0]);
	
	//UI of the shipping page
	//_click(_image("/Checkout/", _in(_div("header-right"))));
	_click(_link("/checkout/", _in(_div("header-right"))))
	_assertVisible(_div("checkloglabel"));
	_assertEqual("Have an Account? Log In(optional)", _getText(_div("checkloglabel")));
	_assertVisible(_link("Log In", _in(_div("user-section checkoutlogin"))));
	_click(_link("Log In", _in(_div("user-section checkoutlogin"))));
	_assertVisible(_div("dialog-container"));
	_assertVisible(_heading1("Returning Customers"));
	_assertVisible(_heading2("New Customers"));

	_assertVisible(_link("button", _in(_div("dialog-container"))));
	_assertVisible(_link($Sheet1[0][9], _in(_div("dialog-container"))));
	_assertVisible(_submit("dwfrm_login_login", _in(_div("dialog-container"))));

	
}

catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("219404,219405,219407","verifying the login validation for email and password field in checkout login page");
$t.start();
try
{
	//validating the email id & password field's
	for(var $i=0;$i<$validation.length;$i++)
		{
			_setValue(_textbox("/dwfrm_login_username/", _in(_div("dialog-container"))), $validation[$i][1]);
			_setValue(_password("/dwfrm_login_password/", _in(_div("dialog-container"))),$validation[$i][2]);
			_click(_submit("dwfrm_login_login", _in(_div("dialog-container"))));
			_wait(3000);
		//blank
		if($i==0)
			{
			//
			//textbox color
			_assertEqual($validation[0][5],_style(_textbox("/dwfrm_login_username/", _in(_div("dialog-container"))), "background-color"));
			//error msg color
			_assertEqual($validation[0][6],_style(_span($validation[5][3]),"color"));
			_assertVisible(_span($validation[5][3]));
			
			}
		
		//invalid email and password
		else if($i==1)
			{
			//email id error msg
			_assertVisible(_div($validation[1][3]));
			_assertEqual($validation[0][5],_style(_textbox("/dwfrm_login_username/", _in(_div("dialog-container"))), "background-color"));
			_assertEqual($validation[0][6],_style(_span($validation[1][3]),"color"));

			//password error msg
			_assertVisible(_div($validation[1][4]));
			_assertEqual($validation[0][5],_style(_password("/dwfrm_login_password/"), "background-color"));
			_assertEqual($validation[0][6],_style(_span($validation[1][4]),"color"));

			}
		//invalid email 
		else if($i==2||$i==3) 
			{
			_assertVisible(_div($validation[1][3]));
			_assertEqual($validation[0][5],_style(_textbox("/dwfrm_login_username/", _in(_div("dialog-container"))), "background-color"));
			_assertEqual($validation[0][6],_style(_span($validation[1][3]),"color"));

			}
		//non existing email id
		else if($i==4)
			{
			_assertVisible(_div($validation[4][3]));
			//_assertEqual($validation[0][6],_style(_span($validation[1][3]),"color"));

			}
		else
			{
			//check out section not visible
			_assertNotVisible(_div("inter-login"));
			_assertNotVisible(_div("guest-login"));
			_assertNotVisible(_div("dialog-container"));
			_assertNotVisible(_heading1("Returning Customers"));
			_assertNotVisible(_heading2("New Customers"));
			}
		}
	
	
	
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


/*
_log("######## Dhiraj #########");

var $t = _testcase("12345","Create Account in checkout and place order");
$t.start();
try
{
	
	NavigateToCheckoutLogin($Checkout_data[0][0]);
	_wait(2000);
	//click on create account
	_click(_link("Create a Login", _in(_div("login-box-content clearfix"))));
	//enter the values
	Createaccount_Checkout($Sheet1,0);
	//enter shipping address
	shippingAddress_Checkout($Checkout_data,13)
	//event calendar
	eventcalendar();
	
	//Selecting shipping method
	_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
	
	//Submit
	_click(_submit("/continue-button/"));
	
	_wait(3000);
	//address proceed
	adddressProceed();
	       
    _assertVisible(_link("Log Out", _in(_div("user-section"))));
    _assertVisible(_div($Checkout_data[12][1], _in(_div("address-content"))));
	_assertVisible(_link($Checkout_data[17][3]));
    _assertVisible(_div($Checkout_data[17][0]));
    _assertVisible(_div($Checkout_data[17][1]));
    _assertVisible(_div($Checkout_data[17][2]));
	PaymentDetails($Checkout_data,16);
	//Placing order
	_click(_checkbox("/dwfrm_billing_confirm/"));
	_click(_submit("dwfrm_billing_save"));
	_wait(3000);
	//order confirmation details
	_assertVisible(_div("order-confirmation-details"));
	var $orderno=_getText(_span("order-number")).split(" ")[2];
	_log($orderno);
	
	var $orderkkkk=_getText(_span("order-number"))
	_log($orderkkkk);
	_click(_link("Log Out", _in(_div("user-section"))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/

//_log("############## functions #############")

function deleteUser_checkoutlogin()
{
	BM_Login();
_click(_link("Customers"));
_click(_link("Customers[1]"));
_click(_link("Advanced"));
_setValue(_textbox("WFCustomerAdvancedSearch_Email"),$Sheet1[0][1]);
_click(_submit("Find", _in(_div("/Advanced Customer Search/"))));
if(_isVisible(_row("/Select All/")))
{
	if(_assertEqual($email,_getText(_cell("table_detail e s[4]"))))
		{
		_click(_checkbox("DeleteCustomer"));
		_click(_submit("Delete"));
		_click(_submit("OK"));
		_log("Successfully deleted the user")
		}
}
else
{
	_log("search did not match any users.");
}
_click(_link("Log off."));

}

function Createaccount_Checkout($sheet,$row)
{
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email", _in(_div("login-box-content clearfix"))),$sheet[$row][1]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail", _in(_div("login-box-content clearfix"))),$sheet[$row][2]);
	_setValue(_password("/dwfrm_singleshipping_shippingAddress_addressFields_passwordFields_password/", _in(_div("login-box-content clearfix"))),$sheet[$row][3]);
	_setValue(_password("/dwfrm_singleshipping_shippingAddress_addressFields_passwordFields_confirmpassword/", _in(_div("login-box-content clearfix"))),$sheet[$row][4]);
}
function shippingAddress_Checkout($sheet,$i)
{
		
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), $sheet[$i][0]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), $sheet[$i][1]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"), $sheet[$i][2]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $sheet[$i][3]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),$sheet[$i][4]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), $sheet[$i][5]);
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"), $sheet[$i][6]);
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), $sheet[$i][7]);
	
}













