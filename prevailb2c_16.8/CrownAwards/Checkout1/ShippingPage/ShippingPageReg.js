_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("ShippingPage.xls");

var $shipaddress=_readExcelFile("ShippingPage.xls","ShipAddress");
var $billaddress=_readExcelFile("ShippingPage.xls","BillAddress");
var $shippingdata=_readExcelFile("ShippingPage.xls","ShippingData");
var $validations=_readExcelFile("ShippingPage.xls","Validations");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

if (isMobile())
{
 _assertVisible(_link("/mobile-show/", _in(_div("user-info"))));
 _click(_link("/mobile-show/", _in(_div("user-info"))));
}
else
	{
	 _assertVisible(_link("/desktop-show/", _in(_div("user-info"))));
	 _click(_link("/desktop-show/", _in(_div("user-info"))));
	 //Visibility of overlay
	 _assertVisible(_div("dialog-container"));
	}

//login to the app
login();
_wait(5000);

//navigate to my account landing page
NavigatetoAccountLanding();
//click on address link
_click(_link("ADDRESSES"));
//delete address
deleteAddress();
//navigate to my account landing page
NavigatetoAccountLanding();
_click(_link("Make a Payment"));
//deleting card
DeleteCreditCard();

//vinay lanka 333-333-3333 vinay@gmail.com microsoft way ELMWOOD PARK, IL 98052 [shipping address format in payment page]
var $shippingaddress=$shipaddress[0][1]+" "+$shipaddress[0][2]+" "+$shipaddress[0][9]+" "+$uId+" "+$shipaddress[0][3]+" "+$shipaddress[0][4]+" "+$shipaddress[0][7]+", "+$shipaddress[0][11]+" "+$shipaddress[0][8];
//vinay lanka 333-333-3333 vinay@gmail.com microsoft way ELMWOOD PARK, IL 98052 [billing address format in payment page]
var $billingaddress=$billaddress[1][1]+" "+$billaddress[1][2]+" "+$billaddress[1][9]+" "+$uId+" "+$billaddress[1][3]+" "+$billaddress[1][4]+" "+$billaddress[1][7]+", "+$billaddress[1][11]+" "+$billaddress[1][8];	

var $t = _testcase("219486/222785/219468/219494/219478/222532","Verify the application navigation on click of Product name in the cart summary section for a REG user./Verify the Default Selection of Save Address option in shipping page as a REG user./Verify the Login section is displayed for a Registered User" +
		"Verify the UI of the Shipping address form as a REG user.");
$t.start();
try
{
_wait(2000);
//navigate to cart page with single 
navigateToCart($shippingdata[0][0],$shippingdata[0][1]);
//click mini cart link
_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));
//product name should be same
//_assertEqual($prodname,$pName);
//navigate to checkout page
_click(_link("/mini-cart-link-checkout-link/"));
//navigate to shipping page
_assertVisible(_heading2("Shipping Address"));
_assertVisible(_div("single-ship address-fields"));
_assertVisible(_div("/ checkout-progress-indicator/"));

//shipping page UI
shippingAddressUI();

//UI of progress indicator
_assertVisible(_div("Shopping Cart Review Your Cart", _in(_div("/ checkout-progress-indicator/"))));
_assertVisible(_div("step-1 inactive"));
_assertVisible(_div("Shipping & Delivery", _in(_div("/ checkout-progress-indicator/"))));
_assertVisible(_div("step-2 active"));
_assertVisible(_div("Payment & Place Order", _in(_div("/ checkout-progress-indicator/"))));
_assertVisible(_div("step-3 inactive"));

//Returning customer section should not display as a register user
//_assertNotVisible(_heading2($shippingdata[0][3], _in(_div("login-box"))));
//_assertNotVisible(_heading2($shippingdata[1][3], _in(_div("login-box login-account"))));

//Add to address section should display
_assertVisible(_div("Save Address to Address Book"));
_assertVisible(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook"));
//Add to address section should be un checked by default
_assertEqual(false,(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook").checked));

//prod name in shipping page
var $prodnameinshippingpage=_getText(_link("/(.*)/", _in(_div("/mini-cart-names/"))));

//click on product name in cart summary in shipping page
_click(_link("/(.*)/", _in(_div("/mini-cart-names box/"))));
_expectConfirm("/Are you sure you want to leave this page/", true);

_assertVisible(_heading2("product-name"));

//should navigate to PD page
//var $prodnameinPDP=_getText(_heading2("product-name"));
//_assertEqual($prodnameinshippingpage,$prodnameinPDP);

//navigate to checkout page
_click(_link("/mini-cart-link-checkout-link/"));

//click on edit cart link in cart summary page in shipping page
_click(_link("Edit Cart", _in(_heading3("CartSummaryHeader"))));
_expectConfirm("/Are you sure you want to leave this page/", true);

//should navigate to cart page
_assertVisible(_div("CartWrap"));

//capturing product name in cart page
//var $productnameincartpage=_extract(_getText(_link("/(.*)/", _in(_div("name")))),"/(.*) -/",true);
//_assertEqual($prodnameinshippingpage,$productnameincartpage);


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("222792/222789/222793","Veirfy the Pre poplutaion of city, state, country values in the Shipping form when a 10 Zip code as a REG user./Veirfy the Pre poplutaion of city, state, country values in the Shipping form when a 5 Zip code as a REG user.");
$t.start();
try
{

//navigate to checkout page
_click(_link("/mini-cart-link-checkout-link/"));

//zip code,city,country,state should be empty
_assertEqual("",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")));
_assertEqual("",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")));
_assertEqual("Select...",_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));

//entering 5-digit zip code 
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"),$shippingdata[0][2]);
_wait(2000);
//values should pre populate
_assertEqual($shippingdata[0][2],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")));
_assertEqual($shippingdata[1][2],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")));
_assertEqual($shippingdata[2][2],_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
_assertEqual($shippingdata[3][2],_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_country")));

//entering 10-digit zip code 
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"),$shippingdata[0][4]);
_wait(2000);
//values should pre populate
_assertEqual($shippingdata[0][4],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")));
_assertEqual("/"+$shippingdata[1][4]+"/i",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")));
_assertEqual($shippingdata[2][4],_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
_assertEqual($shippingdata[3][4],_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_country")));



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("222531/222569","Verify the Ui of the Billing address form as a REG user./Verify the functionality of Use As Billing Address checkbox in shipping page");
$t.start();
try
{
	if (_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked)
		{
		_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"))
		_assertEqual(true,(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked))
		_uncheck(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"))
		}

	billingPageUI();
	
_assertEqual(false,(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked));	
//un checking the check box
_uncheck(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"))
_assertNotVisible(_div("billing-fields"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219483/222557/219505/222559/219506/219507/222561/219510/222550/222552","validations");
$t.start();
try
{

	for (var $i=0;$i<$validations.length;$i++) 
		{
		
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), $validations[$i][1]);
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), $validations[$i][2]);
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"), $validations[$i][3]);
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"),$validations[$i][4]);
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $validations[$i][8]);
			_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$validations[$i][5]);
			_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), $validations[$i][6]);
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), $validations[$i][7]);
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),$validations[$i][9]);
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email"),$validations[$i][10]);
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail"),$validations[$i][10]);
		
		if ($i==0 || $i==6)
			{
			
			//click on submit button
			//_click(_submit("continue-button handledmixedpopup"));
			_assertEqual(false,(_submit("continue-button handledmixedpopup").disabled));
			
	
			}
		else if ($i==1)
			{
			
			_assertEqual($validations[0][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName")).length);
			_assertEqual($validations[0][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName")).length);
			_assertEqual($validations[2][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1")).length);
			_assertEqual($validations[2][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2")).length);
			_assertEqual($validations[2][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")).length);
			_assertEqual($validations[3][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone")).length);
			_assertEqual($validations[4][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")).length);
			_assertEqual($validations[1][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email")).length);
			_assertEqual($validations[1][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail")).length);
			
			}
		else if ($i==2)
			{
			
			//click on submit button
			//_click(_submit("continue-button handledmixedpopup"));
	
			_assertEqual($validations[0][14],_getText(_span("dwfrm_singleshipping_shippingAddress_addressFields_postal-error")));
			_assertEqual($validations[1][14],_getText(_span("dwfrm_singleshipping_shippingAddress_addressFields_phone-error")));
			
			}
			else if ($i==3 || $i==4 || $i==5)
			{
				
			//click on submit button
			//_click(_submit("continue-button handledmixedpopup"));
	
			_assertEqual($validations[0][14],_getText(_span("dwfrm_singleshipping_shippingAddress_addressFields_postal-error")));
			_assertEqual($validations[0][15],_getText(_span("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail-error")));
			_assertEqual($validations[2][14],_getText(_span("dwfrm_singleshipping_shippingAddress_addressFields_phone-error")));
			
			}
			else
			{
			_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"))
				//event calendar
				eventcalendar();
				_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
				//click on submit button
				_click(_submit("continue-button handledmixedpopup"));
			
				adddressProceed();
				//should navigate to billing/payment page
				_assertEqual($shippingdata[1][0], _getText(_div("address-header-main")));
			
			}
		
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//click on your orders button	
_click(_link("Your Orders"));
//navigate to my account page
_click(_link("My Account", _in(_div("breadcrumb"))));
_click(_link("ADDRESSES"));

//delete existing address
deleteAddress()

//click on add new address 
_click(_link("/ADD NEW/"));
//Add address
addAddress($shippingdata,10);
//click on create address
_click(_submit("dwfrm_profile_address_create"));
//click on continue in address
if (_isVisible(_div("/invalid-address/")))
	 {
	 _click(_link("Continue"));
	 }
_click(_link("Crown Awards"));
var $t = _testcase("219623/222545/222546","Verify the display of Select an Address Drop down in shipping section as a REG user./Verify the Default value seen in the shipping addres drop down in shipping page./Verify the Select shipping address functionality in Shipping Page");

$t.start();
try
{
	_wait(2000);
//navigate to cart page with single 
navigateToCart($shippingdata[0][0],$shippingdata[0][1]);
//navigate to checkout page
_click(_link("/mini-cart-link-checkout-link/"));

if (_isVisible(_select("dwfrm_singleshipping_addressList")))
	{
	//select address from drop down
	_setSelected(_select("dwfrm_singleshipping_addressList"),"2");
	}

adddressProceed();
//_setSelected(_select("dwfrm_singleshipping_addressList"),"("+$shipaddress[3][9]+")"+" "+$shipaddress[3][5]+" "+$shipaddress[3][9]+" "+$shipaddress[3][8]+" "+$shipaddress[3][10]);
//_setSelected(_select("dwfrm_singleshipping_addressList"), "("+ELMWOOD PARK+")" 25 call st ELMWOOD PARK IL 60707");
//comparing same address is selecting or not
_assertEqual("/"+$shippingdata[10][1]+"/i",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email")));
_assertEqual("/"+$shippingdata[10][2]+"/i",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail")));
_assertEqual("/"+$shippingdata[10][3]+"/i",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName")));
_assertEqual("/"+$shippingdata[10][4]+"/i",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName")));
_assertEqual($shippingdata[10][5],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1")));
_assertEqual($shippingdata[10][7],_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_country")));
_assertEqual($shippingdata[10][8],_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
_assertEqual($shippingdata[10][9],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")));
_assertEqual($shippingdata[10][10],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")));
_assertEqual($shippingdata[10][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone")));




//enter the different detial in shipping fields
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email"),$shippingdata[11][1]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail"),$shippingdata[11][2]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), $shippingdata[11][3]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), $shippingdata[11][4]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"), $shippingdata[11][5]);

_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$shippingdata[11][7]);
_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), $shippingdata[11][8]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), $shippingdata[11][9]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $shippingdata[1][10]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),$shippingdata[11][11]);

if (_isVisible(_select("dwfrm_singleshipping_addressList")))
{
//select address from drop down
_setSelected(_select("dwfrm_singleshipping_addressList"),"2");
}
else
	{
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), $shippingdata[14][1]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), $shippingdata[14][2]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"), $shippingdata[14][3]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"),$shippingdata[14][4]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $shippingdata[14][8]);
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$shippingdata[14][5]);
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), $shippingdata[14][6]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), $shippingdata[14][7]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),$shippingdata[14][9]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email"),$shippingdata[14][10]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail"),$shippingdata[14][10]);
	}


//comparing same address is selecting or not
_assertEqual("/"+$shippingdata[10][1]+"/i",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email")));
_assertEqual("/"+$shippingdata[10][2]+"/i",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail")));
_assertEqual("/"+$shippingdata[10][3]+"/i",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName")));
_assertEqual("/"+$shippingdata[10][4]+"/i",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName")));
_assertEqual($shippingdata[10][5],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1")));
_assertEqual($shippingdata[10][7],_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_country")));
_assertEqual($shippingdata[10][8],_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
_assertEqual($shippingdata[10][9],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")));
_assertEqual($shippingdata[10][10],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")));
_assertEqual($shippingdata[10][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone")));

_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"))
//click on continue in address
if (_isVisible(_div("/invalid-address/")))
	 {
	 _click(_link("Continue"));
	 }

//event calendar
eventcalendar();
_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
//click on continue
//_assertVisible(_submit("/continue-button/"));
//_click(_submit("/continue-button/"));
_assertVisible(_submit("continue-button handledmixedpopup"));
_click(_submit("continue-button handledmixedpopup"));

//verify the address
adddressProceed();

//displaying and verifying payment page
_assertVisible(_div("Payment & Place Order"));
_assertVisible(_div("/checkout-mini-cart CartSummaryWrap/", _in(_div("secondary"))));
_assertVisible(_div("checkout-order-total CartSummaryWrap", _in(_div("secondary"))));
_assertVisible(_div("address-details"));
_assertVisible(_fieldset("Select Payment Method"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("222784/222788/222786","Verify the display of Save Address to Address book option to a REg user./Verify the when the Save card functionality when the user has not clicked on Continue button of the shipping Page.");
$t.start();
try
{

	//delete address
	deleteAddress();
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
	//shipping form should display
	shippingAddress($shipaddress,1);
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"))
	adddressProceed();

	_wait(5000);
	//display of add to address checkbox
	_assertVisible(_span("Save Address to Address Book"));
	_assertVisible(_div("Save Address to Address Book"));
	_assertVisible(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook"));
	//it should not be checked
	_assertEqual(false,(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook").checked));
	//event calendar
	eventcalendar();


	//click on continue in shipping page
	_assertVisible(_submit("continue-button handledmixedpopup"));
	_click(_submit("continue-button handledmixedpopup"));
	//verify the address
	adddressProceed();
	//should navigate to billing/payment page
	_assertVisible(_div("Payment & Place Order"));
	_assertVisible(_div("/checkout-mini-cart CartSummaryWrap/", _in(_div("secondary"))));
	_assertVisible(_div("/checkout-order-total CartSummaryWrap/", _in(_div("secondary"))));
	_assertVisible(_div("address-details"));
	_assertVisible(_fieldset("Select Payment Method"));

	//navigate to my account landing page
	NavigatetoAccountLanding();
	//click on address link
	_click(_link("ADDRESSES"));
	_assertNotVisible(_div($shipaddress[1][3]+" "+$shipaddress[1][4]+" "+$shipaddress[1][7]+", "+$shipaddress[1][8]+" "+$shipaddress[1][5]+" "+$shipaddress[1][12]+" "+$shipaddress[1][10]));

	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
	//shipping form should display
	shippingAddress($shipaddress,1);
	adddressProceed();
	//click on check box
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook"));
	//event calendar
	eventcalendar();
	//click on continue in shipping page
	_click(_submit("continue-button handledmixedpopup"));
	adddressProceed();
	//should navigate to billing/payment page
	//should navigate to billing/payment page
	_assertVisible(_div("Payment & Place Order"));
	_assertVisible(_div("/checkout-mini-cart CartSummaryWrap/", _in(_div("secondary"))));
	_assertVisible(_div("/checkout-order-total CartSummaryWrap/", _in(_div("secondary"))));
	_assertVisible(_div("address-details"));
	_assertVisible(_fieldset("Select Payment Method"));

	//navigate to my account landing page
	NavigatetoAccountLanding();

	//click on address link
	_click(_link("ADDRESSES"));
	_assertVisible(_div($shipaddress[1][13]+" "+$uId));
	
	_click(_link("/mini-cart-link-checkout-link/"));
	//event calendar
	eventcalendar();
	//click on continue in shipping page
	_click(_submit("continue-button handledmixedpopup"));

	//enter payment details
	PaymentDetails($shipaddress,5);
	//check the check box
	_check(_checkbox("/dwfrm_billing_confirm/"));
	//place order button
	_click(_submit("dwfrm_billing_save"));
	//order confirmation page
	_assertVisible(_heading1($shippingdata[0][5]));
	_assertVisible(_div($shippingdata[1][5]));

	var $orderno=_getText(_span("order-number")).split(" ")[2];
	_log($orderno);

	var $orderkkkk=_getText(_span("order-number"))
	_log($orderkkkk);

	//navigate to my account landing page
	NavigatetoAccountLanding();
	//click on address link
	_click(_link("ADDRESSES"));
	//verifying address is saved or not
	//_assertVisible(_div("333-333-3333 VINAY@GMAIL.COM 71 Old Gate Avenue Fairfield, CA 89680 United States"));
	//_assertVisible(_div($shipaddress[1][3]+" "+$shipaddress[1][4]+" "+$shipaddress[1][7]+", "+$shipaddress[1][8]+" "+$shipaddress[1][5]+" "+$shipaddress[1][12]+" "+$shipaddress[1][10]));
	_assertVisible(_div($shipaddress[1][12]));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("222788","Verify the when the Save card functionality when the user has not clicked on Continue button of the shipping Page.");
$t.start();
try
{
	
//click mini cart link
_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));
//product name should be same
//_assertEqual($prodname,$pName);
//navigate to checkout page
_click(_link("/mini-cart-link-checkout-link/"));
//shipping form should display
shippingAddress($shipaddress,1);
_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"))
adddressProceed();
//event calendar
eventcalendar()
_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
//click on continue in shipping page
_click(_submit("continue-button handledmixedpopup"));
//address proceed
adddressProceed();
//enter and save the payment address
PaymentDetails($shipaddress,5);
//click on save card
_click(_checkbox("dwfrm_billing_paymentMethods_creditCard_saveCard"));
//navigate to profile and check for saved card
_check(_checkbox("/dwfrm_billing_confirm/"));
//place order
_click(_submit("dwfrm_billing_save"));
//order confirmation page
_assertVisible(_heading1($shippingdata[0][5]));
_assertVisible(_div($shippingdata[1][5]));

var $orderno=_getText(_span("order-number")).split(" ")[2];
_log($orderno);

var $orderkkkk=_getText(_span("order-number"))
_log($orderkkkk);

//navigate to my account landing page
NavigatetoAccountLanding();
//navigate to payment setting page
_click(_link("PAYMENT SETTINGS"));
//verifying payment setting page
  _assertVisible(_link("Payment Settings", _in(_div("breadcrumb"))));
  _assertVisible(_list("payment-list"));
_assertVisible(_heading1("Payment Settings"))
_assertVisible(_submit("Edit"));
_assertVisible(_submit("Delete"));
_assertVisible(_submit("Add new"));
_assertVisible(_div("cc-type"));
_assertVisible(_div("cc-number"));
_assertVisible(_div("cc-exp"));

//verifying the card  details
_assertEqual($shippingdata[5][5], _getText(_div("cc-number")));
_assertEqual($shippingdata[5][7], _getText(_div("cc-exp")));
_assertEqual($shippingdata[5][6], _getText(_div("cc-type")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("222540/222541/222542","Verify the Select Calender date event in the shipping page/Verify the display of Tool tip icon in the Select Event date section./Verify the application behavior on click of Tool tip icon displayed in the Select Event date Section.");
$t.start();
try
{
	_wait(2000);
    //navigate to cart page with single 
	navigateToCart($shippingdata[0][0],$shippingdata[0][1]);
	//click mini cart link
	_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));
	//product name should be same
	//_assertEqual($prodname,$pName);
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
	//navigate to shipping page
	_assertVisible(_heading2("Shipping Address"));
	_assertVisible(_div("single-ship address-fields"));
	_assertVisible(_div("/ checkout-progress-indicator/"));

	//enter address
	shippingAddress($shipaddress,1);
	adddressProceed();
	
	//should select the date 
	var $currentyear=_getText(_div("/delevery-date/")).split("/")[2];
	_click(_textbox("dwfrm_singleshipping_datePicker_datePickerSelection"));
	_assertVisible(_span("ui-datepicker-year"));
	//calendar should be displayed
	_assertVisible(_table("ui-datepicker-calendar"));
	_assertVisible(_div("ui-datepicker-div"));
	//select random date
	for (var $i=0;$i<14;$i++)
		{
		//clcik on next button
		_click(_span("Next"));
		}
	//click link 25
	_click(_link("25",_in(_table("ui-datepicker-calendar"))));
	var $changeddate=_getText(_textbox("dwfrm_singleshipping_datePicker_datePickerSelection")).split(" ")[1].replace(","," ");
	var $changedyear=_getText(_textbox("dwfrm_singleshipping_datePicker_datePickerSelection")).split(" ")[2];
	_assert($currentyear<$changedyear);	
	_assertEqual("25",$changeddate);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("222975","Verify the display of tool tip icon in the shipping section next to the shipping Price value");
$t.start();
try
{

	//shipping method selection verification
	var $shippingmethods=_collectAttributes("_div","ship-details","sahiText",_in(_div("ship-table")));

	var $j=0;
	for (var $i=0;$i<$shippingmethods.length;$i++)
		{
		
		if ($i%2!==0)
			{
			_assertVisible(_link("tooltipimg["+$i+"]",_near(_label($shippingmethods[$j]))));
			$j++;
			}
		
		}	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("223269","Verify the application when the user removes all the products from Cart in shipping page.");
$t.start();
try
{


var $countofprodsincartsummary=_count("_div","/mini-cart-product-list box-row/", _in(_div("CartTableLeft")));

if ($countofprodsincartsummary>1)
	{
	var $pricebeforedeleting=_getText(_div("order-value", _in(_listItem("OrderTotalNum"))));
	_click(_link("Remove Item"));
	var $priceafterdeleting=_getText(_div("order-value", _in(_listItem("OrderTotalNum"))));
	_assertNotEqual($pricebeforedeleting,$priceafterdeleting);
	var $countofprodsafterdeleting=_count("_div","/mini-cart-product-list box-row/", _in(_div("CartTableLeft")));
	_assertEqual($countofprodsincartsummary-1,$countofprodsafterdeleting);
	}
	else
		{
		_click(_link("Remove Item"));
		_assertVisible(_heading1($shippingdata[2][5]));
		_assertVisible(_submit("dwfrm_cart_continueShopping"));
		}


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("222547","Verify the Pre population of Address in billing form when the Select Shipping address value is selected.");
$t.start();
try
{
	//navigate to my account landing page
	//delete address
	deleteAddress();

	//login to the app
	//login();
	_wait(5000);
	//click on your orders button	
	_click(_link("Your Orders"));
	//navigate to my account page
	_click(_link("My Account", _in(_div("breadcrumb"))));
	_click(_link("ADDRESSES"));
	//click on add new address 
	_click(_link("/ADD NEW/"));
	//Add address
	addAddress($shippingdata,11);
	//click on create address
	_click(_submit("dwfrm_profile_address_create"));
	_click(_link("Crown Awards"));
	_wait(2000);
	//navigate to cart page with single 
	navigateToCart($shippingdata[0][0],$shippingdata[0][1]);
	//click mini cart link
	_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));
	//product name should be same
	//_assertEqual($prodname,$pName);
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
	//navigate to shipping page
	_assertVisible(_heading2("Shipping Address"));
	_assertVisible(_div("single-ship address-fields"));
	_assertVisible(_div("/ checkout-progress-indicator/"));

	//shipping page UI
	shippingAddressUI();
	
	//select address from drop down
	if(_isVisible(_select("dwfrm_singleshipping_addressList")))
		{
		_assertVisible(_div("select-address form-row"));
		_setSelected(_select("dwfrm_singleshipping_addressList"),1);
		}
	

	_uncheck(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"))

   billingPageUI();
   //select the existing address for billing
   _setSelected(_select("dwfrm_billing_addressList"),1);
   
   
   //verify the prepopulated values
  //checkout login
   var $var1=_getText(_textbox("dwfrm_billing_billingAddress_email_emailAddress"));
   var $var2=_getText(_textbox("dwfrm_billing_billingAddress_email_confirmemail"));
   var $var3=_getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"));
   var $var4=_getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"));
   var $var5=_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1"));
   var $var6=_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address2"));
   var $var7=_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country"));
   var $var8=_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state"));
   var $var9=_getText(_textbox("dwfrm_billing_billingAddress_addressFields_city"));
   var $var10=_getText(_textbox("dwfrm_billing_billingAddress_addressFields_postal"));
   var $var11=_getText(_textbox("dwfrm_billing_billingAddress_addressFields_phone"));
   
          _assertEqual($var1,_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email")));
          _assertEqual($var2,_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail")));
         
          //Shipping section
          _assertEqual($var3,_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName")));
          _assertEqual($var4,_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName")));
          _assertEqual($var5,_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1")));
          _assertEqual($var6,_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2")));
          _assertEqual($var7,_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_country")));
          _assertEqual($var8,_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
          _assertEqual($var9,_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")));
          _assertEqual($var10,_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")));
          _assertEqual($var11,_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone")));
   }
    
 
  

catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("226646/226653/226654/226655/226656/226678/226679/226657/226660/226676","Verify the Functionality of the EDIT cart link in the cart summary section of the Shipping Page/" +
		"Verify of each line displayed in the Cart Summary section/" +
		"Verify the application behavior on click of Product Image in the Cart summary section of the Shipping Page./Verify the application behavior on click of Product Name in the Cart summary section of the Shipping Page/Verify the application behavior on click of EDIT Item in Cart Summary Section of Shipping Page/Verify the UI of the Order Summary section displayed below Cart summary Section./Verify the Application behavior on click of Tool Tip text in Order summary Section of the Billing Page/Verify the application behavior on click of Remove Item displayed in Cart Summary Section of the Shipping Page/Verify the View Logo functionality in the Cart summary Section in the Shipping Page.");
$t.start();
try
{
	_log("*********************************verifying each lines in order section*********");
	
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
		
	//verify display of each and every line
	_assertVisible(_heading3("CartSummaryHeader"));
	_assertVisible(_div("CartTableLeft"));
	_assertVisible(_link("Edit Cart", _in(_div("/checkout-mini-cart CartSummaryWrap/"))));
	_assertVisible(_div("box-row"));
	_assertVisible(_link("/(.*)/", _in(_div("/mini-cart-names box/"))));
	_assertVisible(_link("Edit Item"));
	_assertVisible(_link("Remove Item"));
	_assertVisible(_div("mini-cart-product-list box-row  "));
	
	_log("*********************************Edit cart button*********");
	//prod name in shipping page
	var $prodnameinshippingpage=_extract(_getText(_link("/(.*)/",_in(_div("/mini-cart-names/")))),"/(.*)-/",true).toString();
	
     //display and verify when click on of edit card 
	_assertVisible(_link("Edit Cart", _in(_div("/checkout-mini-cart CartSummaryWrap/"))));

	//click on edit cart link in order summary page in shipping page
	_click(_link("Edit Cart", _in(_heading3("CartSummaryHeader"))));
	_expectConfirm("/Are you sure you want to leave this page/", true);

	//should navigate to cart page
	_assertVisible(_div("CartWrap"));

	//capturing product name in cart page
	var $productnameincartpage=_extract(_getText(_link("/(.*)/", _in(_div("product-list-item view-logo-main")))),"/(.*)-/",true).toString();
	_assertEqual($prodnameinshippingpage,$productnameincartpage);
	
	/*_log("*********************************verifying display of name********************");
	
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
	
  //prod name in shipping page
	//var $prodnameinshippingpage=_extract(_getText(_link("/(.*)/",_in(_div("/mini-cart-names/")))),"/(.*)-/",true).toString();

	
	//click on product name in cart summary in shipping page
	_click(_link("/(.*)/", _in(_div("/mini-cart-names box/"))));
	_expectConfirm("/Are you sure you want to leave this page/", true);

	_assertVisible(_heading2("product-name"));

	//should navigate to PD page

	//prod name in PD
	var $prodnameinPDP=_extract(_getText(_heading2("product-name")),"/(.*)P/",true);
	var $prodnameinPDP1=_extract(_getText(_heading2("product-name")),"/On (.*)/",true);
	var $prodnameinPDP2=$prodnameinPDP+$prodnameinPDP1+" "+"Trophy"
	
	//_assertEqual($prodnameinshippingpage,$prodnameinPDP2);
	*/
	_log("*********************************verifying display of image*********");
	
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
	
	//click on image in cart section
	_assertVisible(_image(/(.*)/, _in(_div("/checkout-mini-cart CartSummaryWrap/"))));
	_click(_image(/(.*)/, _in(_div("/checkout-mini-cart CartSummaryWrap/"))))
	
	//verifying the image in cart summary in shipping page
	_assertVisible(_heading2("product-name"));
	_assertVisible(_div("pdpMain"));
	
	
	_log("*********************************Edit item *********");
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
	
	  //prod name in shipping page
	var $prodnameinshippingpage=_extract(_getText(_link("/(.*)/",_in(_div("/mini-cart-names/")))),"/(.*)-/",true).toString();
	
	//verify the cart summary section in shipping page
	//_assertVisible(_heading3("CartSummaryHeader", _near(_div("Shipping & Delivery"))));
	_assertVisible(_heading3("CartSummaryHeader", _near(_div("step-2 active"))));
	_assertVisible(_div("/checkout-mini-cart CartSummaryWrap/"));
	
	
	
	//verify the display and functionality of edit item in cart summary section
	if(_isVisible(_link("Edit Item", _in(_div("/mini-cart-names/")))))
		
	{
	_assertVisible(_link("Edit Item", _in(_div("/mini-cart-names/"))));
    _click(_link("Edit Item", _in(_div("/mini-cart-names/"))))
    }
	//should navigate to PD page
	_assertVisible(_heading2("product-name"));
	_assertVisible(_div("pdpMain"));
//	var $prodnameinPDP=_extract(_getText(_heading2("product-name")),"/(.*)P/",true);
//	var $prodnameinPDP1=_extract(_getText(_heading2("product-name")),"/On (.*)/",true);
//	var $prodnameinPDP2=$prodnameinPDP+$prodnameinPDP1+" "+"Trophy"
//	_assertEqual($prodnameinshippingpage,$prodnameinPDP2);
	
	//prod must be editable
	selectSwatch();	
	
//	_log("*********************************view logo*********");
//	//navigate to checkout page
//	_click(_link("/mini-cart-link-checkout-link/"));
//	
//	//verify the cart summary section in shipping page
//	_assertVisible(_heading3("CartSummaryHeader", _near(_div("Shipping & Delivery"))));
//	_assertVisible(_div("/checkout-mini-cart CartSummaryWrap/"));
//	_assertVisible(_link("View Logo", _in(_div("/mini-cart-names/"))));
//	
//	//click on view logo
//	while(_isVisible(_link("View Logo", _in(_div("/mini-cart-names/")))))
//		{
//	_click(_link("View Logo", _in(_div("/mini-cart-names/"))));
//	
//	_assert(false, "view logo is not configured");
//		}
	
_log("*********************************remove *********");
	
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
	
	//verifying and clicking remove option in cart summary in shipping page
	_assertVisible(_link("Remove Item", _in(_div("/mini-cart-names/"))));
	
	while(_isVisible(_link("Remove Item", _in(_div("/mini-cart-names/")))))
		{
	_click(_link("Remove Item", _in(_div("/mini-cart-names/"))))
	
		}
	
	//should navigate to cart page if there are no items in cart summary section in shipping page
	_assertVisible(_heading1($shippingdata[2][5]));
	_assertVisible(_submit("dwfrm_cart_continueShopping"));

	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("222787","Verify the Save addrress Functionality when the user is trying to save already saved address from shipping Page.");
$t.start();
try
{
	//navigate to my account landing page
	//delete address
	deleteAddress();
	//click on add new address 	
	_click(_link("Add new"));
	//Add address
	addAddress($shippingdata,11);
	//click on create address
	_click(_submit("dwfrm_profile_address_create"));
	
	//verify the product in my accnt address 
	var productno=_count("_listItem","/address-tile/", _in(_div("addresses")));
	var name=_getText(_div("/mini-address-name/"));
	_click(_link("Crown Awards"));
	_wait(2000)
	//navigate to cart page with single 
	navigateToCart($shippingdata[0][0],$shippingdata[0][1]);
	//click mini cart link
	_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));
	//product name should be same
	//_assertEqual($prodname,$pName);
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
	//navigate to shipping page
	_assertVisible(_heading2("Shipping Address"));
	//_assertVisible(_div("single-ship address-fields"));
	_assertVisible(_div("shipping-address-section"));
	_assertVisible(_div("/ checkout-progress-indicator/"));
	//shipping page UI
	shippingAddressUI();
	
	//select address from drop down
	if(_isVisible(_select("dwfrm_singleshipping_addressList")))
	{
	_assertVisible(_div("select-address form-row"));
	_setSelected(_select("dwfrm_singleshipping_addressList"),1);
	}
	//Add address
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email"),$shippingdata[11][1]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail"),$shippingdata[11][2]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), $shippingdata[11][3]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), $shippingdata[11][4]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"), $shippingdata[11][5]);
	
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$shippingdata[11][7]);
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), $shippingdata[11][8]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), $shippingdata[11][9]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $shippingdata[11][10]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),$shippingdata[11][11]);
	
	//verifying the add adress checkbox
	//_assertEqual(false, _checkbox("Add to Address Book").checked);
	 _assertEqual(false,_checkbox("Save Address to Address Book").checked)

	
	//check the checkbox
	//_click(_checkbox("Save Address to Address Book"));
	 _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"))
	//event calendar
	eventcalendar();
	_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
	//continue
	//_click(_submit("/continue-button/"));
	_click(_submit("continue-button handledmixedpopup"));
	
	//displaying and verifying payment page
   _assertVisible(_div("Payment & Place Order"));
   _assertVisible(_div("/checkout-mini-cart CartSummaryWrap/", _in(_div("secondary"))));
   //_assertVisible(_div("address-details"));
   _assertVisible(_div("single-ship address-fields"));

 //  _assertVisible(_fieldset("Select Payment Method"));
	
	//navigate to my accnt addresses
   NavigatetoAccountLanding();
   //click on address link
   _click(_link("ADDRESSES"));
   
  //verify the address after enterring same details in shipping page
   var productno1=_count("_listItem","/address-tile/", _in(_div("addresses")));
   _assertEqual($shippingdata[11][3]+" "+$shippingdata[11][4], _getText(_div("/mini-address-name/")));
   _log("by default 1 address is present when that thing is removed at that time this assert will pass");
   
   
} 

catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();




var $t = _testcase("226678/226662","Verify the UI of the Order Summary section displayed below Cart summary Section./Verify the UI of the Order Summary section displayed below Cart summary Section.");
$t.start();
try
{
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
			
	//UI
	_assertVisible(_div("CartSummaryHeader"),_under(_heading3("CartSummaryHeader")));
	_assertVisible(_listItem("order-summery"));
	//_assertVisible(_listItem("/Shipping/"));
	//_assertVisible(_link("tooltipimg"));
	//_assertVisible(_listItem("Sales Tax"));
	_assertVisible(_div("Sales Tax:"));

	_assertVisible(_listItem("Order Total:"));
	_assertVisible(_listItem("OrderTotalNum"));
	//_assertVisible(_listItem("-"));
	//_assertVisible(_div("order-shipping  first "));

} 

catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
/*
$t = _testcase("219535","Verify shipping method price updating with respect to the Address 1,2 , Country, State, City, Postal code.");
$t.start();
try
{



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/
