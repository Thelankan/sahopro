_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("ShippingPage.xls");

var $shipaddress=_readExcelFile("ShippingPage.xls","ShipAddress");
var $billaddress=_readExcelFile("ShippingPage.xls","BillAddress");
var $shippingdata=_readExcelFile("ShippingPage.xls","ShippingData");
var $validations=_readExcelFile("ShippingPage.xls","Validations");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


//vinay lanka 333-333-3333 vinay@gmail.com microsoft way ELMWOOD PARK, IL 98052 [shipping address format in payment page]
var $shippingaddress=$shipaddress[0][1]+" "+$shipaddress[0][2]+" "+$shipaddress[0][9]+" "+$uId+" "+$shipaddress[0][3]+" "+$shipaddress[0][4]+" "+$shipaddress[0][7]+", "+$shipaddress[0][11]+" "+$shipaddress[0][8];
//vinay lanka 333-333-3333 vinay@gmail.com microsoft way ELMWOOD PARK, IL 98052 [billing address format in payment page]
var $billingaddress=$billaddress[1][1]+" "+$billaddress[1][2]+" "+$billaddress[1][9]+" "+$uId+" "+$billaddress[1][3]+" "+$billaddress[1][4]+" "+$billaddress[1][7]+", "+$billaddress[1][11]+" "+$billaddress[1][8];

var $t = _testcase("219469/219470/222528/222783/219501/219537","Verify the UI of checkout 'SHIPPING' page in Application as a Guest  user./Verify the UI of checkout 'SHIPPING' page in Application as a Anonymous user./Verify the Ui of the Billing address form as a Guest user." +
		"Verify the display of Save Address to Address book option to a Guest user./Veirfy the application navigation on click of Product name in the cart summary section for a Guest user./Verify if the Checkout as Guest section is displayed in the shipping page");
$t.start();
try
{
	
//navigate to cart page with single 
navigateToCart($shippingdata[0][0],$shippingdata[0][1]);
//click mini cart link
_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));
//product name should be same
//_assertEqual($prodname,$pName);
//navigate to checkout page
_click(_link("/mini-cart-link-checkout-link/"));
//navigate to shipping page
_assertVisible(_heading2("Shipping Address"));
_assertVisible(_div("/checkout-progress-indicator/"));

if (_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked)
{
_uncheck(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
}

//Shipping Ui
_assertVisible(_div("single-ship address-fields"));
//_assertVisible(_div("Use this address for Billing"));
_assertVisible(_div("Use as Billing Address"));
_assertVisible(_div("billing-fields address-fields "));
_assertVisible(_div("shipping-billing-form"));
_assertVisible(_textbox("dwfrm_singleshipping_datePicker_datePickerSelection"));
_assertVisible(_div("shipping-methods"));
_assertVisible(_submit("/continue-button/"));

//login sections in shipping guest as guest user
//_assertVisible(_heading2($shippingdata[0][3], _in(_div("login-box"))));
//_assertVisible(_heading2($shippingdata[1][3], _in(_div("login-box login-account"))));
//_assertVisible(_heading2($shippingdata[1][3], _in(_div("login-box login-account js-login-form handledmixedpopup"))));
_assertVisible(_div("Have an Account? Log In(optional)"));
_assertVisible(_link("Log In", _in(_div("checkloglabel"))));



//Add to address section should not display
//_assertVisible(_div("Add to Address Book"));
_assertNotVisible(_div("Save Address to Address Book"));

_assertNotVisible(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook"));

//shipping UI
shippingAddressUI();
adddressProceed();

billingPageUI();

var $prodnameinshippingpage=_getText(_link("/(.*)/", _in(_div("/mini-cart-names/"))));

//click on product name in cart summary in shipping page
_click(_link("/(.*)/", _in(_div("/mini-cart-names box/"))));
_expectConfirm("/Are you sure you want to leave this page/", true);

_assertVisible(_heading2("product-name"));

//should navigate to PD page
var $prodnameinPDP=_getText(_heading2("product-name"));
//_assertEqual($prodnameinshippingpage,$prodnameinPDP);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("222548/222567","Verify the Display of Select Shipping address drop down for a Guest user in shipping page./Verify the display of Use as Billing address option in Shipping section of the shipping page");
$t.start();
try
{

//navigate to checkout page
_click(_link("/mini-cart-link-checkout-link/"));

//use this for billing address checkbox	
_assertVisible(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));	
_assertVisible(_label("Use as Billing Address"));	
//_assertVisible(_div("Use this address for Billing"));
_assertVisible(_div("Use as Billing Address"));

	
//Select Shipping address drop down should not display
_assertNotVisible(_div("Select an Address"));
_assertNotVisible(_div("/select-address/"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("222790/","Veirfy the Pre poplutaion of city, state, country values in the Shipping form when a 5 Zip code as a Guest User.");
$t.start();
try
{

//navigate to checkout page
_click(_link("/mini-cart-link-checkout-link/"));

//zip code,city,country,state should be empty
_assertEqual("",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")));
_assertEqual("",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")));
_assertEqual("Select...",_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));

//entering 5-digit zip code 
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"),$shippingdata[0][2]);
_wait(2000);
//values should pre populate
_assertEqual($shippingdata[0][2],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")));
_assertEqual($shippingdata[1][2],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")));
_assertEqual($shippingdata[2][2],_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
_assertEqual($shippingdata[3][2],_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_country")));

//entering 10-digit zip code 
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"),$shippingdata[0][4]);
_wait(2000);
//values should pre populate
_assertEqual($shippingdata[0][4],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")));
_assertEqual("/"+$shippingdata[1][4]+"/i",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")));
_assertEqual($shippingdata[2][4],_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
_assertEqual($shippingdata[3][4],_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_country")));



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("222533/222536","Veirfy the UI of the Prigress indicator in shipping Page as a Guest User./Verify if the Payment and Place Order Section in progress indicator is active when the user is in Shipping page.");
$t.start();
try
{

//UI of progress indicator
_assertVisible(_div("Review your Cart", _in(_div("/(.*) checkout-progress-indicator/"))));
_assertVisible(_div("step-1 inactive"));
_assertVisible(_div("Shipping & Delivery", _in(_div("/(.*) checkout-progress-indicator/"))));
_assertVisible(_div("step-2 active"));
_assertVisible(_div("Payment & Place Order", _in(_div("/checkout-progress-indicator/"))));
_assertVisible(_div("step-3 inactive"));

//click on  Payment and Place Order Section in progress indicator
_click(_div("/Payment & Place Order/"));
//Application should de in shipping page no action should be seen after clicking on Payment and Place Order Section in progress indicator
_assertVisible(_heading2("Shipping Address"));
_assertVisible(_div("single-ship address-fields"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("222540/222541/222542","Verify the Select Calender date event in the shipping page/Verify the display of Tool tip icon in the Select Event date section./Verify the application behavior on click of Tool tip icon displayed in the Select Event date Section.");
$t.start();
try
{
	shippingAddress($shipaddress,1);
	adddressProceed();
	//should select the date 
	var $currentyear=_getText(_div("/delevery-date/")).split("/")[2];
	_click(_textbox("dwfrm_singleshipping_datePicker_datePickerSelection"));
	//calendar should be displayed
	_assertVisible(_table("ui-datepicker-calendar"));
	_assertVisible(_div("ui-datepicker-div"));
	//select random date
	for (var $i=0;$i<14;$i++)
		{
		//clcik on next button
		_click(_span("Next"));
		}
	//click link 25
	_click(_link("25",_in(_table("ui-datepicker-calendar"))));
	var $changeddate=_getText(_textbox("dwfrm_singleshipping_datePicker_datePickerSelection")).split(" ")[1].replace(","," ");
	var $changedyear=_getText(_textbox("dwfrm_singleshipping_datePicker_datePickerSelection")).split(" ")[2];
	_assert($currentyear<$changedyear);	
	_assertEqual("25",$changeddate);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
/*
var $t = _testcase("222558/222560/222563/219518/219519/219520/219521/219524/222549/222551","validations");
$t.start();
try
{


	for (var $i=0;$i<$validations.length;$i++) 
		{
		
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), $validations[$i][1]);
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), $validations[$i][2]);
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"), $validations[$i][3]);
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"),$validations[$i][4]);
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $validations[$i][8]);
			_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$validations[$i][5]);
			_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), $validations[$i][6]);
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), $validations[$i][7]);
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),$validations[$i][9]);
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email"),$validations[$i][10]);
			_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail"),$validations[$i][10]);
		
		if ($i==0 || $i==6)
			{
			
			//click on submit button
			_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
			_assertEqual(false,(_submit("dwfrm_singleshipping_shippingAddress_save").disabled));
			}
		else if ($i==1)
			{
			
			_assertEqual($validations[0][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName")).length);
			_assertEqual($validations[0][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName")).length);
			_assertEqual($validations[2][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1")).length);
			_assertEqual($validations[1][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2")).length);
			_assertEqual($validations[1][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")).length);
			_assertEqual($validations[1][13],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone")).length);
			_assertEqual($validations[1][12],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")).length);
			_assertEqual($validations[1][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email")).length);
			_assertEqual($validations[1][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail")).length);
			
			}
		else if ($i==2)
			{
			
			//click on submit button
			_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
			_assertEqual($validations[0][14],_getText(_span("dwfrm_singleshipping_shippingAddress_addressFields_postal-error")));
			_assertEqual($validations[1][14],_getText(_span("dwfrm_singleshipping_shippingAddress_addressFields_phone-error")));
			
			}
			else if ($i==3 || $i==4 || $i==5)
			{
				
			//click on submit button
			_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
			_assertEqual($validations[0][14],_getText(_span("dwfrm_singleshipping_shippingAddress_addressFields_postal-error")));
			_assertEqual($validations[0][15],_getText(_span("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail-error")));
			_assertEqual($validations[2][14],_getText(_span("dwfrm_singleshipping_shippingAddress_addressFields_phone-error")));
			
			}
			else
			{
				//click on submit button
				_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
				//should navigate to billing/payment page
				_assertEqual($shippingdata[1][0], _getText(_div("address-header-main")));
			
			}
		
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/


// $t = _testcase("219535","Verify shipping method price updating with respect to the Address 1,2 , Country, State, City, Postal code.");
//$t.start();
//try
//{
//
//
//
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();

$t = _testcase("226678/226662","Verify the UI of the Order Summary section displayed below Cart summary Section./Verify the UI of the Order Summary section displayed below Cart summary Section.");
$t.start();
try
{
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
			
	//UI
	_assertVisible(_div("CartSummaryHeader"),_under(_heading3("CartSummaryHeader")));
	_assertVisible(_listItem("order-summery"));
	//_assertVisible(_listItem("Shipping"));
	_assertVisible(_link("tooltip"));
	//_assertVisible(_link("tooltipimg"));
	_assertVisible(_listItem("Sales Tax:"));
	_assertVisible(_listItem("Order Total:"));
	_assertVisible(_listItem("OrderTotalNum"));
	//_assertVisible(_listItem("-"));
	//_assertVisible(_div("order-shipping  first "));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("226646/226653/226654/226655/226656/226678/226679/226657/226660/226676","Verify the Functionality of the EDIT cart link in the cart summary section of the Shipping Page/" +
		"Verify of each line displayed in the Cart Summary section/" +
		"Verify the application behavior on click of Product Image in the Cart summary section of the Shipping Page./Verify the application behavior on click of Product Name in the Cart summary section of the Shipping Page/Verify the application behavior on click of EDIT Item in Cart Summary Section of Shipping Page/Verify the UI of the Order Summary section displayed below Cart summary Section./Verify the Application behavior on click of Tool Tip text in Order summary Section of the Billing Page/Verify the application behavior on click of Remove Item displayed in Cart Summary Section of the Shipping Page/Verify the View Logo functionality in the Cart summary Section in the Shipping Page.");
$t.start();
try
{
	_log("*********************************verifying each lines in order section*********");
	
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
		
	//verify display of each and every line
	_assertVisible(_heading3("CartSummaryHeader"));
	_assertVisible(_div("CartTableLeft"));
	_assertVisible(_link("Edit Cart", _in(_div("/checkout-mini-cart CartSummaryWrap/"))));
	_assertVisible(_div("box-row"));
	_assertVisible(_link("/(.*)/", _in(_div("/mini-cart-names box/"))));
	_assertVisible(_link("Edit Item"));
	_assertVisible(_link("Remove Item"));
	_assertVisible(_div("mini-cart-product-list box-row  "));
	
	_log("*********************************Edit cart button*********");
	//prod name in shipping page
	var $prodnameinshippingpage=_extract(_getText(_link("/(.*)/",_in(_div("/mini-cart-names/")))),"/(.*)-/",true).toString();
	
     //display and verify when click on of edit card 
	_assertVisible(_link("Edit Cart", _in(_div("/checkout-mini-cart CartSummaryWrap/"))));

	//click on edit cart link in order summary page in shipping page
	_click(_link("Edit Cart", _in(_heading3("CartSummaryHeader"))));
	_expectConfirm("/Are you sure you want to leave this page/", true);

	//should navigate to cart page
	_assertVisible(_div("CartWrap"));

	//capturing product name in cart page
	var $productnameincartpage=_extract(_getText(_link("/(.*)/", _in(_div("product-list-item view-logo-main")))),"/(.*)-/",true).toString();
	_assertEqual($prodnameinshippingpage,$productnameincartpage);
	
	/*_log("*********************************verifying display of name********************");
	
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
	
  //prod name in shipping page
	//var $prodnameinshippingpage=_extract(_getText(_link("/(.*)/",_in(_div("/mini-cart-names/")))),"/(.*)-/",true).toString();

	
	//click on product name in cart summary in shipping page
	_click(_link("/(.*)/", _in(_div("/mini-cart-names box/"))));
	_expectConfirm("/Are you sure you want to leave this page/", true);

	_assertVisible(_heading2("product-name"));

	//should navigate to PD page

	//prod name in PD
	var $prodnameinPDP=_extract(_getText(_heading2("product-name")),"/(.*)P/",true);
	var $prodnameinPDP1=_extract(_getText(_heading2("product-name")),"/On (.*)/",true);
	var $prodnameinPDP2=$prodnameinPDP+$prodnameinPDP1+" "+"Trophy"
	
	//_assertEqual($prodnameinshippingpage,$prodnameinPDP2);
	*/
	_log("*********************************verifying display of image*********");
	
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
	
	//click on image in cart section
	_assertVisible(_image(/(.*)/, _in(_div("/checkout-mini-cart CartSummaryWrap/"))));
	_click(_image(/(.*)/, _in(_div("/checkout-mini-cart CartSummaryWrap/"))))
	
	//verifying the image in cart summary in shipping page
	_assertVisible(_heading2("product-name"));
	_assertVisible(_div("pdpMain"));
	
	
	_log("*********************************Edit item *********");
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
	
	  //prod name in shipping page
	var $prodnameinshippingpage=_extract(_getText(_link("/(.*)/",_in(_div("/mini-cart-names/")))),"/(.*)-/",true).toString();
	
	//verify the cart summary section in shipping page
	//_assertVisible(_heading3("CartSummaryHeader", _near(_div("Shipping & Delivery"))));
	_assertVisible(_heading3("CartSummaryHeader", _near(_div("step-2 active"))));
	_assertVisible(_div("/checkout-mini-cart CartSummaryWrap/"));
	
	
	
	//verify the display and functionality of edit item in cart summary section
	if(_isVisible(_link("Edit Item", _in(_div("/mini-cart-names/")))))
		
	{
	_assertVisible(_link("Edit Item", _in(_div("/mini-cart-names/"))));
    _click(_link("Edit Item", _in(_div("/mini-cart-names/"))))
    }
	//should navigate to PD page
	_assertVisible(_heading2("product-name"));
	_assertVisible(_div("pdpMain"));
//	var $prodnameinPDP=_extract(_getText(_heading2("product-name")),"/(.*)P/",true);
//	var $prodnameinPDP1=_extract(_getText(_heading2("product-name")),"/On (.*)/",true);
//	var $prodnameinPDP2=$prodnameinPDP+$prodnameinPDP1+" "+"Trophy"
//	_assertEqual($prodnameinshippingpage,$prodnameinPDP2);
	
	//prod must be editable
	selectSwatch();	
	
//	_log("*********************************view logo*********");
//	//navigate to checkout page
//	_click(_link("/mini-cart-link-checkout-link/"));
//	
//	//verify the cart summary section in shipping page
//	_assertVisible(_heading3("CartSummaryHeader", _near(_div("Shipping & Delivery"))));
//	_assertVisible(_div("/checkout-mini-cart CartSummaryWrap/"));
//	_assertVisible(_link("View Logo", _in(_div("/mini-cart-names/"))));
//	
//	//click on view logo
//	while(_isVisible(_link("View Logo", _in(_div("/mini-cart-names/")))))
//		{
//	_click(_link("View Logo", _in(_div("/mini-cart-names/"))));
//	
//	_assert(false, "view logo is not configured");
//		}
	
_log("*********************************remove *********");
	
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
	
	//verifying and clicking remove option in cart summary in shipping page
	_assertVisible(_link("Remove Item", _in(_div("/mini-cart-names/"))));
	
	while(_isVisible(_link("Remove Item", _in(_div("/mini-cart-names/")))))
		{
	_click(_link("Remove Item", _in(_div("/mini-cart-names/"))))
	
		}
	
	//should navigate to cart page if there are no items in cart summary section in shipping page
	_assertVisible(_heading1($shippingdata[2][5]));
	_assertVisible(_submit("dwfrm_cart_continueShopping"));

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();




