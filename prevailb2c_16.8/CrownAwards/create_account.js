_include("util.GenericLibrary/GlobalFunctions.js");
_resource("util.GenericLibrary/User_Credentials.xls");

_navigateTo("https://development-webstore-crownawards.demandware.net/on/demandware.store/Sites-MAI-Site/default/Home-Show");

//SiteURLs();

var $user=_readExcelFile("util.GenericLibrary/User_Credentials.xls","Register");

for (var $i=0;$i<$user.length;$i++)
{
	
	 //_click(_link("user-account-login"));
	 _click(_link("/desktop-show/", _in(_div("user-info"))));
	 _click(_link("button", _in(_div("/login-box/"))));
	 _setValue(_textbox("dwfrm_profile_customer_firstname"), $user[$i][0]);
	 _setValue(_textbox("dwfrm_profile_customer_lastname"),$user[$i][1]);
	 _setValue(_textbox("dwfrm_profile_customer_email"),$user[$i][2]);
	 _setValue(_textbox("dwfrm_profile_customer_emailconfirm"),$user[$i][3]);
	 _setValue(_password("/dwfrm_profile_login_password/"),$user[$i][4]);
	 _setValue(_password("/dwfrm_profile_login_passwordconfirm/"),$user[$i][5]);
	
	 if(_isVisible(_submit("Apply")))
		 {
		 	_click(_submit("Apply")); 
		 } 	 
		 else
			 {
			   _click(_submit("dwfrm_profile_confirm"));
			 }
	 _wait(5000);
	 
	 _assertVisible(_link("/user-logout/"));
	 
	 _wait(5000);
	 
	 _click(_link("/user-logout/"));
	 _wait(2000);
}