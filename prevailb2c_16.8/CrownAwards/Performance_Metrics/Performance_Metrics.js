_resource("Performance_Metrics.xls");
var $URL=_readExcelFile("Performance_Metrics.xls","URL");
var $BM=_readExcelFile("Performance_Metrics.xls","BM");
var $SpeedTest=_readExcelFile("Performance_Metrics.xls","WebTools");
var $Site= _readExcelFile("Performance_Metrics.xls","SiteName");
var $Cache= _readExcelFile("Performance_Metrics.xls","Cache");
var $result= _getExcel("Performance_Metrics.xls","FinalResult");
var $SecurityBoolean="";
var $CacheBoolean="";
var $date = "" + new Date();

try
{
	var $t=0;
	for(var $i=0;$i<$Site.length;$i++)
		{
			for(var $j=0;$j<$Cache.length;$j++)
				{				
					startProfiling($Site[$i][0],$Cache[$j][0],$t);	
					performance($result, $Site[$i][0], $t);
					stopProfiling($Site[$i][0],$Cache[$j][0],$result,$t);
					$result.insertRows([[]],2);
					$result.set(2,0,$Cache[$j][0]);
				}	
			for(var $p=$t;$p<$URL.length;$p++)
				{
					if($Site[$i][0]==$URL[$p][0])
					{
						$t++;				
					}
				}		
		}
	$result.insertRows([[]],2);
	$result.set(2,0,"Date");
	$result.set(2,1,$date);
	$result.insertRows([[]],2);
	//_copyFile("Performance_Metrics.xls", _userDataPath("C:/sahi_pro6.1.0/userdata/Performance_Metrics.xls"));
}
catch($e)
{
	Onfailure($Site[$i][0]);
}
_copyFile("Performance_Metrics.xls", _userDataPath("C:/sahi_pro6.1.0/userdata/Performance_Metrics.xls"));
//------------------------------------------------------------------------------------------------

function startProfiling($site, $cache, $t)
{
	BM_Login();
	_click(_span("/sod_label/"));
	_click(_span($site));
	if(_isVisible(_cell("Welcome to the Demandware Business Manager.")))
		{
			_click(_span("sod_option  active selected"));
			_assertEqual($site, _getText(_span("sod_label")));
		}
	_click(_link("Administration"));
	//disabling the cache
	_click(_link("Sites"));
	_click(_link("Manage Sites"));
	_click(_link($site,_in(_cell($site))));
	//disabling the store front tool kit (If it is enabled)
	_click(_link("Site Status"));	
	$SecurityBoolean=_getSelectedText(_select("ChannelStatusForm_DomainStatus"));
	//testing purpose
    _log("Before setting:"+$SecurityBoolean);
	if(_getSelectedText(_select("ChannelStatusForm_DomainStatus"))!="Online")
		{
			_setSelected(_select("ChannelStatusForm_DomainStatus"), "Online");
			_click(_submit("updateStatus"));
		}
    
	//disabling cache
	_click(_link("Cache"));
	$CacheBoolean=_getText(_textbox("DomainStaticContentMaxAge"));
	   //testing purpose
    _log("Before setting:"+$CacheBoolean);
	if($cache=="WithoutCache")
		{
			_setValue(_textbox("DomainStaticContentMaxAge"),"0");
			_uncheck(_checkbox("DomainPageCachingAllowed"));			
		}
	else 
		{
			_setValue(_textbox("DomainStaticContentMaxAge"),"86,400");
			_check(_checkbox("DomainPageCachingAllowed"));			
		}		
	_click(_submit("Apply"));
	_click(_button("Invalidate"));
	_click(_button("pageCacheRoot")); 
	if($cache=="WithCache")
		{
			_click(_link("Log off."));	
			for(var $c=$t;$c<$URL.length;$c++)
				{
					if($site==$URL[$c][0])
						{
							_navigateTo($URL[$c][2]);
						}
					else
						{
							break;
						}				
				}
			BM_Login();
		}
	//enable the pipeline profiler
	_click(_link("Administration"));
	_click(_link("Operations"));
	_click(_link("Pipeline Profiler"));
	if(_isVisible(_cell("The profiler is capturing performance data.")))
		{      
		   _log("Already set");
		   _click(_submit("Reset Sensors"));
		   _click(_image("Stop profiling."));
		}
	_click(_image("Start profiling."));
	_click(_submit("Reset Sensors"));		
	_click(_link("Log off."));	
}


function stopProfiling($site, $cache,$sheet,$t)
{
	BM_Login();
	//fetching the values from pipeline profiler and storing in excel
	var $r=2;
	_click(_link("Administration"));
	_click(_link("Operations"));	
	for(var $d=$t;$d<$URL.length;$d++)
	{
		if($site==$URL[$d][0])
			{
				_click(_link("Pipeline Profiler"));	
				if(_isVisible(_link("Sites-"+$URL[$d][0]+"-Site")))
					{
						_click(_link("Sites-"+$URL[$d][0]+"-Site"));
					}
				else
					{
						_click(_link("Sites-"+$URL[$d][5]+"-Site"));
					}	
				var $AvgTime=_getText(_row("/"+$URL[$d][3]+" "+$URL[$d][4]+"/")).split(" ")[4].toString().replace(",","");
				if($cache=="WithCache")
					{
						if($AvgTime=="-1")
							{
								$sheet.set($r,6,"NA");
							}
						else
							{
								$sheet.set($r,6,$AvgTime);
							}
					}
				else
					{
						$sheet.set($r,6,$AvgTime);
					}		
				$r++;
				$t++;				
			}
		else
			{
				break;
			}		
	}
	//stopping profiler
	_click(_link("Pipeline Profiler"));
	_click(_submit("Reset Sensors"));
	_click(_image("Stop profiling."));
	_click(_link("Administration"));
	_click(_link("Sites"));
	_click(_link("Manage Sites"));
	_click(_link($site,_in(_cell($site))));
	//resetting  store front
	_click(_link("Site Status"));
	_setSelected(_select("ChannelStatusForm_DomainStatus"), $SecurityBoolean);
	_click(_submit("updateStatus"));
	//testing purpose
    _log("After re-setting:"+$SecurityBoolean);
	//resetting cache
	_click(_link("Cache"));
	if($CacheBoolean=="0")
		{
			_setValue(_textbox("DomainStaticContentMaxAge"),"0");
			_uncheck(_checkbox("DomainPageCachingAllowed"));			
		}
	else if($CacheBoolean=="86,400")
		{
			_setValue(_textbox("DomainStaticContentMaxAge"),"86,400");
			_check(_checkbox("DomainPageCachingAllowed"));			
		}	
	_click(_submit("Apply"));
	_click(_button("Invalidate"));
	_click(_button("pageCacheRoot"));	
	//testing purpose
    _log("After re-setting:"+$CacheBoolean);
	_click(_link("Log off."));
	$SecurityBoolean="";
	$CacheBoolean="";	
}


function performance($sheet, $site, $t)
{
	var $h=2;
	//$sheet.insertRows([[]],$h);
	for(var $k=$t;$k<$URL.length;$k++)
	{      
		if($site==$URL[$k][0])
			{
				$sheet.insertRows([[]],$h);
				$sheet.set($h,0,$URL[$k][0]);
				$sheet.set($h,1,$URL[$k][1]);
			    for(var $l=0;$l<$SpeedTest.length;$l++)
			        {      
				    	_navigateTo($SpeedTest[$l][1]);
				    	if($SpeedTest[$l][0]=="PingDom")
                         {
                                _setValue(_urlbox("testurl"), $URL[$k][2]);                                                                   
                                //_click(_submit("button button-starttest"));
                                _click(_submit("/button-starttest/")); 
                                _wait(180000,_isVisible(_image("screenshot")));
                                while (_isVisible(_heading4("An error occured")))
                                      {
                                             _setValue(_urlbox("testurl"), $URL[$k][2]); 
                                             //_click(_submit("button button-starttest"));
                                             _click(_submit("/button-starttest/")); 
                                             _wait(180000,_isVisible(_image("screenshot")));
                                      }
                                _wait(3000);
                                if(_isVisible(_div("popup-modal-close icon-cross")))
                                _click(_div("popup-modal-close icon-cross"));
                                var $Perf_Grade= parseFloat(_extract(_getText(_div("rbc-summary-info-value", _in(_div("rbc-summary-item rbc-summary-perfgrade first")))),"/ (.*)/",true));
                                var $LoadTime= parseFloat(_extract(_getText(_div("rbc-summary-info-value", _in(_div("rbc-summary-item rbc-summary-loadtime")))),"/(.*) [ms]/",true));
                                $sheet.set($h,2,$Perf_Grade);
                                $sheet.set($h,4,$LoadTime);
                                var $Ref;
                                _set($Ref, window.location.href);
                                $sheet.set($h,8,$Ref);
                         }
						else if($SpeedTest[$l][0]=="WebPage test")
						   {
							   _setValue(_textbox("url"), $URL[$k][2]); 
							   _click(_submit("start_test"));
							   _wait(1800000,_isVisible(_table("tableResults"))); 
							   if(_isVisible(_heading3("The test completed but there were no successful results.")))
							   {
									  _navigateTo($SpeedTest[$l][1]);
									  _setValue(_textbox("url"), $URL[$k][2]); 
									  _click(_submit("start_test"));
									  _wait(1800000,_isVisible(_table("tableResults")));
							   }
							   _wait(3000);
							   var $LoadTimeFirst=parseFloat(_getText(_cell("fvDocComplete")));
							   if(_isVisible(_cell("rvDocComplete")))
									  {
									  var $LoadTimeRepeat=parseFloat(_getText(_cell("rvDocComplete")));
									  }
							   else
									  {
									  var $LoadTimeRepeat="Did not check";     
									  }                          
							   $sheet.set($h,10,$LoadTimeFirst);
							   $sheet.set($h,12,$LoadTimeRepeat);
						   }
						else if($SpeedTest[$l][0]=="Page speed insights")
						   {
							   _setValue(_textbox("url"), $URL[$k][2]); 
							   _click(_div("ANALYZE"));							   
							   _wait(180000,_isVisible(_div("result-container")));
							  // _wait(8000,_isVisible(_span("/(.*)/", _in(_heading2("result-group-title")))));
							   _wait(8000);
							   //mobile
							   var $ScoreMobile=parseFloat(_getText(_span("/(.*)/", _in(_heading2("result-group-title")))));
							   $sheet.set($h,14,$ScoreMobile);                 
							   //desktop
							   _click(_div("Desktop"));
							   var $ScoreDesktop=parseFloat(_getText(_span("/(.*)/", _in(_div("/Suggestions Summary/")))));
							   $sheet.set($h,16,$ScoreDesktop);                
						   }
			        }
			    $h++;
			}
		else
			{
				break;
			}
	}
}

function BM_Login()
{
    _navigateTo($BM[0][0]);
    if(_isVisible(_link("Log off.")))
    {
    _click(_link("Log off."));
    }
	//BM Login
	_setValue(_textbox("LoginForm_Login"),$BM[0][1]);
	_setValue(_password("LoginForm_Password"),$BM[0][2]);
	_click(_submit("Log In"));
}

function Onfailure($site)
{
	BM_Login();
	_click(_link("Administration"));
	_click(_link("Sites"));
	_click(_link("Manage Sites"));
	_click(_link($site,_in(_cell($site))));
	//resetting  store front
	_click(_link("Site Status"));
	//testing purpose
    _log($SecurityBoolean);
    if($SecurityBoolean!="")
    {
    _setSelected(_select("ChannelStatusForm_DomainStatus"), $SecurityBoolean);
    _click(_submit("updateStatus"));
    }
	//resetting cache
	_click(_link("Cache"));
    //testing purpose
    _log($CacheBoolean);
	if($CacheBoolean!="")
    {
		if($CacheBoolean=="0")
		    {
		        _setValue(_textbox("DomainStaticContentMaxAge"),"0");
		        _uncheck(_checkbox("DomainPageCachingAllowed"));                                                
		    }
		else if($CacheBoolean=="86,400")
		    {
		        _setValue(_textbox("DomainStaticContentMaxAge"),"86,400");
		        _check(_checkbox("DomainPageCachingAllowed"));                                      
		    }
		_click(_submit("Apply"));
		_click(_button("Invalidate"));
		_click(_button("pageCacheRoot"));
    }		
	_click(_link("Log off."));	
}