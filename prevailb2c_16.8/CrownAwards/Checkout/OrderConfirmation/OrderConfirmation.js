_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("OrderConfirmation.xls");

var $shipaddress=_readExcelFile("OrderConfirmation.xls","ShippingAddress");
var $billaddress=_readExcelFile("OrderConfirmation.xls","BillingAddress");
var $OCP_Data=_readExcelFile("OrderConfirmation.xls","OCP_Data");
var $paymentdetails=_readExcelFile("OrderConfirmation.xls","Payment");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


//Ashley Phillips 817-478-1802 CrownTrophies5@gmail.com District 1 1656 Union Street Eureka, CA 95502 United States
var $shippingaddress=$shipaddress[0][1]+" "+$shipaddress[0][2]+" "+$shipaddress[0][12]+" "+$uId+" "+$shipaddress[0][3]+" "+$shipaddress[0][4]+" "+$shipaddress[0][7]+", "+$shipaddress[0][11]+" "+$shipaddress[0][8]+" "+$shipaddress[0][5];
var $shippingaddressComp=$shipaddress[0][1]+" "+$shipaddress[0][2]+" "+$shipaddress[0][3]+" "+$shipaddress[0][4]+" "+$shipaddress[0][7]+", "+$shipaddress[0][11]+" "+$shipaddress[0][8]+" "+$shipaddress[0][5];
//Ashley Phillips District 1 1656 Union Street Eureka, CA 95502 United States
var $billingaddress=$billaddress[0][1]+" "+$billaddress[0][2]+" "+$billaddress[0][3]+" "+$billaddress[0][4]+" "+$billaddress[0][7]+", "+$billaddress[0][11]+" "+$billaddress[0][8]+" "+$billaddress[0][5];	
var $ShippingMethodShippingpage;

var $t = _testcase("219895/219884/219882/219889","Verify the application behvaior on click of Print Recipt Button in Order Confirmation page./Verify the UI of the Order Info section in Order Confirmation page" +
		"Verify the UI of the Order confirmation Page Heading section");
$t.start();
try
{


//navigate to cart page
navigateToCart($OCP_Data[0][0],$OCP_Data[0][1]);

var $totalpriceincart=_getText(_div("order-value"));
var $lineitempriceincart=_collectAttributes("_span","price-sales","sahiText", _in(_div("/item-price box/")));
//click mini cart link
_click(_link("/mini-cart-link-checkout-link/"));
//shipping form should display
shippingAddress($shipaddress,0);
//Use shipping address as billing checkbox should be checked by default
_assertEqual(true,_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked);
//verifying price in shipping page
_assertVisible(_div($totalpriceincart, _in(_div("box ordersumsnum"))));

//event calendar
eventcalendar();



//selecting shipping method
_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
_wait(3000);


//shipping method selection verification
var $shippingmethods=_collectAttributes("_div","ship-details","sahiText",_in(_div("ship-table")));
var $shippingmethodprice=0;

//Below logic will work only if the shipping method is selected by default 
//for(var $i=0;$i<$shippingmethods.length;$i++)
//    {
//	  _log($i);
//	  var $boolean=_radio("input-radio shipping-methods-length["+$i+"]").checked;
//	  $boolean=$boolean.toString();
//	  _log($boolean);
//      if($boolean=="true")
//      {
//    	  _log($i);
//    	//shipping method
//    	$ShippingMethodShippingpage=_getText(_div("ship-details",_near(_radio("input-radio shipping-methods-length["+$i+"]"))));	  
//    	$shippingmethodprice=_extract(_getText(_div("ship-price",_near(_radio("input-radio shipping-methods-length["+$i+"]")))),"/[$](.*)/",true);
//      }
//    }

$ShippingMethodShippingpage=_getText(_div("/ship-details/",_in(_div("shipping-details active"))));
$shippingmethodprice=_extract(_getText(_div("/ship-price/",_in(_div("shipping-details active")))),"/[$](.*)/",true);
_log("shipping method price="+$shippingmethodprice);

_assertVisible(_listItem("/"+$shippingmethodprice+"/",_in(_list("order-details-value"))));

//total price in shipping page
var $priceinshippingpage=_extract(_getText(_div("order-value", _in(_listItem("OrderTotalNum")))),"/[$](.*)/",true);
_log("total price in shipping page="+$priceinshippingpage);
var $totalshipprice=parseFloat($orderTotal) + parseFloat($shippingmethodprice);
round($totalshipprice,2);
_log("cart total price="+$orderTotal);
_log("Calculating total shipping price="+$totalshipprice);
_assertEqual($totalshipprice,$priceinshippingpage)

//_assertEqual($totalpriceincart,);
//click on continue button in shipping page
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
adddressProceed();

var $billingprice=_extract(_getText(_div("order-value", _in(_listItem("OrderTotalNum")))),"/[$](.*)/",true);
//comparing shipping and billing price
_assertEqual($priceinshippingpage,$billingprice);

//should navigate to billing/payment page
_assertEqual($OCP_Data[0][2], _getText(_div("address-header-main")));
//comparing address
_assertEqual($shippingaddress,_getText(_div("address-content", _in(_div("billing-address-section")))));
_assertEqual($shippingaddressComp,_getText(_div("address-content", _in(_div("shipping-address-section")))));
//line item total in payment page
var $lineitemtotalinpaymentpage=_collectAttributes("_span","price-sales","sahiText", _in(_div("box cartunit")));
//comparing arrays
_assertEqualArrays($lineitempriceincart,$lineitemtotalinpaymentpage);
//entering payment details
//enter payment details
PaymentDetails($paymentdetails,0);
//check the check box
_check(_checkbox("/dwfrm_billing_confirm/"));
//place order button
_click(_submit("dwfrm_billing_save"));
//order confirmation page
_assertVisible(_heading1($OCP_Data[0][3]));
_assertVisible(_div($OCP_Data[1][3]));
_assertVisible(_span($OCP_Data[2][3]));
_assertVisible(_span($OCP_Data[3][3]));
_assertVisible(_span($OCP_Data[4][3]));

var $orderno=_extract(_getText(_span("order-number")),"/:(.*)/",true);
_log($orderno);

var $priceinOCP=_extract(_getText(_span("order-price")),"/: [$](.*)/",true);
var $ordertotalOCP=_extract(_getText(_div("order-value", _in(_listItem("OrderTotalNum")))),"/[$](.*)/",true);

//price verification
_assertEqual($priceinshippingpage,$priceinOCP);
_assertEqual($billingprice,$priceinOCP);
_assertEqual($priceinOCP,$ordertotalOCP);
_assertEqual($totalshipprice,$ordertotalOCP);
_assertEqual($totalshipprice,$priceinOCP);

//District 1 1656 Union Street Eureka, 95502 United States (817) 478-1802 CrownTrophies5@gmail.com
var $shippingaddressinOCPpage =$shipaddress[0][1]+" "+$shipaddress[0][2]+" "+$shipaddress[0][4]+", "+$shipaddress[0][3]+" "+$shipaddress[0][7]+" "+$shipaddress[0][11]+" "+$shipaddress[0][8]+" "+$shipaddress[0][5];
//Ashley Phillips District 1 1656 Union Street Eureka, 95502 United States
var $billingaddressinOCPpage=$shipaddress[0][13]+" "+$uId+" "+$shipaddress[0][4]+" "+$shipaddress[0][3]+" "+$shipaddress[0][7]+" "+$shipaddress[0][11]+", "+$shipaddress[0][8]+" "+$shipaddress[0][5];
//comparing addresses
_assertEqual($billingaddressinOCPpage,_getText(_div("mini-address-location")));
_assertEqual($shippingaddressinOCPpage,_getText(_div("address")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219885/219886","Verify the display of data in Order Summary section in order confirmation page/Verify the values displayed in the Order Details section of the Order confirmation page");
$t.start();
try
{

	var $orderno=_extract(_getText(_span("order-number")),"/:(.*)/",true);
	//shipping method verification in order confirmation page
	var $ShippingMethodInOcpPage=_getText(_div("value", _in(_div("shipping-method"))));
	_assertEqual($ShippingMethodShippingpage+" Shipping",$ShippingMethodInOcpPage);
	
	//shipping and billing address
	_assertVisible(_div("Billing Address"));
	_assertVisible(_div("Shipping Address"));
	_assertVisible(_div("Shipping Method"));
	_assertVisible(_div("order-billing"));
	_assertVisible(_div("order-shipment-address1"));
	
	//order number
	//total
	//delivery date
	
	//payment info
	_assertVisible(_div("Payment Information"));
	_assertVisible(_div("order-payment-instruments"));
	_assertContainsText($paymentdetails[0][5], _div("order-payment-instruments"));
	_assertContainsText($paymentdetails[0][6], _div("cc-number"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219881","Verify the UI of the Order confirmation Page");
$t.start();
try
{

	//order summary section
	_assertVisible(_div("orderdetails-leftsection"));
	_assertVisible(_heading3("ORDER SUMMARY"));
	_assertVisible(_div("order-information",_near(_heading3("ORDER SUMMARY"))));
	//billing address
	_assertVisible(_div("order-billing",_in(_div("orderdetails-leftsection"))));
	_assertVisible(_div("/order-shipment-address/", _in(_div("orderdetails-leftsection"))));
	//cart summary section
	_assertVisible(_div("order-details-right-sec"));
	_assertVisible(_div("order-shipments CartSummaryWrap", _in(_div("order-details-right-sec"))));
	_assertVisible(_heading3("CART DETAILS"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("219891/219892/219893/219883","Verify the display of Content slot in the Order Summary Section/Veirfy the Source of the Content Slot displayed in the Order Confirmation Page./Verify the display of Content asset above the footer and Below the Continue shopping button in Order confirmation page/Verify the UI of the Page heading in Order confirmation page");
$t.start();
try
{

	//check the print receipt button
	_assertVisible(_submit("/print-page/", _in(_div("print-conform-data"))));
	//content slot ion order summary section
	_assertVisible(_div("order-information", _in(_div("orderdetails-leftsection"))));
	//content slot
	_assertVisible(_heading2($OCP_Data[0][4]));
	_assertVisible(_div("html-slot-container", _in(_div("printReceipt"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("12345","checking event calender scenarios");
$t.start();
try
{

//navigate to cart page
navigateToCart($OCP_Data[0][0],$OCP_Data[0][1]);
//click mini cart link
_click(_link("/mini-cart-link-checkout-link/"));
//shipping form should display
shippingAddress($shipaddress,0);
//Use shipping address as billing checkbox should be checked by default
_assertEqual(true,_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked);
//check first shipping method
_click(_radio("input-radio shipping-methods-length"));
	
//should select the date 
var $Monthinshippingmethod=parseInt(_getText(_div("/delevery-date/", _in(_div("shp-details cell")))).split("-")[0]);
//var $dateinshippingmethod=_extract(_getText(_div("/delevery-date/", _in(_div("shp-details cell")))).split("-")[1],"/[0](.*)/",true).toString();
var $dateinshippingmethod=parseInt(_getText(_div("/delevery-date/", _in(_div("shp-details cell")))).split("/")[1],"/[0](.*)/").toString();
_log($dateinshippingmethod);
	
	for (var $j=0;$j<$eventdate.length;$j++)
		{
		
		_log($j);
		if ($Monthinshippingmethod==$j)
			{
			$j=$j-1;
			$currentmonth=$eventdate[$j][0];
			_log($currentmonth)
			break;
			}
		
		}
	
//click on event date text box
_mouseOver(_textbox("dwfrm_singleshipping_datePicker_datePickerSelection"));
var $monthToBeSelected=_getText(_span("/(.*)/", _in(_div("ui-datepicker-title"))));
_log($monthToBeSelected);

while ($currentmonth!=$monthToBeSelected)
{
	_click(_span("Next"));
	$monthToBeSelected=_getText(_span("/(.*)/", _in(_div("ui-datepicker-title"))));
	_log($monthToBeSelected);
}

_assertEqual($currentmonth,$monthToBeSelected);
//click current date
_click(_link($dateinshippingmethod,_in(_table("ui-datepicker-calendar"))));

//fetching event date
var $eventdate=_getText(_textbox("dwfrm_singleshipping_datePicker_datePickerSelection"));
_log($eventdate);

//click on continue button in shipping page
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	
adddressProceed();

//enter payment details
PaymentDetails($paymentdetails,0);
//check the check box
_check(_checkbox("/dwfrm_billing_confirm/"));
//place order button
_click(_submit("dwfrm_billing_save"));

//order confirmation page
_assertVisible(_heading1($OCP_Data[0][3]));
_assertVisible(_div($OCP_Data[1][3]));
_assertVisible(_span($OCP_Data[2][3]));
_assertVisible(_span($OCP_Data[3][3]));
_assertVisible(_span($OCP_Data[4][3]));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("12345","Handling failed order scenario");
$t.start();
try
{


//login to the BM	
BM_Login();
//capture cyber source
_click(_link("Custom Preferences"));
_click(_link("Cybersource", _in(_cell("Cybersource"))));
var $cybersourcevalue=_getText(_textbox("inputfield_en[1]"));
_log($cybersourcevalue);
_setValue(_textbox("inputfield_en[1]"),$cybersourcevalue+"=");
_click(_submit("Apply"));
//logout from BM
_click(_link("Log off."));

//navigate to site
SiteURLs()

//place an order
//navigate to cart page
navigateToCart($OCP_Data[0][0],$OCP_Data[0][1]);
//click mini cart link
_click(_link("/mini-cart-link-checkout-link/"));
//shipping form should display
shippingAddress($shipaddress,0);
//event calendar
eventcalendar();

//selecting shipping method
_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));

//click on continue button in shipping page
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
adddressProceed();

//enter payment details
PaymentDetails($paymentdetails,0);
//check the check box
_check(_checkbox("/dwfrm_billing_confirm/"));
//place order button
_click(_submit("dwfrm_billing_save"));

//checking failed order
_assertVisible(_heading2("order-hold"));
_assertVisible(_heading2("ORDER ON HOLD - PLEASE DO NOT PLACE A NEW ORDER."));
_assertVisible(_heading1("CREDIT CARD PROCESSING ERROR"));
_assertVisible(_heading1("credit-error"));
_assertVisible(_div("Submit Cancel"));
_assertVisible(_div("credit-info"));
_assertVisible(_span("unable to process"));
_assertVisible(_div("credit-details-card"));

var $orderno=_getText(_span("order-number")).split(" ")[2];
_log($orderno);

var $orderkkkk=_getText(_span("order-number"))
_log($orderkkkk);

//setback cybersource to original value
//login to the BM	
BM_Login();
//capture cyber source
_click(_link("Custom Preferences"));
_click(_link("Cybersource", _in(_cell("Cybersource"))));
_setValue(_textbox("inputfield_en[1]"),$cybersourcevalue);
_click(_submit("Apply"));
//logout from BM
_click(_link("Log off."));

//navigate to site
SiteURLs()

//place an order
//navigate to cart page
navigateToCart($OCP_Data[0][0],$OCP_Data[0][1]);
//click mini cart link
_click(_link("/mini-cart-link-checkout-link/"));
//shipping form should display
shippingAddress($shipaddress,0);
//event calendar
eventcalendar();
	
//selecting shipping method
_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));	
	
//click on continue button in shipping page
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
adddressProceed();

//enter payment details
PaymentDetails($paymentdetails,0);
//check the check box
_check(_checkbox("/dwfrm_billing_confirm/"));
//place order button
_click(_submit("dwfrm_billing_save"));

//order confirmation page
_assertVisible(_heading1($OCP_Data[0][3]));
_assertVisible(_div($OCP_Data[1][3]));
_assertVisible(_span($OCP_Data[2][3]));
_assertVisible(_span($OCP_Data[3][3]));
_assertVisible(_span($OCP_Data[4][3]));

var $orderno=_getText(_span("order-number")).split(" ")[2];
_log($orderno);

var $orderkkkk=_getText(_span("order-number"))
_log($orderkkkk);


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase("","");
$t.start();
try
{

	var $priceincart=_extract(_getText(_div("order-value", _in(_listItem("OrderTotalNum")))),"/[$](.*)/",true);
	var $shippingtax=_extract(_getText(_div("order-shipping  first ")),"/[$](.*)/",true);
	//verification of sales tax
	_assert(false,"should verify sales tax when its configured");

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/


/*var $t = _testcase("","");
$t.start();
try
{



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/