_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("billingPage.xls");

var $shipaddress=_readExcelFile("billingPage.xls","ShipAddress");
var $billaddress=_readExcelFile("billingPage.xls","BillAddress");
var $shippingdata=_readExcelFile("billingPage.xls","ShippingData");
var $validations=_readExcelFile("billingPage.xls","Validations");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


//vinay lanka 333-333-3333 vinay@gmail.com microsoft way ELMWOOD PARK, IL 98052 [shipping address format in payment page]
var $shippingaddress=$shipaddress[0][1]+" "+$shipaddress[0][2]+" "+$shipaddress[0][9]+" "+$uId+" "+$shipaddress[0][3]+" "+$shipaddress[0][4]+" "+$shipaddress[0][7]+", "+$shipaddress[0][11]+" "+$shipaddress[0][8];
//vinay lanka 333-333-3333 vinay@gmail.com microsoft way ELMWOOD PARK, IL 98052 [billing address format in payment page]
var $billingaddress=$billaddress[1][1]+" "+$billaddress[1][2]+" "+$billaddress[1][9]+" "+$uId+" "+$billaddress[1][3]+" "+$billaddress[1][4]+" "+$billaddress[1][7]+", "+$billaddress[1][11]+" "+$billaddress[1][8];


/*if (isMobile())
{
 _assertVisible(_link("/mobile-show/", _in(_div("user-info"))));
 _click(_link("/mobile-show/", _in(_div("user-info"))));
}
else
	{
	 _assertVisible(_link("/desktop-show/", _in(_div("user-info"))));
	 _click(_link("/desktop-show/", _in(_div("user-info"))));
	 //Visibility of overlay
	 _assertVisible(_div("dialog-container"));
	}

//login to the app
login();
_wait(5000);*/


//navigate to cart page with single 
navigateToCart($shippingdata[0][0],$shippingdata[0][1]);
//click mini cart link
_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));
//product name should be same
//_assertEqual($prodname,$pName);
//navigate to checkout page
_click(_link("/mini-cart-link-checkout-link/"));
//navigate to shipping page
_assertVisible(_heading2("Shipping Address"));
_assertVisible(_div("/checkout-progress-indicator/"));

if (_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked)
{
_click(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
}

//Shipping Ui
_assertVisible(_div("single-ship address-fields"));
_assertVisible(_div("Use this address for Billing"));
_assertVisible(_div("billing-fields address-fields"));
_assertVisible(_div("shipping-billing-form"));
_assertVisible(_textbox("dwfrm_singleshipping_datePicker_datePickerSelection"));
_assertVisible(_div("shipping-methods"));
_assertVisible(_submit("continue-button"));

//click on logout link in header
if (!_isVisible(_link("/user-logout/")))
	{
	//login sections in shipping guest as guest user
	_assertVisible(_heading2($shippingdata[0][3], _in(_div("login-box"))));
	_assertVisible(_heading2($shippingdata[1][3], _in(_div("login-box login-account"))));
	//_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email", _in(_div("login-box-content clearfix"))),$validations[6][10]);
	//_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail", _in(_div("login-box"))),$validations[6][10]);
	//Add to address section should not display
	_assertNotVisible(_div("Add to Address Book"));
	_assertNotVisible(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook"));
	}
_assertVisible(_div("Add to Address Book"));
_assertVisible(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook"));

//shipping UI
//shippingAddressUI();

var $t = _testcase("223001/223002/223003/223004/223005/223006/223007/223010/223011/223014/223015/223016/223017/223018/223440","Verify the validations in the Billing form as a guest user.");
$t.start();

try
{
		billingPageUI();
		shippingAddress($validations,7);
	
for (var $i=0;$i<$validations.length;$i++) 
{

	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"), $validations[$i][1]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"), $validations[$i][2]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1"), $validations[$i][3]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address2"),$validations[$i][4]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_postal"), $validations[$i][8]);
	_setSelected(_select("dwfrm_billing_billingAddress_addressFields_country"),$validations[$i][5]);
	_setSelected(_select("dwfrm_billing_billingAddress_addressFields_states_state"), $validations[$i][6]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_city"), $validations[$i][7]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_phone"),$validations[$i][9]);
	_setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),$validations[$i][10]);
	_setValue(_textbox("dwfrm_billing_billingAddress_email_confirmemail"),$validations[$i][10]);


if ($i==0 || $i==6)
	{
	
	//click on submit button
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//_click(_submit("continue-button"));
	
	_assertEqual(true,(_submit("dwfrm_singleshipping_shippingAddress_save").disabled));
	}
else if ($i==1)
	{
	
	_assertEqual($validations[1][11],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")).length);
	_assertEqual($validations[1][11],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")).length);
	_assertEqual($validations[1][11],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")).length);
	_assertEqual($validations[1][11],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address2")).length);
	_assertEqual($validations[1][11],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_city")).length);
	_assertEqual($validations[1][13],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_phone")).length);
	_assertEqual($validations[1][12],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_postal")).length);
	_assertEqual($validations[1][11],_getText(_textbox("dwfrm_billing_billingAddress_email_emailAddress")).length);
	_assertEqual($validations[1][11],_getText(_textbox("dwfrm_billing_billingAddress_email_confirmemail")).length);
	
	}
else if ($i==2)
	{
	
	//click on submit button
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//_click(_submit("continue-button"));
	_assertEqual($validations[0][14],_getText(_span("dwfrm_billing_billingAddress_addressFields_postal-error")));
	_assertEqual($validations[1][14],_getText(_span("dwfrm_billing_billingAddress_addressFields_phone-error")));
	_assertEqual($validations[7][14],_getText(_span("dwfrm_billing_billingAddress_email_emailAddress-error")));
	_assertEqual($validations[3][14],_getText(_span("dwfrm_billing_billingAddress_email_confirmemail-error")));
	//_assertEqual($validations[4][14],_getText(_span("dwfrm_billing_billingAddress_addressFields_firstName-error")));
	//_assertEqual($validations[5][14],_getText(_span("dwfrm_billing_billingAddress_addressFields_lastName-error")));
	//_assertEqual($validations[6][14],_getText(_span("dwfrm_billing_billingAddress_addressFields_address1-error")));
	
	//_assertEqual($validations[3][15],_getText(_span("dwfrm_billing_billingAddress_addressFields_city-error")));
//	_assertEqual($validations[4][15],_getText(_span("dwfrm_billing_billingAddress_addressFields_states_state-error")));
	
	}
	else if ($i==3)
	{
		
	//click on submit button
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//_click(_submit("continue-button"));
	_assertEqual($validations[5][15],_getText(_span("dwfrm_billing_billingAddress_addressFields_postal-error")));
	_assertEqual($validations[6][15],_getText(_span("dwfrm_billing_billingAddress_email_emailAddress-error")));
	
	_assertEqual($validations[7][15],_getText(_span("dwfrm_billing_billingAddress_email_confirmemail-error")));
	_assertEqual($validations[2][14],_getText(_span("dwfrm_billing_billingAddress_addressFields_phone-error")));
	
	}

	else if ($i==4)
		{
	
		_assertEqual($validations[6][15],_getText(_span("dwfrm_billing_billingAddress_email_emailAddress-error")));
		
		_assertEqual($validations[7][15],_getText(_span("dwfrm_billing_billingAddress_email_confirmemail-error")));
		_assertEqual($validations[2][14],_getText(_span("dwfrm_billing_billingAddress_addressFields_phone-error")));
		
		}
	
	else if ($i==5)
    	{
		_assertEqual($validations[6][15],_getText(_span("dwfrm_billing_billingAddress_email_emailAddress-error")));
		
		_assertEqual($validations[6][15],_getText(_span("dwfrm_billing_billingAddress_email_confirmemail-error")));
		_assertEqual($validations[2][14],_getText(_span("dwfrm_billing_billingAddress_addressFields_phone-error")));
		
		}

	else
	{
		//proceed to next page
		adddressProceed();
		
		//event calendar
		eventcalendar();
		
		//Selecting shipping method
		_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));

		//click on submit button
       _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
		
		PaymentDetails($shipaddress,5);
		
		//click on place order button without checking the checkbox
		_assertEqual(false, _checkbox("/dwfrm_billing_confirm/").checked);
		_click(_submit("continue-button"));
		//error message will display
		_assertVisible(_span("/dwfrm_billing_confirm/"));

		 
		 //checking the checkbox
		   _check(_checkbox("/dwfrm_billing_confirm/"));
		   _assertEqual(true, _checkbox("/dwfrm_billing_confirm/").checked);
		   _assertVisible(_div("self-confirm"));
       
       
     //click on  Payment and Place Order Section in progress indicator
     // _click(_div("/Payment & Place Order/"));
      _click(_submit("continue-button"));
      //navigating to the order confirmation page
      _assertVisible(_heading1($shippingdata[0][5], _near(_span("conformation"))));
      _assertVisible(_div($shippingdata[1][5], _in(_div("confirmation-message"))));
      
      var $orderno=_getText(_span("order-number")).split(" ")[2];
      _log($orderno);

      var $orderkkkk=_getText(_span("order-number"))
      _log($orderkkkk);
      
      //verifying the order total in order confirmation page
      var $expPrice=_extract(_getText(_div("order-value", _in(_listItem("/OrderTotalNum/")))),"[$](.*)",true);
    var $actPrice = _extract(_getText(_div("order-value", _in(_listItem("OrderTotalNum")))),"[$](.*)",true);
    _assertVisible(_div("/order-value/"));
    _assertEqual( $expPrice, $actPrice);
    
     }

}

}

catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("223270","Verify the application when the user removes all the products from Cart in billing page.");
$t.start();

try
{
	//navigate to home page
	
	NavigatetoHomepage();
	
	
	//navigate to cart page with single 
	navigateToCart($shippingdata[0][0],$shippingdata[0][1]);
	//click mini cart link
	_click(_link("/(.*)/", _in(_div("miniCartViewCart"))));
	//product name should be same
	_assertEqual($prodname,$pName);
	//navigate to checkout page
	_click(_link("/mini-cart-link-checkout-link/"));
	//navigate to shipping page
	_assertVisible(_heading2("Shipping Address"));
	_assertVisible(_div("/checkout-progress-indicator/"));

	if (_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked)
	{
	_click(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	}

	//click on logout link in header
	if (!_isVisible(_link("/user-logout/")))
		{
		//login sections in shipping guest as guest user
		_assertVisible(_heading2($shippingdata[0][3], _in(_div("login-box"))));
		_assertVisible(_heading2($shippingdata[1][3], _in(_div("login-box login-account"))));
		//_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_email", _in(_div("login-box-content clearfix"))),$validations[6][10]);
		//_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_emailFields_confirmemail", _in(_div("login-box"))),$validations[6][10]);
		//Add to address section should not display
		_assertNotVisible(_div("Add to Address Book"));
		_assertNotVisible(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook"));
		}
	_assertVisible(_div("Add to Address Book"));
	_assertVisible(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook"));
		
	//entring shipping details 
	
	shippingAddress($validations,7);
	BillingAddress($validations,7)
      
   //event calendar
	eventcalendar();

	//click on submit button
    _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
		
    //proceed to next page
     adddressProceed();
		
     //entering the payment details
      PaymentDetails($shipaddress,5);
	//try to click on place order button without checking the checkbox
		_assertEqual(false, _checkbox("/dwfrm_billing_confirm/").checked);
		_click(_submit("continue-button"));
		//error message will display
		_assertVisible(_span("/dwfrm_billing_confirm/"));

		 
		 //checking the checkbox
		   _check(_checkbox("/dwfrm_billing_confirm/"));
		   _assertEqual(true, _checkbox("/dwfrm_billing_confirm/").checked);
		   _assertVisible(_div("self-confirm"));
       
       
    
     
     //click on remove items in billing page
     // _click(_link("Remove Item", _in(_div("mini-cart-names box cartdesc[1]")))); 
	//  while(isVisible(_link("Remove Item", _in(_listItem("item-remove")))))
		  
		   while(_isVisible(_link("Remove", _in(_div("secondary")))))
		  {
    //  _click(_link("Remove Item", _in(_div("/mini-cart-names/"))));
      _click(_link("Remove", _in(_div("secondary"))));
		  }
	  
	  //navigating to empty cart
      _assertVisible(_heading1($shippingdata[2][5], _in(_div("cart-empty"))));



   }
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();






