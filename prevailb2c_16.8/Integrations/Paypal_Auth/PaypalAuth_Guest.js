_include("../../util.GenericLibrary/GlobalFunctions.js");
_include("../../util.GenericLibrary/BM_Configuration.js");
_resource("Paypal_Auth.xls");

var $item =_readExcelFile("Paypal_Auth.xls","Item");
var $Valid_Data =_readExcelFile("Paypal_Auth.xls","Valid_Data");
var $Common =_readExcelFile("Paypal_Auth.xls","Common");
var $Payment_Data=_readExcelFile("Paypal_Auth.xls","Payment_Data");
var $CreditCard_Data=_readExcelFile("Paypal_Auth.xls","CreditCard_Data");
var $Paypal=_readExcelFile("Paypal_Auth.xls","Paypal");





//set the authorize_net payment processor
BM_Login();
//navigate to prevail dash board
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));
//Click on PREVAIL Integrations link
_click(_link($userLog[2][2]));


_setSelected(_select("/inputfield_en/",_rightOf(_row("/Credit Card Payment Service:/"))),$BMConfig[0][4]);
//click on apply
_click(_submit("update"));

//click on ordering
_click(_link("Ordering"));
_click(_link("Payment Methods"));

//gift certificate
var $gc="Yes";
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATE Gift Certificate/"))));
	}
else
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATEGift Certificate/"))));
	}
_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
_click(_div("/"+$gc+"/",_in(_div("x-combo-list-inner"))));
_click(_div("x-panel-header x-unselectable"));
_click(_button("Apply[1]"));
_wait(2000);
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATE Gift Certificate/"))),[2]).toString();
	}
else
	{
		var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATEGift Certificate/"))),[2]).toString();
	}
_log($giftCertificate+"......GiftCardPaymentorder");
_assertEqual($Run[9][1],$giftCertificate,"Data not configured properly");

if($giftCertificate=="Yes")
{
	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
	}
	else
	{
		_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
	}
	_click(_image("s.gif", _in(_row("Payment Processor:"))));
	_click(_div($BMConfig[0][7]));
	_click(_button("Apply[1]"));
}


//credit card
var $cc="Yes";
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/CREDIT_CARD Credit Card/"))));
	}
else
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/CREDIT_CARDCredit Card/"))));
	}
_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
_click(_div("/"+$cc+"/",_in(_div("x-combo-list-inner"))));
_click(_div("x-panel-header x-unselectable"));
_click(_button("Apply[1]"));
_wait(2000);

if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))),[2]).toString();
	}
else
	{
		var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))),[2]).toString();
	}
_log($CreditCard+"......CreditCardPaymentorder");
_assertEqual($Run[9][0],$CreditCard,"Data not configured properly");

if($CreditCard=="Yes")
{
	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
	}
	else
	{
		_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
	}
	_click(_image("s.gif", _in(_row("Payment Processor:"))));
	_click(_div($BMConfig[2][5]));
	_click(_button("Apply[1]"));
}

//log of from BM
_click(_link("Log off."));


	//creating gift card
	createGC($GC);
	
	
SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();

var $expAddress=$Valid_Data[0][1]+" "+$Valid_Data[0][2]+" "+$Valid_Data[0][3]+" "+$Valid_Data[0][4]+" "+$Valid_Data[0][7]+" "+$Valid_Data[1][6]+" "+$Valid_Data[0][8]+" "+$Valid_Data[0][5];
	
	
var $t=_testcase("167002","Verify the Authorization amount displayed in DW database when the guest user places an order with combination Coupon + custom GC + CC");
$t.start();
try
{	
	//navigate to billing page
	navigateToBillingPage($item[0][0],$item[1][0]);
	//enter the valid Coupon
	enterCouponCode($item[0][1]);
	//apply GC
    applyGC($GC[0][0],$GC[0][1]);
    //valid data 
    _assertVisible(_div("success giftcert-pi"));
    _assertEqual("USD "+$GC[0][4]+" has been redeemed from Gift Certificate **"+$GC[0][3]+" Remove", _getText(_div("success giftcert-pi")));
	//enter valid billing address
	BillingAddress($Valid_Data,0);
	//enter payment data
	enterPaymentDetails($Paypal,$Valid_Data);
	//verify whether valid Coupon got applied or not
	_assertVisible(_span("Applied"));
	_assertVisible(_div("promo  first "));
	_assertVisible(_span("Coupon Number:"));
	_assertVisible(_span("value",_in(_div("discount clearfix  first "))));
	var $price=_extract(_getText(_span("value",_in(_div("discount clearfix  first ")))),"/[(](.*)[)]/",true).toString();
	var $discountPrice=parseFloat(_extract(_getText(_span("value",_in(_div("discount clearfix  first ")))),"/[$](.*)/",true).toString());
	//click on submit order
	_click(_submit("submit"));
	var $expSubTotal=$subTotal-$discountPrice;
	_assertEqual($expSubTotal,parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true)));
	var $actOrderTotal=parseFloat(_extract(_getText(_row("/order-total/")),"/[$](.*)/",true).toString());
	//verify the navigation to OCP
	_assertVisible(_heading1($item[0][2]));
	var $orderNum=_getText(_span("value", _in(_heading1("order-number"))));
	var $expCreditCardAmt=$actOrderTotal-parseFloat($GC[0][2]);
	_assertEqual($GC[0][2],parseFloat(_extract(_getText(_span("value",_in(_span("payment-amount")))),"/[$](.*)/",true)));
	var $creitCardAmount=parseFloat(_extract(_getText(_span("value",_in(_div("payment-amount")))),"/[$](.*)/",true));
	_assertEqual($expCreditCardAmt,$creitCardAmount);
	//navigate toBM
	navigateToRespectiveOrder($orderNum);
	//click on order number
	_click(_link($orderNum));
	//verify the coupon details
	_assertVisible(_cell("/"+$item[0][1]+"/"));
	_assertVisible(_font($price));
	//click on shipment id
	_click(_link("/Shipment/"));
	//verify the shipping address
	_assertVisible(_cell($expAddress));
	//navigate back
	_click(_link("Order: "+$orderNum));
	//click on payment tab
	_click(_link("Payment"));
	authorizationForGCAndCC($GC[0][2],$creitCardAmount);

}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("167000","Verify the Authorization amount displayed in DW database when the guest user places an order with combination custom GC + CC");
$t.start();
try
{	
	//navigate to store front
	SiteURLs();
	//navigate to billing page
	navigateToBillingPage($item[0][0],$item[1][0]);
	//apply GC
    applyGC($GC[1][0],$GC[1][1]);
    //valid data 
    _assertVisible(_div("success giftcert-pi"));
    _assertEqual("USD "+$GC[1][4]+" has been redeemed from Gift Certificate **"+$GC[1][3]+" Remove", _getText(_div("success giftcert-pi")));
	//enter valid billing address
	BillingAddress($Valid_Data,0);
	//enter payment data
	enterPaymentDetails($Paypal,$Valid_Data);
	//click on submit order
	_click(_submit("submit"));
	var $actOrderTotal=parseFloat(_extract(_getText(_row("/order-total/")),"/[$](.*)/",true).toString());
	//verify the navigation to OCP
	_assertVisible(_heading1($item[0][2]));
	var $orderNum=_getText(_span("value", _in(_heading1("order-number"))));
	_assertEqual($GC[1][2],parseFloat(_extract(_getText(_span("value",_in(_span("payment-amount")))),"/[$](.*)/",true)));
	var $expCreditCardAmt=$actOrderTotal-parseFloat($GC[1][2]);
	$expCreditCardAmt=round($expCreditCardAmt,2);
	var $creitCardAmount=parseFloat(_extract(_getText(_span("value",_in(_div("payment-amount")))),"/[$](.*)/",true));
	_assertEqual($expCreditCardAmt,$creitCardAmount);
	//navigate to BM
	navigateToRespectiveOrder($orderNum);
	//click on order number
	_click(_link($orderNum));
	_click(_link("/Shipment/"));
	//verify the shipping address
	_assertVisible(_cell($expAddress));
	//navigate back
	_click(_link("Order: "+$orderNum));
	//click on payment tab
	_click(_link("Payment"));
	authorizationForGCAndCC($GC[1][2],$creitCardAmount);
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("166986","Verify the Paypal Billing address verification when a guest user places an order with valid address in the application");
$t.start();
try
{	
	//navigate to store front
	SiteURLs();
	//place order
	//navigate to billing page
	navigateToBillingPage($item[0][0],$item[1][0]);
	//enter valid billing address
	BillingAddress($Valid_Data,0);
	//enter payment data
	enterPaymentDetails($Paypal,$Valid_Data);
	//click on submit order
	_click(_submit("submit"));
	//verify the order placement
	_assertVisible(_heading1($item[0][2]));
	_assertVisible(_span("value", _in(_heading1("order-number"))));  
	_assertVisible(_div("order-billing"));
	_assertVisible(_div("order-payment-instruments"));
	_assertVisible(_table("order-totals-table"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("166990","Verify the amount authorization by Paypal when a guest user places an order in the application");
$t.start();
try
{	
	//place order
	//navigate to billing page
	navigateToBillingPage($item[0][0],$item[1][0]);
	//enter valid billing address
	BillingAddress($Valid_Data,0);
	//enter payment data
	enterPaymentDetails($Paypal,$Valid_Data);
	//click on submit order
	_click(_submit("submit"));
	//verify the order placement
	_assertVisible(_heading1($item[0][2]));
	//fetch order number
	var $orderNum=_getText(_span("value", _in(_heading1("order-number"))));
	var $creitCardAmount=parseFloat(_extract(_getText(_span("value",_in(_div("payment-amount")))),"/[$](.*)/",true));
	//navigate toBM
	navigateToRespectiveOrder($orderNum);
	//click on order number
	_click(_link($orderNum));
	//click on payment tab
	_click(_link("Payment"));
	//verifying the GC payment 
	var $paymentThroughCreditCard=_getText(_cell("infobox_item top",_in(_cell("infobox_item top s"))));
	commonData($paymentThroughCreditCard);
	for(var $i=0;$i<$CreditCard_Data.length;$i++)
	{
		if($paymentThroughCreditCard.indexOf($CreditCard_Data[$i][0])>=0)
			{
			_assert(true);
			}
		else
			{
			_assert(false);
			}
	}	
	//amount
	_assert($paymentThroughCreditCard.indexOf($creitCardAmount)>=0);
	//logoff
	_click(_link("Log off."));
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("166993","Verify the amount authorization when credit card authorization fails for a guest user");
$t.start();
try
{	
	//navigate to store front
	SiteURLs();
	//navigate to billing page
	navigateToBillingPage($item[0][0],$item[1][0]);
	//enter valid billing address
	BillingAddress($Valid_Data,0);
	//enter credit card details which is not valid for paypal_auth
	PaymentDetails($Valid_Data,4);
	//click on continue
	_click(_submit("dwfrm_billing_save"));	
	//click on submit order
	_click(_submit("submit"));
	//verify the display of authorization failure message
	_assertVisible(_div("error-form"));
	_assertEqual($item[0][3],_getText(_div("error-form")));
	//it should be in order review page only
	_assertVisible(_submit("submit"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();




function commonData($verificationMethod)
{
	for(var $i=0;$i<$Common.length;$i++)
	{
		if($verificationMethod.indexOf($Common[$i][0])>=0)
			{
			_assert(true);
			}
		else
			{
			_assert(false);
			}
	}	
}

function authorizationForGCAndCC($gcData,$orderTotal)
{
	//verifying the GC payment 
	authForGC($gcData);
	//verify the credit card data
	authForCC($orderTotal);
}

function authForGC($gcData)
{
	//verifying the GC payment 
	var $paymentThroughGC=_getText(_cell("infobox_item top",_in(_cell("infobox_item top s"))));
	commonData($paymentThroughGC);
	for(var $i=0;$i<$Payment_Data.length;$i++)
	{
		if($paymentThroughGC.indexOf($Payment_Data[$i][0])>=0)
			{
			_assert(true);
			}
		else
			{
			_assert(false);
			}
	}
	//amount
	_assert($paymentThroughGC.indexOf($gcData)>=0);	
}

function authForCC($orderTotal)
{
	//verify the credit card data
	var $paymentThroughCreditCard=_getText(_cell("infobox_item top[1]",_in(_cell("infobox_item top s"))));
	commonData($paymentThroughCreditCard);
	for(var $i=0;$i<$CreditCard_Data.length;$i++)
	{
		if($paymentThroughCreditCard.indexOf($CreditCard_Data[$i][0])>=0)
			{
			_assert(true);
			}
		else
			{
			_assert(false);
			}
	}	
	//amount
	_assert($paymentThroughCreditCard.indexOf($orderTotal)>=0);	
	//logoff
	_click(_link("Log off."));
}

function enterCouponCode($coupon)
{
	//enter the valid Coupon
	_setValue(_textbox("dwfrm_billing_couponCode"),$coupon);
	_click(_span("APPLY"));	
}

function applyGC($pin,$code)
{
	//apply GC
    _setValue(_textbox("dwfrm_billing_giftCertCode"),$pin);
    _setValue(_textbox("dwfrm_billing_giftCertPin"),$code);                
    _click(_submit("dwfrm_billing_redeemGiftCert"));
}

function enterPaymentDetails($paypalSheet,$creditCardSheet)
{
	//enter credit card details
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
		{
		  if($CreditCardPayment==$BMConfig[0][4])
		  {
		  	PaymentDetails($paypalSheet,2);
		  }
	  	  else
		  {
		  	PaymentDetails($creditCardSheet,3);
		  }
		//click on continue
		_click(_submit("dwfrm_billing_save"));	
		}
   else if($CreditCard=="No" && $paypalNormal=="Yes")
		{
   		_assert(_radio("PayPal").checked);
		//click on continue
		_click(_submit("dwfrm_billing_save"));	
		//paypal
   		if(isMobile())
             {
             _setValue(_emailbox("login_email"), $paypalSheet[0][0]);
             }
          else
             {
             _setValue(_textbox("login_email"), $paypalSheet[0][0]);
             }
          _setValue(_password("login_password"), $paypalSheet[0][1]);
          _click(_submit("Log In"));
          _click(_submit("Continue"));
          _wait(10000,_isVisible(_submit("submit")));
		  }	
}

function navigateToRespectiveOrder($orderNumber)
{
	//navigate to BM 
	BM_Login();
	//click on ordering
	_click(_link("Ordering"));
	_click(_link("Orders"));
	//enter order number
	_setValue(_textbox("OrderSearchForm2_SimpleSearchTerm"),$orderNumber);
	_click(_submit("Find"));	
}