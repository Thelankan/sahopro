_include("../../util.GenericLibrary/GlobalFunctions.js");
_include("../../util.GenericLibrary/BM_Configuration.js");
_resource("Ogone.xls");

var $Default=_readExcelFile("Ogone.xls","Default");
var $Address=_readExcelFile("Ogone.xls","Address");
var $Ogone=_readExcelFile("Ogone.xls","Ogone");
var $Ogone_BM=_readExcelFile("Ogone.xls","Ogone_BM");


//enable Ogone in BM
enableOgone();

//navigate to URL
SiteURLs();
cleanup()
_setAccessorIgnoreCase(true);



var $t=_testcase("161721","Verify whether guest user is able to place order using Ogone as a payment option");
$t.start();
try
{
	//navigate to shipping page
	navigateToShippingPage($Default[0][0],$Default[1][0]);
	//enter shipping address
	shippingAddress($Address,0);
	//check use this for billing check box
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	addressVerificationOverlay();
	//enter billing details
	BillingAddress($Address,0);
	//select ogone as payment type
	_click(_radio("OGONE"));
	//select VISA as paymnet type
	_setSelected(_select("dwfrm_billing_paymentMethods_ogoneCard_type"),$Default[0][1]);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	var $expPrice=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
	//click on submit order
	_click(_submit("submit"));
	//fill card details
	ogonePayment($Ogone,0);
	//click on continue
	_click(_submit("payment"));
	_wait(20000,_isVisible(_table("ncol_ref")));
	//verify navigation to OCP page
	verifyOCPThroughOgone($Default,$expPrice);
	var $orderNum=_getText(_span("value", _in(_heading1("order-number"))));
	//verifying payment in BM
	verifyPaymentSecInBM($orderNum,$Ogone_BM,$expPrice);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("161749","Verify the scenario when guest user cancel the order in ogone site.");
$t.start();
try
{
	//navigate to URL
	SiteURLs();
	//navigate to shipping page
	navigateToShippingPage($Default[0][0],$Default[1][0]);
	//enter shipping address
	shippingAddress($Address,0);
	//check use this for billing check box
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//UPS verification
	addressVerificationOverlay();
	//enter billing details
	BillingAddress($Address,0);
	//select ogone as payment type
	_click(_radio("OGONE"));
	//select VISA as paymnet type
	_setSelected(_select("dwfrm_billing_paymentMethods_ogoneCard_type"),$Default[0][1]);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	//click on submit order
	_click(_submit("submit"));
	//fill card details
	ogonePayment($Ogone,0);
	//click cancel button
	_click(_submit("Cancel"));
	_expectConfirm("/Are you sure/",true);
	_assertVisible(_heading3($Default[0][6]));
	//verify the navigation
	verifyNavigationToBillingPageFromOgone($Default);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("161755","Verify the scenario when guest user cancel the order from the ogone site and try to use different payment");
$t.start();
try
{
	//select different payment type(American Express)
	_setSelected(_select("dwfrm_billing_paymentMethods_ogoneCard_type"),$Default[1][1]);
	//click on continue on billing page
	_click(_submit("dwfrm_billing_save"));
	var $expPrice=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
	//click on submit order
	_click(_submit("submit"));
	//fill card details
	ogonePayment($Ogone,1);
	//click on continue
	_click(_submit("payment"));
	_wait(20000,_isVisible(_table("ncol_ref")));
	//verify navigation to OCP page
	verifyOCPThroughOgone($Default,$expPrice);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("161750","Verify the scenario when guest user navigates back from the ogone site");
$t.start();
try
{
	//navigate to shipping page
	navigateToShippingPage($Default[0][0],$Default[1][0]);
	//enter shipping address
	shippingAddress($Address,0);
	//check use this for billing check box
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	addressVerificationOverlay();
	//enter billing details
	BillingAddress($Address,0);
	//select ogone as payment type
	_click(_radio("OGONE"));
	//select VISA as paymnet type
	_setSelected(_select("dwfrm_billing_paymentMethods_ogoneCard_type"),$Default[0][1]);
	//click on continue on billing page
	_click(_submit("dwfrm_billing_save"));
	//click on submit order
	_click(_submit("submit"));
	//fill card details
	ogonePayment($Ogone,0);
	//click on back button
	_click(_button("Back"));
	//verify the navigation back to billing page
	verifyNavigationToBillingPageFromOgone($Default);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();



var $t=_testcase("161754","Verify the scenario when guest user navigates back from the ogone site and try to use different payment");
$t.start();
try
{
	//select different payment type(American Express)
	_setSelected(_select("dwfrm_billing_paymentMethods_ogoneCard_type"),$Default[1][1]);
	//click on continue on billing page
	_click(_submit("dwfrm_billing_save"));
	var $expPrice=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
	//click on submit order
	_click(_submit("submit"));
	//fill card details
	ogonePayment($Ogone,1);
	//click on continue
	_click(_submit("payment"));
	_wait(20000,_isVisible(_table("ncol_ref")));
	//verify navigation to OCP page
	verifyOCPThroughOgone($Default,$expPrice);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("161740","Verify whether guest user is able to place order with combination of Coupon and Credit card using Ogone as a payment option.");
$t.start();
try
{
	//navigate to shipping page
	navigateToShippingPage($Default[0][0],$Default[1][0]);
	//enter shipping address
	shippingAddress($Address,0);
	//check use this for billing check box
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	addressVerificationOverlay();
	//fetch order total before applying coupon
	var $orderTotalBeforeApplyingCouopn=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
	//apply Coupon Code
	_setValue(_textbox("dwfrm_billing_couponCode"),$Default[0][5]);
	_click(_submit("dwfrm_billing_applyCoupon"));
	//verify success message
	_assertVisible(_span("success"));
	//enter billing details
	BillingAddress($Address,0);
	//select ogone as payment type
	_click(_radio("OGONE"));
	//select VISA as paymnet type
	_setSelected(_select("dwfrm_billing_paymentMethods_ogoneCard_type"),$Default[0][1]);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	var $couponDiscount=parseFloat(_extract(_getText(_div("discount clearfix  first ")),"/[$](.*)[)]/",true));
	var $expSubTotal=$orderTotalBeforeApplyingCouopn-$couponDiscount;
	var $expPrice=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
	var $actSubTotal=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
	_assertEqual($expSubTotal,$actSubTotal);
	//click on submit order
	_click(_submit("submit"));
	//fill card details
	ogonePayment($Ogone,0);
	//click on continue
	_click(_submit("payment"));
	_wait(20000,_isVisible(_table("ncol_ref")));
	//verify navigation to OCP page
	verifyOCPThroughOgone($Default,$expPrice);
	var $orderNum=_getText(_span("value", _in(_heading1("order-number"))));
	//verifying payment in BM
	verifyPaymentSecInBM($orderNum,$Ogone_BM,$expPrice);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

SiteURLs();
cleanup();