_include("../../util.GenericLibrary/GlobalFunctions.js");
_include("../../util.GenericLibrary/BM_Configuration.js");
_resource("Ogone.xls");

var $Default=_readExcelFile("Ogone.xls","Default");
var $Address=_readExcelFile("Ogone.xls","Address");
var $Ogone=_readExcelFile("Ogone.xls","Ogone");
var $addr_data=_readExcelFile("Ogone.xls","Multiship_Address");
var $Ogone_BM=_readExcelFile("Ogone.xls","Ogone_BM");


//enable Ogone in BM
enableOgone();

//navigate to URL
SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);
//login to the application
_click(_link("Login"));
login();

ClearCartItems();/*
var $t=_testcase("161752","Verify the scenario when registered user navigates back from the ogone site.");
$t.start();
try
{
	//navigate to shipping page
	navigateToShippingPage($Default[0][0],$Default[1][0]);
	//enter shipping address
	shippingAddress($Address,0);
	//check use this for billing check box
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	addressVerificationOverlay();
	//enter billing details
	BillingAddress($Address,0);
	//select ogone as payment type
	_click(_radio("OGONE"));
	//select VISA as paymnet type
	_setSelected(_select("dwfrm_billing_paymentMethods_ogoneCard_type"),$Default[0][1]);
	//click on continue on billing page
	_click(_submit("dwfrm_billing_save"));
	//click on submit order
	_click(_submit("submit"));
	//fill card details
	ogonePayment($Ogone,0);
	//click on back button
	_click(_button("Back"));
	//verify the navigation back to billing page
	verifyNavigationToBillingPageFromOgone($Default);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("161753","Verify the scenario when registered user navigates back from the ogone site and try to use different payment.");
$t.start();
try
{
	//select different payment type(American Express)
	_setSelected(_select("dwfrm_billing_paymentMethods_ogoneCard_type"),$Default[1][1]);
	//click on continue on billing page
	_click(_submit("dwfrm_billing_save"));
	var $expPrice=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
	//click on submit order
	_click(_submit("submit"));
	//fill card details
	ogonePayment($Ogone,1);
	//click on continue
	_click(_submit("payment"));
	_wait(20000,_isVisible(_table("ncol_ref")));
	//verify navigation to OCP page
	verifyOCPThroughOgone($Default,$expPrice);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("161747","Verify the scenario when registered user cancel the order in ogone site");
$t.start();
try
{
	//navigate to shipping page
	navigateToShippingPage($Default[0][0],$Default[1][0]);
	//enter shipping address
	shippingAddress($Address,0);
	//check use this for billing check box
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	addressVerificationOverlay();
	//enter billing details
	BillingAddress($Address,0);
	//select ogone as payment type
	_click(_radio("OGONE"));
	//select VISA as paymnet type
	_setSelected(_select("dwfrm_billing_paymentMethods_ogoneCard_type"),$Default[0][1]);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	//click on submit order
	_click(_submit("submit"));
	//fill card details
	ogonePayment($Ogone,0);
	//click cancel button
	_click(_submit("Cancel"));
	_expectConfirm("/Are you sure/",true);
	//verify the navigation
	verifyNavigationToBillingPageFromOgone($Default);

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("161756","Verify the scenario when registered user cancel the order from the ogone site and try to use different payment");
$t.start();
try
{
	//select different payment type(American Express)
	_setSelected(_select("dwfrm_billing_paymentMethods_ogoneCard_type"),$Default[1][1]);
	//click on continue on billing page
	_click(_submit("dwfrm_billing_save"));
	var $expPrice=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
	//click on submit order
	_click(_submit("submit"));
	//fill card details
	ogonePayment($Ogone,1);
	//click on continue
	_click(_submit("payment"));
	_wait(20000,_isVisible(_table("ncol_ref")));
	//verify navigation to OCP page
	verifyOCPThroughOgone($Default,$expPrice);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

SiteURLs();*/
var $t=_testcase("161743","Verify whether registered user is able to place order for multiple shipment with Credit card using Ogone as a payment option");
$t.start();
try
{
	//navigate to shipping page
	navigateToShippingPage($Default[0][0],$Default[2][0]);
	//click on yes button 'Do you want multishipping' 
	_click(_submit("dwfrm_singleshipping_shipToMultiple"));	
	var $addrCount=0;
	//adding addresses
	for(var $i=0;$i<$addr_data.length;$i++)
	{
		_click(_span("Add/Edit Address["+$i+"]"));
		addAddressMultiShip($addr_data,$i);
		$addrCount++;
	}
	//select addresses from drop down 
	var $dropDowns=_count("_select","/dwfrm_multishipping_addressSelection_quantityLineItems/",_in(_table("item-list")));
	var $c1=1;
	var $j=1;
	for(var $i=0;$i<$dropDowns;$i++)
		{
		   if($c1>$addrCount)
		    {
		      $c1=1;
		    }
		_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"),$j);
		$j++;
		}
	//click on continue
	_click(_submit("dwfrm_multishipping_addressSelection_save"));
	//click on continue
	_click(_submit("dwfrm_multishipping_shippingOptions_save"));
	//enter billing details
	BillingAddress($Address,0);
	//select ogone as payment type
	_click(_radio("OGONE"));
	//select VISA as paymnet type
	_setSelected(_select("dwfrm_billing_paymentMethods_ogoneCard_type"),$Default[0][1]);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	var $expPrice=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
	//click on submit order
	_click(_submit("submit"));
	//fill card details
	ogonePayment($Ogone,0);
	//click on continue
	_click(_submit("payment"));
	_wait(20000,_isVisible(_table("ncol_ref")));
	//verify navigation to OCP page
	verifyOCPThroughOgone($Default,$expPrice);
	var $orderNum=_getText(_span("value", _in(_heading1("order-number"))));
	//verifying payment in BM
	verifyPaymentSecInBM($orderNum,$Ogone_BM,$expPrice);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();




var $t=_testcase("161722","Verify whether registered user is able to place order using Ogone as a payment option");
$t.start();
try
{
	//navigate to URL
	SiteURLs();
	//login to the application
	_click(_link("Login"));
	login();
	//navigate to shipping page
	navigateToShippingPage($Default[0][0],$Default[1][0]);
	//enter shipping address
	shippingAddress($Address,0);
	//check use this for billing check box
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	addressVerificationOverlay();
	//enter billing details
	BillingAddress($Address,0);
	//select ogone as payment type
	_click(_radio("OGONE"));
	//select VISA as paymnet type
	_setSelected(_select("dwfrm_billing_paymentMethods_ogoneCard_type"),$Default[0][1]);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	var $expPrice=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
	//click on submit order
	_click(_submit("submit"));
	//fill card details
	ogonePayment($Ogone,0);
	//click on continue
	_click(_submit("payment"));
	_wait(20000,_isVisible(_table("ncol_ref")));
	//verify navigation to OCP page
	verifyOCPThroughOgone($Default,$expPrice);
	var $orderNum=_getText(_span("value", _in(_heading1("order-number"))));
	//verifying payment in BM
	verifyPaymentSecInBM($orderNum,$Ogone_BM,$expPrice);
	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("161739","Verify whether registered user is able to place order with combination of Coupon and Credit card using Ogone as a payment option.");
$t.start();
try
{
	//navigate to URL
	SiteURLs();
	//login to the application
	_click(_link("Login"));
	login();
	//navigate to shipping page
	navigateToShippingPage($Default[0][0],$Default[1][0]);
	//enter shipping address
	shippingAddress($Address,0);
	//check use this for billing check box
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	addressVerificationOverlay();
	//fetch order total before applying coupon
	var $orderTotalBeforeApplyingCouopn=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
	//apply Coupon Code
	_setValue(_textbox("dwfrm_billing_couponCode"),$Default[0][5]);
	_click(_submit("dwfrm_billing_applyCoupon"));
	//verify success message
	_assertVisible(_span("success"));
	//enter billing details
	BillingAddress($Address,0);
	//select ogone as payment type
	_click(_radio("OGONE"));
	//select VISA as paymnet type
	_setSelected(_select("dwfrm_billing_paymentMethods_ogoneCard_type"),$Default[0][1]);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	var $couponDiscount=parseFloat(_extract(_getText(_div("discount clearfix  first ")),"/[$](.*)[)]/",true));
	var $expSubTotal=$orderTotalBeforeApplyingCouopn-$couponDiscount;
	var $expPrice=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
	var $actSubTotal=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
	_assertEqual($expSubTotal,$actSubTotal);
	//click on submit order
	_click(_submit("submit"));
	//fill card details
	ogonePayment($Ogone,0);
	//click on continue
	_click(_submit("payment"));
	_wait(20000,_isVisible(_table("ncol_ref")));
	//verify navigation to OCP page
	verifyOCPThroughOgone($Default,$expPrice);
	var $orderNum=_getText(_span("value", _in(_heading1("order-number"))));
	//verifying payment in BM
	verifyPaymentSecInBM($orderNum,$Ogone_BM,$expPrice);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


//navigate to URL
SiteURLs();
cleanup();