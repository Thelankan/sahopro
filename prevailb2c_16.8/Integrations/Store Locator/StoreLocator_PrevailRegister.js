_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("StoreLocator.xls");


function SetStoreLocator()
{
BM_Login();
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));
_click(_link("PREVAIL Integrations"));
//var $PreviousValue=_getSelectedText(_select("MetabcIjQiaag0uYwaaadhGjpzlnq4"));

/*if (_isSafari())
{
_setSelected(_select("MetabcIjQiaag0uYwaaadhGjpzlnq4"),"PREVAIL");
}
else
	{
	_setSelected(_select("MetabcPPEiaag19XIaaadh11QACTfI"),"PREVAIL");//DEMANDWARE
	}*/

_setSelected(_select("inputfield_en", _in(_cell("PREVAIL DEMANDWARE"))),"PREVAIL");
_click(_submit("update"));
_click(_link("Log off."));
}

SetStoreLocator();
data();
SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();
_click(_link("Login"));
login();

var $Storelocator=_readExcelFile("StoreLocator.xls","Storelocator");
var $Validation=_readExcelFile("StoreLocator.xls","Validation");
//getting left navigation links from LeftNavLinks sheet
var $leftNavLink=_readExcelFile("StoreLocator.xls","LeftNavLinks");
var $data=_readExcelFile("StoreLocator.xls","Data");


var $t=_testcase("128108","Verify the navigation to 'Store Locator' page as a registered user in the application.");
$t.start();
try{
//click on store locator link
_click(_link($Storelocator[0][0]));
//verify the navigation
_assertVisible(_div("breadcrumb"));
_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
_assert(_isVisible(_heading1($Storelocator[0][1])));
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("128134","Verify the error message when search is not found for zipcode entered by registered user in the application.");
$t.start();
try{

//Navigate to store locator page	
_click(_link($Storelocator[0][0]));
_setValue(_textbox("dwfrm_storelocator_postalCode"),$data[0][1]);
_setSelected(_select("dwfrm_storelocator_maxdistance"),$data[1][1]);
_setSelected(_select("dwfrm_storelocator_state"),$data[2][2]);
//click on search button
_click(_submit("/Search/",_near(_select("dwfrm_storelocator_state"))));
//verify error message
_assertVisible(_div("store-locator-no-results"));
_assertEqual($Storelocator[0][7], _getText(_div("store-locator-no-results")));
	
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("128159","Verify the error message when search is not found for zipcode entered by registered user in the application.");
$t.start();
try{

//Navigate to store locator page	
_click(_link($Storelocator[0][0]));
_setValue(_textbox("dwfrm_storelocator_postalCode"),$Storelocator[2][5]);
//click on search button
_click(_submit("/Search/",_near(_select("dwfrm_storelocator_maxdistance"))));
//verify error message
_assertVisible(_div("store-locator-no-results"));
_assertEqual($Storelocator[0][7], _getText(_div("store-locator-no-results")));
	
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();



var $t=_testcase("128136","Verify the application behavior when registered user clicks on the address link from the 'Store Locator' results page in the application.");
$t.start();
try{

//Navigate to store locator page	
_click(_link($Storelocator[0][0]));
//
_setValue(_textbox("dwfrm_storelocator_postalCode"),$data[0][1]);
_setSelected(_select("dwfrm_storelocator_maxdistance"),$data[1][1]);
_setSelected(_select("dwfrm_storelocator_state"),$data[2][1]);
_setSelected(_select("dwfrm_storelocator_country"),$data[3][1]);
//Click on search button
_click(_submit("/Search/",_near(_select("dwfrm_storelocator_country"))));
//UI of store locator results page
if (_isIE())
	{
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	}
else
	{
	_assertEqual($Storelocator[1][2], _getText(_div("breadcrumb")));
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	}
_assertVisible(_heading1("/"+$data[2][3]+" /"));
_assertEqual($data[0][3], _getText(_div("store-locator-header")));
//Table Headers
_assertVisible(_tableHeader($data[0][2]));
_assertVisible(_tableHeader($data[1][2]));
//Google maps
_assertVisible(_div("map_canvas"));
_assertVisible(_link("<< Back to Store Locator"));
_assertEqual($data[0][3], _getText(_div("store-locator-header")));
_assertVisible(_div("click-area"));

	
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("128147","Verify the store locator results page when registered user search by State");
$t.start();
try{

	//Navigate to store locator page	
	_click(_link($Storelocator[0][0]));
	_setSelected(_select("dwfrm_storelocator_state"),$data[2][1]);
	//click on search button
	_click(_submit("/Search/",_near(_select("dwfrm_storelocator_state"))));
	//UI of store locator results page
	if (_isIE())
	{
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	}
else
	{
	_assertEqual($Storelocator[1][2], _getText(_div("breadcrumb")));
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	}
	_assertVisible(_heading1("/"+$data[2][3]+" /"));
	//_assertEqual($data[0][4], _getText(_div("store-locator-header")));
	_assert(_isVisible(_table("store-location-results")));
	_assertVisible(_tableHeader("STORE NAME"));
	_assertVisible(_tableHeader("ADDRESS"));
	//Google maps
	_assertVisible(_div("map_canvas"));
	_assertVisible(_link("<< Back to Store Locator"));
	_assertEqual($data[1][3], _getText(_div("store-locator-header")));
	_assertVisible(_div("click-area"));
	
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("128151","Verify the store locator results page when registered user search by Country (Search Internationally) in the application.");
$t.start();
try{

	//Navigate to store locator page	
	_click(_link($Storelocator[0][0]));
	_setSelected(_select("dwfrm_storelocator_country"),$data[3][1]);
	//click on search button
	_click(_submit("/Search/",_near(_select("dwfrm_storelocator_country"))));
	//UI of store locator results page
	if (_isIE())
	{
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	}
else
	{
	_assertEqual($Storelocator[1][2], _getText(_div("breadcrumb")));
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	}
	_assertVisible(_heading1("/"+$data[2][3]+" /"));
	_assertEqual($data[0][3], _getText(_div("store-locator-header")));
	//Table Headers
	_assert(_isVisible(_table("store-location-results")));
	_assertVisible(_tableHeader("STORE NAME"));
	_assertVisible(_tableHeader("ADDRESS"));
	//Google maps
	_assertVisible(_div("map_canvas"));
	_assertVisible(_link("<< Back to Store Locator"));
	_assertEqual($data[0][3], _getText(_div("store-locator-header")));
	_assertVisible(_div("click-area"));
	
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

/*//By default Country US is selected in the drop down so we can't execute below test script 
var $t=_testcase("128155","Verify the 'Country' drop down when registered user clicks on 'Search' button without selecting any drop-down value in the application.");
$t.start();
try{
	
	//Navigate to store locator page	
	_click(_link($Storelocator[0][0]));
	//click on search button near country field
	_click(_submit("/Search/",_near(_select("dwfrm_storelocator_country"))));
	//error message
	_assertEqual($Storelocator[1][7], _getText(_span("dwfrm_storelocator_country-error")));
	
	
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();
*/


var $t=_testcase("128112","Verify the UI of 'Store Locator' page for registered user in the application.");
$t.start();
try{
	
//Navigate back to store locator 	
_click(_link("/Back to Store Locator/"));
//verify the UI
_assertVisible(_div("breadcrumb"));
_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
//heading
_assert(_isVisible(_heading1($Storelocator[0][1])));
_assert(_isVisible(_paragraph("/(.*)/",_near(_heading1($Storelocator[0][1])))));
_assert(_isVisible(_heading2($Storelocator[1][1])));
_assert(_isVisible(_textbox("dwfrm_storelocator_postalCode")));
_assert(_isVisible(_span($Storelocator[3][1])));
_assert(_isVisible(_select("dwfrm_storelocator_maxdistance")));
_assert(_isVisible(_submit("dwfrm_storelocator_findbyzip")));;
_assertVisible(_span($Storelocator[4][1]));
_assert(_isVisible(_select("dwfrm_storelocator_state")));
_assert(_isVisible(_submit("dwfrm_storelocator_findbystate")));
_assert(_isVisible(_heading2($Storelocator[5][1])));
_assert(_isVisible(_span($Storelocator[6][1])));
_assert(_isVisible(_select("dwfrm_storelocator_country")));
_assert(_isVisible(_submit("dwfrm_storelocator_findbycountry")));
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("128118","Verify the UI of 'Store Locator' result page as a registered user in the application.");
$t.start();
try{
//select state 
_setSelected(_select("dwfrm_storelocator_state"),$Storelocator[0][3]);
//click on search
_click(_submit("dwfrm_storelocator_findbystate"));
//verify the UI
_assert(_isVisible(_heading1("/"+$Storelocator[1][3]+"/")));
_assert(_isVisible(__div("breadcrumb")));
_assertVisible(_link($Storelocator[2][2], _in(_div("breadcrumb"))));
_assert(_isVisible(_link("/Back to Store Locator/")));
_assert(_isVisible(_tableHeader("Store Name")));
_assert(_isVisible(_tableHeader("Address")));
//Google maps
_assertVisible(_div("map_canvas"));
_assert(_isVisible(_table("store-location-results")));
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("128116","Verify the Breadcrumb functionality in 'Store Locator' result page for registered user in the application.");
$t.start();
try{
//navigate to 'Store Locator' result page
_click(_link($Storelocator[0][0]));
_setSelected(_select("dwfrm_storelocator_state"),$data[2][1]);
//click on search
_click(_submit("dwfrm_storelocator_findbystate"));
_assertVisible(_div("breadcrumb"));
_assertVisible(_link($Storelocator[2][2], _in(_div("breadcrumb"))));
//click on store locator link
_click(_link($Storelocator[0][2],_in(_div("breadcrumb"))));
//verify the navigation
_assertVisible(_div("breadcrumb"));
_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
_assert(_isVisible(_heading1($Storelocator[0][1])));
//navigate to 'Store Locator' result page
_setSelected(_select("dwfrm_storelocator_state"),$Storelocator[0][3]);
//click on search
_click(_submit("dwfrm_storelocator_findbystate"));
//home link should not visible in bread crumb
_assertNotVisible(_link("Home",_in(__div("breadcrumb"))));


////click on home link
//_click(_link("Home",_in(__div("breadcrumb"))));
////verify the navigation to home page
//_assert(_isVisible(_list("homepage-slides")));
//_assert(_isVisible(_div("home-bottom-right")));

}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("128120","Verify the 'Store Locator' results page when any registered user clicks on Search button by enter valid 'Zipcode' and 'Radius'(Miles)");
$t.start();
try{
//click on store locator link
_click(_link($Storelocator[0][0]));	
//enter zip code
_setValue(_textbox("dwfrm_storelocator_postalCode"),$Storelocator[0][5]);
_click(_submit("dwfrm_storelocator_findbyzip"));
//verify the navigation
_assert(_isVisible(_heading1("/"+$Storelocator[1][3]+"/")));
_assert(_isVisible(_div($data[4][3]+" "+$Storelocator[0][5])));
_assert(_isVisible(_tableHeader("Store Name")));
_assert(_isVisible(_tableHeader("Address")));
//Google maps
_assertVisible(_div("map_canvas"));
_assert(_isVisible(_table("store-location-results")));
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("128122","Verify the application behavior when a registered user clicks on 'Search' button without entering any value in the input field");
$t.start();
try{
//back to store locator
_click(_link("/Back to Store Locator/"));
//validate zip code field
_click(_submit("dwfrm_storelocator_findbyzip"));
//verify error message
_assertEqual($Storelocator[0][6],_style(_textbox("dwfrm_storelocator_postalCode"),"background-color"));
_assert(_isVisible(_span($Storelocator[1][6])));
_assertEqual($Storelocator[2][6],_style(_span("dwfrm_storelocator_postalCode-error"),"color"));
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("128124","Verify application behavior when a value selected from 'Country' drop down and clicks on 'Search' button.");
$t.start();
try{
	//click on store locator link
	_click(_link($Storelocator[0][0]));
	//select country
	_setSelected(_select("dwfrm_storelocator_country"),$Storelocator[2][3]);
	//click on search
	_click(_submit("dwfrm_storelocator_findbycountry"));
	//verify the navigation
	_assert(_isVisible(_heading1("/"+$Storelocator[1][3]+"/")));
	_assert(_isVisible(_div($data[3][3]+" "+$Storelocator[2][3])));
	_assert(_isVisible(_tableHeader("Store Name")));
	_assert(_isVisible(_tableHeader("Address")));
	//Google maps
	_assertVisible(_div("map_canvas"));
	_assert(_isVisible(_table("store-location-results")));
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("128142","Verify the validation of Zip Code field in Store Locator page for registered user in the application.");
$t.start();
try{
	
	//click on store locator link
	_click(_link($Storelocator[0][0]));
	//validating zip code field
	for(var $i=0;$i<$Validation.length;$i++)
		{
		_setValue(_textbox("dwfrm_storelocator_postalCode"),$Validation[$i][1]);
		_click(_submit("dwfrm_storelocator_findbyzip"));
		//verify the error messsage
		_assertVisible(_div($Validation[0][2]));
		_assertEqual($Validation[1][2],_style(_div($Validation[0][2]),"color"));
		}
	
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("128128","Verify the 'Store Locator' results page when any registered user Search with State.");
$t.start();
try{
//select state 
_setSelected(_select("dwfrm_storelocator_state"), $Storelocator[0][3]);
//click on search
_click(_submit("dwfrm_storelocator_findbystate"));
//verify the navigation
_assert(_isVisible(_heading1("/"+$Storelocator[1][3]+"/")));
_assert(_isVisible(_div("store-locator-header")));
_assert(_isVisible(_tableHeader("Store Name")));
_assert(_isVisible(_tableHeader("Address")));
//Google maps
_assertVisible(_div("map_canvas"));
_assert(_isVisible(_table("store-location-results")));
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("128130","Verify the 'Store Locator' results page when any registered user Find Store with Country in the application.");
$t.start();
try{
_click(_link("/Back to Store Locator/"));
//select country
_setSelected(_select("dwfrm_storelocator_country"),$Storelocator[2][3]);
//click on search
_click(_submit("dwfrm_storelocator_findbycountry"));
//verify the navigation
_assert(_isVisible(_heading1("/"+$Storelocator[1][3]+"/")));
_assert(_isVisible(_div($data[3][3]+" "+$Storelocator[2][3])));
_assert(_isVisible(_tableHeader("Store Name")));
_assert(_isVisible(_tableHeader("Address")));
//Google maps
_assertVisible(_div("map_canvas"));
_assert(_isVisible(_table("store-location-results")));
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

/*//By default Country US is selected in the drop down so we can't execute below test script 
var $t=_testcase("128132","Verify the 'Country' drop down when registered user clicks on 'Search' button without selecting any value in the application.");
$t.start();
try{
_click(_link("/Back to Store Locator/"));
//don't select any value from drop down click on search
_click(_submit("dwfrm_storelocator_findbycountry"));
//verify the error message
_assert(_isVisible(_span($Storelocator[4][6])));
_assertEqual($Storelocator[2][6],_style(_span("dwfrm_storelocator_country-error"),"color"));
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();
*/

var $t=_testcase("128173","Verify the navigation of 'back to store locator' link in the Store results page as a registered user in the application.");
$t.start();
try{
	
	//Back to store locator
	_click(_link("/Back to Store Locator/"));
	
	//enter zipcode
	_setValue(_textbox("dwfrm_storelocator_postalCode"),$data[0][1]);
	_click(_submit("dwfrm_storelocator_findbyzip"));
	//Verifying 'Back to store locator' link in the Store results page
	_click(_link("/Back to Store Locator/"));
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	_assert(_isVisible(_heading1($Storelocator[0][1])));
	
	//select country
	_setSelected(_select("dwfrm_storelocator_country"),$Storelocator[2][3]);
	//click on search
	_click(_submit("dwfrm_storelocator_findbycountry"));
	//Verifying 'Back to store locator' link in the Store results page
	_click(_link("/Back to Store Locator/"));
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	_assert(_isVisible(_heading1($Storelocator[0][1])));
	
	//Select Country
	_setSelected(_select("dwfrm_storelocator_state"),$Storelocator[0][3]);
	//click on search
	_click(_submit("dwfrm_storelocator_findbystate"));
	//Verifying 'Back to store locator' link in the Store results page
	_click(_link("/Back to Store Locator/"));
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	_assert(_isVisible(_heading1($Storelocator[0][1])));
	
	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();




var $t=_testcase("128178","Verify the application behavior when user clicks left nav links in Store locator result page for registered user in the application.");
$t.start();
try
{
	//click on store locator link
	_click(_link($Storelocator[0][0]));	

	_assertEqual($leftNavLink[0][2], _getText(_span("toggle[0]")));//First Toggle is MY ACCOUNT
	_assertEqual($leftNavLink[1][2], _getText(_span("toggle[1]")));//Second Toggle is ORDER INFORMATION
	_assertEqual($leftNavLink[2][2], _getText(_span("toggle[2]")));//Third Toggle is WISH LIST
	_assertEqual($leftNavLink[3][2], _getText(_span("toggle[3]")));//Fourth Toggle is GIFT REGISTRIES
	_assertEqual($leftNavLink[4][2], _getText(_span("toggle[4]")));//Fifth Toggle is SHOP CONFIDENTLY
	
	for(var $i=0;$i<$leftNavLink.length;$i++)
	{
		//Click on links
		_click(_link($leftNavLink[$i][3]));
		//Verify the headings
		_assert(_isVisible(_heading1($leftNavLink[$i][4])));
		//Verify the bread crumb
		_assertVisible(_link($leftNavLink[$i][5], _in(_div("breadcrumb"))));
		_click(_link($Storelocator[0][0]));			
	}
	
	var $Count_leftnav=_count("_span","toggle",_in(_div("secondary-navigation")));

	//Click on the 'V' down arrow and verify
	for(var $i=0;$i<$Count_leftnav-1;$i++)
	{
		_click(_span("toggle["+$i+"]"));
		
		if($i==0)
			{
			_assertEqual(false,_isVisible(_link("Personal Data", _in(_listItem("Personal Data")))));
			_assertEqual(false,_isVisible(_link("Addresses", _in(_listItem("Addresses")))));
			_assertEqual(false,_isVisible(_link("Payment Settings", _in(_listItem("Payment Settings")))));
			}
		else if($i==1)
			{
			_assertEqual(false,_isVisible(_link("Order History")));
			}
		else if($i==2)
			{
			_assertEqual(false,_isVisible(_link("Modify Wish List")));
			_assertEqual(false,_isVisible(_link("Search Wish Lists")));
			}
		else if($i==3)
			{
			_assertEqual(false,_isVisible(_link("Create Registry")));
			_assertEqual(false,_isVisible(_link("Search Registries")));
			_assertEqual(false,_isVisible(_link("Modify Registries")));
			}
		else 
			{
			_assertEqual(false,_isVisible(_link("Privacy Policy")));
			_assertEqual(false,_isVisible(_link("Secure Shopping")));
			}
	}

	//again Click on the 'V' down arrow and verify	
	for(var $j=0;$j<$Count_leftnav-1;$j++)
	{
		_click(_span("toggle["+$j+"]"));
		
		if($j==0)
			{
			_assertEqual(true,_isVisible(_link("Personal Data", _in(_listItem("Personal Data")))));
			_assertEqual(true,_isVisible(_link("Addresses", _in(_listItem("Addresses")))));
			_assertEqual(true,_isVisible(_link("Payment Settings", _in(_listItem("Payment Settings")))));
			}
		else if($j==1)
			{
			_assertEqual(true,_isVisible(_link("Order History")));
			}
		else if($j==2)
			{
			_assertEqual(true,_isVisible(_link("Modify Wish List")));
			_assertEqual(true,_isVisible(_link("Search Wish Lists")));
			}
		else if($j==3)
		{
		_assertEqual(true,_isVisible(_link("Create Registry")));
		_assertEqual(true,_isVisible(_link("Search Registries")));
		_assertEqual(true,_isVisible(_link("Modify Registries")));
		}
		else 
			{
			_assertEqual(true,_isVisible(_link("Privacy Policy")));
			_assertEqual(true,_isVisible(_link("Secure Shopping")));
			}
	}

}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

