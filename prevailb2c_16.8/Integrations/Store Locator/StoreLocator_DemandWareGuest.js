_include("../../util.GenericLibrary/GlobalFunctions.js");

_resource("StoreLocator.xls");

function SetStoreLocator()
{
BM_Login();
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));
_click(_link("PREVAIL Integrations"));
//var $PreviousValue=_getSelectedText(_select("MetabcIjQiaag0uYwaaadhGjpzlnq4"));

/*if (_isSafari())
{
_setSelected(_select("MetabcIjQiaag0uYwaaadhGjpzlnq4"),"DEMANDWARE");
}
else
	{
	_setSelected(_select("MetabcPPEiaag19XIaaadh11QACTfI"),"DEMANDWARE");
	}*/

_setSelected(_select("inputfield_en", _in(_cell("PREVAIL DEMANDWARE"))),"DEMANDWARE");
_click(_submit("update"));
_click(_link("Log off."));
}

SetStoreLocator();


SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();

var $Storelocator=_readExcelFile("StoreLocator.xls","Storelocator");
var $Validation=_readExcelFile("StoreLocator.xls","Validation");
//getting left navigation links from LeftNavLinks sheet
var $leftNavLink=_readExcelFile("StoreLocator.xls","LeftNavLinks");
var $data=_readExcelFile("StoreLocator.xls","Data");

var $t=_testcase("156473","Verify the navigation to 'Store Locator' page as a guest user in the application.");
$t.start();
try{
//click on store locator link
_click(_link($Storelocator[0][0]));
//verify the navigation
_assertVisible(_div("breadcrumb"));
_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
_assert(_isVisible(_heading1($Storelocator[0][1])));
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("156495","Verify the error message in the Store locator result page when search is not found for zipcode entered by guest user in the application.");
$t.start();
try{

//Navigate to store locator page	
_click(_link($Storelocator[0][0]));
_setValue(_textbox("dwfrm_storelocator_postalCode"),$Storelocator[2][5]);
//click on search button
_click(_submit("/Search/",_near(_select("dwfrm_storelocator_maxdistance"))));
//verify error message
_assertVisible(_div("store-locator-no-results"));
_assertEqual($Storelocator[0][7], _getText(_div("store-locator-no-results")));
	
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("156489","Verify the store locator results page when guest user search by State");
$t.start();
try{

	//click on store locator link
	_click(_link($Storelocator[0][0]));
	//verify the UI
	_assertVisible(_div("breadcrumb"));
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	//heading
	_assert(_isVisible(_heading1($Storelocator[0][1])));
	_assert(_isVisible(_paragraph("/(.*)/",_near(_heading1($Storelocator[0][1])))));
	_assert(_isVisible(_heading2($Storelocator[1][1])));
	_assert(_isVisible(_span($Storelocator[2][1])));
	_assert(_isVisible(_textbox("dwfrm_storelocator_postalCode")));
	_assert(_isVisible(_span($Storelocator[3][1])));
	_assert(_isVisible(_select("dwfrm_storelocator_maxdistance")));
	_assert(_isVisible(_submit("dwfrm_storelocator_findbyzip")));
	_assert(_isVisible(_span($Storelocator[4][1])));
	_assert(_isVisible(_select("dwfrm_storelocator_state")));
	_assert(_isVisible(_submit("dwfrm_storelocator_findbystate")));
	_assert(_isVisible(_heading2($Storelocator[5][1])));
	_assert(_isVisible(_span($Storelocator[6][1])));
	_assert(_isVisible(_select("dwfrm_storelocator_country")));
	_assert(_isVisible(_submit("dwfrm_storelocator_findbycountry")));
	
	//Navigate to store locator page	
	_click(_link($Storelocator[0][0]));
	_setSelected(_select("dwfrm_storelocator_state"),$data[2][1]);
	//click on search button
	_click(_submit("/Search/",_near(_select("dwfrm_storelocator_state"))));
	//UI of store locator results page
	if (_isIE())
	{
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	}
else
	{
	_assertEqual($Storelocator[1][2], _getText(_div("breadcrumb")));
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	}
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	_assertVisible(_heading1("/Your search found /"));
	_assert(_isVisible(_table("store-location-results")));
	_assertVisible(_tableHeader("STORE NAME"));
	_assertVisible(_tableHeader("ADDRESS"));
	_assertVisible(_tableHeader("DIRECTIONS"));
	_assertVisible(_link("google-map"));
	_assertVisible(_link("<< Back to Store Locator"));
	_assertEqual($data[1][3], _getText(_div("store-locator-header")));
	
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("156491","Verify the store locator results page when guest user search by Country (Search Internationally) in the application.");
$t.start();
try{

	//Navigate to store locator page	
	_click(_link($Storelocator[0][0]));
	_setSelected(_select("dwfrm_storelocator_country"),$data[3][1]);
	//click on search button
	_click(_submit("/Search/",_near(_select("dwfrm_storelocator_country"))));
	//UI of store locator results page
	if (_isIE())
	{
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	}
else
	{
	_assertEqual($Storelocator[1][2], _getText(_div("breadcrumb")));
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	}
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	_assertVisible(_heading1("/Your search found /"));
	_assertEqual($data[0][4], _getText(_div("store-locator-header")));
	//Table Headers
	_assert(_isVisible(_table("store-location-results")));
	_assertVisible(_tableHeader("STORE NAME"));
	_assertVisible(_tableHeader("ADDRESS"));
	_assertVisible(_tableHeader("DIRECTIONS"));
	_assertVisible(_link("google-map"));
	_assertVisible(_link("<< Back to Store Locator"));
	_assertEqual($data[0][4], _getText(_div("store-locator-header")));
	
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

/*//By default Country US is selected in the drop down so we can't execute below test script 
var $t=_testcase("156493","Verify the 'Country' drop down when guest user clicks on 'Search' button without selecting any drop-down value in the application.");
$t.start();
try{
	
	//Navigate to store locator page	
	_click(_link($Storelocator[0][0]));
	//click on search button near country field
	_click(_submit("/Search/",_near(_select("dwfrm_storelocator_country"))));
	//error message
	_assertEqual($Storelocator[1][7], _getText(_span("dwfrm_storelocator_country-error")));
	
	
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();
*/


var $t=_testcase("156501","Verify the navigation of 'Back to store locator' link in the Store results page as a Guest User in the application.");
$t.start();
try{
	//click on 'Back to store locator' link 
	_click(_link("/Back to Store Locator/"));
	//enter zipcode
	_setValue(_textbox("dwfrm_storelocator_postalCode"),$data[0][1]);
	_click(_submit("dwfrm_storelocator_findbyzip"));
	//Verifying 'Back to store locator' link in the Store results page
	_click(_link("/Back to Store Locator/"));
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	_assert(_isVisible(_heading1($Storelocator[0][1])));
	
	//select country
	_setSelected(_select("dwfrm_storelocator_country"),$Storelocator[2][3]);
	//click on search
	_click(_submit("dwfrm_storelocator_findbycountry"));
	//Verifying 'Back to store locator' link in the Store results page
	_click(_link("/Back to Store Locator/"));
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	_assert(_isVisible(_heading1($Storelocator[0][1])));
	
	//Select Country
	_setSelected(_select("dwfrm_storelocator_state"),$Storelocator[0][3]);
	//click on search
	_click(_submit("dwfrm_storelocator_findbystate"));
	//Verifying 'Back to store locator' link in the Store results page
	_click(_link("/Back to Store Locator/"));
	_assertVisible(_link($Storelocator[0][2], _in(_div("breadcrumb"))));
	_assert(_isVisible(_heading1($Storelocator[0][1])));
	
	
}catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();



var $t=_testcase("156506","Verify the application behavior when user clicks left nav links in Store locator page for guest user in the application.");
$t.start();
try{
	// ACCOUNT SETTINGS 
	_assert(_isVisible(_div("nav")));
	_assertEqual($leftNavLink[0][0], _getText(_span("toggle[0]")));//First Toggle is ACCOUNT SETTINGS
	//Create an account link
	_click(_link($leftNavLink[0][1]));
	_assertVisible(_heading1("Create Account"));

	//SHOP CONFIDENTLY
	_assertEqual($leftNavLink[1][0], _getText(_span("toggle[1]")));//Second Toggle is SHOP CONFIDENTLY

	//Privacy Policy link
	_click(_link($Storelocator[0][0]));
	_click(_link($leftNavLink[2][1]));
	_assertVisible(_heading1("Privacy Policy"));

	//Secure shopping link
	_click(_link($Storelocator[0][0]));
	_click(_link($leftNavLink[3][1]));
	_assertVisible(_heading1("Security Policy"));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();





