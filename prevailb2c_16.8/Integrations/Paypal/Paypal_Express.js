//_include("../../util.GenericLibrary/BM_Configuration.js");
_include("../../util.GenericLibrary/GlobalFunctions.js");
_include("../../util.GenericLibrary/BM_Configuration.js");

_resource("Paypal.xls");

var $Search=_readExcelFile("Paypal.xls","Search Data");
var $addr_data=_readExcelFile("Paypal.xls","Address");
var $Paypal=_readExcelFile("Paypal.xls","Paypal");
var $CartDetails=_readExcelFile("Paypal.xls","CartDetails");
var $default=_readExcelFile("Paypal.xls","Default");
var $item = _readExcelFile("Paypal.xls","Item");
/*
BM_Login();
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));

//Click on PREVAIL Integrations link
_click(_link($userLog[2][2]));


//Gift Card Payment Service
_setSelected(_select("/inputfield_en/",_rightOf(_row("/Gift Card Payment Service:/"))),$Run[0][3]);
var $GiftCardPayment=_getSelectedText(_select("/(.*)/", _near(_row("/Gift Card Payment Service:/"))));

_log($GiftCardPayment+"......GiftCardPayment");
_assertEqual($Run[0][3],$GiftCardPayment,"Data not configured properly");



//Paypal


_check(_checkbox("/(.*)/", _near(_span("Is PayPal Express Enabled"))));

var $paypal=_checkbox("/(.*)/", _near(_span("Is PayPal Express Enabled"))).checked;

_log($paypal+"...........paypal");
//click on apply
_click(_submit("update"));

//Order section

//click on ordering
_click(_link("Ordering"));
_click(_link("Payment Methods"));



//gift certificate
var $gc=$Run[9][1];
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATE Gift Certificate/"))));
	}
else
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATEGift Certificate/"))));
	}
_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
_click(_div("/"+$gc+"/",_in(_div("x-combo-list-inner"))));
_click(_div("x-panel-header x-unselectable"));
_click(_button("Apply[1]"));
_wait(2000);
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATE Gift Certificate/"))),[2]).toString();
	}
else
	{
		var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATEGift Certificate/"))),[2]).toString();
	}
_log($giftCertificate+"......GiftCardPaymentorder");
_assertEqual($Run[9][1],$giftCertificate,"Data not configured properly");

//paypal
var $pp="Yes"
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/PayPal Pay Pal/"))));
	}
else
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/PayPalPay Pal/"))));
	}
_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
_click(_div("/"+$pp+"/",_in(_div("x-combo-list-inner"))));
_click(_div("x-panel-header x-unselectable"));
_click(_button("Apply[1]"));
_wait(2000);
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		var $paypalNormal=_getTableContents(_table("x-grid3-row-table", _in(_div("/PayPal Pay Pal/"))),[2]).toString();
	}
else
	{
		var $paypalNormal=_getTableContents(_table("x-grid3-row-table", _in(_div("/PayPalPay Pal/"))),[2]).toString();
	}
_log($paypalNormal+"...........paypalorder");
_assertEqual($Run[9][2],$paypalNormal,"Data not configured properly");



//Setting paypal payment type in payment processor Under payment methods
if ($paypal && $paypalNormal=="Yes")
	{
	
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/PayPal Pay Pal/"))));
		}
		else
		{
		_click(_table("x-grid3-row-table", _in(_div("/PayPalPay Pal/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))))
		_click(_div($BMConfig[0][6]));
		_click(_table("apply_payment_method_changes_button"));
	}


_click(_link("Log off."));




//creating gift card
createGC($GC);

*/
//navigate to URL
SiteURLs();
cleanup()
_setAccessorIgnoreCase(true);
//login to the application
_click(_link("Login"));
login();
ClearCartItems();


var $t=_testcase("127655","Verify the UI of the Cart page for PayPal_Express flow.");
$t.start();
try
{
//adding items to the cart
addItemToCart($Search[0][0],$Search[1][0]);
	if(_isVisible(_div("Bonus Productclose")))
	{
	   _click(_link("close"));
	}
_click(_link("mini-cart-link"));
//cart navigation verification
_assertVisible(_table("cart-table"));
_assertVisible(_div("cart-actions cart-actions-top"));
_assertVisible(_cell("item-image"));
_assertVisible(_div("name"));
_assertVisible(_div("sku"));
_assertVisible(_numberbox("input-text"));
_assertVisible(_span("price-sales"));
_assertVisible(_row("order-subtotal"));
_assertVisible(_textbox("dwfrm_cart_couponCode"));
_assertVisible(_submit("update-cart"));
_assertVisible(_row("order-shipping"));
_assertVisible(_row("order-sales-tax"));
_assertVisible(_row("order-total"));
_assertVisible(_submit("dwfrm_cart_checkoutCart"));
_assertVisible(_submit("dwfrm_cart_continueShopping"));
_assertVisible(_image("/(.*)/", _in(_div("cart-actions cart-actions-top"))));
var $cost=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
var $name=_getText(_div("name"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("127656","Verify the functionality of the 'Proceed to PayPal' button in Cart page for PayPal_Express flow.");
$t.start();
try
{
	_click(_image("/(.*)/", _in(_div("cart-actions cart-actions-top"))));
	//verify the navigation to pay pal login window
	_assertVisible(_div("PayPal Checkout"));
	_assertVisible(_emailbox("login_email"));
	_assertVisible(_password("login_password"));
	_assertVisible(_submit("btnLogin"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("127658","Verify the functionality of PayPal log in page with invalid credentials for PayPal_Express flow.");
$t.start();
try
{
	//validate the paypal UserName & password Fields
	for(var $j=1;$j<$Paypal.length;$j++)
	{
		_wait(2000);
		paypal($Paypal[$j][0],$Paypal[$j][1]);
		_assertVisible(_div($Paypal[$j][2]));
	}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("127657","Verify the functionality of PayPal log in page with valid credentials for PayPal_Express flow.");
$t.start();
try
{
	//login to the pay pal with valid credentials
	paypal($Paypal[0][0],$Paypal[0][1]);
	//verify the navigation 
	_assertVisible(_div("PayPal Checkout"));
	_assertVisible(_paragraph("reviewUserInfo"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("127659","Verify the UI of the PayPal review information page for PayPal_Express flow.");
$t.start();
try
{
	_assertVisible(_div("PayPal Checkout"));
	_assertVisible(_span("ltrOverride ng-binding"));
	//User name
	_assertVisible(_paragraph("reviewUserInfo"));
	//shipping address
	_assertVisible(_heading4("Ship to"));
	_assertVisible(_div("addressDisplay"));
	_assertVisible(_link("Change"));
	//Payment
	_assertVisible(_div("method BANK_ACCOUNT INSTANT_TRANSFER"));
	_assertVisible(_span("Pay with"));
	_assertVisible(_link("Change", _in(_div("paymentMethod"))));
	//policy
	_assertVisible(_link("PayPal Policies"));
	//continue
	_assertVisible(_submit("Continue"));
	//cancel
	_assertVisible(_span($default[5][0]));
	//click on arrow
	_click(_span("arrow ng-scope"));
	_assertVisible(_div("details"));
	_assertContainsText($name, _span("itemName ng-binding"));
	_assertVisible(_listItem("Item total $"+$cost+" USD"));
	_assertVisible(_listItem("/Shipping/"));
	_assertVisible(_div("/Subtotal /"));
	//click on close
	_click(_link("closeCart"));
	_assertNotVisible(_div("details"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("127660/127661","Verify whether user is able to edit the shipping address section of the PayPal review information page for PayPal_Express flow.");
$t.start();
try
{
	//click on change ship address link
	_click(_link("Change"));
	_wait(2000);
	var $count=_count("_link", "/shippingBlk/",_in(_list("items")));
	_log($count);
	for(var $i=0;$i<$count;$i++)
	{
		//change address
		_click(_link("shippingBlk["+$i+"]"));
		if($i==0)
			{
				var $changedAddress=_extract(_getText(_link("shippingBlk[0]")),"/(.*) Preferred /",true);
			}
		else
			{
				var $changedAddress=_getText(_link("shippingBlk["+$i+"]"));
			}
		_assertVisible(_div("Address updated"));
		_assertVisible(_div("addressDisplay"));
		_assertEqual($changedAddress+" United States", _getText(_div("addressDisplay")));
		_click(_link("Change"));
	}
	//change address
	_click(_link("shippingBlk", _in(_listItem("/(.*) Preferred/"))));
	_wait(2000);
	var $ShippingAddr=_getText(_div("addressDisplay"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
       _click(_link("Change"));
       //change address
   		_click(_link("shippingBlk", _in(_listItem("/(.*) Preferred/"))));
}
$t.end();



var $t=_testcase("127665","Verify the functionality of 'Cancel' link on PayPal window for PayPal_Express flow.");
$t.start();
try
{
_click(_span($default[5][0]));
//cart navigation verification
_assertVisible(_table("cart-table"));
_assertVisible(_div("cart-actions cart-actions-top"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("127664","Verify the functionality of 'Continue' button on PayPal window for PayPal_Express flow.");
$t.start();
try
{
	_click(_image("/(.*)/", _in(_div("cart-actions cart-actions-top"))));
	//login to paypal
	paypal($Paypal[0][0],$Paypal[0][1]);
	var $Addr=_getText(_div("addressDisplay")).replace(/,/g,"");
	var $PaypalPrice=_extract(_getText(_span("ltrOverride ng-binding")),"/[$](.*) USD/", true);
	//click on continue button
	_click(_submit("Continue"));
	_wait(5000,_isVisible(_fieldset("/Select or Enter Shipping Address /")));
	var $arr=new Array();
	$arr=[_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName")),_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName")),_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1")),_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_country")),_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")),_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"))];
	for(var $i=0;$i<$arr.length;$i++)
		{
		if($Addr.indexOf($arr[$i])>=0)
			{
				_assert(true, $arr[$i]+" data is Prepopulated");
			}
		else
			{
			_assert(false, $arr[$i]+" data is not Prepopulated");
			}
		}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("127668","Verify the UI of Shipping page navigating from PayPal_Express flow for logged in user.");
$t.start();
try
{
	//verify the shipping page UI
	_assertVisible(_fieldset("/Select or Enter Shipping Address /"));
	_assertVisible(_div("minishipments-method"));
	_assertVisible(_fieldset("Select Shipping Method"));
	_assertVisible(_div("shipping-method-list"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("127670/127663","Verify the shipping address displayed on the shipping page after navigating from PayPal_Express flow.");
$t.start();
try
{
//verify the selection of shipping address
//_assertEqual($Search[0][1],$headerName);
var $shippingAddress=_extract(_getText(_div("details")),"/(.*) Method/",true).toString().replace(",","");
_assertEqual($ShippingAddr,$shippingAddress);
shippingAddress($addr_data,0);
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
addressVerificationOverlay();
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("127671/127663","Verify the functionality of 'Continue' button on the shipping page after navigating from PayPal_Express flow.");
$t.start();
try
{
	//verify the navigation to Order Review 
	_assertVisible(_submit("Place Order"));
	//verify the selected payment method
	_assertVisible(_div("Pay Pal", _in(_div("mini-payment-instrument order-component-block  first "))));
	var $billingAddress=_getText(_div("details", _in(_div("mini-billing-address order-component-block")))).replace(",","");
	_assertEqual($ShippingAddr,$billingAddress);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("127662/127676/127674","Verify the functionality of changing the payment method to Credit card/Verify the order details on Order review page in the application while proceeding with Paypal Express.");
$t.start();
try
{
	//adding items to the cart
	addItemToCart($Search[0][0],$Search[1][0]);
	_click(_link("mini-cart-link"));
	//fetch the product name
	var $pName=_getText(_link("/(.*)/",_in(_div("name"))));
	var $qty=_getText(_numberbox("input-text"));
	
	_click(_link("mini-cart-link"));
	//navigate to pay pal
	_click(_image("/(.*)/", _in(_div("cart-actions cart-actions-top"))));
	//login to the pay pal with valid credentials
	paypal($Paypal[0][0],$Paypal[0][1]);
	//change the payment method to credit card
	_click(_link("Change", _in(_div("paymentMethod"))));
	_click(_link("PayPal Credit"));
	//click on continue
	_click(_submit("Continue"));
	//enter the shipping,billing addresses
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),$addr_data[0][9]);
	//fetch the itemPrice
	var $itemPrice=_extract(_getText(_row("order-total")),"/[$](.*)/",true);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	addressVerificationOverlay();
	//verify the navigation to order review page
	//_assertVisible(_heading2("Order Review"));
	//verify the billing address displayed in order review page
	var $billingAddress=_getText(_div("details", _in(_div("mini-billing-address order-component-block")))).replace(",","");
	//verifying with the  billing address and other details
	_assertEqual($pName,_getText(_link("/(.*)/",_in(_div("name")))));
	_assertEqual($qty,_getText(_cell("item-quantity")));
	_assertEqual($ShippingAddr,$billingAddress);
	_assertVisible(_submit("submit"));
	//verify the selected payment method
	_assertVisible(_div("Pay Pal", _in(_div("mini-payment-instrument order-component-block  first "))));
	//navigate to OCP 
	_click(_submit("Place Order"));
	if(_isVisible(_image("CloseIcon.png")))
	{
		_click(_image("CloseIcon.png"));
	}
	//verify the payment method
	_assertVisible(_div("payment-type"));
	_assertEqual("Pay Pal", _getText(_div("payment-type")));
	_assertVisible(_div("payment-amount"));
		if(_isIE())
		{
			var $paymentAmmount=_extract(_getText(_div("payment-amount")),"/[$] (.*)/",true);
		}
		else
		{
			var $paymentAmmount=_extract(_getText(_div("payment-amount")),"/[$](.*)/",true);
		}
		_assertEqual($itemPrice,$paymentAmmount);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

SiteURLs();
var $t=_testcase("127653","Verify whether user is able to place order only with PayPal while proceeding with Paypal Express.");
$t.start();
try
{
	//adding items to the cart
	addItemToCart($Search[0][0],$Search[1][0]);
		if(_exists(_div("Bonus Productclose")))
		{
		   _click(_link("close"));
		}
	_click(_link("mini-cart-link"));
	//click on pay pal link
	_click(_image("/(.*)/", _in(_div("cart-actions cart-actions-top"))));
	//login to pay pal
	paypal($Paypal[0][0],$Paypal[0][1]);
	//click on continue
	_click(_submit("Continue"));
	//phone number details
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),$addr_data[0][9]);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	addressVerificationOverlay();
	//navigate to OCP 
	_click(_submit("Place Order"));
	if(_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
	//Checking order is placed or not
	_assertVisible(_heading1($default[0][2]));
	_assertVisible(_div("order-confirmation-details"));
	//verify the payment method
	_assertVisible(_div("payment-type"));
	_assertEqual("Pay Pal", _getText(_div("payment-type")));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

//logout from application
_click(_link("user-account"));
_click(_link("Logout"));

var $t=_testcase("127669/127672","Verify the UI of Shipping page for guest user in the application while proceeding with Paypal Express.");
$t.start();
try
{

//adding items to the cart
addItemToCart($Search[0][0],$Search[1][1]);
_click(_link("mini-cart-link"));
//click on pay pal link
_click(_image("/(.*)/", _in(_div("cart-actions cart-actions-top"))));
//login to pay pal
paypal($Paypal[0][0],$Paypal[0][1]);
//click on continue
_click(_submit("Continue"));
//verify the navigation to IL Page
_assertVisible(_fieldset("/Select or Enter Shipping Address /"));
//phone number details
_click(_submit("dwfrm_singleshipping_shipToMultiple"));
_click(_span("Add/Edit Address[1]"));
addAddressMultiShip($addr_data,1);
//select addresses from drop down 
var $dropDowns=_count("_select","/dwfrm_multishipping_addressSelection_quantityLineItems/",_in(_table("item-list")));
var $c1=1;
var $j=1;
for(var $i=0;$i<$dropDowns;$i++)
	{
	   if($c1>$addrCount)
	    {
	      $c1=1;
	    }
	   _setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"),$j);
	   $j++;
	}

_click(_submit("dwfrm_multishipping_addressSelection_save"));
_click(_submit("dwfrm_multishipping_shippingOptions_save"));
BillingAddress($addr_data,0);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


cleanup();
SiteURLs();
var $t=_testcase("127654","Verify whether user is able to place order with Gift card and PayPal while proceeding with Paypal Express");
$t.start();
try
{
//Navigation till shipping page
navigateToShippingPage($item[0][0],$item[0][1]);
//shipping details
shippingAddress($addr_data,0);
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
addressVerificationOverlay();
//Billing details
BillingAddress($addr_data,0);
//applying GC
_setValue(_textbox("dwfrm_billing_giftCertCode"),$GC[0][0]);
_setValue(_textbox("dwfrm_billing_giftCertPin"),$GC[0][1]);
_click(_submit("dwfrm_billing_redeemGiftCert"));
//come back to cart page
_click(_link("mini-cart-link"));
//select express pay pal
_click(_image("/(.*)/", _in(_div("cart-actions cart-actions-top"))));
//login to pay pal
paypal($Paypal[0][0],$Paypal[0][1]);
//click on continue
_click(_submit("/Continue/",_in(_div("step"))));
//shipping details
shippingAddress($addr_data,0);
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
addressVerificationOverlay();
//Placing order
_click(_submit("submit"));
if(_isVisible(_image("CloseIcon.png")))
	{
	_click(_image("CloseIcon.png"));
	}
//Checking order is placed or not
_assertVisible(_heading1($default[0][2]));
_assertVisible(_div("order-confirmation-details"));

//checking Paypal
 var $paymentType=_getText(_div("payment-type"));
 _assertEqual($default[0][1],$paymentType);
 //checking GC
 _assert(_isVisible(_cell("order-payment-instruments")));
 var $exp=$GC[0][0].replace("GC","**");
 var $act=_extract(_getText(_div("orderpaymentinstrumentsgc")),"/Gift Certificate(.*)Amount:/",true).toString().trim();
 _assertEqual($exp,$act);
_assertEqual($GC[0][2],parseFloat(_extract(_getText(_span("payment-amount")),"/[$](.*)/",true)));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();



function paypal($paypalUN,$paypaalPwd)
{
	_setValue(_emailbox("login_email"), $paypalUN);
	_setValue(_password("login_password"), $paypaalPwd);
	_click(_submit("btnLogin"));
}
