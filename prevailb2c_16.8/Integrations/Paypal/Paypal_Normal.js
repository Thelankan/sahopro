_include("../../util.GenericLibrary/BM_Configuration.js");
_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Paypal.xls");

var $Search=_readExcelFile("Paypal.xls","Search Data");
var $addr_data=_readExcelFile("Paypal.xls","Address1");
var $Paypal=_readExcelFile("Paypal.xls","Paypal");
var $default=_readExcelFile("Paypal.xls","Default");
var $item = _readExcelFile("Paypal.xls","Item");
var $Billing_Data = _readExcelFile("Paypal.xls","Billing_Data");
var $shippingCharge;



BM_Login();
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));

//Click on PREVAIL Integrations link
_click(_link($userLog[2][2]));


//Gift Card Payment Service
_setSelected(_select("/inputfield_en/",_rightOf(_row("/Gift Card Payment Service:/"))),$Run[0][3]);
var $GiftCardPayment=_getSelectedText(_select("/(.*)/", _near(_row("/Gift Card Payment Service:/"))));

_log($GiftCardPayment+"......GiftCardPayment");
_assertEqual($Run[0][3],$GiftCardPayment,"Data not configured properly");



//click on apply
_click(_submit("update"));

//Order section

//click on ordering
_click(_link("Ordering"));
_click(_link("Payment Methods"));


//gift certificate
var $gc=$Run[9][1];
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATE Gift Certificate/"))));
	}
else
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATEGift Certificate/"))));
	}
_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
_click(_div("/"+$gc+"/",_in(_div("x-combo-list-inner"))));
_click(_div("x-panel-header x-unselectable"));
_click(_button("Apply[1]"));
_wait(2000);
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATE Gift Certificate/"))),[2]).toString();
	}
else
	{
		var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATEGift Certificate/"))),[2]).toString();
	}
_log($giftCertificate+"......GiftCardPaymentorder");
_assertEqual($Run[9][1],$giftCertificate,"Data not configured properly");

//paypal
var $pp=$Run[9][2];
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/PayPal Pay Pal/"))));
	}
else
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/PayPalPay Pal/"))));
	}
_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
_click(_div("/"+$pp+"/",_in(_div("x-combo-list-inner"))));
_click(_div("x-panel-header x-unselectable"));
_click(_button("Apply[1]"));
_wait(2000);
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		var $paypalNormal=_getTableContents(_table("x-grid3-row-table", _in(_div("/PayPal Pay Pal/"))),[2]).toString();
	}
else
	{
		var $paypalNormal=_getTableContents(_table("x-grid3-row-table", _in(_div("/PayPalPay Pal/"))),[2]).toString();
	}
_log($paypalNormal+"...........paypalorder");
_assertEqual($Run[9][2],$paypalNormal,"Data not configured properly");



//click on apply after selecting the payment processor
_click(_table("apply_payment_method_changes_button"));

//Setting paypal payment type in payment processor Under payment methods
if ($paypal && $paypalNormal=="Yes")
	{
	
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/PayPal Pay Pal/"))));
		}
		else
		{
		_click(_table("x-grid3-row-table", _in(_div("/PayPalPay Pal/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))))
		_click(_div($BMConfig[0][6]));
		_click(_table("apply_payment_method_changes_button"));
	}


_click(_link("Log off."));


//creating gift card
createGC($GC);


//navigate to URL
SiteURLs();
cleanup()
_setAccessorIgnoreCase(true);


var $t=_testcase("127636/127637","Verify the UI of Billing page and functionality of the PayPal radio button for PayPal_Normal flow.");
$t.start();
try
{
//adding items to the cart
addItemToCart($Search[0][0],$Search[1][0]);
if(_exists(_div("Bonus Productclose")))
{
   _click(_link("close"));
}
_click(_link("mini-cart-link")); 

var $name=_getText(_div("name"));
var $SKU=_extract(_getText(_div("sku")),"/: (.*)/",true);
var $price=_extract(_getText(_span("price-sales")),"/[$](.*)/",true);
var $quantity=_getText(_numberbox("input-text"));
var $subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true);
_click(_submit("dwfrm_cart_checkoutCart"));
_click(_submit("dwfrm_login_unregistered")); 
//navigate to shipping page
_assert(_isVisible(_fieldset("/SELECT OR ENTER A SHIPPING ADDRESS/")));
shippingAddress($addr_data,0);

_click(_submit("dwfrm_singleshipping_shippingAddress_save"));

if(_isVisible(_div("address-validation-dialog")))
{
	_click(_submit("ship-to-original-address"));
}
//verify the UI of billing page
billingPageUI($Billing_Data);
//fetching values from billing page
var $shippingAddress=_extract(_getText(_div("details")),"/(.*) Method/",true).toString().replace($addr_data[0][9],"");
var $orderTotal=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
var $salesTax=_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true);
$shippingCharge=parseFloat(_extract(_getText(_row("order-shipping  first ")),"/[$](.*)/",true));
if(_isVisible(_row("order-shipping-discount discount")))
	{
	var $discountPrice=parseFloat(_extract(_getText(_row("order-shipping-discount discount")),"/[$](.*)/",true));
	$shippingCharge=$shippingCharge-$discountPrice;
	}
//enter billing address
BillingAddress($addr_data,0);
_assert(_isVisible(_radio("PayPal")));
_click(_radio("PayPal"));
//checking whether paypal is selected or not
_assert(_radio("PayPal").checked);
_assert(_isVisible(_submit("dwfrm_billing_save")));
}
catch($e)
{      
       _logExceptionAsFailure($e);
       _click(_radio("PayPal"));
      //checking whether paypal is selected or not
      _assert(_radio("PayPal").checked);
      _assert(_isVisible(_submit("dwfrm_billing_save")));
}
$t.end();


var $t=_testcase("127638","Verify the functionality of the 'CONTINUE' button under PayPal option for PayPal_Normal flow.");
$t.start();
try
{
	_click(_submit("dwfrm_billing_save"));
	//verify the navigation to pay pal login window
	_assertVisible(_div("secureCheckout"));
	_assertVisible(_div("loginTitle"));
	_assertVisible(_textbox("login_email"));
	_assertVisible(_password("login_password"));
	_assertVisible(_submit("submitLogin"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("127641","Validate the log in fields on PayPal log in page with invalid credentials for PayPal_Normal flow.");
$t.start();
try
{
	//validate the paypal UserName & password Fields
	for(var $j=1;$j<$Paypal.length;$j++)
	{
		paypal($Paypal[$j][0],$Paypal[$j][1]);
		_assertEqual($Paypal[1][3],_getText(_span($Paypal[1][3])));
		_assertEqual($Paypal[1][4],_getText(_span($Paypal[1][4])));
	}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("127640","Verify the functionality of PayPal log in button on PayPal log in page with valid credentials for PayPal_Normal flow.");
$t.start();
try
{
	//login to the pay pal with valid credentials
	paypal($Paypal[0][0],$Paypal[0][1]);
	//verify the navigation 
	_assertVisible(_div("hdrContainer"));
	_assertEqual($Paypal[0][2],_getText(_div("hdrContainer")));
	_assertVisible(_div("reviewInfo"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("127639/127643","Verify the display of shipping address on Review your inormation page while placing the order with PayPal_Normal flow.");
$t.start();
try
{
	if(_isVisible(_checkbox("set_preferred_address")))
	{	
		var $adressInReviewInfoPage=_extract(_getText(_div("inset confidential")),"/(.*) Use/",true).toString();
	}
	else
	{
		var $adressInReviewInfoPage=_getText(_div("inset confidential")).toString();
	}
	_assertEqual($shippingAddress,$adressInReviewInfoPage);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("127642/127646","Verify the UI of the PayPal review information page for PayPal_Normal flow.");
$t.start();
try
{
	//verify the UI of PayPal review information page
	_assertVisible(_heading4($default[0][0]));
	_assertVisible(_div("reviewInfo"));
	if(_isVisible(_checkbox("checkbox")))
	{
		_assertVisible(_checkbox("checkbox"));
		_assertVisible(_label("prefship"));
	}
	_assertVisible(_heading4($default[1][0]));
	_assertVisible(_submit("funding_select"));
	_assertVisible(_submit("add_cc"));
	_assertVisible(_heading4($default[2][0]));
	var $userName=_getText(_span("confidential"));
	_assertEqual($Paypal[0][0],$userName);
	_assertVisible(_submit("continue_abovefold"));
	var $continue=_count("_submit","/Continue/");
	_assertEqual($default[3][0],$continue);
	_assertVisible(_heading3($default[4][0]));
	var $actualCost=_extract(_getText(_span("grandTotal amount highlight")),"/[$](.*) USD/",true);
	_assertEqual($orderTotal,$actualCost);
	var $itemName=_getText(_span("accessAid",_link("/showName/")));
	_assertEqual($name,$itemName);
	_assertVisible(_div("miniCart"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("127645","Verify the UI of the Order summary section for PayPal_Normal flow.");
$t.start();
try
{
	//verify the UI of the Order summary section
	var $itemSKU=_extract(_getText(_listItem("secondary")),"/:(.*)/",true);
	var $itemPrice=_extract(_getText(_listItem("secondary[1]")),"/[$](.*)/",true);
	var $itemQuantity=_extract(_getText(_listItem("secondary[2]")),"/:(.*)/",true).toString();
	var $tax=_extract(_getText(_span("minicartTaxAmount")),"/[$](.*)/",true);
	var $shippingTax=_extract(_getText(_span("displayShippingAmount")),"/[$](.*)/",true);
	_assertEqual($SKU,$itemSKU);
	_assertEqual($price,$itemPrice);
	_assertEqual($quantity,$itemQuantity);
	_assertEqual($salesTax,$tax);
	_assertEqual($shippingCharge,$shippingTax);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("TC_127649","Verify the functionality of 'Cancel' link on PayPal window for PayPal_Normal flow.");
$t.start();
try
{
//click on cancel link
_click(_submit("action"));
//verify the navigation back to billing page
_assert(_isVisible(_fieldset("/SELECT OR ENTER A BILLING ADDRESS/")));
_click(_submit("dwfrm_billing_save"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("127647/127648","Verify the functionality of changing the payment method to Credit card in PayPal_Normal flow.");
$t.start();
try
{
//login to pay pal
paypal($Paypal[0][0],$Paypal[0][1]);
_click(_submit("funding_select"));
//change payment method to credit card
_click(_radio("Credit card"));
_click(_submit("Continue"));
//click on continue
_click(_submit("/Continue/",_in(_div("step"))));
//verify the navigation	
//_assertVisible(_heading2("Order Review"));
_assert(_isVisible(_submit("submit")));
//verify the selected payment method
var $paymentType=_getText(_div("Pay Pal"));
_assertEqual($default[0][1],$paymentType);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("TC_127652","Verify the payment method displayed on Order confirmation page navigating from PayPal_Normal flow.");
$t.start();
try
{
	_click(_submit("submit"));
	if(_isVisible(_heading1($default[0][2])))
	{
	if(_isVisible(_image("CloseIcon.png")))
	{
		_click(_image("CloseIcon.png"));
	}
	_assert(_isVisible(_div("payment-type")));
	var $paymentType=_getText(_div("payment-type"));
	_assertEqual($default[0][1],$paymentType); 
	_assert(_isVisible(_div("payment-amount")));
		if(_isIE())
		{
			var $paymentAmmount=_extract(_getText(_div("payment-amount")),"/[$] (.*)/",true);
		}
		else
		{
			var $paymentAmmount=_extract(_getText(_div("payment-amount")),"/[$](.*)/",true);
		}
		_assertEqual($orderTotal,$paymentAmmount);
	}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


//placing order only with Paypal
SiteURLs();
var $t=_testcase("127632","Verify whether user is able to place order only with PayPal in the application while proceeding with Normal Paypal.");
$t.start();
try
{
//Navigation till shipping page
navigateToShippingPage($item[0][0],$item[0][1]);
//shipping details
shippingAddress($addr_data,0);
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
if(_isVisible(_div("address-validation-dialog")))
{
	_click(_submit("ship-to-original-address"));
}
//Billing details
BillingAddress($addr_data,0);
//Payment section through paypal
_click(_radio("PayPal"));
_click(_submit("dwfrm_billing_save"));
if(isMobile())
	 {
	 	_setValue(_emailbox("login_email"), $Paypal[0][0]);
	 }
 else
	 {
	 _setValue(_textbox("login_email"), $Paypal[0][0]);
	 }
 _setValue(_password("login_password"), $Paypal[0][1]);
 _click(_submit("Log In"));
 _click(_submit("Continue"));
 _wait(10000,_isVisible(_submit("submit")));
//Placing order
_click(_submit("submit"));
if(_isVisible(_image("CloseIcon.png")))
	{
	_click(_image("CloseIcon.png"));
	}
//Checking order is placed or not
_assertVisible(_heading1($default[0][2]));
_assertVisible(_div("order-confirmation-details"));

//checking Paypal
 var $paymentType=_getText(_div("payment-type"));
 _assertEqual($default[0][1],$paymentType);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("127632","Verify whether user is able to place order with Gift card and PayPal in the application while proceeding with Normal Paypal.");
$t.start();
try
{
//Navigation till shipping page
navigateToShippingPage($item[0][0],$item[0][1]);
//shipping details
shippingAddress($addr_data,0);
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
if(_isVisible(_div("address-validation-dialog")))
{
	_click(_submit("ship-to-original-address"));
}
//applying GC
_setValue(_textbox("dwfrm_billing_giftCertCode"),$GC[0][0]);
_setValue(_textbox("dwfrm_billing_giftCertPin"),$GC[0][1]);
_click(_submit("dwfrm_billing_redeemGiftCert"));
//Billing details
BillingAddress($addr_data,0);
//pay remaining amount through paypal
_click(_radio("PayPal"));
_click(_submit("dwfrm_billing_save"));
if(isMobile())
	 {
	 	_setValue(_emailbox("login_email"), $Paypal[0][0]);
	 }
 else
	 {
	 _setValue(_textbox("login_email"), $Paypal[0][0]);
	 }
 _setValue(_password("login_password"), $Paypal[0][1]);
 _click(_submit("Log In"));
 _click(_submit("Continue"));
 _wait(10000,_isVisible(_submit("submit")));
//Placing order
_click(_submit("submit"));
if(_isVisible(_image("CloseIcon.png")))
	{
	_click(_image("CloseIcon.png"));
	}
//Checking order is placed or not
_assertVisible(_heading1($default[0][2]));
_assertVisible(_div("order-confirmation-details"));

//checking Paypal
 var $paymentType=_getText(_div("payment-type"));
 _assertEqual($default[0][1],$paymentType);
 //checking GC
 _assert(_isVisible(_cell("order-payment-instruments")));
 var $exp=$GC[0][0].replace("GC","**");
 var $act=_extract(_getText(_div("orderpaymentinstrumentsgc")),"/Gift Certificate(.*)Amount:/",true).toString().trim();
 _assertEqual($exp,$act);
_assertEqual($GC[0][2],parseFloat(_extract(_getText(_span("payment-amount")),"/[$](.*)/",true)));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();
