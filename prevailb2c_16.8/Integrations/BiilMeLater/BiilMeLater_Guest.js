_include("../../util.GenericLibrary/BM_Configuration.js");
_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("BiilMeLater.xls");

var $bml=_readExcelFile("BiilMeLater.xls","BML");
var $data=_readExcelFile("BiilMeLater.xls","Data");
var $address=_readExcelFile("BiilMeLater.xls","Address");


if($GiftCardPayment==$BMConfig[0][3]  && $giftCertificate=="Yes")
	{
	//creating giftcard
	createGC($GC);
	//_log("created");
	}

SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();


var $t=_testcase("213700","Verify the UI of the Bill me Later section in payment section of the billing page as a Guest user.");
$t.start();
try
{
	//Navigate to billing page
	navigateToBillingPageWithAddress($data[0][0],$data[1][0],$address);
	//enter billing address
	BillingAddress($address,0);
	//Check for bill me later check box 
	_assertVisible(_label("Bill Me Later"));
	_assertVisible(_div("Bill Me Later"));
	_assertVisible(_label("Bill Me Later", _in(_div("payment-method-options form-indent"))));
	//Checking bill me later check box
	_assertVisible(_radio("is-BML"));
	_click(_radio("is-BML"));
	//UI of bill me later section
	_assertVisible(_div("payment-method payment-method-expanded"));
	//Static message
	_assertEqual($data[0][3], _getText(_paragraph("form-caption")));
	//UI of the page
	_assertVisible(_select("dwfrm_billing_paymentMethods_bml_year"));
	_assertVisible(_select("dwfrm_billing_paymentMethods_bml_month"));
	_assertVisible(_select("dwfrm_billing_paymentMethods_bml_day"));
	_assertVisible(_textbox("dwfrm_billing_paymentMethods_bml_ssn"));
	_assertVisible(_span("I have read and agree to the terms above"));
	_assertVisible(_checkbox("dwfrm_billing_paymentMethods_bml_termsandconditions"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("213692","Verify whether anonymous user is able to place order using BML as a payment option.");
$t.start();
try
{
	 //Entering bill me later values
	 BillMeLater_Values($bml,0);
	 //click on place order button
	 _click(_submit("dwfrm_billing_save"));
	 //Click on place order in order summary page
	 _click(_submit("button-fancy-large"));
	 //Verify Order confirmation page
	 _assertVisible(_heading1("Thank you for your order."));
	 var $orderno=_extract(_getText(_div("order-number ")),"/: (.*)/",true);
	 _log($orderno);
	 //Verifying bill me later section in order confirmation page
	 _assertEqual("Bill Me Later", _getText(_div("payment-type")));
	 
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("213699","Verify if the user is able to modify the Line items details after been authorized by Bill me later as a Guest user.");
$t.start();
try
{
	//Navigate to billing page
	navigateToBillingPageWithAddress($data[0][0],$data[1][0],$address);
	//enter billing address
	BillingAddress($address,0);
	//click on bml check box
	_click(_radio("is-BML"));
	//UI of bill me later section
	_assertVisible(_div("payment-method payment-method-expanded"));
	//Entering bill me later values
	 BillMeLater_Values($bml,0);
	 //click on place order button
	 _click(_submit("dwfrm_billing_save"));
	 //fetch quantity of product in order summary page
	 var $itemQtyInReviwePage=_getText(_cell("item-quantity", _in(_table("cart-table"))));
	 _assertEqual($quantity,$itemQtyInReviwePage);
	 //Navigate back to cart page by click on header
	 _click(_link("mini-cart-link"));
	 //Updating quantity 
	 _setValue(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity"),$data[1][1]);
	 //Click on update cart button
	 _click(_submit("update-cart"));
	 //Fetching quantity in cart page
	 var $quantityincartpage=_getText(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity"));
	 //navigate back to checkout page and place order with bill me later payment method
	//navigating to shipping page
	_click(_submit("dwfrm_cart_checkoutCart"));
	_wait(4000);
	if(isMobile())
		{
			_wait(3000);
		}
	
	_click(_link("user-account"));
	_wait(3000);
	
	if(!_isVisible(_link("Logout")))
		{
		_click(_submit("dwfrm_login_unregistered"));
		}
	//Enter shipping address
	shippingAddress($address,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//verifying address verification overlay
	addressVerificationOverlay();
	
	//Entering billing address
	BillingAddress($address,0)
	
	_click(_link("user-account"));
	_wait(2000);
	
	if(!_isVisible(_link("Logout")))
		{
		_setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),$address[0][10]);
		}
	
	//click on bml checkbox
	_click(_radio("is-BML"));
	//UI of bill me later section
	_assertVisible(_div("payment-method payment-method-expanded"));
	//Entering bill me later values
	 BillMeLater_Values($bml,0);
	 //click on place order button
	 _click(_submit("dwfrm_billing_save"));
	 //fetch quantity of product in order summary page
	 var $itemQtyInReviwePage=_getText(_cell("item-quantity", _in(_table("cart-table"))));
	 _assertEqual($quantityincartpage,$itemQtyInReviwePage);
	 
	 //Click on place order in order summary page
	 _click(_submit("button-fancy-large"));
	 //Verify Order confirmation page
	 _assertVisible(_heading1("Thank you for your order."));
	 var $orderno=_extract(_getText(_div("order-number ")),"/: (.*)/",true);
	 _log($orderno);
	 //Verifying bill me later section in order confirmation page
	 _assertEqual("Bill Me Later", _getText(_div("payment-type")));
	 
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("213696","Verify if the user is able to apply a valid coupon in billing page and place an order using Bill me later option as a guest user");
$t.start();
try
{
	//Navigate to billing page
	navigateToBillingPageWithAddress($data[0][0],$data[1][0],$address);
	//enter billing address
	BillingAddress($address,0);
	//before coupon total
	var $subtotalBeforeApplyingCoupon=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
	//Enter valid coupon code
	_setValue(_textbox("dwfrm_billing_couponCode"),$data[0][2]);
	//click on apply button
	_click(_submit("dwfrm_billing_applyCoupon"));
	//success message
	_assertEqual("Promo Code "+$data[0][2]+" has been added to your order and was applied.", _getText(_span("success")));
	//Click on bml checkbox
	_click(_radio("is-BML"));
	//UI of bill me later section
	_assertVisible(_div("payment-method payment-method-expanded"));
	//Entering bill me later values
	 BillMeLater_Values($bml,0);
	 //click on place order button
	 _click(_submit("dwfrm_billing_save"));
	 
		//verify wehter valid Coupon got applied or not
		_assertVisible(_span("Applied"));
		_assertVisible(_div("promo  first "));
		_assertVisible(_span("Coupon Number:"));
		_assertVisible(_span("value",_in(_div("discount clearfix  first "))));
		var $discountPrice=parseFloat(_extract(_getText(_span("value",_in(_div("discount clearfix  first ")))),"/[$](.*)/",true).toString());
		var $expDiscountPrice=$subtotalBeforeApplyingCoupon-$discountPrice;
		var $actDiscountPrice=parseFloat(_getText(_cell("item-total")).replace("$",""));
		//verifying the price
		_assertEqual($expDiscountPrice,$actDiscountPrice);
		//verify whether subtotal section got applied or not
		var $subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true);
		_assertEqual($actDiscountPrice,$subTotal);
	 
	 //Click on place order in order summary page
	 _click(_submit("button-fancy-large"));
	 //Verify Order confirmation page
	 _assertVisible(_heading1("Thank you for your order."));
	 var $orderno=_extract(_getText(_div("order-number ")),"/: (.*)/",true);
	 _log($orderno);
	 //Verifying bill me later section in order confirmation page
	 _assertEqual("Bill Me Later", _getText(_div("payment-type")));

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("213695","Verify whether anonymous user is able to place order using GC and BML as a payment options as a Guest user.");
$t.start();
try
{
	//Navigate to billing page
	navigateToBillingPageWithAddress($data[0][0],$data[1][0],$address);
	//address verification over lay
	addressVerificationOverlay();
	var $OrderTotal=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
	 _setValue(_textbox("dwfrm_billing_giftCertCode"),$GC[0][0]);
	 _setValue(_textbox("dwfrm_billing_giftCertPin"), $GC[0][1]);
	 _click(_submit("dwfrm_billing_redeemGiftCert"));
	 
	 if(_assertExists(_div("success giftcert-pi")))
	 {
	 var $didamt=parseFloat(_extract(_getText(_div("success giftcert-pi")),"/USD (.*) has/",true));
	 _log("Diducted amount is"+$didamt);
	  var $remamt=$OrderTotal-$didamt;
	 _log($remamt); 
	 }
	//enter billing address
	BillingAddress($address,0);
	//Click on bml checkbox
	_click(_radio("is-BML"));
	//UI of bill me later section
	_assertVisible(_div("payment-method payment-method-expanded"));
	//Entering bill me later values
	 BillMeLater_Values($bml,0);
	 //click on place order button
	 _click(_submit("dwfrm_billing_save"));
	 //checking whether navigated to Order review Page
	 if(_isVisible(_submit("submit"))){
	 _click(_submit("submit"));
	 }
	//checking whether navigated to Order conformation Page
	 if(_isVisible(_image("CloseIcon.png"))){
	 _click(_image("CloseIcon.png"));
	 }
	 _wait(5000,_isVisible(_heading1("Thank you for your order.")));
	 _assertExists(_heading1("Thank you for your order."));
	 
	 var $creditCardamt=parseFloat(_extract(_getText(_div("payment-amount")),"/[$](.*)/",true));
		 
		//checking UI in Order Confirmation page
		//checking 1GC
		var $count=_count("_span","/Gift Certificate/");
		 _assert(_isVisible(_cell("order-payment-instruments")));
		 _assertEqual(1,$count);
		 var $exp=$GC[0][0].replace("GC","");
		 _log($exp);
		 _assertContainsText($exp,_div("orderpaymentinstrumentsgc"));
		 
		 //Verify Order confirmation page
		 _assertVisible(_heading1("Thank you for your order."));
		 var $orderno=_extract(_getText(_div("order-number ")),"/: (.*)/",true);
		 _log($orderno);
		 //Verifying bill me later section in order confirmation page
		 _assertEqual("Bill Me Later", _getText(_div("payment-type")));
		 
		//checking Order total
		 if(_getText(_row("order-total")).indexOf("Order Total: $")>-1){
			 var $orderTotalAct=_getText(_row("order-total")).replace("Order Total: $","");
		 }else{
		 var $orderTotalAct=_getText(_row("order-total")).replace("Order Total:$","");
		 }
		 _assertEqual($OrderTotal,$orderTotalAct);
		 
		 var $Temp=$didamt+$creditCardamt;
			var $total=round($Temp,2);	
		 _assertEqual($OrderTotal,$total);
	
	

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


function BillMeLater_Values($sheet,$rowno)
{
	//Enter all the required fields in bill me later section
	_setSelected(_select("dwfrm_billing_paymentMethods_bml_year"),$sheet[$rowno][1]);
	_setSelected(_select("dwfrm_billing_paymentMethods_bml_month"),$sheet[$rowno][2]);
	_setSelected(_select("dwfrm_billing_paymentMethods_bml_day"),$sheet[$rowno][3]);
	_setValue(_textbox("dwfrm_billing_paymentMethods_bml_ssn"),$sheet[$rowno][4]);
	_check(_checkbox("dwfrm_billing_paymentMethods_bml_termsandconditions"))
	 
}

function navigateToBillingPageWithAddress($data,$quantity,$sheet)
{
	//Navigation till shipping page
	navigateToShippingPage($data,$quantity);
	//shipping details
	shippingAddress($sheet,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//verifying address verification overlay
	addressVerificationOverlay();	
	$orderTotal=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());
	_log($orderTotal);
}
