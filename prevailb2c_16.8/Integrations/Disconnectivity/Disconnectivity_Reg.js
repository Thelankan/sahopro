_include("../../util.GenericLibrary/GlobalFunctions.js");
_include("../../util.GenericLibrary/BM_Configuration.js");
_resource("Disconnectivity.xls");

var $item =_readExcelFile("Disconnectivity.xls","Item");
var $Valid_Data =_readExcelFile("Disconnectivity.xls","Valid_Data");
var $Paypal =_readExcelFile("Disconnectivity.xls","Paypal");

enableEndToEnd();


var $t=_testcase("166830","Verify when logon service is down");
$t.start();
try
{	
	//make logon service down
	makeServiceDown($item[0][4]);
	//place the order
	SiteURLs();
	_setAccessorIgnoreCase(true); 
	cleanup();

	_click(_link("user-login"));
	login();
	//place order
	orderPlacement($item,$Paypal,$Valid_Data,"false");
	//make log on service up and running
	makeServiceUpAndRunning($item[0][4]);
}
catch($e)
{
	_logExceptionAsFailure($e);
	//make log on service up and running
	makeServiceUpAndRunning($item[0][4]);
}
$t.end();


var $t=_testcase("166832","Verify when RTIC service is down.");
$t.start();
try
{	
	//make RTIC service down
	makeServiceDown($item[1][4]);
	SiteURLs();
	_setAccessorIgnoreCase(true); 
	cleanup();

	_click(_link("user-login"));
	login();
	orderPlacement($item,$Paypal,$Valid_Data,"false");
	//make log on service up and running
	makeServiceUpAndRunning($item[1][4]);
}
catch($e)
{
	_logExceptionAsFailure($e);
	//make log on service up and running
	makeServiceUpAndRunning($item[1][4]);
}
$t.end();


var $t=_testcase("166871","Verify the application as a registered user when credit card payment service (Cybersource-SOAP) is down");
$t.start();
try
{	
	//make logon service down
	makeServiceDown($item[2][4]);
	//place the order
	SiteURLs();
	_setAccessorIgnoreCase(true); 
	cleanup();

	_click(_link("user-login"));
	login();
	//place order
	orderPlacement($item,$Paypal,$Valid_Data,"false");
	//make log on service up and running
	makeServiceUpAndRunning($item[2][4]);
}
catch($e)
{
	_logExceptionAsFailure($e);
	//make log on service up and running
	makeServiceUpAndRunning($item[2][4]);
}
$t.end();


var $t=_testcase("166818","Verify the application as a registered user when tax service (Vertex-O) is down.");
$t.start();
try
{	
	//make logon service down
	makeServiceDown($item[3][4]);
	//place the order
	SiteURLs();
	_setAccessorIgnoreCase(true); 
	cleanup();

	_click(_link("user-login"));
	login();
	//place order
	orderPlacement($item,$Paypal,$Valid_Data,"true");
	//make log on service up and running
	makeServiceUpAndRunning($item[3][4]);
}
catch($e)
{
	_logExceptionAsFailure($e);
	//make log on service up and running
	makeServiceUpAndRunning($item[3][4]);
}
$t.end();


var $t=_testcase("166819","Verify the application as a registered user when Order HIstory service is down.");
$t.start();
try
{	
	//make logon service down
	makeServiceDown($item[4][4]);
	//place the order
	SiteURLs();
	_setAccessorIgnoreCase(true); 
	cleanup();

	_click(_link("user-login"));
	login();
	//navigate to order history
	_click(_link("Orders"));
	//check for the error message
	_assert(false,"not available");
	//make log on service up and running
	makeServiceUpAndRunning($item[4][4]);
}
catch($e)
{
	_logExceptionAsFailure($e);
	//make log on service up and running
	makeServiceUpAndRunning($item[4][4]);
}
$t.end();


var $t=_testcase("166822","Verify the application as a guest user when address standardization service (Group-1) is down.");
$t.start();
try
{	
	//make logon service down
	makeServiceDown($item[5][4]);
	//place the order
	SiteURLs();
	_setAccessorIgnoreCase(true); 
	cleanup();

	_click(_link("user-login"));
	login();
	navigateToShippingPage($item[0][0],$item[1][0]);
	//enter invalid shipping address
	shippingAddress($Invalid_Data,0);
	//click on continue
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//verify for the group-1 overlay
	_assertNotVisible(_submit("ship-to-original-address"));
	_assertNotVisible(_div("address-validation-dialog"));
	//click on continue
	_click(_submit("ship-to-original-address"));
	//enter invalid billing address and click on continue
	BillingAddress($Invalid_Data,0);
	//enter credit card details
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
		{
			  if($CreditCardPayment==$BMConfig[0][4])
			  {
			  	PaymentDetails($paypalSheet,2);
			  }
		  	  else
			  {
			  	PaymentDetails($creditCardSheet,3);
			  }
			//click on continue
			_click(_submit("dwfrm_billing_save"));	
		}
   else if($CreditCard=="No" && $paypalNormal=="Yes")
		{
	   		_assert(_radio("PayPal").checked);
			//click on continue
			_click(_submit("dwfrm_billing_save"));	
			//paypal
   			if(isMobile())
             {
             _setValue(_emailbox("login_email"), $paypalSheet[0][0]);
             }
   			else
             {
             _setValue(_textbox("login_email"), $paypalSheet[0][0]);
             }
          _setValue(_password("login_password"), $paypalSheet[0][1]);
          _click(_submit("Log In"));
          _click(_submit("Continue"));
          _wait(10000,_isVisible(_submit("submit")));
		}
	//address verification overlay shouldn't come
	_assertNotVisible(_submit("ship-to-original-address"));
	_assertNotVisible(_div("address-validation-dialog"));
	//make log on service up and running
	makeServiceUpAndRunning($item[5][4]);
}
catch($e)
{
	_logExceptionAsFailure($e);
	//make log on service up and running
	makeServiceUpAndRunning($item[5][4]);
}
$t.end();


var $t=_testcase("166826","Verify the application as a registered user when credit card payment service (Cybersource-SCMP) is down");
$t.start();
try
{	
	//make logon service down
	makeServiceDown($item[6][4]);
	//place the order
	SiteURLs();
	_setAccessorIgnoreCase(true); 
	cleanup();

	_click(_link("user-login"));
	login();
	//place order
	orderPlacement($item,$Paypal,$Valid_Data,"false");
	//make log on service up and running
	makeServiceUpAndRunning($item[6][4]);
}
catch($e)
{
	_logExceptionAsFailure($e);
	//make log on service up and running
	makeServiceUpAndRunning($item[6][4]);
}
$t.end();


disableEndToEnd();