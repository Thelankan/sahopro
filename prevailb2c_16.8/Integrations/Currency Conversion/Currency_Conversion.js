_include("../../util.GenericLibrary/BrowserSpecific.js");
_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Currency_Conversion.xls");
_resource("../../Profile/GiftRegistry/GiftRegistry_Data.xls");


SiteURLs();
cleanup();
_setAccessorIgnoreCase(true); 
_click(_link("user-login"));
login();

//deleting gift registries
Delete_GiftRegistry();

var $data=_readExcelFile("Currency_Conversion.xls","Data");
var $Address=_readExcelFile("Currency_Conversion.xls","Address");
var $creditcard_data=_readExcelFile("Currency_Conversion.xls","Payment");
var $GiftRegistry=_readExcelFile("../../Profile/GiftRegistry/GiftRegistry_Data.xls","GiftRegistry");


var $FirstName1=$FName;
var $LastName1=$LName;
var $UserEmail=$uId;
var $HomePage=_span("/product-sales-price/", _in(_div("product-tile")));
var $rootCategoryRefinements=_link("/refinement-link/", _in(_div("refinement Price")));
var $categoryProduct=_span("/product-sales-price/", _in(_div("product-tile")));
//var $categoryRefinements=_link("/refinement-link/", _in(_div("refinement Price")));
var $PLPageProduct=_span("/product-sales-price/", _in(_div("product-tile")));
var $searchresult=_span("/product-sales-price/", _in(_div("product-tile")));
//var $PLPageRefinement=_span("/product-sales-price/", _in(_div("product-tile")));
var $Compare=_span("product-sales-price");
var $quickview=_span("price-sales");
var $PDP=_span("price-sales");
var $wishlist=_span("price-sales");
var $giftregistry=_span("price-sales");
var $minicart=_span("value", _in(_div("mini-cart-subtotals")));
var $cartSubTotal=_span("price-total");
var $cartEstimatedTotal=_row("order-total");
var $shipping=_row("order-total");
var $billing=_row("order-total");
var $orderSummarySubTotal=_row("order-subtotal");
var $orderSummaryOrderTotal=_row("order-total");
var $orderConfirmationSubTotal=_row("order-subtotal");
var $orderConfirmationEstimatedTotal=_row("order-total");
var $orderConfirmationItemprice=_cell("line-item-price");
var $orderhistorypage=_table("order-history-table");
var $orderdetailspage=_row("order-total");




function CurrencyVerify($PriceExcel,$Rowno)
{
	if(_getText($PriceExcel).indexOf($data[$Rowno][1])>-1)
	{
	_assert(true,$data[$Rowno][0]+"="+$data[$Rowno][1]);
	}
else
	{
	_assert(false,"Symbol is not expected as per currency selected");
	}
}
		


var $t=_testcase("155855/155856/134096/134098/134099/134101/134103/134105/134106/134107/134119/134111/134112","Verify the currency changes when INR currency is applied");
$t.start();
try
{
	var $dropdownlength=_select("currencyMnemonic").length;
	for(var $i=0; $i<$dropdownlength; $i++)
		{
			_setSelected(_select("currencyMnemonic"),$data[$i][0]);
			var $SelectedValue=_getSelectedText(_select("currencyMnemonic"));
			_assertEqual($SelectedValue,$data[$i][0]);		
		}
	for(var $i=0; $i<$dropdownlength; $i++)
		{		
			_setSelected(_select("currencyMnemonic"),$data[$i][0]);		
			//Home Page
			_click(_image("Demandware SiteGenesis"));
			CurrencyVerify($HomePage,$i);		
			//Root Category Page
			_click(_link($data[0][5], _in(_list("menu-category level-1"))));
			CurrencyVerify($rootCategoryRefinements,$i);		
			//Category Page
			_click(_link($data[1][5]));
			CurrencyVerify($categoryProduct,$i)
			//CurrencyVerify($categoryRefinements,$i);		
			//Subcategory-PLP
			_click(_link($data[2][5]));
			CurrencyVerify($PLPageProduct,$i);
			//CurrencyVerify($PLPageRefinement,$i);		
			//Search Results Page
			search($data[3][5]);
			CurrencyVerify($searchresult,$i);		
			//PDP page
			_click(_link("name-link"));
			CurrencyVerify($PDP,$i);		
			//Wish List Page
			selectSwatch();
			_click(_submit("add-to-cart"));	
			//Mini Cart
			CurrencyVerify($minicart,$i);		
			_click(_link("Add to Wishlist"));
			CurrencyVerify($wishlist,$i);		
			//Cart
			_click(_link("View Cart"));
			CurrencyVerify($cartSubTotal,$i);
			CurrencyVerify($cartEstimatedTotal,$i);		
			//Shipping Page
			_click(_submit("dwfrm_cart_checkoutCart"));
			shippingAddress($Address,0);
			CurrencyVerify($shipping,$i);		
			//Billing Page
			_click(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
			_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
			if(_isVisible(_div("address-validation-dialog")))
				{
					_click(_submit("ship-to-original-address"));
				}
			CurrencyVerify($billing,$i);
			 PaymentDetails($creditcard_data,0);		 
			//click on submit order
			 _click(_submit("dwfrm_billing_save"));		 
			 //Order Summary
			 CurrencyVerify($orderSummarySubTotal,$i);
			 CurrencyVerify($orderSummaryOrderTotal,$i);		 
			 //Order Confirmation
			 _click(_submit("submit"));		 
			//verify the navigation to OCP page
			 if(_isVisible(_image("CloseIcon.png")))
			 	{
			 		_click(_image("CloseIcon.png"));
			 	}		 
			 CurrencyVerify($orderConfirmationSubTotal,$i);
			 CurrencyVerify($orderConfirmationEstimatedTotal,$i);
			 CurrencyVerify($orderConfirmationItemprice,$i);		
		}	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();



var $t=_testcase("134108","Verify the functionality related to currency change on Gift Registry page in application as a Registered user.");
$t.start();
try
{	
	_setSelected(_select("currencyMnemonic"),$data[0][0]);
	_click(_link("user-account"));
	_click(_link("Gift Registries"));
	//click on new registry
	_click(_submit("dwfrm_giftregistry_create"));
	//creating the registry
	createRegistry($GiftRegistry,0);
	//entering pre post address
	Pre_PostEventInfo($GiftRegistry,4);
	_click(_submit("Submit"));
	addItems($data[1][2]);
	CurrencyVerify($giftregistry,0);
	_setSelected(_select("currencyMnemonic"),$data[1][0]);
	CurrencyVerify($giftregistry,1);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();



var $t=_testcase("134113/134114","Verify the Behavior of Order History and order details Page when user changes Currency.");
$t.start();
try
{	 
	 //Click on user account
	 _click(_link("user-account"));
	 //click on orders link
	 _click(_link("Orders"))	 
	 if (_isVisible(_submit("Order Details")) && _isVisible(_div("order-history-header")))
		 {		 
			 _setSelected(_select("currencyMnemonic"),$data[0][0]);
			 CurrencyVerify($orderhistorypage,3);
			 _click(_submit("Order Details"));
			 CurrencyVerify($orderdetailspage,3);		 
		 }
	 else
		 {
			 _setSelected(_select("currencyMnemonic"),$data[0][0]);
			//navigating to billing page
			navigateToCart($data[0][2],1);
			_click(_submit("dwfrm_cart_checkoutCart"));
			_log($quantity);
			_log($price);
			//check use this for billing check box
			_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
			shippingAddress($Address,0);			
			_click(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
			_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
			if(_isVisible(_div("address-validation-dialog")))
				{
					_click(_submit("ship-to-original-address"));
				}
			//click on submit order
			 _click(_submit("dwfrm_billing_save"));			
			PaymentDetails($creditcard_data,0);
			_click(_submit("dwfrm_billing_save"));			
			 //Order Confirmation
			 _click(_submit("submit"));			 
			//verify the navigation to OCP page
			 if(_isVisible(_image("CloseIcon.png")))
			 	{
			 	_click(_image("CloseIcon.png"));
			 	}
			 _click(_link("user-account"));
			 //click on orders link
			 _click(_link("Orders"));			 
			 _setSelected(_select("currencyMnemonic"),$data[1][0]);
			 CurrencyVerify($orderhistorypage,0);
			 _click(_submit("Order Details"));
			 CurrencyVerify($orderdetailspage,0);				 
		 }
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("134109/134110","Verify whether user can apply valid coupon for selected and different currency.");
$t.start();
try
{	
	//Selecting currency to USD
	_setSelected(_select("currencyMnemonic"),$data[0][0]);
	//Adding product to cart
	navigateToCart($data[0][2],1);
	//Applying the coupon code which is created for USD currency
	_setValue(_textbox("dwfrm_cart_couponCode"),$data[0][3]);
	_click(_submit("add-coupon"));
	//Coupon should be applied and Order sub total and estimated sub total should be updated
	_assertVisible(_row("rowcoupons"));
	_assertVisible(_span("Applied", _in(_row("rowcoupons"))));	
	ClearCartItems();	
	//Selecting currency to USD
	_setSelected(_select("currencyMnemonic"),$data[1][0]);
	//Adding product to cart
	navigateToCart($data[1][2],1);
	//Applying the coupon code which is created for USD currency
	_setValue(_textbox("dwfrm_cart_couponCode"),$data[0][3]);
	_click(_submit("add-coupon"));
	//Should not apply for other currency
	_assertNotVisible(_row("rowcoupons"));
	_assertVisible(_div("Coupon code \""+$data[0][3]+"\" can currently not be added to your cart."));	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//==================================  Functions  ======================================= 

function createRegistry()
{
	_setSelected(_select("dwfrm_giftregistry_event_type"),$GiftRegistry[0][1]);
	_setValue(_textbox("dwfrm_giftregistry_event_name"),$GiftRegistry[0][2]);
	_setValue(_textbox("dwfrm_giftregistry_event_date"),$GiftRegistry[0][3]);
	_setSelected(_select("dwfrm_giftregistry_event_eventaddress_country"),$GiftRegistry[0][4]);
	_setSelected(_select("dwfrm_giftregistry_event_eventaddress_states_state"),$GiftRegistry[0][5]);
	_setValue(_textbox("dwfrm_giftregistry_event_town"),$GiftRegistry[0][6]);
	_setSelected(_select("dwfrm_giftregistry_event_participant_role"),$GiftRegistry[0][7]);
	_setValue(_textbox("dwfrm_giftregistry_event_participant_firstName"),$FirstName1);
	_setValue(_textbox("dwfrm_giftregistry_event_participant_lastName"),$LastName1);
	_setValue(_textbox("dwfrm_giftregistry_event_participant_email"),$UserEmail);
	_setSelected(_select("dwfrm_giftregistry_event_coParticipant_role"),$GiftRegistry[0][11]);
	_setValue(_textbox("dwfrm_giftregistry_event_coParticipant_firstName"),$GiftRegistry[0][12]);
	_setValue(_textbox("dwfrm_giftregistry_event_coParticipant_lastName"),$GiftRegistry[0][13]);
	_setValue(_textbox("dwfrm_giftregistry_event_coParticipant_email"),$GiftRegistry[0][14]);
	_click(_submit("dwfrm_giftregistry_event_confirm"));	
}

function Pre_PostEventInfo()
{
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_firstname"),$GiftRegistry[4][1]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_lastname"),$GiftRegistry[4][2]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address1"),$GiftRegistry[4][3]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address2"),$GiftRegistry[4][4]);
	_setSelected(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_country"),$GiftRegistry[4][5]);
	_setSelected(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_states_state"),$GiftRegistry[4][6]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_city"),$GiftRegistry[4][7]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_postal"),$GiftRegistry[4][8]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_phone"),$GiftRegistry[4][9]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_firstname"),$GiftRegistry[4][1]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_lastname"),$GiftRegistry[4][2]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_address1"),$GiftRegistry[4][3]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_address2"),$GiftRegistry[4][4]);
	_setSelected(_select("dwfrm_giftregistry_eventaddress_addressAfterEvent_country"),$GiftRegistry[4][5]);
	_setSelected(_select("dwfrm_giftregistry_eventaddress_addressAfterEvent_states_state"),$GiftRegistry[4][6]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_city"),$GiftRegistry[4][7]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_postal"),$GiftRegistry[4][8]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_phone"),$GiftRegistry[4][9]);
	_click(_submit("dwfrm_giftregistry_eventaddress_confirm"));
}

function SendtoFriend()
{
_setValue(_textbox("dwfrm_sendtofriend_friendsname"),$GiftRegistry[7][1]);
_setValue(_textbox("dwfrm_sendtofriend_friendsemail"),$GiftRegistry[7][2]);
_setValue(_textbox("dwfrm_sendtofriend_confirmfriendsemail"),$GiftRegistry[7][3]);
_setValue(_textarea("dwfrm_sendtofriend_message"),$GiftRegistry[7][4]);       
}

function addItems()
{
	_click(_link("Add Items"));
	//Verifying navigation
	_assertVisible(_list("homepage-slides"));
	_assertVisible(_div("home-bottom-slots"));
	//Adding the items to gift registry
	_click(_link($data[1][2]));
	//click on name link
	$ProductName=_getText(_link("name-link"));
	_click(_link("name-link"));
	//fetching product name
	$ProductPrice=_getText(_div("product-price"));	
	$QTY=_getText(_textbox("Quantity"));	
	//select swatches
	if(_isVisible(_span("SELECT COLOR")))
    {
    $colorBoolean=true;
    if(!_isVisible(_listItem("selected",_in(_list("swatches Color")))))
           {
           _click(_link("/swatchanchor/",_in(_listItem("emptyswatch",_in(_list("swatches Color"))))));
           }
    }
if(isMobile())
    {
    _wait(4000);
    }
if(_isVisible(_span("SELECT SIZE")))
    {
    $sizeBoolean=true;
    if(!_isVisible(_listItem("selected",_in(_list("swatches size")))))
           {
           _click(_link("/swatchanchor/",_in(_listItem("emptyswatch",_in(_list("swatches size"))))));
           }
    }
if(isMobile())
{
_wait(4000);
}
if(_isVisible(_span("SELECT WIDTH")))
    {
    $widthBoolean=true;
           if(!_isVisible(_listItem("selected",_in(_list("swatches width")))))
           {
           _click(_link("/swatchanchor/",_in(_listItem("emptyswatch",_in(_list("swatches width"))))));
           }
    }

	$ProductNumber=_extract(_getText(_div("product-number")), "/Item# (.*)/", true).toString();
	_click(_link("Add to Gift Registry"));	
}



