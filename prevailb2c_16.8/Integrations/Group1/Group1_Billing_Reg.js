_include("../../util.GenericLibrary/GlobalFunctions.js");
_include("../../util.GenericLibrary/BM_Configuration.js");
_resource("Group1.xls");

var $Address=_readExcelFile("Group1.xls","Address");
var $item=_readExcelFile("Group1.xls","Item");
var $validAddress=_readExcelFile("Group1.xls","validAddress");
var $paymentData=_readExcelFile("Group1.xls","paymentData");
var $billmelater=_readExcelFile("Group1.xls","BillMe");



//navigate to URL
SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);

/*_click(_link("Login"));
login();
//removing item from cart
ClearCartItems();

var $t=_testcase("158082/158061/158063","Verify the Group1 behavior in the application when a registered user navigates back to billing accordion from any other page.","Verify the Billing address validation by Group1 for a guest user in the application.","Verify Group1 behavior in the application when a guest user enters an Invalid (Non exact match) Billing address.");
$t.start();
try
{
//Navigation till shipping page
navigateToShippingPage($item[0][0],$item[0][1]);
shippingAddress($validAddress,0);
//Navigate to billing page
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
for(var $i=0;$i<$Address.length;$i++)
	{
		//enter Billing address
		BillingAddress($Address,$i);
		//Enter payment details
		PaymentDetails($paymentData,0)
		//click on continue
         _click(_submit("dwfrm_billing_save")); 
		if($i==0 || $i==1)
			{
			var $actCity=_extract(_getText(_div("details",_in(_div("/BILLING ADDRESS/")))),"/(.*)[,]/",true).toString().split(" ")[6];
			//var $zipCode=_extract(_getText(_div("details",_in(_div("/BILLING ADDRESS/")))),"/[,](.*)/",true).toString().trim().split(" ")[1];
			//var $addedZipCode=_extract($zipCode,"/[-](.*)/",true); 
			var $addedZipCode=_extract(_getText(_div("details",_in(_div("/BILLING ADDRESS/")))),"/[,](.*)/",true).toString().split(" ")[2];
			_assertEqual($Address[0][12],$actCity);
			_assertEqual($Address[1][12],$addedZipCode);
			_click(_link("Edit", _in(_heading3("Edit Billing Address"))));
			}
		if($i==2)
			{
			//should auto correct the state field
			var $actState=_extract(_getText(_div("details",_in(_div("/BILLING ADDRESS/")))),"/[,](.*)/",true).toString().trim().split(" ")[0];
			var $actAddress=_extract(_getText(_div("details",_in(_div("/BILLING ADDRESS/")))),"/(.*)[,]/",true).toString();
			_assertEqual($Address[2][12],$actState);
			_assertEqual($Address[2][11],$actAddress);
			//navigate back to shipping page from billing page
			_click(_link("Edit", _in(_heading3("Edit Billing Address"))));
			}
		if($i==3)
			{
			var $actzipCode=_extract(_getText(_div("details",_in(_div("/BILLING ADDRESS/")))),"/[,](.*)/",true).toString().trim().split(" ")[1];
			_assertEqual($Address[3][12],$actzipCode);
			//navigate back to shipping page from billing page
			_click(_link("Edit", _in(_heading3("Edit Billing Address"))));
			}
		if($i==4)
		{
			//verify the address validation dialog
			_assertVisible(_heading1($Address[4][13]));
			_assertVisible(_paragraph("/(.*)/",_in(_div("address-validation-dialog"))));
			_assertVisible(_submit("original-address-edit"));
			_assertVisible(_submit("ship-to-original-address"));
			_assertVisible(_div("address-validation-dialog"));
			_assertVisible(_heading3($Address[4][12]));
			_assertEqual($item[0][5],_style(_heading3($Address[4][12]),"color"));
			//verify the entered address
			_assertVisible(_paragraph("/(.*)/",_in(_div("original-address left-pane"))));
			_assertEqual($Address[4][11],_getText(_paragraph("/(.*)/",_in(_div("original-address left-pane")))));
			//click on edit address
			_click(_submit("original-address-edit"));
		}
	}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("158067/158070/158080", "Verify the Group1 behavior/Verify the error message that gets displayed/Verify the functionality of 'Close' button  in the Shipping accordion in the application when a guest user re-enters or updates the billing address after validation fails in the application");
$t.start();
try
{

//Navigate back to cart page
if (_isVisible(_link("mini-cart-link")))
	{
	_click(_link("mini-cart-link"));
	_click(_submit("dwfrm_cart_checkoutCart"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	}	

//Enter payment details
PaymentDetails($paymentData,0)

//modified in corrected fields & click on continue
_click(_submit("dwfrm_billing_save")); 
//verify the overlay
_assertVisible(_submit("original-address-edit"));
_assertVisible(_submit("ship-to-original-address"));
_assertVisible(_div("address-validation-dialog"));
//verify the error message
_assertEqual($item[0][4],_getText(_paragraph("/(.*)/",_in(_div("address-validation-dialog")))));
//click on close button 
_click(_button("Close"));
//should be in Billing page
_assertVisible(_fieldset("/"+$item[0][3]+"/"));
//enter valid data
_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1"),$Address[0][3]);
_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_postal"),$Address[0][8]);
_assertEqual($Address[0][3],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
_assertEqual($Address[0][8],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_postal")));	
//modified in corrected fields & click on continue
_click(_submit("dwfrm_billing_save")); 
//should navigate to order summary
_assert(_isVisible(_submit("Place Order")));	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("158084","Verify the Group1 behavior in the application when a guest user navigates back to checkout flow from any other page");
$t.start();
try
{
	//click on view cart button
	_click(_link("mini-cart-link"));
	//click on checkout
	_click(_submit("dwfrm_cart_checkoutCart"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
		//should navigate to billing page
	_assertVisible(_fieldset("/"+$item[0][3]+"/"));	
	PaymentDetails($paymentData,0);
	_click(_submit("dwfrm_billing_save")); 
	//should navigate to order summary
	_assert(_isVisible(_submit("Place Order")));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("158065","Verify the Correct This Address functionality of the original address in the modal window when a registered user enters an Invalid (Non exact match) Billing address in the application.");
$t.start();
try
{
//navigate back to billing page
_click(_link("Edit",_in(_heading3("Edit Billing Address"))));
//modify adress1
_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1"),$Address[1][12]);
//modified in corrected fields & click on continue
PaymentDetails($paymentData,0);
_click(_submit("dwfrm_billing_save"));
//verify the address validation dialog
_assertVisible(_heading1($Address[4][13]));
_assertVisible(_paragraph("/(.*)/",_in(_div("address-validation-dialog"))));
_assertVisible(_submit("original-address-edit"));
_assertVisible(_submit("ship-to-original-address"));
_assertVisible(_div("address-validation-dialog"));
//click on continue
_click(_submit("ship-to-original-address"));
//should navigate to order summary
_assert(_isVisible(_submit("Place Order")));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("158073","Verify the Edit link functionality of the original address in the modal window when a guest user enters an Invalid (No match) shipping address in the application.");
$t.start();
try
{
	//navigate back to billing page
	_click(_link("Edit",_in(_heading3("Edit Billing Address"))));
	//enter invalid billing address
	BillingAddress($Address,4);
	//enter payment details
	PaymentDetails($paymentData,0);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	//click on edit address
	_click(_submit("original-address-edit"));
	//verify the address prepopulation in billing page
	_assertEqual($Address[4][1],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
	_assertEqual($Address[4][2],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
	_assertEqual($Address[4][3],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
	//_assertEqual($Address[4][4],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address2")));
	_assertEqual($Address[4][5],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
	_assertEqual($Address[4][6],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
	_assertEqual($Address[4][7],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
	_assertEqual($Address[4][8],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_postal")));
	_assertEqual($Address[4][9],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_phone")));
	
	//Navigate back to cart page
	if (_isVisible(_link("mini-cart-link")))
		{
		_click(_link("mini-cart-link"));
		_click(_submit("dwfrm_cart_checkoutCart"));
		_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
		}	
	
	//enter invalid billing address
	BillingAddress($Address,4);
	//enter payment details
	PaymentDetails($paymentData,0);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	_assertVisible(_div("address-validation-dialog"));
	//click on edit address
	_click(_submit("original-address-edit"));
	//Entering valid address
	BillingAddress($validAddress,0);
	//enter payment details
	PaymentDetails($paymentData,0);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	//should navigate to order summary
	_assert(_isVisible(_submit("Place Order")));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("158076","Verify the functionality of  'Continue' button in modal window for a guest user in the application.");
$t.start();
try
{
	//navigate back to billing page
	_click(_link("Edit",_in(_heading3("Edit Billing Address"))));
	//enter invalid billing address
	BillingAddress($Address,4);
	//enter payment details
	PaymentDetails($paymentData,0);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	//click on continue
	_click(_submit("ship-to-original-address"));
	//should navigate to order summary
	_assert(_isVisible(_submit("Place Order")));	
	var $ActAddress=_getText(_div("details", _in(_div("mini-billing-address order-component-block"))))
	_assertEqual($Address[4][16],$ActAddress);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();
*/

cleanup();
_click(_link("Login"));
login();
//removing item from cart
ClearCartItems();

var $t=_testcase("158185","Verify the application behavior when user enters a non exact match in bailling page and selects paypal as payment mode.");
$t.start();
try
{
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping address
	shippingAddress($validAddress,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//enter invalid billing address
	BillingAddress($Address,4);
	_click(_radio("PayPal"));
	//click on continue
	_click(_submit("dwfrm_billing_save"));	
	_assertVisible(_heading3($Address[4][12]));
	_assertVisible(_paragraph("/(.*)/",_in(_div("address-validation-dialog"))));
	_assertVisible(_submit("original-address-edit"));
	_assertVisible(_submit("ship-to-original-address"));
	_assertVisible(_div("address-validation-dialog"));
	_click(_submit("ship-to-original-address"));
	if(isMobile())
		 {
		 	_setValue(_emailbox("login_email"), $paymentData[4][0]);
		 }
	else
		 {
		 _setValue(_textbox("login_email"), $paymentData[4][0]);
		 }
	_setValue(_password("login_password"), $paymentData[4][1]);
	_click(_submit("Log In"));
	_click(_submit("Continue"));
	_wait(10000,_isVisible(_submit("submit")));	
	//click on submit
	_click(_submit("Place Order"));
	_assertVisible(_div("order-confirmation-details"));	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

//removing item from cart
ClearCartItems();

var $t=_testcase("158186","Verify the application behvaior when valid address is entered in shipping and billing page and order is placed through bill me later option.");
$t.start();
try
{
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping address
	shippingAddress($validAddress,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//enter invalid billing address
	BillingAddress($validAddress,0);
	//Bill me later
	_click(_radio("BML"));
	_setSelected(_select("dwfrm_billing_paymentMethods_bml_year"), $billmelater[0][0]);
	_setSelected(_select("dwfrm_billing_paymentMethods_bml_month"),$billmelater[0][1]);
	_setSelected(_select("dwfrm_billing_paymentMethods_bml_day"),$billmelater[0][2]);
	 _setValue(_textbox("dwfrm_billing_paymentMethods_bml_ssn"),$billmelater[0][3]);
	_check(_checkbox("dwfrm_billing_paymentMethods_bml_termsandconditions"));
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	//should navigate to order summary
	_assert(_isVisible(_submit("Place Order")));
	_assertVisible(_link("Edit", _in(_heading3("Edit SHIPPING ADDRESS"))));
	_assertVisible(_link("Edit", _in(_heading3("Edit Billing Address"))));
	_assertVisible(_link("Edit", _in(_heading3("Edit PAYMENT METHOD"))));
	//click on submit
	_click(_submit("Place Order"));
	_assertVisible(_div("order-confirmation-details"));	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

//removing item from cart
ClearCartItems();

var $t=_testcase("158187/158188/158189","Verify the application behvaior behvaior when user selects Edit address button in address validation overlay /when user enters the non exact address in billing page and bill me later option is selected /when user selectsContinue button in address validation overlay");
$t.start();
try
{
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping address
	shippingAddress($validAddress,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//enter invalid billing address
	BillingAddress($Address,4);
	//Bill me later
	_click(_radio("BML"));
	_setSelected(_select("dwfrm_billing_paymentMethods_bml_year"), $billmelater[0][0]);
	_setSelected(_select("dwfrm_billing_paymentMethods_bml_month"),$billmelater[0][1]);
	_setSelected(_select("dwfrm_billing_paymentMethods_bml_day"),$billmelater[0][2]);
	 _setValue(_textbox("dwfrm_billing_paymentMethods_bml_ssn"),$billmelater[0][3]);
	_check(_checkbox("dwfrm_billing_paymentMethods_bml_termsandconditions"));
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	_assertVisible(_heading3($Address[4][12]));
	_assertVisible(_paragraph("/(.*)/",_in(_div("address-validation-dialog"))));
	_assertVisible(_submit("original-address-edit"));
	_assertVisible(_submit("ship-to-original-address"));
	_assertVisible(_div("address-validation-dialog"));
	//click on edit address
	_click(_submit("original-address-edit"));
	//verify the address prepopulation in billing page
	_assertEqual($Address[4][1],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
	_assertEqual($Address[4][2],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
	_assertEqual($Address[4][3],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
	//_assertEqual($Address[4][4],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address2")));
	_assertEqual($Address[4][5],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
	_assertEqual($Address[4][6],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
	_assertEqual($Address[4][7],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
	_assertEqual($Address[4][8],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_postal")));
	_assertEqual($Address[4][9],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_phone")));
	
	//Navigate back to cart page
	if (_isVisible(_link("mini-cart-link")))
		{
		_click(_link("mini-cart-link"));
		_click(_submit("dwfrm_cart_checkoutCart"));
		_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
		}	
	
	//enter invalid billing address
	BillingAddress($Address,4);
	//Bill me later
	_click(_radio("BML"));
	_setSelected(_select("dwfrm_billing_paymentMethods_bml_year"), $billmelater[0][0]);
	_setSelected(_select("dwfrm_billing_paymentMethods_bml_month"),$billmelater[0][1]);
	_setSelected(_select("dwfrm_billing_paymentMethods_bml_day"),$billmelater[0][2]);
	 _setValue(_textbox("dwfrm_billing_paymentMethods_bml_ssn"),$billmelater[0][3]);
	_check(_checkbox("dwfrm_billing_paymentMethods_bml_termsandconditions"));

	//click on continue
	_click(_submit("dwfrm_billing_save"));
	_click(_submit("ship-to-original-address"));
	//should navigate to order summary
	_assert(_isVisible(_submit("Place Order")));
	//click on submit
	_click(_submit("Place Order"));
	_assertVisible(_div("order-confirmation-details"));	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();



var $t=_testcase("158190","Verify the application behvaior when user click on Close icon in address validation overlay of billing page and bill me later option is selected");
$t.start();
try
{
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping address
	shippingAddress($validAddress,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//enter invalid billing address
	BillingAddress($Address,4);
	//Bill me later
	_click(_radio("BML"));
	_setSelected(_select("dwfrm_billing_paymentMethods_bml_year"), $billmelater[0][0]);
	_setSelected(_select("dwfrm_billing_paymentMethods_bml_month"),$billmelater[0][1]);
	_setSelected(_select("dwfrm_billing_paymentMethods_bml_day"),$billmelater[0][2]);
	 _setValue(_textbox("dwfrm_billing_paymentMethods_bml_ssn"),$billmelater[0][3]);
	_check(_checkbox("dwfrm_billing_paymentMethods_bml_termsandconditions"));
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	//click on close button 
	_click(_button("Close"))
	//should be in billing page
	_assertVisible(_fieldset("/"+$item[0][3]+"/"));	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

cleanup();