_include("../../util.GenericLibrary/GlobalFunctions.js");
_include("../../util.GenericLibrary/BM_Configuration.js");
_resource("Group1.xls");

var $Address=_readExcelFile("Group1.xls","Address");
var $item=_readExcelFile("Group1.xls","Item");
var $validAddress=_readExcelFile("Group1.xls","validAddress");
var $paymentData=_readExcelFile("Group1.xls","paymentData");



//navigate to URL
SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);


var $t=_testcase("155884/155888/155950","Verify the Shipping address validation by Group1 for a guest user in the application/an Invalid (Non exact match) shipping address/user navigates back to shipping accordion from any other page");
$t.start();
try
{
//Navigation till shipping page
navigateToShippingPage($item[0][0],$item[0][1]);	
for(var $i=0;$i<$Address.length;$i++)
	{
	//enter Shipping address
	shippingAddress($Address,$i);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//city field
	if($i==0 || $i==1)
		{
			//should auto correct the city field
			var $actCity=_extract(_getText(_div("details",_in(_div("/SHIPPING ADDRESS/")))),"/(.*)[,]/",true).toString().split(" ")[6];
			var $zipCode=_extract(_getText(_div("details",_in(_div("/SHIPPING ADDRESS/")))),"/[,](.*)/",true).toString().trim().split(" ")[1];
			var $addedZipCode=_extract($zipCode,"/[-](.*)/",true); 
			_assertEqual($Address[0][12],$actCity);
			_assertEqual($Address[1][12],$addedZipCode);
			//navigate back to shipping page from billing page
			_click(_link("Edit",_in(_heading3("Edit SHIPPING ADDRESS"))));
		}
	//state field
	if($i==2)
		{
			//should auto correct the state field
			var $actState=_extract(_getText(_div("details",_in(_div("/SHIPPING ADDRESS/")))),"/[,](.*)/",true).toString().trim().split(" ")[0];
			var $actAddress=_extract(_getText(_div("details",_in(_div("/SHIPPING ADDRESS/")))),"/(.*)[,]/",true).toString();
			_assertEqual($Address[2][12],$actState);
			_assertEqual($Address[2][11],$actAddress);
			//navigate back to shipping page from billing page
			_click(_link("Edit",_in(_heading3("Edit SHIPPING ADDRESS"))));
		}
	if($i==3)
		{
			var $actzipCode=_extract(_getText(_div("details",_in(_div("/SHIPPING ADDRESS/")))),"/[,](.*)/",true).toString().trim().split(" ")[1];
			_assertEqual($Address[3][12],$actzipCode);
			//navigate back to shipping page from billing page
			_click(_link("Edit",_in(_heading3("Edit SHIPPING ADDRESS"))));
		}
	//invalid
	if($i==4)
		{
			//verify the address validation dialog
			_assertVisible(_heading1($Address[4][13]));
			_assertVisible(_paragraph("/(.*)/",_in(_div("address-validation-dialog"))));
			_assertVisible(_submit("original-address-edit"));
			_assertVisible(_submit("ship-to-original-address"));
			_assertVisible(_div("address-validation-dialog"));
			_assertVisible(_heading3($Address[4][12]));
			_assertEqual($item[0][5],_style(_heading3($Address[4][12]),"color"));
			//verify the entered address
			_assertVisible(_paragraph("/(.*)/",_in(_div("original-address left-pane"))));
			_assertEqual($Address[4][11],_getText(_paragraph("/(.*)/",_in(_div("original-address left-pane")))));
			//click on edit address
			_click(_submit("original-address-edit"));
		}	
	}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("155897","Verify the Edit link functionality of the original address in the modal window when a guest user enters an Invalid (Non exact match) shipping address in the application");
$t.start();
try
{
	//click on edit address
	_click(_submit("original-address-edit"));
	//user should be in shipping page
	_assertVisible(_fieldset("/"+$item[0][2]+"/"));
	
	//Navigate back to ILP
	
	if (_isVisible(_link("mini-cart-link")))
		{
		_mouseOver(_link("mini-cart-link"));
		_click(_link("mini-cart-link-checkout"));
		_click(_submit("dwfrm_login_unregistered"));
		}
	
	
	//address should be editable
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"),$Address[1][12]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $Address[0][8]);
	_assertEqual($Address[1][12],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1")));
	_assertEqual($Address[0][8],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("155899/155913/155947","Verify the Group1 behavior/Verify the error message that gets displayed/Verify the functionality of 'Close' button  in the Shipping accordion in the application when a guest user re-enters or updates the shipping address after validation fails in the application");
$t.start();
try
{
	//modified in corrected fields & click on continue
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//verify the overlay
	_assertVisible(_submit("original-address-edit"));
	_assertVisible(_submit("ship-to-original-address"));
	_assertVisible(_div("address-validation-dialog"));
	//verify the error message
	_assertEqual($item[0][4],_getText(_paragraph("/(.*)/",_in(_div("address-validation-dialog")))));
	//click on close button 
	_click(_button("Close"));
	//should be in shipping page
	_assertVisible(_fieldset("/"+$item[0][2]+"/"));
	//enter valid data
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"),$Address[0][3]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $Address[0][8]);
	_assertEqual($Address[0][3],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1")));
	_assertEqual($Address[0][8],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")));	
	//modified in corrected fields & click on continue
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//should navigate to billing page
	_assertVisible(_fieldset("/"+$item[0][3]+"/"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("155954","Verify the Group1 behavior in the application when a guest user navigates back to checkout flow from any other page");
$t.start();
try
{
	//click on view cart button
	_click(_link("mini-cart-link"));
	//click on checkout
	_click(_submit("dwfrm_cart_checkoutCart"));
	//click on guest checkout
	_click(_submit("dwfrm_login_unregistered"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//should navigate to billing page
	_assertVisible(_fieldset("/"+$item[0][3]+"/"));	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("155903","Verify the functionality of Continue button in We did not recognize this address overlay");
$t.start();
try
{
	//navigate back to shipping page
	_click(_link("Edit",_in(_heading3("Edit SHIPPING ADDRESS"))));
	//modify adress1
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"),$Address[1][12]);
	//modified in corrected fields & click on continue
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//verify the address validation dialog
	_assertVisible(_heading1($Address[4][13]));
	_assertVisible(_paragraph("/(.*)/",_in(_div("address-validation-dialog"))));
	_assertVisible(_submit("original-address-edit"));
	_assertVisible(_submit("ship-to-original-address"));
	_assertVisible(_div("address-validation-dialog"));
	//click on continue
	_click(_submit("ship-to-original-address"));
	//should navigate to billing page
	_assertVisible(_fieldset("/"+$item[0][3]+"/"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


cleanup();
var $t=_testcase("155958","Verify the address displayed in BM after selecting the original address from Group1 address pop up");
$t.start();
try
{
//Navigation till shipping page
navigateToShippingPage($item[0][0],$item[0][1]);	
//enter Shipping address
shippingAddress($Address,4);
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
var $actShippingAddress=$Address[4][15];
//click on continue
_click(_submit("ship-to-original-address"));
if(_isVisible(_submit("ship-to-original-address")))
	{
	_click(_submit("ship-to-original-address"));
	}
//enter valid billing address
BillingAddress($validAddress,0);
PaymentDetails($paymentData,0);
//click on continue
_click(_submit("dwfrm_billing_save"));
//click on submit
_click(_submit("submit"));
//verify whether order got placed or not
_assertVisible(_heading1($item[0][6]));
var $orderNum=_getText(_span("value",_in(_div("order-information"))));
//navigate to BM
BM_Login();
_click(_link("Ordering"));
_click(_link("Orders"));
_setValue(_textbox("OrderSearchForm2_SimpleSearchTerm"),$orderNum);
_click(_submit("Find"));
_click(_link($orderNum));
_click(_link("/Shipment/"));
var $shippingAddress=_extract(_getText(_row("/Shipping Address:/")),"/[:](.*)/",true).toString().trim();
_assertEqual($actShippingAddress,$shippingAddress);
_click(_link("Log off."));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


SiteURLs();
var $t=_testcase("155925/155935","Verify the Edit link functionality of the original address in the modal window when a guest user enters an Invalid (No match) shipping address / User updates the addresswhen shipping address validation fails in the application.");
$t.start();
try
{
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//enter invalid Shipping address
	shippingAddress($Address,4);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//click on edit address
	_click(_submit("original-address-edit"));
	//verify the address prepopulation in billing page
	_assertEqual($Address[4][1],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName")));
	_assertEqual($Address[4][2],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName")));
	_assertEqual($Address[4][3],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1")));
	//_assertEqual($Address[4][4],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2")));
	_assertEqual($Address[4][5],_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_country")));
	_assertEqual($Address[4][6],_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
	_assertEqual($Address[4][7],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")));
	_assertEqual($Address[4][8],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")));
	_assertEqual($Address[4][9],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone")));	
	
	//Navigate back to ILP
	if (_isVisible(_link("mini-cart-link")))
		{
		_mouseOver(_link("mini-cart-link"));
		_click(_link("mini-cart-link-checkout"));
		_click(_submit("dwfrm_login_unregistered"));
		}
	
	
	//entering invalid address
	shippingAddress($Address,4);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	_assertVisible(_div("address-validation-dialog"));
	//click on edit address
	_click(_submit("original-address-edit"));
	//Entering valid address
	shippingAddress($validAddress,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//should navigate to billing page
	_assertVisible(_fieldset("/"+$item[0][3]+"/"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("155939","Verify the functionality of  'Continue' button in modal window for a guest user in the application.");
$t.start();
try
{
	//navigate back to shipping page
	_click(_link("Edit",_in(_heading3("Edit SHIPPING ADDRESS"))));
	//enter invalid Shipping address
	shippingAddress($Address,4);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//click on continue
	_click(_submit("ship-to-original-address"));
	//should navigate to billing page
	_assertVisible(_fieldset("/"+$item[0][3]+"/"));
	var $ActAddress=_extract(_getText(_div("details",_in(_div("/SHIPPING ADDRESS/")))),"/(.*) Method/",true).toString();
	_assertEqual($Address[4][16],$ActAddress);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("155959","Verify the address displayed in BM after editing the address in We did not recognize this address model pop up.");
$t.start();
try
{
	//navigate back to shipping page
	_click(_link("Edit",_in(_heading3("Edit SHIPPING ADDRESS"))));
	//enter invalid Shipping address
	shippingAddress($Address,4);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//click on edit address
	_click(_submit("original-address-edit"));
	//Entering valid address
	shippingAddress($validAddress,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//should navigate to billing page
	_assertVisible(_fieldset("/"+$item[0][3]+"/"));
	//enter valid billing address
	BillingAddress($validAddress,0);
	PaymentDetails($paymentData,0);
	//click on continue
	_click(_submit("dwfrm_billing_save"));
	//click on submit
	_click(_submit("submit"));
	//verify whether order got placed or not
	_assertVisible(_heading1($item[0][6]));
	var $orderNum=_getText(_span("value",_in(_div("order-information"))));
	//navigate to BM
	BM_Login();
	_click(_link("Ordering"));
	_click(_link("Orders"));
	_setValue(_textbox("OrderSearchForm2_SimpleSearchTerm"),$orderNum);
	_click(_submit("Find"));
	_click(_link($orderNum));
	_click(_link("/Shipment/"));
	var $shippingAddress=_extract(_getText(_row("/Shipping Address:/")),"/[:](.*)/",true).toString().trim();
	_assertEqual($Address[4][14],$shippingAddress);
	_click(_link("Log off."));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();