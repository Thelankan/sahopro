_include("../../util.GenericLibrary/BrowserSpecific.js");
_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Back_In_Stock.xls");

SiteURLs();
cleanup();
_setAccessorIgnoreCase(true); 

var $data=_readExcelFile("Back_In_Stock.xls","Data");
var $Inventory=_readExcelFile("Back_In_Stock.xls","Inventory");


function setInventory()
{
	//_click(_link("PREVAIL"));
	//Navigating to change the inventory in BM tool
	_click(_link("Products and Catalogs"));
	_click(_link("Inventory Lists"));
	_click(_link("inventory"));
	_click(_link("Records"));
	for(var $i=0; $i<$Inventory.length-4; $i++)
		{
		_setValue(_textbox("SearchTerm"), $Inventory[$i][1]);
		_click(_submit("Find"));
		_click(_button("Edit"));
		_setValue(_textbox("InventoryRecordForm_Allocation"),  $Inventory[$i][2]);
		
		if(!_getAttribute(_radio("NONE"),"checked"))
		{
		_click(_radio("NONE"));
		_setValue(_textbox("PreorderBackorderAllocation"),0);
		}
		
		_click(_submit("Apply"));
		if(_isVisible(_button("Yes")))
			{
			_click(_button("Yes"));
			}
		
		_click(_link("Attributes"));

		if (!_checkbox("true").true)
			{
			_check(_checkbox("true"));
			}
		
		_click(_submit("Apply"));
		
	if (_isVisible(_span("Reset Allocation")))
		{
		_click(_button("Yes"));
		}
		
		_click(_link("inventory - Records"));
		if(isMobile())
			{
			_wait(4000);
			}
		
		}
_click(_link("Log off."));
	
}




function PreBackOrder()
{
	//_click(_link("PREVAIL"));
	//Navigating to change the inventory in BM tool
	_click(_link("Products and Catalogs"));
	_click(_link("Inventory Lists"));
	_click(_link("inventory"));
	_click(_link("Records"));
	for(var $i=8; $i<$Inventory.length; $i++)
		{
		_setValue(_textbox("SearchTerm"), $Inventory[$i][1]);
		_click(_submit("Find"));
		_click(_button("Edit"));
		_setValue(_textbox("InventoryRecordForm_Allocation"),"0");
		_click(_radio( $Inventory[$i][2]));
		_setValue(_textbox("PreorderBackorderAllocation"),"10");
		
		_click(_submit("Apply"));
		
		if(_isVisible(_button("Yes")))
			{
			_click(_button("Yes"));
			}
		
		_click(_link("Attributes"));

		if (!_checkbox("true").true)
			{
			_check(_checkbox("true"));
			}
		
		_click(_submit("Apply"));
		
	if (_isVisible(_span("Reset Allocation")))
		{
		_click(_button("Yes"));
		}
		
		_click(_link("inventory - Records"));
		if(isMobile())
			{
			_wait(4000);
			}
		
		}
	
}
	
	
function ProductSetInventory($InstockProdIds,$OutOfStockProdIds)
{
	
//_click(_link("PREVAIL"));
_click(_link("Products and Catalogs"));
_click(_link("Products"));
var $j=0;
for(var $i=0; $i<2; $i++)
	{
	if($i==0)
		{
		$ProdIds=$InstockProdIds;
		}
	else
		{
		$ProdIds=$OutOfStockProdIds;
		}
	
			for(var $k=0; $k<$ProdIds.length; $k++)
			{
			
		_setValue(_textbox("inputfield_en perm_not_disabled"),$ProdIds[$k]);
		_click(_submit("Find"));
		_click(_link("table_detail_link"));
		_click(_link("Inventory", _in(_cell("Inventory"))));
		_click(_button("Edit"));
		_setValue(_textbox("InventoryRecordForm_Allocation"),$data[$j][4]);
	
		
			if(!_getAttribute(_radio("NONE"),"checked"))
			{
			_click(_radio("NONE"));
			_setValue(_textbox("PreorderBackorderAllocation"),0);
			}
		
		_click(_submit("Apply"));
			if(_isVisible(_button("Yes")))
				{
				_click(_button("Yes"));
				}
		
		_click(_link("Attributes"));
		
			if (!_checkbox("true").true)
				{
				_check(_checkbox("true"));
				}
		_click(_submit("Apply"));
		
		if (_isVisible(_span("Reset Allocation")))
		{
		_click(_button("Yes"));
		}
		
		
		_click(_link("Products"));
		
		if(isMobile())
			{
			_wait(4000);
			}
			}
			$j++;
	}
//Setting Instock inventory

//_click(_link("Log off."));
	
}


//Setting to Instock
search($Inventory[6][4])
var $SetTotProdsInstock=_collectAttributes("_div","/product-number/","sahiText",_in(_div("product-set-list")));
var $InstockProdIds=new Array();
for(var $i=0; $i<$SetTotProdsInstock.length; $i++)
	{
	var $id=_extract($SetTotProdsInstock[$i],"/Item# (.*)/",true).toString();
	$InstockProdIds.push($id);
	}
	
//Setting to out of stock
search($Inventory[7][4])
var $SetTotProdsOutOfStock=_collectAttributes("_div","/product-number/","sahiText",_in(_div("product-set-list")));
var $OutOfStockProdIds=new Array();
for(var $i=0; $i<$SetTotProdsOutOfStock.length; $i++)
	{
	var $id=_extract($SetTotProdsOutOfStock[$i],"/Item# (.*)/",true).toString();
	$OutOfStockProdIds.push($id);
	}

BM_Login();
//Setting inventory for product sets
ProductSetInventory($InstockProdIds,$OutOfStockProdIds);
//Pre and Back Order
PreBackOrder();
//Setting the product inventory to in stock,out of stock,pre order and post order 
setInventory();




var $t = _testcase("156021/156023/156027","Verify the Functionality of Email Subscription when product is back in stock & Verify the display of Email Subscription section when product is back in stock in PD page " +
		  "& Verify the display of Email Subscription section when product is In stock in PD page.");
$t.start();
try
{
	
	SiteURLs();
	_log($Inventory.length);
	//In Stock
	for(var $i=0; $i<$Inventory.length-7; $i++)
	{
	//Search the product which is out of stock [Variation product,Standard product,Product Bundle,Product Set]
	search($Inventory[$i][1]);
	var $availability=_extract(_getText(_div("availability")),"Availability: (.*)",true).toString();
	
		_assertEqual($Inventory[0][3],$availability);//
		//UI of back in stock field
		_assertNotVisible(_label($data[0][2]));
		_assertNotVisible(_emailbox("bisnemail email"));
	}
	
	
	
	//Out of Stock
	for(var $i=3; $i<$Inventory.length-4; $i++)
	{
	//Search the product which is out of stock [Variation product,Standard product,Product Bundle,Product Set]
	search($Inventory[$i][1]);
	if($i==4)
	{
		//UI of back in stock field
		_assertVisible(_label($data[0][2]));
		_assertVisible(_emailbox("bisnemail email"));
		_assertVisible(_submit("emailnotification"));
		//entering valid email in email field
		_setValue(_emailbox("bisnemail email"),$data[0][0]);
		_click(_submit("emailnotification"))
		//Confirmation Message 
		_assertEqual($data[0][1], _getText(_div("bisnemailnstatus")));
	
	}
	else
		{
		var $availability=_extract(_getText(_div("availability")),"Availability: (.*)",true).toString();
		_assertEqual($Inventory[3][3],$availability);
		//UI of back in stock field
		_assertVisible(_label($data[0][2]));
		_assertVisible(_emailbox("bisnemail email"));
		_assertVisible(_submit("emailnotification"));
		//entering valid email in email field
		_setValue(_emailbox("bisnemail email"),$data[0][0]);
		_click(_submit("emailnotification"))
		//Confirmation Message 
		_assertEqual($data[0][1], _getText(_div("bisnemailnstatus")));
		}
		
	}
	
	//Product Set In stock
	for(var $i=0; $i<$InstockProdIds.length; $i++)
	{
	//Search the product which is out of stock [Variation product,Standard product,Product Bundle,Product Set]
	search($Inventory[6][4]);
	
	var $availability=_extract(_getText(_div("availability["+$i+"]")),"Availability: (.*)",true).toString();
	
		_assertEqual($Inventory[0][3],$availability);
		//UI of back in stock field
		_assertNotVisible(_label($data[0][2]+"["+$i+"]"));
		_assertNotVisible(_emailbox("bisnemail email["+$i+"]"));
	}
	
	
	//Product Set Out of stock
	for(var $i=0; $i<$OutOfStockProdIds.length; $i++)
	{
	//Search the product which is out of stock [Variation product,Standard product,Product Bundle,Product Set]
	search($Inventory[7][4]);
	
	var $availability=_extract(_getText(_div("availability["+$i+"]")),"Availability: (.*)",true).toString();
	
		_assertEqual($Inventory[3][3],$availability);
		//UI of back in stock field
		
		_assertVisible(_label($data[0][2]+"["+$i+"]"));
		_assertVisible(_emailbox("bisnemail email["+$i+"]"));
		_assertVisible(_submit("emailnotification["+$i+"]"));
		//entering valid email in email field
		_setValue(_emailbox("bisnemail email["+$i+"]"),$data[0][0]);
		_click(_submit("emailnotification["+$i+"]"));
		//Confirmation Message 
		_assertEqual($data[0][1], _getText(_div("bisnemailnstatus["+$i+"]")));
	}

	//Pre Order
	search($Inventory[8][1]);
	var $availability=_extract(_getText(_div("availability")),"Availability: (.*)",true).toString();
	_assertEqual($Inventory[8][3],$availability);
	//UI of back in stock field
	_assertNotVisible(_label($data[0][2]));
	_assertNotVisible(_emailbox("bisnemail email"));


	//Back Order
	search($Inventory[9][1]);
	var $availability=_extract(_getText(_div("availability")),"Availability: (.*)",true).toString();
	_assertEqual($Inventory[9][3],$availability);
	//UI of back in stock field
	_assertNotVisible(_label($data[0][2]));
	_assertNotVisible(_emailbox("bisnemail email"));

}	
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();



BM_Login();
//ReSetting inventory for product sets
ResetProductSetInventory($InstockProdIds,$OutOfStockProdIds);
//Resetting Pre and Back Order inventory
ResetPreBackOrder();
//ReSetting the product inventory 
ReSetInventory();




function ReSetInventory()
{
	//_click(_link("PREVAIL"));
	//Navigating to change the inventory in BM tool
	_click(_link("Products and Catalogs"));
	_click(_link("Inventory Lists"));
	_click(_link("inventory"));
	_click(_link("Records"));
	for(var $i=0; $i<$Inventory.length-4; $i++)
		{
		_setValue(_textbox("SearchTerm"), $Inventory[$i][1]);
		_click(_submit("Find"));
		_click(_button("Edit"));
		_setValue(_textbox("InventoryRecordForm_Allocation"),$Inventory[0][2]);
		
		if(!_getAttribute(_radio("NONE"),"checked"))
		{
		_click(_radio("NONE"));
		_setValue(_textbox("PreorderBackorderAllocation"),0);
		}
		
		_click(_submit("Apply"));
		if(_isVisible(_button("Yes")))
			{
			_click(_button("Yes"));
			}
		
		_click(_link("inventory - Records"));
		if(isMobile())
			{
			_wait(4000);
			}
		
		}
_click(_link("Log off."));
	
}


function ResetPreBackOrder()
{
	//_click(_link("PREVAIL"));
	//Navigating to change the inventory in BM tool
	_click(_link("Products and Catalogs"));
	_click(_link("Inventory Lists"));
	_click(_link("inventory"));
	_click(_link("Records"));
	for(var $i=8; $i<$Inventory.length; $i++)
		{
		_setValue(_textbox("SearchTerm"), $Inventory[$i][1]);
		_click(_submit("Find"));
		_click(_button("Edit"));
		_setValue(_textbox("InventoryRecordForm_Allocation"),  $Inventory[0][2]);
		
		if(!_getAttribute(_radio("NONE"),"checked"))
		{
		_click(_radio("NONE"));
		_setValue(_textbox("PreorderBackorderAllocation"),0);
		}
		
		_click(_submit("Apply"));
		if(_isVisible(_button("Yes")))
			{
			_click(_button("Yes"));
			}
		
		_click(_link("inventory - Records"));
		if(isMobile())
			{
			_wait(4000);
			}
		}
	
}
	
	
function ResetProductSetInventory($InstockProdIds,$OutOfStockProdIds)
{
	
//_click(_link("PREVAIL"));
_click(_link("Products and Catalogs"));
_click(_link("Products"));
var $j=0;
for(var $i=0; $i<2; $i++)
	{
	if($i==0)
		{
		$ProdIds=$InstockProdIds;
		}
	else
		{
		$ProdIds=$OutOfStockProdIds;
		}
	
			for(var $k=0; $k<$ProdIds.length; $k++)
			{
			
		_setValue(_textbox("inputfield_en perm_not_disabled"),$ProdIds[$k]);
		_click(_submit("Find"));
		_click(_link("table_detail_link"));
		_click(_link("Inventory", _in(_cell("Inventory"))));
		_click(_button("Edit"));
		_setValue(_textbox("InventoryRecordForm_Allocation"),  $Inventory[0][2]);
		
		if(!_getAttribute(_radio("NONE"),"checked"))
		{
		_click(_radio("NONE"));
		_setValue(_textbox("PreorderBackorderAllocation"),0);
		}
		
		_click(_submit("Apply"));
		if(_isVisible(_button("Yes")))
			{
			_click(_button("Yes"));
			}
		
		_click(_link("Products"));
		if(isMobile())
			{
			_wait(4000);
			}
			}
			$j++;
	}

	
}

