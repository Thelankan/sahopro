_include("../../util.GenericLibrary/GlobalFunctions.js");

//set the authorize_net payment processor
BM_Login();
//navigate to prevail dash board
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));
//Click on PREVAIL Integrations link
_click(_link($userLog[2][2]));
//fetch checkout type
var $CheckoutType=_getSelectedText(_select("/(.*)/", _near(_row("/Checkout Template Type:/"))));

_setSelected(_select("/inputfield_en/",_rightOf(_row("/Credit Card Payment Service:/"))),$BMConfig[2][4]);
//click on apply
_click(_submit("update"));

//click on ordering
_click(_link("Ordering"));
_click(_link("Payment Methods"));

//gift certificate
var $gc=$Run[9][1];
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATE Gift Certificate/"))));
	}
else
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATEGift Certificate/"))));
	}
_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
_click(_div("/"+$gc+"/",_in(_div("x-combo-list-inner"))));
_click(_div("x-panel-header x-unselectable"));
_click(_button("Apply[1]"));
_wait(2000);
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATE Gift Certificate/"))),[2]).toString();
	}
else
	{
		var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATEGift Certificate/"))),[2]).toString();
	}
_log($giftCertificate+"......GiftCardPaymentorder");
_assertEqual($Run[9][1],$giftCertificate,"Data not configured properly");

if($giftCertificate=="Yes")
{
	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
	}
	else
	{
		_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
	}
	_click(_image("s.gif", _in(_row("Payment Processor:"))));
	_click(_div($BMConfig[0][7]));
	_click(_button("Apply[1]"));
}


//credit card
var $cc=$Run[9][0];
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/CREDIT_CARD Credit Card/"))));
	}
else
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/CREDIT_CARDCredit Card/"))));
	}
_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
_click(_div("/"+$cc+"/",_in(_div("x-combo-list-inner"))));
_click(_div("x-panel-header x-unselectable"));
_click(_button("Apply[1]"));
_wait(2000);

if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))),[2]).toString();
	}
else
	{
		var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))),[2]).toString();
	}
_log($CreditCard+"......CreditCardPaymentorder");
_assertEqual($Run[9][0],$CreditCard,"Data not configured properly");

if($CreditCard=="Yes")
{
	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
	}
	else
	{
		_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
	}
	_click(_image("s.gif", _in(_row("Payment Processor:"))));
	_click(_div($BMConfig[0][5]));
	_click(_button("Apply[1]"));
}

//log of from BM
_click(_link("Log off."));



if($CheckoutType==$BMConfig[1][1])
{
	_include("../../Checkout/Multi_Page_Checkout/EndToEndScenarios/CreditCard/EndToEndCC_Suite.js");
}
else if($CheckoutType==$BMConfig[0][1])
{
	_include("../../Checkout/Single_Page_Checkout/EndToEndScenarios/CreditCard/EndToEndCC_Suite.js");
}
else
{
	_log(false,"Select any one checkout type");
}