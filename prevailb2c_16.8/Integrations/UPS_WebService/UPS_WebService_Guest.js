_include("../../util.GenericLibrary/BrowserSpecific.js");
_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("UPS_Data.xls");

var $address=_readExcelFile("UPS_Data.xls","Address");
var $searchData=_readExcelFile("UPS_Data.xls","Search Data");
var $Generic=_readExcelFile("UPS_Data.xls","Generic");

BM_Login();
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));

//Click on PREVAIL Integrations link
_click(_link($userLog[2][2]));

//Shipping Address Verification Service
_setSelected(_select("/inputfield_en/",_rightOf(_row("Shipping Address Verification Service:"))),$Run[0][2]);
var $AddrVerification=_getSelectedText(_select("/(.*)/", _near(_row("Shipping Address Verification Service:"))));

_log($AddrVerification+"......Address verification");
_assertEqual($Run[0][2],$AddrVerification,"Data configured properly");

//click on apply
_click(_submit("update"));
_click(_link("Log off."));

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//Navigate to Shipping page
navigateToShippingPage($searchData[0][0],1);


var $t=_testcase("128092/128102/128179/128129/128154","Verify the Shipping address validation by UPS for a guest user in the application and Verify the original address format in the modal window when a guest user enters an Invalid (Non exact match) shipping address in the application and Verify the UPS behavior in the application when guest user enters an Invalid (No match) shipping address and Verify the warning message displayed on modal window when guest user enters an invalid (Non exact match or No match) address in the application");
$t.start();
try
{
	//Providing Valid Address
	shippingAddress($address,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));

	//Should Navigate to Billing Page
	_assertVisible(_fieldset("/SELECT OR ENTER BILLING ADDRESS/"));
	_assertVisible(_submit("dwfrm_billing_save"));
	
	ClearCartItems();
	//Navigate to Shipping page
	navigateToShippingPage($searchData[0][0],1);
	
	//Providing Invalid Address
	shippingAddress($address,1);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));

	_wait(4000,_isVisible(_div("address-validation-dialog")));
	
	_assertVisible(_div("hide ui-dialog-content ui-widget-content"));
	//Original address contains the address entered by the User Address 1 & Address 2 City, State, Country and Zip Code
	var $enteredAddress=$address[1][3]+" "+$address[1][4]+" "+$address[1][7]+" , "+$address[1][6]+" "+$address[1][5]+" "+$address[1][8];	
	_assertVisible(_paragraph($enteredAddress, _in(_div("original-address left-pane"))));
	//Matching Address
	_assertVisible(_div("suggested-addresses origin"));
	_assertContainsText($address[1][3], _div("suggested-addresses-1"));
	
	//UI Of Overlay
	//Verifying Heading "Verify your Shipping Address"
	_assertVisible(_heading1($Generic[0][1]));
	//Verifying Error message
	_assertEqual($Generic[0][0],_getText(_paragraph("/(.*)/",_in(_div("address-validation-dialog")))));
	//Original Address: Heading
	_assertVisible(_heading1($Generic[0][1]));
	//Edit Link
	_assertVisible(_submit("original-address-edit"));
	//Continue Button
	_assertVisible(_submit("Continue"));
	//Warning Message"Please note, you are responsible for the accuracy of your shipping address"
	_assertVisible(_paragraph("/(.*)/",_in(_div("addresses-section"))));		
	
	_assertVisible(_div("/hide ui-dialog-content ui-widget-content/"));
	var $enteredAddress=$address[1][3]+" "+$address[1][4]+" "+$address[1][7]+" , "+$address[1][6]+" "+$address[1][5]+" "+$address[1][8];
	_assertVisible(_paragraph($enteredAddress, _in(_div("/original-address left-pane/"))));
	//Clicking on Continue Button
	_click(_submit("Continue"));
	//Should Navigate to Billing Page
	_assertVisible(_fieldset("/SELECT OR ENTER BILLING ADDRESS/"));
	_assertVisible(_submit("dwfrm_billing_save"));
	ClearCartItems();
	//Navigate to Shipping page
	navigateToShippingPage($searchData[0][0],1);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("128098/128133/128158","Verify the error message that gets displayed in the Shipping accordion when a guest user enters an Invalid (Non exact match) shipping address in the application and Verify the functionality of 'Close' button in modal window for a guest user in the application");
$t.start();
try
{
	//Providing Invalid Address
	shippingAddress($address,1);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	_assertContainsText($Generic[0][0],_div("address-validation-dialog"));
	//Clicking on close
	_click(_button("Close"));
	//Shipping accordion should be displayed with the User entered address
	_assertNotVisible(_div("/ui-dialog ui-widget ui-widget-content ui-corner-all address-validation-dialog ui-draggable/"));
	_assertNotVisible(_div("/original-address left-pane/"));
	ClearCartItems();
	//Navigate to Shipping page
	navigateToShippingPage($searchData[0][0],1);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("128104/128143","Verify the Edit link functionality of the original address in the modal window when a guest user enters an Invalid (Non exact match) shipping address in the application");
$t.start();
try
{
	//Providing Invalid Address
	shippingAddress($address,1);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//click on Edit Address
	_click(_submit("/original-address-edit/"));
	//Application should display the Shipping accordion with original address pre populated in the input fields
	_assertEqual($address[1][1],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName")));
	_assertEqual($address[1][2],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName")));
	_assertEqual($address[1][3],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1")));
	_assertEqual($address[1][4],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2")));
	_assertEqual($address[1][5],_getValue(_select("dwfrm_singleshipping_shippingAddress_addressFields_country")));
	_assertEqual($address[1][6],_getValue(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
	_assertEqual($address[1][7],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")));
	_assertEqual($address[1][8],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")));
	_assertEqual($address[1][9],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone")));
	//Providing Valid Address
	shippingAddress($address,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//Should Navigate to Billing Page
	_assertVisible(_fieldset("/SELECT OR ENTER BILLING ADDRESS/"));
	_assertVisible(_submit("dwfrm_billing_save"));
	ClearCartItems();
	//Navigate to Shipping page
	navigateToShippingPage($searchData[0][0],1);
	
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();
var $t=_testcase("128094/128096","Verify UPS behavior in the application when a guest user enters a Valid (Exact match)/Invalid (Non exact match) shipping address");
$t.start();
try
{
	//Providing Valid Address
	shippingAddress($address,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	
	//Should Navigate to Billing Page
	_assertVisible(_fieldset("/SELECT OR ENTER BILLING ADDRESS/"));
	_assertVisible(_submit("dwfrm_billing_save"));
	ClearCartItems();
	//Navigate to Shipping page
	navigateToShippingPage($searchData[0][0],1);
	//Providing Invalid Address
	shippingAddress($address,1);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	_click(_submit("Continue"));
	//Should Navigate to Billing Page
	_assertVisible(_fieldset("/SELECT OR ENTER BILLING ADDRESS/"));
	_assertVisible(_submit("dwfrm_billing_save"));
	ClearCartItems();
	//Navigate to Shipping page
	navigateToShippingPage($searchData[0][0],1);
		
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();
var $t=_testcase("128106/128146","Verify the UPS behavior in the application when a guest user re-enters or updates the shipping address after validation fail fails in the application");
$t.start();
try
{
	//Providing Invalid Address
	shippingAddress($address,1);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	_wait(4000,_isVisible(_div("address-validation-dialog")));
	
	_assertVisible(_div("hide ui-dialog-content ui-widget-content"));
	//Original address contains the address entered by the User Address 1 & Address 2 City, State, Country and Zip Code
	var $enteredAddress=$address[1][3]+" "+$address[1][4]+" "+$address[1][7]+" , "+$address[1][6]+" "+$address[1][5]+" "+$address[1][8];	
	_assertVisible(_paragraph($enteredAddress, _in(_div("original-address left-pane"))));
	//Matching Address
	_assertVisible(_div("suggested-addresses origin"));
	_assertContainsText($address[1][3], _div("suggested-addresses-1"));
	
	//click on Edit Address
	_click(_submit("original-address-edit"));
	//Providing Valid Address
	shippingAddress($address,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//Should Navigate to Billing Page
	_assertVisible(_fieldset("/SELECT OR ENTER BILLING ADDRESS/"));
	_assertVisible(_submit("dwfrm_billing_save"));
	ClearCartItems();
	//Navigate to Shipping page
	navigateToShippingPage($searchData[0][0],1);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("128110","Verify the number of suggested addresses displayed in modal window when a guest user enters an Invalid (Non exact match) shipping address in shipping accordion in the application");
$t.start();
try
{
	//Providing Valid Address
	shippingAddress($address,2);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	_assertVisible(_heading1($Generic[0][1]));
	var $enteredAddress=$address[2][3]+" "+$address[2][4]+" "+$address[2][7]+" , "+$address[2][6]+" "+$address[2][5]+" "+$address[2][8];
	//Able to update the address
	_assertVisible(_paragraph($enteredAddress, _in(_div("original-address left-pane"))));
	var $TotSuggestedAddr=_count("_div","/suggested-address-text/", _in(_div("suggested-addresses origin")));
	//. Maximum of 2 Suggested address should be displayed in the 'Verify Your Shipping Address' modal window
	_assertEqual(2,$TotSuggestedAddr);
	//Clicking on close
	_click(_button("Close"));
	_wait(4000,!_isVisible(_button("Close")));
	ClearCartItems();
//Navigate to Shipping page
navigateToShippingPage($searchData[0][0],1);
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();




var $t=_testcase("128113/128125/128150","Verify the functionality related to selection of suggested address from the modal window when a guest user enters an Invalid (Non exact match) shipping address in shipping accordion in the application/Verify the functionality of Select and Continue button in modal window for guest user in the application");
$t.start();
try
{
	//Providing Not Exact matching Address
	shippingAddress($address,1);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	var $SuggestedAddr=_getText(_paragraph("/(.*)/", _in(_div("suggested-addresses origin"))));
	//Click on Select and Continue Button
	_click(_submit("ship-to-address-selected-1", _in(_div("suggested-addresses origin"))));
	//Should Navigate to Billing Page
	_assertVisible(_fieldset("/SELECT OR ENTER BILLING ADDRESS/"));
	_assertVisible(_submit("dwfrm_billing_save"));
	  
	if(_isVisible(_heading2("Shipping")))
		{
		_click(_heading2("Shipping"));  //SPC
		}
	else
		{
		_click(_link("Shipping")); //MPC
		}
	var $addr1=_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"));
	var $addr2=_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"));
	var $city=_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"));
	var $state=_getValue(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"));
	var $country=_getValue(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"));
	var $zipCode=_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"));
	//Address should match with Suggested address which selected
	//_assertEqual($SuggestedAddr,$addr1+" "+$addr2+" "+$city+" , "+$state+" "+$country+" "+$zipCode);
	_assertEqual($SuggestedAddr,$addr1+" "+$city+" , "+$state+" "+$country+" "+$zipCode);  
	ClearCartItems();
	//Navigate to Shipping page
	navigateToShippingPage($searchData[0][0],1);
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("128117","Verify the Edit link functionality of the suggested address in the modal window when a guest user enters an Invalid (Non exact match) shipping address in the application");
$t.start();
try
{
	//Providing Not Exact matching Address
	shippingAddress($address,1);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	var $SuggestedAddr=_getText(_paragraph("/(.*)/", _in(_div("suggested-addresses origin"))));
	//Click on Edit button
	_click(_submit("suggested-address-edit-1", _in(_div("suggested-addresses origin"))));
	var $addr1=_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"));
	var $addr2=_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"));
	var $city=_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"));
	var $state=_getValue(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"));
	var $country=_getValue(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"));
	var $zipCode=_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"));
	//Address should match with Suggested address which selected
	//_assertEqual($SuggestedAddr,$addr1+" "+$addr2+" "+$city+" , "+$state+" "+$country+" "+$zipCode);
	_assertEqual($SuggestedAddr,$addr1+" "+$city+" , "+$state+" "+$country+" "+$zipCode); 
	ClearCartItems();
	//Navigate to Shipping page
	navigateToShippingPage($searchData[0][0],1);
		
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("No ID","No Description");
$t.start();
try{
	//Providing Not Exact matching Address
	shippingAddress($address,1);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	var $BeforeSuggestedAddr=_getText(_div("suggested-address-tex-1", _in(_div("suggested-addresses origin"))));
	//Click on Edit button
	_click(_submit("suggested-address-edit-1", _in(_div("suggested-addresses origin"))));
	//Updating Address 1 & Zip code
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"),$Generic[0][2]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $Generic[0][3]);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//should not display suggest overlay
	_assertNotVisible(_div("address-validation-dialog"));
	//Should Navigate to Billing Page
	_assertVisible(_fieldset("/SELECT OR ENTER BILLING ADDRESS/"));
	_assertVisible(_submit("dwfrm_billing_save"));
	ClearCartItems();
	//Navigate to Shipping page
	navigateToShippingPage($searchData[0][0],1);
}
catch($e)
{
_logExceptionAsFailure($e);	
}
$t.end();


var $t=_testcase("128163","Verify the UPS behavior in the application when a guest user navigatea back to shipping accordion from any other page");
$t.start();
try
{
	//Providing Not Exact matching Address
	shippingAddress($address,1);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//Click onSelect and Continue button
	_click(_submit("ship-to-address-selected-1"));
	
	if(_isVisible(_heading2("Shipping")))
		{
		_click(_heading2("Shipping"));  //SCP
		}
	else
		{
		_click(_link("Shipping")); //MPC
		}
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//should not display suggest overlay
	_assertNotVisible(_div("address-validation-dialog"));
	//Should Navigate to Billing Page
	_assertVisible(_fieldset("/SELECT OR ENTER BILLING ADDRESS/"));
	_assertVisible(_submit("dwfrm_billing_save"));
	ClearCartItems();
	//Navigate to Shipping page
	navigateToShippingPage($searchData[0][0],1);
	
}
catch($e)
{
_logExceptionAsFailure($e);	
}
$t.end();



var $t=_testcase("128168","Verify the UPS behavior in the application when a guest user navigates back to checkout flow from any other page");
$t.start();
try
{
	//Providing Not Exact matching Address
	shippingAddress($address,2);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//Click Continue button
	_click(_submit("Continue"));
	//Click on view cart button from  Mini cart overlay
	_click(_link("mini-cart-link"));
	//Update qty of item
	_setValue(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity"),4);
	_click(_submit("update-cart"));
	//click on Checkout
	_click(_submit("dwfrm_cart_checkoutCart"));
	//Guest User
	_click(_submit("dwfrm_login_unregistered")); 
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//UPS should validate the Shipping address, even if the user has not made any changes in shipping address
	_assertVisible(_div("address-validation-dialog"));
	//Click Continue button
	_click(_submit("Continue"));
	//Should Navigate to Billing Page
	_assertVisible(_fieldset("/SELECT OR ENTER BILLING ADDRESS/"));
	_assertVisible(_submit("dwfrm_billing_save"));

	

}
catch($e)
{
_logExceptionAsFailure($e);	
}
$t.end();


ClearCartItems();
//Navigate to Shipping page
navigateToShippingPage($searchData[0][0],1);
var $t=_testcase("154890/154893","Verify the address displayed in BM after selecting the original address from UPS address pop up and Verify the address displayed in BM when a exact match is provided as shipping address");
$t.start();
try
{
	//Providing shipping Valid Address
	shippingAddress($address,2);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//Click Continue button
	_click(_submit("Continue"));
		BillingAddress($address,3);
	PaymentDetails($address,5);
_click(_submit("dwfrm_billing_save"));
	_click(_submit("submit"));
	if(_isVisible(_image("CloseIcon.png")))
	{
	_click(_image("CloseIcon.png"));
	}
	var $orderNumber=_getText(_span("value", _in(_heading1("order-number"))));
	_log($orderNumber);
	BM_Login();
	_click(_link("Ordering"));
	_click(_link("Orders"));
	_setValue(_textbox("OrderSearchForm2_SimpleSearchTerm"),$orderNumber);
	_click(_submit("Find"));
	_click(_link($orderNumber));
	//_click(_link("Shipment "+$orderNumber));
	_click(_link(/Shipment/));    //Dilip
	var $originalAddress=$address[2][1]+" "+$address[2][2]+" "+$address[2][3]+" "+$address[2][4]+" "+$address[2][7]+" "+$address[2][6]+" "+$address[2][8]+" "+$address[2][5];
	//Comparing original address
	_assertVisible(_cell($originalAddress));
	_click(_link("Log off."));
	SiteURLs();
	ClearCartItems();
	//Navigate to Shipping page
	navigateToShippingPage($searchData[0][0],1);
	
}
catch($e)
{
	_logExceptionAsFailure($e);	
}
$t.end();


var $t=_testcase("154891","Verify the address displayed in BM after selecting the Suggested Address 1 from UPS address pop up");
$t.start();
try
{
	//Providing shipping Valid Address
	shippingAddress($address,2);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	var $SuggestedAddr=_getText(_div("suggested-address-tex-1")).replace(" , "," ");
	//Click Select & Continue button
	_click(_submit("Select and Continue"));
	BillingAddress($address,3);
	PaymentDetails($address,5);
	_click(_submit("dwfrm_billing_save"));
	_click(_submit("submit"));
	if(_isVisible(_image("CloseIcon.png")))
	{
	_click(_image("CloseIcon.png"));
	}
	var $orderNumber=_getText(_span("value", _in(_heading1("order-number"))));
	_log($orderNumber);
	BM_Login();
	_click(_link("Ordering"));
	_click(_link("Orders"));
	_setValue(_textbox("OrderSearchForm2_SimpleSearchTerm"),$orderNumber);
	_click(_submit("Find"));
	_click(_link($orderNumber));
	_click(_link("/Shipment/")); 
	_click(_link("Shipping Address:"));
	var $addr1=_getText(_textbox("inputfield_en",_rightOf(_cell("/Address 1/"))));
	var $addr2=_getText(_textbox("inputfield_en",_rightOf(_cell("/Address 2/"))));
	var $city=_getText(_textbox("inputfield_en",_rightOf(_cell("/City/"))));
	var $zip=_getText(_textbox("inputfield_en",_rightOf(_cell("/Postal Code/"))));
	var $state=_getText(_textbox("MetacdNGYiaagJXYEaaadimhByDmeb"));
	var $country=_extract(_getSelectedText(_select("inputfield_en",_rightOf(_cell("/Country/")))),"/(.*)[(]/",true).toString();
	_log($state+ " "+$country);
	//var $actAddr=$addr1+" "+$addr2+" "+$city+" "+$zip+" "+$state+" "+$country;
	var $actAddr=$addr1+" "+$city+" "+$state+" "+$country+" "+$zip;
	//Comparing with suggested address
	_assertEqual($SuggestedAddr,$actAddr);
	
	_click(_link("Log off."));
	SiteURLs();
	ClearCartItems();
	//Navigate to Shipping page
	navigateToShippingPage($searchData[0][0],1);

}
catch($e)
{
_logExceptionAsFailure($e);	
}
$t.end();
	

	var $t=_testcase("154892","Verify the address displayed in BM after selecting the Suggested Address 2 from UPS address pop up");
	$t.start();
	try
	{
		//Providing shipping Valid Address
		shippingAddress($address,2);
		_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
		var $SuggestedAddr=_getText(_div("suggested-address-tex-2")).replace(" , "," ");
		//Click Select & Continue button
		_click(_submit("ship-to-address-selected-2"));
		BillingAddress($address,3);
		PaymentDetails($address,5);
		_click(_submit("dwfrm_billing_save"));
		_click(_submit("submit"));
		if(_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
		var $orderNumber=_getText(_span("value", _in(_heading1("order-number"))));
		
		BM_Login();
		_click(_link("Ordering"));
		_click(_link("Orders"));
		_setValue(_textbox("OrderSearchForm2_SimpleSearchTerm"),$orderNumber);
		_click(_submit("Find"));
		_click(_link($orderNumber));
		//_click(_link("Shipment "+$orderNumber));
		_click(_link(/Shipment/));    //Dilip
		_click(_link("Shipping Address:"));
		var $addr1=_getText(_textbox("inputfield_en",_rightOf(_cell("/Address 1/"))));
		var $city=_getText(_textbox("inputfield_en",_rightOf(_cell("/City/"))));
		var $zip=_getText(_textbox("inputfield_en",_rightOf(_cell("/Postal Code/"))));
		var $state=_getText(_textbox("MetacdNGYiaagJXYEaaadimhByDmeb"));
		var $country=_extract(_getSelectedText(_select("inputfield_en",_rightOf(_cell("/Country/")))),"/(.*)[(]/",true).toString();
		var $actAddr=$addr1+" "+$city+" "+$state+" "+$country+" "+$zip;
		//Comparing with suggested address
		_assertEqual($SuggestedAddr,$actAddr);
		
		_click(_link("Log off."));
		SiteURLs();
	}
	catch($e)
	{
	_logExceptionAsFailure($e);	
	}
	$t.end();
		
cleanup();