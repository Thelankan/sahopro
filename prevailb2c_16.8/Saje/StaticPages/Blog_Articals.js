_include("../util.GenericLibrary/GlobalFunctions.js");
_resource("All_Store.xls");

var $Store_Data=_readExcelFile("All_Store.xls","Store");
//var $Cart_Data=_readExcelFile("All_Store.xls","Store");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();

var $t = _testcase("279642"," Verify Video URL in Saje Blog Page.");
$t.start();
try
{
	//click on the blog 
	_click(_link($Store_Data[0][1]));
	//click on the view all
	_click(_button("button-text"));
	//verify the navvigation
	_assertVisible(_div("All Blogs"));
	//verify the viedo
	_assertVisible(_link("http://development-shop-saje.demandware.net/on/demandware.store/Sites-SajeUS-Site/default/Page-ShowBlogArticle?cid=blogArticle1&viewall=true"));

	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("279643/ 279644/ 279645:"," Verify the Blog Article Image and Body Section3.");
$t.start();
try
{
	_assertVisible(_div("article-container  "));
	_assertVisible(_image("/(.*)/", _in(_link("thumb-link"))));
	_assertEqual($Store_Data[2][3], _getText(_link("content-title headingone")));
	_assertEqual($Store_Data[2][4], _getText(_link("readmore ptwotext")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("279646/279647 /279641 ","Verify Featured Products Heading in Saje Blog Page");
$t.start();
try
{
	_click(_link($Store_Data[2][3]));

	_assertEqual($Store_Data[2][4], _getText(_div("product")));
	_assertEqual($Store_Data[2][5], _getText(_div("contributor-title")));
	_assertEqual($Store_Data[2][7], _getAttribute(_image("vedio.png"),"src"));
	_assertEqual($Store_Data[1][4], _getText(_div("art-head")));
	_assertVisible(_div("/(.*)/",_under(_div("/"+$Store_Data[2][4]+"/"))));
	_assertVisible(_div("contribute-desc"));
	_assertVisible(_div("contribute-img", _in(_div("contribute"))));

}

catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("279649:"," Verify Email share, Facebook share and Twitter share in Saje Blog Page.");
$t.start();
try
{
	_assertEqual($Store_Data[3][4], _getText(_div("share-story")));
	_assertVisible(_link($Store_Data[3][5],_in(_div("share-socially"))));
	_assertVisible(_link("FACEBOOK",_in(_div("share-socially"))));
	_assertVisible(_link("TWITTER",_in(_div("share-socially"))));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("279692"," Verify the navigation related to the 404 Error page page in the application both as an anonymous and a registered user.");
$t.start();
try
{
    _navigateTo($Store_Data[2][8]);
    _assertVisible(_div("/_error/"));
		
  	_assertVisible(_heading1($Store_Data[3][1]));
  	_assertVisible(_paragraph($Store_Data[3][2]));
  	_assertVisible(_label($Store_Data[3][3]));
  	_assertVisible(_div("error-page-search"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
//279661 
var $t = _testcase("279661 ","Verify the navigation related to the Events page in the application both as an anonymous and a registered user.");
$t.start();
try
{
	//click on the event link in the footer
	_click(_link($Store_Data[3][6]));
	//verify the navigation
	_assertEqual($Store_Data[3][7], _getText(_heading1("head")));

	_assertVisible(_heading1($Store_Data[3][8]));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("","");
$t.start();
try
{


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//
//var $t = _testcase("","");
//$t.start();
//try
//{
//
//
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();