_include("../util.GenericLibrary/GlobalFunctions.js");
_resource("StaticPages.xls");

var $staticdata=_readExcelFile("StaticPages.xls","StaticpagesData");
var $ConactUs_Validations=_readExcelFile("StaticPages.xls","contactUsValidations");



var $t = _testcase("261347","Verify the navigation related to 'Contact Us' link  present at  global footer in application both as an  Anonymous and Registered user. '");
$t.start();
try
{
	
//click on contact us link
_assertVisible(_link($staticdata[0][0], _in(_div("/Need Help/"))));
_click(_link($staticdata[0][0], _in(_div("/Need Help/"))));
//contact us page
_assertVisible(_heading1($staticdata[0][1]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("261349/261350/261353/261354/261354/261355/261356/261357","Verify the validation related to 'First name' field   on contact us page in application both as an Anonymous and Registered user./Verify the validation related to 'Last name' field   ion contact us page in application both as an Anonymous and Registered user." +
		"Verify the validation related to 'email' field   on contact us/ page in application both as an Anonymous and Registered user.");
$t.start();
try
{

	//click on contact us link
	_assertVisible(_link($staticdata[0][0], _in(_div("/Need Help/"))));
	
	var $length=$ConactUs_Validations.length;
	
	for(var $i=0;$i<$length;$i++)
	{
		_log($i);
		_setValue(_textbox("dwfrm_contactus_firstname"),$ConactUs_Validations[$i][1]);
		_setValue(_textbox("dwfrm_contactus_lastname"),$ConactUs_Validations[$i][2]);
		_setValue(_textbox("dwfrm_contactus_email"),$ConactUs_Validations[$i][3]);
		_setValue(_textbox("dwfrm_contactus_phone"),$ConactUs_Validations[$i][4]);
		_setValue(_textbox("dwfrm_contactus_ordernumber"),$ConactUs_Validations[$i][5]);
		_setSelected(_select("dwfrm_contactus_myquestion"),$ConactUs_Validations[$i][6]);
		_setValue(_textarea("dwfrm_contactus_comment"),$ConactUs_Validations[$i][7]);
		_click(_submit("dwfrm_contactus_send"));
		
		if ($i==0)
			{
			_assertEqual($ConactUs_Validations[0][8],_style(_textbox("dwfrm_contactus_firstname"),"background-color"));
			_assertEqual($ConactUs_Validations[0][8],_style(_textbox("dwfrm_contactus_lastname"),"background-color"));
			_assertEqual($ConactUs_Validations[0][8],_style(_textbox("dwfrm_contactus_email"),"background-color"));
			_assertEqual($ConactUs_Validations[0][8],_style(_textbox("dwfrm_contactus_phone"),"background-color"));
			_assertEqual($ConactUs_Validations[0][8],_style(_textbox("dwfrm_contactus_ordernumber"),"background-color"));
			_assertEqual($ConactUs_Validations[0][8],_style(_textarea("dwfrm_contactus_comment"),"background-color"));
			
			_assertEqual($ConactUs_Validations[0][10],_getText(_span("error",_near(_textbox("dwfrm_contactus_firstname")))));
			_assertEqual($ConactUs_Validations[1][10],_getText(_span("error",_near(_textbox("dwfrm_contactus_lastname")))));
			_assertEqual($ConactUs_Validations[2][10],_getText(_span("error",_near(_textbox("dwfrm_contactus_email")))));
			_assertEqual($ConactUs_Validations[3][10],_getText(_span("error",_near(_textbox("dwfrm_contactus_phone")))));
			_assertEqual($ConactUs_Validations[4][10],_getText(_span("error",_near(_textbox("dwfrm_contactus_ordernumber")))));
			_assertEqual($ConactUs_Validations[0][11],_getText(_span("error",_near(_textarea("dwfrm_contactus_comment")))));
			}
			 else if($i==1)
	 		 {
		 		_assertEqual($ConactUs_Validations[1][8],_getText(_textbox("dwfrm_contactus_firstname")).length);
		 		_assertEqual($ConactUs_Validations[1][8],_getText(_textbox("dwfrm_contactus_lastname")).length);
		 		_assertEqual($ConactUs_Validations[1][8],_getText(_textbox("dwfrm_contactus_email")).length);
		 		_assertEqual($ConactUs_Validations[1][8],_getText(_textbox("dwfrm_contactus_phone")).length);
		 		_assertEqual($ConactUs_Validations[1][8],_getText(_textbox("dwfrm_contactus_ordernumber")).length);
		 		_assertEqual($ConactUs_Validations[1][8],_getText(_textarea("dwfrm_contactus_comment")).length);
	 		 }
			 else if($i==2)
				 {
				 _assertVisible(_div($ConactUs_Validations[3][8]));
				 
				 _assertEqual($ConactUs_Validations[3][8], _getText(_span("dwfrm_contactus_email-error")));
				 _assertEqual($ConactUs_Validations[0][9], _getText(_span("dwfrm_contactus_phone-error")));
				 _assertEqual($ConactUs_Validations[2][8], _getText(_span("dwfrm_contactus_ordernumber-error")));
				 
				 _assertEqual($ConactUs_Validations[0][8],_style(_textbox("dwfrm_contactus_email"),"background-color"));
				 _assertEqual($ConactUs_Validations[1][9], _style(_span("dwfrm_contactus_email-error"), "color"));
				 _assertEqual($ConactUs_Validations[0][8],_style(_textbox("dwfrm_contactus_phone"),"background-color"));
				 _assertEqual($ConactUs_Validations[1][9], _style(_span("dwfrm_contactus_phone-error"), "color"));
				 _assertEqual($ConactUs_Validations[0][8],_style(_textbox("dwfrm_contactus_ordernumber"),"background-color"));
				 _assertEqual($ConactUs_Validations[1][9], _style(_span("dwfrm_contactus_ordernumber-error"), "color"));
				 }
			 else if($i==3)
				 {
				 _assertEqual($ConactUs_Validations[3][8], _getText(_span("dwfrm_contactus_email-error")));
				 _assertEqual($ConactUs_Validations[0][8],_style(_textbox("dwfrm_contactus_email"),"background-color"));
				 _assertEqual($ConactUs_Validations[1][9], _style(_span("dwfrm_contactus_email-error"), "color"));
				 } 
				 else
					 {
					 _assertVisible(_paragraph($ConactUs_Validations[4][8]));
					 }
	}	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("261344","Verify the functionality related to the question links in FAQ's page in application as an Anonymous and Registered user.");
$t.start();
try
{

//click on Faq's link	
_assertVisible(_link($staticdata[1][0], _in(_div("/Need Help/"))));
//faq page
_assertEqual($staticdata[1][1], _getText(_heading1("faq-top")));
//click on any question
_click(_link("/(.*)/", _in(_div("FAQs"))));
_assertVisible(_heading2("Answers", _in(_div("FAQs"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase("","");
$t.start();
try
{


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/
