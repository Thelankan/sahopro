_include("../util.GenericLibrary/GlobalFunctions.js");
_resource("Cart.xls");

var $Coupon_Data=_readExcelFile("Cart.xls","Coupon_Validation");
var $Cart_Data=_readExcelFile("Cart.xls","Cart_Data");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();

var $t = _testcase("279541/279542"," Verify UI of special Category Landing Page planned for the Holiday category and used only when this category is active around the holidays.");
$t.start();
try
{
	//click on the holiday link in the header section 
	_click(_link("Holiday"));
	_assertVisible(_div("category-hero"));
	_assertVisible(_div("category-tiles"));

	
	_assertVisible(_div($Store_Data[0][4], _in(_div("promo-div"))));
	_assertVisible(_div("promo-content-text", _in(_div("promo-div"))));
	_assertVisible(_link("/button-box-promo /", _in(_div("promo-div"))));

	//verify the "Those you Love" section 
	_assertEqual($Store_Data[0][5], _getText(_div("/product-top-heading headingone/")));
	_assertVisible(_div("holiday-carousal thoseyoulove slick-slide"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase(" 279672/279685 /279683 ","Verify the navigation related to the MEDIA page in the application both as an anonymous and a registered user.");
$t.start();
try
{
	//click on the Media section in the footer
	_click(_link("Media"));
	// verfiy the navigation 
	_assertEqual($Store_Data[0][6], _getText(_heading3("sone-text")));
	_assertVisible(_div("press-media-room"));
	_assertVisible(_div("contact-inquary"));
	_assertVisible(_div("meet-the-team"));
	
	
	//Click on any one of the Executive present in the Meet The Team section
	var $Executive_Name=_getText(_link("person-name none-text"));
	_click(_link("person-name none-text"));
	_assertEqual($Store_Data[1][2], _getText(_div("heading-information sone-text")));
	_assertVisible(_div("exec-bio-info"));
	_assertEqual($Store_Data[0][7], _getText(_div("person-name headingone")));

	//Click on BACK link in the Executive present in the Meet The Team section.
	_click(_link("ptwotext"));
	_assertVisible(_heading3($Store_Data[0][6]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("279650"," Verify the navigation related to the OUR STORY page in the application both as an anonymous and a registered user.");
$t.start();
try
{
	//Click the OUR STORY link in the footer.
	_click(_link($Store_Data[1][9], _in(_list("footer-menu"))));
	// User should be navigated to the OUR STORY page
	_assertEqual("the saje story", _getText(_div("story-head")));
	_assertEqual($Store_Data[4][1], _getText(_div("feel-better")));
	_assertVisible(_heading2("saje today"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("279561"," Verify 'New Store Content' and 'Saje Careers Content' in Store Search Result.");
$t.start();
try
{
	//Navigate for Store Locator Page.
	_click(_span("store-icon spriteimg"));
	//System should redirect to Store Locator Page.
	_assertVisible(_heading1($Store_Data[0][3]));

	//3.Verify 'New Store Content' and 'Saje Careers Content'.
	_assertVisible(_div("store-locator-top-content"));

	//3a.Verify 'New Store Content' is configured.
	_assertVisible(_div("store-locator-col2"));

	//3b.Verify 'Saje Careers Content' is configured.
	_assertVisible(_div($Store_Data[4][2]));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("279562","Verify 'City/store name' links in New Store Content.");
$t.start();
try
{
	//Verify 'City/store name' links in New Store Content.
	_assertEqual($Store_Data[1][1], _getText(_div("store-locator-name")));
	_click(_link($Store_Data[1][1], _in(_div("store-locator-row"))));
	_assertContainsText($Store_Data[1][2], _div("store-address1"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase(" 279563:"," Verify View Saje Careers CTA in Saje Careers Content.");
$t.start();
try
{
	//Navigate for Store Locator Page.
	_click(_span("store-icon spriteimg"));
	//Verify View Saje Careers CTA in Saje Careers Content.
	_click(_link($Store_Data[4][2]));
	_assertVisible(_heading1("Careers"));
	_assertVisible(_div("links-career"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("279572/279575/279576"," [Desktop] Verify the UI of Store Details Page.");
$t.start();
try
{
	//Navigate for Store Locator Page.
	_click(_span("store-icon spriteimg"));
	//click on the store 
	_click(_link($Store_Data[1][1], _in(_div("store-locator-row"))));
	//verfy the UI of the page
	_assertVisible(_div("store-details-name"));
	//working hours
	_assertVisible(_div("store-hours"));
	//address
	_assertVisible(_div("store-address1"));
	//direction on map
	_assertVisible(_div("Get Directions"));
	//Events ij store
	_assertVisible(_link("Store Events"));
	//Maps
	_assertVisible(_div("map_canvas_storedetails"));
	//search drop down
	_assertVisible(_select("dwfrm_storelocator_searchby"));
	//select value from dropdown 
	_setSelected(_select("dwfrm_storelocator_searchby"), "Store Name | Mall Name");
	//search bar
	_assertVisible(_textbox("dwfrm_storelocator_searchkey"));
	//store Strore
	_assertVisible(_div("store-staus"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("279577"," Verify 'Store Events' in Store Details Page.");
$t.start();
try
{

	_click(_div("Store Events"));
	//verify  the naviagtion 
	_assertVisible(_div("event-image desktop-only"));
	_assertVisible(_heading1($Store_Data[3][7]));
	_assertVisible(_heading1($Store_Data[3][8]));
	_assertVisible(_div("events-list"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("279581"," Verify 'Search Field' in Store Details Page.");
$t.start();
try
{
	//Navigate for Store Locator Page.
	_click(_span("store-icon spriteimg"));
	//click on the store 
	_click(_link($Store_Data[1][1], _in(_div("store-locator-row"))));
	_setSelected(_select("dwfrm_storelocator_searchby"), "Store Name | Mall Name");
	_setValue(_textbox("dwfrm_storelocator_searchkey"), $Store_Data[1][2]);
	_assertEqual($Store_Data[1][2], _getValue(_textbox("dwfrm_storelocator_searchkey")));
	_assertVisible(_heading1("Find a Store"));
	_assertVisible(_div("map_canvas_searchby"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("","");
$t.start();
try
{


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//var $t = _testcase("","");
//$t.start();
//try
//{
//
//
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();