_include("../util.GenericLibrary/GlobalFunctions.js");
_resource("Cart.xls");

var $Coupon_Data=_readExcelFile("Cart.xls","Coupon_Validation");
var $Cart_Data=_readExcelFile("Cart.xls","Cart_Data");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();




var $t = _testcase("279583"," Verify 'Saje Stores heading' in All Stores List.");
$t.start();
try
{
	//2. Click on Store Locator Page.
	_click(_span("store-icon spriteimg", _in(_div("header-icon"))));
	//2.Navigate for Store Locator Page..
	_assertVisible(_heading1($Store_Data[0][3]));

	//3. From listed store details, click on particular store details.
	_click(_link($Store_Data[0][2]));
	_wait(2000);
	_click(_link("/(.*)/", _in(_div("stores-address2"))))

	_assertVisible(_div("all-stores-address"));
	_assertVisible(_div("all-stores-address"));

	//3. Should navigate to particular store detail page.
	_assertVisible(_div("store-locator-details"));
	_assertVisible(_div("store-details-name"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("279584:"," Verify 'Country column headings' in All Stores List.");
$t.start();
try
{
	//2. Click on Store Locator Page.
	_click(_span("store-icon spriteimg", _in(_div("header-icon"))));
	//2.Navigate for Store Locator Page..
	_assertVisible(_heading1($Store_Data[0][3]));

	//3. From listed store details, click on particular store details.
	_click(_link($Store_Data[0][2]));
	//verify the country
	_assertEqual($Store_Data[2][2], _getText(_div("all-store-col2")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("279585:"," Verify 'State/Province link list' in All Stores List section.");
$t.start();
try
{
	//click on the country
	_click(_link("Alberta", _in(_div("all-stores-state"))));
	//verify thye navigation 
	_assertContainsText("Alberta", _div("stores-address1"));	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("279586:"," Verify 'State/Province store list section' in All Stores List section.");
$t.start();
try
{
	


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("279587:"," Verify 'Store listing' in All Stores List section.");
$t.start();
try
{
	_click(_link("Hours", _in(_div("stores-address2"))));
	_assertVisible(_div("store-hours"));
	_click(_span("store-icon spriteimg"));
	_click(_link($Store_Data[0][2]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("279588/ 279589  ","Verify 'Store listing' in All Stores List section.");
$t.start();
try
{
	var $State=_collectAttributes("_div","/(.*)/","sahiText",_in(_div("all-stores-state")));
	for(var $i=0;$i<$State.length;$i++)
		{
		//click on the states
		_click(_link($State[$i]));
		//verify the display of the State/Province
		_assertContainsText($State[$i], _div("stores-address1["+$i+"]"));
		}
	//verfiy the store is opening
	_assertVisible(_image("we_are_opening.jpg"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("279590/279591"," Verify 'Email Signup label' in footer section of SAJE STORES Page.");
$t.start();
try
{
	//	. From listed store details go to 'SAJE STORES' Page.
	_assertVisible(_textbox("dwfrm_emailsubscribe_email"));
	_assertVisible(_div("store-email-subscription"));
	//	4. Verify 'Email Signup label'.
	_assertVisible(_label($Store_Data[0][9]));
	_assertEqual("Sign Up", _getText(_submit("/button-box/")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("279633/279634"," Verify 'Blog Home Header' in BLOG Page.");
$t.start();
try
{
	//Click on 'Blog Home Header' Section.
	_click(_link($Store_Data[0][1], _in(_div("footer-follow-links footer-levels"))));
	//verify the naviagtion
	_assertEqual($Store_Data[1][0], _getText(_div("blog-head")));
	_assertVisible(_div("blogslot"));
	_assertVisible(_div("image-blog"));
	_assertVisible(_div("img-blog"));
	_assertVisible(_div("leftdesc"));
	_assertVisible(_div("blog-image"));
	_assertEqual($Store_Data[2][0], _getText(_button("button-text")));
	_assertVisible(_div("up-arrow"));
	//functinallity of arrow button
	_click(_button("button-text"));
	_assertVisible(_div("article-container  "));
	_assertVisible(_image("books.png"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("279635/279637/279639","check the read more ");
$t.start();
try
{
    //  click on the read more 
	_click(_link($Store_Data[0][8], _in(_div("blog-loop    "))));
	_assertVisible(_div("art-description"));
	_assertVisible(_div("articles-blogs"));
	
	//functinality of the pin to start
	_mouseOver(_image("Image Title"));
	_assertVisible(_span("/button_pin/"));

	//click on the upward arrow
	_click(_link($Store_Data[0][1]));
	_click(_div("up-arrow"));
	//click on the back button 
	_click(_link($Store_Data[2][1]));
	_assertEqual($Store_Data[1][0], _getText(_div("blog-head")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("279640","Verify Headline, Sub-Headline, Body Section 1 Present in Saje Blog Page2.");
$t.start();
try
{
	//verify the heading of the Blog page
	_assertVisible(_div($Store_Data[1][4]));
	_assertVisible(_div($Store_Data[1][5]));
	_assertVisible(_div($Store_Data[1][6]));
	_assertVisible(_div($Store_Data[1][7]));
	_assertVisible(_div($Store_Data[1][8]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("","");
$t.start();
try
{
    //  click on the read more 
	_click(_link($Store_Data[0][8], _in(_div("blog-loop    "))));
	_assertVisible(_div("art-description"));
	_assertVisible(_div("articles-blogs"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("","");
$t.start();
try
{


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
//
//var $t = _testcase("","");
//$t.start();
//try
//{
//
//
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();