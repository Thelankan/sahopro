_include("../util.GenericLibrary/GlobalFunctions.js");
_resource("StoreLocator.xls");

var $storelocatordata=_readExcelFile("StoreLocator.xls","StorelocatorData");


var $t = _testcase("279544/279545","Verify UI of 'Store Locator' page./Verify functionality of 'Search Field' in Store Locator page.");
$t.start();
try
{

//navigate to store locator
_click(_link("Locate Stores"));
//store locator heading
_assertVisible(_heading1("Find a Store"));
_assertVisible(_select("dwfrm_storelocator_searchby"));
_assertVisible(_textbox("dwfrm_storelocator_searchkey"));
_assertVisible(_submit("dwfrm_storelocator_searchbyoption"));
_assertVisible(_link("All Locations"));

//Drop down values
_assertEqual($storelocatordata[1][0],_getText(_select("dwfrm_storelocator_searchby")));

//click on all locations link
_assertVisible(_div("store-locator-top-content"));
_assertVisible(_div("store-locator-col1"));
_click(_link("All Locations"));
_assertVisible(_div("store-locator-col2"));
_assertVisible(_div("store-locator-bottom-button"));
_assertVisible(_link($storelocatordata[2][0]));
_assertVisible(_div($storelocatordata[3][0]));

//click on all locations links
_click(_link($storelocatordata[1][0]));

//all locations page UI
_assertVisible(_div("Saje Stores"));
_assertVisible(_link("Directions"));
_assertVisible(_link("Hours"));
_assertVisible(_div("/all-store-col/"));
_assertVisible(_div("all-stores-image"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("279546","Verify functionality of 'Search By drop-down menu' in Store Locator page.");
$t.start();
try
{
	
//navigate to store locator
_click(_link("Locate Stores"));
var $dropdown=_getText(_select("dwfrm_storelocator_searchby"));

for (var $i=0;i<$dropdown.length;$i++)
{
	_setSelected(_select("dwfrm_storelocator_searchby"),$dropdown[$i]);
	_assertEqual($dropdown[$i],_getSelectedText(_select("dwfrm_storelocator_searchby")));	
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("279547/279548","Verify functionality of 'Search button' in Store Locator page.");
$t.start();
try
{

//Drop down Values
_setSelected(_select("dwfrm_storelocator_searchby"),$storelocatordata[2][2]);
//enter value in search field
_setValue(_textbox("dwfrm_storelocator_searchkey"),$storelocatordata[2][1]);
//click on search icon
_click(_submit("search-button"));
//verify the search results
_assertVisible(_link("All Locations"));
_assertVisible(_tableHeader("Name"));
_assertVisible(_tableHeader("Address"));
_assertVisible(_tableHeader("distance"));
_assertVisible(_tableHeader("Phone"));
_assertEqual($storelocatordata[2][1],_getText(_link("/store-name/")));

var $storedetails=_count("_link","store-click store-name",_in(_table("store-location-results")));

for (var $i=0;$i<$storedetails;$i++)
{

	//click on all locations links
	_click(_link("store-click store-name["+$i+"]"));
	//store details list should be displayed
	_assertVisible(_div("store-details-name"));
	//maps section
	_assertVisible(_div("map_canvas_storedetails"));
	_assertVisible(_link("googlemap"));
	_assertVisible(_link("Store Events"));
	_assertVisible(_div("store-hours"));
	_assertVisible(_div("store-address1"));
	//Drop down Values
	_setSelected(_select("dwfrm_storelocator_searchby"),$storelocatordata[2][2]);
	//enter value in search field
	_setValue(_textbox("dwfrm_storelocator_searchkey"),$storelocatordata[2][1]);
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("","");
$t.start();
try
{


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase("","");
$t.start();
try
{


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/
