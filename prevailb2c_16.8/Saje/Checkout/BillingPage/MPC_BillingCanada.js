_include("../../util.GenericLibrary/GlobalFunctions.js");
_include("../../util.GenericLibrary/BM_Configuration.js");

_resource("EndToEndScenarios/Paypal/BillingPage.xls");
var $item = _readExcelFile("BillingPage/BillingPage.xls","Item");
//for deleting user
deleteUser();

SiteURLs();
cleanup();
_setAccessorIgnoreCase(true); 


var $t = _testcase("249060","Verify the validation of Province field on billing page in Application as a Anonymous user for Canada site");
$t.start();
try
{
	_click(_span("drop-icon spriteimg"));
	_click(_link("CANADA"));
	_assertVisible(_div("ui-dialog ui-widget ui-widget-content ui-corner-all ui-front home_page_popup ui-draggable"));

	_click(_link("buttonctaone"));
	_assertEqual("CANADA", _getText(_div("current-country")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("249061","Verify the validation of Province field on billing page in Application as a Anonymous user for Canada site.");
$t.start();
try
{
	cleanup();
	search("640188016372");
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	//click on the min cart icon
	_click(_span("minicart-icon spriteimg"));
	//click on the checkout
	_click(_link("mini-cart-link-checkout", _in(_div("mini-cart-content"))));
	//enter shipping address
	shippingAddress($shippingAddress,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	_assertVisible(_span("Province"));

	_assertVisible(_div("Please select a Country"));
	_assertEqual("rgb(250, 237, 237)",_style(_div("form-caption error-message"),"background-color"));

	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
login();
var $t = _testcase("249062","Verify the validation of Province field on billing page in Application as a Anonymous user for Canada site.");
$t.start();
try
{
	cleanup();
	search("640188016372");
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	//click on the min cart icon
	_click(_span("minicart-icon spriteimg"));
	//click on the checkout
	_click(_link("mini-cart-link-checkout", _in(_div("mini-cart-content"))));
	//enter shipping address
	shippingAddress($Billing,11);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	_assertVisible(_span("Province"));

	_assertVisible(_div("Please select a Country"));
	_assertEqual("rgb(250, 237, 237)",_style(_div("form-caption error-message"),"background-color"));

	

}
catch($e)
{
	_logExceptionAsFailure($e);
}
var $t = _testcase("249034/249060/249064","Verify the validation of 'Address 2' field on billing page in Application as a Anonymous user/Verify the validation of 'Address 1' field on billing page in Application as a Anonymous user");
$t.start();
try
{
	//A1A 1A1
	deleteAddress();
	//add prod to cart
	search("640188016372");
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	//click on the min cart icon
	_click(_span("minicart-icon spriteimg"));
	//click on the checkout
	_click(_link("mini-cart-link-checkout", _in(_div("mini-cart-content"))));
	//enter shipping address
	shippingAddress($shippingAddress,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	for(var $i=0;$i<5;$i++)
		{
			BillingAddress($sheet,$i);
			_click(_submit("dwfrm_billing_save"));
			if($i==0)
				{
				//fname
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"))));
				_assertEqual("rgb(250, 237, 237)", _style(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"),"background-color"));
				//lname
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"))));
				_assertEqual("rgb(250, 237, 237)", _style(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"),"background-color"));
				//addresss
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_billing_billingAddress_addressFields_address1"))));
				_assertEqual("rgb(250, 237, 237)", _style(_textbox("dwfrm_billing_billingAddress_addressFields_address1"),"background-color"));
				//city 
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_billing_billingAddress_addressFields_city"))));
				_assertEqual("rgb(250, 237, 237)", _style(_textbox("dwfrm_billing_billingAddress_addressFields_city"),"background-color"));
				//state
				_assertVisible(_span("This field is required.",_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
				//zipcode
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_billing_billingAddress_addressFields_postal"))));
				_assertEqual("rgb(250, 237, 237)", _style(_textbox("dwfrm_billing_billingAddress_addressFields_postal"),"background-color"));
				//phone
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_billing_billingAddress_addressFields_phone"))));
				_assertEqual("rgb(250, 237, 237)", _style(_textbox("dwfrm_billing_billingAddress_addressFields_phone"),"background-color"));
				//email
				_assertVisible(_span("This field is required.",_near(_textbox("dwfrm_billing_billingAddress_email_emailAddress"))));
				_assertEqual("rgb(250, 237, 237)", _style(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),"background-color"));
				}
			if($i==1)
				{
				//Fname & Last Name
				_assertEqual("50", _getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")).length);
				_assertEqual("50", _getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")).length);
				//email
				_assertVisible(_span("Please enter a valid email address."));
				_assertEqual("rgb(250, 237, 237)", _style(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),"background-color"));

				//zipcode   A1A 1A1
				_assertVisible(_span("This field is required."));
				_assertEqual("rgb(250, 237, 237)", _style(_textbox("dwfrm_billing_billingAddress_addressFields_postal"),"background-color"));
				//country
				_assertContainsText(" United StatesCanada", _div("form-row  required[4]"));
				}
			if($i==2)
				{
				//address validation 
				_assertEqual("50", _getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")).length);
				_assertEqual("50", _getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2")).length);
				//email
				_assertVisible(_span("Please enter a valid email address."));
				_assertEqual("rgb(250, 237, 237)", _style(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),"background-color"));

				//phone
				_assertVisible(_span("Please specify a valid phone number."));
				_assertEqual("rgb(250, 237, 237)", _style(_textbox("dwfrm_billing_billingAddress_addressFields_phone"),"background-color"));
				
				}
			if($i==3)
				{
				//city
				_assertEqual("50",_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")).length);
				
				//email
				_assertVisible(_span("Please enter a valid email address."));
				_assertEqual("rgb(250, 237, 237)", _style(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),"background-color"));
				}
			if($i==4)
				{
				
				_assertVisible(_div("step-3 active"));
				_click(_div("checkout-placeorder"));
				_assertVisible(_div("place-order-totals"));
				//click on the edit details link
				_click(_link("section-header-note", _in(_heading3("section-header pone-cartpage"))));
				//verify the navigation
				_assertVisible(_table("cart-table"));
				_assertVisible(_div("custom-cart-page"));
				_assertEqual("Your cart", _getText(_div("cart-header-text")));

				}
			
				
			
		}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("249030/249026/249027/249032/249040/249042","Verify the UI of 'SELECT CREDIT CARD TYPE Section on billing page in Application as a Registered user");
$t.start();
try
{
    navigateToCart("Bottoms","1");
    //navigate to checkout page
   // _click(_link("/mini-cart-link-checkout-link/"));
    _click(_submit("dwfrm_cart_checkoutCart"));
                  
    _wait(5000);
    
    _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
    shippingAddress($Address_data,0);
    _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
    adddressProceed();
    ///.'ORDER SUMMARY' as protected text and edit link should be displayed as ORDER SUMMARY 
    _assertVisible(_cell("Subtotal"))
    _assertVisible(_cell("Shipping"));
    _assertVisible(_cell("Sales Tax"));
    _assertVisible(_cell("Estimated Total"));
    
    //SELECT CREDIT CARD TYPE Section with following fields :
    _assertVisible(_span("Name on Card"));
    _assertVisible(_span("Type"));
    _assertVisible(_span("Number"));
    _assertVisible(_span("Expiration Date"));
    _assertVisible(_span("Security Code"));
    _assertVisible(_link("What is this?"));
    _assertVisible(_submit("Continue to Place Order"));
    _assertVisible(_span("Save this card"));

    //SHIPPING ADDRESS' as protected text and edit link should be displayed as SHIPPING 
    _assertVisible(_link("Edit", _in(_div("mini-shipment order-component-block  first "))));
    _assertVisible(_div("address", _in(_div("mini-shipment order-component-block  first "))));
    
    // 'STEP 1: Shipping Addresses STEP 2: Shipping Methods STEP 3: Billing STEP 4: Place Order' 
    _assertEqual("Step 1: Shipping / Step 2: Billing / Step 3: Place Order", _getText(_div("breadcrumb bonetext checkout-progress-indicator")));

    for(var $i=0;$i<$PaymentValidation.length;$i++)
    	{
    	 PaymentDetails($PaymentValidation,$i);
    	 _click(_submit("dwfrm_billing_save"));
    	 if($i==0)
    		 {
    		   _assertEqual(true, _submit("dwfrm_billing_save").disabled);
    		   _assertVisible(_span("This field is required.", _near(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"))));
    		   _assertVisible(_span("This field is required.", _near(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"))));
    		   _assertVisible(_span("This field is required.", _near(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"))));

    		 }
    	 if($i==1)
    		 {
    		 _assertVisible(_div("Invalid Credit Card Number"));
    		 _assertEqual("rgb(250, 237, 237)",_style(_div("form-caption error-message"),"background-color"));
    		 }
    	 if($i==2)
    		 {
    		 _assertVisible(_div("Invalid Credit Card Number"));
    		 _assertEqual("rgb(250, 237, 237)",_style(_div("form-caption error-message"),"background-color"));
    		 _assertVisible(_div("Invalid Security Code"));
    		 _assertEqual("rgb(250, 237, 237)",_style(_div("form-caption error-message", _near(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"))),"background-color"))
    		 }
    	 if($i==3)
    		 {
    		 _assertVisible(_div("This Credit Card is expired"));
    		 _assertEqual("rgb(250, 237, 237)",_style(_div("form-caption error-message"),"background-color"))
    		 }
    	 if($i==4)
    		 {
    		 _assertVisible(_div("Invalid Security Code"));
    		 _assertEqual("rgb(250, 237, 237)",_style(_div("form-caption error-message", _near(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"))),"background-color"))
    		 }
    	 if($i==5)
    		 {
    		 _assertVisible(_heading1($payment_data[1][3]));
             _assertEqual("Step 3: Place Order", _getText(_div("step-3 active")));
    		 }
    	
    	}
    

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


