_include("../../util.GenericLibrary/GlobalFunctions.js");
//_include("../../util.GenericLibrary/BM_Configuration.js");


_resource("MPCheckout.xls");
var $credit_card= _readExcelFile("MPCheckout.xls","credit_card");
var $CreateCreditCard= _readExcelFile("MPCheckout.xls","CreateCreditCard");
var $Billing= _readExcelFile("MPCheckout.xls","Billing");
var $PaymentValidation= _readExcelFile("MPCheckout.xls","PaymentValidation");
var $Checkout_Data= _readExcelFile("MPCheckout.xls","Checkout_Data");


//for deleting user
//deleteUser();

SiteURLs();
cleanup();
_setAccessorIgnoreCase(true); 


logout();

var $t = _testcase("249059/249065/249066/249067/249076/249077","Verify the validation of 'Address 2' field on billing page in Application as a Anonymous user/Verify the validation of 'Address 1' field on billing page in Application as a Anonymous user");
$t.start();
try
{
	//deleteAddress();
	//add prod to cart
	search($Checkout_Data[0][0]);
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	//click on the min cart icon
	_click(_span("minicart-icon spriteimg"));
	//click on the checkout
	_click(_link("mini-cart-link-checkout"));
	_wait(3000);
	_click(_submit("dwfrm_login_unregistered"));
	//enter shipping address
	shippingAddress($shippingAddress,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	for(var $i=0;$i<5;$i++)
		{
		
			
			if($i==0)
				{
				BillingAddress($Billing,$i);
				_click(_submit("dwfrm_billing_save"));
				//fname
				_assertVisible(_span($Billing[0][13],_near(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"))));
				_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"),"background-color"));
				//lname
				_assertVisible(_span($Billing[0][14],_near(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"))));
				_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"),"background-color"));
				//addresss
				_assertVisible(_span($Billing[0][15],_near(_textbox("dwfrm_billing_billingAddress_addressFields_address1"))));
				_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_addressFields_address1"),"background-color"));
				//city 
				
				_assertVisible(_span($Billing[1][13],_near(_textbox("dwfrm_billing_billingAddress_addressFields_city"))));
				_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_addressFields_city"),"background-color"));
				//country
				//_assertVisible(_span($Billing[1][14]));
				//_assertEqual($Billing[3][12], _style(_select("dwfrm_billing_billingAddress_addressFields_country"),"background-color"));
				_assertEqual("rgb(56, 60, 62)", _style(_select("dwfrm_billing_billingAddress_addressFields_country"), "color"))
				//state
				_assertVisible(_span($Billing[1][15],_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
				//zipcode
				_assertVisible(_span($Billing[2][13],_near(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone"))));
				_assertEqual($Billing[3][12], _style(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone"),"background-color"));
				//phone
				_assertVisible(_span($Billing[2][14],_near(_textbox("dwfrm_billing_billingAddress_addressFields_phone"))));
				_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_addressFields_phone"),"background-color"));
				//email
				_assertVisible(_span($Billing[2][15],_near(_textbox("dwfrm_billing_billingAddress_email_emailAddress"))));
				_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),"background-color"));
				}
			if($i==1)
				{
				BillingAddress($Billing,$i);
				_click(_submit("dwfrm_billing_save"));
				//Fname & Last Name
				_assertEqual($Billing[0][12], _getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")).length);
				_assertEqual($Billing[0][12], _getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")).length);
				_assertEqual($Billing[0][12], _getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")).length);
				_assertEqual($Billing[1][12], _getText(_textbox("dwfrm_billing_billingAddress_addressFields_address2")).length);
				_assertEqual($Billing[0][12],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_city")).length);
				_assertEqual($Billing[2][12],_getText(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone")).length);
				_assertEqual($Billing[4][12],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_phone")).length);
				_assertEqual($Billing[1][12],_getText(_textbox("dwfrm_billing_billingAddress_email_emailAddress")).length);
				//email
				_assertVisible(_span($Billing[3][13]));
				_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),"background-color"));

				//zipcode
				_assertVisible(_span($Billing[3][14]));
				_assertEqual($Billing[3][12], _style(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone"),"background-color"));
				//country
				//_assertContainsText(" United StatesCanada", _div("form-row  required[4]"));
				}
			if($i==2)
				{
				BillingAddress($Billing,$i);
				_click(_submit("dwfrm_billing_save"));
				//address validation 
				//email
				_assertVisible(_span($Billing[3][13]));
				_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),"background-color"));

				//phone
				_assertVisible(_span($Billing[3][14]));
				_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_addressFields_phone"),"background-color"));
				
				}
			if($i==3)
				{
				BillingAddress($Billing,$i);
				_click(_submit("dwfrm_billing_save"));
				//Zip
				_assertVisible(_span($Billing[3][14]));
				//phone
				_assertVisible(_span($Billing[3][14]));
				_assertVisible(_span($Billing[3][14], _near(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone"))));
				//email
				_assertVisible(_span($Billing[3][13]));
				_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),"background-color"));
				}
			if($i==4)
				{
				BillingAddress($Billing,$i);
				 PaymentDetails($credit_card,0);
				_click(_submit("dwfrm_billing_save"));
                 _assertVisible(_div("/step-3 active/"))
				_click(_div("checkout-placeorder"));
				_assertVisible(_div("place-order-totals"));
				//click on the edit details link
				_click(_span("checkout-bag-desktop", _in(_div("breadcrumb bonetext checkout-progress-indicator"))))
				//verify the navigation
				_assertVisible(_table("cart-table"));
				_assertVisible(_div("custom-cart-page"));
				_assertEqual($Checkout_Data[0][1], _getText(_div("cart-header-text")));

				}
		}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase(" 249041/248904","Verify the navigation to Checkout Billing page in Application as a Anonymous user");
$t.start();
try
{
	cleanup();
	//add prod to cart
	search($Checkout_Data[0][0]);
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	//click on the min cart icon
	_click(_span("minicart-icon spriteimg"));
	//click on the Take me to checkout link
	_click(_link("mini-cart-link-checkout", _in(_div("mini-cart-content"))));
	//verify the naviagtion 
	_assertVisible(_div("login-box-content returning-customers clearfix"));
	_assertVisible(_div("login-box-content clearfix"));

	_click(_submit("dwfrm_login_unregistered"));
	//enter shipping address
	shippingAddress($shippingAddress,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	BillingAddress($billingAddress,0);
	_assertContainsText("/Billing/", _div("/step-2 active/"))
	_assertVisible(_div("checkout-billing"));
	 PaymentDetails($credit_card,0);
	_click(_submit("dwfrm_billing_save"));
	_wait(3000);
	//_click(_link("Step 1: Billing", _in(_div("step-1 inactive"))));
	_click(_link($Checkout_Data[0][2], _in(_div("/step-2 inactive/"))));
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


if(!isMobile())
{
	
		var $t=_testcase("249029","Verify the functionality of '( What is this?)' link on billing page in Application as a Anonymous user.");
	$t.start();
	try
	{
	_assertVisible(_link("tooltip", _in(_div("form-row cvn required"))));
	_mouseOver(_link("tooltip", _in(_div("form-row cvn required"))));
	//verifying the display of tool tip
    if(_getAttribute(_link("tooltip", _in(_div("form-row cvn required"))),"aria-describedby")!=null)
              {
                     _assert(true);
              }
        else
              {
                     _assert(false);
              }
	}
	catch($e)
	{      
	       _logExceptionAsFailure($e);
	}
	$t.end();
}




var $t = _testcase("249092/249015","Verify the functionality of 'EDIT' link in order summary Section in right nav on billing page in Application as a Anonymous user.");
$t.start();
try
{
	//verify the UI of the Billing page
	billingPageUI();
	BillingAddress($billingAddress,1);
	 PaymentDetails($PaymentValidation,5);
	_wait(3000);
	//click on the edit details link
	_click(_link("section-header-note", _in(_heading3("/section-header pone-cartpage/"))));
	//verify the navigation
	_assertVisible(_table("cart-table"));
	_assertVisible(_div("custom-cart-page"));
	_assertEqual($Checkout_Data[0][1], _getText(_div("cart-header-text")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("249095","Verify the functionality of 'EDIT' link in SHIPPING ADDRESS Section in right nav on billing page in Application as a as a Anonymous user");
$t.start();
try
{
	_click(_submit("dwfrm_cart_checkoutCart"));
	_wait(3000);
	_click(_submit("dwfrm_login_unregistered"));
	////click on the edit details link
	_click(_link("section-header-note", _in(_heading3("/section-header pone-cartpage/"))));
	//verify the navigation
	_assertVisible(_table("cart-table"));
	_assertVisible(_div("custom-cart-page"));
	_assertEqual($Checkout_Data[0][1], _getText(_div("cart-header-text")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("249043/ 249022 / 249025","place an order with thw help of shipping level cuopon code");
$t.start();
try
{
	ClearCartItems();
	_wait(3000);
	
    search("431401");
	_click(_submit("add-to-cart"));
    // _click(_link("/mini-cart-link-checkout-link/"));
    _click(_link("button mini-cart-link-cart"));
    _wait(5000);
    var $ProdPrice=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
	var $ProdPrice=round($ProdPrice,2);
	var $prodquantity=_extract(_getText(_div("quantity cart-no-image")),"/: (.*)/",true).toString().split(" ")[0]

    _click(_link("mini-cart-link-checkout"));

	_click(_submit("dwfrm_login_unregistered"));
	
	//give shipping address
	 // var $pnameshippingpage=_getText(_div("mini-cart-name ponetext", _in(_div("checkout-mini-cart"))))
	var $pnameshippingpage=_getText(_div("mini-cart-name ponetext", _in(_div("checkout-mini-cart"))))
	  shippingAddress($shippingAddress,0);
    _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
    _click(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
    var $priceinshippingpage=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
    
    _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
    //adddressProceed();
    var $billingprice=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
    //comparing shipping and billing price
    _assertEqual($priceinshippingpage,$billingprice);
    //apply the coupon code at shipping page
    if(!_isVisible(_textbox("dwfrm_billing_couponCode")))
    	{
    	_assertVisible(_span("opened", _in(_div("primary"))));
    	_click(_span("opened", _in(_div("primary"))));
    	}
    _setValue(_textbox("dwfrm_billing_couponCode"), $Checkout_Data[3][1]);
    _click(_submit("dwfrm_billing_applyCoupon"));
    _assertVisible(_span($Checkout_Data[3][2]));
    _setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"), $uId);

      var $Prod_Price=parseFloat(_extract(_getText(_row("order-subtotal", _in(_table("order-totals-table")))),"/[$](.*)/",true));
      var $ShippingTax=parseFloat(_extract(_getText(_row("order-shipping bonetext  first ", _in(_table("order-totals-table")))),"/[$](.*)/",true));
      var $saleTax=parseFloat(_extract(_getText(_row("order-sales-tax bonetext", _in(_table("order-totals-table")))),"/[$](.*)/",true));
      var $discount=parseFloat(_extract(_getText(_row("order-shipping-discount discount")),"/[$](.*)/",true));
    
      var  $Prod_Price= $Prod_Price-$discount;
      var  $Prod_Price=round( $Prod_Price,2);
    
      var $Exp_TotalPrice=parseFloat(_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true));
	  var $ActualPrice=$Prod_Price+$ShippingTax+$saleTax;
      var $ActualPrice=round($ActualPrice,2);
	  _assertEqual($Exp_TotalPrice,$ActualPrice);
      //Payment details
	  PaymentDetails($credit_card,0);
	                      
	  //Placing order
	  // _click(_checkbox("/dwfrm_billing_confirm/"));
	  _click(_submit("dwfrm_billing_save"));
	  
	  //Checking order is placed or not
	  //_assertVisible(_heading1($payment_data[1][3]));
	  _assertEqual($Checkout_Data[1][2], _getText(_div("/step-3 active/")));
	
	  var $pnameinocppage=_getText(_link("/(.*)/", _in(_div("product-list-item")))).toString();
		  _click(_submit("submit"));
	  _wait(3000);
	  var $Qty=_getText(_div("/product-list-item/",_near(_div("Qty"))));


	  _assertEqual($prodquantity,$Qty);
	  
	  
	  var $priceinOCP=_extract(_getText(_div("line-item-price", _in(_div("order-shipment-table")))),"/[$](.*)/",true);
	 
	  var $ActordertotalOCP=_extract(_getText(_row("order-total", _in(_div("order-detail-summary")))),"/[$](.*)/",true);
	  //price verification
	  _assertEqual($ProdPrice,$priceinOCP);
	 // _assertEqual($billingprice,$Exp_TotalPrice);
	  _assertEqual($Exp_TotalPrice,$ActordertotalOCP);
	  
	  
	  var $orderno=_extract(_getText(_heading3("order-number")),"/[:] (.*)/",true);
	  _log($orderno);
	  
	  // var $orderkkkk=_getText(_span("order-number"))
	  // _log($orderkkkk);
	  
	  _focusWindow();
	  _takePageScreenShot();
	  
	  //payment in fo
	  _assertVisible(_div("order-payment-instruments"));
	 // _assertContainsText($credit_card[$i][5], _div("order-payment-instruments"));
	  _wait(5000);

 }
 catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("12334/249017","palce an order by order Level promotion ");
$t.start();
try
{
	//[4348]-[22275801]-Order promotion-AUTOORDER
	ClearCartItems();
	search($Checkout_Data[4][2]);
	_click(_submit("add-to-cart"));
	_wait(5000);
	
	search($Checkout_Data[5][2]);
	_click(_submit("add-to-cart"));
	//click /mini-cart-link-checkout
    _click(_link("button mini-cart-link-cart"));

	_wait(5000);
	var $ProdPrice=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
	var $ProdPrice=round($ProdPrice,2);
	_click(_link("mini-cart-link-checkout"));

	_click(_submit("dwfrm_login_unregistered"));
	//give shipping address

	shippingAddress($shippingAddress,0);
    _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
    _click(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
    

    var $priceinshippingpage=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
    
    _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
    //adddressProceed();
    
    var $billingprice=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
    //comparing shipping and billing price
    _assertEqual($priceinshippingpage,$billingprice);

    var $ToTPrice=parseFloat(_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true));
    var $Prod_Price=parseFloat(_extract(_getText(_row("order-subtotal", _in(_table("order-totals-table")))),"/[$](.*)/",true));
 
    if(!_isVisible(_textbox("dwfrm_billing_couponCode")))
	{
	_assertVisible(_span("opened", _in(_div("primary"))));
	_click(_span("opened", _in(_div("primary"))));
	}
    
    //apply the coupon code at order level
    _setValue(_textbox("dwfrm_billing_couponCode"), $Checkout_Data[4][1]);
    _click(_submit("dwfrm_billing_applyCoupon"));
    _assertVisible(_span($Checkout_Data[3][2]));
    _setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"), $uId);

    var $discount=parseFloat(_extract(_getText(_row("order-discount discount")),"/[$](.*)/",true));
    $Prod_Price=$Prod_Price-$discount;
    //extract shipping price and sale tax
    var $OldShippingTax=parseFloat(_extract(_getText(_row("order-shipping bonetext  first ", _in(_table("order-totals-table")))),"/[$](.*)/",true));
    var $saleTax=parseFloat(_extract(_getText(_row("order-sales-tax bonetext", _in(_table("order-totals-table")))),"/[$](.*)/",true));
    var $ActualPrice= $Prod_Price+$OldShippingTax+$saleTax;
    $ActualPrice=round($ActualPrice,2);

    var $Exp_TotalPrice=parseFloat(_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true));
	_assertEqual($ActualPrice,$Exp_TotalPrice);
	
	//Payment details
	PaymentDetails($credit_card,1);
	                      
	//Placing order
	// _click(_checkbox("/dwfrm_billing_confirm/"));
	_click(_submit("dwfrm_billing_save"));
	  
	//Checking order is placed or not
	//_assertVisible(_heading1($payment_data[1][3]));
	_assertEqual($Checkout_Data[1][2], _getText(_div("/step-3 active/")));
	
	var $pnameinocppage=_getText(_link("/(.*)/", _in(_div("product-list-item")))).toString();
	
	_click(_submit("submit"));
	_wait(3000);
	var $Qty=_getText(_div("/product-list-item/",_near(_div("Qty"))));
	_assertEqual($prodquantity,$Qty);
	  
	  
	var $priceinOCP=_extract(_getText(_row("order-subtotal", _in(_table("order-totals-table")))),"/[$](.*)/",true);
	 
	var $ActordertotalOCP=_extract(_getText(_row("order-total", _in(_div("order-detail-summary")))),"/[$](.*)/",true);
	//price verification
	_assertEqual($ProdPrice,$priceinOCP);
	//_assertEqual($billingprice,$ExpTotPrice);
	_assertEqual($Exp_TotalPrice,$ActordertotalOCP);
	  
	  
	var $orderno=_extract(_getText(_heading3("order-number")),"/[:] (.*)/",true);
	_log($orderno);
	  
	// var $orderkkkk=_getText(_span("order-number"))
	// _log($orderkkkk);
	  
	  _focusWindow();
	  _takePageScreenShot();
	  
	  //payment in fo
	  _assertVisible(_div("order-payment-instruments"));
	 // _assertContainsText($credit_card[$i][1], _div("order-payment-instruments"));
	  _wait(5000);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
ClearCartItems();
var $t=_testcase("249083/249084/249085/249086/249028/249052 ","Verify whether user is able to place order using VISA, MASTERCARD, AMERICAN EXPRESS, DISCOVER as credit card type  and Credit card alone on billing page in Application as a Anonymous user.");
$t.start();
try
{
       for(var $i=0;$i<$credit_card.length;$i++)
              {
                   //navigate to cart page
    		       search($Checkout_Data[0][0]);
    	           _click(_submit("add-to-cart"));
    	           //navigate to cart page
    	          _click(_link("button mini-cart-link-cart", _in(_div("mini-cart-totals"))));
        	      	var $ProdPrice=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
    		      	var $ProdPrice=round($ProdPrice,2);
                    var $prodquantity=_extract(_getText(_div("quantity cart-no-image")),"/: (.*)/",true).toString().split(" ")[0]

                     //navigate to checkout page
                 
    		      	 //click on the checkout
    		      	_click(_link("/mini-cart-link-checkout/", _in(_div("mini-cart-content"))));
    		      	
    		      	_click(_submit("dwfrm_login_unregistered"));
                     //comparing product name in shipping page
                    var $pnameshippingpage=_getText(_div("mini-cart-name ponetext", _in(_div("checkout-mini-cart"))))
//             
                     shippingAddress($shippingAddress,0);
                 
                     _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
                     _click(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
                    
                     var $priceinshippingpage=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
                     
                     _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
                    // adddressProceed();
                     _setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"), $uId);
                     var $billingprice=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
                     //comparing shipping and billing price
                     _assertEqual($priceinshippingpage,$billingprice);
                     var $shipppingCharge=parseFloat(_extract(_getText(_row("order-shipping bonetext  first ")),"/[$](.*)/",true));
                 	 var $saleTax=parseFloat(_extract(_getText(_row("order-sales-tax bonetext")),"/[$](.*)/",true));
                     var $ExpTotPrice=$ProdPrice+$shipppingCharge+$saleTax;
                     var $ExpTotPrice=round($ExpTotPrice,2);

                     //Payment details
                       PaymentDetails($credit_card,$i);
                     _click(_submit("dwfrm_billing_save"));
                 
                     _assertEqual($Checkout_Data[1][2], _getText(_div("/step-3 active/")));

                     var $pnameinocppage=_getText(_link("/(.*)/", _in(_div("product-list-item")))).toString();
                  
                     _click(_submit("submit"));
                     _wait(3000);
                     var $Qty=_getText(_div("/product-list-item/",_near(_div("Qty"))));
                     _assertEqual($prodquantity,$Qty);
                     
                     
                     var $priceinOCP=_extract(_getText(_div("line-item-price", _in(_div("order-shipment-table")))),"/[$](.*)/",true);
                    
                     var $ActordertotalOCP=_extract(_getText(_row("order-total", _in(_div("order-detail-summary")))),"/[$](.*)/",true);
                     //price verification
                     _assertEqual($ProdPrice,$priceinOCP);
                     _assertEqual($billingprice,$ExpTotPrice);
                     _assertEqual($ExpTotPrice,$ActordertotalOCP);
                     
                     
                     var $orderno=_extract(_getText(_heading3("order-number")),"/[:] (.*)/",true);
                     _log($orderno);
                     
                     _focusWindow();
                     _takePageScreenShot();
                     
                     //payment in fo
                     _assertVisible(_div("order-payment-instruments"));
                     //_assertContainsText($credit_card[$i][5], _div("order-payment-instruments"));
                     _wait(5000);
                 }
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

//var $t = _testcase("249096/249081/249080/249069/249068/249050/248940","Verify the UI of 'SELECT CREDIT CARD TYPE Section on billing page in Application as a Registered user");
//$t.start();
//try
//{
//	 search($Checkout_Data[0][0]);
//    //navigate to checkout page
//	  _click(_submit("add-to-cart"));
//   // _click(_link("/mini-cart-link-checkout-link/"));
//	 	_click(_link("/mini-cart-link-checkout/", _in(_div("mini-cart-content"))));
//      	
//      	_click(_submit("dwfrm_login_unregistered"));
//    _wait(5000);
//    
//  
//    shippingAddress($shippingAddress,0);
//    _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
//    _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//   // adddressProceed();
//    ///.'ORDER SUMMARY' as protected text and edit link should be displayed as ORDER SUMMARY 
//    _assertVisible(_cell($Checkout_Data[1][0]));
//    _assertVisible(_cell($Checkout_Data[2][0]));
//    _assertVisible(_cell($Checkout_Data[3][0]));
//    _assertVisible(_cell($Checkout_Data[4][0]));
//    
//    //SELECT CREDIT CARD TYPE Section with following fields :
//    _assertVisible(_span($Checkout_Data[5][0]));
//    _assertVisible(_span($Checkout_Data[6][0]));
//    _assertVisible(_span($Checkout_Data[7][0]));
//    _assertVisible(_span($Checkout_Data[8][0]));
//    _assertVisible(_span($Checkout_Data[9][0]));
//    _assertVisible(_link("tooltip", _in(_div("form-row cvn required"))));
//    _assertVisible(_submit($Checkout_Data[1][1]));
//    //_assertVisible(_span("Save this card"));
//
//    //SHIPPING ADDRESS' as protected text and edit link should be displayed as SHIPPING 
//    _assertVisible(_link($Checkout_Data[2][2], _in(_div("mini-shipment order-component-block  first "))));
//    _assertVisible(_div("address", _in(_div("mini-shipment order-component-block  first "))));
//    
//    // 'STEP 1: Shipping Addresses STEP 2: Shipping Methods STEP 3: Billing STEP 4: Place Order' 
//    _assertEqual($Checkout_Data[2][1], _getText(_div("breadcrumb bonetext checkout-progress-indicator")));
//    _setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"), $uId);
//
//    for(var $i=0;$i<$PaymentValidation.length;$i++)
//    	{
//    	 PaymentDetails($PaymentValidation,$i);
//    	 _click(_submit("dwfrm_billing_save"));
//    	 _wait(3000);
//    	 if($i==0)
//    		 {
//    		   _assertEqual(true, _submit("dwfrm_billing_save").disabled);
//    		   _assertVisible(_span($PaymentValidation[0][8], _near(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"))));
//    		   _assertVisible(_span($PaymentValidation[1][8], _near(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"))));
//    		   _assertVisible(_span($PaymentValidation[0][11], _near(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"))));
//    		   _assertVisible(_span($PaymentValidation[0][9]));
//    		   _assertVisible(_span($PaymentValidation[0][10]));
//
//
//    		 }
//    	 if($i==1)
//    		 {
//
//  		     _assertVisible(_span($PaymentValidation[1][9], _near(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"))));
//    		 _assertEqual($PaymentValidation[1][7], _style(_span("/dwfrm_billing_paymentMethods_creditCard_number/"),"color"));
//    		 }
//    	 if($i==2)
//    		 {
//    		 _assertVisible(_span($PaymentValidation[1][8], _near(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"))));
//    		 _assertEqual($PaymentValidation[1][7],_style(_span("/dwfrm_billing_paymentMethods_creditCard_number/"),"color"));
//    		 _assertVisible(_span($PaymentValidation[0][11]));
//    		 _assertEqual($PaymentValidation[1][7],_style(_span("/dwfrm_billing_paymentMethods_creditCard_cvn/"),"color"));
//    		 }
//    	 if($i==3)
//    		 {
//    		 _assertVisible(_div($PaymentValidation[3][9]));
//    		 _assertEqual($PaymentValidation[0][7], _style(_div("form-caption error-message"),"background-color"));
//    		 _assertVisible(_div($PaymentValidation[3][8]));
//    		 _assertEqual($PaymentValidation[0][7], _style(_div("form-caption error-message", _in(_div("form-row required"))),"background-color"));    		 }
//    	 if($i==4)
//    		 {
//    		 _assertVisible(_div($PaymentValidation[3][9]));
//    		 _assertEqual($PaymentValidation[0][7],_style(_div("form-caption error-message", _near(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"))),"background-color"));
//    		 }
//    	 if($i==5)
//    		 {
//    		 //_assertVisible(_heading1($payment_data[1][3]));
//             _assertEqual($Checkout_Data[1][2], _getText(_div("/step-3 active/")));
//    		 }
//    	
//    	}
//    
//
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();

//
//var $t = _testcase("","");
//$t.start();
//try
//{
//
//
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();
//var $t = _testcase("","");
//$t.start();
//try
//{
//
//
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();
//
///*var $t = _testcase("","");
//$t.start();
//try
//{
//
//
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();*/
//

