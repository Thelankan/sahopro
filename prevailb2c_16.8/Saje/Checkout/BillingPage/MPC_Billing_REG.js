_include("../../util.GenericLibrary/GlobalFunctions.js");
//_include("../../util.GenericLibrary/BM_Configuration.js");

_resource("MPCheckout.xls");
var $credit_card= _readExcelFile("MPCheckout.xls","credit_card");
var $CreateCreditCard= _readExcelFile("MPCheckout.xls","CreateCreditCard");
var $Billing= _readExcelFile("MPCheckout.xls","Billing");
var $PaymentValidation= _readExcelFile("MPCheckout.xls","PaymentValidation");
var $Checkout_Data= _readExcelFile("MPCheckout.xls","Checkout_Data");

var $OwnerName=new Array();

//for deleting user
//deleteUser();

SiteURLs();
cleanup();
_setAccessorIgnoreCase(true); 
//log in
login();

var $t = _testcase("249018/249046","Verify the scenario if User selected 'Use this address for Billing' checkbox on shipping page in Application as a Registered user.");
$t.start();
try
{
	//add prod to cart
	search($Checkout_Data[0][0]);
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	//click on the min cart icon
	_click(_span("minicart-icon spriteimg"));
	//click on the checkout
	_click(_link("mini-cart-link-checkout", _in(_div("mini-cart-content"))));
	//enter shipping address
	shippingAddress($shippingAddress,0);
	//verify the UI of the billing page

	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	billingPageUI();
	_assertEqual($shippingAddress[0][1], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
	_assertEqual($shippingAddress[0][2], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
	_assertEqual($shippingAddress[0][3], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
	_assertEqual($shippingAddress[0][4], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_address2")));
	_assertEqual($shippingAddress[0][7], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
	_assertEqual($shippingAddress[0][5], _getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
	_assertEqual($shippingAddress[0][6], _getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
	_assertEqual($shippingAddress[0][8], _getValue(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone")));
	_assertEqual($shippingAddress[0][9], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_phone")));
	_assertEqual($shippingAddress[0][10], _getValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("249019/248904","Verify the scenario if User not selected 'Use this address for Billing' checkbox on shipping page in Application as a Registered user.");
$t.start();
try
{
	deleteAddress();
	_click(_span("minicart-icon spriteimg"));
	//functonality of Take me to the check out out
	
	_click(_link("mini-cart-link-checkout"));
	//verify the navigation of the link Take me to the check out out
	_assertVisible(_div("js-checkout-shipping"));
	
	//enter shipping address
	shippingAddress($shippingAddress,0);
	//uncheck the checkbox
	
	_uncheck(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	//click on the submit
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	_assertNotEqual($shippingAddress[0][1], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
	_assertNotEqual($shippingAddress[0][2], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
	_assertNotEqual($shippingAddress[0][3], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
	_assertNotEqual($shippingAddress[0][4], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_address2")));
	_assertNotEqual($shippingAddress[0][7], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
	_assertEqual($shippingAddress[0][5], _getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
	_assertNotEqual($shippingAddress[0][6], _getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
	_assertNotEqual($shippingAddress[0][8], _getValue(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone")));
	_assertNotEqual($shippingAddress[0][9], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_phone")));
//	_assertNotEqual($shippingAddress[0][10], _getValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
if(!isMobile())
{
		var $t=_testcase("249056 ","Verify the functionality of '( What is this?)' link on billing page in Application as a Anonymous user.");
	$t.start();
	try
	{
	_assertVisible(_link("tooltip", _in(_div("form-row cvn required"))));
	_mouseOver(_link("tooltip", _in(_div("form-row cvn required"))));
	//verifying the display of tool tip
    if(_getAttribute(_link("tooltip", _in(_div("form-row cvn required"))),"aria-describedby")!=null)
              {
                     _assert(true);
              }
        else
              {
                     _assert(false);
              }
	}
	catch($e)
	{      
	       _logExceptionAsFailure($e);
	}
	$t.end();
}

var $t = _testcase("249020/249031","Verify the Scenario if user select 'Choose an Address' from the drop down on billing page in Application as a Registered user.");
$t.start();
try
{
	deleteAddress();
	_click(_link("section-header-note address-create buttonctaone button"));
	
	_click(_link("section-header-note address-create buttonctaone button"));
	addAddress($shippingAddress,0);
	
	_click(_submit("dwfrm_profile_address_create"));
	_wait(3000);
	
	_click(_link("section-header-note address-create buttonctaone button"));
	addAddress($shippingAddress,1);
	_click(_submit("dwfrm_profile_address_create"));
	_wait(3000);
	_click(_span("minicart-icon spriteimg"));
	_click(_link("mini-cart-link-checkout"));
	//enter shipping address
	shippingAddress($shippingAddress,0);
	_uncheck(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));

	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//uncheck the checkbox
	_setSelected(_select("dwfrm_billing_addressList"), "1");
	_assertEqual($shippingAddress[0][1], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
	_assertEqual($shippingAddress[0][2], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
	_assertEqual($shippingAddress[0][3], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
	_assertEqual($shippingAddress[0][4], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_address2")));
	_assertEqual($shippingAddress[0][7], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
	_assertEqual($shippingAddress[0][5], _getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
	_assertEqual($shippingAddress[0][6], _getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
	_assertEqual($shippingAddress[0][8], _getValue(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone")));
	_assertEqual($shippingAddress[0][9], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_phone")));
	_assertEqual($shippingAddress[0][10], _getValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress")));
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
/*
var $t = _testcase("","");
$t.start();
try
{
	deleteAddress();
	//add prod to cart
	search($Checkout_Data[0][0]);
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	//click on the min cart icon
	_click(_span("minicart-icon spriteimg"));
	//click on the checkout
	_click(_link("mini-cart-link-checkout", _in(_div("mini-cart-content"))));
	//enter shipping address
	shippingAddress(($shippingAddress,0);
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/

//CreateCreditCard($sheet, $i);
var $t = _testcase("249023/249033/249093/249037/249038/249039/249049/249070/ 249071/249074/249055  " ,"Verify the Field validation of 'Email' field on billing page in Application as a Registered user.");
$t.start();
try
{
	deleteAddress();
	//add prod to cart
	search($Checkout_Data[0][0]);
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	//click on the min cart icon
	_click(_span("minicart-icon spriteimg"));
	//click on the checkout
	_click(_link("mini-cart-link-checkout", _in(_div("mini-cart-content"))));
	//enter shipping address
	shippingAddress($shippingAddress,0);
	  _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
			for(var $i=0;$i<5;$i++)
			{
				
				if($i==0)
				{
						BillingAddress($Billing,$i);
						_click(_submit("dwfrm_billing_save"));
						//fname
						_assertVisible(_span($Billing[0][13],_near(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"))));
						_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"),"background-color"));
						//lname
						_assertVisible(_span($Billing[0][14],_near(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"))));
						_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"),"background-color"));
						//addresss
						_assertVisible(_span($Billing[0][15],_near(_textbox("dwfrm_billing_billingAddress_addressFields_address1"))));
						_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_addressFields_address1"),"background-color"));
						//city 
						
						_assertVisible(_span($Billing[1][13],_near(_textbox("dwfrm_billing_billingAddress_addressFields_city"))));
						_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_addressFields_city"),"background-color"));
						//country
						//_assertVisible(_span($Billing[1][14]));
						//_assertEqual($Billing[3][12], _style(_select("dwfrm_billing_billingAddress_addressFields_country"),"color"));

						//state
						_assertVisible(_span($Billing[1][15],_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
						//zipcode
						_assertVisible(_span($Billing[2][13],_near(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone"))));
						_assertEqual($Billing[3][12], _style(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone"),"background-color"));
						//phone
						_assertVisible(_span($Billing[2][14],_near(_textbox("dwfrm_billing_billingAddress_addressFields_phone"))));
						_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_addressFields_phone"),"background-color"));
						//email
						_assertVisible(_span($Billing[2][15],_near(_textbox("dwfrm_billing_billingAddress_email_emailAddress"))));
						_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),"background-color"));
						}
					if($i==1)
						{
						BillingAddress($Billing,$i);
						_click(_submit("dwfrm_billing_save"));
						//Fname & Last Name
						_assertEqual($Billing[0][12], _getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")).length);
						_assertEqual($Billing[0][12], _getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")).length);
						_assertEqual($Billing[0][12], _getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")).length);
						_assertEqual($Billing[1][12], _getText(_textbox("dwfrm_billing_billingAddress_addressFields_address2")).length);
						_assertEqual($Billing[0][12],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_city")).length);
						_assertEqual($Billing[2][12],_getText(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone")).length);
						_assertEqual($Billing[2][12],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_phone")).length);
						_assertEqual($Billing[1][12],_getText(_textbox("dwfrm_billing_billingAddress_email_emailAddress")).length);
						//email
						_assertVisible(_span($Billing[3][13]));
						_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),"background-color"));

						//zipcode
						_assertVisible(_span($Billing[3][14]));
						_assertEqual($Billing[3][12], _style(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone"),"background-color"));
						//country
						//_assertContainsText(" United StatesCanada", _div("form-row  required[4]"));
						}
					if($i==2)
						{
						BillingAddress($Billing,$i);
						_click(_submit("dwfrm_billing_save"));
						//address validation 
						//email
						_assertVisible(_span($Billing[3][13]));
						_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),"background-color"));

						//phone
						_assertVisible(_span($Billing[3][14]));
						_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_addressFields_phone"),"background-color"));
						
						}
					if($i==3)
						{
						BillingAddress($Billing,$i);
						_click(_submit("dwfrm_billing_save"));
						//Zip
						_assertVisible(_span($Billing[3][14]));
						//phone
						_assertVisible(_span($Billing[3][14]));
						_assertVisible(_span($Billing[3][14], _near(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone"))));
						//email
						_assertVisible(_span($Billing[3][13]));
						_assertEqual($Billing[3][12], _style(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),"background-color"));
						}
					if($i==4)
						{
						BillingAddress($Billing,$i);
						 PaymentDetails($credit_card,0);
						_click(_submit("dwfrm_billing_save"));
	                  	_assertVisible(_div("/step-3 active/"))
						_click(_div("checkout-placeorder"));
						_assertVisible(_div("place-order-totals"));
						//click on the edit details link
						_click(_span("checkout-bag-desktop", _in(_div("breadcrumb bonetext checkout-progress-indicator"))))
						//verify the navigation
						_assertVisible(_table("cart-table"));
						_assertVisible(_div("custom-cart-page"));
						_assertEqual($Checkout_Data[0][1], _getText(_div("cart-header-text")));

				}
			}
		}
	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("249094","Verify the functionality of 'EDIT' link in SHIPPING ADDRESS Section in right nav on billing page in Application as a Registered user");
$t.start();
try
{
       _click(_submit("dwfrm_cart_checkoutCart"));
       ////click on the edit details link
       shippingAddress($shippingAddress,0);
       _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
       _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
       //click on the edit link
       _click(_link("section-header-note", _in(_div("mini-shipment order-component-block  first "))));
       //verify the navigation
       _assertEqual("/Step 1: Shipping /", _getText(_div("/step-1 active/")));
       shippingAddress($shippingAddress,1);
       _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
       _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
       //verify the changes
       _assertEqual($shippingAddress[1][1], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
       _assertEqual($shippingAddress[1][2], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
       _assertEqual($shippingAddress[1][3], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
       _assertEqual($shippingAddress[1][4], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_address2")));
       _assertEqual($shippingAddress[1][7], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
       _assertEqual($shippingAddress[1][5], _getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
       _assertEqual($shippingAddress[1][6], _getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
       _assertEqual($shippingAddress[1][8], _getValue(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone")));
       _assertEqual($shippingAddress[1][9], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_phone")));
       _assertEqual($shippingAddress[1][10], _getValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress")));
       //verify the navigation
       

}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();

ClearCartItems();
var $t = _testcase("249030/249026/249027/249032/249040/249042","Verify the UI of 'SELECT CREDIT CARD TYPE Section on billing page in Application as a Registered user");
$t.start();
try
{
	search($Checkout_Data[0][0]);
	_click(_submit("add-to-cart"));
   // _click(_link("/mini-cart-link-checkout-link/"));
    _click(_link("mini-cart-link-checkout"));
                  
    _wait(5000);
    
    _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
    shippingAddress($shippingAddress,0);
    _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
    ///.'ORDER SUMMARY' as protected text and edit link should be displayed as ORDER SUMMARY 
    _assertVisible(_cell($Checkout_Data[1][0]));
    _assertVisible(_cell($Checkout_Data[2][0]));
    _assertVisible(_cell($Checkout_Data[3][0]));
    _assertVisible(_cell($Checkout_Data[4][0]));
    
    //SELECT CREDIT CARD TYPE Section with following fields :
    _assertVisible(_span($Checkout_Data[5][0]));
    _assertVisible(_span($Checkout_Data[6][0]));
    _assertVisible(_span($Checkout_Data[7][0]));
    _assertVisible(_span($Checkout_Data[8][0]));
    _assertVisible(_span($Checkout_Data[9][0]));
    _assertVisible(_link("tooltip", _in(_div("form-row cvn required"))));
    _assertVisible(_submit($Checkout_Data[1][1]));
    //_assertVisible(_span("Save this card"));
    
    
    //SHIPPING ADDRESS' as protected text and edit link should be displayed as SHIPPING 
    _assertVisible(_link($Checkout_Data[2][2], _in(_div("mini-shipment order-component-block  first "))));
    _assertVisible(_div("address", _in(_div("mini-shipment order-component-block  first "))));
    
    // 'STEP 1: Shipping Addresses STEP 2: Shipping Methods STEP 3: Billing STEP 4: Place Order' 
    _assertEqual($Checkout_Data[2][1], _getText(_div("breadcrumb bonetext checkout-progress-indicator")));

    for(var $i=0;$i<$PaymentValidation.length;$i++)
    	{
    	 PaymentDetails($PaymentValidation,$i);
    	 _click(_submit("dwfrm_billing_save"));
    	 if($i==0)
		 {
		   _assertEqual(true, _submit("dwfrm_billing_save").disabled);
		   _assertVisible(_span($PaymentValidation[0][8], _near(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"))));
		   _assertVisible(_span($PaymentValidation[1][8], _near(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"))));
		   _assertVisible(_span($PaymentValidation[0][11], _near(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"))));
		   _assertVisible(_span($PaymentValidation[0][9]));
		   _assertVisible(_span($PaymentValidation[0][10]));


		 }
	 if($i==1)
		 {

		 _assertVisible(_span($PaymentValidation[1][9], _near(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"))));
		 _assertEqual($PaymentValidation[1][7], _style(_span("/dwfrm_billing_paymentMethods_creditCard_number/"),"color"));
		 }
	 if($i==2)
		 {
		 _assertVisible(_span($PaymentValidation[1][8], _near(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"))));
		 _assertEqual($PaymentValidation[1][7],_style(_span("/dwfrm_billing_paymentMethods_creditCard_number/"),"color"));
		 _assertVisible(_span($PaymentValidation[0][11]));
		 _assertEqual($PaymentValidation[1][7],_style(_span("/dwfrm_billing_paymentMethods_creditCard_cvn/"),"color"));
		 }
	 if($i==3)
		 {
		 _assertVisible(_div($PaymentValidation[3][8]));
		 _assertEqual($PaymentValidation[0][7], _style(_div("form-caption error-message"),"background-color"));
		 _assertVisible(_div($PaymentValidation[3][8]));
		 _assertEqual($PaymentValidation[0][7], _style(_div("form-caption error-message", _in(_div("form-row required"))),"background-color"));    		 }
	 if($i==4)
		 {
		 _assertVisible(_div($PaymentValidation[3][9]));
		 _assertEqual($PaymentValidation[0][7],_style(_div("form-caption error-message", _near(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"))),"background-color"));
		 }
	 if($i==5)
		 {
		 //_assertVisible(_heading1($payment_data[1][3]));
         _assertEqual($Checkout_Data[1][2], _getText(_div("/step-3 active/")));
		 }
    	
    	}
    

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("249087/249088/249089/249090/249091","Verify whether user is able to place order using VISA, MASTERCARD, AMERICAN EXPRESS, DISCOVER as credit card type  and Credit card alone on billing page in Application as a Registered user.");
$t.start();
try
{
             //delete existing address
             deleteAddress();
                       
             //click on add new address 
             _click(_link("Address Book"));
             _click(_link("section-header-note address-create buttonctaone button"));
             //Add address
             addAddress($shippingAddress,0);
             //click on create address
             _click(_submit("dwfrm_profile_address_create"));
             _wait(2000);
             //click on continue in address
             if (_isVisible(_div("/invalid-address/")))
             {
             _click(_link("Continue"));
             }
                
              for(var $i=0;$i<$credit_card.length-1;$i++)
             {
                //navigate to cart page
             search($Checkout_Data[0][0]);
             _click(_submit("add-to-cart"));
             //navigate to cart page
             _click(_link("button mini-cart-link-cart", _in(_div("mini-cart-totals"))));
             var $ProdPrice=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
             var $ProdPrice=round($ProdPrice,2);
             var $prodquantity=_extract(_getText(_div("quantity cart-no-image")),"/: (.*)/",true).toString().split(" ")[0]

             //navigate to checkout page
             
             //click on the checkout
             _click(_link("/mini-cart-link-checkout/", _in(_div("mini-cart-content"))));
             
                           //_click(_submit("dwfrm_login_unregistered"));
               //comparing product name in shipping page
               var $pnameshippingpage=_getText(_div("mini-cart-name ponetext", _in(_div("checkout-mini-cart"))))
             
               shippingAddress($shippingAddress,0);
               
               _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
               _click(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
             
               var $priceinshippingpage=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
               
               _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
               // adddressProceed();
               _setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"), $uId);
               var $billingprice=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
               //comparing shipping and billing price
               _assertEqual($priceinshippingpage,$billingprice);
               var $shipppingCharge=parseFloat(_extract(_getText(_row("order-shipping bonetext  first ")),"/[$](.*)/",true));
               var $saleTax=parseFloat(_extract(_getText(_row("order-sales-tax bonetext")),"/[$](.*)/",true));
               var $ExpTotPrice=$ProdPrice+$shipppingCharge+$saleTax;
               var $ExpTotPrice=round($ExpTotPrice,2);
             
               //Payment details
               PaymentDetails($credit_card,$i);
               
               var $CardName= _getText(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"));
               $OwnerName.push($CardName);
               //Placing order
               _check(_checkbox("dwfrm_billing_paymentMethods_creditCard_saveCard"))
               _click(_submit("dwfrm_billing_save"));
               
               //Checking order is placed or not
               //_assertVisible(_heading1($payment_data[1][3]));
               _assertEqual($Checkout_Data[1][2], _getText(_div("/step-3 active/")));
             
               _click(_submit("submit"));
               _wait(3000);
               var $Qty=_getText(_div("/product-list-item/",_near(_div("Qty"))));
               _assertEqual($prodquantity,$Qty);
               
               
               var $priceinOCP=_extract(_getText(_div("line-item-price", _in(_div("order-shipment-table")))),"/[$](.*)/",true);
             
               var $ActordertotalOCP=_extract(_getText(_row("order-total", _in(_div("order-detail-summary")))),"/[$](.*)/",true);
               //price verification
               _assertEqual($ProdPrice,$priceinOCP);
               //_assertEqual($billingprice,$ExpTotPrice);
               _assertEqual($ExpTotPrice,$ActordertotalOCP);
               
               
               var $orderno=_extract(_getText(_heading3("order-number")),"/[:] (.*)/",true);
               _log($orderno);
               
               // var $orderkkkk=_getText(_span("order-number"))
               // _log($orderkkkk);
               
               _focusWindow();
               _takePageScreenShot();
               
               //payment in fo
               _assertVisible(_div("order-payment-instruments"));
               _assertContainsText($credit_card[$i][5], _div("order-payment-instruments"));
               _wait(5000);
       }
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();


var $t = _testcase("123456","check the saved card in the profile");
$t.start();
try
{
       _click(_span("/Hello/"));
       _click(_link("Payment methods"));
       var $Profile_CardName=_collectAttributes("_div","/cc-owner/","sahiText",_in(_list("payment-list")));
       _assertEqualArrays($OwnerName.sort(),$Profile_CardName.sort());


}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();


var $t = _testcase("249043/ 249022 / 249025","Verify the newly added card in profile get displayed in checkout Payment and Promotions page and vise versa in Application as a Registered user");
$t.start();
try
{
	 ClearCartItems();
//	_click(_link("user-account"));
//	//click Payment methods
//	_click(_link("Payment methods"));
//	
//	//DeleteCreditCard();
//	
//	_click(_link("section-header-note buttonctaone add-card button"));
//	//add new
//	
//	CreateCreditCard($CreateCreditCard,0);
//	//click on the ok
//	_click(_submit("dwfrm_paymentinstruments_creditcards_create"));
//	//_click(_submit("dwfrm_paymentinstruments_creditcards_create"));
//	_wait(3000);
	
     search("22258901");
	_click(_submit("add-to-cart"));
	//click on view bag
	_click(_link("button mini-cart-link-cart"));
    //comparing product name in shipping page
    var $pnameshippingpage=_getText(_div("mini-cart-name ponetext", _in(_div("checkout-mini-cart"))))
    var $prodquantity=_extract(_getText(_div("quantity cart-no-image")),"/: (.*)/",true).toString().split(" ")[0];
    
	//click on the ckeck out
    _click(_link("mini-cart-link-checkout"));
    _wait(5000);
    
    _setSelected(_select("dwfrm_singleshipping_addressList"), "1");
    
    _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
    _click(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
    var $priceinshippingpage=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
    
    _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
    //adddressProceed();
    
    var $billingprice=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
    //comparing shipping and billing price
    _assertEqual($priceinshippingpage,$billingprice);
    
    _setSelected(_select("dwfrm_billing_paymentMethods_creditCardList"), "1");
    //check the prepopulation
//    _assertEqual($CreateCreditCard[0][1], _getValue(_textbox("dwfrm_billing_paymentMethods_creditCard_owner")));
//    _assertEqual($CreateCreditCard[0][2], _getSelectedText(_select("dwfrm_billing_paymentMethods_creditCard_type")));
//   // _assertEqual($CreateCreditCard[0][3], _getValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/")));
//    _assertEqual($CreateCreditCard[0][4], _getSelectedText(_select("dwfrm_billing_paymentMethods_creditCard_expiration_month")));
//    _assertEqual($CreateCreditCard[0][5], _getSelectedText(_select("dwfrm_billing_paymentMethods_creditCard_expiration_year")));
    
    //apply the coupon code at shipping page
    _setValue(_textbox("dwfrm_billing_couponCode"), $Checkout_Data[3][1]);
    _click(_submit("dwfrm_billing_applyCoupon"));
    _assertVisible(_span($Checkout_Data[3][2]));

      var $Prod_Price=parseFloat(_extract(_getText(_row("order-subtotal", _in(_table("order-totals-table")))),"/[$](.*)/",true));
      var $ShippingTax=parseFloat(_extract(_getText(_row("order-shipping bonetext  first ", _in(_table("order-totals-table")))),"/[$](.*)/",true));
      var $saleTax=parseFloat(_extract(_getText(_row("order-sales-tax bonetext", _in(_table("order-totals-table")))),"/[$](.*)/",true));
      var $discount=parseFloat(_extract(_getText(_row("order-shipping-discount discount")),"/[$](.*)/",true));
    
      var  $Prod_Price= $Prod_Price-$discount;
      var  $Prod_Price=round( $Prod_Price,2);
    
      var $Exp_TotalPrice=parseFloat(_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true));
	  var $ActualPrice=$Prod_Price+$ShippingTax+$saleTax;
      var $ActualPrice=round($ActualPrice,2);
	  _assertEqual($Exp_TotalPrice,$ActualPrice);
      //Payment details
	  PaymentDetails($credit_card,0);
	                      
	  //Placing order
	  // _click(_checkbox("/dwfrm_billing_confirm/"));
	  _click(_submit("dwfrm_billing_save"));
	  
	  //Checking order is placed or not
	  //_assertVisible(_heading1($payment_data[1][3]));
	  _assertEqual($Checkout_Data[1][2], _getText(_div("/step-3 active/")));
	
	  var $pnameinocppage=_getText(_link("/(.*)/", _in(_div("product-list-item")))).toString();
		  _click(_submit("submit"));
	  _wait(3000);
	  var $Qty=_getText(_div("/product-list-item/",_near(_div("Qty"))));
	  _assertEqual($prodquantity,$Qty);
	  
	  
	  var $priceinOCP=_extract(_getText(_div("line-item-price", _in(_div("order-shipment-table")))),"/[$](.*)/",true);
	 
	  var $ActordertotalOCP=_extract(_getText(_row("order-total", _in(_div("order-detail-summary")))),"/[$](.*)/",true);
	  //price verification
	  _assertEqual($ProdPrice,$priceinOCP);
	  _assertEqual($billingprice,$ExpTotPrice);
	  _assertEqual($Exp_TotalPrice,$ActordertotalOCP);
	  
	  
	  var $orderno=_extract(_getText(_heading3("order-number")),"/[:] (.*)/",true);
	  _log($orderno);
	  
	  // var $orderkkkk=_getText(_span("order-number"))
	  // _log($orderkkkk);
	  
	  _focusWindow();
	  _takePageScreenShot();
	  
	  //payment in fo
	  _assertVisible(_div("order-payment-instruments"));
	  _assertContainsText($credit_card[$i][5], _div("order-payment-instruments"));
	  _wait(5000);

 }
 catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("12334/249017","palce an order by order Level promotion ");
$t.start();
try
{
	//[4348]-[22275801]-Order promotion-AUTOORDER
	ClearCartItems();
	search($Checkout_Data[4][2]);
	_click(_submit("add-to-cart"));
	_wait(5000);
	
	search($Checkout_Data[5][2]);
	_click(_submit("add-to-cart"));
	//click on view bag
	_click(_link("button mini-cart-link-cart"));
    //comparing product name in shipping page
    var $pnameshippingpage=_getText(_div("mini-cart-name ponetext", _in(_div("checkout-mini-cart"))))
    var $prodquantity=_extract(_getText(_div("quantity cart-no-image")),"/: (.*)/",true).toString().split(" ")[0];
    
	// _click /mini-cart-link-checkout
	_click(_link("mini-cart-link-checkout"));
	
	//give shipping address
    _setSelected(_select("dwfrm_singleshipping_addressList"), "1");
    _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
    _click(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
    var $priceinshippingpage=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
    
    _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
    //adddressProceed();
    
    var $billingprice=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
    //comparing shipping and billing price
    _assertEqual($priceinshippingpage,$billingprice);
    
    var $ToTPrice=parseFloat(_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true));
    var $Prod_Price=parseFloat(_extract(_getText(_row("order-subtotal", _in(_table("order-totals-table")))),"/[$](.*)/",true));

    if(!_isVisible(_textbox("dwfrm_billing_couponCode")))
	{
	_assertVisible(_span("opened", _in(_div("primary"))));
	_click(_span("opened", _in(_div("primary"))));
	}
    //apply the coupon code at order level
    _setValue(_textbox("dwfrm_billing_couponCode"), $Checkout_Data[4][1]);
    _click(_submit("dwfrm_billing_applyCoupon"));
    _assertVisible(_span($Checkout_Data[3][2]));

    var $discount=parseFloat(_extract(_getText(_row("order-discount discount")),"/[$](.*)/",true));
     $Prod_Price=$Prod_Price-$discount;
     var $OldShippingTax=parseFloat(_extract(_getText(_row("order-shipping bonetext  first ", _in(_table("order-totals-table")))),"/[$](.*)/",true));
     var $saleTax=parseFloat(_extract(_getText(_row("order-sales-tax bonetext", _in(_table("order-totals-table")))),"/[$](.*)/",true));
     
    var $ActualPrice= $Prod_Price+$OldShippingTax+$saleTax;
    $ActualPrice=round($ActualPrice,2);

    var $Exp_TotalPrice=parseFloat(_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true));
	_assertEqual($ActualPrice,$Exp_TotalPrice);
	
	//Payment details
	  PaymentDetails($credit_card,1);
	                      
	  //Placing order
	  // _click(_checkbox("/dwfrm_billing_confirm/"));
	  _click(_submit("dwfrm_billing_save"));
	  
	  //Checking order is placed or not
	  //_assertVisible(_heading1($payment_data[1][3]));
	  _assertEqual($Checkout_Data[1][2], _getText(_div("/step-3 active/")));
	
	  var $pnameinocppage=_getText(_link("/(.*)/", _in(_div("product-list-item")))).toString();
	
	  _click(_submit("submit"));
	  _wait(3000);
	  var $Qty=_getText(_div("/product-list-item/",_near(_div("Qty"))));
	  _assertEqual($prodquantity,$Qty);
	  
	  
	  var $priceinOCP=_extract(_getText(_row("order-subtotal", _in(_table("order-totals-table")))),"/[$](.*)/",true);
	 
	  var $ActordertotalOCP=_extract(_getText(_row("order-total", _in(_div("order-detail-summary")))),"/[$](.*)/",true);
	  //price verification
	  _assertEqual($Prod_Price,$priceinOCP);
	 // _assertEqual($billingprice,$ExpTotPrice);
	  _assertEqual($Exp_TotalPrice,$ActordertotalOCP);
	  
	  
	  var $orderno=_extract(_getText(_heading3("order-number")),"/[:] (.*)/",true);
	  _log($orderno);
	  
	  // var $orderkkkk=_getText(_span("order-number"))
	  // _log($orderkkkk);
	  
	  _focusWindow();
	  _takePageScreenShot();
	  
	  //payment in fo
	  _assertVisible(_div("order-payment-instruments"));
	  _assertContainsText($credit_card[$i][5], _div("order-payment-instruments"));
	  _wait(5000);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();