_include("../../util.GenericLibrary/GlobalFunctions.js");
//_include("../../util.GenericLibrary/BM_Configuration.js");

_resource("MPC_data.xls");
var $loginValidation = _readExcelFile("MPC_data.xls","loginValidation");
var $Login_Data = _readExcelFile("MPC_data.xls","Login_Data");
var $Card = _readExcelFile("MPC_data.xls","Card");

//for deleting user
deleteUser();

SiteURLs();
cleanup();
_setAccessorIgnoreCase(true); 


var $t = _testcase("248941","Verify the navigation to 'Checkout- Intermediate Login' page from mini cart page in Application as an Anonymous user.");
$t.start();
try
{
	//add prod to cart
	search($Login_Data[0][0]);
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	//click on the min cart icon
	_click(_span("minicart-icon spriteimg"));
	//click on the checkout
	_click(_link("mini-cart-link-checkout", _in(_div("mini-cart-content"))));
	//verify the navigation
	//varify the navigation
	_assertVisible(_div("login-box login-account"));
	_assertVisible(_div("checkoutlogin"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248943","Verify the functionality of 'X(Close)' button on 'forgot password' confirmation overlay in application as an Anonymous user");
$t.start();
try
{
	_assertVisible(_div("login-box login-account"));
	_assertVisible(_div("checkoutlogin"));
	//click on forgot password 
	_click(_link("/password-reset-id ptwotext ipad-desktop-show/", _in(_div("login-box login-account"))));
	//verify the navigation
	_assertVisible(_div("dialog-content ui-dialog-content ui-widget-content"));
	_assertVisible(_heading1($Login_Data[0][1]));
	//clik on the close
	_click(_button("ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close"));
	//verify the functinality
	_assertNotVisible(_div("dialog-content ui-dialog-content ui-widget-content"));
	_assertNotVisible(_heading1($Login_Data[0][1]));
	_wait(3000);
	_click(_link("mini-cart-link-checkout", _in(_div("mini-cart-content"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248944/248947248924","To validate fields in Login section in intermediate login page/To verify functionality when clicked on 'Login' button in intermediate login page");
$t.start();
try
{
     for(var $i=0;$i<$loginValidation.length;$i++)
    	 {
    	 _setValue(_textbox("/dwfrm_login_username_/", _in(_div("login-box login-account"))), $loginValidation[$i][1]);
    	 
    	 _setValue(_password("/dwfrm_login_password/",_in(_div("login-box login-account"))), $loginValidation[$i][2]);
    	 
    	 _click(_submit("Login",_in(_div("login-box login-account"))));
    	 _wait(3000);
    	 if($i==0)
    		 {
    		 //for email
    		 _assertVisible(_span($loginValidation[0][3]));
    		 _assertEqual($loginValidation[0][9], _style(_textbox("/dwfrm_login_username/"),"background-color"));
    		 //for pW
    		 _assertVisible(_span($loginValidation[0][4]));
    		 _assertEqual($loginValidation[0][8], _style(_textbox("/dwfrm_login_username/",_in(_div("login-box login-account"))),"background-color"));
    		 }
    	 if($i==1)
    		 {
    		 _assertVisible(_span($loginValidation[1][3]));
    		 _assertEqual($loginValidation[0][8], _style(_textbox("/dwfrm_login_username/",_in(_div("login-box login-account"))),"background-color"));

    		 }
    	 if($i==2)
    		 {
    		 _assertVisible(_span($loginValidation[1][3]));
    		 _assertEqual($loginValidation[0][8], _style(_textbox("/dwfrm_login_username/",_in(_div("login-box login-account"))),"background-color"));
    		 }
    	 if($i==3)
    		 {
    		 _assertVisible(_span($loginValidation[1][3]));
    		 _assertEqual($loginValidation[0][8], _style(_textbox("/dwfrm_login_username/",_in(_div("login-box login-account"))),"background-color"));
    		 
    		 }
    	 if($i==4)
    		 {
    		 _assertVisible(_div($loginValidation[2][3]));
    		 _assertEqual($loginValidation[0][6], _style(_div("error-form", _in(_div("login-box-content returning-customers clearfix", _in(_div("login-box login-account"))))),"background-color"));
    		 }
    	 if($i==5)
    		 {
    		 _assertEqual($loginValidation[5][4], _getText(_textbox("/dwfrm_login_username_/",_in(_div("login-box login-account")))).length);
    		 _assertEqual($loginValidation[5][4], _getText(_password("/dwfrm_login_password_/",_in(_div("login-box login-account")))).length);
    		 }
    	 if($i==6)
    		 {
    		 _assertVisible(_div("js-checkout-shipping"));
    		 _assertVisible(_div("shipping-method-list"));
    		 _assertVisible(_div("checkout-mini-cart"));
    		 
    		 }
   }
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
  _click(_link("button mini-cart-link-cart", _in(_div("mini-cart-totals"))));
  var $ProdPrice=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
  var $ProdPrice=round($ProdPrice,2);
  var $prodquantity=_extract(_getText(_div("quantity cart-no-image")),"/: (.*)/",true).toString().split(" ")[0]
  //navigate to checkout page
  //click on the checkout
  _click(_link("/mini-cart-link-checkout/", _in(_div("mini-cart-content"))));
  //comparing product name in shipping page
  var $pnameshippingpage=_getText(_div("mini-cart-name ponetext", _in(_div("checkout-mini-cart"))))

  shippingAddress($shippingAddress,0);
  _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
  _click(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
  
  _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
  // adddressProceed();
  _setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"), $uId);
  var $billingprice=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
  //comparing shipping and billing price
  _assertEqual($priceinshippingpage,$billingprice);
  var $shipppingCharge=parseFloat(_extract(_getText(_row("order-shipping bonetext  first ")),"/[$](.*)/",true));
  var $saleTax=parseFloat(_extract(_getText(_row("order-sales-tax bonetext")),"/[$](.*)/",true));
  var $ExpTotPrice=$ProdPrice+$shipppingCharge+$saleTax;
  var $ExpTotPrice=round($ExpTotPrice,2);

  //Payment details
  PaymentDetails($Card,0);
                      
  //Placing order
  // _click(_checkbox("/dwfrm_billing_confirm/"));
  _click(_submit("dwfrm_billing_save"));
  
  //Checking order is placed or not
  //_assertVisible(_heading1($payment_data[1][3]));
  _assertEqual($Login_Data[1][1], _getText(_div("step-3 active")));

  var $pnameinocppage=_getText(_link("/(.*)/", _in(_div("product-list-item")))).toString();
  if($pnameshippingpage.indexOf($pnameinocppage)>=0)                         
  {
         _assert(true);
  }
  _click(_submit("submit"));
  _wait(3000);
  var $Qty=_getText(_div("/product-list-item/",_near(_div("Qty"))));
  _assertEqual($prodquantity,$Qty);
  
  
  var $priceinOCP=_extract(_getText(_div("line-item-price", _in(_div("order-shipment-table")))),"/[$](.*)/",true);
 
  var $ActordertotalOCP=_extract(_getText(_row("order-total", _in(_div("order-detail-summary")))),"/[$](.*)/",true);
  //price verification
  _assertEqual($ProdPrice,$priceinOCP);
  _assertEqual($billingprice,$ExpTotPrice);
  _assertEqual($ExpTotPrice,$ActordertotalOCP);
  
  
  var $orderno=_extract(_getText(_heading3("order-number")),"/[:] (.*)/",true);
  _log($orderno);
  
  // var $orderkkkk=_getText(_span("order-number"))
  // _log($orderkkkk);
  
  _focusWindow();
  _takePageScreenShot();
  
  //payment in fo
  _assertVisible(_div("order-payment-instruments"));
 // _assertContainsText($credit_card[$i][5], _div("order-payment-instruments"));
  _wait(5000);


  cleanup();

var $t = _testcase("248946","Verify the functionality of 'Forgot password' link on intermediate login page in application as an Anonymous user");
$t.start();
try

{	 
		//add prod to cart
		search($Login_Data[0][0]);
		_click(_submit("add-to-cart"));
		//click on the min cart icon
		_click(_span("minicart-icon spriteimg"));
		//click on the checkout
		_click(_link("mini-cart-link-checkout", _in(_div("mini-cart-content"))));
	    _click(_link("/password-reset-id ptwotext ipad-desktop-show/", _in(_div("login-box-content returning-customers clearfix", _in(_div("login-box login-account"))))));
	    for(var $i=0;$i<5;$i++)
		 {
		 _setValue(_textbox("dwfrm_requestpassword_email"), $loginValidation[$i][1]);
		 _click(_submit("dwfrm_requestpassword_send"));
		 if($i==0)
			 {
    		 _assertVisible(_span($loginValidation[0][3]));
			 	_assertEqual($Login_Data[1][0], _style(_textbox("dwfrm_requestpassword_email"),"background-color"));
			 }
		 if($i==1)
			 {
			     _assertVisible(_span($loginValidation[1][3]));
			 	_assertEqual($Login_Data[1][0], _style(_textbox("dwfrm_requestpassword_email"),"background-color"));
			 }
		 if($i==2)
			 {
			 	_assertVisible(_span($loginValidation[1][3]));
			 	_assertEqual($Login_Data[1][0], _style(_textbox("dwfrm_requestpassword_email"),"background-color"));
			 }
		 if($i==3)
		 {
		 	_assertVisible(_span($loginValidation[1][3]));
		 	_assertEqual($Login_Data[1][0], _style(_textbox("dwfrm_requestpassword_email"),"background-color"));
		 }
		 if($i==4)
			 {
			 	_assertNotVisible(_span($loginValidation[1][3]));
			 	_assertVisible(_heading1($Login_Data[2][1]));
			 	_assertVisible(_link($Login_Data[0][2]));
			 	_click(_link($Login_Data[0][2]));
			 	_assertVisible(_div("hompage-better-icon"));
			 	_assertVisible(_div("homepage-heading"));
			 }

		 }
	 

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("248948","Verify the functionality of 'Create account now' button in intermediate login page in application as an Anonymous user.");
$t.start();
try
{
	_click(_link("mini-cart-link-checkout"));
	//click on the create account now
	_click(_submit("dwfrm_login_register"));
	//verify the navigation
	_assertVisible(_div("account-rightinner"));
	_assertVisible(_heading1($Login_Data[3][1]));
	_assertVisible(_div("/create_account account/"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("248949","Verify the functionality of 'Checkout as guest' button in intermediate login page in application as an Anonymous user");
$t.start();
try
{
	_click(_link("mini-cart-link-checkout"));
	//click on checkout as guest button. 
	_click(_submit("dwfrm_login_unregistered"));
	//verify the navigation
	_assertVisible(_div("js-checkout-shipping"));
	_assertVisible(_div("shipping-method-list"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("123444","palce an order with new account ");
$t.start();
try
{
	navigateToHomePage();
	if(!isMobile)
		{
		_click(_span($Login_Data[1][2]));

		}
	else
	{
		_click(_submit("/menu-toggle /"));

		_click(_span($Login_Data[1][2]));

	}
	createAccount();
	_wait(2000);
    //navigate to cart page
    search($Login_Data[0][0]);
    _click(_submit("add-to-cart"));
   //navigate to cart page
    _click(_link("button mini-cart-link-cart", _in(_div("mini-cart-totals"))));
	var $ProdPrice=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
	var $prodquantity=_extract(_getText(_div("quantity cart-no-image")),"/: (.*)/",true).toString().split(" ")[0]
	var $ProdPrice=round($ProdPrice,2);

    //navigate to checkout page
    //click on the checkout
	_click(_link("/mini-cart-link-checkout/", _in(_div("mini-cart-content"))));
	
    //comparing product name in shipping page
	var $pnameshippingpage=_getText(_div("mini-cart-name ponetext", _in(_div("checkout-mini-cart"))))

	shippingAddress($shippingAddress,0);
   
   _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
   _click(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
   var $priceinshippingpage=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
   
   _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
   // adddressProceed();
   _setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"), $uId);
   var $billingprice=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
   //comparing shipping and billing price
   _assertEqual($priceinshippingpage,$billingprice);
   var $shipppingCharge=parseFloat(_extract(_getText(_row("order-shipping bonetext  first ")),"/[$](.*)/",true));
   var $saleTax=parseFloat(_extract(_getText(_row("order-sales-tax bonetext")),"/[$](.*)/",true));
   var $ExpTotPrice=$ProdPrice+$shipppingCharge+$saleTax;
   var $ExpTotPrice=round($ExpTotPrice,2);

   //Payment details
   PaymentDetails($Card,0);
                       
   //Placing order
   // _click(_checkbox("/dwfrm_billing_confirm/"));
   _click(_submit("dwfrm_billing_save"));
   
   //Checking order is placed or not
   //_assertVisible(_heading1($payment_data[1][3]));
   _assertEqual($Login_Data[1][1], _getText(_div("step-3 active")));

   var $pnameinocppage=_getText(_link("/(.*)/", _in(_div("product-list-item")))).toString();
   if($pnameshippingpage.indexOf($pnameinocppage)>=0)                         
   {
          _assert(true);
   }
   _click(_submit("submit"));
   _wait(3000);
   var $Qty=_getText(_div("/product-list-item/",_near(_div("Qty"))));
   _assertEqual($prodquantity,$Qty);
   
   
   var $priceinOCP=_extract(_getText(_div("line-item-price", _in(_div("order-shipment-table")))),"/[$](.*)/",true);
  
   var $ActordertotalOCP=_extract(_getText(_row("order-total", _in(_div("order-detail-summary")))),"/[$](.*)/",true);
   //price verification
   _assertEqual($ProdPrice,$priceinOCP);
   _assertEqual($billingprice,$ExpTotPrice);
   _assertEqual($ExpTotPrice,$ActordertotalOCP);
   
   
   var $orderno=_extract(_getText(_heading3("order-number")),"/[:] (.*)/",true);
   _log($orderno);
   
   _focusWindow();
   _takePageScreenShot();
   
   //payment in fo
   _assertVisible(_div("order-payment-instruments"));
   //_assertContainsText($card][5], _div("order-payment-instruments"));
   _wait(5000);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();