_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Shipping Page.xls");

var $Shipping_Page=_readExcelFile("Shipping Page.xls","Shipping_Page");
var $addr_data=_readExcelFile("Shipping Page.xls","Address");
var $Shipping_addr=_readExcelFile("Shipping Page.xls","Address2");
var $Validation=_readExcelFile("Shipping Page.xls","Validations");
var $Payment_Data=_readExcelFile("Shipping Page.xls","Payment_Data");
var $Address=_readExcelFile("Shipping Page.xls","Address1");


SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


//login to the application
login();
_wait(3000);


var $t = _testcase("248957/248994", "Verify the navigation to  'SHIPPING' page in Application as a Registered  user./Verify the calculation functionality in Order Summary pane on shipping page in Application as a Registered user.");
$t.start();
try
{
//add items to cart
navigateToCart($Shipping_Page[0][1],1);
//click on go strait to checkout link
_click(_link("mini-cart-link-checkout"));
//verify the navigation
verifyNavigationToShippingPage();

//enter shipping address
shippingAddress($shippingAddress,0);

//verify order summary page
var $productNameinOrdersummary=_getText(_link("/(.*)/",_in(_div("checkout-mini-cart"))));
var $quantityinOrdersummary=parseFloat(_extract(_getText(_div("/mini-cart-pricing/",_in(_div("checkout-mini-cart")))),"/: (.*) /",true));
var $PriceinOrderSummary=parseFloat(_extract(_getText(_div("/mini-cart-pricing/",_in(_div("checkout-mini-cart")))),"/[$](.*)/",true));

//fields in order summary page
_assertVisible(_cell("Subtotal"));
_assertVisible(_cell("Shipping"));
_assertVisible(_cell("order-sale-text"));
_assertVisible(_cell("/order-total-label/"));

//verifying the fields
_assertEqual($prodNameincart,$productNameinOrdersummary);
_assertEqual($QuantityInCart,$quantityinOrdersummary);
_assertEqual($prodpriceincart,$PriceinOrderSummary);

//selecting radio buttons
_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
//verify the selection
_assert(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID").checked);
_assertEqual(true,_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID").checked);
var $ShippingMethodCost=parseFloat(_extract(_getText(_span("/(.*)/", _in(_div("form-row form-indent label-inline")))),"/[$](.*)/",true));
_wait(2000);
var $shippingsummaryCost=parseFloat(_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true));
_assertEqual($ShippingMethodCost,$shippingsummaryCost);

//calculating shipping total in order summary pane
var $subtotal=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));

if (_isVisible(_row("/order-sales-tax/")))
	{
	var $salestax=parseFloat(_extract(_getText(_row("/order-sales-tax/")),"/[$](.*)/",true));
	}
	else
		{
		var $salestax=0;
		}

_log("SubTotal in shipping page="+$subtotal);
_log("shipping cost in shipping page="+$shippingsummaryCost);
_log("salestax in shipping page="+$salestax);

var $addedtotal=parseFloat($subtotal+$shippingsummaryCost+$salestax);
var $totalRoundoff=round($addedtotal,2);
//_log($totaladd);
//var $addedtotal=parseInt(($totaladd)+($salestax));
//_log($addedtotal);

var $ordertotal=_extract(_getText(_row("order-total")),"/[$](.*)/",true);

//comparing added total and order total
_assertEqual($totalRoundoff,$ordertotal);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248958", "Verify the UI of checkout 'SHIPPING' page in Application as a Registered  user.");
$t.start();
try
{
//verify the UI of shipping page
//first name
_assertVisible(_label("/First Name/"));
_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"));
//last name
_assertVisible(_label("/Last Name/"));
_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"));
//address1
_assertVisible(_label("/Address 1/"));
_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"));
//address2
_assertVisible(_label("/Address 2/"));
_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"));
//country
_assertVisible(_label("/Country/"));
_assertVisible(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"));
//state
_assertVisible(_label("/State/"));
_assertVisible(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"));
//city
_assertVisible(_label("/City/"));
_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"));
//zip
_assertVisible(_label("/Zip Code/"));
_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")); 
//phone number
_assertVisible(_label("/Phone/"));
_assertVisible(_telephonebox("dwfrm_singleshipping_shippingAddress_addressFields_phone"));
_assertVisible(_div($Shipping_Page[10][0]));
//add to address book
_assertVisible(_label($Shipping_Page[0][0]));
_assertVisible(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook"));
_assertNotTrue(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook").checked);
//use this billing address for registered user
_assertVisible(_label($Shipping_Page[1][0]));
_assertVisible(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
_assertNotTrue(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked);
if(!isMobile() || mobile.iPad())
	{
	//tooltip links
	_assertVisible(_link("tooltip"));
	_assertVisible(_link("tooltip[1]"));
	}

//select shipping method section
_assertVisible(_div("shipping-method-list"));
var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_div("shipping-method-list")));
	for(var $i=0;$i<$totalShippingMethods;$i++)
	{
	//shipping methods
	_assertVisible(_div("form-row form-indent label-inline["+$i+"]"));
	}
//continue button
_assertVisible(_submit("dwfrm_singleshipping_shippingAddress_save"));
//summary section
_assertVisible(_heading3($Shipping_Page[6][0]));
_assertVisible(_div("secondary"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248967","Verify the functionality of the Checkbox 'Use this address for Billing' on shipping page in Application as aRegistered user.");
$t.start();
try
{

shippingAddress($shippingAddress,0);
//check the use this address for billing checkbox
_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//verify the address prepopulation in billing page
_assertEqual($shippingAddress[0][1],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
_assertEqual($shippingAddress[0][2],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
_assertEqual($shippingAddress[0][3],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
_assertEqual($shippingAddress[0][5],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
_assertEqual($shippingAddress[0][6],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
_assertEqual($shippingAddress[0][7],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
_assertEqual($shippingAddress[0][8],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_postal")));
_assertEqual($shippingAddress[0][9],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_phone")));
//navigating back to shipping page
_click(_link("Shipping"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248967","Verify the UI of 'SELECT SHIPPING METHOD' Section on shipping page in Application as a Registered user.");
$t.start();
try
{
//verifying the UI of Shipping section
_assertVisible(_div("shipping-method-list"));
var $totalToolTips=_count("_link","tooltip",_in(_div("shipping-method-list")));
var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_div("shipping-method-list")));
_assertEqual($totalShippingMethods,$totalToolTips);
for(var $i=0;$i<$totalShippingMethods;$i++)
{
//shipping methods
_assertVisible(_div("form-row form-indent label-inline["+$i+"]"));
_assertVisible(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
/*
//mousehovering on the tooltips
_mouseOver(_link("tooltip",_in(_div("form-row form-indent label-inline["+$i+"]"))));
//verifying the display of tool tip
_assertVisible(_div("small tooltip-shipping"));
*/	
}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248968/248975","Verify the functionality of radio buttons under 'SELECT SHIPPING METHOD' Section on shipping page in Application as a Registered user./Verify the functionality of shipping charge on shipping page in Application as a Registered user.");
$t.start();
try
{
//verify the functionality of radio buttons
var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_div("shipping-method-list")));
var $count=0;
for(var $i=0;$i<$totalShippingMethods;$i++)
	{
	//selecting radio buttons
	_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
	//verify the selection
	_assert(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked);
	_assertEqual(true,_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked);
	var $ShippingMethodCost=_extract(_getText(_span("/(.*)/", _in(_div("form-row form-indent label-inline["+$i+"]")))),"/[$](.*)/",true);
	_wait(2000);
	var $shippingsummaryCost=_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true);
	_assertEqual($ShippingMethodCost,$shippingsummaryCost);
	}		
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248974","Verify the display of 'Details' tool tip in select shipping option Section on shipping page in Application as a Registered user.");
$t.start();
try
{
//verifying the tooltips
var $totalToolTips=_count("_link","tooltip",_in(_div("shipping-method-list")));
var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_div("shipping-method-list")));
_assertEqual($totalShippingMethods,$totalToolTips);
for(var $i=0;$i<$totalShippingMethods;$i++)
	{
	//mousehovering on the tooltips
    _mouseOver(_link("tooltip",_in(_div("form-row form-indent label-inline["+$i+"]"))));
    //verifying the display of tool tip
    if(_getAttribute(_link("tooltip",_in(_div("form-row form-indent label-inline["+$i+"]"))),"aria-describedby")!=null)
          {
                 _assert(true);
          }
    else
          {
                 _assert(false);
          }
	}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248971","Verify the functionality of 'EDIT' link in order summary Section in right nav on shipping page in Application as a Registered user.");
$t.start();
try
{
//navigating to shipping page
if(!isMobile() || mobile.iPad())
{
var $productName=_getText(_link("/(.*)/",_in(_div("/mini-cart-name/",_in(_div("checkout-mini-cart"))))));
//clicking on edit link present in the order Summuary section
_click(_link("EDIT",_in(_div("secondary"))));
}
else
{
var $productName=_getText(_link("/(.*)/",_in(_div("/mini-cart-name/",_in(_div("checkout-mini-cart"))))));
//clicking on edit link present in the order Summuary section
_click(_link("Edit",_in(_heading3("ORDER SUMMARY Edit"))));
}
//verify the navigation to Cart page

_assertVisible(_div("cart-actions cart-actions-top"));
_assertVisible(_table("cart-table"));
_assertVisible(_link("/(.*)/",_in(_div("/name/"))));
_assertEqual($productName,_getText(_link("/(.*)/",_in(_div("/name/")))));
//fetch the total items in cart
var $total=_count("_row","/cart-row/",_in(_table("cart-table")));

//product price in cart


for(var $i=0;$i<$total;$i++)
	{	
//updating the quantity
_setSelected(_select("/input-text/",_in(_cell("item-quantity "))),$Shipping_Page[5][5]);
	}
//click on update cart
//_click(_submit("update-cart"));
_wait(5000,_isVisible(_link("mini-cart-link-checkout")));
//navigate to shipping page
_click(_link("mini-cart-link-checkout"));
//quantity in order summary section in shipping page
_assertEqual($Shipping_Page[5][5],_extract(_getText(_div("/mini-cart-pricing bonetext/",_in(_div("checkout-mini-cart")))),"/: (.*) [$]/",true));


//verify the cart updation
//for(var $i=0;$i<$total;$i++)
//{
//_assertEqual($Shipping_Page[5][5],_extract(_getText(_div("/mini-cart-pricing bonetext/",_in(_div("checkout-mini-cart")))),"/: (.*) [$]/",true));
//}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248972","Verify the navigation of 'product name' in order summary Section in right nav on shipping page in Application as a Registered user.");
$t.start();
try
{
//clicking on product name in shipping page
if(!isMobile() || mobile.iPad())
	{
var $productName=_getText(_link("/(.*)/",_in(_div("/mini-cart-name/",_in(_div("secondary"))))));
_click(_link("/(.*)/",_in(_div("/mini-cart-name/",_in(_div("secondary"))))));
	}
else
	{
	var $productName=_getText(_link("/(.*)/",_in(_div("/mini-cart-name/",_in(_div("checkout-mini-cart"))))));
	_click(_link("/(.*)/",_in(_div("/mini-cart-name/",_in(_div("checkout-mini-cart"))))));
	}

_assertVisible(_div("pdpMain"));
_assertVisible(_div("product-primary-image "));
_assertEqual($productName,_getText(_heading1("/product-name/", _in(_div("product-col-2 product-detail")))));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
//
//if(!isMobile())
//{
//var $t = _testcase("248973","Verify the mouse hover link functionality for 'APO/FPO and Why is this required?' on shipping page in Prevail application as a Registered user");
//$t.start();
//try
//{
//
//
////click on go strait to checkout link
//_click(_link("mini-cart-link-checkout"));
//	
////mouse hover on apo/Fpo tool tip
//	 _mouseOver(_link("tooltip",_near(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"))));
//                     //verifying the display of tool tip
//                     if(_getAttribute(_link("tooltip"),"aria-describedby")!=null)
//                           {
//                                  _assert(true);
//                           }
//                     else
//                           {
//                                  _assert(false);
//                           }
//
//
////mouse hovering on why is this required tool tip
//_mouseOver(_link("tooltip",_in(_div("/Why is this required/"))));
//
//                     //verifying the display of tool tip
//                     if(_getAttribute(_link("tooltip[1]"),"aria-describedby")!=null)
//                           {
//                                  _assert(true);
//                           }
//                     else
//                           {
//                                  _assert(false);
//                           }
//                     
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();
//}
//
//var $t = _testcase("248974","Verify the display of 'Details' tool tip in select shipping option Section on shipping page in Application as a Registered user.");
//$t.start();
//try
//{
////verifying the tooltips
//var $totalToolTips=_count("_link","tooltip",_in(_div("shipping-method-list")));
//var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_div("shipping-method-list")));
//_assertEqual($totalShippingMethods,$totalToolTips);
//for(var $i=0;$i<$totalShippingMethods;$i++)
//	{
//	//mousehovering on the tooltips
//    _mouseOver(_link("tooltip",_in(_div("form-row form-indent label-inline["+$i+"]"))));
//    //verifying the display of tool tip
//    if(_getAttribute(_link("tooltip",_in(_div("form-row form-indent label-inline["+$i+"]"))),"aria-describedby")!=null)
//          {
//                 _assert(true);
//          }
//    else
//          {
//                 _assert(false);
//          }
//	}	
//}
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();

var $t = _testcase("248964","Verify the functionality of the Checkbox 'Use this address for Billing' on shipping page in Application as a Registered user.");
$t.start();
try
{
	
//navigate to shipping page
_click(_link("mini-cart-link-checkout"));
//entering the shipping address
shippingAddress($shippingAddress,0);
//check the use this address for billing checkbox
_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));

//verify the address prepopulation in billing page
_assertEqual($shippingAddress[0][1],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
_assertEqual($shippingAddress[0][2],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
_assertEqual($shippingAddress[0][3],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
_assertEqual($shippingAddress[0][5],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
_assertEqual($shippingAddress[0][6],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
_assertEqual($shippingAddress[0][7],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
_assertEqual($shippingAddress[0][8],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_postal")));
_assertEqual($shippingAddress[0][9],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_phone")));
//navigating back to shipping page
_click(_link("Shipping"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248965","Verify the functionality of the Checkbox 'Add to Address Book' on shipping page in Application as a Registered user.");
$t.start();
try
{
//delete if any existing addresses are there
_click(_span("/Hello/",_in(_link("user-account"))));
_click(_link("Address Book"));
while(_isVisible(_link("Delete")))
	{
	_click(_link("Delete"));
	_wait(3000);
	}
//navigate to shipping page
_click(_link("/mini-cart-link-checkout/"));
//enter data in all optinal fields
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), "");
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), "");
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"),"");
_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),"");
_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), "");
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), "");
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), "");
_setValue(_telephonebox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),"");
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"),$Shipping_addr[0][4]);
//click on continue button
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//verify the display
_assert(_submit("dwfrm_singleshipping_shippingAddress_save").disabled);
//enter all required data except optional data
shippingAddress($shippingAddress,0);
//check add to address check box
_check(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook"));
//click on continue button
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));

//verify the navigation to billing page
_assertVisible(_fieldset("/"+$Shipping_Page[0][3]+"/"));
_assertVisible(_submit("dwfrm_billing_save"));
//navigate to my account page
_assertVisible(_link("user-account"));
_click(_link("My Account"));
_click(_link("Addresses"));
//verify the presence of saved address
_assertVisible(_listItem("address-tile  default"));
_assertVisible(_div("/mini-address-location/"));
var $savedAddress=$shippingAddress[0][7]+" "+$shippingAddress[0][1]+" "+$shippingAddress[0][2]+" "+$shippingAddress[0][3]+" "+$shippingAddress[0][4]+" "+$shippingAddress[0][7]+" "+$shippingAddress[0][11]+" "+$shippingAddress[0][8]+" "+$shippingAddress[0][5]+" "+"Phone: "+$shippingAddress[0][9];
var $adress=$shippingAddress[0][7]+" "+$shippingAddress[0][1]+" "+$shippingAddress[0][2]+" "+_getText(_div("/mini-address-location/")).replace(",","");
_assertEqual($savedAddress,$adress);
//deleting the address
_click(_link("Delete"));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248963","Verify the functionality of 'Select an Address:' drop down from Checkout Shipping page in Application as a Registered  user.");
$t.start();
try
{
	
//navigate address page
_click(_link("MY ACCOUNT"));
_click(_link("Addresses"));
//deleting the existing addresses
deleteAddress();
//create new address
_click(_link("Create New Address"));
//adding address
addAddress($Address,0);
_click(_submit("dwfrm_profile_address_create"));
_wait(3000);
//navigate to shipping page
_click(_link("mini-cart-link-checkout"));
//verify the drop down
_assertVisible(_fieldset($Shipping_Page[11][0]));
_assertVisible(_label($Shipping_Page[7][0]));
//_assertVisible(_select($Shipping_Page[7][0]));
_assertVisible(_select("dwfrm_singleshipping_addressList"));
//selecting the address from drop down
_setSelected(_select("dwfrm_singleshipping_addressList"), "1");
//verify the pre population
var $defaultAddress=new Array();
$defaultAddress=$Address[0][2]+$Address[0][3]+$Address[0][4]+$Address[0][5]+$Address[0][6]+$Address[0][7]+$Address[0][8]+$Address[0][9]+$Address[0][10]+$Address[0][13];
var $address=new Array();
$address=[_getText(_textbox("/firstName/")),_getText(_textbox("/lastName/")),_getText(_textbox("/address1/")),_getText(_textbox("/Address 2/")),_getText(_textbox("/city/")),_getText(_textbox("/postal/")),_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_country")),_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")),_getText(_telephonebox("dwfrm_singleshipping_shippingAddress_addressFields_phone"))];
for(var $i=0;$i<$address.length;$i++)
	{
	if($defaultAddress.indexOf($address[$i])>=0)
		{
			_assert(true, $address[$i]+" data is Prepopulated");
		}
	else
		{
		_assert(false, $address[$i]+" data is not Prepopulated");
		}
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("248988","Verify the validation of 'Country' field on shipping page in Application as a Registered user.");
$t.start();
try
{

//selecting US from drop down
var $countries=_getText(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"));
for(var $i=0;$i<4;$i++)
  {
	_assertEqual($countries[$i],$Shipping_Page[$i][4]);
	//selecting US from drop down
	if($i==1)
		{
		_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$Shipping_Page[$i][4]);
		//verify the states present in states drop down
			for(var $j=0;$j<$Shipping_Page[5][5];$j++)
			{
			_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"),$Shipping_Page[$j][5]);
			//verifying the selection
			_assertEqual($Shipping_Page[$j][5], _getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
			}
		}
  }

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase("248993","Verify the functionality of 'CONTINUE &gt;' button on shipping page in Application as a Registered user.");
$t.start();
try
{
	
//click directly on continue link with out entering data
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//should not navigate to billing page
_assertNotVisible(_fieldset("/"+$Shipping_Page[0][3]+"/"));
_assertVisible(_submit("dwfrm_billing_save"));
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/

var $t = _testcase("248969/248985/248986/248987/248989/248990/248991/248966/248992/248993/248991  .............","Validate the fields in Checkout like firstname/  Single Shipping");
$t.start();
try
{
//validating the fields
for(var $i=0;$i<$Validation.length;$i++)
	{
	if($i==0)
	{
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"),"");
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"),"");
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"),"");
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"),"");
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),"");
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), "");
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"),"");
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), "");
	_setValue(_telephonebox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),"");
	}
else
	{
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), $Validation[$i][1]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), $Validation[$i][2]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"), $Validation[$i][3]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"),$Validation[$i][4]);
_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$Validation[$i][5]);
_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), $Validation[$i][6]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), $Validation[$i][7]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $Validation[$i][8]);
_setValue(_telephonebox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),$Validation[$i][9]);
	}
	//click on continue button
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//with blank data
if($i==0)
	{	
	_assert(_submit("dwfrm_singleshipping_shippingAddress_save").disabled);
	}
//checking the max length
if($i==1)
	{
	//Fname,Lname Should not highlight
	_assertNotEqual($Validation[0][10],_style(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"),"background-color"));
	_assertNotEqual($Validation[0][10],_style(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"),"background-color"));
	//for address label verification
	_assertVisible(_label("/"+$Shipping_Page[8][0]+"/"));
	_assertVisible(_label("/"+$Shipping_Page[9][0]+"/"));
	//verifying the length of field's first name,last name,add1,addr2,phone number,City 
	_assertEqual($Validation[1][10],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName")).length);
	_assertEqual($Validation[1][10],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName")).length);
	_assertEqual($Validation[1][10],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1")).length);
	_assertEqual($Validation[1][10],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2")).length);
	_assertEqual($Validation[1][10],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")).length);
	_assertEqual($Validation[0][11],_getText(_telephonebox("dwfrm_singleshipping_shippingAddress_addressFields_phone")).length);
	//Zip Code max length
	_assertEqual($Validation[1][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")).length); 
	}
//numeric,special chars,combination of both,alphabets data in city,phone number,Zip code field's
if($i==2 || $i==3 || $i==4 || $i==5)
	{
	//Fname,Lname Should not highlight
	_assertNotEqual($Validation[0][10],_style(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"),"background-color"));
	_assertNotEqual($Validation[0][10],_style(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"),"background-color"));
	//Zip Code
	_assertEqual($Validation[6][10],_style(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"),"background-color")); 
	_assertEqual($Validation[4][10],_style(_span($Validation[2][11]),"color"));
	_assertVisible(_span($Validation[2][11])); 
	//Phone Number
	_assertEqual($Validation[6][10],_style(_telephonebox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),"background-color"));
	_assertVisible(_span($Validation[2][10])); 
	_assertEqual($Validation[4][10],_style(_span($Validation[2][10]),"color")); 
	//City
	//In Progress
	
	}
//state field validation
if($i==6)
	{
	_assert(_submit("dwfrm_singleshipping_shippingAddress_save").disabled);
	}
//with valid data
if($i==7)
	{
	//Should navigate to billing page
	_assertVisible(_fieldset("/"+$Shipping_Page[0][3]+"/"));
	_assertVisible(_submit("dwfrm_billing_save"));
	}


}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

