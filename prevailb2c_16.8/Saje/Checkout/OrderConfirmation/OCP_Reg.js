_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("OrderConfirmation.xls");

var $OCP=_readExcelFile("OrderConfirmation.xls","OCP");
var $creditcard_data=_readExcelFile("OrderConfirmation.xls","Payment");
var $Address=_readExcelFile("OrderConfirmation.xls","Address");
var $cardDetails=_readExcelFile("OrderConfirmation.xls","CardDetails");

var $shippingTax;
var $tax;
var $subTotal;
var $Ordertotal;
var $paymentType=$OCP[0][9];

var $sheetName;
var $rowNum;

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();
//click on My Account link	
//_click(_span("My Account"));
login();
_wait(3000);

ClearCartItems(); 
var $Address1=$Address[0][1]+" "+$Address[0][2]+" "+$Address[0][3]+" "+$Address[0][4]+" "+$Address[0][7]+" "+$Address[1][6]+" "+$Address[0][8]+" "+$Address[0][5]+" "+$Address[0][9];
var $shippingAddress="Shipping To: "+$Address1;
//var $billingAddress="Billing Address "+$Address[0][1]+" "+$Address[0][2]+" "+$Address[0][3]+" "+$Address[0][4]+" "+$Address[0][7]+" "+$Address[1][6]+" "+$Address[0][8]+" "+$Address[0][5]+" "+"Phone: "+$Address[0][9];
var $billingAddress="Billing Address "+$Address[0][1]+" "+$Address[0][2]+" "+$Address[0][3]+" "+$Address[0][4]+" "+$Address[0][7]+" "+$Address[1][6]+" "+$Address[0][8]+" "+$Address[0][5];

var $t=_testcase("249145/249131","Verify the navigation to/UI of 'Thank you for your order' page in Laura Mercier application as a registered user/Verify the UI of the 'Order summery' Section on Receipt Page  in Application as a Registered user.");
$t.start();
try
{

	//navigating to billing page
	navigateToCart($OCP[0][8],1);
	_click(_submit("dwfrm_cart_checkoutCart"));
	_log($QuantityInCart);
	_log($prodpriceincart);
	//check use this for billing check box
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	shippingAddress($Address,0);
	if(isMobile())
	{
	$shippingTax=_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true).toString();	
	$subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
	}
	else
	{
	$shippingTax=_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true).toString();
	$subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
	}
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	$tax=_extract(_getText(_cell("order-sale-value")),"/[$](.*)/",true).toString();
	$Ordertotal=_extract(_getText(_cell("order-value")),"/[$](.*)/",true).toString();
	//entering Credit card Information
	PaymentDetails($cardDetails,0);
	var $orderTot=_getText(_cell("order-value"));
	//click on Place Order
	_click(_submit("/button-fancy-large/"));
	
	//verify the navigation to OCP page
	if(_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
	
	//click on Place Order
	_click(_submit("/button-fancy-large/"));
	
	//verify the navigation to OCP page
	if(_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
	//verify the navigation to OCP page
	_assertVisible(_heading1($OCP[0][0]));
	_assertVisible(_div("content-asset"));

	_assertVisible(_span($OCP[0][7]));
	_assertVisible(_span($OCP[1][7]));
	_assertVisible(_span("value", _near(_span($OCP[0][7]))));
	_assertVisible(_span("/value/",_near(_span($OCP[1][7]))));
	
	//fetching order number
	var $orderNumber=_extract(_getText(_heading3("order-number")),"/: (.*)/",true);
	_log("Order number is ="+$orderNumber);
	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("249130","Verify the UI of the 'Shipment' Section on Receipt Page  in Application as a Registered user.");
$t.start();
try
{
	
//verifying the UI of Shipment section
_assertVisible(_heading3($OCP[0][4]));
	
//if(!isMobile() || mobile.iPad())
//{
//_assertVisible(_tableHeader($OCP[1][4],_in(_table("order-shipment-table"))));
//_assertVisible(_tableHeader($OCP[2][4]));
//_assertVisible(_tableHeader($OCP[3][4]));
//_assertVisible(_tableHeader($OCP[4][4]));
//}
//product name
_assertVisible(_link("/(.*)/", _in(_div("/name/", _in(_div("product-list-item"))))));
//item number
_assertVisible(_div($OCP[0][3]));
_assertVisible(_span("/value/",_in(_div("/sku/"))));
_assertVisible(_div("/sku/"));
//full product details div
_assertVisible(_div("product-list-item"));

if(!isMobile())
{
	_assertVisible(_div("/product-list-item/",_near(_div("Qty"))));
	_assertVisible(_div("/product-list-item/",_near(_div("Price"))));
}
//verifying the shipping address
_assertVisible(_div("order-shipment-address"));
_assertEqual($shippingAddress,_getText(_div("order-shipment-address")).replace(",",""));
//return shopping
//_assertVisible(_link($OCP[5][4]));
//shipping status and method
//shipping status
_assertVisible(_div($OCP[2][7]));
_assertVisible(_div("/value/",_in(_div("shipping-status"))));
//shipping method
_assertVisible(_div($OCP[3][7]));
_assertVisible(_div("/value/",_in(_div("shipping-method"))));
//entire div
_assertVisible(_div("order-shipment-table"));
	

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148897/140353","Verify the details to be displayed in the 'PAYMENT METHOD' section on Thank you for your order page in Laura Mercier application as a registered user");
$t.start();
try
{
	
	_assertVisible(_div("order-payment-instruments"));
	_assertVisible(_div("/payment-type/"));
	_assertEqual($paymentType, _getText(_div("/payment-type/")));

	var $paymentDetails=new Array();
	$paymentDetails=_getText(_div("order-payment-instruments")).split(" ");
	var $num=$paymentDetails[4].split("*")[12];

	_assertVisible(_div("order-payment-instruments"));
	_assertEqual($cardDetails[0][9]+" "+$orderTot, _getText(_div("order-payment-instruments")));

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("249148","Verify the display of the 'PAYMENT TOTAL' Section on Receipt Page  in Application as a Registered user.");
$t.start();
try
{
	
	//payment total heading
	_assertVisible(_div("Payment Total"));
	//all other values
	_assertVisible(_cell($OCP[1][1]));
	_assertVisible(_cell($OCP[2][1]));
	_assertVisible(_cell("/"+$OCP[3][1]+"/"));
	_assertVisible(_cell($OCP[4][1]));
	_assertEqual($subTotal,_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString());
	_assertEqual($shippingTax,_extract(_getText(_row("order-shipping")),"/[$](.*)/",true).toString());
	_assertEqual($tax, _extract(_getText(_cell("order-sale-value")), "/[$](.*)/", true).toString());
	_assertEqual($Ordertotal,_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("249133","Verify the details to be displayed in the 'SHIPPING TO' Section on Receipt Page  in Application as a Registered user.");
$t.start();
try
{
	
	//verifying the shipping address
	_assertEqual($shippingAddress,_getText(_div("order-shipment-address")).replace(",",""));

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("249143","Verify the details to be displayed in the 'BILLING ADDRESS' section on Thank you for your order page in Laura Mercier application as a registered user");
$t.start();
try
{
	
	//verifying the billing address
	_assertEqual($billingAddress,_getText(_div("order-billing")).replace(",",""));

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("249134","Verify the details to be displayed for product line 'ITEM' on Receipt Page  in Application as a Registered user.");
$t.start();
try
{
	
	//verifying the line item
	_assertVisible(_link("/(.*)/",_in(_div("/name/",_in(_div("product-list-item"))))));
	_assertEqual($pName, _getText(_link("/(.*)/",_in(_div("/name/",_in(_div("product-list-item")))))));
	//_assertVisible(_span($OCP[0][3]));
	_assertVisible(_span("/value/",_in(_div("/sku/")))); 
	if(isMobile())
		{
		_assertVisible(_cell($QuantityInCart));
		_assertVisible(_cell("/"+$prodpriceincart+"/"));
		}
	else
		{
		_assertVisible(_div("/product-list-item/",_near(_div("Qty"))));
		_assertEqual($QuantityInCart,_getText(_div("/product-list-item/",_near(_div("Qty")))));
		_assertVisible(_div("/product-list-item/",_near(_div("Price"))));
		_assertEqual($prodpriceincart,_getText(_div("/product-list-item/",_near(_div("Price")))));
		}
	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("249131","Verify the functionality of Product name  on Receipt Page  in Application as a Registered user.");
$t.start();
try
{
	
	//clicking on product name link
	var $pName=_getText(_link("/(.*)/",_in(_div("/name/",_in(_div("product-list-item"))))));
	_click(_link("/(.*)/",_in(_div("/name/",_in(_div("product-list-item"))))));	
	//verifying the products
	_assertVisible(_heading1("/product-name/", _in(_div("product-col-2 product-detail"))));
	_assertEqual($pName, _getText(_heading1("/product-name/", _in(_div("product-col-2 product-detail")))));
	if(isMobile())
	{
		_assertVisible(_link("user-account"));
	}
	else
		{
			_assertVisible(_link("user-account"));
		}

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


/*var $t=_testcase("249132/126731/249145","Verify the functionality of 'Return to Shopping' button on Receipt Page  in Application as a Registered user.  Note: Verify the same for guest user also.");
$t.start();
try
{
	
	//navigating to OCP 
	//navigating to Shipping page
	navigateToShippingPage($OCP[0][8],1);
	//check use this for billing check box
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"))
	shippingAddress($Address,0);
	if(isMobile())
	{
	$shippingTax=_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true).toString();	
	$subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
	}
	else
	{
	$shippingTax=_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true).toString();
	$subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
	}
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));

	$tax=_extract(_getText(_cell("order-sale-value")),"/[$](.*)/",true).toString();
	$Ordertotal=_extract(_getText(_cell("order-value")),"/[$](.*)/",true).toString();
	//entering Credit card Information
	PaymentDetails($cardDetails,1);
	
	//click on Place Order
	_click(_submit("/button-fancy-large/"));
	
	//verify the navigation to OCP page
	if (_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
	
	
	//click on Place Order
	_click(_submit("/button-fancy-large/"));
	
	//verify the Order number
	//_assertVisible(_span("value",_rightOf(_span($OCP[1][7]))));
	_assertVisible(_span("/value/",_near(_span($OCP[1][7]))));  
	
	//fetching order number
	var $orderNum=_extract(_getText(_heading3("order-number")),"/: (.*)/",true);
	_log("Order number is ="+$orderNum);
	
	//click on returning customer link
	_click(_link("Return to Shopping"));
	//verify the navigation to PDP Page(need clarification)
	_assertVisible(_heading1("/product-name/", _in(_div("product-col-2 product-detail"))));
	_assertEqual($pName,_getText(_heading1("/product-name/", _in(_div("product-col-2 product-detail")))));
	//navigate to My account and verify the Order Number
	_click(_link("MY ACCOUNT"));
	_click(_link("Orders"));
	//verifying the order details
	//_assertVisible(_span("value",_rightOf(_span($OCP[1][7]))));
	_assertVisible(_span("/value/", _in(_div("/order-number/"))));
	//_assertEqual($orderNum,_getText(_span("value",_rightOf(_span($OCP[2][7])))));
	
	_assertEqual($orderNum,_getText(_span("/value/", _in(_div("/order-number/")))));
	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();*/

