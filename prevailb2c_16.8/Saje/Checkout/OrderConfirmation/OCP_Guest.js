_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("OrderConfirmation.xls");


var $creditcard_data=_readExcelFile("OrderConfirmation.xls","Payment");
var $OCP=_readExcelFile("OrderConfirmation.xls","OCP");
var $Address=_readExcelFile("OrderConfirmation.xls","Address");
var $Validation=_readExcelFile("OrderConfirmation.xls","Validation");
var $cardDetails=_readExcelFile("OrderConfirmation.xls","CardDetails");

deleteProvidedUser("test@rev.com");

var $shippingTax;
var $tax;
var $subTotal;
var $Ordertotal;
var $paymentType=$OCP[0][9];

var $sheetName;
var $rowNum;

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

var $Address1=$Address[0][1]+" "+$Address[0][2]+" "+$Address[0][3]+" "+$Address[0][4]+" "+$Address[0][7]+" "+$Address[1][6]+" "+$Address[0][8]+" "+$Address[0][5]+" "+$Address[0][9];
var $shippingAddress="Shipping To: "+$Address1;
//var $billingAddress="Billing Address "+$Address[0][1]+" "+$Address[0][2]+" "+$Address[0][3]+" "+$Address[0][4]+" "+$Address[0][7]+" "+$Address[1][6]+" "+$Address[0][8]+" "+$Address[0][5]+" "+"Phone: "+$Address[0][9];
var $billingAddress="Billing Address "+$Address[0][1]+" "+$Address[0][2]+" "+$Address[0][3]+" "+$Address[0][4]+" "+$Address[0][7]+" "+$Address[1][6]+" "+$Address[0][8]+" "+$Address[0][5];

var $t=_testcase("249135/249154/249129","Verify the UI of the 'Order summery' Section on Receipt Page  in Application as a Anonymous user.");
$t.start();
try{
//navigating to Shipping page
navigateToCart($OCP[0][8],1);
_click(_submit("dwfrm_cart_checkoutCart"));
_click(_submit("dwfrm_login_unregistered")); 

shippingAddress($Address,0);
//check use this for billing check box
_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
if(isMobile() && !mobile.iPad())
{
$shippingTax=_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true).toString();	
$subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
}
else
{
$shippingTax=_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true).toString();
$subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
}
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//Billing address
BillingAddress($Address,0); 

$tax=_extract(_getText(_cell("order-sale-value")),"/[$](.*)/",true).toString();
$Ordertotal=_extract(_getText(_cell("order-value")),"/[$](.*)/",true).toString();

//entering Credit card Information
PaymentDetails($cardDetails,1);
//fetch order total
var $orderTot=_getText(_cell("order-value"));
//Placing order
_click(_submit("/button-fancy-large/"));

if(_isVisible(_image("CloseIcon.png")))
{
_click(_image("CloseIcon.png"));
}


//click on Place Order
_click(_submit("/button-fancy-large/"));

//verify the navigation to OCP page
_assertVisible(_heading1($OCP[0][0]));
_assertVisible(_div("content-asset"));

//verify the UI
_assertVisible(_span($OCP[0][7]));
_assertVisible(_span($OCP[1][7]));
_assertVisible(_span("value", _near(_span($OCP[0][7]))));
_assertVisible(_span("/value/",_near(_span($OCP[1][7])))); 

//fetching order number
var $orderNumber=_extract(_getText(_heading3("order-number")),"/: (.*)/",true);
_log("Order number is ="+$orderNumber);

}catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("249146","Verify the UI of the 'Shipment' Section on Receipt Page  in Application as a Anonymous user.");
$t.start();
try
{
	
	//verifying the UI of Shipment section
	_assertVisible(_heading3($OCP[0][4]));
	
//if(!isMobile() || mobile.iPad())
//{
//_assertVisible(_tableHeader($OCP[1][4],_in(_table("order-shipment-table"))));
//_assertVisible(_tableHeader($OCP[2][4]));
//_assertVisible(_tableHeader($OCP[3][4]));
//_assertVisible(_tableHeader($OCP[4][4]));
//}
//product name
_assertVisible(_link("/(.*)/", _in(_div("/name/", _in(_div("product-list-item"))))));
//item number
_assertVisible(_div($OCP[0][3]));
_assertVisible(_span("/value/",_in(_div("/sku/"))));
_assertVisible(_div("/sku/"));
//full product details div
_assertVisible(_div("product-list-item"));

if(!isMobile() || mobile.iPad())
{
	_assertVisible(_div("/product-list-item/",_near(_div("Qty"))));
	_assertVisible(_div("/product-list-item/",_near(_div("Price"))));
}
//verifying the shipping address
_assertVisible(_div("order-shipment-address"));
_assertEqual($shippingAddress,_getText(_div("order-shipment-address")).replace(",",""));
//return shopping
//_assertVisible(_link($OCP[5][4]));
//shipping status and method
//shipping status
_assertVisible(_div($OCP[2][7]));
_assertVisible(_div("/value/",_in(_div("shipping-status"))));
//shipping method
_assertVisible(_div($OCP[3][7]));
_assertVisible(_div("/value/",_in(_div("shipping-method"))));
//entire div
_assertVisible(_div("order-shipment-table"));

}catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

	
var $t=_testcase("249150","Verify the details to be displayed in the 'SHIPPING TO' Section on Receipt Page  in Application as a Anonymous user.");
$t.start();
try
{
	
	//verifying the shipping address
	_assertEqual($shippingAddress,_getText(_div("order-shipment-address")).replace(",",""));

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("249151","Verify the details to be displayed in the 'BILLING ADDRESS' Section on Receipt Page  in Application as a Anonymous user.");
$t.start();
try
{

	//verifying the billing address
	_assertEqual($billingAddress,_getText(_div("order-billing")).replace(",",""));
	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("249144/249152","Verify the details to be displayed in the 'PAYMENT METHOD' Section on Receipt Page  in Application as a Registered user.");
$t.start();
try
{

_assertVisible(_div("order-payment-instruments"));
_assertVisible(_div("/payment-type/"));
_assertEqual($paymentType, _getText(_div("/payment-type/")));

var $paymentDetails=new Array();
$paymentDetails=_getText(_div("order-payment-instruments")).split(" ");
var $num=$paymentDetails[4].split("*")[12];

_assertVisible(_div("order-payment-instruments"));
_assertEqual($cardDetails[1][9]+" "+$orderTot, _getText(_div("order-payment-instruments")));


}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("249153","Verify the display of the 'PAYMENT TOTAL' Section on Receipt Page  in Application as a Anonymous user.");
$t.start();
try
{

	//payment total heading
	_assertVisible(_div("Payment Total"));
	//all other values
	_assertVisible(_cell($OCP[1][1]));
	_assertVisible(_cell($OCP[2][1]));
	_assertVisible(_cell("/"+$OCP[3][1]+"/"));
	_assertVisible(_cell($OCP[4][1]));
	_assertEqual($subTotal,_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString());
	_assertEqual($shippingTax,_extract(_getText(_row("order-shipping")),"/[$](.*)/",true).toString());
	_assertEqual($tax, _extract(_getText(_cell("order-sale-value")), "/[$](.*)/", true).toString());
	_assertEqual($Ordertotal,_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("249149","Verify the details to be displayed for product line 'ITEM' on Thank you for your order page in Laura Mercier application as a Guest user");
$t.start();
try
{
	
//verifying the line item
_assertVisible(_link("/(.*)/",_in(_div("/name/",_in(_div("product-list-item"))))));
_assertEqual($pName, _getText(_link("/(.*)/",_in(_div("/name/",_in(_div("product-list-item")))))));
//_assertVisible(_span($OCP[0][3]));
_assertVisible(_span("/value/",_in(_div("/sku/")))); 
if(isMobile() && !mobile.iPad())
	{
	_assertVisible(_cell($QuantityInCart));
	_assertVisible(_cell("/"+$prodpriceincart+"/"));
	}
	else
		{
		_assertVisible(_div("/product-list-item/",_near(_div("Qty"))));
		_assertEqual($QuantityInCart,_getText(_div("/product-list-item/",_near(_div("Qty")))));
		_assertVisible(_div("/product-list-item/",_near(_div("Price"))));
		_assertEqual($prodpriceincart,_getText(_div("/product-list-item/",_near(_div("Price")))));
		}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("249137","Verify the navigation of the 'Product name' link on Thank you for your order page in Laura Mercier application as a Guest user");
$t.start();
try
{
	
	//clicking on product name link
	var $pName=_getText(_link("/(.*)/",_in(_div("/name/",_in(_div("product-list-item"))))));
	_click(_link("/(.*)/",_in(_div("/name/",_in(_div("product-list-item"))))));	
	//verifying the products
	_assertVisible(_heading1("/product-name/", _in(_div("product-col-2 product-detail"))));
	_assertEqual($pName, _getText(_heading1("/product-name/", _in(_div("product-col-2 product-detail")))));

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("249140/249142/249155/249141/249136","Verify the field validations for First Name/last name/email/confirm email/password/confirm password & valid data text box in right nav & 'Order Number' on Thank you for your order page in Laura Mercier application as a Guest user");
$t.start();
try
{
	
	//navigating to OCP 
	//navigating to Shipping page
	navigateToShippingPage($OCP[0][8],1);
	shippingAddress($Address,0);
	//check use this for billing check box
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	if(isMobile() && !mobile.iPad())
	{
	$shippingTax=_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true).toString();	
	$subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
	}
	else
	{
	$shippingTax=_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true).toString();
	$subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
	}
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	BillingAddress($Address,0);
	$tax=_extract(_getText(_cell("order-sale-value")),"/[$](.*)/",true).toString();
	$Ordertotal=_extract(_getText(_cell("order-value")),"/[$](.*)/",true).toString();
	//PaymentDetails($Valid_Data,4);
    PaymentDetails($cardDetails,2);
	
	//click on Place Order
	_click(_submit("/button-fancy-large/"));
	
	if(_isVisible(_image("CloseIcon.png")))
	{
	_click(_image("CloseIcon.png"));
	}
	
	
	//click on Place Order
	_click(_submit("/button-fancy-large/"));
	
	//verify the Order Number
	_assertVisible(_span("/value/",_near(_span($OCP[1][7])))); 
	//fetching order number
	var $orderNumber=_extract(_getText(_heading3("order-number")),"/: (.*)/",true);
	_log("Order number is ="+$orderNumber);

//validate the fields
for(var $i=0;$i<$Validation.length;$i++)
	{
		if($i==0)
			{
				_setValue(_textbox("dwfrm_profile_customer_firstname"),"");
				_setValue(_textbox("dwfrm_profile_customer_lastname"),"");
				_setValue(_textbox("dwfrm_profile_customer_email"),"");
				_setValue(_password("/dwfrm_profile_login_password/"), "");
			}
		else
			{
				_setValue(_textbox("dwfrm_profile_customer_firstname"),$Validation[$i][1]);
				_setValue(_textbox("dwfrm_profile_customer_lastname"),$Validation[$i][2]);
			}
		//valid
		if($i==9)
			{
				_setValue(_textbox("dwfrm_profile_customer_email"),$email);
			}
		//invalid
		else
			{
				_setValue(_textbox("dwfrm_profile_customer_email"),$Validation[$i][3]);
			}
		_setValue(_password("/dwfrm_profile_login_password/"),$Validation[$i][5]);        
		_click(_submit("dwfrm_profile_confirm"));
		//with Blank Data
	if($i==0)
		{
		var $Firstname = _style(_textbox("dwfrm_profile_customer_firstname"), "background-color");
		_assertEqual($Validation[0][7], $Firstname);
	
		var $Lastname = _style(_textbox("dwfrm_profile_customer_lastname"), "background-color");
		_assertEqual($Validation[0][7], $Lastname)
	   
		var $Email = _style(_textbox("/dwfrm_profile_customer_email/"), "background-color");
		_assertEqual($Validation[0][7], $Email);
	
		var $Password = _style(_password("/dwfrm_profile_login_password/"), "background-color");
		_assertEqual($Validation[0][7], $Password);
	
		}
	//morethan 50 characters
	else if($i==1)
		{
		_assertEqual($Validation[1][7],_getText(_textbox("dwfrm_profile_customer_firstname")).length);
		_assertEqual($Validation[1][7],_getText(_textbox("dwfrm_profile_customer_lastname")).length);
		_assertEqual($Validation[1][7],_getText(_textbox("dwfrm_profile_customer_email")).length);
		_assertEqual($Validation[1][8],_getText(_password("/dwfrm_profile_login_password/")).length);
		}
	//numeric,alphabets,special characters,Combinations values in email and confirm email
	else if($i==2 || $i==4 || $i==5 || $i==6)
		{
		//Email ID
		_assertEqual($Validation[2][8],_style(_span($Validation[2][7]),"color"));
		_assertVisible(_span($Validation[2][7]));
		//should not highlight first name and last name fields
		_assertNotEqual($Validation[1][7],_getText(_textbox("dwfrm_profile_customer_firstname")).length);
		_assertNotEqual($Validation[1][7],_getText(_textbox("dwfrm_profile_customer_lastname")).length);
		}
	//less than 5 characters in pwd and confirm pwd fields
	else if($i==3)
		{
		_assertEqual($Validation[4][7], _getText(_div("form-caption error-message")));
		_assertEqual($Validation[2][8],_style(_span("Email"),"color"));
		}
	//valid data
	else if($i==7)
		{
		//verify the navigation to Profile page
		_assertVisible(_span("/Hello/",_in(_link("user-account"))));
		//verifying whether user is logged in or not
		if(isMobile() && !mobile.iPad())
			{
				_assertVisible(_span("/Hello/",_in(_link("user-account"))));
				_assertVisible(_link("Logout"));
			}
		else
			{
				_assertVisible(_span("/Hello/",_in(_link("user-account"))));
			}
		}
}

//click on hello link
_click(_span("/Hello/",_in(_link("user-account"))));
//verify the Order Number in Order History
_click(_link("Order history"));
//verifying the order details
_assertVisible(_span("/value/", _in(_div("order-number")))); 
_assertEqual($orderNumber,_getText(_span("/value/", _in(_div("order-number"))))); 
//logout from the application
_click(_span("/Hello/",_in(_link("user-account"))));
_click(_link("/user-logout/"));

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

/*var $t=_testcase("249147","Verify the functionality of 'Return to Shopping' button on Receipt Page  in Application as a Anonymous user.");
$t.start();
try
{
	
	//navigating to OCP 
	//navigating to Shipping page
	navigateToShippingPage($OCP[0][8],1);
	shippingAddress($Address,0);
	//check use this for billing check box
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	if(isMobile() && !mobile.iPad())
	{
	$shippingTax=_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true).toString();	
	$subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
	}
	else
	{
	$shippingTax=_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true).toString();
	$subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
	}
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//billing adress
	BillingAddress($Address,0);
	$tax=_extract(_getText(_cell("order-sale-value")),"/[$](.*)/",true).toString();
	$Ordertotal=_extract(_getText(_cell("order-value")),"/[$](.*)/",true).toString();
	//entering Credit card Information
	PaymentDetails($cardDetails,3);
	
	//click on Place Order
	_click(_submit("/button-fancy-large/"));
	
	//verify the navigation to OCP page
	if(_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
	
	
	//click on Place Order
	_click(_submit("/button-fancy-large/"));
	
	var $productnameinOCP=_getText(_link("/(.*)/", _in(_div("name hone-header"))));
	
	//click on returning customer link
	_click(_link("Return to Shopping"));
	//verify the navigation to PDP Page(need clarification)
	_assertVisible(_heading1("/product-name/", _in(_div("product-col-2 product-detail"))));
	
	var $prodnameinPDP=_getText(_heading1("/product-name/", _in(_div("product-col-2 product-detail"))));
	
	_assertEqual($productnameinOCP,$prodnameinPDP);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();*/
