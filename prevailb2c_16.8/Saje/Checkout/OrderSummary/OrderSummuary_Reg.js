_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("OrderSummuary.xls");

var $OrderSummuary=_readExcelFile("OrderSummuary.xls","OrderSummuary");
var $Valid_Data=_readExcelFile("OrderSummuary.xls","Valid_Data");
var $Address=_readExcelFile("OrderSummuary.xls","Address");


var $sheetName;
var $rowNum;

	
SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();
login();
_wait(3000);
//clear cart items 
ClearCartItems();

//var $billingAddress=$Valid_Data[0][1]+" "+$Valid_Data[0][2]+" "+$Valid_Data[0][3]+" "+$Valid_Data[0][4]+" "+$Valid_Data[0][7]+" "+$Valid_Data[1][6]+" "+$Valid_Data[0][8]+" "+$Valid_Data[0][9]+" "+$Valid_Data[0][5];
var $billingAddress=$Valid_Data[0][1]+" "+$Valid_Data[0][2]+" "+$Valid_Data[0][3]+" "+$Valid_Data[0][4]+" "+$Valid_Data[0][7]+" "+$Valid_Data[1][6]+" "+$Valid_Data[0][8]+" "+$Valid_Data[0][5];
var $shippingAddress=$billingAddress+" "+"Shipping Method: "+$OrderSummuary[0][1]+" "+$OrderSummuary[1][2];

var $t=_testcase("249098/249099","Verify the UI of  'Place Order' Page  in Application as a Registered user for  Province having single tax./Verify the UI of  'Place Order' Page  in Application as a Registered user for  Province having multiple taxes.");
$t.start();
try
{
	_assert(false,"249099 to check this regarding Verify the UI of  'Place Order' Page  in Application as a Registered user for  Province having multiple taxes.");
	//Navigation till shipping page
	navigateToShippingPage($OrderSummuary[0][7],$OrderSummuary[0][8]);
	shippingAddress($Valid_Data,0);
	if(isMobile() && !mobile.iPad())
	{
	$shippingTax=_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true).toString();	
	$subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
	}
		else
		{
		$shippingTax=_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true).toString();
		$subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
		}
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));

	BillingAddress($Valid_Data,0); 
	
	$tax=_extract(_getText(_cell("order-sale-value")),"/[$](.*)/",true).toString();
	$Ordertotal=_extract(_getText(_cell("order-value")),"/[$](.*)/",true).toString();
	var $totalorderprice=_getText(_cell("order-value"));
	
	//entering Credit card Information
	PaymentDetails($Valid_Data,4);
	
	//navigate to order summary page
	_click(_submit("/button-fancy-large/"));

	if(!isMobile() || mobile.iPad())
		{
	//varify the navigation & UI of Order Summuary
	_assertVisible(_tableHeader($OrderSummuary[0][0]));
	_assertVisible(_tableHeader($OrderSummuary[2][0]));
		}
	//image
	_assertVisible(_cell("item-image"));
	//name
	_assertVisible(_link("/(.*)/",_in(_div("name hone-header"))));
	//qty
	_assertVisible(_div("product-quantity-value qty-desktop-only", _in(_row("cart-row first "))));
	//item num
	_assertVisible(_span($OrderSummuary[4][0]));
	_assertVisible(_span("value",_near(_span("/sku/"))));
	
	//total price
	_assertVisible(_row("order-subtotal", _in(_div("place-order-totals"))));
	//edit link
	_assertVisible(_link("My Bag", _in(_div("breadcrumb bonetext checkout-progress-indicator"))));
	//summuary section
	_assertVisible(_cell("Subtotal", _in(_div("place-order-totals"))));
	_assertVisible(_cell("Shipping", _in(_div("place-order-totals"))));
	_assertVisible(_cell("Taxes", _in(_div("place-order-totals"))));
	_assertVisible(_cell($OrderSummuary[5][0], _in(_div("place-order-totals"))));

	_assertVisible(_row("order-subtotal", _in(_div("place-order-totals"))));
	_assertVisible(_row("order-shipping", _in(_div("place-order-totals"))));
	_assertVisible(_row("/order-sales-tax/", _in(_div("place-order-totals"))));
	_assertVisible(_row("order-total", _in(_div("place-order-totals"))));
	_assertVisible(_submit("Place Order"));
	
	//Order Summuary 
	_assertVisible(_heading3($OrderSummuary[6][0]));
	//shipping address
	_assertVisible(_heading3($OrderSummuary[7][0]));
	_assertVisible(_div("details",_near(_heading3($OrderSummuary[7][0]))));
	//billing address
	_assertVisible(_heading3($OrderSummuary[8][0]));
	_assertVisible(_div("details",_near(_heading3($OrderSummuary[8][0]))));
	//payment details
	_assertVisible(_heading3($OrderSummuary[9][0]));
	_assertVisible(_div("details",_near(_heading3($OrderSummuary[9][0]))));
	
//	if(!_isSafari())
//		{
//		_assertVisible(_heading3($OrderSummuary[9][1]));
//		_assertVisible(_div("details",_near(_heading3($OrderSummuary[9][1]))));	
//		}
//	else
//		{
//		_assertVisible(_heading3($OrderSummuary[9][0]));
//		_assertVisible(_div("details",_near(_heading3($OrderSummuary[9][0]))));
//		}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("249107","Verify the functionality of progress indicator on Place Order page in Application as a Registered user.");
$t.start();
try
{
//verifying the progress indicators
_assertVisible(_link("Shipping"));
_assertVisible(_link("Billing"));
_assertVisible(_link("Place Order"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("249125","Verify the details to be displayed for the cart line items on Summary Page in Application as a Anonymous user.");
$t.start();
try
{
	//verifying the cart line items
	_assertVisible(_link("/(.*)/",_in(_div("product-list-item"))));
	_assertEqual($pName, _getText(_link("/(.*)/",_in(_div("product-list-item")))));
	//_assertVisible(_image("/(.*)/",_in(_row("cart-row  first "))));
	_assertVisible(_cell("/item-total /"));
	_assertEqual($totalPrice, parseFloat(_extract(_getText(_cell("/item-total /", _in(_row("cart-row first ")))), "/[$](.*)/", true)));
	//_assertVisible(_div("product-quantity-value"));
	_assertEqual($QuantityInCart,_extract(_getText(_div("product-quantity-value qty-desktop-only")),"/: (.*)/",true));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("249104","Verify the 'ORDER SUMMARY' Section in the right nav on Summary Page in Application as a Anonymous user.");
$t.start();
try
{
	//Verify the 'ORDER SUMMARY'section
	_assertVisible(_row("order-subtotal", _in(_div("place-order-totals"))));
	_assertVisible(_row("order-shipping", _in(_div("place-order-totals"))));
	_assertVisible(_row("/order-sales-tax/", _in(_div("place-order-totals"))));
	_assertVisible(_row("order-total", _in(_div("place-order-totals"))));
	_assertEqual($subTotal,_extract(_getText(_row("order-subtotal", _in(_div("place-order-totals")))),"/[$](.*)/",true).toString());
	_assertEqual($shippingTax,_extract(_getText(_row("order-shipping", _in(_div("place-order-totals")))),"/[$](.*)/",true).toString());
	_assertEqual($tax,_extract(_getText(_row("order-total", _in(_div("place-order-totals")))),"/[$](.*)/",true).toString());
	_assertEqual($Ordertotal,_extract(_getText(_row("order-total", _in(_div("place-order-totals")))),"/[$](.*)/",true).toString());
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("249109","Verify the details to be displayed in the Shipping address Section in right nav on Summary Page in Application as a Registered user.");
$t.start();
try
{
	//verifying the shipping address
	_assertVisible(_heading3($OrderSummuary[7][0]));
	_assertVisible(_div("details",_near(_heading3($OrderSummuary[7][0]))));
	var $ShippingAddress=_getText(_div("details",_near(_heading3($OrderSummuary[7][0])))).replace(",","");
	_assertEqual($shippingAddress,$ShippingAddress);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("249111","Verify the details to be displayed in the Billing address Section in right nav on Summary Page in Application as a Registered user.");
$t.start();
try
{
	//verifying the billing address
	_assertVisible(_div("details",_near(_heading3($OrderSummuary[8][0]))));
	var $BillingAddress=_getText(_div("details",_near(_heading3($OrderSummuary[8][0])))).replace(",","");
	_assertEqual($billingAddress,$BillingAddress);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("249113","Verify the details to be displayed in the Payment method Section in right nav on Summary Page in Application as a Registered user.");
$t.start();
try
{
	//verifying the payment details section
	if(!_isSafari())
		{
		_assertVisible(_heading3($OrderSummuary[9][1]));
		_assertVisible(_div("details",_near(_heading3($OrderSummuary[9][1]))));
		var $payment_Details=_getText(_div("details",_near(_heading3($OrderSummuary[9][1]))));
		}
	else
		{
		_assertVisible(_heading3($OrderSummuary[9][0]));
		_assertVisible(_div("details",_near(_heading3($OrderSummuary[9][0]))));
		var $payment_Details=_getText(_div("details",_near(_span("Payment Method"))));
		}
	
	//Credit Card Test1 Visa ************1111 Exp. 05.2020 Amount: $69.29
	var $ExpCreditCard_Details=$Valid_Data[4][7]+" "+$totalorderprice+" "+$OrderSummuary[1][2];
	_assertEqual($ExpCreditCard_Details,$payment_Details);
	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("249115","Verify the Edit link functionality for shipping address Section in right nav on Summary Page in Application as a Registered user.");
$t.start();
try
{
	//clicking on edit link present in shipping address section
	_click(_link("EDIT",_in(_heading3($OrderSummuary[7][0]))));
	//verify the navigation
	_assertVisible(_fieldset("/"+$OrderSummuary[0][2]+"/"));
	//_assertVisible(_div("shippingForm"));
	_assertVisible(_submit("dwfrm_singleshipping_shippingAddress_save"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("249117","Verify the Edit link functionality for the billing address Section in right nav on Summary Page in Application as a Registered user.");
$t.start();
try
{
	//navigate to summuary page
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	BillingAddress($Valid_Data,0); 
	//entering Credit card Information
	PaymentDetails($Valid_Data,4);
	//navigate to order summary page
	_click(_submit("/button-fancy-large/"));
	//click on edit link present in billing section
	_click(_link("EDIT",_in(_heading3($OrderSummuary[8][0]))));
	//verify the navigation
	_assertVisible(_div("/"+$OrderSummuary[0][3]+"/"));
	_assertVisible(_submit("dwfrm_billing_save"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
       //navigate back to billing page
       _click(_link("Billing"));
}
$t.end();

var $t=_testcase("249119","Verify the Edit link functionality for Payment Method Section in right nav on Summary Page in Application as a Registered user.");
$t.start();
try
{
	//navigate to summuary page
	BillingAddress($Valid_Data,0); 
	//entering Credit card Information
	PaymentDetails($Valid_Data,4);

	//click on edit link present in payment deatils section
	if(!_isSafari())
		{
		_click(_link("Edit",_in(_heading3($OrderSummuary[9][1]))));
		}
	else
		{
		_click(_link("EDIT",_in(_heading3("EDITPAYMENT METHOD"))));
		}
	//verify the navigation to billing page
	_assertVisible(_div("/"+$OrderSummuary[0][3]+"/"));
	_assertVisible(_submit("dwfrm_billing_save"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("249121","Verify the Edit link displayed in Order Summary Section in the SUMMARY Section for registered user");
$t.start();
try
{
	//navigate to summuary page
	_click(_link("mini-cart-link-checkout"));
	if(_isVisible(_link("nothank")))
		{
	_click(_link("nothank"));
		}
	//navigate to summuary page
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	BillingAddress($Valid_Data,0); 
	//entering Credit card Information
	PaymentDetails($Valid_Data,4);

	//navigate to order summary page
	_click(_submit("/button-fancy-large/"));
	
	//click on edit link present in order Summuary section
	_click(_link("EDIT",_in(_heading3("ORDER SUMMARY EDIT"))));
	//verify the navigation to cart page
	 _assertVisible(_table("cart-table"));
	 _assertVisible(_div("cart-actions cart-actions-top"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("249101","Verify the functionality of 'Edit ' link next to PLACE ORDER  button on Summary Page in Application as a Registered user.");
$t.start();
try
{
	//navigate to summuary page
	_click(_link("mini-cart-link-checkout"));
	if(_isVisible(_link("nothank")))
		{
	_click(_link("nothank"));
		}
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	BillingAddress($Valid_Data,0); 
	//entering Credit card Information
	PaymentDetails($Valid_Data,4);

	//navigate to order summary page
	_click(_submit("/button-fancy-large/"));
	
	//click on edit cart link
_click(_link("My Bag", _in(_div("breadcrumb bonetext checkout-progress-indicator"))));
	//verify the navigation to Cart page
	 _assertVisible(_table("cart-table"));
	 _assertVisible(_div("cart-actions cart-actions-top"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("249105","Verify the functionality of 'PLACE ORDER ' button on Summary Page in Application as a Registered user.");
$t.start();
try
{
	navigateToShippingPage($OrderSummuary[0][7],$OrderSummuary[0][8]);
	//navigate to summuary page
	_click(_link("mini-cart-link-checkout"));
	if(_isVisible(_link("nothank")))
	{
		_click(_link("nothank"));
	}
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));

	BillingAddress($Valid_Data,0); 
	//entering Credit card Information
	PaymentDetails($Valid_Data,4);
	
	//navigate to order summary page
	_click(_submit("/button-fancy-large/"));

	//Click on Place Order
	_click(_submit("Place Order"));
	if(_isVisible(_image("CloseIcon.png")))
	{
	_click(_image("CloseIcon.png"));
	}
	//verify the navigation to thank you page
	_assertVisible(_heading1($OrderSummuary[0][5]));
	_assertVisible(_heading3("order-number"));
	
	//fetching order number
	var $orderNumber=_extract(_getText(_heading3("order-number")),"/: (.*)/",true);
	_log("Order number is ="+$orderNumber);
	
	_assertVisible(_div("orderdetails"));
	_assertVisible(_div("order-shipment-table"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();



cleanup();