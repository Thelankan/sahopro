_include("../util.GenericLibrary/GlobalFunctions.js");
_resource("Cart.xls");

var $Coupon_Data=_readExcelFile("Cart.xls","Coupon_Validation");
var $Cart_Data=_readExcelFile("Cart.xls","Cart_Data");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


var $t = _testcase("248901/248933/248923/248930/","Verify the Expand / Collapse arrows are removed and expanded line./Verify the functionality of Cart line items on shipping page in Application as a Registered user.");
$t.start();
try
{
		//1. Load the application and login with valid credentials.
		login();
		//1. The Home page should be loaded with all the required contents. and user should be logged in
		//_assertVisible(_div("account-info-content"));
		var $Act_Price=0;
		for(var $i=0;$i<3;$i++)
			{
			 
				//NavigatetoPDP("701643407248");//008884303996//25419334
			     search($Coupon_Data[$i][3]);
				_assertVisible(_div("pdpMain"));
				_assertVisible(_div("product-detail-info"));
				//4. Add the product to Cart. 
				var $prodName=_getText(_heading1("/product-name/", _in(_div("product-col-2 product-detail"))));
				_click(_submit("add-to-cart"));
				//6. Verify the Mini Cart.
				//a. Product should be listed in Mini cart, arrows should be removed, as all line items in the cart will always be expanded.
				_assertVisible(_image("/(.*)/", _in(_div("mini-cart-content"))));
				_assertVisible(_div($prodName, _in(_div("mini-cart-content"))));
				//5. Click on 'Cart' icon from header .
				//_click(_span("minicart-icon spriteimg"));
				//5. Mini cart should be displayed.
				_assertVisible(_div("mini-cart-content"));
				$Act_Price=$Act_Price+parseFloat(_extract(_getText(_div("mini-cart-pricing bonetext")),"/[$](.*)/",true));
				
			}
		//varifying the total price
		var $Exp_Tot=_extract(_getText(_div("mini-cart-subtotals")),"/[$](.*)/",true);
		
		_assertEqual($Act_Price,$Exp_Tot);
		_assertEqual("3", _count("_image","/desktop-only/",_in(_div("mini-cart-products"))));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248906 ","Verify the display of 'Promo message on mini Cart in the application both as an anonymous and a registered user");
$t.start();
try
{
	//Click on 'Cart' icon from header .
	_click(_span("minicart-icon spriteimg"));
	// Verify the Drop Down of Mini Cart.
	_assertVisible(_div("mini-cart-content"));
	 //Promo message will be displayed.
	//_assertVisible(_paragraph("/(.*)/", _in(_div("minicartslot"))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
ClearCartItems();
var $t = _testcase("296545/296546/296547/296550/296551","Verify the navigation on click of 'VIEW Cart' button in mini cart in the application both as an anonymous and a registered user./Verify the Coupon Code for Register and Anonymous user");
$t.start();
try
{
	search($Coupon_Data[5][3]);
	_click(_submit("add-to-cart"));
	
	_click(_span("minicart-icon spriteimg"));

	//Click on View cart button in mini cart
	_click(_link("button mini-cart-link-cart", _in(_div("mini-cart-totals"))));
	// The Cart page must be loaded/displayed. 
	//Promo code section. 
	_assertNotVisible(_textbox("dwfrm_cart_couponCode"));
	_assertEqual("COUPON CODE", _getText(_heading1("/cart-big-headings/")));
	_click(_heading1("/cart-big-headings/"));
	
	_assertVisible(_textbox("dwfrm_cart_couponCode"));

	
	//c. Total section .
	_assertVisible(_div("cart-order-totals"));
	//d. Product Name, Image, Quantity, Price with strike out if applicable and Total.
	if(!isMobile())
		{
	_assertVisible(_image("/(.*)/", _in(_table("cart-table"))));
	_assertVisible(_link("/(.*)/", _in(_div("product-list-item",_in(_table("cart-table"))))));
		}
	else
		{
		_assertVisible(_cell("item-image item-image-phone iphone-only"));
		_assertVisible(_link("/(.*)/",_in(_table("cart-table"))));

		}
	//e. Remove and Edit Button.
	_assertVisible(_submit("Remove"));
	//_assertVisible(_link("/Edit/"));
	//f. ECO FEE-NEBULIZER section will be next to all line item.(if applicable for canada site)
	
	//g. Subtotal, Shipping, Sales Tax and Estimated Total in Price Format : $100.85 followed by Checkout button.
	//h. Promotional discount (If applicable)
	//NOTE: The exact UI elements should match as per PSD/Style Guide. 

	_assertEqual($Cart_Data[0][1], _getText(_div("cart-header-text")));
	_assertVisible(_submit("dwfrm_cart_checkoutCart"));
	var $OrderTot=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
	var $ProdPrice=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true);
	
	//On cart page enter valid coupon and click on 'apply code' button.
	_assertVisible(_div("cart-coupon-code"));
	_setValue(_textbox("dwfrm_cart_couponCode"), $Cart_Data[0][0]);
	_click(_submit("dwfrm_cart_addCoupon"));
	//enetr the coupon Reedeme coupon code
	_setValue(_textbox("dwfrm_cart_couponCode"), $Cart_Data[0][0]);
	_click(_submit("dwfrm_cart_addCoupon"));
	
	//verify the error message
	_assertVisible(_div("Oops! This promo code has already been redeemed."));
	
	var $shipppingCharge=_extract(_getText(_row("order-shipping")),"/[$](.*)/",true);
	var $saleTax=_extract(_getText(_row("order-sales-tax bonetext")),"/[$](.*)/",true);
	//System should update the cart page and coupon details along with discount should get added in item line level or order level 
	_assertVisible(_row("rowcoupons"));
	//_assertVisible(_row("order-discount discount"));
	var $Discount=parseFloat(_extract(_getText(_div("discount")),"/[$](.*)[)]/",true));
	var $ExpProdPrice=$ProdPrice-$Discount;
	var $ActProdPrice=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true);
	
	_assertEqual($ActProdPrice,$ExpProdPrice);
	
	var $ExpTotPrice=$ExpProdPrice+$shipppingCharge+$saleTax;
	
	var $ActPrice=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
	_assertEqual($ExpTotPrice,$ActPrice);
	//click on the remove coupon
	while(_isVisible(_submit("dwfrm_cart_coupons_i0_deleteCoupon")))
	{
	_click(_submit("dwfrm_cart_coupons_i0_deleteCoupon"));
	}

	_assertNotVisible(_row("rowcoupons"));
	//_assertNotVisible(_row("order-discount discount"));
	var $ActPrice=_extract(_getText(_row("order-total")),"/[$](.*)/",true);
	_assertEqual( $OrderTot,$ActPrice);
	
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
if(!isMobile())
	{
var $t = _testcase("296552 /248908","Verify by providing valid coupon code and hit enter key in the application both as an anonymous and a registered user");
$t.start();
try
{
	//Searching & adding respective product to the cart to apply the coupon

	search($Coupon_Data[5][3]);
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	_click(_span("minicart-icon spriteimg"));
	_click(_link("button mini-cart-link-cart"));
	_assertNotVisible(_textbox("dwfrm_cart_couponCode"));
	_assertEqual("COUPON CODE", _getText(_heading1("/cart-big-headings/")));
	_click(_heading1("/cart-big-headings/"));
	//Applying Coupon code
	_setValue(_textbox("dwfrm_cart_couponCode"),$Cart_Data[0][0]);
	_focusWindow();
	//Statement to hit on enter key
	_focus(_textbox("dwfrm_cart_couponCode"));
	//_keyPress(document.body, [13,13]);
	 _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	//Verifying coupon row is visible or not
	_assertVisible(_row("rowcoupons"));
	//Verifying the coupon code is expected or not
	_assertEqual($Cart_Data[0][0],_getText(_span("/value/", _in(_div("cartcoupon clearfix")))));
	//Verifying $Cart_Data[1][0] message
	//_assertEqual($coupon_data[0][2], _getText(_span("bonus-item", _near(_div("cartcoupon clearfix")))));
	_assertEqual($Cart_Data[1][0], _getText(_span("/bonus-item/")));
	while(_isVisible(_submit("dwfrm_cart_coupons_i0_deleteCoupon")))
	{
	_click(_submit("dwfrm_cart_coupons_i0_deleteCoupon"));
	}


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
	}
var $t = _testcase("296553/54/55/56/57","Verify the system's response when user enters invalid coupon code, in the application both as an anonymous and a registered user.");
$t.start();
try
{
	_assertVisible(_cell($Cart_Data[2][0]));
	_assertVisible(_cell($Cart_Data[3][0]));
	_assertVisible(_cell($Cart_Data[4][0]));
	_assertVisible(_cell($Cart_Data[5][0]));
	_assertEqual($Cart_Data[1][1], _getAttribute(_textbox("dwfrm_cart_couponCode"),"placeholder"));
	for(var $i=0;$i<6;$i++)
	{
	_setValue(_textbox("dwfrm_cart_couponCode"), $Coupon_Data[$i][1]);
	_click(_submit("dwfrm_cart_addCoupon"));
	_wait(4000);
	if($i==0)
		{
		_assertVisible(_div($Coupon_Data[$i][2]));

		}
	if($i==1)
		{
		_assertVisible(_div($Coupon_Data[$i][2]));

		}
	if($i==2)
		{
		_assertVisible(_div($Coupon_Data[$i][2]));

		}
	if($i==3)
		{
		_assertVisible(_div($Coupon_Data[$i][2]));
		}
	if($i==4)
		{
		_assert(false,"EM");
		
		}
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("296560/296561/296562","Verify whether user can remove the line item from cart in the application both as an anonymous and a registered user");
$t.start();
try
{
	search($Coupon_Data[5][3]);
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	_click(_span("minicart-icon spriteimg"));
	_click(_link("button mini-cart-link-cart"));
	var $ProdNmae=_getText(_div("name hone-header"));
	
	//_assertVisible(_link($ProdNmae));
	//functionalty of editdetail
//	if(!isMobile())
//		{
//			
//	_click(_link("/Edit/", _in(_div("item-quantity-details"))));
//		}
//	else
//		{
//		_click(_link("/EDIT/", _in(_div("/cart-edit-details/ "))));
//		}
	_click(_link("/(.*)/", _in(_div("name hone-header"))))
	//veriy the navigation
	  _assertVisible(_div("product-detail-info"));
	  _assertVisible(_div("pdpMain"));
	  _assertEqual($ProdNmae, _getText(_heading1("/product-name/", _in(_div("desktop-only")))));
	  _click(_span("minicart-icon spriteimg"));
	  
	  _click(_link("button mini-cart-link-cart"));
	//functionalty of remove link
	_click(_submit("dwfrm_cart_shipments_i0_items_i0_deleteProduct"));
	_assertNotVisible(_link($ProdNmae));
	//UI of the empty cart
	_assertVisible(_heading1($Cart_Data[2][1]));
	_assertEqual($Cart_Data[4][1], _getText(_link("button-fancy-large cart-empty-link ")));
	_click(_link($Cart_Data[4][1]));
	//verify the  navigation 
	_assertVisible(_div($Cart_Data[5][1]));
	_assertVisible(_link("shop all best sellers"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248918","Verify user can update the quantity of each line item in the application both as an anonymous and a registered use");
$t.start();
try
{
	search($Coupon_Data[5][3]);
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	var $ValidQty=4;
	_click(_span("minicart-icon spriteimg"));
	_click(_link("button mini-cart-link-cart"));

	//verifying by entering valid Quantity
	//_setValue(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity"),$ValidQty);
	_setSelected(_select("/js-cart-quantity-dropdown/"), $ValidQty);
	//Click on enter key from keyboard
	//Statements to hit on enter key
	_focusWindow();
	_focus(_select("/js-cart-quantity-dropdown/"));
	_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	//Verifying Qty
	_assertEqual($ValidQty,_getText(_select("/js-cart-quantity-dropdown/")));	
	//verifying by entering 0 Quantity
	var $prodID=_getText(_span("value", _in(_div("/sku/"))));
	//_setValue(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity", _in(_row("cart-row"))),0);
	_setValue(_select("/js-cart-quantity-dropdown/"), "0");
	//Statements to hit on enter key
	_focusWindow();
	_focus(_select("/js-cart-quantity-dropdown/"));
	_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	_wait(2000);
	//Product Should not present
	//_assertNotVisible(_span($prodID));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248920/248928/248903","Verify the display of the Line item total as a each line item in the application both as an anonymous and a registered user/Verify the navigation on click of 'Checkout' button from the cart page for registered use");
$t.start();
try
{
	ClearCartItems();
	search($Coupon_Data[5][3]);
	_click(_submit("add-to-cart"));
	_click(_span("minicart-icon spriteimg"));

	_click(_link("button mini-cart-link-cart"));
	var $Qty=_getText(_select("/js-cart-quantity-dropdown/")).length;
	var $prodPrice=_extract(_getText(_div("mini-cart-pricing bonetext")),"/[$](.*)/",true);
	
	for(var $i=0;$i<=$Qty-1;$i++)
		{
		
		var $j=$i+1;
		_setSelected(_select("/js-cart-quantity-dropdown/"),$i);
		_wait(4000);
		var $ExpPrice=$prodPrice* $j;
		var $exp=round($ExpPrice,2).toString();
		var $ActPrice=_extract(_getText(_cell("item-total")),"/[$](.*)/",true).toString();
		_assertEqual($exp,$ActPrice);
		_click(_span("minicart-icon spriteimg"));
		var $qty=_extract(_getText(_div("mini-cart-pricing bonetext")),"/[: ](.*)[ $]/",true);
		_assertEqual($j,$qty);
		}
	_click(_submit($Cart_Data[3][1], _in(_div("cart-actions"))));
	//verify the navigation
	_assertVisible(_div("js-checkout-shipping"));
	_assertVisible(_div("shipping-method-list"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
//logout to the app
logout();

var $t = _testcase("248929","Verify the navigation on click of 'Checkout' button from the cart page for anonymous user.");
$t.start();
try
{
	ClearCartItems();
	search($Coupon_Data[5][3]);
	_click(_submit("add-to-cart"));
	_click(_span("minicart-icon spriteimg"));

	//click on the view cart
	
	_click(_link("button mini-cart-link-cart"));
	//verify the navigation
	_assertVisible(_row("cart-row "));
	_assertEqual($Cart_Data[0][1], _getText(_div("cart-header-text")));
	//click on the checkout
	_click(_submit($Cart_Data[3][1], _in(_div("cart-actions"))));
    //varify the navigation
	_assertVisible(_div("login-box login-account"));
	_assertVisible(_div("checkoutlogin"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase("","");
$t.start();
try
{


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/




