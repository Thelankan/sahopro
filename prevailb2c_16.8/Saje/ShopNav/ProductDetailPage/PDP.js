_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Header&Footer.xls");

var $pdp_data=_readExcelFile("PDP.xls","Pdp_Data");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();

var $t = _testcase("246115/246116","Verify the functionality of attribute 'Select size' on product detail page in the application both as an anonymous and a registered user.");
$t.start();
try
{
	
//navigate to PDP where size swatch should be present
NavigatetoPDP($pdp_data[0][0]);
//select size
if (_isVisible(_div("size-text")))
{
	
	if (!_isVisible(_listItem("selectable selected",_in(_list("swatches size")))))
		{
		//select any swatch
		_click(_link("swatchanchor",_in(_list("swatches size"))));
		}
}

//dollar symbool should display for price
_assertVisible(_span("/"+$currencysymbool+"(.*)/", _in(_div("product-price ptwotext"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246117","Verify the functionality of 'quantity dropdown' on product detail page for in the application both as an anonymous and a registered user.");
$t.start();
try
{
	
//checking the display of quantity drop down
_assertVisible(_div("quantity"));
//click on the quantity drop down
_click(_select("productquantityupdate"));
//quantity selections
var $qty=_getText(_select("productquantityupdate"));
//
for(var $i=0;$i<=$qty.length-1;$i++)
	{
	//set the quantity
	_setSelected(_select("productquantityupdate", _in(_div("quantity"))),$i);
	_assertEqual($qty[$i],_getSelectedText(_select("productquantityupdate", _in(_div("quantity")))));
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246118","Verify the functionality of 'Add to cart' button on product detail  Page  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	
//navigate to PDP where size swatch should be present
NavigatetoPDP($pdp_data[0][0]);
	
//set the quantity
_setSelected(_select("productquantityupdate", _in(_div("quantity"))),$pdp_data[0][3]);
//click on add to cart button
_click(_submit("add-to-cart"));
//click on view car link
_click(_link("/mini-cart-link-cart/"));
//product verifying
_assertEqual($PLPProdName,_getText(_link("/(.*)/", _in(_div("/name/")))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246120/246122/246124","Verify the functionality of product names on Product detail page for few products the application both as an anonymous and a registered user./Verify the Product Name in PDP for both as an anonymous and a registered user.");
$t.start();
try
{

for(var $i=0;$i<4;$i++)
{	
//search the product	
_click(_span("/search-icon spriteimg/"));
_setValue(_textbox("q", _in(_div("global-header-search"))),$pdp_data[0][0]);
_wait(3000);

//Select the suggested products.
var $prodNameinSearchSugg=_getText(_div("/product-name/", _in(_div("product-suggestion"))));

if ($i==0)
{
_click(_link("product-link",_in(_div("product-suggestion"))));
}
else if ($i==1)
{
_click(_image("/(.*)/", _in(_div("product-suggestion"))));
}
else if ($i==2)
{
_click(_div("product-nameExtension",_in(_div("product-suggestion"))));
}
else 
	{
	_click(_link("/(.*)/", _in(_click(_div("product-price", _in(_div("product-suggestion")))))));
	}

//navigate to pdp
var $prodNameinPDP=_getText(_heading1("/product-name/", _in(_div("product-col-2 product-detail"))));
_assertEqual($prodNameinSearchSugg,$prodNameinPDP);
_assertVisible(_div("pdpMain"));
_assertVisible(_div("product-primary-image "));

}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246127/246146/","Verify the UI of Product Icons in PDP both as an anonymous and a registered user./Verify the Functionality of Product name/image/price in Recently viewed section");
$t.start();
try
{
	
//product name in PDP above
var $prodNameinPDP=_getText(_heading1("/product-name/", _in(_div("product-col-2 product-detail"))));
var $prodPrice=_extract(_getText(_span("price-sales", _near(_div("last-visited-contnet")))),"/"+[$currencysymbool]+"(.*)/",true);

//product name in last visited page
var $productNameinbottom=_getText(_link("name-link", _in(_div("last-visited-contnet"))));
var $productpriceinbottom=_extract(_getText(_span("price-sales", _near(_div("last-visited-contnet")))),"/"+[$currencysymbool]+"(.*)/",true);

//asserting product name and price
_assertEqual($prodNameinPDP,$productNameinbottom);
_assertEqual($prodPrice,$productpriceinbottom);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246129","Verify the UI of Shipping and Returns section.");
$t.start();
try
{

//UI of shipping and returns link
_assertVisible(_heading3("/"+$pdp_data[0][1]+"/", _in(_div("pdp-tab-section"))));
//click on shipping and returns link
_click(_heading3("pdp-tab-title",_near(_heading3("/"+$pdp_data[0][1]+"/"))));
//all the UI links
_assertVisible(_span("email-img"));
_assertVisible(_span("phone-img"));
_assertVisible(_span("chat-img"));
_assertVisible(_heading3($pdp_data[1][1]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246131/246154/246155/246156/246125","Verify the UI of How to use this product section./Verify the functionality of How to use this product, in PDP Page./Verify the functionality of Ingredients, in PDP Page./Verify the functionality of  Shipping and returns in PDP Page");
$t.start();
try
{
	
var $toggleCount=_count("_heading2","pdp-tab-title",_in(_div("pdp-tab")));

for (var $i=0;$i<$toggleCount;$i++)
	{
	//UI of shipping and returns link
	_assertVisible(_heading2("/"+$pdp_data[$i][4]+"/", _near(_div("pdp-tab-content["+$i+"]"))));
	//click on shipping and returns link
	_click(_heading2("pdp-tab-title",_near(_heading2("/"+$pdp_data[$i][4]+"/"))));
	//product tab content
	_assertVisible(_div("pdp-tab-content["+$i+"]"));
	//Again click on shipping and returns link
	_click(_heading2("pdp-tab-title",_near(_heading2("/"+$pdp_data[$i][4]+"/"))));
	//product tab content Should not display
	_assertNotVisible(_div("pdp-tab-content["+$i+"]"));
	}


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246155","Verify the functionality of     - Ingredients, in PDP Page.");
$t.start();
try
{

	//UI of shipping and returns link
	_assertVisible(_heading2("/"+$pdp_data[2][1]+"/", _in(_div("pdp-tab-section"))));
	//click on shipping and returns link
	_click(_heading2("pdp-tab-title",_near(_heading2("/"+$pdp_data[2][1]+"/"))));
	//product tab content
	_assertVisible(_div("pdp-tab-content"));
	//Again click on shipping and returns link
	_click(_heading2("pdp-tab-title",_near(_heading2("/"+$pdp_data[2][1]+"/"))));
	//product tab content Should not display
	_assertNotVisible(_div("pdp-tab-content"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246134/246135","Verify the UI of Recently viewed this section./Verify the UI of Recently Viewed Products section.");
$t.start();
try
{
	
//Recently viewed section
_assertVisible(_div("product-listing last-visited"));
_assertVisible(_list("/search-result-items/"));
_assertVisible(_div("last-visited-contnet"));
_assertVisible(_div("product-listing last-visited"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("246136/246139/246140/246138","Verify the functionality of '?' icon in NPN/Verify the functionality of 'X' icon in NPN overlay/Verify the functionality of Got it button in overlay");
$t.start();
try
{

//changing country to canada
CountrySelection($countrysel[1][0]);

//navigate to PDP where size swatch should be present
NavigatetoPDP($pdp_data[0][0]);
	
//check the presence of NPN link in PDP for canda site
_assertVisible(_link("?", _in(_div("/product-number/"))));
_click(_link("?", _in(_div("/product-number/"))));
//verifying pop up
_assertVisible(_heading2($pdp_data[3][1]));
_assertVisible(_heading3("npn-description"));
_assertVisible(_div("dialog-container"));
//got it button
_assertVisible(_link("buttonctaone", _in(_div("read-more-btn"))));
_assertVisible(_button("Close"));
//click on got it button in popup
_click(_button("Close"));
_assertNotVisible(_div("dialog-container"));

//again clikc on ? link
_click(_link("?", _in(_div("/product-number/"))));
//popup should display
_assertVisible(_div("dialog-container"));
//clikc on got it button
_click(_link("buttonctaone", _in(_div("read-more-btn"))));
//popup should not display
_assertNotVisible(_div("dialog-container"));

//changing country to US
CountrySelection($countrysel[0][0]);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246144","Verify the validation on Add to bag button");
$t.start();
try
{

//navigate to PDP where size swatch should be present
NavigatetoPDP($pdp_data[0][2]);
_wait(3000);

//fetching all the attributes in PDP
var $NameInPDP=_getText(_heading1("/product-name/", _in(_div("product-col-2 product-detail"))));
var $PriceinPDP=_extract(_getText(_span("price-sales", _near(_div("last-visited-contnet")))),"/[$](.*)/",true);

if (_isVisible(_link("swatchanchor")))
	{
	var $SwatchInPDP=_getText(_link("swatchanchor"));
	}

var $QtyinPDP=_getSelectedText(_select("productquantityupdate"));

//click on add to cart after selecting variant
selectSwatch();
//click on add to cart button
_click(_submit("add-to-cart"));
//same product should add to cart
_assertEqual($PLPProdName,_getText(_link("/(.*)/", _in(_div("/mini-cart-name/")))));

//click on view cart button
_click(_link("View Bag"));

//verify productname,price,quantity in cart page
var $prodNameincart=_getText(_link("/(.*)/",_in(_div("name"))));

if (_isVisible(_span("/value/",_in(_div("/Size/",_in(_div("product-list-item")))))))
	{
	var $swatchInPDP=_getText(_span("/value/",_in(_div("/Size/",_in(_div("product-list-item"))))));
	_assertEqual($SwatchInPDP,$swatchInPDP);
	}

var $QuantityInCart=_getSelectedText(_select("/js-cart-quantity-dropdown/", _in(_cell("/quantity/"))));
var $prodpriceincart=_extract(_getText(_span("price-total")),"/"+[$currencysymbool]+"(.*)/",true).toString();

//comparing all the elements
_assertEqual($NameInPDP,$prodNameincart);
_assertEqual($PriceinPDP,$prodpriceincart);
_assertEqual($QtyinPDP,$QuantityInCart);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246137","Verify Product Characteristics, Product Details, Product Marketing Hook in PDP page for both Register and Guest user.");
$t.start();
try
{
	
//Product Characteristics UI
_assertVisible(_div("product-characteristics"));
//Product Details
_assertVisible(_div("product-detail-info"));
//Product Marketing
_assertVisible(_div("product-marketing-hook"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246146","Verify the Functionality of Product name/image/price in Recently viewed section");
$t.start();
try
{

//fetching product name and price in PDP
var $prodname=new Array();
var $ProdPrice=new Array();
	
//fetching product name and price in recently viewed
var $NameinRecentlyViewed=new Array();
var $PriceinRecentlyViewed=new Array();

//navigate with 3 products to PDP
for(var $i=0;$i<4;$i++)
	{
	
	//navigate to PDP where size swatch should be present
	NavigatetoPDP($pdp_data[$i][2]);
	
	//fetching all the attributes in PDP
	var $NameInPDP=_getText(_heading1("/product-name/", _in(_div("product-col-2 product-detail"))));
	var $PriceinPDP=_extract(_getText(_span("price-sales", _near(_div("last-visited-contnet")))),"/"+[$currencysymbool]+"(.*)/",true);
	//fetching product name and price in PD page
	$prodname.push($NameInPDP);
	$ProdPrice.push($PriceinPDP);
	
	}
	

var $productsInRecentlyViewed=_count("_link","thumb-link",_in(_div("last-visited-contnet")));

//navigate with 3 products to PDP
for(var $i=0;$i<$productsInRecentlyViewed;$i++)
	{
	var $NameInRecentlyViewed=_getText(_link("name-link", _in(_div("product-name["+$i+"]"))));
	var $priceInRecentlyViewed=_extract(_getText(_link("/(.*)/", _in(_span("product-sales-price["+$i+"]")))),"/"+[$currencysymbool]+"(.*)/",true);
	
	$NameinRecentlyViewed.push($NameInRecentlyViewed);
	$PriceinRecentlyViewed.push($priceInRecentlyViewed);
	
	}

//product name and price should match in PDP and recently viewed section
_assertEqualArrays($prodname,$NameinRecentlyViewed);
_assertEqualArrays($ProdPrice,$PriceinRecentlyViewed);


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246145/246132","Verify the Functionality of Product name/image/price in You may also enjoy section/Verify the UI of you may also enjoy this section.");
$t.start();
try
{

//you may also enjoy section
_assertVisible(_div($pdp_data[4][1]));
_assertVisible(_div("carousel-recommendations"));
_assertVisible(_div("recommendations cross-sell"));

var $productsinYouAlsoEnjoy=_count("_div","/product-tile/",_in(_div("carousel-recommendations")));
	
for (var $i=0;$i<$productsinYouAlsoEnjoy;$i++)
{
//product name in recommendations section
_assertVisible(_link("/(.*)/", _in(_div("product-name ptwotext["+$i+"]"))));
//product price in recommendations section
_assertVisible(_span("/price-sales/", _near(_div("product-name ptwotext["+$i+"]"))));
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246143","Verify the functionality of Chat icon in Shipping and return section.");
$t.start();
try
{

//click on shipping and returns link
_click(_heading3("pdp-tab-title",_near(_heading3($pdp_data[2][4]))));
//click on live chat button in shipping and returns expanded toggle
_click(_link("Saje Live Chat",_in(_div("pdp-tab-icon"))));
//chat link verification
_assertVisible(_span($pdp_data[0][5]));
_assertVisible(_row("body-container"));
_assertVisible(_textbox("email"));
_assertVisible(_submit($pdp_data[1][5]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246142","Verify the functionality of Mail icon in Shipping and return section.");
$t.start();
try
{
	
//click on ingredents section
_click(_heading2($pdp_data[2][4]));
//click on emai, icon
_click(_span("email-img"));
//should navigate to contact us page
_assertVisible(_heading1($pdp_data[0][6]));
_assertVisible(_div("customer-serv-contact-us"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase("","");
$t.start();
try
{
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/