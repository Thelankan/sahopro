_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Search.xls");

var $Searchdata=_readExcelFile("Search.xls","SearchData");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();

var $t = _testcase("243773/243779/243777","Verify the navigation related to suggested keyword and Link in below of search suggestion overlay in application both as an Anonymous and Registered user..");
$t.start();
try
{
	
//_click(_link("Search",_in(_div("right-container"))));
_click(_span("/search-icon spriteimg/"));
_setValue(_textbox("q", _in(_div("global-header-search"))),$Searchdata[2][0]);
_click(_submit("/Search/",_in(_div("global-header-search"))));

//UI of search suggestion overlay
_assertVisible(_div("/Categories/"));
_assertVisible(_div("hitgroup"));
//product name and 
var $countofproducts=_count("_div","product-image",_in(_div("/search-suggestion-wrapper/")));

for (var $i=0;$i<$countofproducts;$i++)
	{
	_assertVisible(_div("product-image["+$i+"]"));
	_assertVisible(_div("product-name["+$i+"]"));
	_assertVisible(_div("product-price["+$i+"]"));
	}


_click(_link("hit", _in(_div("phrase-suggestions"))));
_assertContainsText($Searchdata[0][0], _div("content-heading"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("243780","Verify the functionality related to Suggested products appearing in search field both as an Anonymous and Registered user.");
$t.start();
try
{

//search the product	
_click(_span("/search-icon spriteimg/"));
_setValue(_textbox("q", _in(_div("global-header-search"))),$Searchdata[0][0]);
_click(_submit("/Search/",_in(_div("global-header-search"))));

//3. Select the suggested products.
var $prodName=_getText(_div("product-name"));
_click(_link("product-link", _in(_div("product-suggestion"))));
//navigate to pdp
var $prodNamePDP=_getText(_heading1("product-name", _in(_div("desktop-only"))));
_assertEquals($prodName,$prodNamePDP);
_assertVisible(_div("pdpMain"));
_assertVisible(_heading1("product-name", _in(_div("desktop-only"))));
	
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("243781/243771","Verify the application behavior on click of Product name in the search suggestion overlay/Verify the display of search field in application both as anonymous and registered user.");
$t.start();
try
{
	
_assertVisible(_span("spriteimg search-icon", _in(_div("global-header-search"))));
_click(_link("Search",_in(_div("right-container"))));
_assertVisible(_span("spriteimg mark", _in(_div("global-header-search"))));
_assertNotVisible(_span("spriteimg search-icon", _in(_div("global-header-search"))));


_assertVisible(_textbox("q", _in(_div("global-header-search"))));
_setValue(_textbox("Search Catalog[1]"), "Dig");
_click(_submit("Search[1]"));

var $ImageSugg=_getAttribute(_image("desktop-only", _in(_div("product-image"))),"alt");
//click on the image
_click(_image("/(.*)/", _in(_div("product-suggestion"))));
_assertVisible(_div("pdpMain"));
_assertVisible(_heading1("product-name", _in(_div("desktop-only"))));
//
var $ActImagePDP=_getAttribute(_image("primary-image desktop-only"),"alt");
_assertEqual($ImageSugg,$ActImagePDP);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243898/243922/243778","Verify the display of 'View all' button in search result page");
$t.start();
try
{
	
//search the product	
_click(_span("/search-icon spriteimg/"));
_setValue(_textbox("q", _in(_div("global-header-search"))),$Searchdata[0][0]);
_click(_submit("/Search/",_in(_div("global-header-search"))));

//.Suggested Categories : User should be directed to respective category listing page.
_assertVisible(_list("search-result-items"));
_assertVisible(_div("search-result-content"));
_assertContainsText("\" digital \"", _div("sone resulttext"));
var $ExpTotProd=_extract(_getText(_heading1("results-products")),"/[(](.*)[)]/",true);
_assertVisible(_div("view-all-items"));
_click(_link("viewallitems buttonctaone"));
var $actualProd=_count("_listItem","/(.*)/",_in(_div("search-result-content")));
_assretEqual($ExpTotProd,$actualProd);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243902/243903/243904/243932","Verify the navigation to search no result page from home page in application both as an Anonymous and Registered user./Verify the functionality related to Try new search 'Search' icon for valid keyword on search no result page in application both as an Anonymous and Registered user.");
$t.start();
try
{
	
_click(_link("Search",_in(_div("right-container"))));
_setValue(_textbox("Search Catalog[1]"), "Dgtl");
_click(_submit("Search[1]"));

_assertVisible(_div("search-notfound"));
_assertEqual("Oops! no Products were found for dgtl", _getText(_div("search-data")));
_assertVisible(_div("no-hits-content-results"));
//3. Enter invalid keyword in the try new search text field . Click on 'Search' icon and verify.
_setValue(_textbox("input-text valid"), "asdfg");
_click(_submit("simplesearch"));
//navigation to search results
_assertVisible(_div("search-notfound"));
_assertVisible(_div("no-hits-content-results"));


//Enter valid keyword in the try new search text field . Click on 'Search' icon and verify.
_setValue(_textbox("input-text valid"), "Digital");
_click(_submit("simplesearch"));
//verifying the navigation on search results page
_assertVisible(_list("search-result-items"));
_assertEqual("\" digital \"", _getText(_div("sone resulttext")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("243911/243912/243913/243914/243915","");
$t.start();
try
{
	_click(_link("Search",_in(_div("right-container"))));
	_setValue(_textbox("Search Catalog[1]"), "Dgtl");
	_click(_submit("Search[1]"));
	
	//Verify the Product count.
	_assertEqual("Products (0) // Articles (0)", _getText(_div("no-hits-content-results")));
	
	//Verify the No results message.
	_assertEqual("Oops! no Products were found for dfghj", _getText(_div("search-data")));
	
	//The Email entry is a ''mailto:hello@saje.com'' link, with the subject and body left empty. 
	_assertEqual("Hello@SajeCA", _getText(_span("email-link")));

	//- The Call entry is a ''tel:18772757253'' link. 
	_assertEqual("18772757253", _getText(_div("saje-email[1]")));

	//- The Chat entry will launch the LiveChat javascript window.
	_assertEqual("Saje Live Chat", _getText(_div("saje-chat")));
     
	
	// Click on Best Sellers Products .
	_assertVisible(_heading2("Best Sellers"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("243776","Verify the UI of search suggestion overlay when user types a Product name in application both as an Anonymous and Registered user.");
$t.start();
try
{
	
_click(_link("Search",_in(_div("right-container"))));
_setValue(_textbox("Search Catalog[1]"), "Digital");
var $suggestedProd=_collectAttributes("_div","/product-name/","sahiText",_in(_div("product-suggestions")));
for(var $i=0;$i<$suggestedProd.length;$i++)
	{
	_assertContainsText("Digital", _div("product-name["+$i+"]"));
	}
_click(_submit("Search[1]"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243779/243854/243853/243855/243856/243775","Verify the functionality related to search suggestions present in the global header  in application both as an Anonymous and  Registered  user./Verify the Product Tile for Search Suggestions of Product Name field/Verify the Product Tile for Search Suggestions of Product Image field");
$t.start();
try
{

for(var $i=0;$i<4;$i++)
	{	
	//search the product	
	_click(_span("/search-icon spriteimg/"));
	_setValue(_textbox("q", _in(_div("global-header-search"))),$Searchdata[0][2]);
	
	//Select the suggested products.
	var $prodNameinSearchSugg=_getText(_div("product-name", _in(_div("product-suggestion"))));
	
	if ($i==0)
	{
	_click(_link("product-link",_in(_div("product-suggestion"))));
	}
	else if ($i==1)
	{
	_click(_image("/(.*)/", _in(_div("product-suggestion"))));
	}
	else if ($i==2)
	{
	_click(_div("product-nameExtension",_in(_div("product-suggestion"))));
	}
	else 
		{
		_click(_link("/(.*)/", _in(_click(_div("product-price", _in(_div("product-suggestion")))))));
		}
	
	//navigate to pdp
	var $prodNameinPDP=_getText(_heading1("product-name", _in(_div("product-col-2 product-detail"))));
	_assertEquals($prodNameinSearchSugg,$prodNameinPDP);
	_assertVisible(_div("pdpMain"));
	_assertVisible(_div("product-primary-image "));
	
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243860/243861","Verify the functionality  of  Search Refinement Types for  Anonymous and  Registered  user./Verify the functionality  of  Arrow Icon for  Anonymous and  Registered  user.");
$t.start();
try
{

//search the product	
search($Searchdata[0][0]);
//checking arrow
_assertVisible(_span("spriteimg", _in(_heading3("toggle"))));
//check the refinements
_click(_heading3("Price"));
//checking expanded arrow
_assertVisible(_span("spriteimg", _in(_heading3("toggle expanded"))));
//fetch the selectable refinement
var $pricerefinement=_getText(_link("refinement-link"));
//select any refinement
_click(_link("refinement-link"));
//refinement in bread crumb
var $breadcrumbrefinement=_getText(_span("breadcrumb-refinement-value"));
//refinement should get selected
_assertEqaul($pricerefinement,$breadcrumbrefinement);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243774","Verify the navigation related to suggested keyword and  Link in Right Pane of search suggestion overlay in application both as an  Anonymous and  Registered  user");
$t.start();
try
{

//Enter data in search box
_click(_span("/search-icon spriteimg/"));
_setValue(_textbox("q", _in(_div("global-header-search"))),$Searchdata[1][0]);
//Clicking on any link under other suggested search keyword present in right pane.
_click(_link("hit"));
//enter text should be displayed in PLP
_assertContainsText($Searchdata[1][0], _link("name-link"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243852","Verify the navigation related to Product name Link in Right Pane of search suggestion overlay  in application both as an  Anonymous and  Registered  user");
$t.start();
try
{

//Enter data in search box
_click(_span("/search-icon spriteimg/"));
_setValue(_textbox("q", _in(_div("global-header-search"))),$Searchdata[0][0]);
//get text of product name
var $SearchProdName=_getText(_div("product-name"));
//click on any search element
_click(_link("product-link"));
//should navigate to PDP
var $productNameInPDP=_getText(_heading1("product-name", _in(_div("/product-detail/"))));
//product name should be equal
_assertEqual($SearchProdName,$productNameInPDP);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243862/243864/243859/243863","Verify the functionality  of Search Refinement Selections for  Anonymous and  Registered  user./Verify the functionality related to  'Price' refinement filter on Category grid page in application both as an  Anonymous and  Registered  user./Verify the functionality related to  'Type' refinement filter on search result page in application both as an  Anonymous and  Registered  user." +
		"Verify the functionality related to  'Price' refinement filter on Category grid page. in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{

//search the product	
search($Searchdata[0][0]);

var $pricerefinements=_collectAttributes("_link","refinement-link","sahiText",_in(_div("refinement price ")));

//navigate to search result page
for (var $i=0;$i<$pricerefinements.length;$i++)
{
	_click(_link($pricerefinements[$i],_in(_div("refinement price "))));
	//click on total count
	_click(_div("total-count"));
	//price in bread crumb
	_assertEqual($pricerefinements[$i],_getText(_span("/(.*)/", _in(_span("breadcrumb-refinement")))));
	//clear link should be visible
	_assertVisible(_link("/Clear all/", _near(_span("breadcrumb-refinement"))));
	//checking price
	priceCheck($pricerefinements[$i]);
	//un checking the refinement
	_click(_link($pricerefinements[$i],_in(_div("refinement price "))));
	//clear link should Not be visible
	_assertNotVisible(_link("/Clear all/", _near(_span("breadcrumb-refinement"))));
}


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243865","Verify the functionality  of Search Refinements page Checkbox filters in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{

//search the product	
search($Searchdata[2][0]);

var $Filters=_collectAttributes("_heading3","/toggle/","sahiText",_in(_div("refinements")));
for(var $i=0;$i<$Filters.length;$i++)
	{	
		//click on filter
		_click(_heading3($Filters[$i]));
		
		if ($Filters[$i]=="Size")
			{
			_assertNotVisible(_list("/(.*)/", _in(_div("/refinement  size/"))));
			//click on filter
			_click(_heading3($Filters[$i]));
			//Filter Should Collapse
			_assertVisible(_list("/(.*)/", _in(_div("/refinement  size/"))));
			}
		else if ($Filters[$i]=="Price")
			{
			_assertNotVisible(_listItem("/(.*)/", _in(_div("refinement price "))));
			//click on filter
			_click(_heading3($Filters[$i]));
			//Filter Should Collapse
			_assertVisible(_listItem("/(.*)/", _in(_div("refinement price "))));
			}
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243866/243869/243871/243875/243874","Verify the functionality  of Applied Refinement List in application both as an  Anonymous and  Registered  user./Verify the Applied refinement Labels  appears for anonymous and  registered  user./Verify the UI of Filtered by section in search result page");
$t.start();
try
{

//search the product	
search($Searchdata[2][0]);

var $Filters=_collectAttributes("_heading3","/toggle/","sahiText",_in(_div("refinements")));

for(var $i=0;$i<$Filters.length;$i++)
{
	
//click on filter
_click(_heading3($Filters[$i]));

if ($Filters[$i]=="Size")
	{
	//click on filter
	_click(_heading3($Filters[$i]));
	//select the size
	var $size=_getText(_link("/(.*)/", _in(_list("clearfix swatches size"))));
	_click(_link("/(.*)/", _in(_list("clearfix swatches size"))));
	var $sizeselected=_getText(_span("breadcrumb-refinement-value"));
	_assertVisible(_span($size,_in(_span("breadcrumb-refinement"))));
	_assertEqual($size,$sizeselected);
	_assertVisible(_link("Remove"));
	//Clear selected refinements
	_click(_link("Remove"));
	//selected swatch should not display
	_assertNotVisible(_span($size,_in(_span("breadcrumb-refinement"))));
	//Clear selected refinements should not display
	_assertNotVisible(_link("Remove"));
	}

else if ($Filters[$i]=="Price")
	{
	_click(_link($pricerefinements[$i],_in(_div("refinement price "))));
	//selected refinement should be visible in bread crumb
	_assertVisible(_span($pricerefinements[$i], _in(_span("breadcrumb-refinement"))));
	_assertVisible(_link("Remove"));
	//Clear selected refinements
	_click(_link("Remove"));
	//selected refinement should Not be visible in bread crumb
	_assertNotVisible(_span($pricerefinements[$i], _in(_span("breadcrumb-refinement"))));
	//Clear selected refinements should not display
	_assertNotVisible(_link("Remove"));
	}

}


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243868","Verify the Applied refinement X icon appears for anonymous and  registered  user.");
$t.start();
try
{

//search the product	
search($Searchdata[2][0]);
	
var $sizerefinements=_collectAttributes("_link","/Refine by Size:/","sahiText",_in(_list("clearfix swatches size")));

for(var $i=0;$i<$sizerefinements.length;$i++)
{
	_click(_link($sizerefinements[$i], _in(_div("refinement  size"))));
	//selected size should display in bread crumb
	_assertVisible(_span($sizerefinements[$i],_in(_span("breadcrumb-refinement"))));
	//remove icon should display
	_assertVisible(_link("Remove"));
	_click(_link("Remove"));
	//selected size should display in bread crumb
	_assertNotVisible(_span($sizerefinements[$i],_in(_span("breadcrumb-refinement"))));
	//remove icon should display
	_assertNotVisible(_link("Remove"));	
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243877/243878/243857/243858","Verify the functionality of Filter button in IPad and IPhone/Verify the filter drawers in IPad and IPhone/Verify the functionality related to  'Type' refinement filter on Category grid page. in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{

var $sizerefinements=_collectAttributes("_link","/Refine by Size:/","sahiText",_in(_list("clearfix swatches size")));

for(var $i=0;$i<$sizerefinements.length;$i++)
{
	_click(_link($sizerefinements[$i], _in(_div("refinement  size"))));
}

var $selectedsizeinBreadcrumb=_getText(_span("breadcrumb-refinement"));
//comparing selected refinements are one in the same
_assertEqual($sizerefinements,$selectedsizeinBreadcrumb);
//clear link should be visible
_assertVisible(_link("Clear", _near(_span("breadcrumb-refinement"))));
//click on clear link
_click(_link("Clear", _near(_span("breadcrumb-refinement"))));
//refinements should not display	
_assertNotVisible(_span("breadcrumb-refinement"));	


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243887/243919","Verify the display of 'View all' button in Category grid page/Verify the  ' Product count ' of Search Results Products in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{

//search the product	
search($Searchdata[2][0]);
//should display product count
_assertVisible(_heading1("results-products"));
//total no of products
var $totalproduct=_extract(_getText(_heading1("results-products")),"/ [(](.*)[)]/",true);
//click on view all link
_click(_link("/viewallitems/"));
//total no of products in page
var $totalproductsinpage=_count("_link","name-link",_in(_list("search-result-items")));
_assertEqual($totalproduct,$totalproductsinpage);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243920/243924/243925","Verify the navigation of ' Article count ' of  Search Results Products in application both as an  Anonymous and  Registered  user./Verify the' Search Results banner ' of Search Results, Articles .");
$t.start();
try
{

//Navigation to search result page by searching an article	
search($Searchdata[3][0]);
//search phrase link
_assertVisible(_link("searchphraselink"));
//Articles should display
_assertContainsText("Articles", _span("ptwotext"));
_assertVisible(_div("article-title"));
//content assert 
_assertVisible(_list("folder-content-list"));
_assertEqual($Searchdata[3][0], _getText(_link("security policy")));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}

var $t = _testcase("243926/243927/243928","Verify the functionality of 'Product count ' of Search Results, Articles page in application both as an  Anonymous and  Registered  user./Verify the 'Article count' of Search Results, Articles page in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{

//products 
_assertVisible(_div("/Products/"));
_click(_link("/Articles/"));

//price fetching
var $countarticles=_extract(_getText(_span("ptwotext")),"[(] (.*) [)]",true);

//count of articles
var $countarticlelanding=_count("_link","content-title headingone",_in(_list("folder-content-list")));

//comparing
_assertEqual($countarticles,$countarticlelanding);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("","");
$t.start();
try
{


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase("","");
$t.start();
try
{


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/


