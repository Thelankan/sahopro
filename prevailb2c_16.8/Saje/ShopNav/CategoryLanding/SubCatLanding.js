_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Search.xls");

var $catData=_readExcelFile("CategoryLanding.xls","SearchData");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


var $t = _testcase("243760/243842 ","Verify the Navigation to Category grid page in application both as an Anonymous and Registered user./Verify the display of Product grid in Category grid page.");
$t.start();
try
{
	//Click/mouse hover on any root category and click on any sub category.
	_mouseOver(_link("has-sub-menu", _in(_list("menu-category level-1"))));
	//Subcategory flyout should be displayed
	_assertVisible(_div("level-2"));
	_assertVisible(_list("menu-vertical"));
	//Click on any subcategory.
	_click(_link("Digital Cameras", _in(_list("menu-vertical"))));
	//User should be navigated to category grid page.
	_assertEqual("Digital Cameras", _getText(_div("content-heading")));
	_assertVisible(_listItem("grid-tile new-row "));

	//verfiy the disply of the product Name Image,price 
	_assertVisible(_image("/(.*)/",_in(_div("product-image"), _in(_listItem("grid-tile new-row ")))));
	_assertVisible(_link("name-link",_in(_div("product-name"), _in(_listItem("grid-tile new-row ")))));
	_assertVisible(_link("/(.*)/",_in(_span("product-standard-price"), _in(_listItem("grid-tile new-row ")))));
	//Out Of Stock Message should be displayed below Price (If applicable)
	_assertFlase("");
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("243844/243845/243846/243847/243848","Verify the functionality of Product image/procuctName/ProductPrice in category list page");
$t.start();
try
{

NavigatetoSubCategory($catData[3][0]);	
//click on product image in PLP
for(var $i=0;$i<3;$i++)
{

////search result page
//_assert(false,"temp adding search result page delete it once cat page started working");
//	
////search data	
//search($catData[3][0]);
	
var $prodNameinPLP=_getText(_link("name-link"));
var $priceinPLP=_extract(_getText(_span("product-sales-price")),"/[$](.*)/",true);
	
if ($i==0)
	{
	_click(_link("name-link"));
	}
else if ($i==1)
	{
	_click(_image("/(.*)/", _in(_div("product-tile"))));
	}
	else 
		{
		_click(_link("/(.*)/", _in(_span("product-sales-price"))));
		}

//should navigate to PDP page
var $prodNameinPDP=_getText(_heading1("product-name", _in(_div("product-col-2 product-detail"))));
var $priceinPDP=_extract(_getText(_span("price-sales")),"/[$](.*)/",true);

//PDP page
_assertVisible(_div("pdpMain"));
_assertVisible(_div("product-primary-image "));
//Comparing Name and Price
_assertEqual($prodNameinPLP,$prodNameinPDP);
_assertEqual($priceinPLP,$priceinPDP);
	
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243880","Verify the functionality after selecting different types of filters in category grid page  in the application both as an anonymous and a registered user.");
$t.start();
try
{

//Navigate to sub category landing page
NavigatetoSubCategory($catData[3][0]);

_assert(false,"should remove search once its started working");

//search the product	
search($Searchdata[2][0]);

var $Filters=_collectAttributes("_heading3","/toggle/","sahiText",_in(_div("refinements")));

//click on multiple filters available
for(var $i=0;$i<$Filters.length;$i++)
{	
	//click on filter
	_click(_heading3($Filters[$i]));
	
	if ($Filters[$i]=="Size")
		{
		//Filter Should Visible
		_assertVisible(_list("/(.*)/", _in(_div("/refinement  size/"))));
		//click on filter
		_click(_heading3($Filters[$i]));
		//it should not be visible
		_assertNotVisible(_list("/(.*)/", _in(_div("/refinement  size/"))));
		//select the size
		var $size=_getText(_link("/(.*)/", _in(_list("clearfix swatches size"))));
		_click(_link("/(.*)/", _in(_list("clearfix swatches size"))));
		var $sizeselected=_getText(_span("breadcrumb-refinement-value"));
		
		}
	else if ($Filters[$i]=="Price")
		{
		//Refinements should be visible
		_assertVisible(_listItem("/(.*)/", _in(_div("refinement price "))));
		//click on filter
		_click(_heading3($Filters[$i]));
		//Filter Should Collapse
		_assertNotVisible(_listItem("/(.*)/", _in(_div("refinement price "))));
		//price refinement
		var $pricerefinement=_getText(_link("/(.*)/",_in(_div("refinement price "))));
		//click on any price refinement
		_click(_link("/(.*)/",_in(_div("refinement price "))));
		//selected refinement should be visible in bread crumb
		_assertVisible(_span($pricerefinement, _in(_span("breadcrumb-refinement"))));
		}
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243881","Verify the UI of category grid page  in the application both as an anonymous and a registered user.");
$t.start();
try
{

//UI of category landing page
_assertVisible(_div("refinements"));
//category banner
_assertVisible(_div("banner-headache"));
_assertVisible(_div("content-info-plp"));
//products in plp
_assertVisible(_list("search-result-items"));
//18 products should be displayed per page
_assertEqual("18",_count("_div","product-image",_in(_list("search-result-items"))));
//pagination should be visible
_assertVisible(_div("pagination"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("2243885","Verify the pagination in category grid page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	
//search the product	
_click(_span("/search-icon spriteimg/"));
_setValue(_textbox("q", _in(_div("global-header-search"))),$Searchdata[0][0]);
_click(_submit("/Search/",_in(_div("global-header-search"))));

//.Suggested Categories : User should be directed to respective category listing page.
_assertVisible(_list("search-result-items"));
_assertVisible(_div("search-result-content"));
_assertContainsText("\" digital \"", _div("sone resulttext"));
var $ExpTotProd=_extract(_getText(_heading1("results-products")),"/[(](.*)[)]/",true);
_assertVisible(_div("view-all-items"));
_click(_link("viewallitems buttonctaone"));
var $actualProd=_count("_listItem","/(.*)/",_in(_div("search-result-content")));
_assretEqual($ExpTotProd,$actualProd);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243849/243850","Verify the functionality of 'Sale Pricing'/Verify the functionality of 'Sale Pricing' in Product Tile.");
$t.start();
try
{

//click on sub category 
_click(_link($Searchdata[0][0]));
//count of products
var $prodcount=_count("_div","product-tile",_in(_list("search-result-items")));

for (var $i=0;$i<$prodcount;$i++)
{
	_assertVisible(_span("product-sales-price["+$i+"]"));	
}	


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243851","Verify the navigation  of Out of Stock   in category list page");
$t.start();
try
{

//search product out of stock	
search($Searchdata[1][2]);
//verify out of stock
_assert(false,"should verify out of stock");
//

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("","");
$t.start();
try
{


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase("","");
$t.start();
try
{


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/