_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Header&Footer.xls");

var $headerdata=_readExcelFile("Header&Footer.xls","HeaderData");
var $footerdata=_readExcelFile("Header&Footer.xls","FooterData");
var $emailsignup=_readExcelFile("Header&Footer.xls","EmailSubscription");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();

var $t = _testcase("243726/243727","Verify the functionality on 'X' icon in hello bar./Verify the UI of Hello bar in header.");
$t.start();
try
{

//Visibility of hello bar after loading the application
_assertVisible(_div("header-banner"));
_assertVisible(_div("/header-slot/"));
_assertVisible(_div("menu-promoslot"));
_assertVisible(_link("bannerclose"));

//click on 'X' icon in hello bar
_click(_link("bannerclose"));

//hello bar should not visible
_assertNotVisible(_div("/header-slot/"));
_assertNotVisible(_div("menu-promoslot"));
_assertNotVisible(_link("bannerclose"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243723","Verify the UI of global header  in the application as an anonymous user.");
$t.start();
try
{
	
//Visibility of logo
_assertVisible(_image("/(.*)/", _in(_link($headerdata[0][4]))));
_assertVisible(_link($headerdata[0][4]));
//click on country change drop down
_assertVisible(_span("selected-states"));
_assertVisible(_div("current-country"));
_assertVisible(_span("flag-icon flag-icon-us"));
//Sign up link and my profile link
_assertVisible(_span("Sign up"));
_assertVisible(_span("My Account"));
_assertVisible(_link("Locate Stores"));
_assertVisible(_link("Go", _in(_div("global-header-search"))));
_assertVisible(_list("menu-category level-1"));
_assertVisible(_div("header-main"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("243728","Verify the application behavior on click  of Sign up.");
$t.start();
try
{

//click on sign up	
_click(_span("Sign up"));
//it should navigate to create account page
_assertVisible(_heading1("Create Account"));
_assertVisible(_textbox("dwfrm_profile_customer_firstname"));
_assertVisible(_textbox("dwfrm_profile_customer_lastname"));
_assertVisible(_textbox("dwfrm_profile_customer_email"));
_assertVisible(_password("/dwfrm_profile_login_password/"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243729/243751/243745","Verify the application behavior on click of ' My Profile'  link in the header as a anonymous user./Verify the  functionality  of  outside of the drop-down  In' MY PROFILE ' Drop down section.(Unauthenticated User).");
$t.start();
try
{

//click on my profile link	
_click(_span("My Account"));
//drop down should display
_assertVisible(_div("user-panel account"));
_assertVisible(_heading2("Sign In"));
_assertVisible(_textbox("/dwfrm_login_username/"));
_assertVisible(_password("/dwfrm_login_password/"));
_assertVisible(_checkbox("dwfrm_login_rememberme"));
_assertVisible(_label("Remember Me"));
_assertVisible(_link("Forgot Password?"));
_assertVisible(_submit("/dwfrm_login/"));
_assertVisible(_div("create-account"));
_assertVisible(_link("Create an Account"));

//click on outside of the dropdown
_click(_nav("navigation"));
//drop down should not display
_assertNotVisible(_div("user-panel account"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243747","Verify the Navigation of  'FORGOT PASSWORD'  link  of MY PROFILE ' Drop down section.");
$t.start();
try
{
	
//click on my profile link	
_click(_span("My Account"));
//click on reset password field
_click(_link("password-reset"));
//should navigate to forget password page
_assertVisible(_div("dialog-container"));
_assertVisible(_heading1($headerdata[2][3]));
//click on close button
_assertVisible(_button("Close"));
_click(_button("Close"));
//overlay shouldnot display
_assertNotVisible(_div("dialog-container"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("243730","Verify the application behavior when user click on Saje Logo.");
$t.start();
try
{

//Visibility of logo
_assertVisible(_image("/(.*)/", _in(_link($headerdata[0][4]))));
_assertVisible(_link($headerdata[0][4]));
//Mouse hover on logo	
_mouseOver(_link($headerdata[0][4]));
_assertVisible(_span("visually-hidden"));
_assertEqual($headerdata[0][4], _getText(_span("visually-hidden")));
//on click of home page logo application should load home page
_click(_link($URL[0][1]));
//Should navigate to home page
_assertVisible(_div("/header-image/"));
_assertVisible(_div("hompage-better-icon"));
_assertVisible(_div("product-listing-home"));
_assertVisible(_div("custom-rightimage"));
_assertVisible(_div("custom-videoimage"));
_assertVisible(_div("home-pagevideo"));
_assertVisible(_div($headerdata[1][4]));
_assertVisible(_div("instafeed"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("243732","Verify the application behavior on click of Store locator link in the header.");
$t.start();
try
{
	
//Navigate to home page
_click(_link($URL[0][1]));
//click on store locator link in header
_assertVisible(_link("Locate Stores"));
_click(_link("Locate Stores"));
_assertVisible(_heading1($headerdata[0][5]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243749","e navigation  of ' Sign In' button present in ' MY PROFILE ' Drop down section. If user  authentication Fails.");
$t.start();
try
{
	
//click on my profile link	
_click(_span("My Account"));
//enter in valid email and password
_setValue(_textbox("/dwfrm_login_username/"), $headerdata[0][1]);
_setValue(_password("/dwfrm_login_password/"), $headerdata[0][2]);
_click(_submit("dwfrm_login_login"));
//error message should display
_assertVisible(_span($headerdata[0][8]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243750","Verify the navigation of' Create Account' link In ' MY PROFILE ' Drop down section.");
$t.start();
try
{
	
//click on my profile link	
_click(_span("My Account"));
//click on create account link 
_click(_link("Create an Account"));
//should navigate to my account page
_assertVisible(_heading1("Create Account"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243784/243785/243783/243764/243794","Verify the functionality  of 'Contact Saje Icons/Verify the functionality  of Find A Store Icon/Verify the UI of global Footer in the application .");
$t.start();
try
{
	
//footer links in saje
var $footeelinks=_count("_div","/footer-icon-link/",_in(_div("footer-icon")));

for(var $i=0; $i<$footeelinks; $i++)
{
	
	//store locator link	
	_assertVisible(_div("footer-icon-link["+$i+"]"));
	
	if ($i==0)
		{
		//store locator link	
		_assertVisible(_link($headerdata[0][6]));
		_assertEqual($headerdata[0][7], _getText(_div("footer-icon-link")));
		//store locator link
		_click(_link($headerdata[0][6]));
		_assertVisible(_heading1($headerdata[0][5]));
		}
	else if ($i==1)
		{
		//email link
		_assertVisible(_span("HELLO@SAJE.COM"));
		_assertVisible(_link("Email Us"));
		
		}
	else if ($i==2)
		{
		//phone link
		_assertVisible(_span("/phone-link/"));
		_assertVisible(_link("Call Us"));
		}
	else
		{
		//chat link
		_assertVisible(_span("/chat-link/"));
		_assertVisible(_link("Click to Live Chat"));
		}
	
	//Navigate to home page
	_click(_link($URL[0][1]));
	
}

//gaurantee section
_assertVisible(_heading3("Our guarantee"));
_assertVisible(_div("gauranty"));
_assertVisible(_div("guaranty-img"));
//about sage section
_assertVisible(_div("/About saje/"));
//need help section
_assertVisible(_div("/Need Help?/"));
//follow us section
_assertVisible(_div("/Follow Us/"));
//email section in footer
_assertVisible(_textbox("dwfrm_emailsubscribe_email"));
_assertVisible(_div("Stay connected Sign Up"));
_assertVisible(_submit("Sign Up"));
//privacy policy link
_assertVisible(_link("Privacy Policy"));
//live chat link
_assertVisible(_link("Terms of Use"));
//copy rights
_assertVisible(_div("Copyright 2016 Saje Natural Business Privacy Policy Terms of Use"));
_assertEqual($headerdata[0][3], _getText(_div("gauranty")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("243790/243791/243787/243788/261342","Verify the Navigation of 'Contact Us' Section.' list links of Footer section./Verify the Navigation of 'Need Help ' Section.' list links of Footer section./Verify the UI Contents of About Saje Section in Large view port.");
$t.start();
try
{

//Navigate to home page
_click(_link($URL[0][1]));
	
var $aboutsajelinks=_collectAttributes("_link","/(.*)/","sahiText", _in(_div("/About saje/")));

//check the footr links	
for(var $i=0; $i<$aboutsajelinks.length; $i++)
{

	//_assertVisible(_link($aboutsajelinks[$i]));
	//click on footer links under about saje
	_click(_link($aboutsajelinks[$i]));
	
	if ($i==0)
		{
		//Visibility of About saje section
		_assertVisible(_div($footerdata[$i][1]));
		}
	else if ($i==1 || $i==3)
		{
		_assertVisible(_heading1($footerdata[$i][1]));
		}
	else 
		{
		_assertVisible(_heading3($footerdata[$i][1]));
		}
	

	//BreadCrumb 
	//_assertEqual($footerdata[$i][0], _getText(_link("breadcrumb-element")));
	//Heading
	//_assertVisible(_heading1($footerdata[$i][1]));
	
	//Navigate to home page
	_click(_link($URL[0][1]));
}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243789/261343","Verify the UI Contents of Need Help Section./Verify the navigation related to 'FAQ's' link  present in footer of application both as an  Anonymous and Registered user.");
$t.start();
try
{

var $aboutNeedhelp=_collectAttributes("_link","/(.*)/","sahiText", _in(_div("/Need Help?/")));
//check the footr links	
for(var $i=0; $i<$aboutNeedhelp.length; $i++)
{

	//Navigate to home page
	_click(_link($URL[0][1]));
	//Visibility of need help section
	_assertVisible(_link($aboutNeehhelp[$i]));
	//click on footer links under about saje
	_click(_link($aboutNeehhelp[$i]));
	//Heading
	_assertVisible(_heading1($footerdata[$i][3]));
}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243790/243793","Verify the Navigation of 'Contact Us' Section.' list links of Footer section./Verify the UI Contents of  Follow Us Section for device");
$t.start();
try
{
		
var $aboutNeehhelp=_collectAttributes("_link","/(.*)/","sahiText", _in(_div("/Follow Us/")));
//check the footr links	
for(var $i=0; $i<$aboutNeehhelp.length; $i++)
{

	//Navigate to home page
	_click(_link($URL[0][1]));
	//click on footer links under about saje
	_click(_link($aboutsajelinks[$i]));
	//Heading
	_assertVisible(_heading1($footerdata[$i][5]));
}

_assert(false,"should update for Ipad also");
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243740/243720/243736/243737/243739/243741","Verify the Functionality of 'X' icon in modal window/Verify the functionality of 'Ok' button in the modal window/Verify the UI of Country selector dropdown." +
		"Verify the functionality of  'No, I stay on this page' link in modal window.");
$t.start();
try
{
	
//click on country change drop down
_assertVisible(_span("selected-states"));
_assertVisible(_div("current-country"));
_assertVisible(_span("flag-icon flag-icon-us"));
//select canada state
_click(_div("current-country"));
_wait(2000);
_assertEqual($countrysel[1][0], _getText(_div("sub-counrty")));
_assertVisible(_span("flag-icon flag-icon-ca"));
	
//Add item to cart
addItemToCart($headerdata[0][0],1);
//after adding product the cart quantity should be one
_assertEqual("1", _getText(_span("minicart-quantity")));
//change country to canada
CountrySelection($countrysel[1][0]);
//pop up should display and count of cart products should not display
_assertVisible(_div("dialog-container"));
_assertVisible(_link("buttonctaone",_in(_div("dialog-container"))));
_assertVisible(_link("popup-nothanks-link",_in(_div("dialog-container"))));
_assertEqual($headerdata[0][7], _getText(_link("popup-nothanks-link")));
_click(_link("buttonctaone",_in(_div("dialog-container"))));
_assertNotVisible(_div("dialog-container"));
//Should Navigate to home page
_assertVisible(_div("homepage-banner"));
//after adding product the cart quantity should be one
_assertEqual("0", _getText(_span("minicart-quantity")));
//change country to US
CountrySelection($countrysel[0][0]);

//navigate to PDP
NavigatetoPDP($subcat);

//Selection of CANADA	
_click(_div("current-country"));
_click(_link($countrysel[1][0]));
_click(_link($headerdata[1][7], _in(_div("popup-nothanks"))));
_assertVisible(_div($countrysel[0][0]));

//Verification fo PDP
_assertVisible(_heading1("/product-name/",_in(_div("product-col-2 product-detail"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//################ For Register User ######################

var $t = _testcase("243748","Verify the functionality of ' Sign In' button present in ' MY PROFILE ' Drop down section.");
$t.start();
try
{
	
//click on my profile link	
_click(_span("My Account"));
//login to the application
login();
//should logged in successfully
//_assertVisible(_heading1("My Account Saje Pfs1 Logout"));
_assertVisible(_heading1("My Account "+$FName+" "+$LName+" "+"Logout"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243759","Verify the Functionality of 'Click outside of  the drop-down ' of    My Profile   navigation menu options section.( Authenticated User).");
$t.start();
try
{

//click on hello drop down
_click(_span("/Hello/"));
//dropdown should display
_assertVisible(_div("user-panel account"));
//click on outside of the dropdown
_click(_nav("navigation"));
//dropdown should not display
_assertNotVisible(_div("user-panel account"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243752/243754/243755/243756/243757/243758/243735","Verify the  Navigation  of 'MY ACCOUNT ' link of  My Profile navigation menu Options.");
$t.start();
try
{
	
//click on hello message
_click(_span("/Hello/", _in(_link("user-account"))));

var $MyaccountDropdown=_collectAttributes("_link","/(.*)/","sahiText",_in(_div("user-links")));	

for (var $i=0;$i<$MyaccountDropdown.length;$i++)
{
	_assertVisible(_link($headerdata[$i][9], _in(_div("/user-panel/"))));
}

for (var $i=0;$i<$MyaccountDropdown.length;$i++)
	{
	_click(_link($MyaccountDropdown[$i]));
	
	if ($i==0)
		{
		_assertVisible(_heading1("My Account "+$FName+" "+$LName+" Logout"));
		_assertVisible(_list("account-options"));
		}
	else if ($i==1)
		{
		_assertVisible(_heading1("Addresses"));
		_assertVisible(_link("Create New Address"));
		}
	else if ($i==2)
		{
		_assertVisible(_heading1("Credit Card Information"));
		_assertVisible(_link("Add Credit Card"));
		}
	else if ($i==3)
		{
		_assertVisible(_heading1("Credit Card Information"));
		_assertVisible(_link("Add Credit Card"));
		}
	else if ($i==4)
		{
		_assertVisible(_link("Add Credit Card"));
		_assertVisible(_link("Order History", _in(_div("breadcrumb"))));
		}
	else
		{
		_assertVisible(_link("SIGN UP"));
		_assertVisible(_link("My Account"));
		//Should Navigate to home page
		_assertVisible(_div("homepage-banner"));
		}
	
	}


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243795","Verify the Email Entry Label.");
$t.start();
try
{
	
//email section in footer
_assertVisible(_textbox("dwfrm_emailsubscribe_email"));
_assertVisible(_div("Stay connected Sign Up"));
_assertVisible(_submit("Sign Up"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243724","Verify the UI of global header  in the application  as an Registered user.");
$t.start();
try
{
	
//Visibility of logo
_assertVisible(_image("/(.*)/", _in(_link("Saje Natural Wellness"))));
_assertVisible(_link("Saje Natural Wellness"));	
//searsh icon
_assertVisible(_link("Search"));
//click on store locator link in header
_assertVisible(_link("Locate Stores"));
//cart icon
_assertVisible(_link("/mini-cart-link/"));
//country dropdown
_assertVisible(_div("saje-country-selector"));
//Root categories should be displayed
_assertVisible(_list("menu-category level-1"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243761","Verify the UI of global navigation menu in application for both registered and anonymous");
$t.start();
try
{

//fetching color before mouse hover	
var $colorBeforeMouseHover=_style(_link("Electronics"),"color");
//mouse hover on electronics category	
_mouseOver(_link("Electronics"));
//fetching color after mouse hover
var $colorAfterMouseHover=_style(_link("Electronics"),"color");
//color should not match 
_assertEqual($colorBeforeMouseHover,$colorAfterMouseHover);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243762/243763/243764","Verify the UI of global navigation menu in application for both registered and anonymous /Verify the UI of global navigation menu in application for selected categories with Arrow  Note for Both Registered and Anonymous need to be validated./Verify the global navigation menu for sub category grid positioning. Note for Both Registered and Anonymous need to be validated.");
$t.start();
try
{
	
var $CountOfSubCatLinks=_count("_link","/(.*)/", _in(_list("menu-vertical")));

//sub category links
_assertVisible(_list("menu-vertical[1]"));
//sub category links
_assertVisible(_listItem("has-level-3"));
_assertVisible(_div("menu-header"));

//product image in right pane
_assertVisible(_image("/(.*)/",_under(_div("right-container"))));

//Category name should be displayed in left side of flyout.
_assertVisible(_div("menu-slot[1]"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243796","Verify the Validation of Email Field in Footer section.");
$t.start();
try
{
	
//Navigate to home page
_click(_link($URL[0][1]));

for(var $i=0; $i<$emailsignup.length; $i++)
{
	_setValue(_textbox("dwfrm_emailsubscribe_email"),$emailsignup[$i][1]);
	_click(_submit("home-email"));
	// The Defect Need to be fixed for Email Sign up functionality in Home page		
	if($i==0)
		{
			//Text box should be highlighted with red color
			_assertEqual($emailsignup[0][3],_style(_textbox("dwfrm_emailsubscribe_email"),"background-color"));
		}
	else if($i==1)
		{
			//Verifying Error message and Font Color of Error message
			_assertEqual($emailsignup[$i][2],_getText(_span("error", _in(_div("home-bottom")))));
			_assertEqual($emailsignup[1][3],_style(_span("error", _in(_div("home-bottom"))),"color"));			
		}
	else
		{
			//Valid
			_assertNotVisible(_span("error", _in(_div("home-bottom"))));
		}
}		

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end(); 

var $t = _testcase("243918","Verify the  ' Search term message ' of Search Results Products in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	
//click on search icon
_click(_link("Search"));
//verify the source message
_assertEqual($headerdata[1][3],_getText(_textbox("q", _in(_div("global-header-search")))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243733","Verify the application behavior on click  of Search icon.");
$t.start();
try
{
	
//click on search icon
_click(_span("/search-icon spriteimg/"));
//search text box should be visible
_assertVisible(_textbox("q", _in(_div("global-header-search"))));
_assertVisible(_submit("Search", _in(_div("Search CatalogSearch"))));
_assertVisible(_span("spriteimg mark", _in(_div("global-header-search"))));
_assertVisible(_link("Search", _in(_div("global-header-search"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243792","Verify the Navigation of 'Follow Us ' Section.' list links of Footer section.");
$t.start();
try
{
	
//capture all the follow us links from footer
var $FooterlinksInFooter=_collectAttributes("_link","/(.*)/","sahiText",_under(_heading3("Follow Us")));

//verifying saje blog
_click(_link($FooterlinksInFooter[0]));
var $url;
_set($url, window.location.href);
_assertEqual($url,$footerdata[0][3]);


for (var $i=1;$i<$FooterlinksInFooter.length;$i++)
{

	_click(_link($FooterlinksInFooter[$i]));
	
	
	var $recentWinID;
	$recentWinID=_getRecentWindow().sahiWinId;
	_log($recentWinID);
	_selectWindow($recentWinID);
	
if ($i==1)
	{
	var $url;
	_set($url, window.location.href);
	_assertEqual($url,$footerdata[$i][3]);
	}
else if ($i==2)
	{
	var $url;
	_set($url, window.location.href);
	_assertEqual($url,$footerdata[$i][3]);
	}
else if ($i==3)
	{
	var $url;
	_set($url, window.location.href);
	_assertEqual($url,$footerdata[$i][3]);
	}
else if ($i==4)
	{
	var $url;
	_set($url, window.location.href);
	_assertEqual($url,$footerdata[$i][3]);
	}
else if ($i==5)
	{
	var $url;
	_set($url, window.location.href);
	_assertEqual($url,$footerdata[$i][3]);
	}
	else
		{
		var $url;
		_set($url, window.location.href);
		_assertEqual($url,$footerdata[$i][3]);
		}

//closing window after verification	
_closeWindow();
_selectWindow();
	
//clicking on browser back button
//_call(window.history.back());
//Or call site urls every time
//SiteURLs()

}


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243725","Verify the  functionality of country selector dropdown");
$t.start();
try
{

//click on country selector dropdown to US
CountrySelection($countrysel[0][0]);
//click on sub cat
_click(_link($headerdata[0][0]));
//Change country to Canada
CountrySelection($countrysel[1][0]);
//Application is navigating to home page
_assertVisible(_div("header-banner"));
_assertVisible(_div("homepage-banner"));
_assertVisible(_div("homepage-sideimages"));
//_assert(false,"Not developed that y not checking in all pages");
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase("","");
$t.start();
try
{
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/


