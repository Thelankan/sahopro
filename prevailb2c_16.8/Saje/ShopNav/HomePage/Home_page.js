_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("HomaPage.xls");

var $homepageData= _readExcelFile("HomaPage.xls","HomePageData");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();

var $t = _testcase("243801/243802/243840","Verify the UI of body of the home page in the application both as an anonymous and a registered user./Verify the UI of Hero Content slot present in below to the Header part./Verify the content asset of Natural Healing Section.");
$t.start();
try
{
	//click on the logo
	_click(_link("/(.*)/", _in(_div("primary-logosaje"))));
	//navigate to the home page
	_assertVisible(_div("homepage-banner"));
	//The Home slider images must be displayed below the Header.
	
	//Fell Better MAP must display.
	_assertVisible(_image("/feelbetter/"));
	//Content asset 'OUR BEST SELLERS' must display.
	_assertVisible(_div($homepageData[0][0]));
	//'SHOP ALL BEST SELLERS ' CTA must display.
	_assertVisible(_div($homepageData[1][0]));
	//Feature Content Slot 'FRESHandBREZY' must display which also contains below button.
	_assertVisible(_div($homepageData[2][0]));
	//'SHOP KATES FAVES' link.
	_assertVisible(_link($homepageData[3][0]));
	//Should contain Saje Community heading.
	_assertVisible(_div($homepageData[4][0]));
	//Instagram photo carousel - Large viewports must display.
	_assert(false,"Instagram photo carousel - Large viewports must display.");
	//Instagram name should display as'@SAJEWELLNESS'.
	_assert(false,"Instagram name should display as'@SAJEWELLNESS'."),
	//Instagram description must display.
	_assert(false,"Instagram description must display.");
	//hero slot present under header section 
	_assertVisible(_image("hero", _near(_div("header-main"))));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("243806/243817","Verify the UI contains of FELL BETTER Section./Verify the UI contains of OUR BEST SELLER Section.");
$t.start();
try
{
	var $FeelBetter=_collectAttributes("_area","/poly/","class",_in(_div("imagewrap")));
	for(var $i=0;$i<$FeelBetter.length;$i++)
		{
		_assertVisible(_area($FeelBetter[$i],_in(_div("imagewrap"))));
		}
	//Product Image should display.
	_assertVisible(_image("/(.*)/", _in(_div("/product-list-home/"),_under(_heading2("our best sellers")))));
	//Product name should display.
	
	//Product Name Extension should be present.
	_log(false,"Product extension should display");
	//Product Description should display.
	_assertVisible(_div("product-description", _in(_div("/product-list-home/"),_under(_heading2("our best sellers")))));
	//Price should be display.
	//SHOP ALL BEST SELLERS button should display.
	_assertVisible(_link($homepageData[1][0]));
	//Back ground images should be display.
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("243834/243835/243836","Verify the system response on click on Product Image of Best Seller Section./Verify the system response on click on Product Name Extension .");
$t.start();
try
{
	var $ImgName=_extract(_getAttribute(_image("desktop-only", _in(_div("product-home-image ipad-image"))),"alt"),"/(.*) [,]/",true);
	_click(_image("desktop-only", _in(_div("product-home-image ipad-image"))));
	_assertVisible(_div("pdpMain"));
	_assertEqual( $ImgName, _getText(_heading1("product-name", _in(_div("desktop-only")))));
	//click on the logo
	_click(_link("/(.*)/", _in(_div("primary-logosaje"))));
	var $prodNamelink=_getAttribute(_image("desktop-only", _in(_div("product-home-image ipad-image"))),"alt")
	
	
	//_click(_link("/(.*)/", _in(_div("product-description"))));
	_click(_link("Love our easy care solid shirting? Then give our stripes a try! This subtle blue stripe is a nice option to have in your closet for workplace wear and it even has a bit of stretch for extra comfort.", _in(_div("product-description"))));
	_assertVisible(_div("pdpMain"));
	_assertEqual("Love our easy care solid shirting? Then give our stripes a try! This subtle blue stripe is a nice option to have in your closet for workplace wear and it even has a bit of stretch for extra comfort.", _getText(_div("product-detail-info")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("243837","Verify the system response on click on Product Description of Best Seller Section.");
$t.start();
try
{
	//Click on Product Description.
	_click(_link("/(.*)/", _in(_div("product-description",_near(_div("our best sellers"))))));
	//.Should Navigate to Product Detail Page(PDP) Page.
	_assertVisible(_div("pdpMain"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("243722","Verify the system response on click of 'Store locator' icon at global header in the application both as an anonymous and a registered user");
$t.start();
try
{
	//Click on 'Store locator' link from global header
	_click(_link("Locate Stores", _in(_div("header-main"))));
	//verfiy the navigation
	_assertVisible(_heading1($homepageData[0][1]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243797","Verify the system response on click of 'Copyright and Legal' link at global footer in the application both as an anonymous and a registered user");
$t.start();
try
{
	 //Click on 'Copyright and Legal' link from global Footer
	_click(_link($homepageData[0][2], _in(_div("footer-copyright-legal"))));
	 //verify the navigation
	_assertVisible(_heading1($homepageData[1][2]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243798","Verify the system response on click of 'Privacy policy' link at global footer in the application both as an anonymous and a registered user.");
$t.start();
try
{
 //Click on 'Privacy policy' link from global Footer.
_click(_link($homepageData[0][3]));
 //vrify the navigation
_assertEqual($homepageData[0][3], _getText(_heading1("content-header")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243817","Verify the UI  contains of OUR BEST SELLER Section.");
$t.start();
try
{

//Our best seller section
var $productsinBestSellerSection=_count("_div","product-home-image ipad-image",_in(_div("product-listing-home")));

for(var $i=0; $i<$productsinBestSellerSection; $i++)
{
	//product name should display
	_assertVisible(_link("/(.*)/", _in(_div("product-home-details ipad-details["+$i+"]"))));
	//product Image should display
	_assertVisible(_image("/(.*)/", _in(_div("product-home-image ipad-image["+$i+"]"))));
	//product description
	_assertVisible(_div("product-description", _in(_div("product-home-details ipad-details["+$i+"]"))));
}

//shop all best sellers section
_assertVisible(_link("shop all best sellers"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243818","Verify the systems' response when user clicks on the SHOP ALL BEST SELLERS CTA .");
$t.start();
try
{
	
//click on shop all best sellers section
_click(_link("shop all best sellers"));
//user should be navigate to configured caegory
_assert(false,"link is not configured yet");

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243804","Verify the system response when user clicks on any section in the feel better map");
$t.start();
try
{
	
//click on any link in fell better map
_click(_area("stress"));
//should navigate to configred page
_assert(false,"links should configured");

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243824","Verify the functionality when user clicks on a image at the LEFT position of the image carousel in the SAJE COMMUNITY section.");
$t.start();
try
{
	
//


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase("","");
$t.start();
try
{
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/

var $t = _testcase("243721","Verify the UI on mouse hover of root categories.");
$t.start();
try
{

var $categories=_collectAttributes("_link","has-sub-menu","sahiText",_in(_list("menu-category level-1")));

for(var $i=0; $i<$categories.length; $i++)
{
	_mouseOver(_link($categories[$i]));
	_assertVisible(_list("menu-vertical["+$i+"]"));
}


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

