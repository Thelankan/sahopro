_include("../../util.GenericLibrary/GlobalFunctions.js");
//_resource("Header&Footer.xls");

//var $headerdata=_readExcelFile("Header&Footer.xls","HeaderData");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();

var $t = _testcase("243801/243802","Verify the UI of body of the home page in the application both as an anonymous and a registered user./Verify the UI of Hero Content slot present in below to the Header part.");
$t.start();
try
{
	//click on the logo
	_click(_link("/(.*)/", _in(_div("primary-logosaje"))));
	//navigate to the home page
	_assertVisible(_div("homepage-banner"));
	//The Home slider images must be displayed below the Header.
	
	//Fell Better MAP must display.
	_assertVisible(_image("/feelbetter/"));
	//Content asset 'OUR BEST SELLERS' must display.
	_assertVisible(_heading2("our best sellers"));
	//'SHOP ALL BEST SELLERS ' CTA must display.
	_assertVisible(_div("shop all best sellers"));
	//Feature Content Slot 'FRESHandBREZY' must display which also contains below button.
	_assertVisible(_div("Fresh & Breezy"));

	//'SHOP KATES FAVES' link.
	_assertVisible(_link("shop kate's faves"));
	//Should contain Saje Community heading.
	_assertVisible(_heading2("saje community"));
	//Instagram photo carousel - Large viewports must display.
	_assert(false,"Instagram photo carousel - Large viewports must display.");
	//Instagram name should display as'@SAJEWELLNESS'.
	_assert(false,"Instagram name should display as'@SAJEWELLNESS'."),
	//Instagram description must display.
	_assert(false,"Instagram description must display.");
	//hero slot present under header section 
	_assertVisible(_image("hero", _near(_div("header-main"))));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("243806/243817","Verify the UI contains of FELL BETTER Section./Verify the UI contains of OUR BEST SELLER Section.");
$t.start();
try
{
	var $FeelBetter=_collectAttributes("_area","/poly/","class",_in(_div("imagewrap")));
	for(var $i=0;$i<$FeelBetter.length;$i++)
		{
		_assertVisible(_area($FeelBetter[$i],_in(_div("imagewrap"))));
		}
	//Product Image should display.
	_assertVisible(_image("/(.*)/", _in(_div("/product-list-home/"),_under(_heading2("our best sellers")))));
	//Product name should display.
	
	//Product Name Extension should be present.
	
	//Product Description should display.
	_assertVisible(_div("product-description", _in(_div("/product-list-home/"),_under(_heading2("our best sellers")))));
	//Price should be display.
	//SHOP ALL BEST SELLERS button should display.
	_assertVisible(_link("shop all best sellers"));
	//Back ground images should be display.
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("243834/243835/243836","Verify the system response on click on Product Image of Best Seller Section./Verify the system response on click on Product Name Extension .");
$t.start();
try
{
	var $ImgName=_extract(_getAttribute(_image("desktop-only", _in(_div("product-home-image ipad-image"))),"alt"),"/(.*) [,]/",true);
	_click(_image("desktop-only", _in(_div("product-home-image ipad-image"))));
	_assertVisible(_div("pdpMain"));
	_assertEqual( $ImgName, _getText(_heading1("product-name", _in(_div("desktop-only")))));
	//click on the logo
	_click(_link("/(.*)/", _in(_div("primary-logosaje"))));
	var $prodNamelink=_getAttribute(_image("desktop-only", _in(_div("product-home-image ipad-image"))),"alt");
	
	
	//_click(_link("/(.*)/", _in(_div("product-description"))));
	_click(_link("Love our easy care solid shirting? Then give our stripes a try! This subtle blue stripe is a nice option to have in your closet for workplace wear and it even has a bit of stretch for extra comfort.", _in(_div("product-description"))));
	_assertVisible(_div("pdpMain"));
	_assertEqual("Love our easy care solid shirting? Then give our stripes a try! This subtle blue stripe is a nice option to have in your closet for workplace wear and it even has a bit of stretch for extra comfort.", _getText(_div("product-detail-info")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("243837","Verify the system response on click on Product Description of Best Seller Section.");
$t.start();
try
{
	//Click on Product Description.
	_click(_link("/(.*)/", _in(_div("product-description"))));
	//.Should Navigate to Product Detail Page(PDP) Page.
	_assertVisible(_div("pdpMain"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("243722","Verify the system response on click of 'Store locator' icon at global header in the application both as an anonymous and a registered user");
$t.start();
try
{
	//Click on 'Store locator' link from global header
	_click(_link("Locate Stores", _in(_div("header-main"))));
	//verfiy the navigation
	_assertVisible(_heading1("Find a Store Near You"));
	


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243797","Verify the system response on click of 'Copyright and Legal' link at global footer in the application both as an anonymous and a registered user");
$t.start();
try
{
	 //Click on 'Copyright and Legal' link from global Footer
	_click(_link("Terms of Use", _in(_div("footer-copyright-legal"))));
	 //verify the navigation
	_assertVisible(_heading1("Terms & Conditions of Sale"));

	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("243798","Verify the system response on click of 'Privacy policy' link at global footer in the application both as an anonymous and a registered user.");
$t.start();
try
{
	 //Click on 'Privacy policy' link from global Footer.
	_click(_link("Privacy Policy"));
	 //vrify the navigation
	_assertEqual("Privacy Policy", _getText(_heading1("content-header")));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("","");
$t.start();
try
{



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("","");
$t.start();
try
{
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("","");
$t.start();
try
{
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

