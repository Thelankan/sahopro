_include("BrowserSpecific.js");
_include("BM_Functions.js");
_resource("User_Credentials.xls");


//Excel sheet declaration
var $URL=_readExcelFile("User_Credentials.xls","URL");
var $userLog=_readExcelFile("User_Credentials.xls","BM_Login");
var $countrysel=_readExcelFile("User_Credentials.xls","CountrySelection");
//Address and card declerations 
var $shippingAddress=_readExcelFile("User_Credentials.xls","ShippingAddressCheckout");
var $billingAddress=_readExcelFile("User_Credentials.xls","BillingAddressCheckout");
var $addressProfile=_readExcelFile("User_Credentials.xls","AddressProfile");
var $cardDetailsProfile=_readExcelFile("User_Credentials.xls","CardDetailsProfile");
var $cardDetailsCheckout=_readExcelFile("User_Credentials.xls","CardDetailsCheckout");

//global variables
var $pName;
var $selectedSize;
var $TotalNoOfPages;
var $currentpage;
var $PLPProdName;
var $currencysymbool=$countrysel[0][1];
var $prodNameincart;
var $QuantityInCart;
var $prodpriceincart;
var $totalPrice;
var $orderTotal;


//############################### Saje Functions ##################################

function onScriptError()
{
  _log("Error log"); 
  _lockWindow(5000);
  _focusWindow();
  _takePageScreenShot();
  _unlockWindow();
}

function onScriptFailure()
{
  _log("Failure log");
  _lockWindow(5000);
  _focusWindow();
  _takePageScreenShot();
  _unlockWindow();
} 

//Saje Generic Functions
function SiteURLs() {
	
_navigateTo($URL[0][0]);
if(_isVisible(_submit("Authenticate")))
	{
	_setValue(_textbox("authUser"), "storefront");
	_setValue(_password("authPassword"), "sajeus2016");
	_click(_submit("Authenticate"));
	}
_wait(6000);

//close the popup	
if (_isVisible(_div("dialog-container")))
	{
	_click(_button("Close"));
	}

if (isMobile())
	{
	_click(_submit("Menu"));
	if (!_isVisible(_div($countrysel[0][0],_in(_div("country-code")))))
		{
		_click(_span("/drop-icon spriteimg/",_in(_div("country-code"))));
		_click(_link($countrysel[0][0], _in(_div("country-code"))));
		_click(_link("OK", _in(_div("dialog-container"))));
		_click(_span("/drop-icon spriteimg/",_in(_div("country-code"))));
		_assertVisible(_div($countrysel[0][0],_in(_div("country-code"))));
		}
	}

	else
		{
		//Selection of United states	
		if (!_isVisible(_div($countrysel[0][0],_in(_div("/saje-country-selector/")))))
		{
			_click(_div("current-country"));
			_click(_link($countrysel[0][0]));
			_click(_link("buttonctaone", _in(_div("dialog-container"))));
			_assertVisible(_div($countrysel[0][0]));
		}
		}

}

function deleteProvidedUser($providedemail)
{
	BM_Login();
_click(_link("Customers"));
_click(_link("Customers[1]"));
_click(_link("Advanced"));
_setValue(_textbox("WFCustomerAdvancedSearch_Email"),$providedemail);
_click(_submit("Find", _in(_div("/Advanced Customer Search/"))));
if(_isVisible(_row("/Select All/")))
{
	if(_assertEqual($providedemail,_getText(_cell("table_detail e s[4]"))))
		{
		_click(_checkbox("DeleteCustomer"));
		_click(_submit("Delete"));
		_click(_submit("OK"));
		_log("Successfully deleted the user")
		}
}
else
{
	_log("search did not match any users.");
}
_click(_link("Log off."));

}


function cleanup()
{
	//Clearing the items from cart
	ClearCartItems();
	if(isMobile())
	{
		_click(_submit("/menu-nav-toggle/"));
	}
	//logout from the application
	logout();
	navigateToHomePage();
}

function logout()
{
	
	if(!isMobile())
		{
			if(_isVisible(_span("/Hello/",_in(_link("user-account")))))
			{	
				_click(_span("/Hello/",_in(_link("user-account"))));
				_click(_link("/user-logout/"));
			}
		}
		//logout from the application
	else
		{
		if(_isVisible(_span("/Hello/",__in(_div("sign-right js-device-header-login")))))
			{
			_submit("Menu")
			//_click(_submit("menu-nav-toggle menu-toggle"));
			_click(_span("/Hello/", _in(_div("sign-right js-device-header-login"))));
			_assertVisible(_div("user-panel account", _in(_div("sign-right js-device-header-login"))));
			_click(_link("/user-logout/",_in(_div("sign-right js-device-header-login"))));
			_wait(3000);
			}
		
		}
}

function ClearCartItems()

{        

    if (_isVisible(_div("mini-cart-link js-header-mini-cart")))
    {               
            //_click(_span("minicart-icon spriteimg"));
            _click(_link("/mini-cart-link-cart/"));
            _wait(3000);
            while (_isVisible(_submit("/remove button/")))

            {
                    _click(_submit("Remove"));
                    _wait(3000); 
            }
            while (_isVisible(_submit("dwfrm_cart_coupons_i0_deleteCoupon")))
                    {
                    _click(_submit("dwfrm_cart_coupons_i0_deleteCoupon"));
                    }

    }

}


//############################### Saje ShopNav Functions  ##################################

function navigateToHomePage()
{
	//Navigate to home page
	_click(_link($URL[0][1]));
	_wait(3000);
}


function CountrySelection($selection)
{

	if (isMobile())
	{
	_click(_submit("Menu"));
	if (!_isVisible(_div($selection,_in(_div("country-code")))))
		{
		_click(_span("/drop-icon spriteimg/",_in(_div("country-code"))));
		_click(_link($selection, _in(_div("country-code"))));
		_click(_link("OK", _in(_div("dialog-container"))));
		_click(_span("/drop-icon spriteimg/",_in(_div("country-code"))));
		_assertVisible(_div($selection,_in(_div("country-code"))));
		}
	}

	else
		{
		//Selection of United states	
		if (!_isVisible(_div($selection,_in(_div("/saje-country-selector/")))))
		{
			_click(_div("current-country"));
			_click(_link($selection));
			_click(_link("buttonctaone", _in(_div("dialog-container"))));
			_assertVisible(_div($selection));
		}
		}

}

function NavigatetoSubCategory($subcat)
{

//click on any sub category
_click(_link($subcat));
_log("we are in sub category landing page");

}

function NavigatetoPDP($subcat)
{

//search the product
search($subcat);
//click on name link if it is visible
if (_isVisible(_link("name-link",_in(_list("search-result-items")))))
	{
	$PLPProdName=_getText(_link("name-link"));
	//click on any product name link  in plp
	_click(_link("name-link"));
	_assertEqual($PLPProdName, _getText(_heading1("/product-name/",_in(_div("product-col-2 product-detail")))));
	}

//Verification fo PDP
_assertVisible(_heading1("/product-name/",_in(_div("product-col-2 product-detail"))));

//bread crumbs are not available through out the site
//_assertVisible(_span("breadcrumb-element"));
//_assertEqual($pName, _getText(_span("breadcrumb-element")));


}

function selectSwatch()
{
	if (_isVisible(_div("size-text")))
	{
	
 	var $size= _count("_link","swatchanchor",_in(_listItem("/attribute/")));
	
	if ($size>1)
		{
				if (!_isVisible(_listItem("selectable selected",_in(_div("product-variations")))))
					{
					//select any swatch
					_click(_link("swatchanchor",_in(_div("product-variations"))));
					}
		}
	else
		{
		_log("swatch is pre selected");
		}
		
	}

}

/*function selectSwatch()
{
	//select size
	if (_isVisible(_div("size-text")))
	{
		
		if (!_isVisible(_listItem("selectable selected",_in(_div("product-variations")))))
			{
			//select any swatch
			_click(_link("swatchanchor",_in(_div("product-variations"))));
			}
		
	}
}*/

//search related function
function search($product)
{
	
	_click(_span("/spriteimg/", _in(_span("search-icon"))));
	_wait(2000);
	_setValue(_textbox("q", _in(_div("global-header-search"))),$product);
	_wait(2000);
	_click(_submit("/Search/",_in(_div("global-header-search"))));
	_wait(5000);
	
}

//--------Pagination function----------//
function pagination()
{  
	
//var $currentpage=_getText(_listItem("current-page"));
//_assertEqual("1",$currentpage);
//
//while (_isVisible(_link("Next Page")))
//{
//_click(_link("Next Page"));
//}

//Fetching number of the products 	
var $totalNoofproducts=_extract(_getText(_link("viewallitems buttonctaone")),"/L (.*) I/",true);
//Fetching number of pages	
$TotalNoOfPages=parseInt($totalNoofproducts/18);

_log("Total Number of pages="+$TotalNoOfPages);

if ($TotalNoOfPages%18!=0)
	{
	$TotalNoOfPages=$TotalNoOfPages+1;
	_log("updated total no of pages"+$TotalNoOfPages);
	}

//verifying pages
for (var $i=1;$i<$TotalNoOfPages+1;$i++)
{
	
if($i==1)
{
	//previous link disabled
	_assertVisible(_listItem("first-last-disable"));
	//next link is visible
	_assertVisible(_listItem("Next Page"));
	_assertEqual($i,_getText(_listItem("current-page")));
}

else if ($i==$TotalNoOfPages)
{
	_click(_link("Next Page"));
	//previous link should be visible
	_assertVisible(_link("page-previous"));
	//next link not visible 
	_assertVisible(_listItem("first-last-disable-last"));
}
else
	{
	_assertVisible(_listItem("Next Page"));
	_click(_link("Next Page"));
	_assertEqual($i,_getText(_listItem("current-page")));
	_assertVisible(_link("page-previous"));
	}
}
	
for (var $j=$TotalNoOfPages;$j>=1;$j--)
{

if($j==$TotalNoOfPages)
{	
	_assertEqual($TotalNoOfPages,_getText(_listItem("current-page")));
	//previous link should be visible
	_assertVisible(_link("page-previous"));
	//next link not visible 
	_assertVisible(_listItem("first-last-disable-last"));
}

else if ($j==1)
{
_click(_link("page-previous"));	
//previous link disabled
_assertVisible(_listItem("first-last-disable"));
//next link should be disabled
_assertVisible(_listItem("Next Page"));

}

else
	{
	_assertVisible(_link("page-previous"));
	_click(_link("page-previous"));
	_assertVisible(_listItem("Next Page"));
	_assertEqual($j,_getText(_listItem("current-page")));
	}
}


for (var $i=1;$i<$TotalNoOfPages+1;$i++)
{

if($i==1)
{	
	//previous link disabled
	_assertVisible(_listItem("first-last-disable"));
	//next link is visible
	_assertVisible(_listItem("Next Page"));
	_assertEqual($i,_getText(_listItem("current-page")));
}
else if ($i==$TotalNoOfPages)
{
	_click(_link("page-"+$i));
	//previous link should be visible
	_assertVisible(_link("page-previous"));
	//next link not visible 
	_assertVisible(_listItem("first-last-disable-last"));
}
else
	{
	_assertVisible(_listItem("Next Page"));
	_click(_link("page-"+$i));
	_assertEqual($i,_getText(_listItem("current-page")));
	_assertVisible(_link("page-previous"));
	}
}

for (var $j=$TotalNoOfPages;$j>=1;$j--)
{

if($j==$TotalNoOfPages)
{	
	_assertEqual($TotalNoOfPages,_getText(_listItem("current-page")));
	//previous link should be visible
	_assertVisible(_link("page-previous"));
	//next link not visible 
	_assertVisible(_listItem("first-last-disable-last"));
}

else if ($j==1)
{
_click(_link("page-"+$j));	
//previous link disabled
_assertVisible(_listItem("first-last-disable"));
//next link should be disabled
_assertVisible(_listItem("Next Page"));

}

else
	{
	_assertVisible(_link("page-previous"));
	_click(_link("page-"+$j));
	_assertVisible(_listItem("Next Page"));
	_assertEqual($j,_getText(_listItem("current-page")));
	}
}
	
}

//Price filter
function priceCheck($Range)
{
	var $priceBounds = _extract(_getText(_link($Range)), "/[$](.*) to [$](.*)/",true).toString().split(",");
	var $minPrice = $priceBounds[0].toString();
	var $maxPrice = $priceBounds[1].toString();
	_log("$minPrice = " + $minPrice);
	_log("$maxPrice = " + $maxPrice);
	
	
	if ((_isVisible(_listItem("first-last-disable-last"))) || (_isVisible(_link("Next Page"))))
		{
		var $prices = _collectAttributes("_span","product-sales-price","sahiText",_in(_list("search-result-items")));
		for (var $k = 0; $k < $prices.length; $k++) 
			{
				var $price = parseFloat(_extract($prices[$k],"/[$](.*)/",true)[0].replace(",",""));				
				_log("$price = " + $price);				
				if ($price < $minPrice) 
				{
					_assert(false, "Price " + $price + " is lesser than min price " + $minPrice);
				} 
				else if ($price > $maxPrice) 
				{
					_assert(false, "Price " + $price + " is greater than max price " + $maxPrice);
				}					
			}
		}
	
	while (_isVisible(_link("Next Page")))
		{
		_click(_link("Next Page"));
		
		var $prices = _collectAttributes("_span","product-sales-price","sahiText",_in(_list("search-result-items")));
		for (var $k = 0; $k < $prices.length; $k++) 
			{
				var $price = parseFloat(_extract($prices[$k],"/[$](.*)/",true)[0].replace(",",""));				
				_log("$price = " + $price);				
				if ($price < $minPrice) 
				{
					_assert(false, "Price " + $price + " is lesser than min price " + $minPrice);
				} 
				else if ($price > $maxPrice) 
				{
					_assert(false, "Price " + $price + " is greater than max price " + $maxPrice);
				}					
			}
		
		}
}

//############################### Saje Cart Functions  ##################################

function addItemToCart($subCat,$i)
{
	//navigating to search page
	search($subCat);
	
	if (_isVisible(_list("search-result-items")))
		{
		//navigate to PDP
		_click(_link("name-link"));
		_wait(2000);
		}

		$pName=_getText(_heading1("/product-name/", _in(_div("product-col-2 product-detail"))));
	//selecting swatches
	selectSwatch();
	//set the quantity
	_setSelected(_select("productquantityupdate", _in(_div("quantity"))),$i);
	//click on add to cart
	_click(_submit("add-to-cart"));
	_wait(3000);
}

function navigateToCart($subCat,$i)
{
	//add item to cart
	addItemToCart($subCat,$i);
	//View Bag
	_click(_link("View Bag"));
	
	_wait(5000);
	
	$prodNameincart=_getText(_link("/(.*)/",_in(_div("name hone-header"))));
	
if (isMobile())
	{
	$QuantityInCart=_extract(_getSelectedText(_select("/js-cart-quantity-dropdown/")),"/: (.*)/",true).toString().split(" ")[0];
	}
else
	{
	$QuantityInCart=_extract(_getText(_div("quantity cart-no-image")),"/: (.*)/",true).toString().split(" ")[0];
	}
	$prodpriceincart=_extract(_getText(_span("price-total")),"/[$](.*)/",true).toString();
	
	if(_isVisible(_span("price-total")))
		{
	$totalPrice=_extract(_getText(_span("price-total")),"/[$](.*)/",true).toString();
		}
	else if(_isVisible(_span("price-unadjusted")))
		{
		$totalPrice=_extract(_getText(_span("price-adjusted-total")),"/[$](.*)/",true).toString();
		}
	//$shippingTax=_extract(_getText(_row("order-shipping")),"/[$](.*)/",true).toString();
	$orderTotal=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
}

//Navigate to IOL page
function navigateTOIL($excel)
{
	//navigate to cart page
	navigateToCart($excel[0][0],$excel[0][1]);
	//click on go straight to checkout to navigate to IL Page
	_click(_link("mini-cart-link-checkout"));
	_wait(2000);
}

//############################### Saje Profile Functions  ##################################

function createAccount()
{
	 _setValue(_textbox("dwfrm_profile_customer_firstname"), $userData[$user][1]);
	 _setValue(_textbox("dwfrm_profile_customer_lastname"),$userData[$user][2]);
	 _setValue(_textbox("dwfrm_profile_customer_email"),$userData[$user][3]);
	 _setValue(_password("/dwfrm_profile_login_password/"),$userData[$user][5]);
		 	 
	 _click(_submit("dwfrm_profile_confirm"));
	 _wait(3000);
} 


function deleteAddress()
{
	 _click(_link("user-account"));
	 _click(_link("Address Book"));
	 //deleting the existing addresses
	 while(_isVisible(_link("Delete")))
	   	{
	   	  _click(_link("Delete"));
	   	  _expectConfirm("/Do you want/",true);
	   	}
}

function addAddress($sheet,$r)
{
		
//	_setValue(_textbox("dwfrm_profile_address_addressid"), $sheet[$r][12]);
//	_setValue(_textbox("dwfrm_profile_address_firstname"), $sheet[$r][1]);
//	_setValue(_textbox("dwfrm_profile_address_lastname"), $sheet[$r][2]);
//	_setValue(_textbox("dwfrm_profile_address_address1"), $sheet[$r][3]);
//	_setValue(_textbox("dwfrm_profile_address_address2"), $sheet[$r][4]);
//	_setSelected(_select("dwfrm_profile_address_country"),$sheet[$r][5]);
//	_setSelected(_select("dwfrm_profile_address_states_state"), $sheet[$r][6]);
//	_setValue(_textbox("dwfrm_profile_address_city"), $sheet[$r][7]);
//	_setValue(_textbox("dwfrm_profile_address_postal"), $sheet[$r][8]);
//	_setValue(_textbox("dwfrm_profile_address_phone"),$sheet[$r][9]);	
	
  	_setValue(_textbox("dwfrm_profile_address_addressid"), $sheet[$r][1]);
	_setValue(_textbox("dwfrm_profile_address_firstname"), $sheet[$r][2]);
	_setValue(_textbox("dwfrm_profile_address_lastname"), $sheet[$r][3]);
	_setValue(_textbox("dwfrm_profile_address_address1"), $sheet[$r][4]);
	_setValue(_textbox("dwfrm_profile_address_address2"), $sheet[$r][5]);
	_setSelected(_select("dwfrm_profile_address_country"),$sheet[$r][6]);
	_setSelected(_select("dwfrm_profile_address_states_state"), $sheet[$r][7]);
	_setValue(_textbox("dwfrm_profile_address_city"), $sheet[$r][8]);
	_setValue(_textbox("dwfrm_profile_address_postal"), $sheet[$r][9]);
	_setValue(_textbox("dwfrm_profile_address_phone"),$sheet[$r][10]);	
	
}

function login()
{	 
	 if(!isMobile())
		 {
		     _click(_span("My Account"));
			 _setValue(_textbox("/dwfrm_login_username/"), $uId);
			 _setValue(_password("/dwfrm_login_password/"), $pwd);
			    if(_isVisible(_submit("dwfrm_login_login")))
			    {
			      _click(_submit("dwfrm_login_login"));
			      _wait(4000);
			    }
			   else
			    {
			        _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
			    }
		 }
	
	 else
		 {
		 
		 
	    	{
	    	_click(_submit("/menu-toggle /"));
	    	_click(_span("My Account", _in(_div("sign-right js-device-header-login"))));
	    	_wait();
	    	_setValue(_textbox("/dwfrm_login_username/", _in(_div("sign-right js-device-header-login"))),$uId);
	    	 _setValue(_password("/dwfrm_login_password/" ,_in(_div("sign-right js-device-header-login"))), $pwd);
	   	    if(_isVisible(_submit("dwfrm_login_login")))
	   	    {
	   	      _click(_submit("dwfrm_login_login",_in(_div("sign-right js-device-header-login"))));
	   	      _wait(4000);
	   	    }
	   	   else
	   	    {
	   	        _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	   	    }
	    	
	     	}
		 }
}

//credit card info
function CreateCreditCard($sheet, $i)
{
               
      // _setValue(_textbox("dwfrm_paymentinstruments_creditcards_newcreditcard_owner"),$sheet[$i][1]);
       _setSelected(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_type"),$sheet[$i][2]);
       _setValue(_textbox("/dwfrm_paymentinstruments_creditcards_newcreditcard_number/"),$sheet[$i][3]);
       _setSelected(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_month"),$sheet[$i][4]);
       _setSelected(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_year"),$sheet[$i][5]);
    	
               _setValue(_textbox("dwfrm_paymentinstruments_creditcards_address_firstname"),$sheet[$i][6]);
               _setValue(_textbox("dwfrm_paymentinstruments_creditcards_address_lastname"),$sheet[$i][7]);
               _setValue(_textbox("dwfrm_paymentinstruments_creditcards_address_address1"),$sheet[$i][8]);
               _setValue(_textbox("dwfrm_paymentinstruments_creditcards_address_address2"),$sheet[$i][9]);
               _setSelected(_select("dwfrm_paymentinstruments_creditcards_address_country"),$sheet[$i][10]);
               _setValue(_textbox("dwfrm_paymentinstruments_creditcards_address_city"),$sheet[$i][11]);
               _setSelected(_select("dwfrm_paymentinstruments_creditcards_address_states_state"),$sheet[$i][12]);
               _setValue(_textbox("dwfrm_paymentinstruments_creditcards_address_postal"),$sheet[$i][13]);
              // _setValue(_textbox("dwfrm_paymentinstruments_creditcards_address_phone"),$sheet[$i][14]);
              // _setValue(_textbox("dwfrm_paymentinstruments_creditcards_address_email_emailAddress"), $sheet[$i][20]);
}


function closedOverlayVerification()
{
   _assertNotVisible(_div("ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable"));
   //heading
//   _assertVisible(_heading1($Payment_Data[0][3]));
//   _assertVisible(_div("/"+$Payment_Data[1][3]+"/"));
   _assertVisible(_link("Add Credit Card") );           
}


function DeleteCreditCard()
{               
    //verifying the address in account
    _click(_span("/Hello/"));
    _click(_link("Payment methods"));
    if(_isVisible(_list("payment-list")))
    	{
    	     var $totCards=_count("_div","/cc-owner/",_in(_list("payment-list")));
    	    //click on delete card link
    	    for(var $i=0; $i<$totCards; $i++)
    	    {
    	      _byPassWaitMechanism(true);
    	      //click on ok in confirmation overlay
    	      _expectConfirm("/Do you want/",true);
    	      _click(_submit("Delete Card"));  
    	      //click on ok in confirmation overlay
    	       _focusWindow();
    	       // CTRL+R will reload this page using robot events
    	       var $robot = new java.awt.Robot();
    	       $robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
    	       _wait(500);
    	       $robot.keyPress(java.awt.event.KeyEvent.VK_R);
    	       _wait(500);
    	       $robot.keyRelease(java.awt.event.KeyEvent.VK_R);
    	       _wait(500);
    	       $robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
    	       _wait(500);
    	       _byPassWaitMechanism(true);
        	}
   
       
    }
}

//############################### Saje Checkout Functions  ##################################

//Checkout related functions

function navigateToShippingPage($data,$quantity)
{
	//navigate to cart page
	navigateToCart($data,$quantity);	
	//navigating to shipping page
	_click(_submit("dwfrm_cart_checkoutCart"));
	_wait(4000);
	if(isMobile())
		{
			_wait(3000);
		}
	
	//_div("login-box login-account")
	
	if(_isVisible(_div("login-box login-account")))
		{
		_click(_submit("dwfrm_login_unregistered"));
		}
	
//	_click(_link("user-account"));
//	_wait(2000);
//	if(!_isVisible(_link("/Logout/", _in(_div("user-links")))))
//		{
//		_click(_submit("dwfrm_login_unregistered"));
//		}

	$subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
	$tax=parseFloat(_extract(_getText(_row("/order-sales-tax/")),"/[$](.*)/",true));
	_log($tax);
	$shippingTax=parseFloat(_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true));
	_log($shippingTax);
}

function navigateToBillingPage($data,$quantity)
{
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[1][0]);
	//shipping details
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//verifying address verification overlay
	addressVerificationOverlay();	
	$orderTotal=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());
	_log($orderTotal);
}

function shippingAddress($sheet,$i)
{
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), $sheet[$i][1]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), $sheet[$i][2]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"), $sheet[$i][3]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"),$sheet[$i][4]);
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$sheet[$i][5]);
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), $sheet[$i][6]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), $sheet[$i][7]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $sheet[$i][8]);
	_setValue(_telephonebox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),$sheet[$i][9]);
}

/*function BillingAddress($sheet,$i)
{
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"), $sheet[$i][1]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"), $sheet[$i][2]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1"), $sheet[$i][3]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address2"),$sheet[$i][4]);
	_setSelected(_select("dwfrm_billing_billingAddress_addressFields_country"),$sheet[$i][5]);
	_setSelected(_select("dwfrm_billing_billingAddress_addressFields_states_state"), $sheet[$i][6]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_city"), $sheet[$i][7]);
	_setValue(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone"), $sheet[$i][8]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_phone"),$sheet[$i][9]);
	
	_click(_link("user-account"));
	_wait(4000);
	
	if(!_isVisible(_link("Logout")))
		{
		_setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),$sheet[$i][10]);
		}
	
}*/

function BillingAddress($sheet,$i)
{
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"), $sheet[$i][1]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"), $sheet[$i][2]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1"), $sheet[$i][3]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address2"),$sheet[$i][4]);
	_setSelected(_select("dwfrm_billing_billingAddress_addressFields_country"),$sheet[$i][5]);
	_setSelected(_select("dwfrm_billing_billingAddress_addressFields_states_state"), $sheet[$i][6]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_city"), $sheet[$i][7]);
	_setValue(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone"), $sheet[$i][8]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_phone"),$sheet[$i][9]);
	_setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"), $sheet[$i][10]);
	
}

function verifyOrderSummary($shipmentType,$taxService,$numOfItems)
{
	var $actCount=0;
	var $actPrice=0; 
	//product name
	_assertEqual($pName, _getText(_link("/(.*)/",_in(_div("checkout-mini-cart")))));
	var $numOfShipments=_count("_div","/mini-cart-product/",_in(_div("checkout-mini-cart")));
	for(var $i=0;$i<$numOfShipments;$i++)
		{
		var $count=parseInt(_getText(_span("value",_in(_div("mini-cart-pricing["+$i+"]",_in(_div("checkout-mini-cart")))))));
		var $price=_extract(_getText(_span("mini-cart-price",_in(_div("checkout-mini-cart")))),"/[$](.*)/",true).toString();
		$actCount=parseFloat($actCount)+$count;
		$actPrice=parseFloat($actPrice)+parseFloat($price);		
		}
	//quantity
	_assertEqual($quantity,$actCount);
	//price
	_assertEqual($totalPrice,$actPrice);
	//sub total
	_assertVisible(_row("order-subtotal"));
	_assertEqual($totalPrice,_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString());
	//Shipping tax
	if($shipmentType=="Multishipment")
		{
		//var $multiShipmentTax=parseFloat(_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString())*numOfItems;
		for(var $i=0;$i<$numOfItems;$i++)
			{
			_assertEqual($shippingTax,parseFloat(_extract(_collectAttributes("_row","/order-shipping/","sahiText")[$i].toString(),"/[$](.*)/",true)));
			}
		//_assertEqual($shippingTax,$multiShipmentTax);
		}
	else
		{
	_assertVisible(_row("/order-shipping/"));
	_assertEqual($shippingTax,_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString());
		}
	//sales tax
	_assertVisible(_row("order-sales-tax"));
	
	if($taxService=="DEMANDWARE")
		{
			_assertEqual($tax,_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString());
			//order total
			//$actOrderTotal=$subTotal+$shippingTax+$salesTax;
			$actOrderTotal=parseFloat($totalPrice)+parseFloat($shippingTax)+parseFloat($tax);
			_log($actOrderTotal);
			$actOrderTotal=round($actOrderTotal,2);
			_assertEqual($actOrderTotal,_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());	
		}
	else
		{
		var $boolean=_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString();
		if($boolean=="false")
			{
			_assert(false,"sales tax is not added");
			}
		else
			{
			_assert(true,"sales tax got added");
			var $salesTax=parseFloat(_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString());
			//order total
			_assertVisible(_row("order-total"));
			
				if(_isVisible(_row("/Shipping Discount/")))
				{
					var $shippingDiscount=parseFloat(_extract(_getText(_row("/Shipping Discount/")),"/[$](.*)/",true));
					$actOrderTotal=$subTotal+$shippingTax*$numOfItems+$salesTax-$shippingDiscount;
					$actOrderTotal=round($actOrderTotal,2);
					_assertEqual($actOrderTotal,_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());
				}
				else
				{
					_log($subTotal);
					_log($shippingTax);
					_log($salesTax);
					$actOrderTotal=$subTotal+$shippingTax*$numOfItems+$salesTax;
					$actOrderTotal=round($actOrderTotal,2);
					_assertEqual($actOrderTotal,_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());
				}
			}
		}
}


/*function billingPageUI($excel)
{
	//verify the UI of Billing page 
	//fields 
	_assertVisible(_span("/First Name/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_firstname")); 
	_assertVisible(_span("/Last Name/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_lastname"));
	_assertVisible(_span("/Address 1/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_address1"));
	_assertVisible(_span("/Address 2/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_address2"));
	_assertVisible(_span("/State/"));
	_assertVisible(_select("dwfrm_billing_billingAddress_addressFields_states_state"));
	_assertVisible(_span("/City/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_city"));
	_assertVisible(_span("/Zip Code/"));
	_assertVisible(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone"));
	_assertVisible(_span("/Phone/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_phone"));
	_assertVisible(_span("/Email/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_email_emailAddress"));
	//subscribe to pandora check box
	_assertVisible(_checkbox("dwfrm_billing_billingAddress_addToEmailList"));
	
	//checkbox
	_assertVisible(_checkbox("dwfrm_billing_billingAddress_addToEmailList"));
	_assertNotTrue(_checkbox("dwfrm_billing_billingAddress_addToEmailList").checked);
	//Privacy policy
	//_assertVisible(_link("See Privacy Policy"));
	//Privacy Policy
	_assertVisible(_link("Privacy Policy"));
	//ENTER GIFT CERTIFICATE OR COUPON/DISCOUNT CODES section
	_assertVisible(_span($excel[1][0]));
	_assertVisible(_textbox("dwfrm_billing_couponCode"));
	_assertVisible(_submit("APPLY"));
	
	//checking UI of credit card
	//Payment section
	_assertVisible(_label("Credit Card:"));
	_assertVisible(_radio("dwfrm_billing_paymentMethods_selectedPaymentMethodID"));
	_assert(_radio("dwfrm_billing_paymentMethods_selectedPaymentMethodID").checked);
	//Payment fields
	_assertVisible(_fieldset("/"+$excel[0][1]+"/"));
	_assertVisible(_span("Name on Card"));
	_assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"));
	_assertVisible(_span("Type"));
	_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_type"));
	_assertVisible(_span("Number"));
	_assertVisible(_span($excel[1][1], _in(_div("PaymentMethod_CREDIT_CARD"))));
	//_assertEqual($excel[1][1], _getText(_span("form-caption", _in(_div("PaymentMethod_CREDIT_CARD")))));
	_assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_number"));
	_assertVisible(_span("/Expiration Date/"));
	_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_month"));
	_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_year"));
	_assertVisible(_span("Security Code"));
	_assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_cvn"));
	
	//Order summary section
	_assertVisible(_heading3("/ORDER SUMMARY/"));
	_assertVisible(_link("Edit"));
	
	//Click on expand link 
	if (_isVisible(_div("mini-cart-product collapsed")))
		{
		_click(_span("mini-cart-toggle fa fa-caret-right"));
		}
	_assertVisible(_div("mini-cart-image", _in(_div("checkout-mini-cart"))));
	_assertVisible(_div("mini-cart-name", _in(_div("checkout-mini-cart"))));
	_assertVisible(_row("order-subtotal"));
	_assertVisible(_row("/order-shipping/"));
	_assertVisible(_row("order-sales-tax"));
	_assertVisible(_row("order-total"));
}*/

function billingPageUI()
{
	
   //verify the UI of Billing page 
   //fields 
   _assertVisible(_span("/First Name/"));
   _assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_firstname")); 
   _assertVisible(_span("/Last Name/"));
   _assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_lastname"));
   _assertVisible(_span("/Address 1/"));
   _assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_address1"));
   _assertVisible(_span("/Address 2/"));
   _assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_address2"));
   _assertVisible(_span("/State/"));
   _assertVisible(_select("dwfrm_billing_billingAddress_addressFields_states_state"));
   _assertVisible(_span("/City/"));
   _assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_city"));
   _assertVisible(_span("/Zip Code/"));
   _assertVisible(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone"));
   _assertVisible(_span("/Phone/"));
   _assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_phone"));
   _assertVisible(_span("/Email/"));
   _assertVisible(_textbox("dwfrm_billing_billingAddress_email_emailAddress"));
   //subscribe to pandora check box
   _assertVisible(_checkbox("dwfrm_billing_billingAddress_addToEmailList"));
   
   //checkbox
   _assertVisible(_checkbox("dwfrm_billing_billingAddress_addToEmailList"));
   _assertNotTrue(_checkbox("dwfrm_billing_billingAddress_addToEmailList").checked);
   
   _assertNotVisible(_textbox("dwfrm_billing_couponCode"));
	_click(_span(97));
	_assertVisible(_textbox("dwfrm_billing_couponCode"));
	
	_assertVisible(_submit("APPLY"));
       
       //checking UI of credit card
       //Payment section
       //SELECT CREDIT CARD TYPE Section with following fields :
    _assertVisible(_span("Name on Card"));
    _assertVisible(_span("Type"));
    _assertVisible(_span("Card Number"));
    _assertVisible(_span("Expiration Date"));
    _assertVisible(_span("Security Code"));
    
   _assertVisible(_span("Name on Card"));
   _assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"));

   _assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_type"));
   _assertVisible(_span("/Number/"));
   //_assertVisible(_span($excel[1][1], _in(_div("PaymentMethod_CREDIT_CARD"))));
   //_assertEqual($excel[1][1], _getText(_span("form-caption", _in(_div("PaymentMethod_CREDIT_CARD")))));
   _assertVisible(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"));

   _assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_expiration_month"));
   _assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_expiration_year"));


   _assertVisible(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"));
       
   //Order summary section
   _assertVisible(_heading3("/ORDER SUMMARY/"));
   _assertVisible(_link("Edit"));
   
 //Click on expand link 
 if (_isVisible(_div("mini-cart-product collapsed")))
 {
 _click(_span("mini-cart-toggle fa fa-caret-right"));
 }
 
 _assertVisible(_div("mini-cart-image", _in(_div("checkout-mini-cart"))));
 _assertVisible(_cell("Subtotal"))
 _assertVisible(_cell("Shipping"));
 _assertVisible(_cell("/Taxes/"));
 _assertVisible(_cell("Estimated Total"));
 
}


function PaymentDetails($sheet,$i)
{
	  _setValue(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"), $sheet[$i][1]);
		 _setSelected(_select("dwfrm_billing_paymentMethods_creditCard_type"), $sheet[$i][2]);
		 _setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"), $sheet[$i][3]);
		 _setSelected(_select("dwfrm_billing_paymentMethods_creditCard_expiration_month"), $sheet[$i][4]);
	    _setSelected(_select("dwfrm_billing_paymentMethods_creditCard_expiration_year"), $sheet[$i][5]);
	    _setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"), $sheet[$i][6]);
}

function verifyNavigationToShippingPage()
{
	if(!_isVisible(_span("My Account", _in(_link("user-account")))))
	{
		_assertVisible(_select("dwfrm_singleshipping_addressList"));
	}
	_assertVisible(_submit("dwfrm_singleshipping_shippingAddress_save"));	
}

function round(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0)
      return Math.round(value);

    value = +value;
    exp  = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
      return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
  }

