_resource("User_Credentials.xls");

var $userLog=_readExcelFile("User_Credentials.xls","BM_Login");

function BM_Login()//function added
{
	_navigateTo($URL[2][0]);
	_setValue(_textbox("LoginForm_Login"),$userLog[0][0]);
	_setValue(_password("LoginForm_Password"),$userLog[0][1]);
	_click(_submit("Log In"));
	if(isMobile())
		{			
			_setSelected(_select("SelectedSiteID"), $userLog[0][2]);
		}
	else
		{
			_click(_span("/sod_select/"));
			_click(_span($userLog[0][2]));
			if(_isVisible(_cell("Welcome to the Demandware Business Manager.")))
				{
					_click(_span("sod_option  active selected"));
					_assertEqual($userLog[0][2], _getText(_span("sod_label")));
				}
		}	 
}

function deleteUser()
{
	BM_Login();
_click(_link("Customers"));
_click(_link("Customers[1]"));
_click(_link("Advanced"));
_setValue(_textbox("WFCustomerAdvancedSearch_Email"),$email);
_click(_submit("Find", _in(_div("/Advanced Customer Search/"))));
if(_isVisible(_row("/Select All/")))
{
	if(_assertEqual($email,_getText(_cell("table_detail e s[4]"))))
		{
		_click(_checkbox("DeleteCustomer"));
		_click(_submit("Delete"));
		_click(_submit("OK"));
		_log("Successfully deleted the user")
		}
}
else
{
	_log("search did not match any users.");
}
_click(_link("Log off."));

}

//----------------- Store Locator----

function SetStoreLocator_Demandware($name)
{
BM_Login();
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));
_click(_link("PREVAIL Integrations"));

//var $PreviousValue=_getSelectedText(_select("MetabcIjQiaag0uYwaaadhGjpzlnq4"));
/*if (_isSafari())
{
_setSelected(_select("MetabcIjQiaag0uYwaaadhGjpzlnq4"),"DEMANDWARE");
}
else
	{
	_setSelected(_select("MetabcPPEiaag19XIaaadh11QACTfI"),"DEMANDWARE");
	}*/

_setSelected(_select("inputfield_en", _in(_cell("PREVAIL DEMANDWARE"))),$name);
_click(_submit("update"));
_click(_link("Log off."));
}

function SetStoreLocator_Prevail($name)
{
BM_Login();
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));
_click(_link("PREVAIL Integrations"));
//var $PreviousValue=_getSelectedText(_select("MetabcIjQiaag0uYwaaadhGjpzlnq4"));

/*if (_isSafari())
{
_setSelected(_select("MetabcIjQiaag0uYwaaadhGjpzlnq4"),"PREVAIL");
}
else
	{
	_setSelected(_select("MetabcPPEiaag19XIaaadh11QACTfI"),"PREVAIL");//DEMANDWARE
	}*/

_setSelected(_select("inputfield_en", _in(_cell("PREVAIL DEMANDWARE"))),$name);
_click(_submit("update"));
_click(_link("Log off."));
}

function ResetStoreLocator()
{
BM_Login();
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));
_click(_link("PREVAIL Integrations"));
_setSelected(_select("MetabcIjQiaag0uYwaaadhGjpzlnq4"),$PreviousValue);
_click(_submit("update"));
_click(_link("Log off."));
}
