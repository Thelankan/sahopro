_include("../../util.GenericLibrary/GlobalFunctions.js");
_include("../../util.GenericLibrary/BrowserSpecific.js");
//_include("../../util.GenericLibrary/BM_Configuration.js");
_resource("Payment.xls");

SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();

var $Payment_Data=_readExcelFile("Payment.xls","Payment_Data");
var $Createcard=_readExcelFile("Payment.xls","CreateCard");
var $Validations=_readExcelFile("Payment.xls","Creditcard_validations");



login();
DeleteCreditCard();
var $t = _testcase("261322","Verify the navigation related to 'Payment Settings' menu on my Account Home page content section in application as a Registered user.");
$t.start();
try
{
	_click(_link("user-account"));
	//clcik on the payment setting 
	_click(_link("Payment methods"));
	//verify the navigation 
	_assertVisible(_heading1("Credit Card Information"));
	_assertEqual("Payment Settings", _getText(_listItem("current-page")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("261323/ 261325","Verify the functionality related tothe 'ADD A CREDIT CARD' link present in the Payment section header on 'My Account Payment page' of the application as a Registered user");
$t.start();
try
{
	_assertVisible(_link("/add-card button/"));
	_click(_link("/add-card button/"));
	if (_isVisible(_div("/add-credit-card-overlay/")))
		{		  
		  _assert(true, "Add a Credit Card overlay is displayed");		
		}
    else
		{
		_assert(false, "Add a Credit Card overlay is not displayed");
		}
	//click on the close button
	_click(_button("/button-icon-only ui-dialog-titlebar-close/"));
	//verify  the functionality
	_assertNotVisible(_div("/add-credit-card-overlay ui-draggable/"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("261324/261326","Verify the UI of Add a credit card overlay on payment methods page in application as a Registered user.");
$t.start();
try
{
	_click(_link("section-header-note buttonctaone add-card button"));
	_assertVisible(_textbox("/dwfrm_paymentinstruments_creditcards_newcreditcard_number/"));
	//Tyep
	_assertVisible(_span("Type"));
	_assertVisible(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_type"));
	//card number
	_assertVisible(_span("Number"));
	_assertVisible(_textbox("/dwfrm_paymentinstruments_creditcards_newcreditcard_number/"));
	//month
	_assertVisible(_span("Month"));
	_assertVisible(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_month"));
	//Year
	_assertVisible(_span("Year"));
	_assertVisible(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_year"));
	//checking which credit card type is enabled
		//first name
		_assertVisible(_span("First Name"));
		_assertVisible(_textbox("dwfrm_paymentinstruments_creditcards_address_firstname"));
		//last name
		_assertVisible(_span("Last Name"));
		_assertVisible(_textbox("dwfrm_paymentinstruments_creditcards_address_lastname"));
		//address1
		_assertVisible(_span("Address 1"));
		_assertVisible(_textbox("dwfrm_paymentinstruments_creditcards_address_address1"));
		//address2
		_assertVisible(_span("Address 2"));
		_assertVisible(_textbox("dwfrm_paymentinstruments_creditcards_address_address2"));
		//country
		_assertVisible(_span("Country"));
		_assertVisible(_select("dwfrm_paymentinstruments_creditcards_address_country"));
		//city
		_assertVisible(_span("City"));
		_assertVisible(_textbox("dwfrm_paymentinstruments_creditcards_address_city"));
		//state
		_assertVisible(_span("State"));
		_assertVisible(_select("dwfrm_paymentinstruments_creditcards_address_states_state"));
		//zip code
		_assertVisible(_span("Zip Code"));
		_assertVisible(_textbox("dwfrm_paymentinstruments_creditcards_address_postal"));
	


		
	_assertVisible(_submit("applyBtn"));
	_assertVisible(_submit("Cancel"));
	//click on the cancle button 
	_click(_submit("dwfrm_paymentinstruments_creditcards_cancel"));
	//verify the functinality
	_assertNotVisible(_div("/add-credit-card-overlay ui-draggable/"));

	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("","");
$t.start();
try
{
	if (_isIE())
	{
	_wait(2000,_isVisible(_link("Add Credit Card") ));
	}
	_click(_link("Add Credit Card") );
	//validation of credit card overlay
	for(var $i=0;$i<$Validations.length;$i++)
		{
				CreateCreditCard($Validations,$i);
		
		_click(_submit("applyBtn"));
		_log($i);
		//blank
		if($i==0)
			{
				//_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_newcreditcard_owner"),"background-color"));
				_assertEqual($Validations[0][15],_style(_textbox("/dwfrm_paymentinstruments_creditcards_newcreditcard_number/"),"background-color"));
				
					_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_firstname"),"background-color"));
					_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_lastname"),"background-color"));
					_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_address1"),"background-color"));
					_assertEqual($Validations[0][15],_style(_select("dwfrm_paymentinstruments_creditcards_address_country"),"background-color"));
					_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_city"),"background-color"));
					_assertEqual($Validations[0][15],_style(_select("dwfrm_paymentinstruments_creditcards_address_states_state"),"background-color"));
					_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_postal"),"background-color"));
					//_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_phone"),"background-color"));
//					_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_email_emailAddress"),"background-color"));
//					_assertVisible(_span($Validations[4][15]));
					_assertEqual($Validations[4][16], _getText(_span("/dwfrm_paymentinstruments_creditcards_newcreditcard_number/")));
					_assertVisible(_span($Validations[2][17]));
					_assertVisible(_span($Validations[2][18]));
					_assertVisible(_span($Validations[5][15]));
					_assertVisible(_span($Validations[5][16]));
					_assertVisible(_span($Validations[5][17]));
					_assertVisible(_span($Validations[5][18]));
					_assertVisible(_span($Validations[5][19]));
//					_assertVisible(_span($Validations[6][15]));
//					_assertVisible(_span($Validations[6][16]));
			


			}
		//max length
		if($i==1)
			{
			//_assertEqual($Validations[1][15],_getText(_textbox("dwfrm_paymentinstruments_creditcards_newcreditcard_owner")).length);
			//_assertEqual($Validations[1][16],_getText(_textbox("/dwfrm_paymentinstruments_creditcards_newcreditcard_number/")).length);
		
					_assertEqual($Validations[1][17],_getText(_textbox("dwfrm_paymentinstruments_creditcards_address_firstname")).length);
					_assertEqual($Validations[1][17],_getText(_textbox("dwfrm_paymentinstruments_creditcards_address_lastname")).length);
					_assertEqual($Validations[1][17],_getText(_textbox("dwfrm_paymentinstruments_creditcards_address_address1")).length);
					_assertEqual($Validations[1][16],_getText(_textbox("dwfrm_paymentinstruments_creditcards_address_address2")).length);
					_assertEqual($Validations[1][17],_getText(_textbox("dwfrm_paymentinstruments_creditcards_address_city")).length);
					_assertEqual($Validations[1][18],_getText(_textbox("dwfrm_paymentinstruments_creditcards_address_postal")).length);
//					_assertEqual($Validations[1][18],_getText(_textbox("dwfrm_paymentinstruments_creditcards_address_phone")).length);
//					_assertEqual($Validations[1][17],_getText(_textbox("dwfrm_paymentinstruments_creditcards_address_email_emailAddress")).length);
			}
		//invalid card number & Exp Year
		if($i==2)
			{
			//card number 
			_assertEqual($Validations[0][15],_style(_textbox("/dwfrm_paymentinstruments_creditcards_newcreditcard_number/"),"background-color"));
			_assertVisible(_div($Validations[4][16]));
			//_assertEqual($Validations[0][15],_style(_div($Validations[2][15]),"background-color"));
			
			


			}
		//invalid in zip code,phone number & number
		if($i==3)
			{
			//number
			
			_assertVisible(_div($Validations[4][16]));
			_assertEqual($Validations[0][15],_style(_textbox("/dwfrm_paymentinstruments_creditcards_newcreditcard_number/"),"background-color"));
//			_assertVisible(_span($Validations[6][17]));
//			_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_email_emailAddress"),"background-color"));


			}
		if($i==5)
			{
			
				//zip code
				_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_postal"),"background-color"));
				_assertVisible(_span($Validations[3][15]));
				_assertEqual($Validations[0][16],_style(_span($Validations[3][15]),"color"));

				_assertVisible(_div($Validations[2][15]));

			    //_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_email_emailAddress"),"background-color"));
			}
		if($i==4)
			{	
			
			//Exp Year
			_assertEqual($Validations[0][16],_style(_span("Month"),"color"));
			_assertEqual($Validations[0][16],_style(_span("Year"),"color"));
			_assertVisible(_div($Validations[2][16]));
			_assertEqual($Validations[0][15],_style(_div($Validations[2][16]),"background-color"));				
			}
		
		if($i==6)
			{
		         	_assertEqual($Validations[0][15],_style(_div($Validations[2][15]),"background-color"));
			
					//zip code
					_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_postal"),"background-color"));
					_assertVisible(_span($Validations[3][15]));
					_assertEqual($Validations[0][15],_style(_span($Validations[3][15]),"backgroung-color"));
//					_assertVisible(_span($Validations[6][17]));
//					_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_email_emailAddress"),"background-color"));
			
			}
		}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("261327/ 261334 ","Verify the functionality related to 'Apply' button the Add a credit card overlay on my account payment methods page of the application as a Registered user.");
$t.start();
try
{
	//open the credit card overlay
	_click(_link("Add Credit Card") );
	//filling details in create card fields
	CreateCreditCard($Createcard,0);
	//click on apply button
	_click(_submit("applyBtn"));
	//overlay got closed or not
	closedOverlayVerification();
	//verify whether credit card got saved or not and also UI
	_wait(3000);
	_assertVisible(_list("payment-list"));
	var $saveInfo=_getText(_listItem("/first/")).split(" ");
	var $actualExpdate=$saveInfo[4]+""+$saveInfo[5];
	_assertEqual($Createcard[0][6],$saveInfo[0]);
	_assertEqual($Createcard[0][7],$saveInfo[1]);
	var $expDate=$Createcard[0][16];
	_assertEqual($expDate,$actualExpdate);
	_assertVisible(_submit("Delete Card"));
//	//left nav section
////	var $Heading=_count("_span", "/(.*)/",_in(_div("secondary-navigation")));
//	var $Links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
//	//checking headings
////	for(var $i=0;$i<$Heading;$i++)
////	       {
////	              _assertVisible(_span($Payment_Data[$i][0]));                
////	       }
////	//checking links
//	for(var $i=0;$i<$Links;$i++)
//	       {
//	              _assertVisible(_link($Payment_Data[$i][1]));                
//	       }
//	//need help section
//	_assertVisible(_heading2("/"+$Payment_Data[2][3]+"/"));s
//	_assertVisible(_div("account-nav-asset"));
//	//contact us link
//	_assertVisible(_link($Payment_Data[3][3]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();




var $t = _testcase("261335","Verify the UI of 'My Account Payment page' when there are multiple payment methods added in application as a Registered user.");
$t.start();
try
{
	for(var $i=0;$i<=1;$i++)
		{
		_click(_link("section-header-note buttonctaone add-card button"));

		CreateCreditCard($Createcard,$i);
		
		_click(_submit("dwfrm_paymentinstruments_creditcards_create"));
		}
//	_collectAttributes("_div","/cc-type addr-common/","sahiText",_in(_list("payment-list")));
//	_assertVisible(_list("payment-list"));



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("261328/261331/261333 ","Verify the functionality of 'Delete Card' link in the Credit card information page");
$t.start();
try
{
	//click on delete card link
	
	var $Tot_Card=_count("_div","cc-type addr-common",_in(_list("payment-list")));
	for($i=0;$i<$Tot_Card;$i++)
		{
		_click(_submit("Delete Card"));
		_expectConfirm("/Do you want/",true);
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("261330","Verify the UI of 'My Account Payment page' when there are no payment methods added in application as a Registered user.");
$t.start();
try
{
	//login to the application
	
	_wait(3000);
	_click(_link("Payment methods"));
	_assertVisible(_heading1("Credit Card Information"));
	_assertVisible(_link("section-header-note buttonctaone add-card button"));
	_assertNotVisible(_list("payment-list"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();