_include("../../util.GenericLibrary/BrowserSpecific.js");
_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("MyAcc_Home.xls");
var $MyAcc_Home=_readExcelFile("MyAcc_Home.xls","MyAcc_HomeUS");

SiteURLs()
cleanup();
_setAccessorIgnoreCase(true);
//login to the application
//_click(_span("My Profile"));
//if(isMobile())
//	{
//	
//	_click(_submit("/menu-toggle /"));
//	_click(_span("My Profile", _in(_div("sign-right js-device-header-login"))));
//	}

login();

_click(_link("My Account"));
var $t = _testcase("261250/261251/261252/261253/261254/2612548/2612547/261250", "Verify the navigation related to  'Personal Data'/Address/payment settings/order history/modify wish list/search Wish Lists/create registry/Search Registries/Modify Registries/privacy policy/secure shopping  link  present in left nav on My account home page in application");
$t.start();
try
{
_click(_link("My Account"));

//left nav links	
var $Links=  _count("_listItem", "/(.*)/",_in(_div("nav show-left-nav")));
for(var $i=0;$i<$Links;$i++)
	{
		//navigate to my account home page
		_click(_link($MyAcc_Home[$i][3],_in(_listItem($MyAcc_Home[$i][3]))));
		//click on link personal data
		if($i==0)
			{
			//verify the navigation
			_assertEqual($MyAcc_Home[$i][4], _getText(_span("account-heading nfourtext")));
			_assertVisible(_div("my-account-page"));

			}
		//click on link Address
		if($i==1)
			{
			//verify the navigation
			_assertVisible(_heading1($MyAcc_Home[$i][4]));
			_assertVisible(_div("create_account account-rightwrapper edit-account-wrapper"));

			}
		//click on link payment settings
		if($i==2)
			{
			//verify the navigation
			_assertVisible(_heading1($MyAcc_Home[$i][4]));
			_assertVisible(_div("order-history-page"));

			
			}
		//click on link order history
		if($i==3)
			{
			//verify the navigation
			_assertVisible(_link("Addresses", _in(_listItem("Addresses"))));

			}
		//click on link payment
		if($i==4)
			{
			_assertVisible(_heading1("Credit Card Information"));

			}
	
            //click on link Privacy Policy
		if($i==5)
			{
			
			_assertVisible(_heading1("Privacy Policy"));
			_click(_link("user-account"));
			_click(_link("My Account"));
			}
		//click on link Secure Shopping
		if($i==6)
			{
			_assertVisible(_heading1($MyAcc_Home[$i][4]));
			_assert(false,"connect US link");
			_click(_link("user-account"));
			_click(_link("My Account"));

			}
		
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


$t.end();var $t = _testcase("261258/261257/261247","Verify the navigation related to 'Payment Settings' menu on My Account Landing page content section in application as a Registered use");
$t.start();
try
{
	
	var $MyAccountLink=_collectAttributes("_heading2","/(.*)/","sahiText",_in(_div("my-account-page")));
	for(var $i=0;$i<$MyAccountLink.length;$i++)
		{	
		//navigate to my account home page
		//_click(_link("user-account"));
		_click(_link("My Account"));
		//click on the links in the My Account page
		_click(_link("/"+$MyAccountLink[$i]+"/",_in(_div("my-account-page"))));
		//click on link personal data
		if($i==0)
			{
			//verify the navigation
			_assertVisible(_heading1($MyAcc_Home[$i][2]));
			_assertEqual("Update", _getText(_div("form-row form-row-button")));

			}
		//click on link Address
		if($i==1)
			{
			//verify the navigation
			_assertVisible(_heading1($MyAcc_Home[$i][2]));
			_assertVisible(_div("order-history-page"));

			}
		//click on link payment settings
		if($i==2)
			{
			//verify the navigation
			_assertVisible(_heading1($MyAcc_Home[$i][2]));
			_assertVisible(_link("section-header-note address-create buttonctaone button"));

			
			}
		//click on link order history
		if($i==3)
			{
			//verify the navigation
			_assertVisible(_heading1($MyAcc_Home[$i][2]));
			_assertEqual("Add Credit Card", _getText(_link("section-header-note buttonctaone add-card button")));


			}
	
		}
		

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("261248","Verify the UI of 'My Account home page in application as a Registered user.");
$t.start();
try
{
	//navigate to my account home page
	_click(_link("user-account"));
	_click(_link("My Account"));
	var $MyAccountLink=_collectAttributes("_heading2","/(.*)/","sahiText",_in(_div("my-account-page")));
	for(var $i=0;$i<$MyAccountLink.length;$i++)
		{
		_click(_link("My Account"));
		_assertVisible(_heading2($MyAccountLink[$i],_in(_div("my-account-page"))));
		_assertVisible(_div($MyAcc_Home[$i][5],_near(_heading2($MyAccountLink[$i]))));
		}
	//left nav links	
	var $Links=  _count("_listItem", "/(.*)/",_in(_div("nav show-left-nav")));
	for(var $i=0;$i<$Links;$i++)
		{
		_assertVisible(_link($MyAcc_Home[$i][3],_in(_div("nav show-left-nav"))));		
		}
	_assertVisible(_span("account-user-name sone-text"));
	_assertVisible(_link("Logout"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase(" 261249 ","Verify the functionality related to 'Logout' link on My Account Landing page on application as a Registered user");
$t.start();
try
{
	//click on the logout link
	_click(_link("Logout"));
	//verify the navigation
	//_assertNotVisible(_link("/Hello/"));
	_assertNotVisible(_div("my-account-page"));
	_assertVisible(_div("home-products"));
	_assertVisible(_div("our best sellers"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
