_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("EditAccount_Data.xls");

var $Generic=_readExcelFile("EditAccount_Data.xls","Generic");
var $Account_Validation=_readExcelFile("EditAccount_Data.xls","Account_Validation");

deleteProvidedUser($Generic[2][7]);
deleteProvidedUser($Account_Validation[5][3]);

var $color=$Account_Validation[0][6];
var $color1=$Account_Validation[1][6];
//from util.GenericLibrary/BrowserSpecific.js
var $FirstName=$FName.toUpperCase();
var $LastName=$LName.toUpperCase();
var $UserEmail=$uId;
var $UserPwd=$pwd;
_log($FirstName);
_log($LastName);
SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);
//Login()

//click on My Account link	
_click(_span("My Account"));
//click on create account link
_click(_link("Create an Account"));

_setValue(_textbox("dwfrm_profile_customer_firstname"), $Generic[0][7]);
_setValue(_textbox("dwfrm_profile_customer_lastname"),$Generic[1][7]);
_setValue(_textbox("dwfrm_profile_customer_email"),$Generic[2][7]);
_setValue(_password("/dwfrm_profile_login_password/"),$Generic[3][7]);
//profile confirm	 	 
_click(_submit("dwfrm_profile_confirm"));
_wait(3000);

//verify the hello 
_click(_span("/Hello/", _in(_link("user-account"))));
_click(_heading3("My Account"));
_click(_heading2("Personal Data"));

var $t = _testcase("261261", "Verify the UI  of 'Personal data (Edit Account)' page in application as a  Registered user.");
$t.start();
try
{	
	//heading
	_assertVisible(_heading1("Edit Account"));
	//Name section
	_assertVisible(_fieldset($Generic[1][0]));
	//Fname
	_assertVisible(_span("First Name"));
	_assertVisible(_textbox("dwfrm_profile_customer_firstname"));
	_assertEqual($Generic[0][7], _getValue(_textbox("dwfrm_profile_customer_firstname")));
	//Lname
	_assertVisible(_span("Last Name"));
	_assertVisible(_textbox("dwfrm_profile_customer_lastname"));
	_assertEqual($Generic[1][7], _getValue(_textbox("dwfrm_profile_customer_lastname")));
	
	//email required field
	_assertVisible(_fieldset($Generic[2][0]));
	
	//email
	_assertVisible(_span("Email"));
	_assertVisible(_textbox("dwfrm_profile_customer_email"));
	_assertEqual($Generic[2][7], _getValue(_textbox("dwfrm_profile_customer_email")));
	//Password
	_assertVisible(_span("Password"));
	_assertVisible(_password("/dwfrm_profile_login_password/"));
	_assertEqual("", _getValue(_password("/dwfrm_profile_login_password/")));
	_assertVisible(_div($Generic[3][0]));
	//Apply
	_assertVisible(_submit("dwfrm_profile_confirm"));

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("261260/261262", "Verify the validation related to  'First name and Last name'/'Email', 'Confirm Email', 'Password' and 'Confirm Password'  fields and functionality related to 'Apply' button on Edit Account  page in application as an  Anonymous user.");
$t.start();
try
{
	for(var $i=0;$i<$Account_Validation.length;$i++)
		{
		  _log($i);
		  if($i==0)
			  {
			  	 _setValue(_textbox("dwfrm_profile_customer_firstname"), "");
			 	 _setValue(_textbox("dwfrm_profile_customer_lastname"), "");
			 	 _setValue(_textbox("dwfrm_profile_customer_email"), "");
			  }
		 _setValue(_textbox("dwfrm_profile_customer_firstname"), $Account_Validation[$i][1]);
	 	 _setValue(_textbox("dwfrm_profile_customer_lastname"), $Account_Validation[$i][2]);
	 	 _setValue(_textbox("dwfrm_profile_customer_email"), $Account_Validation[$i][3]);
	 	 _setValue(_password("/dwfrm_profile_login_password/"), $Account_Validation[$i][4]);
	 	 if($i!=1)
	 		 {
	 		 	_click(_submit("dwfrm_profile_confirm")); 
	 		 }	
	 	 //blank field validation
	 	 if($i==0)
		 	 {
		 		 _assertEqual($color, _style(_textbox("dwfrm_profile_customer_firstname"), "background-color"));
		 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_lastname"), "background-color"))
		 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_email"), "background-color"));
		 		  _assertEqual($color, _style(_password("/dwfrm_profile_login_password/"), "background-color"));
		 	 }
	 	 //Max characters
	 	 else if($i==1)
	 		 {
		 		_assertEqual($Account_Validation[0][7],_getText(_textbox("dwfrm_profile_customer_firstname")).length);
		 		_assertEqual($Account_Validation[1][7],_getText(_textbox("dwfrm_profile_customer_lastname")).length);
		 		_assertEqual($Account_Validation[2][7],_getText(_textbox("dwfrm_profile_customer_email")).length);
		 		_assertEqual($Account_Validation[4][7],_getText(_password("/dwfrm_profile_login_password/")).length);
	 		 }
	 	 //Invalid data
	 	 else if($i==2 || $i==3 ||$i==4 )
	 		 {
		 		 _assertVisible(_div($Account_Validation[$i][5],_near(_textbox("dwfrm_profile_customer_email"))));
		 		 //_assertEqual($color, _style(_textbox("dwfrm_profile_customer_email"), "background-color")); 		  
		 		 //_assertEqual($color, _style(_textbox("dwfrm_profile_customer_emailconfirm"), "background-color"));
	 		 }	
	 	 //Valid
		 else
			 {
			 	 _click(_span("/Hello/"));
			 	 _click(_heading3("My Account"));
				 _assertVisible(_heading1($Generic[5][0]+" "+$Account_Validation[$i][1]+" "+$Account_Validation[$i][2]+" "+$Generic[6][0]));
				 _assertVisible(_link("Logout"));
				 
			 }
		}
	
	
//left nav in my account landing page	
var $links=_collectAttributes("_listItem","/(.*)/","sahiText", _in(_list("account-primary-nav")));

for (var $i=0;$i<$links.length;$i++)
{
_assertVisible(_link($links[$i]));
_click(_link($links[$i]));
//verifying heading

if ($i==0)
	{
	 _assertVisible(_heading1($Generic[5][0]+" "+$Account_Validation[5][1]+" "+$Account_Validation[5][2]+" "+$Generic[6][0]));
	}
	else
		{
		//headings and navigation
		_assertVisible(_heading1($Generic[$i][3]));
		}
}

//click on My Account link	
_click(_link("Profile", _near(_link("user-account"))));
//click on create account link
_click(_heading2("Personal Data"));
//click on privacy policy link
_click(_link($Generic[0][6]));
//heading in privacy policy page
_assertVisible(_heading1($Generic[0][5]));

//click on My Account link	
_click(_link("Profile", _near(_link("user-account"))));
//click on create account link
_click(_heading2("Personal Data"));	 
//click on privacy policy link
_click(_link($Generic[1][6]));
//heading in privacy policy page
_assertVisible(_heading1($Generic[1][5]));


//verify the hello 
_click(_span("/Hello/", _in(_link("user-account"))));
//click on logout link
_click(_link("/user-logout/"));
 
//click on My Account link	
 _click(_span("My Account"));
 //login with new account
 _setValue(_textbox("/dwfrm_login_username/"),$Account_Validation[5][3]);
 _setValue(_password("/dwfrm_login_password/"),$Account_Validation[5][4]);
 _click(_submit("dwfrm_login_login"));
 
 //resetting the data
 _click(_span("/Hello/", _in(_link("user-account"))));
 _click(_heading3("My Account"));
 _click(_heading2("Personal Data"));
 _setValue(_textbox("dwfrm_profile_customer_firstname"), $Generic[0][7]);
 _setValue(_textbox("dwfrm_profile_customer_lastname"),$Generic[1][7]);
 _setValue(_textbox("dwfrm_profile_customer_email"),$Generic[2][7]);
 _setValue(_password("/dwfrm_profile_login_password/"),$Generic[3][7]);
 _click(_submit("dwfrm_profile_confirm"));
_wait(6000);
_click(_span("/Hello/", _in(_link("user-account"))));
_assertVisible(_heading1($Generic[5][0]+" "+$Generic[0][7]+" "+$Generic[1][7]+" "+$Generic[6][0]));
_assertVisible(_link("/user-logout/"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();
