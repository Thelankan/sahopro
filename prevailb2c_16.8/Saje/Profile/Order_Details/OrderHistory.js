_include("../../util.GenericLibrary/GlobalFunctions.js");
//_include("../../util.GenericLibrary/BM_Configuration.js");
//_resource("Order_Data.xls");

var $Generic=_readExcelFile("Order_Data.xls","Generic");
var $Account=_readExcelFile("Order_Data.xls","Account");
var $Valid_Data=_readExcelFile("Order_Data.xls","Valid_Data");
var $creditcard_data=_readExcelFile("Order_Data.xls","Payment_Data");
var $paypal=_readExcelFile("Order_Data.xls","Paypal");

SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);
//login to the application
var $TemID="SajeUS@gmail.com";
var $TemPWD="India@123"



var $t = _testcase("261267/261268","Verify the UI of 'My Account Order history page' when there are no order records created in application as a Registered user.");
$t.start();
try
{
	if(!isMobile())
		{
		 	_click(_span($Generic[1][4]));
			_setValue(_textbox("/dwfrm_login_username/"),  $TemID);
			_setValue(_password("/dwfrm_login_password/"), $TemPWD);
			_click(_submit("dwfrm_login_login"));
			_wait(3000);
		}
	else
	{
		_click(_submit("/menu-toggle /"));
		_click(_span($Generic[1][4], _in(_div("sign-right js-device-header-login"))));
    	_wait();
    	_setValue(_textbox("/dwfrm_login_username/", _in(_div("sign-right js-device-header-login"))),  $TemID);
    	_setValue(_password("/dwfrm_login_password/" ,_in(_div("sign-right js-device-header-login"))), $TemPWD);
  	    _click(_submit("dwfrm_login_login",_in(_div("sign-right js-device-header-login"))));
		_wait(3000);
		
		//_assertVisible(_heading2($Login_Data[3][1], _in(_div("sign-right js-device-header-login"))));
	}
   
	_click(_link($Generic[6][0]));
	_wait(4000);
	//click on the order history
	_click(_link($Generic[3][4]));
	//verify the navigation 
	_assertVisible(_heading1($Generic[1][0]));
	_assertEqual($Generic[1][5], _getText(_div("no_orders bonetext")));
	//click on the logout link
	_click(_link("user-logout buttonstyle"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

login();
var $t = _testcase("261266","Verify the UI of 'My Account Order history page' when there are multiple order records in application as a Registered user.");
$t.start();
try
{
	
	//navigate to cart page
	
			search($Generic[0][3]);
			_click(_submit("add-to-cart"));
			//navigate to cart page
			_click(_link("button mini-cart-link-cart", _in(_div("mini-cart-totals"))));
			var $ProdPrice=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
			var $ProdPrice=round($ProdPrice,2);
			var $prodquantity=_extract(_getText(_div("quantity cart-no-image")),"/: (.*)/",true).toString().split(" ")[0];

			//navigate to checkout page
			//click on the checkout
			_click(_link("/mini-cart-link-checkout/", _in(_div("mini-cart-content"))));
			
			//_click(_submit("dwfrm_login_unregistered"));
			//comparing product name in shipping page
			var $pnameshippingpage=_getText(_link("/(.*)/", _in(_div("/mini-cart-name/")))).toString();
			
			shippingAddress($shippingAddress,0);
			
			////address proceed
			//adddressProceed();
			
			_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
			_click(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
						
			var $priceinshippingpage=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
			
			_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
			// adddressProceed();
			_setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"), $uId);
			var $billingprice=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
			//comparing shipping and billing price
			_assertEqual($priceinshippingpage,$billingprice);
			var $shipppingCharge=parseFloat(_extract(_getText(_row("order-shipping bonetext  first ")),"/[$](.*)/",true));
			var $saleTax=parseFloat(_extract(_getText(_row("order-sales-tax bonetext")),"/[$](.*)/",true));
			var $ExpTotPrice=$ProdPrice+$shipppingCharge+$saleTax;
			var $ExpTotPrice=round($ExpTotPrice,2);
			
			//Payment details
			PaymentDetails($creditcard_data,0);
			                    
			//Placing order
			// _click(_checkbox("/dwfrm_billing_confirm/"));
			_click(_submit("dwfrm_billing_save"));
			
			//Checking order is placed or not
			//_assertVisible(_heading1($payment_data[1][3]));
			_assertEqual("Step 3: Place Order", _getText(_div("step-3 active")));
			
		
			_click(_submit("submit"));
			_wait(3000);
			 var $Qty=_getText(_div("/product-list-item/",_near(_div("Qty"))));
			_assertEqual($prodquantity,$Qty);
			
			
			var $priceinOCP=_extract(_getText(_div("line-item-price", _in(_div("order-shipment-table")))),"/[$](.*)/",true);
			
			var $ActordertotalOCP=_extract(_getText(_row("order-total", _in(_div("order-detail-summary")))),"/[$](.*)/",true);
			//price verification
			_assertEqual($ProdPrice,$priceinOCP);
			_assertEqual($billingprice,$ExpTotPrice);
			_assertEqual($ExpTotPrice,$ActordertotalOCP);
			
			
			var $Exporderno=_extract(_getText(_heading3("order-number")),"/[:] (.*)/",true);
			_log($Exporderno);
			
			// var $orderkkkk=_getText(_span("order-number"))
			// _log($orderkkkk);
			
			_focusWindow();
			_takePageScreenShot();
			
			//payment in fo
			_assertVisible(_div("order-payment-instruments"));
			//_assertContainsText($credit_card[$i][5], _div("order-payment-instruments"));
			_wait(5000);
			//navigate to my account home page
			_click(_link("user-account"));
			_click(_link($Generic[6][0]));
			//click on the order history
			_click(_link($Generic[3][4]));
			//verify the navigation 
			_assertVisible(_div("order-history-page"));
			var $ActOrder=_extract(_getText(_div("order-number")),"/[:] (.*)/",true);
			_assertEqual($Exporderno,$ActOrder);
			var $PrdName=_getText(_cell("order-items bonetext"))
			_assertEqual($pnameshippingpage,$PrdName);
			_assertVisible(_heading1($Generic[1][0]));
			_assertVisible(_div("pagination order-pagingbar"));
			_assertVisible(_link("page-next"));

			_assertVisible(_div("order-date"));
			_assertVisible(_div("order-status"));
			_assertVisible(_div("order-number"));
			_assertVisible(_submit($Generic[2][0]));
			_assertVisible(_tableHeader("Shipped to:"));
			_assertVisible(_tableHeader("Order Total:"));



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("261269","Verify the navigation related to 'Order Details' button on 'My Account order history' page in application as a Registered user.");
$t.start();
try
{
	//click on the order details
	_click(_submit("dwfrm_orders_orderlist_i0_show"));
	//verify the navigation 
	_assertVisible(_div("order-details-header sone-text"));
	_assertVisible(_div("orderdetails"));
	
	//UI of the order details page
	_assertVisible(_div("instruments-billing-summary"));
	_assertVisible(_div($Generic[4][4]));
	_assertVisible(_div($Generic[5][4]));
	_assertVisible(_div($Generic[6][4]));
	_assertVisible(_cell($Generic[1][3], _in(_div("detail-order-page"))));
	_assertVisible(_cell($Generic[2][3], _in(_div("detail-order-page"))));
	_assertVisible(_cell($Generic[3][3], _in(_div("detail-order-page"))));
	_assertVisible(_cell($Generic[4][3], _in(_div("detail-order-page"))));

	_assertVisible(_heading3($Generic[5][3]));
	_assertVisible(_div($Generic[6][3]));
	_assertVisible(_div($Generic[7][3]));
	_assertVisible(_div($Generic[7][4]));
	_assertVisible(_div("line-item-details"));

	_assertVisible(_link($Generic[4][0]));
	_assertVisible(_link("/shipping-return-back/", _in(_div("detail-order-page"))));



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("261270/261273","Verify the navigation related to 'Return to order history' link on 'My Account order summery' page in application as a Registered user");
$t.start();
try
{
	//click on the link
	_click(_link($Generic[4][0]));
	//verify the navigation
	_assertVisible(_heading1($Generic[1][0]));
	_assertVisible(_div("order-history-page"));
	//click on the order details link 
	_click(_submit("dwfrm_orders_orderlist_i0_show"));
	//verify the navigation
	_assertExists(_div($Generic[2][0]));
	_assertVisible(_div("detail-order-page"));
	//click on the Return to shoping
	_click(_link("ptwotext shipping-return-back"));
	//verify the navigation 
	_assertVisible(_div("our best sellers"));
	_assertVisible(_link("shop all best sellers"));

	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("261275","Verify the navigation related to product name on 'My Account order summery' page in application as a Registered user");
$t.start();
try
{
	_click(_link("user-account"));
	_click(_link($Generic[6][0]));
	//click on the order history
	_click(_link($Generic[3][4]));
	//verify the navigation 
	_assertVisible(_div("order-history-page"));
	_click(_submit("dwfrm_orders_orderlist_i0_show"));
	var $ProdName=_getText(_div("name hone-header"));
	_click(_link($Generic[2][5]));
	//verify the navigation 
	_assertVisible(_div("product-col-2 product-detail"));
	_assertVisible(_div("pdpMain"));
	_assertEqual($ProdName, _getText(_heading1("product-name sone-text", _in(_div("desktop-only")))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("261274","Verify the functionality related to pagination on 'My Account order history' page in application as a Registered user");
$t.start();
try
{
	_click(_link("user-account"));
	_click(_link($Generic[6][0]));
	//click on the order history
	_click(_link($Generic[3][4]));
	//verify the navigation 
	_assertVisible(_heading1($Generic[1][0]));
	_assertVisible(_div("order-history-page"));
	//verify the navigation 
	
	//Pagination
	if(_isVisible(_div("/pagination/")))
		{
		Orderdetailspagination();
		}
	else
		{
			_assert(false, "Pagination is not displaying");
		}
	//logout from application
	_click(_link($Generic[6][0]));
	_click(_link("Logout"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
function Orderdetailspagination()
{
		var $OrderINPage=parseInt(_count("_div","/order-history-items/",_in(_div("order-history-page"))));
		var $TotalOrder=parseInt(_extract(_getText(_div("middle-move-link-box bonetext")),"/[Of] (.*) [R]/",true));
		var $TotalPages=parseInt($TotalOrder/$OrderINPage);
		for(var $i=0;$i<$TotalPages-1;$i++)
			{
				if($i==0)
					{
					_assertNotVisible(_link("page-previous"));
					_assertVisible(_link("page-next"));
					_click(_link("page-next"));
					}
				if($i==$TotalPages-2)
					{
					_assertNotVisible(_link("page-next"));
					_assertVisible(_link("page-previous"));
					}
				else
					{
					_assertVisible(_link("page-previous"));
					_assertVisible(_link("page-next"));
					_click(_link("page-next"));
					}
			}
		for(var $i=0;$i<$TotalPages;$i++)
			{
				if($i==0)
					{
					_assertVisible(_link("page-previous"));
					_assertNotVisible(_link("page-next"));
					_click(_link("page-previous"));		
					}
				
				if($i==$TotalPages-2)
					{
					_assertVisible(_link("page-next"));
					_asserNotVisible(_link("page-previous"));
					}
				else
					{
					_assertVisible(_link("page-previous"));
					_assertVisible(_link("page-next"));
					_click(_link("page-next"));
					}
			}
}