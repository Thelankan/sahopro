//_include("../../util.GenericLibrary/BM_Configuration.js");
_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("MyLogin_Data.xls");

var $Login_Data=_readExcelFile("MyLogin_Data.xls","Login_Data");
var $Login_Validation=_readExcelFile("MyLogin_Data.xls","Login_Validation");
var $Order_Validation=_readExcelFile("MyLogin_Data.xls","Order_Validation");
var $Data=_readExcelFile("MyLogin_Data.xls","Data");
var $UserEmail=$uId;
SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);

var $t = _testcase("261210","Verify the navigation to MY PROFILE login page in application as an Anonymous user");
$t.start();
try
{
	if(!isMobile())
		{
		//click on login link
		_click(_span($Login_Data[4][1]));
		//verify the navigation
		_assertVisible(_div("header-login-box header-login-account"));
		_assertVisible(_heading2($Login_Data[3][1]));
		//_assertVisible(_heading1($Login_Data[0][0]));
		
		}
	else
		{
//		_click(_submit("menu-nav-toggle menu-toggle"));
		_click(_span($Login_Data[4][2]));
		_click(_span($Login_Data[4][1], _in(_div("sign-right js-device-header-login"))));
		_assertVisible(_div("user-panel account", _in(_div("sign-right js-device-header-login"))));
		_assertVisible(_heading2($Login_Data[3][1], _in(_div("sign-right js-device-header-login"))));

		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("261213/261214/248956","Verify the UI of 'SIGN IN TO SAJE.COM' section MY PROFILE login page in application as an Anonymous user");
$t.start();
try
{
	//click on the sign in 
	_click(_link("my-account"));
	//verify the navigation
	_assertVisible(_div("/create_account/"));
	//click on the my account link 
	_click(_link($Login_Data[1][3],_in(_div("secondary-navigation"))));
	//verify the navigation
	_assertVisible(_div("my-account-login"));
	//verify the content on SIGN IN TO SAJE.COM
	_assertVisible(_heading1($Login_Data[3][6]));
	//Following text 'If you are a Registered user, please enter your email and password.' should be displayed below the section heading.- ' 
	_assertEqual($Login_Data[3][7], _getText(_div("dialog-required bonetext")));
	//Email Password
	_assertVisible(_span($Login_Data[2][0]));
	_assertVisible(_span($Login_Data[3][0]));
	_assertVisible(_span($Login_Data[5][0], _in(_div("login-box-content returning-customers clearfix"))));
	_assertEqual($Login_Data[0][0], _getText(_submit("dwfrm_login_login", _in(_div("login-box-content returning-customers clearfix")))));
	_assertEqual($Login_Data[4][0], _getText(_link("password-reset-id ptwotext ipad-desktop-show desktop-reset", _in(_div("login-box-content returning-customers clearfix")))));

	//verify the content on guest or new to saje.com
	_assertVisible(_heading2($Login_Data[4][6]));
	//Static text
	_assertEqual($Login_Data[1][1], _getText(_submit("dwfrm_login_register")));
	_assertVisible(_paragraph($Login_Data[4][7], _near(_heading2($Login_Data[4][6]))));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("261212//248942","Verify 'LET's BE PARTNERS IN WELLNESS' section as an Registered user.");
$t.start();
try
{
	//5. 'LET's BE PARTNERS IN WELLNESS' section should Display as below as content slot.
	_assertVisible(_heading1($Login_Data[5][2]));
	_assertVisible(_span($Login_Data[6][2]));
	//a.Quick Ordering.
	_assertVisible(_span($Login_Data[7][2]));
	//b.Store your Information.
	_assertVisible(_span($Login_Data[8][2]));
	//c.See Order History.
	_assertVisible(_span($Login_Data[9][2]));
	//d.Instant Product Review.
	_assertVisible(_span($Login_Data[10][2]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("261217","Verify the navigation related to 'Create an account now' button MY PROFILE login page, New customers section in application as an Anonymous user");
$t.start();
try
{
	//Click on the 'Create an account now' button. 
	_click(_submit("dwfrm_login_register"));
	//verify the navigation
	_assertVisible(_heading1($Login_Data[1][1]));
	_assertVisible(_div("/create_account/"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("261220/261221/261219/261218","Verify the navigation related to 'Privacy Policy' link on MY PROFILE login page left nav in application as an Anonymous user");
$t.start();
try
{
	_click(_link("my-account"));
	//verify the navigation
	_assertVisible(_div("/create_account/"));
	var $LeftNavLink=_collectAttributes("_link","/(.*)/","sahiText",_in((_div("secondary-navigation"))));
	for(var $i=0;$i<$LeftNavLink.length;$i++)
		{
		_click(_link("my-account"));
		//verify the navigation
		_assertVisible(_div("/create_account/"));
		_click(_link($LeftNavLink[$i]));
		if($i==0)
			{
			_assertVisible(_div("my-account-login"));

			}
		if($i==1)
			{
			_assertVisible(_heading1($Login_Data[1][1]));
	
			}
		if($i==2)
			{
			_assertVisible(_heading1($Login_Data[0][4]));
			
			}
		if($i==3)
			{
			_assertVisible(_heading1($Login_Data[1][4]));
			}
		}
	_assert(false,"Customer Service ");
	_assert(false,"Need Help ");

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("261222/248950","Verify the functionality related of 'Forgot Password' link on MY PROFILE login's page Returning customers section in site as an Anonymous user");
$t.start();
try
{
	if(!isMobile())
	{
	//click on login link
	_click(_span($Login_Data[4][1],_in(_div("sign-right js-device-header-login"))));
	//verify the navigation
	_assertVisible(_div("header-login-box header-login-account"));
	_assertVisible(_heading2($Login_Data[3][1]));
	//_assertEqual($Login_Data[1][3], _getText(_span("account-heading nfourtext")));

	//_assertVisible(_heading1($Login_Data[0][0]));
	
	}
	else
		{
		_click(_submit("menu-nav-toggle menu-toggle"));
		_click(_span($Login_Data[4][2]));
		_click(_span($Login_Data[4][1], _in(_div("sign-right js-device-header-login"))));
		_assertVisible(_div("user-panel account", _in(_div("sign-right js-device-header-login"))));
		_assertVisible(_heading2($Login_Data[3][1], _in(_div("sign-right js-device-header-login"))));

		
		}
	//click on the sign IN link 
	_click(_span($Login_Data[4][1]));
	//verify the navigation
	_assertVisible(_heading2($Login_Data[3][1]));

	_click(_link("password-reset"));
	//verify the overlay 
	_assertVisible(_div("passwd-reset-header-form"));
	_assertVisible(_heading1($Login_Data[1][5]));
	_assertVisible(_paragraph("Provide your account email address to receive an email to reset your password.", _in(_div("passwd-reset-header-form"))));
	_assertVisible(_span($Login_Data[2][3], _in(_div("passwd-reset-header-form"))));
	_assertVisible(_textbox("dwfrm_requestpassword_email", _in(_div("passwd-reset-header-form"))));
	_assertVisible(_submit("Send", _in(_div("passwd-reset-header-form"))));
	_assertVisible(_button("Close"));
	//verify the functionallity of close button
	_click(_button("Close"));
	_assertNotVisible(_div("passwd-reset-header-form"));
	_assertNotVisible(_heading1($Login_Data[1][5]));
	//click on the forgot password link
	_click(_link("password-reset"));
	//set Email 
	_setValue(_textbox("dwfrm_requestpassword_email"), $uId);
	_click(_submit("dwfrm_requestpassword_send"));
	_assertVisible(_heading1($Login_Data[4][5], _in(_div("forgot-password-sec-overlay"))));
	_assertVisible(_link($Login_Data[3][3], _in(_div("forgot-password-sec-overlay"))));
	_click(_link($Login_Data[3][3], _in(_div("forgot-password-sec-overlay"))));
	_assertNotVisible(_div("passwd-reset-header-form"));
	_assertNotVisible(_heading1($Login_Data[1][5]));
	_assertVisible(_div("homepage-banner"));



}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("261216 ","Verify the functionality related of 'LOGIN' button MY PROFILE login's page, SIGN IN TO SAJE.COM section in site as an Anonymous user");
$t.start();
try
{
	//click on the sign in 
	_click(_link("my-account"));
	//verify the navigation
	_assertVisible(_div("/create_account/"));
	//click on the my account link 
	_click(_link($Login_Data[1][3],_in(_div("secondary-navigation"))));
	//verify the navigation
	_assertVisible(_div("my-account-login"));
	_setValue(_textbox("/dwfrm_login_username/", _in(_div("login-box login-account"))), $uId);
	_setValue(_password("/dwfrm_login_password/", _in(_div("login-box login-account"))), $pwd);
	_click(_submit("dwfrm_login_login", _in(_div("login-box login-account"))));
	_wait(3000);
	_assertVisible(_div("my-account-page"));
	_assertEqual($Login_Data[1][3], _getText(_span("account-heading nfourtext")));
	logout();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("261224/248951/248952/243908","Verify the validation related to the 'Password' field on MY PROFILE login page, Returning customers section in application as a Anonymous user");
$t.start();
try
{

	if(!isMobile())
		{
		_click(_span($Login_Data[4][1]));
		_assertVisible(_heading2($Login_Data[3][1]));
	     
		}
	else
		{
		//_click(_submit("menu-nav-toggle menu-toggle"));
		_click(_span($Login_Data[4][2]));
		_assertVisible(_div("user-panel account", _in(_div("sign-right js-device-header-login"))));
		_assertVisible(_heading2($Login_Data[3][1], _in(_div("sign-right js-device-header-login"))));

		}


	//validating the email id & password field's
	for(var $i=0;$i<$Login_Validation.length;$i++)
		{
		if($i==5)
			{
				login();
			}
		else
			{
			_setValue(_textbox("/dwfrm_login_username/"),$Login_Validation[$i][1]);
			_setValue(_password("/dwfrm_login_password/"),$Login_Validation[$i][2]);
			_click(_submit("dwfrm_login_login"));
			}	
		if (isMobile())
			{
			_wait(3000);
			}
		//blank
		if($i==0)
			{
			//username
			_assertEqual($Login_Validation[$i][5],_style(_textbox("/dwfrm_login_username/"),"background-color"));
			_assertVisible(_span($Login_Validation[$i][3]));
			//password
			_assertVisible(_span($Login_Validation[$i][4]));
			_assertEqual($Login_Validation[$i][5],_style(_password("/dwfrm_login_password/"),"background-color"));
			}
		
		//in valid email
		else if($i==1)
			{
			_assertEqual($Login_Validation[2][3], _getText(_span("error")));
			_assertEqual($Login_Validation[2][4],_getText(_textbox("/dwfrm_login_username/")).length);
			_assertEqual($Login_Validation[2][4],_getText(_password("/dwfrm_login_password/")).length)
			
			}
		else if($i==2 || $i==3)
			{
			_assertEqual($Login_Validation[2][3], _getText(_span("error")));
			}
		
		//valid
		else if($i==5)
			{
			_click(_heading3($Login_Data[1][3]));
			if (isMobile())
				{
			_wait(3000,_isVisible(_list("account-options")));
				}
			_assertEqual("Logout", _getText(_span("account-logout bonetext")));
			_assertVisible(_span("/"+$Login_Data[1][3]+"/", _in(_div("account-info-content"))));

			}
		//invalid 
		else
			{
			_assertVisible(_div($Login_Validation[1][3]));
			}
		}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("261211","Verify the navigation to MY PROFILE login page in application as an Registered user.");
$t.start();
try
{
	
	_assertVisible(_span("/Hello,/", _in(_link("user-account"))));
	_click(_span("/Hello/"));
	
	_assertVisible(_div("user-panel account"));
	_click(_link("Profile"));
	_assertEqual($Login_Data[1][3], _getText(_span("account-heading nfourtext")));
	_assertVisible(_div("my-account-page"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("","");
$t.start();
try
{


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("","");
$t.start();
try
{


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();