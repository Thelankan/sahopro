_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("CreateAccount.xls");

var $accountdata=_readExcelFile("CreateAccount.xls","CreateaccountData");
var $Account_Validation=_readExcelFile("CreateAccount.xls","Account_Validation");

var $color=$Account_Validation[0][6];
var $color1=$Account_Validation[1][6];
//deleting the user if exists
deleteUser();

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("261229", "Verify the navigation  of 'CREATE AN ACCOUNT '  link of My Account Login page.");
$t.start();
try
{

//click on My Account link	
_click(_span("My Account"));
//click on login button
_click(_submit("dwfrm_login_login"));
//error messages should be displayed
_assertVisible(_span($accountdata[0][2]));
_assertVisible(_span($accountdata[1][2]));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
 $t.end();

var $t = _testcase("261228/261230/261239", "Verify the navigation of 'CREATE AN ACCOUNT'  link of SIGN IN drop down menu of My Profiel /Verify  UI  of 'Create Account  page/Verify the UI  of 'Create Account' page in application as an  Anonymous user. .");
$t.start();
try
{

//click on My Account link	
_click(_span("My Account"));
//click on create account link
_click(_link("Create an Account"));
//It should navigate to create account page
_assertVisible(_heading1($accountdata[0][0]));
//Create account UI 
_assertVisible(_span("First Name"));
_assertVisible(_textbox("dwfrm_profile_customer_firstname"));
_assertVisible(_span("Last Name"));
_assertVisible(_textbox("dwfrm_profile_customer_lastname"));
_assertVisible(_span("Email"));
_assertVisible(_textbox("dwfrm_profile_customer_email"));
_assertVisible(_span("Password"));
_assertVisible(_password("/dwfrm_profile_login_password/"));
_assertVisible(_div($accountdata[1][0]));//create account button
_assertVisible(_submit("dwfrm_profile_confirm"));
//UI fields of my account landing page
_assertVisible(_label($accountdata[0][1]));
_assertVisible(_label($accountdata[1][1]));
_assertVisible(_label($accountdata[2][1]));
_assertVisible(_label($accountdata[3][1]));
//password message
_assertVisible(_div($accountdata[2][2],_near(_password("/dwfrm_profile_login_password/"))));

//left nav links
var $leftnav=_count("_link","/(.*)/",_in(_div("secondary-navigation")));

for (var $i=0;$i<$leftnav;$i++)
	{
	_assertVisible(_link($accountdata[$i][4]));
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
 $t.end();

 
 var $t = _testcase("261232/261233/261236/261237/--/125240/124599", "Verify the validation related to  'First name'/'Last name'/'Email'/'Confirm Email'/'Password'/'Confirm Password' field  and  functionality related to 'Apply' button on Create account page in application as an  Anonymous user.");
  $t.start();
  try
  {
	  //click on My Account link	
	  _click(_span("My Account"));
	  //click on create account link
	  _click(_link("Create an Account"));
	  
	//It should navigate to create account page
	_assertVisible(_heading1($accountdata[0][0]));
  	for(var $i=0;$i<$Account_Validation.length;$i++)
  		{
  			if($i==7)
  				{
  					createAccount();
  					//verify whether account got created or not
  			 	   var $exp=$userData[$user][1]+" "+$userData[$user][2];
  			 	   _assertEqual($exp, _getText(_span("account-user-name sone-text")).toString());
  			 	   _click(_link("Logout"));
  				}
  			else
  				{
  				 _setValue(_textbox("dwfrm_profile_customer_firstname"), $Account_Validation[$i][1]);
  			 	 _setValue(_textbox("dwfrm_profile_customer_lastname"), $Account_Validation[$i][2]);
  			 	 _setValue(_textbox("dwfrm_profile_customer_email"), $Account_Validation[$i][3]);
  			 	 _setValue(_password("/dwfrm_profile_login_password/"), $Account_Validation[$i][4]);
  			 	 if($i!=1)
  			 		 {
  			 			_click(_submit("dwfrm_profile_confirm")); 
  			 		 }	
  			 	 //blank field validation
  			 	 if($i==0)
  			 		 {
  			 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_firstname"), "background-color"));
  			 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_lastname"), "background-color"));
  			 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_email"), "background-color"));
  			 		  _assertEqual($color, _style(_password("/dwfrm_profile_login_password/"), "background-color"));
  			 		  _assertEqual($Account_Validation[0][9], _getText(_span("dwfrm_profile_customer_firstname-error")));
  			 		  _assertEqual($Account_Validation[1][9], _getText(_span("dwfrm_profile_customer_lastname-error")));
  			 		  _assertEqual($Account_Validation[2][9], _getText(_span("dwfrm_profile_customer_email-error")));
  			 		_assertEqual($Account_Validation[3][9], _getText(_span("/dwfrm_profile_login_password/")));
  			 		
  			 		 }
  			 	 //Max characters
  			 	 else if($i==1)
  			 		 {
  			 		_assertEqual($Account_Validation[0][7],_getText(_textbox("dwfrm_profile_customer_firstname")).length);
  			 		_assertEqual($Account_Validation[1][7],_getText(_textbox("dwfrm_profile_customer_lastname")).length);
  			 		_assertEqual($Account_Validation[2][7],_getText(_textbox("dwfrm_profile_customer_email")).length);
  			 		_assertEqual($Account_Validation[4][7],_getText(_password("/dwfrm_profile_login_password/")).length);
  			 		 }
  			 	 //invalid data
  			 	 else if($i==2 || $i==3 ||$i==4 || $i==5)
  			 		 {
  			 		 _assertVisible(_span($Account_Validation[$i][5], _in(_div("/Email/"))));
  			 		 //_assertEqual($color, _style(_textbox("dwfrm_profile_customer_email"), "background-color"));	 		  
  			 		 //_assertEqual($color, _style(_textbox("dwfrm_profile_customer_emailconfirm"), "background-color"));
  			 		 }	 	 
  			 	 //password less than 8 characters
  			 	 else if($i==6)
  			 		 {	 
  			 	  _assertEqual($Account_Validation[6][9], _getText(_div("form-caption error-message")));
  			 		 }
  				}
  		}	
  }
  catch($e)
  {
  	_logExceptionAsFailure($e);
  }	
  $t.end();
  
var $t = _testcase("261240","Verify the navigation related to ''Privacy Policy'  link  on Create account page left nav in  application as an  Anonymous user.");
$t.start();
try
 {

//click on My Account link	
_click(_span("My Account"));
//click on create account link
_click(_link("Create an Account"));
//click on privacy policy link
_click(_link($accountdata[3][2]));
//heading in privacy policy page
_assertVisible(_heading1($accountdata[0][3]));

 }
  catch($e)
  {
  	_logExceptionAsFailure($e);
  }	
 $t.end();
 
 var $t = _testcase("261242","Verify the navigation related to 'Secure Shopping' link  on Create account page left nav in  application as an  Anonymous user.");
 $t.start();
 try
  {

//click on My Account link	
 _click(_span("My Account"));
 //click on create account link
 _click(_link("Create an Account"));	 
 //click on privacy policy link
 _click(_link($accountdata[4][2]));
 //heading in privacy policy page
 _assertVisible(_heading1($accountdata[1][3]));

  }
   catch($e)
   {
   	_logExceptionAsFailure($e);
   }	
  $t.end();
  
  /* var $t = _testcase(" ", " ");
  $t.start();
  try
 {


 }
  catch($e)
  {
  	_logExceptionAsFailure($e);
  }	
   $t.end();*/