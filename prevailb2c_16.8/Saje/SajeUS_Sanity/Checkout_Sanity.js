_include("../util.GenericLibrary/GlobalFunctions.js");

_resource("SajeSanity.xls");
_resource("../Checkout/BillingPage/MPCheckout.xls");

var $credit_card= _readExcelFile("../Checkout/BillingPage/MPCheckout.xls","credit_card");
var $accountdata= _readExcelFile("SajeSanity.xls","OCPCreateAccount");
var $cardDetails= _readExcelFile("SajeSanity.xls","Payment");
var $Address= _readExcelFile("SajeSanity.xls","ShippingAddress");
var $sanitydata= _readExcelFile("SajeSanity.xls","SanityData");
var $profaddr= _readExcelFile("SajeSanity.xls","Address");
var $OwnerName=new Array();
var $Profile_CardName;


deleteProvidedUser($accountdata[0][3]);
_wait(3000);
SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


var $OwnerName=new Array();
var $Address1=$Address[0][1]+" "+$Address[0][2]+" "+$Address[0][3]+" "+$Address[0][4]+" "+$Address[0][7]+" "+$Address[0][11]+" "+$Address[0][8]+" "+$Address[0][5]+" "+$Address[0][9];
var $ship_Address="Shipping To: "+$Address1;
//var $billingAddress="Billing Address "+$Address[0][1]+" "+$Address[0][2]+" "+$Address[0][3]+" "+$Address[0][4]+" "+$Address[0][7]+" "+$Address[1][6]+" "+$Address[0][8]+" "+$Address[0][5]+" "+"Phone: "+$Address[0][9];
var $bill_Address="Billing Address "+$Address[0][1]+" "+$Address[0][2]+" "+$Address[0][3]+" "+$Address[0][4]+" "+$Address[0][7]+" "+$Address[0][11]+" "+$Address[0][8]+" "+$Address[0][5]+" "+$Address[0][9];

//******************** Guest User **********************
//******************* MPC Login ************************
var $t = _testcase("248948","Verify the functionality of 'Create account now' button in intermediate login page in application as an Anonymous user.");
$t.start();
try
{
	//add items to cart
	navigateToCart($sanitydata[0][0],1);
	//click on go strait to checkout link
	_click(_link("mini-cart-link-checkout"));
	_click(_link("mini-cart-link-checkout"));
	//click on the create account now
	_click(_submit("dwfrm_login_register"));
	//verify the navigation
	_assertVisible(_div("account-rightinner"));
	_assertVisible(_heading1($sanitydata[4][1]));
	_assertVisible(_div("/create_account account/"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248949","Verify the functionality of 'Checkout as guest' button in intermediate login page in application as an Anonymous user");
$t.start();
try
{
	_click(_link("mini-cart-link-checkout"));
	//click on checkout as guest button. 
	_click(_submit("dwfrm_login_unregistered"));
	//verify the navigation
	_assertVisible(_div("js-checkout-shipping"));
	_assertVisible(_div("shipping-method-list"));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

ClearCartItems();

var $t=_testcase("249083/249084/249085/249086/249028/249052","Verify whether user is able to place order using VISA, MASTERCARD, AMERICAN EXPRESS, DISCOVER as credit card type  and Credit card alone on billing page in Application as a Anonymous user.");
$t.start();
try
{
       for(var $i=0;$i<$credit_card.length;$i++)
              {
                   //navigate to cart page
    		       search($sanitydata[0][0]);
    		       _wait(2000);
    	           _click(_submit("add-to-cart"));
    	           _wait(5000);
    	           //navigate to cart page
    	          _click(_link("button mini-cart-link-cart", _in(_div("mini-cart-totals"))));
        	      	var $ProdPrice=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
    		      	var $ProdPrice=round($ProdPrice,2);
    		      	if(!isMobile())
    		      	{
                        var $prodquantity=_extract(_getText(_div("quantity cart-no-image")),"/: (.*)/",true).toString().split(" ")[0];
    		      	}
                    //navigate to checkout page
                 
    		      	 //click on the checkout
    		      	_click(_link("/mini-cart-link-checkout/", _in(_div("mini-cart-content"))));
    		      	
    		      	_click(_submit("dwfrm_login_unregistered"));
                     //comparing product name in shipping page
    		      	var $pnameshippingpage=_getText(_link("/Product/"));
                    //var $pnameshippingpage=_getText(_link("/(.*)/", _in(_div("/mini-cart-name/")))).toString();
             
                     shippingAddress($shippingAddress,0);
                 
                     _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
                     _click(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
                    
                     var $priceinshippingpage=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
                     
                     _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
                    // adddressProceed();
                     _setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"), $uId);
                     var $billingprice=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
                     //comparing shipping and billing price
                     _assertEqual($priceinshippingpage,$billingprice);
                     var $shipppingCharge=parseFloat(_extract(_getText(_row("order-shipping bonetext  first ")),"/[$](.*)/",true));
                 	 var $saleTax=parseFloat(_extract(_getText(_row("order-sales-tax bonetext")),"/[$](.*)/",true));
                     var $ExpTotPrice=$ProdPrice+$shipppingCharge+$saleTax;
                     var $ExpTotPrice=round($ExpTotPrice,2);

                     //Payment details
                       PaymentDetails($credit_card,$i);
                       var $CardName= _getText(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"));
             		
                     _click(_submit("dwfrm_billing_save"));
                     
                     //verifying the progress indicators
                     _assertVisible(_link("Shipping"));
                     _assertVisible(_link("Billing"));
                     //_assertVisible(_link("Place Order"));
                     _assertVisible(_submit("Place Order"));
                     
                     _assertEqual($sanitydata[0][3], _getText(_div("/step-3 active/")));

                     var $pnameinocppage=_getText(_link("/(.*)/", _in(_div("product-list-item")))).toString();
                  
                     _click(_submit("submit"));
                     
                     if (_isVisible(_div("dialog-container")))  
                     {
                       _click(_button("Close"));
                     }

                     _wait(3000);
                     if(!isMobile())
	       			  {
	       			  var $Qty=_getText(_div("/product-list-item/",_near(_div("Qty"))));
	       			  _assertEqual($prodquantity,$Qty);
	       			  }
                     
                     var $priceinOCP=_extract(_getText(_div("line-item-price", _in(_div("order-shipment-table")))),"/[$](.*)/",true);
                    
                     var $ActordertotalOCP=_extract(_getText(_row("order-total", _in(_div("order-detail-summary")))),"/[$](.*)/",true);
                     //price verification
                     _assertEqual($ProdPrice,$priceinOCP);
                     _assertEqual($billingprice,$ExpTotPrice);
                     _assertEqual($ExpTotPrice,$ActordertotalOCP);
                     //verify the address
//                     _assertEqual($bill_Address, _getText(_div("order-billing")));
//                     _assertEqual($ship_Address, _getText(_div("order-shipment-address")));

                     
                     var $orderno=_extract(_getText(_heading3("order-number")),"/[:] (.*)/",true);
                     _log($orderno);
                     
                     _focusWindow();
                     _takePageScreenShot();
                     
                     //payment in fo
                     _assertVisible(_div("order-payment-instruments"));
                     //_assertContainsText($credit_card[$i][5], _div("order-payment-instruments"));
                     _wait(5000);
                 }
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("249136","Verify the functionality of  'CREATE AN ACCOUNT' button in right nav on Receipt Page  in Application as a Anonymous user.");
$t.start();
try
{

//create account in OCp page
_setValue(_textbox("dwfrm_profile_customer_firstname"),$accountdata[0][1]);
_setValue(_textbox("dwfrm_profile_customer_lastname"),$accountdata[0][2]);
_setValue(_textbox("dwfrm_profile_customer_email"),$accountdata[0][3]);
_setValue(_password("/dwfrm_profile_login_password/"),$accountdata[0][4]);
//click on submit button
_click(_submit("dwfrm_profile_confirm"));

//verify the navigation to Profile page
_assertVisible(_span("/Hello/",_in(_link("user-account"))));
//verifying whether user is logged in or not
if(isMobile() && !mobile.iPad())
	{
	    _click(_submit("/menu-toggle/"));
		//_assertVisible(_span("/Hello/",_in(_link("user-account"))));
	    _assertVisible(_span("/Hello/", _in(_div("sign-right js-device-header-login"))));

		_assertVisible(_link("Logout"));
	}
	else
		{
			_assertVisible(_span("/Hello/",_in(_link("user-account"))));
		}

//my account landing page
_assertVisible(_div("my-account-page"));
//_assertVisible(_list("account-options"));
_assertVisible(_span("My Account", _in(_div("account-info-content"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//login to the application
cleanup();
login();
_wait(3000);

//******************* Order Confirmation ************************
var $t=_testcase("249145/249131","Verify the navigation to/UI of 'Thank you for your order' page in Laura Mercier application as a registered user/Verify the UI of the 'Order summery' Section on Receipt Page  in Application as a Registered user.");
$t.start();
try
{
	
	//navigating to billing page
	navigateToCart($sanitydata[0][0],1);
	_click(_submit("dwfrm_cart_checkoutCart"));
	_log($QuantityInCart);
	_log($prodpriceincart);
	_log($prodNameincart);
	//check use this for billing check box
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	shippingAddress($Address,0);
	if(isMobile())
	{
	$shippingTax=_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true).toString();	
	$subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
	}
	else
	{
	$shippingTax=_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true).toString();
	$subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString();
	}
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	$tax=_extract(_getText(_cell("order-sale-value")),"/[$](.*)/",true).toString();
	$Ordertotal=_extract(_getText(_cell("order-value")),"/[$](.*)/",true).toString();
	//entering Credit card Information
	PaymentDetails($cardDetails,0);
	var $orderTot=_getText(_cell("order-value"));
	//click on Place Order
	_click(_submit("/button-fancy-large/"));
	
	//verify the navigation to OCP page
	if(_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
	
	//click on Place Order
	_click(_submit("/button-fancy-large/"));
	
	//verify the navigation to OCP page
	if(_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
	_click(_submit("button-fancy-large buttonctaone"));
	//verify the navigation to OCP page
	_assertVisible(_heading1($sanitydata[0][1]));
	_assertVisible(_div("content-asset"));

	_assertVisible(_span($sanitydata[1][1]));
	_assertVisible(_span($sanitydata[2][1]));
	_assertVisible(_span("value", _near(_span($sanitydata[1][1]))));
	_assertVisible(_span("/value/",_near(_span($sanitydata[2][1]))));
	
	//fetching order number
	var $orderNumber=_extract(_getText(_heading3("order-number")),"/: (.*)/",true);
	_log("Order number is ="+$orderNumber);
	
	//product name
	_assertVisible(_link("/(.*)/", _in(_div("/name/", _in(_div("product-list-item"))))));
	var $prodnameinOCP=_getText(_link("/(.*)/", _in(_div("/name/", _in(_div("product-list-item"))))));
	_assertEqual($prodNameincart,$prodnameinOCP);
	//item number
	_assertVisible(_div($sanitydata[3][1]));
	_assertVisible(_span("/value/",_in(_div("/sku/"))));
	_assertVisible(_div("/sku/"));
	//full product details div
	_assertVisible(_div("product-list-item"));
	//shipping status
	_assertVisible(_div("/value/",_in(_div("shipping-status"))));
	//shipping method
	_assertVisible(_div("/value/",_in(_div("shipping-method"))));
	
	_assertEqual($ship_Address,_getText(_div("order-shipment-address")).replace(",",""));
	_assertVisible(_div("order-payment-instruments"));
	_assertEqual($cardDetails[0][9]+" "+$orderTot, _getText(_div("order-payment-instruments")));
	//verifying the billing address
	_assertEqual($bill_Address,_getText(_div("order-billing")).replace(",",""));
	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


//******************* Shipping Page ************************

var $t = _testcase("248957/248994/249009", "Verify the navigation to  'SHIPPING' page in Application as a Registered  user./Verify the calculation functionality in Order Summary pane on shipping page in Application as a Registered user.");
$t.start();
try
{
//add items to cart
navigateToCart($sanitydata[1][0],1);
//click on go strait to checkout link
_click(_link("mini-cart-link-checkout"));
//verify the navigation
//_assertVisible(_fieldset($sanitydata[4][2]));
//_assertVisible(_submit("dwfrm_singleshipping_shippingAddress_save"));	

verifyNavigationToShippingPage();

//enter shipping address
shippingAddress($shippingAddress,0);

//verify order summary page
var $productNameinOrdersummary=_getText(_link("/(.*)/",_in(_div("checkout-mini-cart"))));
var $quantityinOrdersummary=parseFloat(_extract(_getText(_div("/mini-cart-pricing/",_in(_div("checkout-mini-cart")))),"/: (.*) /",true));
var $PriceinOrderSummary=parseFloat(_extract(_getText(_div("/mini-cart-pricing/",_in(_div("checkout-mini-cart")))),"/[$](.*)/",true));

//fields in order summary page
_assertVisible(_cell("Subtotal", _in(_div("order-totals-container"))));
_assertVisible(_cell("Shipping", _in(_div("order-totals-container"))));
_assertVisible(_cell("Taxes", _in(_div("order-totals-container"))));
_assertVisible(_cell($OrderSummuary[5][0], _in(_div("order-totals-container"))));

_assertVisible(_row("order-subtotal", _in(_div("order-totals-container"))));
_assertVisible(_row("order-shipping", _in(_div("order-totals-container"))));
_assertVisible(_row("/order-sales-tax/", _in(_div("order-totals-container"))));
_assertVisible(_row("order-total", _in(_div("order-totals-container"))));
_assertVisible(_submit("Place Order"));


//verifying the fields
_assertEqual($prodNameincart,$productNameinOrdersummary);
_assertEqual($QuantityInCart,$quantityinOrdersummary);
_assertEqual($prodpriceincart,$PriceinOrderSummary);

//selecting radio buttons
_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
//verify the selection
_assert(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID").checked);
_assertEqual(true,_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID").checked);
var $ShippingMethodCost=parseFloat(_extract(_getText(_span("/(.*)/", _in(_div("form-row form-indent label-inline")))),"/[$](.*)/",true));
_wait(2000);
var $shippingsummaryCost=parseFloat(_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true));
_assertEqual($ShippingMethodCost,$shippingsummaryCost);

//calculating shipping total in order summary pane
var $subtotal=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));

if (_isVisible(_row("/order-sales-tax/")))
	{
	var $salestax=parseFloat(_extract(_getText(_row("/order-sales-tax/")),"/[$](.*)/",true));
	}
	else
		{
		var $salestax=0;
		}

_log("SubTotal in shipping page="+$subtotal);
_log("shipping cost in shipping page="+$shippingsummaryCost);
_log("salestax in shipping page="+$salestax);

var $addedtotal=parseFloat($subtotal+$shippingsummaryCost+$salestax);
var $totalRoundoff=round($addedtotal,2);
//_log($totaladd);
//var $addedtotal=parseInt(($totaladd)+($salestax));
//_log($addedtotal);

var $ordertotal=_extract(_getText(_row("order-total")),"/[$](.*)/",true);

//comparing added total and order total
_assertEqual($totalRoundoff,$ordertotal);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248968/248975","Verify the functionality of radio buttons under 'SELECT SHIPPING METHOD' Section on shipping page in Application as a Registered user./Verify the functionality of shipping charge on shipping page in Application as a Registered user.");
$t.start();
try
{
	
//verify the functionality of radio buttons
var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_div("shipping-method-list")));
var $count=0;
for(var $i=0;$i<$totalShippingMethods;$i++)
	{
	//selecting radio buttons
	_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
	//verify the selection
	_assert(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked);
	_assertEqual(true,_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked);
	var $ShippingMethodCost=_extract(_getText(_span("/(.*)/", _in(_div("form-row form-indent label-inline["+$i+"]")))),"/[$](.*)/",true);
	_wait(2000);
	var $shippingsummaryCost=_extract(_getText(_cell("shipping-value")),"/[$](.*)/",true);
	_assertEqual($ShippingMethodCost,$shippingsummaryCost);
	}		
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248971","Verify the functionality of 'EDIT' link in order summary Section in right nav on shipping page in Application as a Registered user.");
$t.start();
try
{
//navigating to shipping page
if(!isMobile() || mobile.iPad())
{
var $productName=_getText(_link("/(.*)/",_in(_div("/mini-cart-name/",_in(_div("checkout-mini-cart"))))));
//clicking on edit link present in the order Summuary section
_click(_link("EDIT",_in(_div("secondary"))));
}
else
{
var $productName=_getText(_link("/(.*)/",_in(_div("/mini-cart-name/",_in(_div("checkout-mini-cart"))))));
//clicking on edit link present in the order Summuary section
_click(_link("Edit",_in(_heading3("ORDER SUMMARY Edit"))));
}
//verify the navigation to Cart page

_assertVisible(_div("cart-actions cart-actions-top"));
_assertVisible(_table("cart-table"));
_assertVisible(_link("/(.*)/",_in(_div("name hone-header"))));
_assertEqual($productName,_getText(_link("/(.*)/",_in(_div("name hone-header")))));
//fetch the total items in cart
var $total=_count("_row","/cart-row/",_in(_table("cart-table")));

//product price in cart


for(var $i=0;$i<$total;$i++)
	{	
//updating the quantity
_setSelected(_select("/input-text/",_in(_cell("item-quantity "))),$sanitydata[1][2]);//$Shipping_Page
	}
//click on update cart
//_click(_submit("update-cart"));
_wait(5000,_isVisible(_link("mini-cart-link-checkout")));
//navigate to shipping page
_click(_link("mini-cart-link-checkout"));
//quantity in order summary section in shipping page
_assertEqual($sanitydata[1][2],_extract(_getText(_div("/mini-cart-pricing bonetext/",_in(_div("checkout-mini-cart")))),"/: (.*) [C$]/",true));


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248972","Verify the navigation of 'product name' in order summary Section in right nav on shipping page in Application as a Registered user.");
$t.start();
try
{
//clicking on product name in shipping page
if(!isMobile() || mobile.iPad())
	{
var $productName=_getText(_link("/(.*)/",_in(_div("/mini-cart/",_in(_div("secondary"))))));
_click(_link("/(.*)/",_in(_div("/mini-cart-name/",_in(_div("secondary"))))));
	}
else
	{
	var $productName=_getText(_link("/(.*)/",_in(_div("/mini-cart-name/",_in(_div("checkout-mini-cart"))))));
	_click(_link("/(.*)/",_in(_div("/mini-cart-name/",_in(_div("checkout-mini-cart"))))));
	}

_assertVisible(_div("pdpMain"));
_assertVisible(_div("/product-primary-image/"));
_assertEqual($productName,_getText(_heading1("/product-name/", _in(_div("product-col-2 product-detail")))));

//navigate to shipping page
_click(_link("mini-cart-link-checkout"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("248965","Verify the functionality of the Checkbox 'Add to Address Book' on shipping page in Application as a Registered user.");
$t.start();
try
{
	
//delete if any existing addresses are there
_click(_span("/Hello/",_in(_link("user-account"))));
_click(_link("Address Book"));
while(_isVisible(_link("Delete")))
	{
	_click(_link("Delete"));
	_wait(3000);
	}
//navigate to shipping page
_click(_link("/mini-cart-link-checkout/"));
//enter data in all optinal fields
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), "");
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), "");
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"),"");
_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),"");
_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), "");
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), "");
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), "");
_setValue(_telephonebox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),"");
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"),$shippingAddress[0][4]);
//click on continue button
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//verify the display
//_assert(_submit("dwfrm_singleshipping_shippingAddress_save").disabled);
//enter all required data except optional data
shippingAddress($shippingAddress,0);
//check add to address check box
_check(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook"));
//click on continue button
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));

//verify the navigation to billing page
_assertVisible(_div("/"+$sanitydata[2][2]+"/"));
_assertVisible(_submit("dwfrm_billing_save"));
//navigate to my account page
_assertVisible(_link("user-account"));
_click(_link("My Account"));
_click(_link("Addresses"));
//verify the presence of saved address
_assertVisible(_listItem("address-tile  default"));
_assertVisible(_div("/mini-address-location/"));
var $savedAddress=$shippingAddress[0][7]+" "+$shippingAddress[0][1]+" "+$shippingAddress[0][2]+" "+$shippingAddress[0][3]+" "+$shippingAddress[0][4]+" "+$shippingAddress[0][7]+" "+$shippingAddress[0][11]+" "+$shippingAddress[0][8]+" "+$shippingAddress[0][5]+" "+" "+$shippingAddress[0][9];
var $adress=$shippingAddress[0][7]+" "+$shippingAddress[0][1]+" "+$shippingAddress[0][2]+" "+_getText(_div("/mini-address-location/")).replace(",","");
_assertEqual($savedAddress,$adress);
//deleting the address
_click(_link("Delete"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//******************* Billing Page and Order Summary ************************

var $t = _testcase("249020/249031/249094/249018/249020","Verify the Scenario if user select 'Choose an Address' from the drop down on billing page in Application as a Registered user.");
$t.start();
try
{
	
	deleteAddress();
	_click(_link("section-header-note address-create buttonctaone button"));
	
	_click(_link("section-header-note address-create buttonctaone button"));
	addAddress($profaddr,0);
	
	_click(_submit("dwfrm_profile_address_create"));
	_wait(3000);
	
	_click(_link("section-header-note address-create buttonctaone button"));
	addAddress($profaddr,1);
	_click(_submit("dwfrm_profile_address_create"));
	_wait(3000);
	_click(_span("minicart-icon spriteimg"));
	_click(_link("mini-cart-link-checkout"));
	//enter shipping address
	shippingAddress($shippingAddress,1);
	_uncheck(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));

	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	_wait(3000);
	//uncheck the checkbox
	//check the functionality of the New address
	_setSelected(_select("dwfrm_billing_addressList"), "New address");
	_assertEqual($profaddr[2][2], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
	_assertEqual($profaddr[2][3], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
	_assertEqual($profaddr[2][4], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
	_assertEqual($profaddr[2][5], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_address2")));
	_assertEqual($profaddr[2][6], _getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
	_assertEqual($profaddr[2][7], _getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
	_assertEqual($profaddr[2][8], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
	_assertEqual($profaddr[2][9], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_postal")));
	_assertEqual($profaddr[2][10], _getValue(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone")));
	_assertEqual($uId, _getValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress")));
	
	//slect the address on the shippping page
	_setSelected(_select("dwfrm_billing_addressList"), "0");
	_assertEqual($profaddr[0][2], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
	_assertEqual($profaddr[0][3], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
	_assertEqual($profaddr[0][4], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
	_assertEqual($profaddr[0][5], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_address2")));
	_assertEqual($profaddr[0][6], _getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
	_assertEqual($profaddr[0][7], _getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
	_assertEqual($profaddr[0][8], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
	_assertEqual($profaddr[0][9], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_postal")));
	_assertEqual($profaddr[0][10], _getValue(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone")));
	_assertEqual($uId, _getValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress")));
	
	 //click on the edit link
	_click(_link("Shipping", _in(_div("/step-1 inactive/"))));
	//verify the navigation
    _assertEqual("/Shipping/", _getText(_div("/step-1 active/")));
    shippingAddress($shippingAddress,1);
    _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
    _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
    //verify the changes
    _assertEqual($shippingAddress[1][1], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
    _assertEqual($shippingAddress[1][2], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
    _assertEqual($shippingAddress[1][3], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
    _assertEqual($shippingAddress[1][4], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_address2")));
    _assertEqual($shippingAddress[1][7], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
    _assertEqual($shippingAddress[1][5], _getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
    _assertEqual($shippingAddress[1][6], _getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
    _assertEqual($shippingAddress[1][8], _getValue(_textbox("dwfrm_billing_billingAddress_addressFields_postal")));
    _assertEqual($shippingAddress[1][9], _getValue(_telephonebox("dwfrm_billing_billingAddress_addressFields_phone")));
    _assertEqual($uId, _getValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("249092/249015","Verify the functionality of 'EDIT' link in order summary Section in right nav on billing page in Application as a Anonymous user.");
$t.start();
try
{
	//verify the UI of the Billing page
	billingPageUI();
	BillingAddress($billingAddress,1);
	 PaymentDetails($cardDetails,0);
	_click(_submit("dwfrm_billing_save"));
	_wait(3000);
	//click on the edit details link
	_click(_link("My Bag", _in(_div("/step-0 your-bag/"))));
	//verify the navigation
	_assertVisible(_table("cart-table"));
	_assertVisible(_div("custom-cart-page"));
	_assertEqual($sanitydata[1][3], _getText(_div("cart-header-text")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("249092/249015","Verify the functionality of 'EDIT' link in order summary Section in right nav on billing page in Application as a Anonymous user.");
$t.start();
try
{
	_click(_link("mini-cart-link-checkout"));
	shippingAddress($shippingAddress,1);
	_assertEqual("/Shipping/", _getText(_div("/step-1 active/")));
	shippingAddress($shippingAddress,1);
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	
	//verify the UI of the Billing page
	 BillingAddress($billingAddress,1);
	 PaymentDetails($cardDetails,0);
	//_click(_submit("dwfrm_billing_save"));
	_wait(3000);
	//click on the edit details link
	_click(_link("/Back to Shipping/", _in(_div("checkout-back-to-bag"))));
	//verify the navigation
	_assertEqual("/Shipping/", _getText(_div("/step-1 active/")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("249087/249088/249089/249090/249091","Verify whether user is able to place order using VISA, MASTERCARD, AMERICAN EXPRESS, DISCOVER as credit card type  and Credit card alone on billing page in Application as a Registered user.");
$t.start();
try
{
		//delete existing address
		deleteAddress();
		          
		//click on add new address 
		_click(_link("Address Book"));
		_click(_link("section-header-note address-create buttonctaone button"));
		//Add address
		addAddress($profaddr,0);
		//click on create address
        _click(_submit("dwfrm_profile_address_create"));
		_wait(2000);
		//click on continue in address
		if (_isVisible(_div("/invalid-address/")))
		 {
		 _click(_link("Continue"));
		 }
		   
		 for(var $i=0;$i<$credit_card.length-1;$i++)
		 //for(var $i=0;$i<2;$i++)
		 {
		   //navigate to cart page
		search($sanitydata[1][0]);
		_click(_submit("add-to-cart"));
		 _wait(5000);
		//navigate to cart page
		_click(_link("button mini-cart-link-cart", _in(_div("mini-cart-totals"))));
		var $ProdPrice=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
		var $ProdPrice=round($ProdPrice,2);
		if(!isMobile())
      	{
            var $prodquantity=_extract(_getText(_div("quantity cart-no-image")),"/: (.*)/",true).toString().split(" ")[0];

      	}
		//navigate to checkout page
		 
		//click on the checkout
		_click(_link("/mini-cart-link-checkout/", _in(_div("mini-cart-content"))));
		
				//_click(_submit("dwfrm_login_unregistered"));
		  //comparing product name in shipping page
		  //var $pnameshippingpage=_getText(_link("/(.*)/", _in(_div("/mini-cart-name/")))).toString();
		  var $pnameshippingpage=_getText(_link("/Product/"));
		
		  shippingAddress($shippingAddress,0);
		  
		  _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
		  _click(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
		
		  var $priceinshippingpage=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
		  
		  _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
		  // adddressProceed();
		  _setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"), $uId);
		  var $billingprice=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
		  //comparing shipping and billing price
		  _assertEqual($priceinshippingpage,$billingprice);
		  var $shipppingCharge=parseFloat(_extract(_getText(_row("order-shipping bonetext  first ")),"/[$](.*)/",true));
		  var $saleTax=parseFloat(_extract(_getText(_row("order-sales-tax bonetext")),"/[$](.*)/",true));
		  var $ExpTotPrice=$ProdPrice+$shipppingCharge+$saleTax;
		  var $ExpTotPrice=round($ExpTotPrice,2);
		
		  //Payment details
     	  PaymentDetails($credit_card,$i);
//	      var $CardName= _getText(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"));
//          $OwnerName.push($CardName);
		                      
		  //Placing order
		  // _click(_checkbox("/dwfrm_billing_confirm/"));
		  _click(_submit("dwfrm_billing_save"));
		  
		  //Checking order is placed or not
		  //_assertVisible(_heading1($payment_data[1][3]));
		  _assertVisible(_submit("Place Order"));
		  _assertEqual($sanitydata[0][3], _getText(_div("/step-3 active/")));
		
		  _click(_submit("submit"));
		  
		     if (_isVisible(_div("dialog-container")))  
             {
               _click(_button("Close"));
             }
		  
		  _wait(3000);
		  if(!isMobile())
			  {
			  var $Qty=_getText(_div("/product-list-item/",_near(_div("Qty"))));
			  _assertEqual($prodquantity,$Qty);
			  }
		
		  
		  
		  var $priceinOCP=_extract(_getText(_div("line-item-price", _in(_div("order-shipment-table")))),"/[$](.*)/",true);
		 
		  var $ActordertotalOCP=_extract(_getText(_row("order-total", _in(_div("order-detail-summary")))),"/[$](.*)/",true);
		  //price verification
		  _assertEqual($ProdPrice,$priceinOCP);
		  //_assertEqual($billingprice,$ExpTotPrice);
		  _assertEqual($ExpTotPrice,$ActordertotalOCP);
		  
		  
		  var $orderno=_extract(_getText(_heading3("order-number")),"/[:] (.*)/",true);
		  _log($orderno);
		  
		  // var $orderkkkk=_getText(_span("order-number"))
		  // _log($orderkkkk);
		  
		  _focusWindow();
		  _takePageScreenShot();
		  
		  //payment in fo
		  _assertVisible(_div("order-payment-instruments"));
		  _assertContainsText($credit_card[$i][5], _div("order-payment-instruments"));
		  _wait(5000);
		  
	//	 _log($OwnerName);
       }
		 	 
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();

//var $t = _testcase("123456","check the saved card in the profile");
//$t.start();
//try
//{
//	
// _click(_span("/Hello/"));
// _click(_link("Payment methods"));
// var $Profile_CardName=_collectAttributes("_div","/cc-owner/","sahiText",_in(_list("payment-list")));
// _assertEqualArrays($OwnerName.sort(),$Profile_CardName.sort());
// 
//}
//catch($e)
//{
//       _logExceptionAsFailure($e);
//}      
//$t.end();

var $t = _testcase("249043/ 249022 / 249025","Verify the newly added card in profile get displayed in checkout Payment and Promotions page and vise versa in Application as a Registered user");
$t.start();
try
{
	ClearCartItems();
	_click(_link("user-account"));
	//click Payment methods
	_click(_link("Payment methods"));
	
	DeleteCreditCard();
	
	_click(_link("section-header-note buttonctaone add-card button"));
	
	//add new
	CreateCreditCard($cardDetails,1);
	//click on the ok
	_click(_submit("dwfrm_paymentinstruments_creditcards_create"));
	//_click(_submit("dwfrm_paymentinstruments_creditcards_create"));
	_wait(3000);
	
     search($sanitydata[1][0]);
	_click(_submit("add-to-cart"));
	 _wait(5000);
    _click(_link("mini-cart-link-checkout"));
    _wait(3000);
    
     _setSelected(_select("dwfrm_singleshipping_addressList"), "0");
    
    _check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
    _click(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID"));
    var $priceinshippingpage=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
    
    _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
    //adddressProceed();
    
    var $billingprice=_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true);
    //comparing shipping and billing price
    _assertEqual($priceinshippingpage,$billingprice);
    
    _setSelected(_select("dwfrm_billing_paymentMethods_creditCardList"), "1");
    //check the prepopulation
    _assertEqual($cardDetails[1][6]+" "+$cardDetails[1][7], _getValue(_textbox("dwfrm_billing_paymentMethods_creditCard_owner")));
    _assertEqual($cardDetails[1][2], _getSelectedText(_select("dwfrm_billing_paymentMethods_creditCard_type")));
    _assertEqual($cardDetails[1][17], _getValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/")));
    _assertEqual($cardDetails[1][4], _getSelectedText(_select("dwfrm_billing_paymentMethods_creditCard_expiration_month")));
    _assertEqual($cardDetails[1][5], _getSelectedText(_select("dwfrm_billing_paymentMethods_creditCard_expiration_year")));
    _setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"), $cardDetails[0][5]);

      var $Prod_Price=parseFloat(_extract(_getText(_row("order-subtotal", _in(_table("order-totals-table")))),"/[$](.*)/",true));
      var $ShippingTax=parseFloat(_extract(_getText(_row("order-shipping bonetext  first ", _in(_table("order-totals-table")))),"/[$](.*)/",true));
      var $saleTax=parseFloat(_extract(_getText(_row("order-sales-tax bonetext", _in(_table("order-totals-table")))),"/[$](.*)/",true));
    
      var $Exp_TotalPrice=parseFloat(_extract(_getText(_cell("order-value", _in(_table("order-totals-table")))),"/[$](.*)/",true));
	  var $ActualPrice=$Prod_Price+$ShippingTax+$saleTax;
      var $ActualPrice=round($ActualPrice,2);
	  _assertEqual($Exp_TotalPrice,$ActualPrice);
	  
      //Payment details
	  //PaymentDetails($cardDetails,0);
	                      
	  //Placing order
	  // _click(_checkbox("/dwfrm_billing_confirm/"));
	  _click(_submit("dwfrm_billing_save"));
	  
	  //Checking order is placed or not
	  //_assertVisible(_heading1($payment_data[1][3]));
	  _assertEqual($sanitydata[0][3], _getText(_div("/step-3 active/")));
	
	  var $pnameinocppage=_getText(_link("/(.*)/", _in(_div("product-list-item")))).toString();
	  _click(_submit("submit"));
	  _wait(3000);
	  var $Qty=_getText(_div("/product-list-item/",_near(_div("Qty"))));
	  _assertEqual($prodquantity,$Qty);
	  
	  
	  var $priceinOCP=_extract(_getText(_div("/product-list-item/",_near(_div("Price")))),"/[$](.*)/",true);
	 
	  var $ActordertotalOCP=_extract(_getText(_row("order-total", _in(_div("order-detail-summary")))),"/[$](.*)/",true);
	  //price verification
	  _assertEqual($ProdPrice,$priceinOCP);
	  _assertEqual($billingprice,$ExpTotPrice);
	  _assertEqual($Exp_TotalPrice,$ActordertotalOCP);
	  
	  
	  var $orderno=_extract(_getText(_heading3("order-number")),"/[:] (.*)/",true);
	  _log($orderno);
	  
	  _focusWindow();
	  _takePageScreenShot();
	  
	  //payment in fo
	  _assertVisible(_div("order-payment-instruments"));
	  _click(_link("user-account"));
		//click Payment methods
		_click(_link("Payment methods"));
	  DeleteCreditCard();
 }
 catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


/*var $t = _testcase("","");
$t.start();
try
{
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/