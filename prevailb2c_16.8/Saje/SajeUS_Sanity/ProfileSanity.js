_include("../util.GenericLibrary/GlobalFunctions.js");

_resource("SajeSanity.xls");

var $sanitydata= _readExcelFile("SajeSanity.xls","SanityData");
var $accountdata= _readExcelFile("SajeSanity.xls","OCPCreateAccount");

deleteProvidedUser($accountdata[0][3]);
deleteProvidedUser($accountdata[1][3]);

_wait(4000);

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();

var $t = _testcase("261236/","Verify the functionality related to 'Apply' button Create account page in application as an  Anonymous user.");
$t.start();
try
{

 //click on My Account link	
 _click(_span("My Account"));
 //click on create account link
 _click(_link("Create an Account"));
  
 _setValue(_textbox("dwfrm_profile_customer_firstname"), $accountdata[0][1]);
 _setValue(_textbox("dwfrm_profile_customer_lastname"),$accountdata[0][2]);
 _setValue(_textbox("dwfrm_profile_customer_email"),$accountdata[0][3]);
 _setValue(_password("/dwfrm_profile_login_password/"),$accountdata[0][4]);
 
//profile confirm	 	 
_click(_submit("dwfrm_profile_confirm"));
_wait(3000);

//verify whether account got created or not
var $exp=$accountdata[0][1]+" "+$accountdata[0][2];
_assertEqual($exp, _getText(_span("account-user-name sone-text")).toString());

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("261262","Verify the functionality related to 'Apply' button Edit Account page in application as an  Anonymous user.");
$t.start();
try
{

//click on personal data link
//_click(_heading2("Personal Data"));
	_click(_link("Personal Data"));
	
//create new account 	
_setValue(_textbox("dwfrm_profile_customer_firstname"), $accountdata[1][1]);
_setValue(_textbox("dwfrm_profile_customer_lastname"), $accountdata[1][2]);
_setValue(_textbox("dwfrm_profile_customer_email"), $accountdata[1][3]);
_setValue(_password("/dwfrm_profile_login_password/"), $accountdata[1][4]);

//profile confirm	 	 
_click(_submit("dwfrm_profile_confirm"));

//verify whether account got created or not
var $exp=$accountdata[1][1]+" "+$accountdata[1][2];
_assertEqual($exp, _getText(_span("account-user-name sone-text")).toString());

//log out
logout();
	
//click on My Account link	
_click(_span("My Account"));

//login with new account
_setValue(_textbox("/dwfrm_login_username/"),$accountdata[1][3]);
_setValue(_password("/dwfrm_login_password/"),$accountdata[1][4]);
_click(_submit("dwfrm_login_login"));
_wait(4000);

//click on my account
//_click(_heading3("My Account"));
//_click(_heading2("Personal Data"));
_click(_link("Personal Data"));
//Resetting back previous values
_setValue(_textbox("dwfrm_profile_customer_firstname"), $accountdata[0][1]);
_setValue(_textbox("dwfrm_profile_customer_lastname"),$accountdata[0][2]);
_setValue(_textbox("dwfrm_profile_customer_email"),$accountdata[0][3]);
_setValue(_password("/dwfrm_profile_login_password/"),$accountdata[0][4]);

//profile confirm	 	 
_click(_submit("dwfrm_profile_confirm"));

//log out


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("261327/ 261334 ","Verify the functionality related to 'Apply' button the Add a credit card overlay on my account payment methods page of the application as a Registered user.");
$t.start();
try
{
	_click(_link("Payment Settings"));
	//open the credit card overlay
	_click(_link("Add Credit Card"));
	//filling details in create card fields
	CreateCreditCard($sanitydata,9);
	//click on apply button
	_click(_submit("applyBtn"));
	//overlay got closed or not
	closedOverlayVerification();
	//verify whether credit card got saved or not and also UI
	_wait(3000);
	_assertVisible(_list("payment-list"));
	var $saveInfo=_getText(_listItem("/first/")).split(" ");
	var $actualExpdate=$saveInfo[4]+""+$saveInfo[5];
	_assertEqual($sanitydata[9][6],$saveInfo[0]);
	_assertEqual($sanitydata[9][7],$saveInfo[1]);
	var $expDate=$sanitydata[9][16];
	_assertEqual($expDate,$actualExpdate);
	_assertVisible(_submit("Delete Card"));

	logout();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("","");
$t.start();
try
{

//login to the application
login();
//


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase("","");
$t.start();
try
{
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/
