_include("../util.GenericLibrary/GlobalFunctions.js");

_resource("SajeSanity.xls");
_resource("../Checkout/BillingPage/MPCheckout.xls");

var $sanitydata= _readExcelFile("SajeSanity.xls","SanityData");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


_wait(3000);
//1. Load the application and login with valid credentials.
login();

var $t = _testcase("248905","Verify the UI of mini Cart on home page when multiple products are added to bag in the application both as an anonymous and a registered user.");
$t.start();
try
{
             
             //1. The Home page should be loaded with all the required contents. and user should be logged in
             //_assertVisible(_div("account-info-content"));
             var $Act_Price=0;
             for(var $i=0;$i<3;$i++)
                    {
                    
                           //NavigatetoPDP("701643407248");//008884303996//25419334
                         search($sanitydata[$i][0]);
                        _wait(2000);
                           _assertVisible(_div("pdpMain"));
                           _assertVisible(_div("product-detail-info"));
                           //4. Add the product to Cart. 
                           var $prodName=_getText(_heading1("/product-name/", _in(_div("product-col-2 product-detail"))));
                           _click(_submit("add-to-cart"));
                           //6. Verify the Mini Cart.
                           //a. Product should be listed in Mini cart, arrows should be removed, as all line items in the cart will always be expanded.
                           _assertVisible(_image("/(.*)/", _in(_div("mini-cart-content"))));
                           _assertVisible(_link("/"+$prodName+"/", _in(_div("mini-cart-content"))));
                           //5. Click on 'Cart' icon from header .
                           //_click(_span("minicart-icon spriteimg"));
                           //5. Mini cart should be displayed.
                          _assertVisible(_div("mini-cart-content"));
                           $Act_Price=$Act_Price+parseFloat(_extract(_getText(_div("mini-cart-pricing bonetext")),"/[$](.*)/",true));
                           $Act_Price=round($Act_Price,2);
                    }
             //varifying the total price
             $Act_Price=round( $Act_Price,2);
             var $Exp_Tot=_extract(_getText(_div("mini-cart-subtotals")),"/[$](.*)/",true);
             
             _assertEqual($Act_Price,$Exp_Tot);
             
             
             _assertEqual("3", _count("_image","/desktop-only/",_in(_div("mini-cart-products"))));


}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();
ClearCartItems();
var $t = _testcase("248916/248939/34","Verify whether user can remove the line item from cart in the application both as an anonymous and a registered user");
$t.start();
try
{
       search($sanitydata[0][0]);
       _click(_submit("add-to-cart"));
       //Navigating to the cart
       _click(_span("minicart-icon spriteimg"));
       _click(_link("button mini-cart-link-cart"));
//     var $ProdNmae=_getText(_div("name hone-header"));
//     
//     //_assertVisible(_link($ProdNmae));
//     //functionalty of editdetail
//     if(!isMobile())
//           {
//                  
//     _click(_link("/Edit/", _in(_div("item-quantity-details"))));
//           }
//     else
//           {
//           _click(_link("/EDIT/", _in(_div("/cart-edit-details/ "))));
//           }
//     //veriy the navigation
//       _assertVisible(_div("product-detail-info"));
//       _assertVisible(_div("pdpMain"));
//       _assertEqual($ProdNmae, _getText(_heading1("/product-name/", _in(_div("desktop-only")))));
         _click(_span("minicart-icon spriteimg"));
         
         _click(_link("button mini-cart-link-cart"));
       //functionalty of remove link
       _click(_submit("Remove"));
//     _assertNotVisible(_link($ProdNmae));
       //UI of the empty cart
       _assertVisible(_heading1($sanitydata[1][4]));

}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();
var $t = _testcase("248918","Verify user can update the quantity of each line item in the application both as an anonymous and a registered use");
$t.start();
try
{
       search($sanitydata[0][0]);
       _click(_submit("add-to-cart"));
       //Navigating to the cart
       var $ValidQty=4;
       _click(_span("minicart-icon spriteimg"));
       _click(_link("button mini-cart-link-cart"));

       _setSelected(_select("/js-cart-quantity-dropdown/"), $ValidQty);
       //Click on enter key from keyboard
       //Statements to hit on enter key
       _focusWindow();
       _focus(_select("/js-cart-quantity-dropdown/"));
       _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
       //Verifying Qty
       var $ACT_QTY=parseInt(_extract(_getSelectedText(_select("/js-cart-quantity-dropdown/")),"/[:] (.*)/",true));
       $ACT_QTY=$ACT_QTY-1;
       _assertEqual($ValidQty,$ACT_QTY); 
       //verifying by entering 0 Quantity
       var $prodID=_getText(_span("value", _in(_div("/sku/"))));
       //_setValue(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity", _in(_row("cart-row"))),0);
       _setValue(_select("/js-cart-quantity-dropdown/"),1);
       //Statements to hit on enter key
       _focusWindow();
       _focus(_select("/js-cart-quantity-dropdown/"));
       _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
       _wait(2000);
       //Product Should not present
       //_assertNotVisible(_span($prodID));    
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();

ClearCartItems();
var $t = _testcase("248920/248928/248903","Verify the display of the Line item total as a each line item in the application both as an anonymous and a registered user/Verify the navigation on click of 'Checkout' button from the cart page for registered use");
$t.start();
try
{
     ClearCartItems();
       search($sanitydata[0][0]);
    _click(_submit("add-to-cart"));
    _click(_span("minicart-icon spriteimg"));

    _click(_link("button mini-cart-link-cart"));
    var $Qty=3;
    var $prodPrice=_extract(_getText(_div("mini-cart-pricing bonetext")),"/[$](.*)/",true);
    
    for(var $i=0;$i<=$Qty;$i++)
           {
           
           var $j=$i+1;
           _setSelected(_select("/js-cart-quantity-dropdown/"),"["+$i+"]");
           _wait(4000);
           var $ExpPrice=$prodPrice*$j;
           $ExpPrice=round($ExpPrice,2);
           var $ActPrice=_extract(_getText(_cell("item-total")),"/[$](.*)/",true);
           _assertEqual( $ExpPrice,$ActPrice);
           _click(_span("minicart-icon spriteimg"));
          // var $qty=_extract(_getText(_div("mini-cart-pricing bonetext")),"/[: ](.*)[ $]/",true);
           var $qty=_extract(_getText(_div("mini-cart-pricing bonetext")),"/[: ](.*)[ C]/",true);
           _assertEqual($j,$qty);
           }
       _click(_submit($sanitydata[1][5], _in(_div("cart-actions"))));
    //verify the navigation
    _assertVisible(_div("js-checkout-shipping"));
    _assertVisible(_div("shipping-method-list"));


}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();
//logout to the app
logout();
var $t = _testcase("248928","Verify the navigation on click of 'Checkout' button from the cart page for anonymous user.");
$t.start();
try
{
       ClearCartItems();
       search($sanitydata[0][0]);
       _click(_submit("add-to-cart"));
       _click(_span("minicart-icon spriteimg"));

       //click on the view cart
       
       _click(_link("button mini-cart-link-cart"));
       //verify the navigation
       _assertVisible(_row("cart-row "));
       _assertEqual($sanitydata[0][5], _getText(_div("cart-header-text")));
       //click on the checkout
       _click(_submit($sanitydata[1][5], _in(_div("cart-actions"))));
    //varify the navigation
       _assertVisible(_div("login-box login-account"));
       _assertVisible(_div("checkoutlogin"));
}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();


var $t = _testcase("279590/279591"," Verify 'Email Signup label' in footer section of SAJE STORES Page.");
$t.start();
try
{
       //2. Click on Store Locator Page.
       _click(_span("/store-icon/", _in(_div("header-icon"))));
       
       _click(_link($sanitydata[3][5]));
       _assertEqual($sanitydata[4][5], _getText(_div("all-stores-heading")));

       //     . From listed store details go to 'SAJE STORES' Page.
       _assertVisible(_textbox("dwfrm_emailsubscribe_email"));
       _assertVisible(_div("store-email-subscription"));
       //     4. Verify 'Email Signup label'.
       _assertVisible(_label($sanitydata[2][5]));
       _assertEqual($sanitydata[2][4], _getText(_submit("/button-box/")));

}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();

var $t = _testcase("279586:"," Verify 'State/Province store list section' in All Stores List section.");
$t.start();
try
{
       var $Country=_getText(_div("all-stores-country", _in(_div("all-store-col1"))));
       var $store=_getText(_link("/(.*)/", _in(_div("stores-address2  "))));
       //click on the link
       _click(_link("/"+$store+"/", _in(_div("stores-address2  "))));
       //verify the navigation and validate the state name 
       _assertContainsText($Country, _div("store-address1"));
       


}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();
var $t = _testcase("281557:"," Verify the functionality of Job Description Anchor Link present careers list page as an anonymous and a registered user.");
$t.start();
try
{
       //click on the Career link 
       _click(_link($sanitydata[5][5]));
       //verify the navigation 
       _assertVisible(_div("saje-career-saje"));



}
catch($e)
{
       _logExceptionAsFailure($e);
}      
$t.end();
var $t = _testcase("243780"," Verify the functionality related to Suggested products appearing in search field both as an Anonymous and Registered user.");
$t.start();
try
{
		//Get into Search Field and enter 3 char.
	    _click(_span("search-icon"));
	    //2. Search suggestion should be displayed with the entered 3 characters.
	    _setValue(_textbox("q", _in(_div("global-header-search"))), "Pai");
		//3. Select the suggested products.
	    _assertVisible(_div("phrase-suggestions"));

		
		//3.On click of any product, go to the PDP for that product. 
	    _click(_link("Pain in Natural Healing", _in(_div("phrase-suggestions"))));
	    _assertVisible(_div("search-result-content"));

}
catch($e)
{
       _logExceptionAsFailure($e);
} 
var $t = _testcase("279692"," Verify the navigation related to the 404 Error page page in the application both as an anonymous and a registered user.");
$t.start();
try
{
    _navigateTo($sanitydata[2][8]);
    _assertVisible(_div("/_error/"));
		
  	_assertVisible(_heading1($sanitydata[4][2]));
  	_assertVisible(_paragraph($sanitydata[4][3]));
  	_assertVisible(_label($sanitydata[4][4]));
  	_assertVisible(_div("error-page-search"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();