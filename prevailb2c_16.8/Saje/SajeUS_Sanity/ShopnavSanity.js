_include("../util.GenericLibrary/GlobalFunctions.js");

_resource("SajeSanity.xls");

var $sanitydata= _readExcelFile("SajeSanity.xls","SanityData");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();

var $t = _testcase("243729/243751/243745","Verify the application behavior on click of ' My Profile'  link in the header as a anonymous user./Verify the  functionality  of  outside of the drop-down  In' MY PROFILE ' Drop down section.(Unauthenticated User).");
$t.start();
try
{

//click on my profile link	
_click(_span("My Account"));
//drop down should display
_assertVisible(_div("user-panel account"));
_assertVisible(_heading2("Sign In"));
_assertVisible(_textbox("/dwfrm_login_username/"));
_assertVisible(_password("/dwfrm_login_password/"));
_assertVisible(_checkbox("dwfrm_login_rememberme"));
_assertVisible(_label("Remember Me"));
//_assertVisible(_link("Forgot Password?"));
_assertVisible(_link("Reset Password"));

_assertVisible(_submit("/dwfrm_login/"));
_assertVisible(_div("create-account"));
_assertVisible(_link("Create an Account"));

//click on outside of the dropdown
_click(_nav("navigation"));
//drop down should not display
_assertNotVisible(_div("user-panel account"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243748","Verify the functionality of ' Sign In' button present in ' MY PROFILE ' Drop down section.");
$t.start();
try
{
	
//login to the application
login();
_assertVisible(_span("/Hello/",_in(_link("user-account"))));

//click on my account
_click(_heading3("My Account"));
_assertVisible(_div("/My Account "+ $FName +" "+$LName +" "+"Logout/i"));

_click(_link("Personal Data"));

//should logged in successfully
//_assertVisible(_heading1("My Account Saje Pfs1 Logout"));
//_assertVisible(_heading1("/My Account "+ $FName +" "+$LName +" "+"Logout/i"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//clear cart items
ClearCartItems();

var $t = _testcase("243725/243739","Verify the  functionality of country selector dropdown/Verify the functionality of  'No, I stay on this page' link in modal window.");
$t.start();
try
{

//click on country selector dropdown to US
CountrySelection($countrysel[0][0]);
_wait(4000);
//click on sub cat
_click(_link($sanitydata[2][3]));
//Change country to Canada
CountrySelection($countrysel[1][0]);
_wait(4000);
//clear cart items
ClearCartItems();
navigateToHomePage();
//Application is navigating to home page
_assertVisible(_div("header-banner"));
_assertVisible(_div("homepage-banner"));
_assertVisible(_div("homepage-sideimages"));
//Add item to cart
ClearCartItems();
addItemToCart($sanitydata[0][9],1);
//after adding product the cart quantity should be one
_assertEqual("1", _getText(_span("minicart-quantity")));
//change country to US
_click(_div("current-country"));
_click(_link($countrysel[0][0]));
_assertVisible(_link("popup-nothanks-link",_in(_div("dialog-container"))));
_click(_link("popup-nothanks-link",_in(_div("dialog-container"))));
//Canada country should get selected
_assertVisible(_div($countrysel[1][0]));
_assertEqual("1", _getText(_span("minicart-quantity")));

//change country to US
_click(_div("current-country"));
_click(_link($countrysel[0][0]));
_click(_link("buttonctaone", _in(_div("dialog-container"))));
//us country should get selected
_assertVisible(_div($countrysel[0][0]));
_assertEqual("0", _getText(_span("minicart-quantity")));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243752/243754/243755/243756/243757/243758/243735","Verify the  Navigation  of 'MY ACCOUNT ' link of  My Profile navigation menu Options.");
$t.start();
try
{
	
//click on hello message
_click(_span("/Hello/", _in(_link("user-account"))));

var $MyaccountDropdown=_collectAttributes("_link","/(.*)/","sahiText",_in(_div("user-links")));	

for (var $i=0;$i<$MyaccountDropdown.length;$i++)
{
	_assertVisible(_link($sanitydata[$i][7], _in(_div("/user-panel/"))));
}

for (var $i=0;$i<$MyaccountDropdown.length;$i++)
	{
	_click(_link($MyaccountDropdown[$i]));
	
	if ($i==0)
		{
		//_assertVisible(_heading1("My Account "+$FName+" "+$LName+" Logout"));
		_assertVisible(_div("My Account "+$FName+" "+$LName+" Logout"));

		_assertVisible(_list("account-options"));
		}
	else if ($i==1)
		{
		_assertVisible(_heading1("Addresses"));
		_assertVisible(_link("Create New Address"));
		}
	else if ($i==2)
		{
		_assertVisible(_heading1("Credit Card Information"));
		_assertVisible(_link("Add Credit Card"));
		}
	else if ($i==3)
		{
		_assertVisible(_heading1("Order History"));
		_assertVisible(_div("order-history-page"));
		}
	else
		{
		_assertVisible(_link("SIGN UP"));
		_assertVisible(_link("My Account"));
		//Should Navigate to home page
		_assertVisible(_div("homepage-banner"));
		}
	
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//click on logout link
logout();

var $t = _testcase("243747","Verify the Navigation of  'FORGOT PASSWORD'  link  of MY PROFILE ' Drop down section.");
$t.start();
try
{
	
//click on my profile link	
_click(_span("My Account"));
//click on reset password field
_click(_link("password-reset"));
//should navigate to forget password page
_assertVisible(_div("dialog-container"));
_assertVisible(_heading1($sanitydata[0][4]));
//click on close button
_assertVisible(_button("Close"));
_click(_button("Close"));
//overlay shouldnot display
_assertNotVisible(_div("dialog-container"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243722","Verify the system response on click of  'Store locator'  icon at global header  in the application both as an anonymous and a registered user");
$t.start();
try
{
	
//navigate to store locator
_click(_link("Locate Stores"));
//store locator heading
_assertVisible(_heading1("Find a Store"));
_assertVisible(_select("dwfrm_storelocator_searchby"));
_assertVisible(_textbox("dwfrm_storelocator_searchkey"));
_assertVisible(_submit("dwfrm_storelocator_searchbyoption"));
_assertVisible(_link("All Locations"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246115/246116","Verify the functionality of attribute 'Select size' on product detail page in the application both as an anonymous and a registered user.");
$t.start();
try
{
	
//navigate to PDP where size swatch should be present
NavigatetoPDP($sanitydata[0][8]);

//select size
if (_isVisible(_div("size-text  ")))
{
	
	if (!_isVisible(_listItem("selectable selected",_in(_div("product-variations")))))
		{
		//select any swatch
		_click(_link("swatchanchor",_in(_div("product-variations"))));
		}
}

//dollar symbool should display for price
//_assertVisible(_span("/"+$currencysymbool+"(.*)/", _in(_div("product-price ptwotext"))));
_assertVisible(_span("/"+$currencysymbool+"(.*)/", _in(_div("/product-price /"))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246117","Verify the functionality of 'quantity dropdown' on product detail page for in the application both as an anonymous and a registered user.");
$t.start();
try
{
	
//checking the display of quantity drop down
_assertVisible(_div("quantity"));
//click on the quantity drop down
_click(_select("productquantityupdate"));
//quantity selections
var $qty=_getText(_select("productquantityupdate"));
//
for(var $i=0;$i<=$qty.length-1;$i++)
	{
	
	if ($i==0)
		{
		//set the quantity
		_setSelected(_select("productquantityupdate", _in(_div("quantity"))),$i);
		_assertEqual(_extract($qty[$i],"/: (.*)/",true),_extract(_getSelectedText(_select("productquantityupdate", _in(_div("quantity")))),"/: (.*)/",true));
		}
	else
		{
		//set the quantity
		_setSelected(_select("productquantityupdate", _in(_div("quantity"))),$i);
		_assertEqual($qty[$i],_extract(_getSelectedText(_select("productquantityupdate", _in(_div("quantity")))),"/: (.*)/",true));
		}
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246120/246122/246124/243779/243780","Verify the functionality of product names on Product detail page for few products the application both as an anonymous and a registered user./Verify the Product Name in PDP for both as an anonymous and a registered user.");
$t.start();
try
{

for(var $i=0;$i<4;$i++)
{	
//search the product	
_click(_span("/search-icon spriteimg/"));
_setValue(_textbox("q", _in(_div("global-header-search"))),$sanitydata[0][0]);
_wait(3000);

//Select the suggested products.
var $prodNameinSearchSugg=_getText(_div("/product-name/", _in(_div("product-suggestion"))));

if ($i==0)
{
_click(_link("product-link",_in(_div("product-suggestion"))));
}
else if ($i==1)
{
_click(_image("/(.*)/", _in(_div("product-suggestion"))));
}
else if ($i==2)
{
_click(_div("product-nameExtension",_in(_div("product-suggestion"))));
}
else 
	{
	_click(_link("/(.*)/", _in(_click(_div("product-price", _in(_div("product-suggestion")))))));
	}

//navigate to pdp
var $prodNameinPDP=_getText(_heading1("/product-name/", _in(_div("product-col-2 product-detail"))));
_assertEqual($prodNameinSearchSugg,$prodNameinPDP);
_assertVisible(_div("pdpMain"));
_assertVisible(_div("/product-primary-image/"));

}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246127/246146/","Verify the UI of Product Icons in PDP both as an anonymous and a registered user./Verify the Functionality of Product name/image/price in Recently viewed section");
$t.start();
try
{
	
//product name in PDP above
var $prodNameinPDP=_getText(_heading1("/product-name/", _in(_div("product-col-2 product-detail"))));

var $prodPrice=_extract(_getText(_span("/price-sales/",_in(_div("/product-price/")))),"/"+[$currencysymbool]+"(.*)/",true);
//var $prodPrice=_getText(_span("price-sales ", _in(_div("/product-price/")))),"/"+[$currencysymbool]+"(.*)/",true);
//product name in last visited page
var $productNameinbottom=_getText(_link("name-link", _in(_div("last-visited-contnet"))));
var $productpriceinbottom=_extract(_getText(_span("product-sales-price",_near(_div("last-visited-contnet")))),"/"+[$currencysymbool]+"(.*)/",true);

//asserting product name and price
_assertEqual($prodNameinPDP,$productNameinbottom);
_assertEqual($prodPrice,$productpriceinbottom);

//fetching product name and price in PDP
var $prodname=new Array();
var $ProdPrice=new Array();
	
//fetching product name and price in recently viewed
var $NameinRecentlyViewed=new Array();
var $PriceinRecentlyViewed=new Array();

//navigate with 3 products to PDP
for(var $i=0;$i<3;$i++)
	{
	
	//navigate to PDP where size swatch should be present
	NavigatetoPDP($sanitydata[$i][6]);
	
	//fetching all the attributes in PDP
	var $NameInPDP=_getText(_heading1("/product-name/", _in(_div("product-col-2 product-detail"))));
	var $PriceinPDP=_extract(_getText(_span("price-sales ",_in(_div("/product-price/")))),"/"+[$currencysymbool]+"(.*)/",true);
	//fetching product name and price in PD page
	$prodname.push($NameInPDP);
	$ProdPrice.push($PriceinPDP);
	
	}
	

var $productsInRecentlyViewed=_count("_link","thumb-link",_in(_div("last-visited-contnet")));

//navigate with 3 products to PDP
for(var $i=0;$i<$productsInRecentlyViewed;$i++)
	{
	var $NameInRecentlyViewed=_getText(_link("name-link", _in(_div("product-name["+$i+"]"))));
	var $priceInRecentlyViewed=_extract(_getText(_link("/(.*)/", _in(_span("product-sales-price["+$i+"]")))),"/"+[$currencysymbool]+"(.*)/",true);
	
	$NameinRecentlyViewed.push($NameInRecentlyViewed);
	$PriceinRecentlyViewed.push($priceInRecentlyViewed);
	
	}

//product name and price should match in PDP and recently viewed section
_assertEqualArrays($prodname.sort(),$NameinRecentlyViewed.sort());
_assertEqualArrays($ProdPrice.sort(),$PriceinRecentlyViewed.sort());

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("246145/246132","Verify the Functionality of Product name/image/price in You may also enjoy section/Verify the UI of you may also enjoy this section.");
$t.start();
try
{

//you may also enjoy section
//_assertVisible(_div($sanitydata[4][1]));
_assertVisible(_div("carousel-recommendations"));
_assertVisible(_div("recommendations cross-sell"));

var $productsinYouAlsoEnjoy=_count("_div","/product-tile/",_in(_div("carousel-recommendations")));
	
for (var $i=0;$i<$productsinYouAlsoEnjoy;$i++)
{
//product name in recommendations section
_assertVisible(_link("/(.*)/", _in(_div("product-name ptwotext["+$i+"]"))));
//product price in recommendations section
_assertVisible(_span("/price-sales/", _near(_div("product-name ptwotext["+$i+"]"))));
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243862/243864/243859/243863","Verify the functionality  of Search Refinement Selections for  Anonymous and  Registered  user./Verify the functionality related to  'Price' refinement filter on Category grid page in application both as an  Anonymous and  Registered  user./Verify the functionality related to  'Type' refinement filter on search result page in application both as an  Anonymous and  Registered  user." +
"Verify the functionality related to  'Price' refinement filter on Category grid page. in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{

//search the product	
search($sanitydata[4][0]);

var $pricerefinements=_collectAttributes("_link","refinement-link","sahiText",_in(_div("refinement price ")));

//navigate to search result page
for (var $i=0;$i<$pricerefinements.length;$i++)
{
_click(_link($pricerefinements[$i],_in(_div("refinement price "))));
//click on total count
_click(_div("total-count"));
//price in bread crumb
_assertEqual($pricerefinements[$i],_getText(_span("/(.*)/", _in(_span("breadcrumb-refinement")))));
//clear link should be visible
_assertVisible(_link("/Clear all/", _near(_span("breadcrumb-refinement"))));
//checking price
priceCheck($pricerefinements[$i]);
//un checking the refinement
_click(_link($pricerefinements[$i],_in(_div("refinement price "))));
//clear link should Not be visible
_assertNotVisible(_link("/Clear all/", _near(_span("breadcrumb-refinement"))));
}


}
catch($e)
{
_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243887/243919","Verify the display of 'View all' button in Category grid page/Verify the  ' Product count ' of Search Results Products in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{

//search the product	
search($sanitydata[4][0]);
//should display product count
//_assertVisible(_heading1("results-products"));
_assertVisible(_div("sone resulttext"));

//total no of products
//var $totalproduct=_extract(_getText(_heading1("results-products")),"/ [(](.*)[)]/",true);
var $totalproduct=_extract(_getText(_span("ptwotext")),"/ [(](.*)[)]/",true);
//click on view all link
_click(_link("/viewallitems/"));
//total no of products in page
var $totalproductsinpage=_count("_div","product-tile",_in(_list("search-result-items")));
_assertEqual($totalproduct,$totalproductsinpage);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243902/243903/243904/243932","Verify the navigation to search no result page from home page in application both as an Anonymous and Registered user./Verify the functionality related to Try new search 'Search' icon for valid keyword on search no result page in application both as an Anonymous and Registered user.");
$t.start();
try
{
	
//search the product	
search($sanitydata[0][10]);

_assertVisible(_div("search-notfound"));
_assertEqual($sanitydata[1][10], _getText(_div("search-data")));
_assertVisible(_div("no-hits-content-results"));
//3. Enter invalid keyword in the try new search text field . Click on 'Search' icon and verify.
_setValue(_textbox("input-text search-text-field valid"),$sanitydata[2][10]);
_click(_submit("simplesearch"));
//navigation to search results
_assertVisible(_div("search-notfound"));
_assertVisible(_div("no-hits-content-results"));


//Enter valid keyword in the try new search text field . Click on 'Search' icon and verify.
//search the product	
search($sanitydata[4][0]);
//verifying the navigation on search results page
_assertVisible(_list("search-result-items"));
_assertEqual("\" "+$sanitydata[4][0]+" \"", _getText(_div("sone resulttext")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("243923","Verify the functionality related to  'Pagination arrows' on search result page in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	
//search the product	
search($sanitydata[2][3]);
//pagination
pagination();

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase("","");
$t.start();
try
{
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/