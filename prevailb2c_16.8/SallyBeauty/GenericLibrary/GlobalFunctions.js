_include("BrowserSpecific.js");
_include("BM_Functions.js");
_resource("User_Credentials.xls");
_include("../ObjectRepository/Generic_OR.sah");

var $URL=_readExcelFile("User_Credentials.xls","URL");
var $userLog=_readExcelFile("User_Credentials.xls","BM_Login");

var $UserID=$userLog[0][0];
var $Password=$userLog[0][1];
var $expFinalDiscount;
var $ActualDiscountPrice;
var $RootCatName=$Generic_Data[0][2];
var $CatName=$Generic_Data[0][5];
var $SubCatName=$Generic_Data[0][3];
var $proName;
var $price;


//Function take a screen shot when a their is a script failure

function onScriptError()
{
  _log("Error log"); 
  _lockWindow(5000);
  _focusWindow();
  _takePageScreenShot();
  _unlockWindow();
}

function onScriptFailure()
{
  _log("Failure log");
  _lockWindow(5000);
  _focusWindow();
  _takePageScreenShot();
  _unlockWindow();
} 

function SiteURLs()
{
	
	//navigate to sally site
	_navigateTo($URL[0][0]);
	//store front authentication 
	if(_isVisible(_submit("Authenticate")))
		{
			_setValue(_textbox("authUser"), $userLog[1][0]);
			_setValue(_password("authPassword"), $userLog[1][1]);
			_click(_submit("Authenticate"));
		}
	//closing popup
	if (_isVisible(_div("emailsignupform")))
		{
		_click(_link("Close"));
		}
	
}

function cleanup()
 {
	//Clearing the items from cart
	ClearCartItems();
	if(isMobile())
		{
			_click(_span("Menu"));
		}
	//logout from the application
	Logout();
	//sally beauty link
	_click(_image("Sally Beauty"));
}

//Checkout related functions
function round(value,exp) {
    if (typeof exp === 'undefined' || +exp === 0)
      return Math.round(value);

    value = +value;
    exp  = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
      return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
  }

//############### BM related Function ######################

function deleteUser()
{
	 data();
	BM_Login();
	_click($BM_CUSTOMER_LINK);
	_click($BM_CUSTOMER_LINK2);
	_click($BM_ADVANCE_LINK);
	_click($BM_ADVANCE_LINK);
	_setValue($BM_EMAIL_TEXTBOX,$email);
	_click($BM_FIND_BUTTON);
	if(_isVisible($BM_SELECTALL_LINK))
	{
		if(_assertEqual($email,_getText($BM_REGISTERED_EMAILID)))
			{
			_click($BM_DELETECUSTOMER_CHECKBOX);
			_click($BM_DELETECUSTOMER_BUTTON);
			_click($BM_CONFIRMOK_BUTTON);
			_log("Successfully deleted the user");
			}
	}
	else
	{
		_log("search did not match any users.");
	}
	_click($BM_LOGOUT_LINK);
}

function deleteProfileUser($Providedemail)
{
	 data();
	BM_Login();
	_click($BM_CUSTOMER_LINK);
	_click($BM_CUSTOMER_LINK2);
	_click($BM_ADVANCE_LINK);
	_click($BM_ADVANCE_LINK);
	_setValue($BM_EMAIL_TEXTBOX,$Providedemail);
	_click($BM_FIND_BUTTON);
	if(_isVisible($BM_SELECTALL_LINK))
	{
		if(_assertEqual($Providedemail,_getText($BM_REGISTERED_EMAILID)))
			{
			_click($BM_DELETECUSTOMER_CHECKBOX);
			_click($BM_DELETECUSTOMER_BUTTON);
			_click($BM_CONFIRMOK_BUTTON);
			_log("Successfully deleted the user");
			}
	}
	else
	{
		_log("search did not match any users.");
	}
	_click($BM_LOGOUT_LINK);
}

//########################### Profile related functions ###################
function Login()
{
	_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$uId);
	_setValue($LOGIN_PASSWORD_TEXTBOX,$pwd);
    //click on sign in button
	_click($LOGIN_BUTTON);

}

function Logout()
{
	if(_isVisible($LOGIN_BREADCRUMB_MYACCOUNT))
	{
		//click on my account
	     _click($LOGIN_BREADCRUMB_MYACCOUNT);
	     //click on sign out link
	     _click($GLOBALHEADER_SIGNOUT_LINK);
	     //login scetion page
	     _assertVisible($LOGIN_HEADING);

	}
}

function clearWishList()
{
	_click($LOGIN_BREADCRUMB_MYACCOUNT);
	 _click($WISHLIST_LINK);
	 if (_isVisible($WISHLIST_EDITADDRESS_DROPDOWN))
	 {
	 _setSelected($WISHLIST_EDITADDRESS_DROPDOWN,0);	 
	 }
	 while(_isVisible($WISHLIST_REMOVE_BUTTON))
	 {
	  	  _click($WISHLIST_REMOVE_BUTTON);
	 } 
	
	
}

function DeleteAddress()
{
	clearWishList();
    //click on my account
	_click($LOGIN_BREADCRUMB_MYACCOUNT);
	//click on address
	_click($ADDRESS_ADDRESSES_LINK);
	//deleting the existing addresses
	   while(_isVisible($ADDRESSES_DELETE_LINK))
	   	{
		   _call($ADDRESSES_DELETE_LINK).click();
	   	  _expectConfirm("/Do you want/",true);
	   	}


}

function AddAddress($validations,$r)
{
	
	_setValue($ADDRESSES_ADDRESSNAME_TEXTBOX, $validations[$r][1]);
	_setValue($ADDRESSES_FIRSTNAME_TEXTBOX, $validations[$r][2]);
	_setValue($ADDRESSES_LASTNAME_TEXTBOX, $validations[$r][3]);
	_setValue($ADDRESSES_ADDRESS1_TEXTBOX, $validations[$r][4]);
	_setValue($ADDRESSES_ADDRESS2_TEXTBOX, $validations[$r][5]);
	_setSelected($ADDRESSES_COUNTRY_DROPDOWN,$validations[$r][6]);
	_setSelected($ADDRESSES_STATE_DROPDOWN, $validations[$r][7]);
	_setValue($ADDRESSES_CITY_TEXTBOX, $validations[$r][8]);
	_setValue($ADDRESSES_ZIPCODE_TEXTBOX, $validations[$r][9]);
	_setValue($ADDRESSES_PHONE_TEXTBOX,$validations[$r][10]);
	_click($ADDRESSES_OVERLAY_APPLYBUTTON);
	
}


function DeleteCreditCard()
{               

	//click on my account
	_click($MY_ACCOUNT_LINK);
	//click on payment settings
	_click($MY_ACCOUNT_PAYMENT_LINK);
	
   //click on delete card link
   while(_isVisible($PAYMENT_DELETECARD_LINK))
       {
	       _click($PAYMENT_DELETECARD_LINK);
	       //click on ok in confirmation overlay
	   		_expectConfirm("/Do you want/",true);
       }
	//verify whether deleted or not
	_assertNotVisible($PAYMENT_CARDLIST);
	_assertNotVisible($LEFTNAV_PAYMENTSETTINGS_LINK);
	
}

function AddCreditCard($sheet,$i)
{

	_setValue($PAYMENT_NAMETEXTBOX,$sheet[$i][1]);
	_setSelected($PAYMENT_CARDTYPE_DROPDOWN,$sheet[$i][2]);
	_setValue($PAYMENT_NUMBER_TEXTBOX,$sheet[$i][3]);
	_setSelected($PAYMENT_MONTH_DROPDOWN,$sheet[$i][4]);
	_setSelected($PAYMENT_YEAR_DROPDOWN,$sheet[$i][5]);
}

function navigateToPDP($category,$subcategory,$Qty)
{
	//mouseover on categoty
	_mouseOver(_link($category));
	//click on root category
	_click(_link($subcategory));
	//click on any product name
	_click($PLP_NAMELINK);
    //PDP page
	_assertVisible($PDP_PDPMAIN_SECTION);
    $proName=_getText($PDP_PRODUCTNAME) ;
    $price=_extract(_getText($PDP_PRICE),"/[$](.*)/",true).toString();
    _setValue($PDP_QUANTITY_TEXTBOX, $Qty);
    $QTY=_getValue($PDP_QUANTITY_TEXTBOX);
}

function addToWishlist($category,$subcategory,$Qty)
{
	navigateToPDP($category,$subcategory,$Qty);
	//wishlist button color
    //click on wish list
    _click($PDP_ADDTOWISHLIST_BUTTON);
    //return product Name
    return $proName;
}

function createAccount()
{
	 _setValue($EDITACCOUNT_FIRSTNAME_TEXTBOX,$userData[$user][1]);
	 _setValue($EDITACCOUNT_LASTNAME_TEXTBOX,$userData[$user][2]);
	 _setValue($ADDRESSES_ADDRESS1_TEXTBOX,$userData[$user][3]);
	 _setValue($ADDRESSES_ADDRESS2_TEXTBOX,$userData[$user][4]);
	 _setSelected($ADDRESSES_COUNTRY_DROPDOWN,$userData[$user][5]);
	 _setSelected($ADDRESSES_STATE_DROPDOWN,$userData[$user][6]);
	 _setValue($ADDRESSES_CITY_TEXTBOX,$userData[$user][7]);
	 _setValue($ADDRESSES_ZIPCODE_TEXTBOX,$userData[$user][8]);
	 _setValue($ADDRESSES_PHONE_TEXTBOX,$userData[$user][13]);
	 _setValue($EDITACCOUNT_EMAIL_TEXTBOX, $userData[$user][9]);
	 _setValue($EDITACCOUNT_CONFIRM_EMAIL_TEXTBOX, $userData[$user][10]);
	 _setValue($REGISTER_CREATEACCOUNT_PASSWORD_TEXTBOX,$userData[$user][11]);
	 _setValue($REGISTER_CREATEACCOUNT_CONFIRMPASSWORD_TEXTBOX, $userData[$user][12]);
	 
	 if (_isVisible($CREATEACCOUNT_SUBMIT_BUTTON))
		 {
		 _click($CREATEACCOUNT_SUBMIT_BUTTON);
		 }
	 
	 if (_isVisible($CREATEACCOUNT_WRONG_EMAIL_DIALOG))
		 {
		 _click($CREATEACCOUNT_ADDRESS_ENTRED_BUTTON);
		 }
	 
}

function createAccountProfile($sheet,$i)
{
	 _setValue($EDITACCOUNT_FIRSTNAME_TEXTBOX, $sheet[$i][1]);
	 _setValue($EDITACCOUNT_LASTNAME_TEXTBOX, $sheet[$i][2]);
	 _setValue($ADDRESSES_ADDRESS1_TEXTBOX, $sheet[$i][3]);
	 _setValue($ADDRESSES_ADDRESS2_TEXTBOX, $sheet[$i][4]);
	 _setSelected($ADDRESSES_COUNTRY_DROPDOWN, $sheet[$i][5]);
	 _setSelected($ADDRESSES_STATE_DROPDOWN, $sheet[$i][6]);
	 _setValue($ADDRESSES_CITY_TEXTBOX, $sheet[$i][7]);
	 _setValue($ADDRESSES_ZIPCODE_TEXTBOX, $sheet[$i][8]);
	 _setValue($EDITACCOUNT_EMAIL_TEXTBOX, $sheet[$i][9]);
	 _setValue($EDITACCOUNT_CONFIRM_EMAIL_TEXTBOX, $sheet[$i][10]);
	 _setValue($REGISTER_CREATEACCOUNT_PASSWORD_TEXTBOX,$sheet[$i][11]);
	 _setValue($REGISTER_CREATEACCOUNT_CONFIRMPASSWORD_TEXTBOX, $sheet[$i][12]);
	 _setValue($ADDRESSES_PHONE_TEXTBOX,$sheet[$i][13]);
	 _click($CREATEACCOUNT_SUBMIT_BUTTON);
}


function EditAccount($sheet,$i)
{
	_setValue($EDITACCOUNT_FIRSTNAME_TEXTBOX,$Editaccount_Data[$i][1]);
	_setValue($EDITACCOUNT_LASTNAME_TEXTBOX,$Editaccount_Data[$i][2]);
	_setValue($EDITACCOUNT_EMAIL_TEXTBOX,$Editaccount_Data[$i][3]);
	_setValue($EDITACCOUNT_CONFIRM_EMAIL_TEXTBOX,$Editaccount_Data[$i][4]);	
   _click($CREATEACCOUNT_SUBMIT_BUTTON);
   if(_isVisible($CREATEACCOUNT_VERIFYEMAILADDRESS_DIALOG))
	{
	_click($CREATEACCOUNT_USETHISADDRESS_BUTTON);
	}
}

//########################## ShopNav Functions #################################

function navigateSubCatPage()
{
_click(_link($RootCatName));
_log("We are now on " + $RootCatName + " landing page");
_wait(2000);
_click(_link($CatName));
_log("We are now on " + $CatName + " landing page");
_wait(2000);
_click(_link($SubCatName));
_log("We are now on " + $SubCatName + " landing page");
_wait(2000);
}

//search function
function search($product)
{
	if(isMobile())
	{
			_navigateTo(document.getElementsByName("simpleSearch")[0].getAttribute("action") + "?q=" + $product);
	}
	else	
	{
			_setValue(_textbox("q"), $product);	
			_click(_submit("rightside"));
	}
	
}

function pagination()
{  
	var $NoOfProds=parseInt(_extract(_getText(_div("pagination")),"/of (.*)/",true).toString().split(" ")[0]);
	if($NoOfProds<="12")
		{
			_assert(true,"Single Page is displayed");		
		}
	else
		{
			var $temp=parseInt($NoOfProds/12);
			_log($temp);
			var $rem=parseInt($NoOfProds%12);
			_log("rem"+$rem);
			if($rem==0)
				{
					var $Pages=$temp;
				}
			else
				{
					var $Pages=$temp+1;
				}
			if(_isVisible(_span("cmpr_pagination_next")))
				{					
					for(var $j=1;$j<=$Pages;$j++)
						{
							if($j==1)
								{
									var $currentPage=_getText(_span("/cmpr_pagination_current /"));
						  	    	_assertEqual($j,$currentPage);
								}
							else
								{
									_click(_span("cmpr_pagination_next"));
									_wait(4000);
									var $currentPage=_getText(_span("/cmpr_pagination_current /"));
						  	    	_assertEqual($j,$currentPage);
								}				
						}
					for(var $j=$Pages;$j>=1;$j--)
						{
							if($j==$Pages)
								{
									var $currentPage=_getText(_span("/cmpr_pagination_current /"));
						  	    	_assertEqual($j,$currentPage);
								}
							else
								{
									_click(_span("cmpr_pagination_previous"));
									_wait(5000);
									var $currentPage=_getText(_span("/cmpr_pagination_current /"));
						  	    	_assertEqual($j,$currentPage);	
								}				
						}
					for(var $j=1;$j<=$Pages;$j++)
						{
							if($j==1)
								{
									var $currentPage=_getText(_span("/cmpr_pagination_current /"));
						  	    	_assertEqual($j,$currentPage);
								}
							else
								{
									_click(_span($j, _in(_div("cmpr_pagination_wrapper"))));
									_wait(5000);
									var $currentPage=_getText(_span("/cmpr_pagination_current /"));
						  	    	_assertEqual($j,$currentPage);						
								}				
						}
				 }
		}
}

//######################## cart functions #######################

function addItemToCart($subCat,$i)
{
	search($subCat);
	//_click(_link($subCat));
	_wait(2000);
	//navigate to PDP
	if (_isVisible($PLP_SEARCH_RESULT_ITEMS))
		{
		_click($PLP_NAMELINK);
		}
	$PName=_getText($PDP_PRODUCTNAME);
	$ProdSKU=_getText($PDP_SKU_NUMBER);
	if(_isVisible($PDP_QUANTITY))
		{
		$ProdSize=_extract(_getText($PDP_QUANTITY),"/[:](.*)[ |]/",true).toString();
		}
	$price=_extract(_getText($PDP_PRICE),"/[$](.*)/",true).toString();
	//set the quantity
	_setValue($PDP_QUANTITY_TEXTBOX,$i);
	//click on add to basket
	_click($PDP_ADDTOCART_BUTTON);
	//wait statement
	_wait(4000);
	//handling popup
	if (_isVisible($PDP_WARRANTYMODAL_POPUP));
	{
		_click($CART_NOWARRANTY_BUTTON);
	}
	//basket confirmation popup
	_assertVisible($CART_BASKETCONFIRMATION_POPUP);
	//closing all the popup's
	closingPopups();
	
}

function closingPopups()
{
	if(_isVisible($PDP_WARRANTYMODAL_POPUP))
	{
	_click($CART_NOWARRANTY_BUTTON);
	}
	
	//wait statement 
	//_wait(4000);
	
	if(_isVisible($PDP_CHOICEOFBONUSPRODUCT_DIALOG))
	{
	_click($PDP_CHOICEOFBONUSPRODUCT_CLOSE_LINK);
	}
}



function navigateToCart($subCat,$i)
{
	//add item to cart
	addItemToCart($subCat,$i);
	 if(isMobile())
	 {
	      //navigate to Cart page
		 _call($MINICART_LINK).click();
	 }
	 else
	 {
		//navigate to Cart page
		 _click($MINICART_LINK);
     }
	 $pName=_getText($CART_NAME_LINK);
	if(_isVisible($CART_SUBTOTAL))
	{
	$ItemSubtotalcart=_extract(_getText($CART_SUBTOTAL),"/[$](.*)/",true).toString();
	}
	else if(_isVisible($CART_PRICEADJUSTED))
	{
	$ItemSubtotalcart=_extract(_getText($CART_PRICEADJUSTEDTOTAL),"/[$](.*)/",true).toString();
	}

	if(_isVisible($CART_SHIPPINGPRICE))
		{
		if(_extract(_getText($CART_SHIPPINGPRICE),"/ (.*)/",true).toString()=="N/A")
		       {
			      $shippingTaxcart=_extract(_getText($CART_SHIPPINGPRICE),"/ (.*)/",true).toString();
			   }
	        else
		       {
		          $shippingTaxcart=_extract(_getText($CART_SHIPPINGPRICE),"/[$](.*)/",true).toString();
		        }
	     }
                $orderTotalcart=_extract(_getText($CART_ESTIMATED_TOTAL),"/[$](.*)/",true).toString();
                 
                 if(_isVisible($CART_SALESTAX))
	              {
	                if(_extract(_getText($CART_SALESTAX),"/ (.*)/",true).toString()=="N/A")
	                       {              
	                          $TaxCart=_extract(_getText($CART_SALESTAX),"/Tax (.*)/",true).toString();
	                      }
	               else
	            {
		              $TaxCart=_extract(_getText($CART_SALESTAX),"/[$](.*)/",true).toString();
	             }
	}
	 
}

function ClearCartItems()
{

	if(_isVisible($MINICART_LINK))
	{ 		
		_call($MINICART_LINK).click();
		
		if(isMobile())
			{
			while(_isVisible(($CART_ITEMDETAILS)))
			  {
			  _call($CART_REMOVE_LINK).click();
			  }
			}
		else
		{
	
		while (_isVisible($CART_PROMOTION_REMOVE_BUTTON))
		{
		_click($CART_PROMOTION_REMOVE_BUTTON);
		}
	
	 while(_isVisible(($CART_ITEM_TOTAL_CELL)))
	  {
	  _click($CART_REMOVE_LINK);
	  }
	 
		}
}
}

function DiscountCalculation($price,$percentage,$roundexp)
{
	var $discount=parseFloat(($price/100)*$percentage);
	_log($discount+"Discount price");
	var $finaldiscount=parseFloat($price-$discount);
	_log($finaldiscount+"final discount before round");
	$expFinalDiscount=round($finaldiscount,$roundexp);
	_log($expFinalDiscount+"final discount After round");
	return $expFinalDiscount;
}


//###################### InterMediateLogin Functions ######################

function InterMediateLogin()
{
	_setValue($LOGIN_INTERMEDIATE_EMAIL_TEXTBOX,$uId);
	_setValue($LOGIN_INTERMEDIATE_PASSWORD_TEXTBOX,$pwd);
	_click($LOGIN_INTERMEDIATELOGIN_BUTTON);

}

function GuestLogin()
{
	
	_setValue($LOGIN_GUESTCHECKOUT_EMAILTEXTBOX,$email);
	_click($LOGIN_GUESTCHECKOUT_CONTINUEBUTTON)

}

//############################ Checkout Functions #####################

function BillingAddress($sheet,$i)
{
	_setValue($CHECKOUT_BILLINGADDRESS_FNAME_TEXTBOX, $sheet[$i][1]);
	_setValue($CHECKOUT_BILLINGADDRESS_LNAME_TEXTBOX, $sheet[$i][2]);
	_setValue($CHECKOUT_BILLINGADDRESS_ADDRESS1_TEXTBOX, $sheet[$i][3]);
	_setValue($CHECKOUT_BILLINGADDRESS_ADDRESS_TEXTBOX, $sheet[$i][4]);
	_setSelected($CHECKOUT_BILLINGADDRESS_COUNTRY_TEXTBOX, $sheet[$i][5]);
	_setValue($CHECKOUT_BILLINGADDRESS_CITY_TEXTBOX, $sheet[$i][6]);
	_setValue($CHECKOUT_BILLINGADDRESS_ZIP_TEXTBOX, $sheet[$i][7]);
	_setSelected($CHECKOUT_BILLINGADDRESS_STATE_TEXTBOX, $sheet[$i][8]);
}

function ShippingAddress($sheet,$i)
{
	
	_setValue($CHECKOUT_SHIPPINGADDRESS_FNAMETEXTBOX, $sheet[$i][1]);
	_setValue($CHECKOUT_SHIPPINGADDRESS_LNAMETEXTBOX, $sheet[$i][2]);
	_setValue($CHECKOUT_SHIPPINGADDRESS_ADDRESS1TEXTBOX, $sheet[$i][3]);
	_setValue($CHECKOUT_SHIPPINGADDRESS_ADDRESS_TEXTBOX,$sheet[$i][4]);
	_setSelected($CHECKOUT_SHIPPINGADDRESS_COUNTRYTEXTBOX,$sheet[$i][5]);
  	_setValue($CHECKOUT_SHIPPINGADDRESS_CITYTEXTBOX, $sheet[$i][6]);
  	_setValue($CHECKOUT_SHIPPINGADDRESS_ZIPCODETEXTBOX,$sheet[$i][7]);
	_setSelected($CHECKOUT_SHIPPINGADDRESS_STATETEXTBOX, $sheet[$i][8]);
    _setValue($CHECKOUT_SHIPPINGADDRESS_PHONENUMBERTEXTBOX,$sheet[$i][9]);
    
}

function Paymentdetails($Sheet,$i)
{
	_setValue($CHECKOUT_PAYMENT_NAMEONCARD_TEXTBOX,$Sheet[$i][1]);	
	_setValue($CHECKOUT_PAYMENT_CARDNUMBER_TEXTBOX,$Sheet[$i][2]);
   _setSelected(_select($CHECKOUT_PAYMENT_MONTH_TEXTBOX),$Sheet[$i][3]);
   _setSelected(_select($CHECKOUT_PAYMENT_YEAR_TEXTBOX),$Sheet[$i][4]);
   _setValue($CHECKOUT_PAYMENT_CVV_TEXTBOX,$Sheet[$i][5]);
}

function navigateToShippingPage($data,$quantity)
{
	//navigate to cart page
	navigateToCart($data,$quantity);
	//navigating to shipping page
	_click($CART_CHECKOUTBUTTON);
	if(_isVisible(_div("checkout-login-container")))
	{
	_assertVisible($INTERMEDIATELOGIN_HEADING);
	_setValue($LOGIN_GUESTCHECKOUT_EMAILTEXTBOX,$email);
	_click($LOGIN_GUESTCHECKOUT_CONTINUEBUTTON);
	}
	$subTotal=parseFloat(_extract(_getText($CART_SUBTOTAL),"/[$](.*)/",true));
	$tax=parseFloat(_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString());
	_log($tax);
	$shippingTax=parseFloat(_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString());
	_log($shippingTax);
}

function paypal($paypalUN,$paypaalPwd)
{
	
	if (_isVisible(_div("PayPal Checkout", _in(_div("sliding-area")))))
		{
		
		if (_assertVisible($PAYPAL_PAGE_CONTINUE_BUTTON)){
			_click($PAYPAL_PAGE_CONTINUE_BUTTON);
			}
		else{
			_setValue($PAYPAL_EMAIL_TEXTBOX, $PayPal_Data[0][0]);
			_setValue($PAYPAL_PASSWORD_TEXTBOX,$PayPal_Data[1][0]);
			_click($PAYPAL_LOGIN_BUTTON);
			//click on continue
			_click($PAYPAL_PAGE_CONTINUE_BUTTON);
		}
		
		}
	
	else

		{
	try
	{
		_domain("www.sandbox.paypal.com")._getText(_link("Not you?"));
		_selectDomain("www.sandbox.paypal.com");
		//click on continue
		_click($PAYPAL_PAGE_CONTINUE_BUTTON);
		_selectDomain();
	}
	catch($e)
	{
		_selectWindow("/xcomponent__ppcheckout__4/");
		if(_isVisible($PAYPAL_EMAIL_TEXTBOX))
			{
			//paypal logo
			_assertVisible($PAYPAL_LOGO);
			//heading pay with paypal
			_assertVisible($PAYPAL_HEADING);	
			_setValue($PAYPAL_EMAIL_TEXTBOX, $PayPal_Data[0][0]);
			_setValue($PAYPAL_PASSWORD_TEXTBOX,$PayPal_Data[1][0]);
			_click($PAYPAL_LOGIN_BUTTON);
			//click on continue
			_click($PAYPAL_PAGE_CONTINUE_BUTTON);
			}
		else
			{
			_click($PAYPAL_PAGE_CONTINUE_BUTTON);
			}
		
		_selectWindow();
	}
		}
	
}

function addressProceed()
{
	while (_isVisible($SHIPPING_RIGHT_DETAILS_SECTION))
		{
		_click($SHIPPING_USETHISADDRESS_BUTTON,_in($SHIPPING_RIGHT_DETAILS_SECTION));
		}
	
}

function OrderPlacingScenarios($product,$Quantity,$ProductCoupon,$ordercoupon,$shippingCoupon,$percentage,$round,$BCCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$i)
{

	//Navigate to cart with product which is having color as swatch
	navigateToCart($product,$Quantity);
	
	//Order placing scenarios
	if ($ProductCoupon)
		{
		//apply the product level coupon
		_setValue($CART_COUPON_TEXTBOX,$ProductCoupon);
		//click the apply button
		_click($CART_COUPON_APPLY);
		DiscountCalculation($ItemSubtotalcart,$percentage,$round);
		//fetch the new price
		$ActualDiscountPrice=_extract(_getText($CART_SUBTOTAL),"/[$](.*)/",true).toString();
		//discount comparison
		_assertEqual($expFinalDiscount,$ActualDiscountPrice);
		}
	
	if ($ordercoupon)
		{
		//order subtotal
		var $actualOrdertotal=_extract(_getText(_cell("/rightalign/", _in(_row("order-total")))),"/[$](.*)/",true);
		//apply the product level coupon
		_setValue($CART_COUPON_TEXTBOX,$ordercoupon);
		//click the apply button
		_click($CART_COUPON_APPLY);
		DiscountCalculation($actualOrdertotal,$percentage,$round);
		//actual discount price
		$ActualDiscountPrice=parseFloat(_extract(_getText(_cell("/rightalign/", _in(_row("order-total")))),"/[$](.*)/",true));
		//discount comparison
		_assertEqual($expFinalDiscount,$ActualDiscountPrice);
		//calculation
		var $itemsubtotal=parseFloat(_extract(_getText($CART_SUBTOTAL),"/[$](.*)/",true));
		_log($itemsubtotal);
		var $orderDiscountprice=parseFloat(_extract(_getText(_cell("/rightalign/",_in(_row("order-discount discount")))),"/[$](.*)/",true));
		_log($orderDiscountprice);
		var $shippingprice=0;
		if (!_isVisible(_cell("Free", _in(_row("order-shipping")))))
			{
			$shippingprice=parseFloat(_extract(_getText(_cell("rightalign", _in(_row("order-shipping")))),"/[$](.*)/",true));
			}
		_log($shippingprice);
		var $estimatedTax=parseFloat(_extract(_getText(_cell("/rightalign/", _in(_row("order-sales-tax")))),"/[$](.*)/",true));
		_log($estimatedTax);
		var $totalOrderDiscount=round(parseFloat(($itemsubtotal+$shippingprice+$estimatedTax)-$orderDiscountprice),2);
		_log($totalOrderDiscount);
		var $actualOrdertotal=parseFloat(_extract(_getText(_cell("/rightalign/", _in(_row("order-total")))),"/[$](.*)/",true));
		_log($actualOrdertotal);
		//comparing 
		_assertEqual($totalOrderDiscount,$ActualDiscountPrice);
		
		}
	
	if ($shippingCoupon)
		{
		var $s=0;
		if (_isVisible(_span("Free",_in(_div("shipping-type")))))
			{
			_click(_div("shipping-est-delivery"));
			_click(_label("cart-ship-method["+$s+"]"));
			$s+1;
			}
		//shipping subtotal
		var $shippingprice=parseFloat(_extract(_getText(_cell("rightalign", _in(_row("order-shipping")))),"/[$](.*)/",true));
		//apply the product level coupon
		_setValue($CART_COUPON_TEXTBOX,$shippingCoupon);
		//click the apply button
		_click($CART_COUPON_APPLY);
		//calculation
		var $itemsubtotal=parseFloat(_extract(_getText($CART_SUBTOTAL),"/[$](.*)/",true));
		var $shippingDiscountprice=parseFloat(_extract(_getText(_cell("/- [$](.*)/", _in(_row("order-shipping-discount discount")))),"/- [$](.*)/",true));
		var $estimatedTax=parseFloat(_extract(_getText(_cell("/rightalign/", _in(_row("order-sales-tax")))),"/[$](.*)/",true));
		
		var $totalOrderDiscount=parseFloat($itemsubtotal+$shippingprice+$estimatedTax-$shippingDiscountprice);
		var $actualOrdertotal=_extract(_getText(_cell("/rightalign/", _in(_row("order-total")))),"/[$](.*)/",true);
		//comparing 
		_assertEqual($totalOrderDiscount,$actualOrdertotal);
		}
	
		if ($BCCard)
		{
		//click on sign up now link in beauty club card
		_click(_link("Sign up now"));
		//dialog container
		_assertVisible(_div("dialog-container"));
		//add to cart button in dialogue container
		_click(_submit("add to basket", _in(_div("dialog-container"))));
		//closing all the popups
		closingPopups();
		//beauty club card added to cart
		_assertVisible(_link("Beauty Club Card", _in(_table("cart-table"))));
		_wait(4000);
		}
	
	if ($expresspaypal)
		{
		//paypal button
		_click($CART_EXPRESSPAYPAL_BUTTON);
		_wait(30000);
		//Express paypal login
		paypal($PayPal_Data[0][0],$PayPal_Data[1][0]);
		//PLACE ORDER BUTTON
		_click($CHECKOUT_PLACEORDER_BUTTON);
		_assertVisible($OCP_HEADING);
		_assertEqual($Generic_Data[3][0], _getText($OCP_HEADING));
		}
	if ($normalflow)
		{
		if ($guest)
			{
			//click on checkout button
			_click($CART_CHECKOUTBUTTON);
			//checkout log in page as guest user
			GuestLogin();
			//entering shipping address
			if (_isVisible(_div("spc-shipping-methods")))
				{
				_click(_link("Edit",_in(_div("shipping-address"))));
				}
			//checkout page
			_assertVisible($CHECKOUT_CHECKOUTPAGE_HEADING);
		   //select shipping address
			_assertVisible($CHECKOUT_SHIPPINGADDRESS_HEADING);
			}
		else if ($intermediatelogin)
			{
			//click on checkout button
			_click($CART_CHECKOUTBUTTON);
			//login from Ilp page
			InterMediateLogin();
			//entering shipping address
			if (_isVisible(_div("spc-shipping-methods")))
				{
				_click(_link("Edit",_in(_div("shipping-address"))));
				}
			//checkout page
			_assertVisible($CHECKOUT_CHECKOUTPAGE_HEADING);
		   //select shipping address
			_assertVisible($CHECKOUT_SHIPPINGADDRESS_HEADING);
			}
		else if ($register)
			{
			//click on checkout button
			_click($CART_CHECKOUTBUTTON);
			//entering shipping address
			if (_isVisible(_div("spc-shipping-methods")))
				{
				_click(_link("Edit",_in(_div("shipping-address"))));
				}
			else if (_isVisible($CHECKOUT_SHIPPINGMETHOD_CONTINUETOPAYMENT_BUTTON))
				{
				_click();
				_click();
				}
			//checkout page
			_assertVisible($CHECKOUT_CHECKOUTPAGE_HEADING);
		   //select shipping address
			_assertVisible($CHECKOUT_SHIPPINGADDRESS_HEADING);
			}
		//Navigate to payment page
		ShippingAddress($Shippingaddress_Data,0);
		//click on continu to payment method button
		_click($CHECKOUT_CONTINUETOSHIPPINGADDRESS_BUTTON);
		//right address details
		addressProceed();
		//shipping method page
		_assertVisible($CHECKOUT_SHIPPINGMETHOD_HEADING);
		//click on continue to payment method button
		_click($CHECKOUT_SHIPPINGMETHOD_CONTINUETOPAYMENT_BUTTON);
			
		}
	
	if ($paypal)
		{
		//select paypal as payment method
		var $PaypalRadioButton=(document.getElementsByTagName("INS")[1]);
		_click($PaypalRadioButton);
		//click on continue to paypal button
		_click($CHECKOUT_CONTINUETOPAYPAL_BUTTON);
		_wait(30000);
		paypal($PayPal_Data[0][0],$PayPal_Data[1][0]);
		_selectWindow();
		_click($CHECKOUT_PLACEORDER_BUTTON);
		_assertVisible($OCP_HEADING);
		_assertEqual($Generic_Data[3][0], _getText($OCP_HEADING));
		}
		else
			{
			//select paypal as payment method
			var $creditCardRadioButton=(document.getElementsByTagName("INS")[0]);
			_click($creditCardRadioButton);
			//payment details with card
			Paymentdetails($PaymentData,$i);
			_click($CHECKOUT_CONTINUETOREVIEWORDER_BUTTON);
			_click($CHECKOUT_PLACEORDER_BUTTON);
			_assertVisible($OCP_HEADING);
			_assertEqual($Generic_Data[3][0], _getText($OCP_HEADING));
			}
}

function Logo()
{
	_click($SITE_LOGO);	
}