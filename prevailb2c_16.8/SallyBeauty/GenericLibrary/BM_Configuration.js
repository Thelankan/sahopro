_include("GlobalFunctions.js");
_resource("User_Credentials.xls");
var $userLog=_readExcelFile("User_Credentials.xls","BM_Login");
var $BMConfig=_readExcelFile("User_Credentials.xls","BMConfig");
var $Run=_readExcelFile("User_Credentials.xls","Run");

//Run type
var $run=$Run[15][0];
_log($run);


//**********************************  SetAndRun  ***********************************************//

if($run == "SetAndRun")
{
_log("SetAndRun");
BM_Login();
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));

//Click on PREVAIL Integrations link
_click(_link($userLog[2][2]));

//Social Sharing Service
_setSelected(_select("/inputfield_en/",_rightOf(_row("Social Sharing Service:"))), $Run[0][0]);
var $socialSharing=_getSelectedText(_select("/(.*)/", _near(_row("Social Sharing Service:"))));

_log($socialSharing+"......Social Sharing");
_assertEqual($Run[0][0],$socialSharing,"Data not configured properly");

//fetching enabled tax service
var $taxService=_getSelectedText(_select("/(.*)/", _near(_row("/Tax Service:/"))));



//Checkout type
_setSelected(_select("/inputfield_en/",_rightOf(_row("/Checkout Template Type:/"))),$Run[0][1]);
var $CheckoutType=_getSelectedText(_select("/(.*)/", _near(_row("/Checkout Template Type:/"))));

_log($CheckoutType+"......Checkout type");
_assertEqual($Run[0][1],$CheckoutType,"Data not configured properly");

//Shipping Address Verification Service
_setSelected(_select("/inputfield_en/",_rightOf(_row("Shipping Address Verification Service:"))),$Run[0][2]);
var $AddrVerification=_getSelectedText(_select("/(.*)/", _near(_row("Shipping Address Verification Service:"))));

_log($AddrVerification+"......Address verification");
_assertEqual($Run[0][2],$AddrVerification,"Data not configured properly");


//Gift Card Payment Service
_setSelected(_select("/inputfield_en/",_rightOf(_row("/Gift Card Payment Service:/"))),$Run[0][3]);
var $GiftCardPayment=_getSelectedText(_select("/(.*)/", _near(_row("/Gift Card Payment Service:/"))));

_log($GiftCardPayment+"......GiftCardPayment");
_assertEqual($Run[0][3],$GiftCardPayment,"Data not configured properly");

//Credit Card Payment Service
_setSelected(_select("/inputfield_en/",_rightOf(_row("/Credit Card Payment Service:/"))),$Run[0][4]);
var $CreditCardPayment=_getSelectedText(_select("/(.*)/", _near(_row("/Credit Card Payment Service:/"))));

_log($CreditCardPayment+"......CreditCardPayment");
_assertEqual($Run[0][4],$CreditCardPayment,"Data not configured properly");


//Paypal
var $status=$Run[0][5];
if($status=="CHECK")
	{
	_check(_checkbox("/(.*)/", _near(_span("Is PayPal Express Enabled"))));
	}
else if($status=="UNCHECK")
	{
	_uncheck(_checkbox("/(.*)/", _near(_span("Is PayPal Express Enabled"))));
	}

var $paypal=_checkbox("/(.*)/", _near(_span("Is PayPal Express Enabled"))).checked;

_log($paypal+"...........paypal");
//click on apply
_click(_submit("update"));

//Order section

//click on ordering
_click(_link("Ordering"));
_click(_link("Payment Methods"));

//credit card
var $cc=$Run[9][0];
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/CREDIT_CARD Credit Card/"))));
	}
else
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/CREDIT_CARDCredit Card/"))));
	}
_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
_click(_div("/"+$cc+"/",_in(_div("x-combo-list-inner"))));
_click(_div("x-panel-header x-unselectable"));
_click(_button("Apply[1]"));
_wait(2000);

if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))),[2]).toString();
	}
else
	{
		var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))),[2]).toString();
	}
_log($CreditCard+"......CreditCardPaymentorder");
_assertEqual($Run[9][0],$CreditCard,"Data not configured properly");


//gift certificate
var $gc=$Run[9][1];
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATE Gift Certificate/"))));
	}
else
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATEGift Certificate/"))));
	}
_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
_click(_div("/"+$gc+"/",_in(_div("x-combo-list-inner"))));
_click(_div("x-panel-header x-unselectable"));
_click(_button("Apply[1]"));
_wait(2000);
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATE Gift Certificate/"))),[2]).toString();
	}
else
	{
		var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATEGift Certificate/"))),[2]).toString();
	}
_log($giftCertificate+"......GiftCardPaymentorder");
_assertEqual($Run[9][1],$giftCertificate,"Data not configured properly");

//paypal
var $pp=$Run[9][2];
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/PayPal Pay Pal/"))));
	}
else
	{
		_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/PayPalPay Pal/"))));
	}
_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
_click(_div("/"+$pp+"/",_in(_div("x-combo-list-inner"))));
_click(_div("x-panel-header x-unselectable"));
_click(_button("Apply[1]"));
_wait(2000);
if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
	{
		var $paypalNormal=_getTableContents(_table("x-grid3-row-table", _in(_div("/PayPal Pay Pal/"))),[2]).toString();
	}
else
	{
		var $paypalNormal=_getTableContents(_table("x-grid3-row-table", _in(_div("/PayPalPay Pal/"))),[2]).toString();
	}
_log($paypalNormal+"...........paypalorder");
_assertEqual($Run[9][2],$paypalNormal,"Data not configured properly");



//Setting Credit card type in payment processor Under payment methods
if ($CreditCardPayment==$BMConfig[2][4] && $CreditCard=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
		}
		else
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[0][5]));
	}
else if ($CreditCardPayment==$BMConfig[1][4] && $CreditCard=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
		}
		else
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[1][5]));
	}
else if($CreditCardPayment==$BMConfig[0][4] && $CreditCard=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari() ||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
		}
		else
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[2][5]));
	}

//click on apply after selecting the payment processor
_click(_table("apply_payment_method_changes_button"));

//Setting paypal payment type in payment processor Under payment methods
if ($paypal && $paypalNormal=="Yes")
	{
	
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/PayPal Pay Pal/"))));
		}
		else
		{
		_click(_table("x-grid3-row-table", _in(_div("/PayPalPay Pal/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))))
		_click(_div($BMConfig[0][6]));
		_click(_table("apply_payment_method_changes_button"));
	}


_click(_link("Log off."));

}


//**********************************  FetchAndRun  ***********************************************//

else if($run == "FetchAndRun")
{
	_log("FetchAndRun");
BM_Login();
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));

//Click on PREVAIL Integrations link
_click(_link($userLog[2][2]));
var $socialSharing=_getSelectedText(_select("/(.*)/", _near(_row("Social Sharing Service:"))));
var $CheckoutType=_getSelectedText(_select("/(.*)/", _near(_row("/Checkout Template Type:/"))));
_log($CheckoutType);
var $paypal=_checkbox("/(.*)/", _near(_span("Is PayPal Express Enabled"))).checked;
_log($paypal);
var $AddrVerification=_getSelectedText(_select("/(.*)/", _near(_row("Shipping Address Verification Service:"))));
var $GiftCardPayment=_getSelectedText(_select("/(.*)/", _near(_row("/Gift Card Payment Service:/"))));
var $CreditCardPayment=_getSelectedText(_select("/(.*)/", _near(_row("/Credit Card Payment Service:/"))));

//fetching enabled tax service
var $taxService=_getSelectedText(_select("/(.*)/", _near(_row("/Tax Service:/"))));

//click on ordering
_click(_link("Ordering"));
_click(_link("Payment Methods"));

if (isIE11() || mobile.iPad() || _isSafari() ||_isIE())
	{
	var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))),[2]);
	var $paypalNormal=_getTableContents(_table("x-grid3-row-table", _in(_div("/PayPal Pay Pal/"))),[2]);
	var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATE Gift Certificate/"))),[2]);
	}
else
	{
var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))),[2]);
var $paypalNormal=_getTableContents(_table("x-grid3-row-table", _in(_div("/PayPalPay Pal/"))),[2]);
var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATEGift Certificate/"))),[2]);
	}

 
//Setting Credit card type in payment processor Under payment methods
if ($CreditCardPayment==$BMConfig[2][4] && $CreditCard=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari() || _isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
		}
		else
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))))
		_click(_div($BMConfig[0][5]));
	}
else if ($CreditCardPayment==$BMConfig[1][4] && $CreditCard=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari() ||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
		}
		else
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[1][5]));
	}
else if($CreditCardPayment==$BMConfig[0][4] && $CreditCard=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari() ||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
		}
		else
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[2][5]));
	}
//click on apply after selecting the payment processor
_click(_table("apply_payment_method_changes_button"));

//Setting paypal payment type in payment processor Under payment methods
if ($paypal && $paypalNormal=="Yes")
	{
	
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/PayPal Pay Pal/"))));
		}
	else
		{
		_click(_table("x-grid3-row-table", _in(_div("/PayPalPay Pal/"))));
		}
	_click(_image("s.gif", _in(_row("Payment Processor:"))))
	_click(_div($BMConfig[0][6]));
	_click(_table("apply_payment_method_changes_button"));
	
	}


_click(_link("Log off."));

}


//**********************************  DefaultRun  ***********************************************//

	else
	{
	
	/*var $socialSharing="NO";
	var $CheckoutType="FIVE_PAGE";
	var $paypal="true";
	_log($paypal);
	var $AddrVerification="NO";
	var $GiftCardPayment="DEMANDWARE";
	var $CreditCardPayment="SOAP";
	var $EECyberSourceMethod="none";
	var $CreditCard="Yes";
	var $paypalNormal="Yes";
	var $giftCertificate="NO";*/
		
	var $socialSharing="NO";
	var $CheckoutType="FIVE_PAGE";
	var $paypal="false";
	_log($paypal);
	var $AddrVerification="NO";
	var $GiftCardPayment="DEMANDWARE";
	var $CreditCardPayment="NO";
	var $EECyberSourceMethod="NO";
	var $CreditCard="Yes";
	var $paypalNormal="Yes";
	var $giftCertificate="NO";
	}
