_include("GlobalFunctions.js");
_resource("User_Credentials.xls");
_include("../ObjectRepository/Generic_OR.sah");
var $CheckoutType;

function BM_Login()//function added
{
	_navigateTo($URL[2][0]);
	_setValue(_textbox("LoginForm_Login"),$userLog[0][0]);
	_setValue(_password("LoginForm_Password"),$userLog[0][1]);
	_click(_submit("Log In"));
	if(isMobile())
		{			
			_setSelected(_select("SelectedSiteID"), $userLog[0][2]);
		}
	else
		{
			_click(_span("/sod_label/"));
			_click(_span($userLog[0][2]));
			if(_isVisible(_cell("Welcome to the Demandware Business Manager.")))
				{
					_click(_span("sod_option  active selected"));
					_assertEqual($userLog[0][2], _getText(_span("sod_label")));
				}
		}	 
}

function createGC($GC)
{
	 
	 deleteGC($GC);
	 BM_Login();
	 _click(_link("Custom Objects"));
	 _click(_link("Custom Object Editor"));
	 var $range=$GC.length;
	 for(var $i=0;$i<$range;$i++)
	 	{
		 _setSelected(_select("ObjectTypeDefinitionUUID"), "GCCustom");
		 _click(_submit("New"));
		 _setValue(_textbox("inputfield_en"), $GC[$i][0]);
		 _setValue(_textbox("inputfield_en[1]"), $GC[$i][1]);
		 _click(_checkbox("true"));
		 _setValue(_textbox("inputfield_en[2]"), $GC[$i][2]);
		 _wait(1000);
		 _click(_submit("Apply"));
		 _click(_submit("/Back to List/"));
		 //_click(_link("CustomObjects"));
		 //_click(_link("breadcrumb"));
	 	}
	 _click(_link("Log off."));
}

function deleteGC($GC)
{ 
	 //BM Login
	 BM_Login();
	 _click(_link("Custom Objects"));
	 _click(_link("Custom Object Editor"));
	 var $range=$GC.length; 
	 for(var $i=0;$i<$range;$i++)
		 {
			 _setSelected(_select("ObjectTypeDefinitionUUID"), "GCCustom");
			 _setValue(_textbox("SearchPhrase"), $GC[$i][1]);
			 _click(_submit("Find"));
			 if(_isVisible(_checkbox("SelectedObjectUUID")))
				{
				 _click(_checkbox("SelectedObjectUUID"));
				 _click(_submit("Delete"));
				 _click(_submit("OK"));
				}
		 }	
	 _click(_link("Log off."));
}

function enableMakeDefault()
{
	//BM Login
	 BM_Login();
	 _click(_link("Site Preferences"));
	 _click(_link("Custom Preferences"));
	 _click(_link("PREVAIL Customizations"));
	 var $boolean=_checkbox("/(.*)/",_rightOf(_row("/Is Default Shipping And Billing Addresses/"))).checked;
	 if($boolean)
		 {
		 _uncheck(_checkbox("/(.*)/",_rightOf(_row("/Is Default Shipping And Billing Addresses/"))));
		 }
	 else
		 {
		 _log("alredy make default is enabled");
		 }
	 //click on apply
	 _click(_submit("update"));
	 _click(_link("Log off."));
}
//Arjun
function enableDefaultShippingAddress()
{
	//BM Login
	 BM_Login();
	 _click(_link("Site Preferences"));
	 _click(_link("Custom Preferences"));
	 _click(_link("PREVAIL Customizations"));
	 //_check(_checkbox("/(.*)/", _near(_span("Is Default Shipping And Billing Addresses Enabled"))));
	 _setSelected(_select("dw-select", _near(_label("Is BISN Enabled"))), "Yes");
	 //click on save
	 _click(_button("Save"));
	 //_click(_submit("update"));
	 _click(_link("Log off."));
}

function deleteUser()
{
	BM_Login();
_click(_link("Customers"));
_click(_link("Customers[1]"));
_click(_link("Advanced"));
_setValue(_textbox("WFCustomerAdvancedSearch_Email"),$email);
_click(_submit("Find", _in(_div("/Advanced Customer Search/"))));
if(_isVisible(_row("/Select All/")))
{
	if(_assertEqual($email,_getText(_cell("table_detail e s[4]"))))
		{
		_click(_checkbox("DeleteCustomer"));
		_click(_submit("Delete"));
		_click(_submit("OK"));
		_log("Successfully deleted the user");
		}
}
else
{
	_log("search did not match any users.");
}
_click(_link("Log off."));

}

function enableOgone()
{
	//login to BM
	BM_Login();
	_click(_link("Site Preferences"));
	_click(_link("Custom Preferences"));
	//Click on PREVAIL Integrations link
	_click(_link($userLog[2][2]));
	//check the ogone check box
	_setSelected(_select("dw-select", _in(_row("/Is Ogone Enabled/"))), "Yes");
	//click on save
	_click(_button("Save"));
	//logoff
	_click(_link("Log off."));
}

function verifyPaymentSecInBM($orderNum,$sheet,$price)
{
	//login to BM
	BM_Login();
	//navigate to orders page
	_click(_link("Ordering"));
	_click(_link("Orders"));
	_setValue(_textbox("OrderSearchForm2_SimpleSearchTerm"),$orderNum);
	_click(_submit("Find"));
	_click(_link("table_detail_link"));
	_click(_link("Payment"));
	//check the payment method	
	for(var $i=0;$i<$sheet.length;$i++)
		{
		if(isMobile())
			{
				_wait(4000);
			}
			if(_getText(_cell("infobox_item top s")).indexOf($sheet[$i][0])>=0)
				{
					_assert(true);
				}
			else
				{
					_assert(false);
				}
		}
	//verifying price
	if(_getText(_cell("infobox_item top s")).indexOf($price)>=0)
	{
		_assert(true);
	}
	else
	{
		_assert(false);
	}
	//logoff
	_click(_link("Log off."));
}

function makeServiceDown($serviceName)
{
	//verify log on service down
	BM_Login();
	_click(_link("Administration"));
	_click(_link("Operations"));
	_click(_link("Services"));
	_click(_link($serviceName));
	_uncheck(_checkbox("/Enabled/"));
	_click(_submit("Apply"));
	_click(_link("Log off."));	
}

function makeServiceUpAndRunning($serviceName)
{
	//verify log on service down
	BM_Login();
	_click(_link("Administration"));
	_click(_link("Operations"));
	_click(_link("Services"));
	_click(_link($serviceName));
	_check(_checkbox("/Enabled/"));
	_click(_submit("Apply"));
	_click(_link("Log off."));	
}

function enableEndToEnd()
{
	//verify log on service down
	BM_Login();
	_click(_link("Site Preferences"));
	_click(_link("Custom Preferences"));
	//Click on PREVAIL Integrations link
	_click(_link("PREVAIL Integrations"));
	_check(_checkbox("/(.*)/",_rightOf(_row("/Is End To End Merchant/"))));
	_click(_submit("update"));
	_click(_link("Log off."));	
}

function disableEndToEnd()
{
	//verify log on service down
	BM_Login();
	_click(_link("Site Preferences"));
	_click(_link("Custom Preferences"));
	//Click on PREVAIL Integrations link
	_click(_link("PREVAIL Integrations"));
	_uncheck(_checkbox("/(.*)/",_rightOf(_row("/Is End To End Merchant/"))));
	_click(_submit("update"));
	_click(_link("Log off."));	
}

//----------------------cart Functions---------------------------------
/*
function setInventory($sheet)
{
	//Navigating to change the inventory in BM tool
	_click(_link("overview_subtitle"));
	_click(_link("Inventory Lists"));
	_click(_link("inventory"));
	_click(_link("Records"));
	for(var $i=0; $i<$sheet.length; $i++)
		{
			_setValue(_textbox("SearchTerm"), $sheet[$i][1]);
			_click(_submit("Find"));
			_click(_button("Edit"));					
			//setting back order and pre order
			if($i==2 || $i==3)
				{
					_setValue(_textbox("InventoryRecordForm_Allocation"),"0");
					_click(_radio( $sheet[$i][2]));
					_setValue(_textbox("PreorderBackorderAllocation"),"4");
				}
			else
				{
				    _setValue(_textbox("InventoryRecordForm_Allocation"),  $sheet[$i][2]);	
					if(!_getAttribute(_radio("NONE"),"checked"))
						{
							_click(_radio("NONE"));
							_setValue(_textbox("PreorderBackorderAllocation"),0);
						}
				}
			_click(_submit("Apply"));
			if(_isVisible(_button("Yes")))
				{
					_click(_button("Yes"));
				}
			_click(_link("inventory - Records"));
			if(isMobile() || _isIE())
				{
					_wait(4000);
				}		
		}
	_click(_link("Log off."));	
}

function resetInventory($sheet)
{	
	_click(_link("overview_subtitle"));
	_click(_link("Inventory Lists"));
	_click(_link("inventory"));
	_click(_link("Records"));
	for(var $i=0; $i<$sheet.length; $i++)
		{
			_setValue(_textbox("SearchTerm"), $sheet[$i][1]);
			_click(_submit("Find"));
			_click(_button("Edit"));
			_setValue(_textbox("InventoryRecordForm_Allocation"),  $sheet[0][2]);
			if(!_getAttribute(_radio("NONE"),"checked"))
				{
					_click(_radio("NONE"));
				}
			_setValue(_textbox("PreorderBackorderAllocation"),"0");
			_click(_submit("Apply"));
			
			if(_isVisible(_button("Yes")))
				{
					_click(_button("Yes"));
				}
			if(isMobile() || _isIE())
			{
				_wait(4000);
			}
			_click(_link("inventory - Records"));
		}
	_click(_link("Log off."));
} 

*/
//----------------- Store Locator----

function SetStoreLocator_Demandware($name)
{
BM_Login();
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));
_click(_link("PREVAIL Integrations"));

//var $PreviousValue=_getSelectedText(_select("MetabcIjQiaag0uYwaaadhGjpzlnq4"));
/*if (_isSafari())
{
_setSelected(_select("MetabcIjQiaag0uYwaaadhGjpzlnq4"),"DEMANDWARE");
}
else
	{
	_setSelected(_select("MetabcPPEiaag19XIaaadh11QACTfI"),"DEMANDWARE");
	}*/

_setSelected(_select("inputfield_en", _in(_cell("PREVAIL DEMANDWARE"))),$name);
_click(_submit("update"));
_click(_link("Log off."));
}

function SetStoreLocator_Prevail($name)
{
BM_Login();
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));
_click(_link("PREVAIL Integrations"));
//var $PreviousValue=_getSelectedText(_select("MetabcIjQiaag0uYwaaadhGjpzlnq4"));

/*if (_isSafari())
{
_setSelected(_select("MetabcIjQiaag0uYwaaadhGjpzlnq4"),"PREVAIL");
}
else
	{
	_setSelected(_select("MetabcPPEiaag19XIaaadh11QACTfI"),"PREVAIL");//DEMANDWARE
	}*/

_setSelected(_select("inputfield_en", _in(_cell("PREVAIL DEMANDWARE"))),$name);
_click(_submit("update"));
_click(_link("Log off."));
}





function ResetStoreLocator()
{
BM_Login();
_click(_link("Site Preferences"));
_click(_link("Custom Preferences"));
_click(_link("PREVAIL Integrations"));
_setSelected(_select("MetabcIjQiaag0uYwaaadhGjpzlnq4"),$PreviousValue);
_click(_submit("update"));
_click(_link("Log off."));
}

//--------- End To End Scenarios ------------
function enableAutharize_net()
{
	//set the authorize_net payment processor
	BM_Login();
	//navigate to prevail dash board
	_click(_link("Site Preferences"));
	_click(_link("Custom Preferences"));
	//Click on PREVAIL Integrations link
	_click(_link($userLog[2][2]));
	//fetch checkout type
	var $CheckoutType=_getSelectedText(_select("/(.*)/", _near(_row("/Checkout Template Type:/"))));

	_setSelected(_select("/inputfield_en/",_rightOf(_row("/Credit Card Payment Service:/"))),$BMConfig[1][4]);
	//click on apply
	_click(_submit("update"));

	//click on ordering
	_click(_link("Ordering"));
	_click(_link("Payment Methods"));

	//gift certificate
	var $gc=$Run[9][1];
	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATE Gift Certificate/"))));
		}
	else
		{
			_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATEGift Certificate/"))));
		}
	_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
	_click(_div("/"+$gc+"/",_in(_div("x-combo-list-inner"))));
	_click(_div("x-panel-header x-unselectable"));
	_click(_button("Apply[1]"));
	_wait(2000);
	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATE Gift Certificate/"))),[2]).toString();
		}
	else
		{
			var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATEGift Certificate/"))),[2]).toString();
		}
	_log($giftCertificate+"......GiftCardPaymentorder");
	_assertEqual($Run[9][1],$giftCertificate,"Data not configured properly");

	if($giftCertificate=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
		}
		else
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[0][7]));
		_click(_button("Apply[1]"));
	}


	//credit card
	var $cc=$Run[9][0];
	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/CREDIT_CARD Credit Card/"))));
		}
	else
		{
			_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/CREDIT_CARDCredit Card/"))));
		}
	_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
	_click(_div("/"+$cc+"/",_in(_div("x-combo-list-inner"))));
	_click(_div("x-panel-header x-unselectable"));
	_click(_button("Apply[1]"));
	_wait(2000);

	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))),[2]).toString();
		}
	else
		{
			var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))),[2]).toString();
		}
	_log($CreditCard+"......CreditCardPaymentorder");
	_assertEqual($Run[9][0],$CreditCard,"Data not configured properly");

	if($CreditCard=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
		}
		else
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[1][5]));
		_click(_button("Apply[1]"));
	}

	//log of from BM
	_click(_link("Log off."));	
}


function enableCyberSource()
{
	//set the authorize_net payment processor
	BM_Login();
	//navigate to prevail dash board
	_click(_link("Site Preferences"));
	_click(_link("Custom Preferences"));
	//Click on PREVAIL Integrations link
	_click(_link($userLog[2][2]));
	//fetch checkout type
	var $CheckoutType=_getSelectedText(_select("/(.*)/", _near(_row("/Checkout Template Type:/"))));

	_setSelected(_select("/inputfield_en/",_rightOf(_row("/Credit Card Payment Service:/"))),$BMConfig[2][4]);
	//click on apply
	_click(_submit("update"));

	//click on ordering
	_click(_link("Ordering"));
	_click(_link("Payment Methods"));

	//gift certificate
	var $gc=$Run[9][1];
	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATE Gift Certificate/"))));
		}
	else
		{
			_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATEGift Certificate/"))));
		}
	_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
	_click(_div("/"+$gc+"/",_in(_div("x-combo-list-inner"))));
	_click(_div("x-panel-header x-unselectable"));
	_click(_button("Apply[1]"));
	_wait(2000);
	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATE Gift Certificate/"))),[2]).toString();
		}
	else
		{
			var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATEGift Certificate/"))),[2]).toString();
		}
	_log($giftCertificate+"......GiftCardPaymentorder");
	_assertEqual($Run[9][1],$giftCertificate,"Data not configured properly");

	if($giftCertificate=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
		}
		else
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[0][7]));
		_click(_button("Apply[1]"));
	}


	//credit card
	var $cc=$Run[9][0];
	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/CREDIT_CARD Credit Card/"))));
		}
	else
		{
			_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/CREDIT_CARDCredit Card/"))));
		}
	_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
	_click(_div("/"+$cc+"/",_in(_div("x-combo-list-inner"))));
	_click(_div("x-panel-header x-unselectable"));
	_click(_button("Apply[1]"));
	_wait(2000);

	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))),[2]).toString();
		}
	else
		{
			var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))),[2]).toString();
		}
	_log($CreditCard+"......CreditCardPaymentorder");
	_assertEqual($Run[9][0],$CreditCard,"Data not configured properly");

	if($CreditCard=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
		}
		else
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[0][5]));
		_click(_button("Apply[1]"));
	}

	//log of from BM
	_click(_link("Log off."));
	
}


function enableEE_CyberSource_SCMP()
{
	//set the authorize_net payment processor
	BM_Login();
	//navigate to prevail dash board
	_click(_link("Site Preferences"));
	_click(_link("Custom Preferences"));
	//click on end to end Cyber source
	_click(_link("EndTOEndCybersource"));
	//select SCMP from drop down
	_setSelected(_select("inputfield_en",_rightOf(_cell("/Cybersource API Method/"))),$BMConfig[4][4]);
	//click on apply
	_click(_submit("update"));

	//navigate back to custom site preferences
	_click(_link("Custom Site Preferences"));
	//Click on PREVAIL Integrations link
	_click(_link($userLog[2][2]));
	//fetch checkout type
	var $CheckoutType=_getSelectedText(_select("/(.*)/", _near(_row("/Checkout Template Type:/"))));

	_setSelected(_select("/inputfield_en/",_rightOf(_row("/Credit Card Payment Service:/"))),$BMConfig[3][4]);
	//click on apply
	_click(_submit("update"));

	//click on ordering
	_click(_link("Ordering"));
	_click(_link("Payment Methods"));

	//gift certificate
	var $gc=$Run[9][1];
	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATE Gift Certificate/"))));
		}
	else
		{
			_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATEGift Certificate/"))));
		}
	_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
	_click(_div("/"+$gc+"/",_in(_div("x-combo-list-inner"))));
	_click(_div("x-panel-header x-unselectable"));
	_click(_button("Apply[1]"));
	_wait(2000);
	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATE Gift Certificate/"))),[2]).toString();
		}
	else
		{
			var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATEGift Certificate/"))),[2]).toString();
		}
	_log($giftCertificate+"......GiftCardPaymentorder");
	_assertEqual($Run[9][1],$giftCertificate,"Data not configured properly");

	if($giftCertificate=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
		}
		else
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[0][7]));
		_click(_button("Apply[1]"));
	}


	//credit card
	var $cc=$Run[9][0];
	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/CREDIT_CARD Credit Card/"))));
		}
	else
		{
			_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/CREDIT_CARDCredit Card/"))));
		}
	_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
	_click(_div("/"+$cc+"/",_in(_div("x-combo-list-inner"))));
	_click(_div("x-panel-header x-unselectable"));
	_click(_button("Apply[1]"));
	_wait(2000);

	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))),[2]).toString();
		}
	else
		{
			var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))),[2]).toString();
		}
	_log($CreditCard+"......CreditCardPaymentorder");
	_assertEqual($Run[9][0],$CreditCard,"Data not configured properly");

	if($CreditCard=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
		}
		else
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[3][5]));
		_click(_button("Apply[1]"));
	}

	//log of from BM
	_click(_link("Log off."));	
}

function enableEE_CyberSource_SOAP()
{
	//set the authorize_net payment processor
	BM_Login();
	//navigate to prevail dash board
	_click(_link("Site Preferences"));
	_click(_link("Custom Preferences"));
	//click on end to end Cyber source
	_click(_link("EndTOEndCybersource"));
	//select SOAP from drop down
	_setSelected(_select("inputfield_en",_rightOf(_cell("/Cybersource API Method/"))),$BMConfig[5][4]);
	//click on apply
	_click(_submit("update"));

	//navigate back to custom site preferences
	_click(_link("Custom Site Preferences"));
	//Click on PREVAIL Integrations link
	_click(_link($userLog[2][2]));
	//fetch checkout type
	var $CheckoutType=_getSelectedText(_select("/(.*)/", _near(_row("/Checkout Template Type:/"))));

	_setSelected(_select("/inputfield_en/",_rightOf(_row("/Credit Card Payment Service:/"))),$BMConfig[3][4]);
	//click on apply
	_click(_submit("update"));

	//click on ordering
	_click(_link("Ordering"));
	_click(_link("Payment Methods"));

	//gift certificate
	var $gc=$Run[9][1];
	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATE Gift Certificate/"))));
		}
	else
		{
			_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/GIFT_CERTIFICATEGift Certificate/"))));
		}
	_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
	_click(_div("/"+$gc+"/",_in(_div("x-combo-list-inner"))));
	_click(_div("x-panel-header x-unselectable"));
	_click(_button("Apply[1]"));
	_wait(2000);
	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATE Gift Certificate/"))),[2]).toString();
		}
	else
		{
			var $giftCertificate=_getTableContents(_table("x-grid3-row-table", _in(_div("/GIFT_CERTIFICATEGift Certificate/"))),[2]).toString();
		}
	_log($giftCertificate+"......GiftCardPaymentorder");
	_assertEqual($Run[9][1],$giftCertificate,"Data not configured properly");

	if($giftCertificate=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
		}
		else
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[0][7]));
		_click(_button("Apply[1]"));
	}


	//credit card
	var $cc=$Run[9][0];
	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/CREDIT_CARD Credit Card/"))));
		}
	else
		{
			_click(_div("/x-grid3-cell-inner x-grid3-col-enabled/",_in(_div("/CREDIT_CARDCredit Card/"))));
		}
	_click(_image("/s.gif/",_in(_div("x-layer x-editor x-small-editor x-grid-editor"))));
	_click(_div("/"+$cc+"/",_in(_div("x-combo-list-inner"))));
	_click(_div("x-panel-header x-unselectable"));
	_click(_button("Apply[1]"));
	_wait(2000);

	if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))),[2]).toString();
		}
	else
		{
			var $CreditCard=_getTableContents(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))),[2]).toString();
		}
	_log($CreditCard+"......CreditCardPaymentorder");
	_assertEqual($Run[9][0],$CreditCard,"Data not configured properly");

	if($CreditCard=="Yes")
	{
		if (isIE11() || mobile.iPad() || _isSafari()||_isIE())
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARD Credit Card/"))));
		}
		else
		{
			_click(_table("x-grid3-row-table", _in(_div("/CREDIT_CARDCredit Card/"))));
		}
		_click(_image("s.gif", _in(_row("Payment Processor:"))));
		_click(_div($BMConfig[3][5]));
		_click(_button("Apply[1]"));
	}

	//log of from BM
	_click(_link("Log off."));	
}