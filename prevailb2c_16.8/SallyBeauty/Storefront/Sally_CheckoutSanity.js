_include("../GenericLibrary/GlobalFunctions.js");
_resource("SanityData.xls");

//delete user
//deleteUser();

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//*************** Guest User **********************
var $t = _testcase("287581/291730","Verify whether user is able to place order with warranty prodcuts/[Guest: Billing]: Verify the functionality of Continue to Shopping Method button on Guest Billing Address page");
$t.start();
try
{
	//Add item to cart
	search($couponCode_data[0][1]);
	
	_wait(2000);
	//navigate to PDP
	if (_isVisible($PLP_SEARCH_RESULT_ITEMS))
		{
		_click($PLP_NAMELINK);
		}
	//add to cart for 
	_click($PDP_ADDTOCART_BUTTON);
	//Aaa Warrenty Button
	_click($CART_ADDWARRANTYFROMPDP_BUTTON);
	
	//no bonus product button
	if(_isVisible($PDP_CHOICEOFBONUSPRODUCT_DIALOG))
	{
	_click($PDP_CHOICEOFBONUSPRODUCT_CLOSE_LINK);
	}

	//click on minicart link
	_click($MINICART_LINK);
	//click on checkout now button in minicart
	_click($MINICART_CHECKOUTNOW_BUTTON);
	//shopping basket page
	_assertVisible($CART_SHOPPING_BASKET_HEADING);
	//click on checkout now button in the cart page
	_click($CART_CHECKOUTBUTTON);
	//login module
	_assertVisible($CART_LOGINMODULE);
	//guest login
	GuestLogin();
	//checkout page
	_assertVisible($CHECKOUT_CHECKOUTPAGE_HEADING);
	//Enter shipping address
	ShippingAddress($Shippingaddress_Data,0);
	 //click on No radio button in billing address
	 var $NotSameAsShipping=document.getElementsByTagName("ins")[4];
	_click($NotSameAsShipping);
	BillingAddress($BillingAddress_data,0);
	//click on continue to shipping method
	_click($CHECKOUT_CONTINUETOSHIPPINGADDRESS_BUTTON);
	//use this address scetion
	addressProceed();
	//shipping method page
	_assertVisible($CHECKOUT_SHIPPINGMETHOD_HEADING);
	//click on continue to payment method button
	_click($CHECKOUT_SHIPPINGMETHOD_CONTINUETOPAYMENT_BUTTON);
	//Payment details
	Paymentdetails($PaymentData,2);
	//click on order review button
	_click($CHECKOUT_PAYMENT_REVIEWORDER_BUTTON);
	//click on place order button
	_click($CHECKOUT_PLACEORDER_BUTTON);
	//Order confirmation page
	_assertVisible($OCP_HEADING);

   //select shipping address
   //_assertVisible($CHECKOUT_SHIPPINGADDRESS_HEADING);
   //$Shippingaddress=document.getElementsByTagName("INS")[1];
   //_click($Shippingaddress);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296130","Verify the Guest User is able to Place order using Master Card as a payment type with Standard Product as part of Line item .");
$t.start();
try
{

	navigateToShippingPage($couponCode_data[2][1],$Cart_Data[0][1]);
	//checkout heading
	_assertVisible($CHECKOUT_CHECKOUTPAGE_HEADING);
	//enter shipping address
	ShippingAddress($Shippingaddress_Data,0);
	//click on continue to shipping method
	_click($CHECKOUT_CONTINUETOSHIPPINGADDRESS_BUTTON);
	//shipping address section
	_assertVisible($CHECKOUT_SHIPPINGADDRESS_SECTION);
	//billing address section
	_assertVisible($CHECKOUT_BILLINGADDRESS_SECTION);
	//click on continu to payment method button
	_click($CHECKOUT_SHIPPINGMETHOD_CONTINUETOPAYMENT_BUTTON);
	//select credit card as payment details
	_assertVisible($CHECKOUT_PAYMENT_CREDITCARD_BUTTON);
	$creditCardRadioButton=document.getElementsByTagName("INS")[0];
	_click($creditCardRadioButton);
	//enter master card as payment details
	Paymentdetails($PaymentData,1);
	//master card image
	_assertVisible($CHECKOUT_PAYMENT_MASTERCARDIMAGE);
	//click on review order button
	_click($CHECKOUT_PAYMENT_REVIEWORDER_BUTTON);
	//click on edit button of shipping address
	_click($CHECKOUT_SHIPPINGADDRESS_EDIT_BUTTON);
	//shippingaddress fields section
	_assertVisible($CHECKOUT_SHIPPINGADDRESS_FEILDSSECTION);
    // shipping address  pre populated with user entered address
	_assertEqual($Shippingaddress_Data[0][1], _getValue($CHECKOUT_SHIPPINGADDRESS_FNAMETEXTBOX));
	_assertEqual($Shippingaddress_Data[0][2], _getValue($CHECKOUT_SHIPPINGADDRESS_LNAMETEXTBOX));
	_assertEqual($Shippingaddress_Data[0][3]+" "+$Shippingaddress_Data[0][4], _getValue($CHECKOUT_SHIPPINGADDRESS_ADDRESS1TEXTBOX));
	_assertEqual($Shippingaddress_Data[0][5], _getSelectedText($CHECKOUT_SHIPPINGADDRESS_COUNTRYTEXTBOX));
	_assertEqual($Shippingaddress_Data[0][6], _getValue($CHECKOUT_SHIPPINGADDRESS_CITYTEXTBOX));
	_assertEqual($Shippingaddress_Data[0][10], _getValue($CHECKOUT_SHIPPINGADDRESS_ZIPCODETEXTBOX));
	_assertEqual($Shippingaddress_Data[0][8], _getSelectedText($CHECKOUT_SHIPPINGADDRESS_STATETEXTBOX));
	_assertEqual($Shippingaddress_Data[0][9], _getValue($CHECKOUT_SHIPPINGADDRESS_PHONENUMBERTEXTBOX));
	_assertEqual($email, _getValue($CHECKOUT_SHIPPINGADDRESS_EMAILTEXTBOX));
	//update shipping address
	ShippingAddress($Shippingaddress_Data,1);
	_click($CHECKOUT_CONTINUETOSHIPPINGADDRESS_BUTTON);
	//use this address
	if(_isVisible($CHECKOUT_USETHISADDRESS_BUTTON))
	{
	_click($CHECKOUT_USETHISADDRESS_BUTTON);
	}
	//click on continue to payment method button
	_assertVisible($CHECKOUT_SHIPPINGADDRESS_SECTION);
	_click($CHECKOUT_SHIPPINGMETHOD_CONTINUETOPAYMENT_BUTTON);
	//enter master card as payment details
	Paymentdetails($PaymentData,1);
	//click on place order button
	_click($CHECKOUT_PAYMENT_REVIEWORDER_BUTTON);
	//place order button
	_click($CHECKOUT_PLACEORDER_BUTTON);
	//OCP Page
	_assertVisible($OCP_HEADING);
	_assertContainsText($PaymentData[1][6], $OCP_CARD_NUMBER);
    //updated shipping address verification//
	_assertEqual($Shippingaddress_Data[1][1], _getText($OCP_SHIPPINGADDRESS_FNAME));
	_assertEqual($Shippingaddress_Data[1][2], _getText($OCP_SHIPPINGADDRESS_LNAME));
	_assertEqual($Shippingaddress_Data[1][3], _getText($OCP_ADDRESSDETAILS));
	_assertEqual($Shippingaddress_Data[1][4], _getText($OCP_SHIPPINGADDRESS_ADDRESSLINE2));
	_assertEqual($Shippingaddress_Data[1][10], _getText($OCP_SHIPPINGADDRESS_STATENAME));
	_assertEqual($Shippingaddress_Data[1][14], _getText($OCP_SHIPPINGADDRESS_PHONENUMBER));

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296791/296130","Verify whether order can be placed when credit card radio button is selected as the payment method with Master credit card number as a guest user");
$t.start();
try
{
	
	//parameters 
	var $productID=$Cart_Data[0][0];
	var $productQuantity=$Cart_Data[0][1];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=false;
	var $shippinglevelCoupon=false;
	var $percentage=false;
	var $round=false;
	var $BcCard=false;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=true;
	var $intermediatelogin=false;
	var $register=false;
	var $paypal=false;
	var $paymentcard=1;//Master Card

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)
_assertContainsText($PaymentData[1][6], $OCP_CARD_NUMBER);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296792/296135","Verify whether order can be placed when credit card radio button is selected as the payment method with Discover credit card number as a guest user.");
$t.start();
try
{
	
	//parameters 
	var $productID=$Cart_Data[0][0];
	var $productQuantity=$Cart_Data[0][1];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=false;
	var $shippinglevelCoupon=false;
	var $percentage=false;
	var $round=false;
	var $BcCard=false;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=true;
	var $intermediatelogin=false;
	var $register=false;
	var $paypal=false;
	var $paymentcard=3;//Discover Card

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)
_assertContainsText($PaymentData[3][6], $OCP_CARD_NUMBER);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296793","Verify whether order can be placed when credit card radio button is selected as the payment method with AMEX credit card number as a guest user.");
$t.start();
try
{
	
	//parameters 
	var $productID=$Cart_Data[0][0];
	var $productQuantity=$Cart_Data[0][1];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=false;
	var $shippinglevelCoupon=false;
	var $percentage=false;
	var $round=false;
	var $BcCard=false;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=true;
	var $intermediatelogin=false;
	var $register=false;
	var $paypal=false;
	var $paymentcard=2;//Amex Card

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)
_assertContainsText($PaymentData[2][6], $OCP_CARD_NUMBER);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296127","Verify if a user is able to place Order using 13 digit Visa card and able to create an account in Order Confirmation Page.");
$t.start();
try
{
	
	//parameters 
	var $productID=$Cart_Data[0][0];
	var $productQuantity=$Cart_Data[0][1];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=false;
	var $shippinglevelCoupon=false;
	var $percentage=false;
	var $round=false;
	var $BcCard=false;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=true;
	var $intermediatelogin=false;
	var $register=false;
	var $paypal=false;
	var $paymentcard=4;//visa 13 digit Card

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)
_assertContainsText($PaymentData[4][6], $OCP_CARD_NUMBER);

//create account
//email
_setValue($EDITACCOUNT_EMAIL_TEXTBOX,$email);
//password
_setValue($REGISTER_CREATEACCOUNT_PASSWORD_TEXTBOX,$password);
_setValue($REGISTER_CREATEACCOUNT_CONFIRMPASSWORD_TEXTBOX,$password);

//click on create account button
_click($CREATEACCOUNT_SUBMIT_BUTTON);

//verifying account is created or not
_assert(false,"Account is not creating in ocp page issue with application");

//account create verification
_assertEqual($Shippingaddress_Data[0][1],_getText($MY_ACCOUNT_LOGOUT_TEXT).replace("If this is not ","").replace(" please (sign out)",""));

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


//******************** Register User ************************

//click on login link
_click($LOGIN_LINK);
//login to the application
Login();
//deleting address
DeleteAddress();
//delete card
DeleteCreditCard();

var $t = _testcase("296790","Verify whether order can be placed when credit card radio button is selected as the payment method with Visa credit card number as a registered user.");
$t.start();
try
{
	
	//parameters 
	var $productID=$Cart_Data[0][0];
	var $productQuantity=$Cart_Data[0][1];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=false;
	var $shippinglevelCoupon=false;
	var $percentage=false;
	var $round=false;
	var $BcCard=false;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=false;
	var $intermediatelogin=false;
	var $register=true;
	var $paypal=false;
	var $paymentcard=0;//visa 16 digit

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)
_assertContainsText($PaymentData[0][6], $OCP_CARD_NUMBER);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296795","Verify whether order can be placed when credit card radio button is selected as the payment method with Master credit card number as a registered user.");
$t.start();
try
{
	
	//parameters 
	var $productID=$Cart_Data[0][0];
	var $productQuantity=$Cart_Data[0][1];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=false;
	var $shippinglevelCoupon=false;
	var $percentage=false;
	var $round=false;
	var $BcCard=false;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=false;
	var $intermediatelogin=false;
	var $register=true;
	var $paypal=false;
	var $paymentcard=1;//Master Card

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)
_assertContainsText($PaymentData[1][6], $OCP_CARD_NUMBER);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296796","Verify whether order can be placed when credit card radio button is selected as the payment method with Discover credit card number as a registered user.");
$t.start();
try
{
	
	//parameters 
	var $productID=$Cart_Data[0][0];
	var $productQuantity=$Cart_Data[0][1];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=false;
	var $shippinglevelCoupon=false;
	var $percentage=false;
	var $round=false;
	var $BcCard=false;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=false;
	var $intermediatelogin=false;
	var $register=true;
	var $paypal=false;
	var $paymentcard=3;//Discover Card

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)
_assertContainsText($PaymentData[3][6], $OCP_CARD_NUMBER);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296794","Verify whether order can be placed when credit card radio button is selected as the payment method with AMEX credit card number as a registered user.");
$t.start();
try
{
	
	//parameters 
	var $productID=$Cart_Data[0][0];
	var $productQuantity=$Cart_Data[0][1];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=false;
	var $shippinglevelCoupon=false;
	var $percentage=false;
	var $round=false;
	var $BcCard=false;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=false;
	var $intermediatelogin=false;
	var $register=true;
	var $paypal=false;
	var $paymentcard=2;//Amex Card

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)
_assertContainsText($PaymentData[2][6], $OCP_CARD_NUMBER);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t = _testcase("296527","Verify if the registered user is able to Place Order with 16 digit visa credit card.");
$t.start();
try
{
	
	//deleting address
	DeleteAddress();
	//delete card
	DeleteCreditCard();
	//click on my account
	_click($MY_ACCOUNT_LINK);
	//click on address link
	_click($ADDRESS_ADDRESSES_LINK);
	//click on add new address button
	_click($ADDRESSES_CREATENEW_ADDRESS_LINK);
	//add new address
	AddAddress($AddAddress_Data,0);
	//added address verification
	_assertEqual($AddAddress_Data[0][1],_getText($ADDRESSES_MINIADDRESS_TITLE));
	_assertEqual($AddAddress_Data[0][2]+" "+$AddAddress_Data[0][3],_getText($ADDRESSES_MINIADDRESS_FNAMEANDLNAME));
	//1706 N Nagle Ave Street1 Chicago, IL 60707-4018
	var $Address=$AddAddress_Data[0][4]+" "+$AddAddress_Data[0][5]+" "+$AddAddress_Data[0][8]+", "+$AddAddress_Data[1][7]+" "+$AddAddress_Data[0][9];
	_assertEqual($Address,_getText($ADDRESSES_MINIADDRESS_LOCATION));
	//click on my account
	_click($MY_ACCOUNT_LINK);
	//click on payment settings
	_click($MY_ACCOUNT_PAYMENT_LINK);
	//apyment link in the breadcrumb
	_assertVisible($PAYMENT_LINK);
	//click on add a credit card button
	_click($PAYMENT_ADDCREDITCARD);
	//ADD CREDIT CARD
	AddCreditCard($AddCard_Data,0);
	//click on apply button
	_click($ADDRESSES_OVERLAY_APPLYBUTTON);
    _assertEqual($AddCard_Data[0][6],_getText($PAYMENT_FIRSTCREDITCARD_LIST).replace("Delete Card",""));
    
	//Naviagte to cart with product which is having color as swatch
	navigateToCart($Cart_Data[0][0],$Cart_Data[0][1]);
	//click on checkout button
	_click($CART_CHECKOUTBUTTON);
	//entering shipping address
	if (_isVisible($CHECKOUT_SHIPPINGMETHODS_SECTION))
		{
		_click($CHECKOUT_SHIPPINGMETHODS_EDITBUTTON);
		}
	//checkout page
	_assertVisible($CHECKOUT_CHECKOUTPAGE_HEADING);
   //select shipping address
	_assertVisible($CHECKOUT_SHIPPINGADDRESS_HEADING);
	//entering shipping address
	if (_isVisible($CHECKOUT_SHIPPINGADDRESS_LIST))
		{
		//click on continu to payment method button
		_click($CHECKOUT_CONTINUETOSHIPPINGADDRESS_BUTTON);
		}
	//right address details
	addressProceed();
	//shipping method page
	_assertVisible($CHECKOUT_SHIPPINGMETHOD_HEADING);
	//click on continue to payment method button
	_click($CHECKOUT_SHIPPINGMETHOD_CONTINUETOPAYMENT_BUTTON);
	//select paypal as payment method
	var $creditCardRadioButton=(document.getElementsByTagName("INS")[0]);
	_click($creditCardRadioButton);
	//selecting saved address from list 
	_setSelected($CHECKOUT_PAYMENT_SAVEDCARDDROPDOWN, 1);
	//entering cvv number
	_setValue($CHECKOUT_PAYMENT_CVV_TEXTBOX,"123");
	_click($CHECKOUT_CONTINUETOREVIEWORDER_BUTTON);
	_click($CHECKOUT_PLACEORDER_BUTTON);
	_assertVisible($OCP_HEADING);
	_assertEqual($Generic_Data[3][0], _getText($OCP_HEADING));

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296529","Verify if the registered user is able to Place Order with 13digit Credit card.");
$t.start();
try
{
	
	//delete card
	DeleteCreditCard();
	//click on add a credit card button
	_click($PAYMENT_ADDCREDITCARD);
	//ADD CREDIT CARD
	AddCreditCard($AddCard_Data,1);
	//click on apply button
	_click($ADDRESSES_OVERLAY_APPLYBUTTON);
	//Naviagte to cart with product which is having color as swatch
	navigateToCart($Cart_Data[0][0],$Cart_Data[0][1]);
	//click on checkout button
	_click($CART_CHECKOUTBUTTON);
	//entering shipping address
	if (_isVisible($CHECKOUT_SHIPPINGMETHODS_SECTION))
		{
		_click($CHECKOUT_SHIPPINGMETHODS_EDITBUTTON);
		}
	//checkout page
	_assertVisible($CHECKOUT_CHECKOUTPAGE_HEADING);
	//entering shipping address
	if (_isVisible($CHECKOUT_SHIPPINGADDRESS_LIST))
		{
		//click on continu to payment method button
		_click($CHECKOUT_CONTINUETOSHIPPINGADDRESS_BUTTON);
		//select shipping address
		_assertVisible($CHECKOUT_SHIPPINGADDRESS_HEADING);
		}
	else
		{
		//select shipping address
		_assertVisible($CHECKOUT_SHIPPINGADDRESS_HEADING);
		}
	//right address details
	addressProceed();
	//shipping method page
	_assertVisible($CHECKOUT_SHIPPINGMETHOD_HEADING);
	//click on continue to payment method button
	_click($CHECKOUT_SHIPPINGMETHOD_CONTINUETOPAYMENT_BUTTON);
	//select paypal as payment method
	var $creditCardRadioButton=(document.getElementsByTagName("INS")[0]);
	_click($creditCardRadioButton);
	//selecting saved address from list 
	_setSelected($CHECKOUT_PAYMENT_SAVEDCARDDROPDOWN, 1);
	//entering cvv number
	_setValue($CHECKOUT_PAYMENT_CVV_TEXTBOX,"123");
	_click($CHECKOUT_CONTINUETOREVIEWORDER_BUTTON);
	_click($CHECKOUT_PLACEORDER_BUTTON);
	_assertVisible($OCP_HEADING);
	_assertEqual($Generic_Data[3][0], _getText($OCP_HEADING));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


//Log Out from the application
Logout();

//************* Login from ILP page *****************

var $t = _testcase("246064/292287","Verify whether user is able to place order using VISA credit card type as a intermediate login user./[Checkout - Auth: Payment]-Verify the functionality of Review Order button present secure payment section both for desktop and mobile As a Authenticated User.");
$t.start();
try
{
	//Add item to cart
	addItemToCart($Cart_Data[0][0],$Cart_Data[0][1]);
	//click on minicart link
	_click($MINICART_LINK);
	//click on checkout now button in minicart
	_click($MINICART_CHECKOUTNOW_BUTTON);
	//shopping basket page
	_assertVisible($CART_SHOPPING_BASKET_HEADING);
	//click on checkout now button in the cart page
	_click($CART_CHECKOUTBUTTON);
	//login module
	_assertVisible($CART_LOGINMODULE);
	//intermediate login
	InterMediateLogin();
	//checkout page
	_assertVisible($CHECKOUT_CHECKOUTPAGE_HEADING);
   //select shipping address
	_assertVisible($CHECKOUT_SHIPPINGADDRESS_HEADING);
	$Shippingaddress=document.getElementsByTagName("INS")[1];
	_click($Shippingaddress);
	//click on continue to shipping method
	_click($CHECKOUT_CONTINUETOSHIPPINGADDRESS_BUTTON);
	//use this address scetion
	if(_isVisible($SHIPPING_USETHISADDRESS_SECTION))
		{
		  _click($SHIPPING_USETHISADDRESS_SECTION);
		}
	//click on continue to payment method button
	_click($CHECKOUT_SHIPPINGMETHOD_CONTINUETOPAYMENT_BUTTON);
	//payment dropdown visibility
	if (_isVisible($CHECKOUT_PAYMENT_SAVEDCARDDROPDOWN))
		{
		//selecting saved address from list 
		_setSelected($CHECKOUT_PAYMENT_SAVEDCARDDROPDOWN, 1);
		}
	//Payment details
	Paymentdetails($PaymentData,0);
	//click on order review button
	_click($CHECKOUT_PAYMENT_REVIEWORDER_BUTTON);
	//click on place order button
	_click($CHECKOUT_PLACEORDER_BUTTON);
	//Order confirmation page
	_assertVisible($OCP_HEADING);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

//Log Out from the application
Logout();

//************************** Express PayPal guest Related test cases ***************

var $t = _testcase("296141","Verify if the Guest User is able to Place an Order using Express Paypal with the action of modfying cart from Order review Page.");
$t.start();
try
{
	
	//Naviagte to cart with product which is having color as swatch
	navigateToCart($Cart_Data[1][2],$Cart_Data[0][1]);
	//capturing 
	var $colorInCartPage=_getText($CART_COLOR_VARIANCE);
	//paypal button
	_click($CART_EXPRESSPAYPAL_BUTTON);
	_wait(30000);
	//Express paypal login
	paypal($PayPal_Data[0][0],$PayPal_Data[1][0]);
	_wait(5000);
	//select back the ot=riginal window
	_selectWindow();
	//Mini cart link in order review page
	_click($MINICART_LINK);
	//link edit
	_click($CART_EDIT_LINK);
	//capturing color in cart page before changing 
	var $colorInCartPageBeforeClickonEdit=_getText($CART_COLOR_VARIANCE);
	//cart page color should be same 
	_assertEqual($colorInCartPage,$colorInCartPageBeforeClickonEdit);
	//change the color in cart page by clicking on edit link
	_click($CART_EDIT_LINK);
	_click($CART_COLOR_CHANGE_SELECT_IN_DROPDOWN);
	_click($CART_EDIT_UPDATE_BASKET_BUTTON);
	//capturing color in cart page after changing 
	var $colorInCartPageAfterClickonEdit=_getText($CART_COLOR_VARIANCE);
	//color changes should not be equal
	_assertNotEqual($colorInCartPageBeforeClickonEdit,$colorInCartPageAfterClickonEdit);
	//paypal button
	_click($CART_EXPRESSPAYPAL_BUTTON);
	//wait
	_wait(15000);
	//Express paypal login
	paypal($PayPal_Data[0][0],$PayPal_Data[1][0]);
	//capture the color in payment page
	var $colorInOrderReviewPage=_getText($ORDERREVIEW_REVIEWSHOPPING_BASKET_COLOR);
	//Changed color should be same in order review and cart page
	_assertEqual($colorInCartPageAfterClickonEdit,$colorInOrderReviewPage);
	//click on place order button
	_click($CHECKOUT_PLACEORDER_BUTTON);
	//OCP Page
	_assertVisible($OCP_HEADING);
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

//Log Out from the application
Logout();

//************************** PayPal guest Related test cases ***************

var $t = _testcase("296206","Verify if the Guest user is able to Place Order using Paypal as payment method with action of modifying cart .");
$t.start();
try
{
	navigateToShippingPage($Cart_Data[0][0],$Cart_Data[0][1]);
	//checkout heading
	_assertVisible($CHECKOUT_CHECKOUTPAGE_HEADING);
	//shipping address visibility
	if (_isVisible($CHECKOUT_SHIPPINGADDRESS_SECTION))
		{
		//enter shipping address
		ShippingAddress($Shippingaddress_Data,0);
		//click on continue to shipping method
		_click($CHECKOUT_CONTINUETOSHIPPINGADDRESS_BUTTON);
		}
	else if ($CHECKOUT_SHIPPINGMETHOD_CONTINUETOPAYMENT_BUTTON)
		{
		_click($CHECKOUT_SHIPPINGMETHOD_CONTINUETOPAYMENT_BUTTON);
		}
	//shipping address section
	_assertVisible($CHECKOUT_SHIPPINGADDRESS_SECTION);
	//billing address section
	_assertVisible($CHECKOUT_BILLINGADDRESS_SECTION);
	//click on continu to payment method button
	_click($CHECKOUT_SHIPPINGMETHOD_CONTINUETOPAYMENT_BUTTON);
	//select paypal as payment method
	var $PaypalRadioButton=(document.getElementsByTagName("INS")[1]);
	_click($PaypalRadioButton);
	//click on continue to paypal button
	_click($CHECKOUT_CONTINUETOPAYPAL_BUTTON);
	_wait(30000);
	paypal($PayPal_Data[0][0],$PayPal_Data[1][0]);
	_wait(5000);
    //click on minicart icon present in the order review page
	_click($MINICART_LINK);
	//shopping basket page
	_assertVisible($CART_SHOPPING_BASKET_HEADING);
	//update the quanity
	_click(_link($Cart_Data[0][6], _in($CART_QUANTITY_CELL)));
	var $Minicartvalue=_getText($MINICART_TOTALITEMS_NUMBER);
	//click on checkout now button
	_click($CART_CHECKOUTBUTTON);
	
	//wait
	_wait(2000);
	GuestLogin();
	
	//Handling checkout shipping address
	if (!_isVisible($CHECKOUT_SHIPPINGADDRESS_SECTION))
	{
		ShippingAddress($Shippingaddress_Data,0);
		_click($CHECKOUT_CONTINUETOSHIPPINGADDRESS_BUTTON);
	}
	
	//wait
	_wait(5000);
	//shipping address section
	_assertVisible($CHECKOUT_SHIPPINGADDRESS_SECTION);
	//billing address section
	_assertVisible($CHECKOUT_BILLINGADDRESS_SECTION);
	//clicking of place order button
	if (_isVisible($CHECKOUT_PLACEORDER_BUTTON))
		{
		//We have already entred paypal address need not to enter one more time 
		_click($CHECKOUT_PLACEORDER_BUTTON);
		}
		else if ($CHECKOUT_CONTINUETOPAYPAL_BUTTON)
			{
			_click($CHECKOUT_CONTINUETOPAYPAL_BUTTON);
			_wait(30000);
			paypal($PayPal_Data[0][0],$PayPal_Data[1][0]);
			_click($CHECKOUT_PLACEORDER_BUTTON);
			}
			else
				{
				//click on continu to payment method button
				_click($CHECKOUT_SHIPPINGMETHOD_CONTINUETOPAYMENT_BUTTON);
				//if it is going to payment page handling paypal scenarios there
				if($CHECKOUT_PAYPALRADIO_BUTTON.checked)
				{
					_click($CHECKOUT_CONTINUETOPAYPAL_BUTTON);
				}
					else
					{
						var $PaypalRadioButton=(document.getElementsByTagName("INS")[1]);
						_click($PaypalRadioButton);
						_click($CHECKOUT_CONTINUETOPAYPAL_BUTTON);
					}
				_wait(30000);
				paypal($PayPal_Data[0][0],$PayPal_Data[1][0]);
				_click($CHECKOUT_PLACEORDER_BUTTON);
				}
	//checking order confirmation page summary
   _assertVisible($OCP_HEADING);
   _assertEqual($Generic_Data[3][0], _getText($OCP_HEADING));
   _assertEqual($Minicartvalue,_extract(_getText($THNAKYOUPAGE_QUANTITY),"/[: ] (.*)/",true).toString());
   //order Id
   var $OrderID=_getText($THANKYOUPAGE_ORDERID);
   _log($OrderID);
   //paymet paypal
   _assertEqual($Generic_Data[4][0], _getText($THANKYOUPAGE_PAYMENT_TYPE));
   
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

//************************** PayPal Register Related test cases ***************

//click on login link
_click($LOGIN_LINK);
//login to the application
Login();
//deleting address
DeleteAddress();
//delete card
DeleteCreditCard();

var $t = _testcase("316548","Verify if the registered User is able to Place an Order using Express Paypal with the action of modfying cart from Order review Page.");
$t.start();
try
{
	
	//Naviagte to cart with product which is having color as swatch
	navigateToCart($Cart_Data[1][2],$Cart_Data[0][1]);
	//capturing 
	var $colorInCartPage=_getText($CART_COLOR_VARIANCE);
	//paypal button
	_click($CART_EXPRESSPAYPAL_BUTTON);
	_wait(30000);
	//Express paypal login
	paypal($PayPal_Data[0][0],$PayPal_Data[1][0]);
	_wait(5000);
	//select back the ot=riginal window
	_selectWindow();
	//Mini cart link in order review page
	_click($MINICART_LINK);
	//link edit
	_click($CART_EDIT_LINK);
	//capturing color in cart page before changing 
	var $colorInCartPageBeforeClickonEdit=_getText($CART_COLOR_VARIANCE);
	//cart page color should be same 
	_assertEqual($colorInCartPage,$colorInCartPageBeforeClickonEdit);
	//change the color in cart page by clicking on edit link
	_click($CART_EDIT_LINK);
	_click($CART_COLOR_CHANGE_SELECT_IN_DROPDOWN);
	_click($CART_EDIT_UPDATE_BASKET_BUTTON);
	//capturing color in cart page after changing 
	var $colorInCartPageAfterClickonEdit=_getText($CART_COLOR_VARIANCE);
	//color changes should not be equal
	_assertNotEqual($colorInCartPageBeforeClickonEdit,$colorInCartPageAfterClickonEdit);
	//paypal button
	_click($CART_EXPRESSPAYPAL_BUTTON);
	//wait
	_wait(15000);
	//Express paypal login
	paypal($PayPal_Data[0][0],$PayPal_Data[1][0]);
	//capture the color in payment page
	var $colorInOrderReviewPage=_getText($ORDERREVIEW_REVIEWSHOPPING_BASKET_COLOR);
	//Changed color should be same in order review and cart page
	_assertEqual($colorInCartPageAfterClickonEdit,$colorInOrderReviewPage);
	//click on place order button
	_click($CHECKOUT_PLACEORDER_BUTTON);
	//OCP Page
	_assertVisible($OCP_HEADING);
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296493","Verify if the Registered user is able to Place Order using Paypal as payment method with action of modifying cart.");
$t.start();
try
{
	
	navigateToShippingPage($Cart_Data[0][0],$Cart_Data[0][1]);
	//checkout heading
	_assertVisible($CHECKOUT_CHECKOUTPAGE_HEADING);
	//enter shipping address
	ShippingAddress($Shippingaddress_Data,0);
	//click on continue to shipping method
	_click($CHECKOUT_CONTINUETOSHIPPINGADDRESS_BUTTON);
	//shipping address section
	_assertVisible($CHECKOUT_SHIPPINGADDRESS_SECTION);
	//billing address section
	_assertVisible($CHECKOUT_BILLINGADDRESS_SECTION);
	//click on continu to payment method button
	_click($CHECKOUT_SHIPPINGMETHOD_CONTINUETOPAYMENT_BUTTON);
	//select paypal as payment method
	var $PaypalRadioButton=(document.getElementsByTagName("INS")[1]);
	_click($PaypalRadioButton);
	//click on continue to paypal button
	_click($CHECKOUT_CONTINUETOPAYPAL_BUTTON);
	_wait(30000);
	paypal($PayPal_Data[0][0],$PayPal_Data[1][0]);
	_wait(5000);
    //click on minicart icon present in the order review page
	_click($MINICART_LINK);
	//shopping basket page
	_assertVisible($CART_SHOPPING_BASKET_HEADING);
	//update the quanity
	_click(_link($Cart_Data[0][6], _in($CART_QUANTITY_CELL)));
	var $Minicartvalue=_getText($MINICART_TOTALITEMS_NUMBER);
	//click on checkout now button
	_click($CART_CHECKOUTBUTTON);
	//Handling checkout shipping address
	if (!_isVisible($CHECKOUT_SHIPPINGADDRESS_SECTION))
	{
		ShippingAddress($Shippingaddress_Data,0);
		_click($CHECKOUT_CONTINUETOSHIPPINGADDRESS_BUTTON);
	}
	
	//shipping address section
	_assertVisible($CHECKOUT_SHIPPINGADDRESS_SECTION);
	//billing address section
	_assertVisible($CHECKOUT_BILLINGADDRESS_SECTION);
	//clicking of place order button
	if (_isVisible($CHECKOUT_PLACEORDER_BUTTON))
		{
		//We have already entred paypal address need not to enter one more time 
		_click($CHECKOUT_PLACEORDER_BUTTON);
		}
	else
		{
		//if it is going to payment page handling paypal scenarios there
		if($CHECKOUT_PAYPALRADIO_BUTTON.checked)
		{
			_click($CHECKOUT_CONTINUETOPAYPAL_BUTTON);
		}
			else
			{
				var $PaypalRadioButton=(document.getElementsByTagName("INS")[1]);
				_click($PaypalRadioButton);
				_click($CHECKOUT_CONTINUETOPAYPAL_BUTTON);
			}
		}
	//checking order confirmation page summary
   _assertVisible($OCP_HEADING);
   _assertEqual($Generic_Data[3][0], _getText($OCP_HEADING));
   _assertEqual($Minicartvalue,_extract(_getText($THNAKYOUPAGE_QUANTITY),"/[: ] (.*)/",true).toString());
   //order Id
   var $OrderID=_getText($THANKYOUPAGE_ORDERID);
   _log($OrderID);
   //paymet paypal
   _assertEqual($Generic_Data[4][0], _getText($THANKYOUPAGE_PAYMENT_TYPE));
   
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

/*var $t = _testcase(" "," ");
$t.start();
try
{

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();*/
