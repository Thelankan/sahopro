_include("../GenericLibrary/GlobalFunctions.js");
_resource("SanityData.xls");

//delete user
deleteUser();
deleteProfileUser($Editaccount_Data[2][9]);

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

var $t = _testcase("296517","Verify if a guest user can create an account in the application");
$t.start();
try
{
	
	//click on login link
	_click($LOGIN_LINK);
	//click on create account button
	_click($CREATEACCOUNTNOW_BUTTON);
	//enter all create account details
	createAccount();
	//
	_wait(20000,(_isVisible($MY_ACCOUNT_OPTIONS)));
	if(_isVisible($CREATEACCOUNT_VERIFYEMAILADDRESS_DIALOG))
	{
	_click($CREATEACCOUNT_USETHISADDRESS_BUTTON);
	}
	//account create verification
	_assertEqual($userData[$user][1],_getText($MY_ACCOUNT_LOGOUT_TEXT).replace("If this is not ","").replace(" please (sign out)",""));
	
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

Logout();

var $t = _testcase("213395 ","Verify user is able to Sign In to the application.");
$t.start();
try
{
	//click on login link
	_click($LOGIN_LINK);
	//my account login  section
	_assertVisible($LOGIN_HEADING);
	//login to the application
	Login();
	_assertVisible($LOGIN_BREADCRUMB_MYACCOUNT);
	_assertVisible($MY_ACCOUNT_OPTIONS);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

DeleteCreditCard();
clearWishList();

var $t = _testcase("213398","Verify user is able to add Credit Card details.");
$t.start();
try
{
	//clikc on payment setting link in left navigation
	_click($LEFTNAV_PAYMENTSETTINGS_LINK);
	//apyment link in the breadcrumb
	_assertVisible($PAYMENT_LINK);
	//click on add a credit card button
	_click($PAYMENT_ADDCREDITCARD);
	//ADD CREDIT CARD
	AddCreditCard($AddCard_Data,0);
	//click on apply button
	_click($ADDRESSES_OVERLAY_APPLYBUTTON);
    _assertEqual($AddCard_Data[0][6],_getText($PAYMENT_FIRSTCREDITCARD_LIST).replace("Delete Card",""));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("213400","Verify user is able to check the order history.");
$t.start();
try
{
	//click on myaccount link in breadcrumb scetion
	_click($BREADCRUMB_MYACCOUNT_LINK);
	//click on order history link
	_click($LEFTNAV_ORDERHISTORY_LINK);
	//start date
	_setValue($OREDRS_STARTDATA_TEXTBOX,$Generic_Data[0][0]);
	//end date
	_setValue($ORDERS_ENDDATA_TEXTBOX,$Generic_Data[0][1]);
	//click on searchbutton
	_click($OREDRS_SEARCH_BUTTON);
	//oredr detaiuls
	_assertVisible($OREDRHISTORY_ORDERLIST_DETAILS);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t = _testcase("316544","Verify if the registered user can add an address in the application");
$t.start();
try
{

	//click on my account link
	_click($MY_ACCOUNT_LINK);
	//click on my account
	_click($LOGIN_BREADCRUMB_MYACCOUNT);
	//click on address link
	_click($ADDRESS_ADDRESSES_LINK);
	//deleting address 
	DeleteAddress();
	//click on add new address button
	_click($ADDRESSES_CREATENEW_ADDRESS_LINK);
	//add new address
	AddAddress($AddAddress_Data,0);
	//added address verification
	_assertEqual($AddAddress_Data[0][1],_getText($ADDRESSES_MINIADDRESS_TITLE));
	_assertEqual($AddAddress_Data[0][2]+" "+$AddAddress_Data[0][3],_getText($ADDRESSES_MINIADDRESS_FNAMEANDLNAME));
	_assertEqual($AddAddress_Data[0][4]+" "+$AddAddress_Data[0][5]+" "+$AddAddress_Data[0][8]+", "+$AddAddress_Data[1][7]+" "+$AddAddress_Data[0][9], _getText(_div("mini-address-location", _in(_listItem("top")))));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296514","Verify if the registered user can edit a saved address in the application");
$t.start();
try
{
	
   //click on edit address link
	_click($ADDRESSES_EDIT_LINK);
	//edit address
	AddAddress($AddAddress_Data,2);
	//edited address verification
	//added address verification
	_assertEqual($AddAddress_Data[2][1],_getText($ADDRESSES_MINIADDRESS_TITLE));
	_assertEqual($AddAddress_Data[2][2]+" "+$AddAddress_Data[2][3],_getText($ADDRESSES_MINIADDRESS_FNAMEANDLNAME));
	_assertEqual($AddAddress_Data[2][4]+" "+$AddAddress_Data[2][5]+" "+$AddAddress_Data[2][8]+", "+$AddAddress_Data[1][7]+" "+$AddAddress_Data[2][9], _getText(_div("mini-address-location", _in(_listItem("top")))));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();	

var $t = _testcase("296515","Verify if the registered user can delete a saved address in the application");
$t.start();
try
{
	//click on my account
	_click($BREADCRUMB_MYACCOUNT_LINK);
	//delete payment cards
	DeleteCreditCard();
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();	

var $t = _testcase("296518","Verify the functionality related to 'Apply' button of change name / email section in Edit Account page of the application");
$t.start();
try
{
	
	//click on my account link
	_click($BREADCRUMB_MYACCOUNT_LINK);
	//click on account info link in left navigation
	_click($LEFTNAV_ACCOUNTSETTINGS_LINK);
	//edit account page
	_assertVisible($EDITACCOUNT_EDITACCOUNT_HEADING);
	EditAccount($Editaccount_Data,0);
	//editted address verification
	_assertEqual($Editaccount_Data[0][1],_getText($MY_ACCOUNT_LOGOUT_TEXT).replace("If this is not ","").replace(" please (sign out)",""));
	//click on account info link in left navigation
	_click($LEFTNAV_ACCOUNTSETTINGS_LINK);
	//edit account
	_setValue($EDITACCOUNT_FIRSTNAME_TEXTBOX,$User[$user][1]);
	_setValue($EDITACCOUNT_LASTNAME_TEXTBOX,$User[$user][2]);
	_setValue($EDITACCOUNT_EMAIL_TEXTBOX,$User[$user][9]);
	_setValue($EDITACCOUNT_CONFIRM_EMAIL_TEXTBOX,$User[$user][10]);
	//reset with the original values
	_click($CREATEACCOUNT_SUBMIT_BUTTON);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();	

var $t = _testcase("296519","Verify the functionality related to 'Change Password' button of change password section in Edit Account page of the application");
$t.start();
try
{
	
	//click on account info link in left navigation
	_click($LEFTNAV_ACCOUNTSETTINGS_LINK);
	//click on change password button
	_click($EDITACCOUNT_CHANGEPASSWORD_BUTTON);
	//update password popup
	_assertVisible($EDITACCOUNT_UPDATEPASSWORD_POPUP);
	 //change password fields
	_setValue($EDITACCOUNT_CURRENTPASSWORD_TEXTBOX,$User[$user][11]);
	_setValue($EDITACCOUNT_NEWPASSWORD_TEXTBOX,$updatepasswor_Data[0][1]);
	_setValue($EDITACCOUNT_CONFIRMPASSWORD_TEXTBOX,$updatepasswor_Data[0][2]);
	_click($UPDATEPASSWOR_BUTTON, _in($EDITACCOUNT_UPDATEPASSWORD_POPUP));
	//Edit account page
	_assertVisible($EDITACCOUNT_EDITACCOUNT_HEADING);
     //click on sign out link
	_click($LEFTNAV_LOGOUT_LINK);
	Login();
	_assertEqual($Generic_Data[2][0], _getText($LOGIN_ERRORMEASSAGE));
	_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$User[$user][9]);
	_setValue($LOGIN_PASSWORD_TEXTBOX,$updatepasswor_Data[0][1]);
	_click($LOGIN_BUTTON);
	//click on account information
	_click($LEFTNAV_ACCOUNTSETTINGS_LINK);
	//click on change password button
	_click($EDITACCOUNT_CHANGEPASSWORD_BUTTON);
	//update password popup
	_assertVisible($EDITACCOUNT_UPDATEPASSWORD_POPUP);
    //change password fields
	_setValue($EDITACCOUNT_CURRENTPASSWORD_TEXTBOX,$updatepasswor_Data[0][2]);
	_setValue($EDITACCOUNT_NEWPASSWORD_TEXTBOX,$User[$user][11]);
	_setValue($EDITACCOUNT_CONFIRMPASSWORD_TEXTBOX,$User[$user][11]);
	_click($UPDATEPASSWOR_BUTTON, _in($EDITACCOUNT_UPDATEPASSWORD_POPUP));
	  //click on sign out link
	_click($LEFTNAV_LOGOUT_LINK);
	 Login();
	 //My account page
	 _assertVisible($MY_ACCOUNT_OPTIONS);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();	

//log out
Logout();

/*var $t = _testcase("213401/246391/296522/296528/296540/246391","Verify user is able to search products by giving first name, last name and mail address in Wishlist./Verify the functionality of Add to Basket button when user navigates through wishlist." +
		"Verify the functionality related to Make This Public button in wish list page of the application as a registered user/Verify the edit details functionality for a product in wish list page of the application as a registered user");
$t.start();
try
{
	
	//click on login link
	_click($LOGIN_LINK);
	//click on create account button
	_click($CREATEACCOUNTNOW_BUTTON);
	//create account and add a product to wishlist
	createAccountProfile($Editaccount_Data,2);
	//add product to wishlist and make it as public
	addToWishlist($Generic_Data[0][2],$Generic_Data[0][3],$Generic_Data[0][4]);
	//make this product as public
	if (_isVisible($WISHLIST_SETTHISPUBLIC_BUTTON))
	{
	_click($WISHLIST_SETTHISPUBLIC_BUTTON);	
	}
	//comparing PDP and wishlist product name is equal or not
	_assertEqual($proName,_getText($WISHLIST_PRODUCTNAME));
	//Quantity updating
	$WishListProdQty=_getValue($PDP_QUANTITY_TEXTBOX);
	_setValue($PDP_QUANTITY_TEXTBOX, $Cart_Data[2][1]);
	//click update button
	_click($WISHLIST_UPDATE_BUTTON);
	_assertEqual($Cart_Data[2][1], _getValue($PDP_QUANTITY_TEXTBOX));
	//click on add to basket button
	_click($WISHLIST_ADDTOCART_BUTTON);
	//Mini cart link in order review page
	_click($MINICART_LINK);
	_assertEqual($proName,_getText($CART_NAME_LINK));
	//log out from old user
	Logout();
	//click on login link
	_click($LOGIN_LINK);
	//login from new user
	Login();
	//my account link
	_click($MY_ACCOUNT_LINK);
	//go to wish list 
	_click($WISHLIST_LINK);
	//invalid details
	//enter invalid data in all the fields
	_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $Editaccount_Data[2][14]);
	_setValue($WISHLIST_LASTNAME_TEXTBOX, $Editaccount_Data[2][15]);
	_setValue($WISHLIST_LASTNAME_TEXTBOX, $Editaccount_Data[2][16]);
	_click($WISHLIST_FIND_BUTTON);
	//invalid message should display
	_assertVisible(_paragraph("No Wish List has been found for "+$Editaccount_Data[2][14]+" "+$Editaccount_Data[2][16]+" "+", please try again above."));
	//product already we made as public
	//enter name in firstname textbox
	_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $Editaccount_Data[2][1]);
	//product should return
	_assertVisible($WISHLIST_RESULT_TABLE);
	_assertVisible($WISHLIST_FIRSTNAME_HEADING);
	_assertVisible($WISHLIST_TABLE_VIEW_LINK);
	_assertEqual($Editaccount_Data[2][1],_getText($WISHLIST_FIRSTNAME_TEXT));
	//lastname 
	_setValue($WISHLIST_LASTNAME_TEXTBOX, $Editaccount_Data[2][2]);
	//product should return
	_assertVisible($WISHLIST_RESULT_TABLE);
	_assertVisible($WISHLIST_LASTNAME_HEADING);
	_assertVisible($WISHLIST_TABLE_VIEW_LINK);
	_assertEqual($Editaccount_Data[2][2],_getText($WISHLIST_LASTNAME_TEXT));
	//email id 
	_setValue($WISHLIST_LASTNAME_TEXTBOX, $Editaccount_Data[2][9]);
	//product should return
	_assertVisible($WISHLIST_RESULT_TABLE);
	_assertVisible($WISHLIST_TABLE_VIEW_LINK);
	_assertVisible($WISHLIST_LASTNAME_HEADING);
	_assertVisible($WISHLIST_FIRSTNAME_HEADING);
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

//logout from the current user
Logout();

var $t = _testcase("296523","Verify the functionality related to Make This List private button in wish list page of the application as a registered user");
$t.start();
try
{
	//click on login link
	_click($LOGIN_LINK);
	//enter with dummy account details
	_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$Editaccount_Data[2][9]);
	_setValue($LOGIN_PASSWORD_TEXTBOX,$Editaccount_Data[2][11]);
    //click on sign in button
	_click($LOGIN_BUTTON);
	//my account link
	_click($MY_ACCOUNT_LINK);
	//go to wish list 
	_click($WISHLIST_LINK);
	//make the product as private
	if (_isVisible($WISHLIST_SETTHISPRIVATE_BUTTON))
		{
		_click($WISHLIST_SETTHISPRIVATE_BUTTON);
		}
	//log out from old user
	Logout();
	//click on login link
	_click($LOGIN_LINK);
	//login from new user
	Login();
	//my account link
	_click($MY_ACCOUNT_LINK);
	//go to wish list 
	_click($WISHLIST_LINK);
	//invalid details
	//enter valid data in all the fields
	_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $Editaccount_Data[2][1]);
	_setValue($WISHLIST_LASTNAME_TEXTBOX, $Editaccount_Data[2][2]);
	_setValue($WISHLIST_LASTNAME_TEXTBOX, $Editaccount_Data[2][9]);
	//invalid message should display
	_assertVisible(_paragraph("No Wish List has been found for "+$Editaccount_Data[2][1]+" "+$Editaccount_Data[2][9]+" "+", please try again above."));

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296521","Verify the remove functionality for a product in wishlist page of the application as a registered user.");
$t.start();
try
{
	
	//add product to wishlist and make it as public
	addToWishlist($Generic_Data[0][2],$Generic_Data[0][3],$Generic_Data[0][4]);
	//count of products in wishlist
	var $countOfProducts=CountOfWishlistProducts()
	
	if ($countOfProducts==1)
		{
		//click on remove link
		_click($WISHLIST_REMOVE_BUTTON);
		//no products are in wish list message should display
		_assertEqual("You have no items on your wish list.", _getText($WISHLIST_EMPTYWISHLIST_TEXT));
		}
	else
		{
		//click on remove link
		_click($WISHLIST_REMOVE_BUTTON);
		//count of products in wishlist
		var $countOfProductsAfterDeleting=CountOfWishlistProducts()
		//comparing
		_assertEqual($countOfProductsAfterDeleting,$countOfProducts-1);
		}

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();*/
