_include("../GenericLibrary/GlobalFunctions.js");
_resource("SanityData.xls");

//delete user
deleteUser();

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

var $t = _testcase("291935/213408","[Basket]:Verify the functionality of Checkout Now button in Basket/Verify the application behaviour on click of Checkout button in cart page as a guest user.");
$t.start();
try
{

	addItemToCart($Cart_Data[0][0],$Cart_Data[0][1]);
	//click on checkout now button
	_click($CART_BASKETCONFIRMATION_CHECKOUTNOWBUTTON);
	//click on checkout button in basket page
	_click($CART_CHECKOUTBUTTON);
	//login module
	_assertVisible($CART_LOGINMODULE);
	//click on close
	_click($CART_ILP_POPUP_CLOSE_BUTTON);
	//login module should not visible
	_assertNotVisible($CART_LOGINMODULE);
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("291932","[Basket]: Verify the functionality of Remove link in Basket Page.");
$t.start();
try
{
	
	ClearCartItems();
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("291945","[Basket - Edit Item Modal]:Verify the functionality of Update Basket button in Edit Item modal window.");
$t.start();
try
{
	
	search($Cart_Data[0][5]);
	//navigate to PDP
	if (_isVisible($PLP_SEARCH_RESULT_ITEMS))
		{
		_click($PLP_NAMELINK);
		}
	//click on add to basket
	_click($PDP_ADDTOCART_BUTTON);
	//closing all the popups
	closingPopups();
	//click on checkout now button
	_click($CART_BASKETCONFIRMATION_CHECKOUTNOWBUTTON);
	//Basket page
	_assertVisible($CART_SHOPPING_BASKET_HEADING);
	//click on edit link of the product
    _click($CART_EDITITEM_LINK);
 	//edit modal window
    _assertVisible($CART_EDITMODALWINDOW);
	//click on qty dropdown
    _click($CART_QUANTITYUPDATE_DROPDOWN);
	//select qty
    _click($CART_QUANTITYNUMBERS_TWO);
    //click on update basket button
    _click($PDP_ADDTOCART_BUTTON);
    //updated cart value
    _assertNotEqual($Cart_Data[0][1],_getText($MINICART_TOTALITEMS_NUMBER));
    
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("292385","[Basket - Shipping Dropdown]:Verify the functionality of different shipping methods in shipping page");
$t.start();
try
{
	var $ESTIMATEDSUBTOTALPRICE=_extract(_getText($CART_ESTIMATED_TOTAL),"/[$](.*)/",true).toString();
	//select the shipping method from dropdown
	_click($CART_SHIPPINGTYPE_DROPDOWN);
	//select any shipping methods
	_click($CART_SHIPPINGMETHODDROPDOWN_VALUES);	
	//Updated estimated total
	var $UPDATEDESTIMATEDSUBTOTALPRICE=_extract(_getText($CART_ESTIMATED_TOTAL),"/[$](.*)/",true).toString();
	_assertNotEqual($ESTIMATEDSUBTOTALPRICE,$UPDATEDESTIMATEDSUBTOTALPRICE);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

//clear car titems
ClearCartItems();

var $t = _testcase("294641","MiniCart Guest: Verify the navigation of Checkout button in minicart as a guest user.");
$t.start();
try
{
	addItemToCart($Cart_Data[0][0],$Cart_Data[0][1]);
	//click on Minicart Link
    _click($MINICART_LINK);
	//click on checkout button present in the minicart
    _click($MINICART_CHECKOUTNOW_BUTTON);
	//Basket Page
    _assertVisible($CART_SHOPPING_BASKET_HEADING);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("291938","[ Basket - Quantity Dropdown and Updates]:Verify the functionality of Quantity update by using Qty drop down.");
$t.start();
try
{
	
	var $priceForOneQuantity=parseFloat(_extract(_getText($CART_SUBTOTAL),"/[$](.*)/",true));
	//update the quanity
	_click(_link($Cart_Data[0][6], _in($CART_QUANTITY_CELL)));
	//capture the price for two quantities should update automatically
	var $priceForTwoQuantity=parseFloat(_extract(_getText($CART_SUBTOTAL),"/[$](.*)/",true));
	_assertEqual($priceForTwoQuantity,($priceForOneQuantity)*2);
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296333","Verify if user is able to Place order after creating an account in cart  Page.");
$t.start();
try
{
	
	//click on register link and create account
	_click($LOGIN_LINK);
	//click on create account button
	_click($CREATEACCOUNTNOW_BUTTON);
	//enter all create account details
	createAccount();
	//create account text box
	if(_isVisible($CREATEACCOUNT_VERIFYEMAILADDRESS_DIALOG))
	{
	_click($CREATEACCOUNT_USETHISADDRESS_BUTTON);
	}
	//account create verification
	_assertEqual($userData[$user][1],_getText($MY_ACCOUNT_LOGOUT_TEXT).replace("If this is not ","").replace(" please (sign out)",""));
	//clcik on 
	if (_isVisible($SHIPPING_RIGHT_DETAILS_SECTION))
		{
		_click($CHECKOUT_USETHISADDRESS_BUTTON);
		}
	//should navigate to account landing page
	_assertVisible($MY_ACCOUNT_HEADING);
	_assertVisible($MY_ACCOUNT_OPTIONS);
	//click on mini cart link
	_click($MINICART_LINK);
	//click on checkout button
	_click($CART_CHECKOUTBUTTON);
	//entering shipping address
	if (_isVisible($CHECKOUT_SHIPPINGMETHODS_SECTION))
		{
		_click($CHECKOUT_SHIPPINGMETHODS_EDITBUTTON);
		}
	//checkout page
	_assertVisible($CHECKOUT_CHECKOUTPAGE_HEADING);
   //select shipping address
	_assertVisible($CHECKOUT_SHIPPINGADDRESS_HEADING);
	//Navigate to payment page
	ShippingAddress($Shippingaddress_Data,0);
	//click on continu to payment method button
	_click($CHECKOUT_CONTINUETOSHIPPINGADDRESS_BUTTON);
	//right address details
	addressProceed();
	//shipping method page
	_assertVisible($CHECKOUT_SHIPPINGMETHOD_HEADING);
	//click on continue to payment method button
	_click($CHECKOUT_SHIPPINGMETHOD_CONTINUETOPAYMENT_BUTTON);
	//select paypal as payment method
	var $creditCardRadioButton=(document.getElementsByTagName("INS")[0]);
	_click($creditCardRadioButton);
	//payment details with card
	Paymentdetails($PaymentData,1);
	_click($CHECKOUT_CONTINUETOREVIEWORDER_BUTTON);
	_click($CHECKOUT_PLACEORDER_BUTTON);
	_assertVisible($OCP_HEADING);
	_assertEqual($Generic_Data[3][0], _getText($OCP_HEADING));
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

//logout link
Logout();

var $t = _testcase("296358","Verify if the user can place an order after logging in from cart page.");
$t.start();
try
{
	
	//Naviagte to cart with product quantity as one
	navigateToCart($Cart_Data[0][0],$Cart_Data[0][1]);
	//click on register link and create account
	_click($LOGIN_LINK);
	//login
	Login();
	//account create verification
	_assertEqual($User[$user][1],_getText($MY_ACCOUNT_FIRSTNAME_TEXT).replace("If this is not ","").replace(" please (sign out)",""));
	//clcik on 
	if (_isVisible($SHIPPING_RIGHT_DETAILS_SECTION))
		{
		_click($CHECKOUT_USETHISADDRESS_BUTTON);
		}
	
	//should navigate to account landing page
	_assertVisible($MY_ACCOUNT_HEADING);
	_assertVisible($MY_ACCOUNT_OPTIONS);
	//click on mini cart link
	_click($MINICART_LINK);
	//click on checkout button
	_click($CART_CHECKOUTBUTTON);
	//entering shipping address
	if (_isVisible($CHECKOUT_SHIPPINGMETHODS_SECTION))
		{
		_click($CHECKOUT_SHIPPINGMETHODS_EDITBUTTON);
		}
	//checkout page
	_assertVisible($CHECKOUT_CHECKOUTPAGE_HEADING);
   //select shipping address
	_assertVisible($CHECKOUT_SHIPPINGADDRESS_HEADING);
	//Navigate to payment page
	ShippingAddress($Shippingaddress_Data,0);
	//click on continu to payment method button
	_click($CHECKOUT_CONTINUETOSHIPPINGADDRESS_BUTTON);
	//right address details
	addressProceed();
	//shipping method page
	_assertVisible($CHECKOUT_SHIPPINGMETHOD_HEADING);
	//click on continue to payment method button
	_click($CHECKOUT_SHIPPINGMETHOD_CONTINUETOPAYMENT_BUTTON);
	//select paypal as payment method
	var $creditCardRadioButton=(document.getElementsByTagName("INS")[0]);
	_click($creditCardRadioButton);
	//payment details with card
	Paymentdetails($PaymentData,1);
	_click($CHECKOUT_CONTINUETOREVIEWORDER_BUTTON);
	_click($CHECKOUT_PLACEORDER_BUTTON);
	_assertVisible($OCP_HEADING);
	_assertEqual($Generic_Data[3][0], _getText($OCP_HEADING));
		
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

/*var $t = _testcase(" "," ");
$t.start();
try
{

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();*/

