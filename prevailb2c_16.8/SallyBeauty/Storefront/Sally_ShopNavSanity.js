_include("../GenericLibrary/GlobalFunctions.js");
_resource("SanityData.xls");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

var $t = _testcase("296217","Verify the functionality of Bread crumbs in search results page");
$t.start();
try
{
	
	//search for a product
	_setValue($SEARCH_TEXTBOX, $Cart_Data[2][0]);
	//click on search sugession
    _click($SEARCH_SUGESSIONS);
	//search results
    //_assertEqual($Generic_Data[1][0]+" \""+$Cart_Data[2][0]+"\"",_getText(_span("breadcrumb-result-text")));
    //select any filter
    _click($LEFTNAV_PRICEREFINEMENT_LINK);
    _click($LEFTNAV_PRICEFILTER_LINK);
	$FilterValue=_extract(_getText($LEFTNAV_PRICEFILTER_LINK),"/(.*)[(]/",true).toString();
	$SeletedPrice=_getText($SEARCH_BREADCRUMB_LASTVALUE);
	_assertVisible($SEARCH_BREADCRUMB_LASTVALUE);
    _assertEqual($FilterValue,$SeletedPrice);
    _click($SEARCHRESULT_FILTER_REMOVEIMAGE);
    _assertNotVisible($SEARCH_BREADCRUMB_LASTVALUE);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("297890/291918/294071","Add a product to cart from PLP/[Basket Confirmation]:Verify the functionality of Checkout Now button in the Basket confirmation modal window./[Basket Confirmation] : Verify the navigation to Basket confirmation prompt");
$t.start();
try
{
	//search a product
	search($Cart_Data[1][0]);
	//first product name in PLP
	var $productNameInPlp=_getText($PLP_PRODUCT_NAME);
	//navigate to PDP
	_click($PLP_ADDTOCART_BUTTON);
	//Product should add to cart confirmation prompt should display
	_assertVisible($CART_BASKETCONFIRMATION_POPUP);
	//1 item is added to cart
	_assertVisible($CART_ITEMADDED_HEADING);
	//closing the unwanted pop up's
	closingPopups();
	//click on check out button in confirmation model
	_click($CART_BASKETCONFIRMATION_CHECKOUTNOWBUTTON);
	//shopping cart heading 
	_assertVisible($CART_HEADING);
	var $productNameInCart=_getText($CART_NAME_LINK);
	_assertEqual($productNameInPlp,$productNameInCart);
	//hover on minicart link
	_mouseOver($MINICART_LINK);
	//product name in mini cart 
	var $productNameInMiniCart=_getText($MINICART_NAME_SECTION);
	_assertEqual($productNameInPlp,$productNameInMiniCart);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

//clear cart
ClearCartItems();

var $t = _testcase("297889","Add a product to cart by changing the variant on PDP");
$t.start();
try
{
	
	//search a product
	search($Cart_Data[1][4]);
	//click on name link
	if (_isVisible($PLP_NAMELINK))
		{
		_click($PLP_NAMELINK);
		}
	//change the varient
	_click($PDP_COLOR_DROPDOWN);
	_click($PDP_FIRST_COLOR_NAME_INDROPDOWN);
	//color link
	var $colorSelectedInDropDown=_getText($PDP_COLOR_DROPDOWN);
	_click($PDP_ADDTOCART_BUTTON);
	_wait(4000);
	//closing popups
	closingPopups();
	//click on mini cart link
	_click($MINICART_LINK);
	//color swatch in cart page
	var $colorSwatchInCartPage=_getText($CART_COLOR_VARIANCE);
	_assertEqual($colorSelectedInDropDown,$colorSwatchInCartPage);
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("213421","Verify whether filter get clear when user click on selected filter value in the application both as an anonymous and a registered user");
$t.start();
try
{
	
	//navigate to subcategory page
	navigateSubCatPage();
	
	var $colors = CountOfColorSwatches();
	var $unSelectable=CountOfColorSwatchesUnSelectable();
	for(var $k=0; $k<$colors.length; $k++)
	{
		if($unSelectable.indexOf($colors[$k])>=0)
			{
			_assert(true, "Swatch is unselectable");
			}
		else
			{
			 _click(_link($colors[$k]));
			  //clear link
			  _assertVisible($PLP_CLEARCOLOR_LINK);
			  _assertEqual($colors[$k]+" x",_getText($SEARCH_BREADCRUMB_LASTVALUE));
			 _click($PLP_BREADCRUMB_CANCEL_BUTTON);
			 _wait(4000,!_isVisible($PLP_BREADCRUMB_CANCEL_BUTTON));
			}  
	}
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("213423", "Verify the system's response on selection of any sort by value in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	if(_isVisible($PLP_SORT_HEADER) && _isVisible($PLP_SORT_FOOTER))
	{
		var $sort=_getText($PLP_SORT_HEADER);
		var $j=0;
		for(var $i=1;$i<$sort.length;$i++)
		{
			_setSelected($PLP_SORT_HEADER, $sort[$i]);
			//Validating the selection
			_assertEqual($sort[$i],_getSelectedText($PLP_SORT_HEADER),"Selected option is displaying properly");
			_assertEqual($sort[$i],_getSelectedText($PLP_SORT_FOOTER),"Selected option is displaying properly");
			if($sort.indexOf($Generic_Data[$j][6])>-1)
			{
				_setSelected($PLP_SORT_HEADER,$Generic_Data[$j][6]);
				sortBy($Generic_Data[$j][6]);
				$j++;
			}
		}
	}
	else
	{
		_assert(false, "sort by drop down is not present");
	}
	_click(_image("logo.png"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


/*var $t = _testcase(" "," ");
$t.start();
try
{

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();*/