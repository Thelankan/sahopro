_include("../GenericLibrary/GlobalFunctions.js");
_resource("SanityData.xls");

//delete user
deleteUser();

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//************************ Guest User **************************

var $t = _testcase("296324","Verify if the user can place an order through Credit cart by applying a order level promotion.");
$t.start();
try
{
	

	//parameters 
	var $productID=$couponCode_data[2][1];
	var $productQuantity=$couponCode_data[2][3];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=$couponCode_data[1][2];
	var $shippinglevelCoupon=false;
	var $percentage=10;
	var $round=2;
	var $BcCard=false;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=true;
	var $intermediatelogin=false;
	var $register=false;
	var $paypal=false;
	var $paymentcard=2;

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)


}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296221","Verify the application behavior when an order is placed with product Level coupon + Beauty Club Card and order is placed using Paypal.");
$t.start();
try
{

	//parameters 
	var $productID=$couponCode_data[1][1];
	var $productQuantity=$couponCode_data[1][3];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=$couponCode_data[1][2];
	var $shippinglevelCoupon=false;
	var $priceCalculation=true;
	var $percentage=10;
	var $round=2;
	var $BcCard=true;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=true;
	var $intermediatelogin=false;
	var $register=false;
	var $paypal=false;
	var $paymentcard=0;

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$priceCalculation,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)

	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296305","Verify the application behavior when a order is placed with Order Level coupon and order is completed using Credit card as a guest user.");
$t.start();
try
{
	

	//parameters 
	var $productID=$couponCode_data[2][1];
	var $productQuantity=$couponCode_data[2][3];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=$couponCode_data[1][2];
	var $shippinglevelCoupon=false;
	var $percentage=10;
	var $round=2;
	var $BcCard=false;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=true;
	var $intermediatelogin=false;
	var $register=false;
	var $paypal=false;
	var $paymentcard=2;

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)


}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("292384","[Basket - Promo Applied]:Verify the functionality of Remove link in Promo code line item");
$t.start();
try
{
	
	//Navigate to cart with product which is having color as swatch
	navigateToCart($couponCode_data[0][1],$couponCode_data[0][3]);
	//apply the product level coupon
	_setValue($CART_COUPON_TEXTBOX,$couponCode_data[0][2]);
	//click the apply button
	_click($CART_COUPON_APPLY);
	//coupon should apply
	_assertVisible($CART_PROMOTION_REMOVE_BUTTON);
	//delete applied coupon
	while (_isVisible($CART_PROMOTION_REMOVE_BUTTON))
	{
	_click($CART_PROMOTION_REMOVE_BUTTON);
	}

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

//************************ Register User **************************
//click on login link
_click($LOGIN_LINK);
//login to the application
Login();
//deleting address
DeleteAddress();

//delete card
DeleteCreditCard();

var $t = _testcase("296495","Verify the application behavior when a order is placed with Order Level coupon and order is completed using Credit card as a Registered user.");
$t.start();
try
{
	
	//parameters 
	var $productID=$couponCode_data[2][1];
	var $productQuantity=$couponCode_data[2][3];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=$couponCode_data[1][2];
	var $shippinglevelCoupon=false;
	var $percentage=10;
	var $round=2;
	var $BcCard=false;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=false;
	var $intermediatelogin=false;
	var $register=true;
	var $paypal=false;
	var $paymentcard=2;

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296496","Verify the application behavior when a order is placed with Shipping Level coupon and order is completed using Credit card as a Registered user");
$t.start();
try
{
	
	//parameters 
	var $productID=$couponCode_data[2][1];
	var $productQuantity=$couponCode_data[2][3];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=false;
	var $shippinglevelCoupon=$couponCode_data[2][2];
	var $percentage=10;
	var $round=2;
	var $BcCard=false;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=false;
	var $intermediatelogin=false;
	var $register=true;
	var $paypal=false;
	var $paymentcard=2;//Amex Card

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296541","Verify the user is able to add the BC Card to cart and Place order using Amex card as a registered user.");
$t.start();
try
{
	
	//parameters 
	var $productID=$Cart_Data[0][0];
	var $productQuantity=$Cart_Data[0][1];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=false;
	var $shippinglevelCoupon=false;
	var $percentage=false;
	var $round=false;
	var $BcCard=true;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=false;
	var $intermediatelogin=false;
	var $register=true;
	var $paypal=false;
	var $paymentcard=2;//Amex Card

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)
_assertContainsText($PaymentData[3][6], $OCP_CARD_NUMBER);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();



//logout from the application
Logout();

//*********************** Paypal Related Scripts ********************

//********************* Paypal Guest User ************************

var $t = _testcase("296319/296326","Verify the application behavior when a Order is placed using Product level promotion with Paypal as Payment.");
$t.start();
try
{
		//parameters 
		var $productID=$couponCode_data[0][1];
		var $productQuantity=$couponCode_data[0][3];
		var $productlevelCoupon=$couponCode_data[0][2];
		var $orderlevelCoupon=false;
		var $shippinglevelCoupon=false;
		var $priceCalculation=true;
		var $percentage=10;
		var $round=2;
		var $BcCard=false;
		var $expresspaypal=false;
		var $normalflow=true;
		var $guest=true;
		var $intermediatelogin=false;
		var $register=false;
		var $paypal=true;
		var $paymentcard=0;

//order placing scenarios		
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$priceCalculation,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296316/296325","Verify the application behavior when a order is placed with Shipping Level coupon and order is completed using Credit card as a guest user");
$t.start();
try
{
	
	//parameters 
	var $productID=$couponCode_data[2][1];
	var $productQuantity=$couponCode_data[2][3];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=false;
	var $shippinglevelCoupon=$couponCode_data[2][2];
	var $percentage=10;
	var $round=2;
	var $BcCard=false;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=true;
	var $intermediatelogin=false;
	var $register=false;
	var $paypal=true;
	var $paymentcard=1;

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t = _testcase("296320","Verify the application behavior when a order is placed with Order Level coupon + Beauty Club Card and order is completed using Paypal as payment.");
$t.start();
try
{
	//parameters 
	var $productID=$couponCode_data[1][1];
	var $productQuantity=$couponCode_data[1][3];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=$couponCode_data[1][2];
	var $shippinglevelCoupon=false;
	var $percentage=10;
	var $round=2;
	var $BcCard=true;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=true;
	var $intermediatelogin=false;
	var $register=false;
	var $paypal=true;
	var $paymentcard=0;

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("296321","Verify the application behavior when a order is placed with Shipping Level coupon + Beauty Club Card and order is completed using Paypal as Payment");
$t.start();
try
{
	
	//parameters 
	var $productID=$couponCode_data[2][1];
	var $productQuantity=$couponCode_data[2][3];
	var $productlevelCoupon=false;
	var $orderlevelCoupon=false;
	var $shippinglevelCoupon=$couponCode_data[2][2];
	var $percentage=10;
	var $round=2;
	var $BcCard=true;
	var $expresspaypal=false;
	var $normalflow=true;
	var $guest=true;
	var $intermediatelogin=false;
	var $register=false;
	var $paypal=true;
	var $paymentcard=0;

//order placing scenarios	
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)


}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

//logout from the application
Logout();
//click on login link
_click($LOGIN_LINK);
//login to the application
Login();
//deleting address
DeleteAddress();
//delete card
DeleteCreditCard();

//*********************** Paypal Register User ****************************
var $t = _testcase("296497","Verify the application behavior when a Order is placed using Product level promotion  with Paypal as Payment as a Registered user.");
$t.start();
try
{
		//parameters 
		var $productID=$couponCode_data[0][1];
		var $productQuantity=$couponCode_data[0][3];
		var $productlevelCoupon=$couponCode_data[0][2];
		var $orderlevelCoupon=false;
		var $shippinglevelCoupon=false;
		var $priceCalculation=true;
		var $percentage=10;
		var $round=2;
		var $BcCard=false;
		var $expresspaypal=false;
		var $normalflow=true;
		var $guest=false;
		var $intermediatelogin=false;
		var $register=true;
		var $paypal=true;
		var $paymentcard=0;

//order placing scenarios		
OrderPlacingScenarios($productID,$productQuantity,$productlevelCoupon,$orderlevelCoupon,$shippinglevelCoupon,$priceCalculation,$percentage,$round,$BcCard,$expresspaypal,$normalflow,$guest,$intermediatelogin,$register,$paypal,$paymentcard)

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

/*var $t = _testcase(" "," ");
$t.start();
try
{

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();*/