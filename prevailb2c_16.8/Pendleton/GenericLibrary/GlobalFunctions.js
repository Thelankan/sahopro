_include("BrowserSpecific.js");
_include("BM_Functions.js");
_resource("User_Credentials.xls");
_includeOnce("../ObjectRepository/Generic_OR.sah");

//business library Includes
_include("../BusinessLibrary/CartBusinessLibrary.js");
_include("../BusinessLibrary/CheckoutBusinessLibrary.js");
_include("../BusinessLibrary/ProfileBusinessLibrary.js");
_include("../BusinessLibrary/ShopNavBusinessLibrary.js");

var $userLog=_readExcelFile("User_Credentials.xls","BM_Login");
var $URL=_readExcelFile("User_Credentials.xls","URL");
var $productQuantity=_readExcelFile("User_Credentials.xls","GenericProducts");
var $GC=_readExcelFile("User_Credentials.xls","GiftCards");


var $UserID=$userLog[0][0];
var $Password=$userLog[0][1];
var $taxService="none";

var $fontfamily;
var $fontsize;
var $color;
var $background;

//Function take a screen shot when a their is a script failure

function onScriptError()
{
  _log("Error log");
  _lockWindow(5000);
  _focusWindow();
  _takePageScreenShot();
  _unlockWindow();
}

function onScriptFailure()
{
  _log("Failure log");
  _lockWindow(5000);
  _focusWindow();
  _takePageScreenShot();
  _unlockWindow();
}

function SiteURLs()
{
	
_navigateTo($URL[0][0]);

//store front authentication 
if(_isVisible(_submit("Authenticate")))
	{
		_setValue(_textbox("authUser"), $URL[0][1]);
		_setValue(_password("authPassword"), $URL[1][1]);
		_click(_submit("Authenticate"));
	}
	
}

function UrlNavigation($providedUrl)
{
	//Url Provided
	_navigateTo($providedUrl);
}

function cleanup()
 {
	//Clearing the items from cart
	ClearCartItems();
	if(isMobile())
		{
			_click(_span("Menu"));
		}
	//logout from the application
	logout();
	_click($HEADER_HOMELOGO);
}

function UIVerification($identifier,$fontfamily,$fontsize,$color,$background)
{

if ($fontfamily)
{
	$fontfamily=_style($identifier,"font-family");
	return $fontfamily;
}
else if($fontsize)
{
	$fontsize=_style($identifier,"font-size");
	return $fontsize;
}
else if($color)
{
	$color=_style($identifier,"font-size");
	return $color;
}
else
{
	$background=_style($identifier,"font-size");
	return $background;
}

}

function getStyles($webElement,$fontSize,$fontFamily,$fontColor,$fontWeight,$underline)
{
       
       var $DataArray=new Array();
       
       if($fontSize == true)
              {
       
                     var $actSize = _style($webElement,"font-size");
                     $DataArray.push($actSize);
       
              }
       else
              {
                     var $actSize = null;
                     $DataArray.push($actSize);
              }
       
       if($fontFamily == true)
              {      
                     var $actFamily = _style($webElement,"font-family");
                     $DataArray.push($actFamily);
              }
       else
              {      
                      var $actFamily = null;
                     $DataArray.push($actFamily);
              }
       
       
       if($fontColor == true)
              {
                     
                     var $actColor = _style($webElement,"color");
                     $DataArray.push($actColor);
              }
       else
              {      
                      var $actColor = null;
                     $DataArray.push($actColor);
              }

       
       if($fontWeight == true)
              {
                     
                     var $actWeight = _style($webElement,"font-weight");
                     $DataArray.push($actWeight);
              }
       else
              {      
                      var $actWeight = null;
                     $DataArray.push($actWeight);
              }
       

       
       if($underline == true)
              {      
                     var $actUnderlined = _style($webElement,"text-decoration");
                     $DataArray.push($actUnderlined);
              }
       else
              {      
                      var $actUnderlined = null;
                     $DataArray.push($actUnderlined);
              }

       return $DataArray;
       _log($DataArray+"  : Colected Array");
}


function verifyStyles($excelSheetName,$expDataRow,$fontSizeColumn,$fontFamilyColumn,$fontColorColumn,$fontWeightColumn,$underlineColumn,$DataArray)
{
       _log($DataArray);
         if($DataArray[0] != null)
              {
                     _assertEqual($excelSheetName[$expDataRow][$fontSizeColumn],$DataArray[0]);
                     _log("Font Size");
              } 
         else
                {
                     _log("Not required");
                }
       
         if($DataArray[1] != null)
              {
                _assertContainsText($excelSheetName[$expDataRow][$fontFamilyColumn],$DataArray[1]);
                     _log("Font Family");
              } 
         else
                {
                     _log("Not required");
                }
         if($DataArray[2] != null)
              {
                _assertEqual($excelSheetName[$expDataRow][$fontColorColumn],$DataArray[2]);
                     _log("Font Color");
              } 
         else
                {
                     _log("Not required");
                }
              
         if($DataArray[3] != null)
              {
                     _assertEqual($excelSheetName[$expDataRow][$fontWeightColumn],$DataArray[3]);
                     _log("Font Weight");
              } 
         else
                {
                     _log("Not required");
                }
                if($DataArray[4] != null)
                {
                       _assertEqual($excelSheetName[$expDataRow][$underlineColumn],$DataArray[4]);
                           _log("Font Underlined");
                } 
                else
                {
                       _log("Not required");
                }
  
}


//Checkout related functions
function round(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0)
      return Math.round(value);

    value = +value;
    exp  = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
      return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
  }



function verifyBorderColor($color, $element) 
{
    _assertEqual($color,_style($element,"border-left-color"));
    _assertEqual($color,_style($element,"border-top-color"));
    _assertEqual($color,_style($element,"border-right-color"));
    _assertEqual($color,_style($element,"border-bottom-color"));
}

function newEmailId() 
{
	var $today = new Date();

	var $date = $today.getFullYear()+"_"+($today.getMonth()+1)+"_"+$today.getDate();
	var $time = $today.getHours() + "_" + $today.getMinutes() + "_" + $today.getSeconds();

	var $dateAndTime = $date+"_"+$time;
	
	_log("$dateTime:-  "+ $dateAndTime);
	var $newEmailId = "test"+$dateAndTime+"@gmail.com";
	_log("$newEmailId:-  "+ $newEmailId);
	return $newEmailId; 
}

function createNewAccount($emailId)
{
	_setValue($REGISTER_FIRSTNAME_TEXTBOX, $userData[$user][1]);
	_setValue($REGISTER_LASTNAME_TEXTBOX, $userData[$user][2]);
	_setValue($REGISTER_EMAIL_TEXTBOX, $emailId);
	_setValue($REGISTER_CONFIRM_EMAIL_TEXTBOX, $emailId);
	_setValue($REGISTER_PASSWORD_TEXTBOX, $userData[$user][5]);
	_setValue($REGISTER_CONFIRM_PASSWORD_TEXTBOX, $userData[$user][6]); 	 
	if(_isVisible($REGISTER_SUBMIT_BUTTON))
	{
		_click($REGISTER_SUBMIT_BUTTON); 
	} 	 
}

