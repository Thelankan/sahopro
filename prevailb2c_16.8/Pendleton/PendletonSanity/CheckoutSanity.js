_include("../GenericLibrary/GlobalFunctions.js");

_log("*** Please Delete card manually Befroe executing the suite because delete card function is not working");

var $color=$ItemILP[0][2];
var $emailID=$uId;

//for deleting user
deleteUser();
_setAccessorIgnoreCase(true); 
SiteURLs();
cleanup();
var $OCP_Address1=$OCP_Address[0][13];

var $savedAddress=$Shipping_addr[0][11]+" "+$Shipping_addr[0][7]+" "+$Shipping_addr[0][1]+" "+$Shipping_addr[0][2]+" "+$Shipping_addr[0][3]+" "+$Shipping_addr[0][4]+" "+$Shipping_addr[0][7]+" "+$Shipping_addr[1][6]+" "+$Shipping_addr[0][8]+" "+$Shipping_addr[0][5]+" "+"Phone: "+$Shipping_addr[0][9]+" "+$Shipping_addr[1][11];


var $t = _testcase("352694","Verify the functionality of 'Create an Account' button in Intemediate Login page under New Customer section for Guest user");
$t.start();
try
{
	
//navigate to cart page
navigateToCartSearch($ItemILP[0][0],$ItemILP[0][1]);
//click on go straight to checkout to navigate to IL Page
_click($CART_CHECKOUT_BUTTON_BOTTOM);
//login create account button
_click($CHECKOUT_LOGINPAGE_CREATEACCOUNTNOW_BUTTON);
//verify the navigation to create account page
_assertVisible($REGISTER_HEADING);
_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
//fill all the details
createAccount();
_assertVisible($SHIPPING_PAGE_HEADER);
//Place an order
shippingAddress($OCP_Address,0);
//check use this for billing check box
_check($SHIPPING_BILLING_ADDRESS_CHECKBOX);
_click($SHIPPING_SUBMIT_BUTTON);
BillingAddress($OCP_Address,0);
//entering Credit card Information
PaymentDetails($Card_Details,0);
//Click on submit button in  billing page
_click($BILLING_CONTINUE_BUTTON);
//click on place order button
_click($BILLING_PAYMENT_PLACE_ORDER_BUTTON);
//verify the navigation to OCP page
_assertVisible($ORDER_CONFIRMATION_HEADING);
_assertEqual($OCP[0][0], _getText(_heading1("/(.*)/", _in(_div("confirmation-message")))));

}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//logout
logout();


for (var $j = 0; $j < 2; $j++) 
{
	if ($j == 1) 
	{
		//Place order as a registered user
		_click($HEADER_LOGIN_LINK);
		login();
	}
	
	
	for (var $i = 0; $i < 4; $i++) 
	{
	
		_log("$i:-  "+ $i);
		_log("******* " +$Card_Details[$i][2]+" *******");
		//var $t=_testcase("125787/125785","Verify the UI & navigation of the 'Order summery' section on Thank you for your order page in a application as a Guest user");
		var $t=_testcase("352646/352649/352652/352655/352656/352658/352662/352665","Verify the UI of the Order Confirmation page in Application as a Anonymous user. / Verify the navigation to Order confirmation page as guest user");
		$t.start();
		try
		{
			$colorBoolean = false;
			$sizeBoolean=false;
			ClearCartItems();
			//navigating to Shipping page
			navigateToCartSearch($Order_Product[0][1],1);
			
			_click($CART_CHECKOUT_BUTTON);
			if (_isVisible($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON)) 
			{
				_log("GUEST USER");
				_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
			} 
			else 
			{
				_log("REGISTERED USER");
			}
			shippingAddress($OCP_Address,0);
			//check use this for billing check box
			_check($SHIPPING_BILLING_ADDRESS_CHECKBOX);
			//Collect the shipping method 
			
			var $radioButtons = _collect("_radio","/(.*)/", _in(_div("shipping-method-list")));
			var $shippingMethodsLabel = _collect("_label","/(.*)/", _in(_div("shipping-method-list")));
			for (var $k = 0; $k < $radioButtons.length; $k++) 
			{
				var $radioButtonStatus = _getAttribute($radioButtons[$k], "checked");
				_log("$radioButtonStatus:-  "+ $radioButtonStatus);
				if ("true" == $radioButtonStatus || true == $radioButtonStatus ) 
				{
					var $temp = _getText($shippingMethodsLabel[$k]);
					var $shippingMethod = $temp.substring(0, $temp.indexOf(":"));
					_log("$shippingMethod:-  "+ $shippingMethod);
					break;
				}
			}
			
			if(isMobile() && !mobile.iPad())
			{
				$shippingTax=_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING),"/[$](.*)/",true).toString();
				$subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_PRICE_MOBILE),"/[$](.*)/",true).toString();
			}
			else
			{
				$shippingTax=_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING),"/[$](.*)/",true).toString();
				$subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_PRICE),"/[$](.*)/",true).toString();
			}
			_click($SHIPPING_SUBMIT_BUTTON);
			//billing address
			BillingAddress($OCP_Address,0); 
			$tax=_extract(_getText($SHIPPING_RIGHT_NAV_SALES_TAX),"/[$](.*)/",true).toString();
			
			//tax displaying
			_log("$tax:-  "+ $tax);
			
			if (!$tax==false || !$tax=="false")
				{
				_log("Sales is not displaying");
				}
			else
				{
				$tax=_extract(_getText($SHIPPING_RIGHT_NAV_SALES_TAX),"/[$](.*)/",true).toString();
				}
			
			//order total
			$Ordertotal=_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/[$](.*)/",true).toString();
			
			//entering Credit card Information
			PaymentDetails($Card_Details,$i);
			
			//unchecking the checkbox
			if (_isVisible($BILLING_PAYMENT_SAVE_CARD_CHECKBOX))
				{
				_uncheck($BILLING_PAYMENT_SAVE_CARD_CHECKBOX);
				}
			
			
			$paymentType="Credit Card";
			_wait(3000,_isVisible($BILLING_CONTINUE_BUTTON));
			_click($BILLING_CONTINUE_BUTTON);
			_wait(10000,_isVisible($BILLING_ADDRESS_SUBMIT));
			
			//Address Verification overlay
	//		addressVerificationOverlay();
			//fetch order total
	//		var $orderTot=_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/[ ].*[ ](.*)/",true);
			var $orderTot=$Ordertotal;
			_log("$orderTot:-  "+ $orderTot);
			//Placing order
			_click($BILLING_PAYMENT_PLACE_ORDER_BUTTON);
			
			if(_isVisible($BILLING_ADDRESS_CORRECTION_CLOSE_LINK))
			{
			_click($BILLING_ADDRESS_CORRECTION_CLOSE_LINK);
			}
			
			//verify the navigation to OCP page
			_assertVisible($ORDER_CONFIRMATION_HEADING);
			_assertEqual($OCP[0][0], _getText(_heading1("/(.*)/", _in(_div("confirmation-message")))));
			//verify the UI
			_assertVisible($ORDER_CONFIRMATION_ORDER_DATE_HEADING);
	//		_assertVisible($ORDER_CONFIRMATION_ORDER_NUMBER);
	//		var $orderID = _getText($ORDER_CONFIRMATION_ORDER_NUMBER);
	//		_log("$orderID:- "+ $orderID);
	//		_assertEqual($OCP[5][0], _getText($ORDER_CONFIRMATION_SECTIOIN_HEADING));
			
			//Payment Section
			_assertEqual("Payment Method", _getText(_div("label", _in(_div("order-payment-instruments")))));
			_assertEqual("Payment Total", _getText(_div("label", _in(_div("order-payment-summary")))));
			_assertEqual("Billing Address", _getText(_div("label", _in(_div("order-billing")))));
			
			//BILLING ADDRESS
			_assertVisible($ORDER_CONFIRMATION_BILLING_DIV);
			//Order Total
			_assertVisible($ORDER_CONFIRMATION_ORDER_TOTAL_SECTIOIN);
			//SHIPPING ADDRESS
			_assertVisible($ORDER_CONFIRMATION_SHIPPING_ADDRESS);
			//Return to Shopping
			_assertEqual($OCP[5][4], _getText($ORDER_CONFIRMATION_RETURN_TO_SHOPPING_LINK));
			
			if ($j == 0) 
			{
				//Create Account section
				_assertEqual($OCP[6][0], _getText($ORDER_CONFIRMATION_CREATE_ACCOUNT_HEADING));
			} 
			else 
			{
				//Create Account section should not display for Registered user
				_assertNotVisible($ORDER_CONFIRMATION_CREATE_ACCOUNT_HEADING);
			}
			
			//Verify the LINE ITEM
			//Product NAme
			_assertVisible(_div("label", _in(_div("line-item-details"))));
			_assertEqual($productNameInPDP, _getText(_div("name", _in(_div("line-item-details")))));
			//SKU
			_assertEqual("Item No.:", _getText(_span("label", _in(_div("line-item-details")))));
			_assertEqual($productSKUinPDP, _getText(_span("value", _in(_div("line-item-details")))));
	
			//Size
			if ($sizeBoolean) 
			{
				_assertEqual("Size:", _getText(_span("label", _in(_div("/Size/", _in(_div("line-item-details")))))));
				_assertEqual($productSizeInPDP, _getText(_span("value", _in(_div("/Size/", _in(_div("line-item-details")))))));
			}
			//Color
			if ($colorBoolean) 
			{
			
				_assertEqual("Color:", _getText(_span("label", _in(_div("/color/", _in(_div("line-item-details")))))));
				_assertEqual($productColorInPDP, _getText(_span("value", _in(_div("/color/", _in(_div("line-item-details")))))));
			}
			
			//Verify the Quantity
			_assertEqual($productQuantityInPDP, _extract(_getText(_div("line-item-quantity")), "/[ ](.*)/",true));
			
			//Verify the Price
			_assertEqual($productPriceInPDP, _extract(_getText(_div("line-item-price")), "/[ ](.*)/", true));
			
			//Verify the Payment details
	//		_assertEqual("Payment Method"+" "+"Credit Card"+" "+"Scott Lang"+" "+"Visa"+" "+"Credit Card Token : 85896360"+" ************4113"+" "+"Amount:"+" "+$orderTot, _getText(_div("order-payment-instruments")));
			//_assertEqual($OCP[1][0]+" "+$OCP[8][0]+" "+$Card_Details[$i][1]+" "+$Card_Details[$i][2]+" "+$Card_Details[$i][7]+" "+"Amount:"+" "+$orderTot, _getText(_div("order-payment-instruments")));
			
			//Verify the billing address
			_assertEqual($OCP[1][6]+" "+$OCP_Address1, _getText(_div("order-billing")).replace(",",""));
			//Verify the Shipping address
			_assertEqual($OCP[0][2]+" "+$OCP_Address1.replace("Phone: ",""), _getText(_div("order-shipment-address")).replace(",",""));
			
		}
		catch($e)
		{      
		     _logExceptionAsFailure($e);
		}
		$t.end();

	}	//End of $i loop

}	//End of $j loop

var $t=_testcase("352545/352585/352558","Verify the functionality of 'Add to Address Book' check box and functionality of 'Save this card' checkbox on billing page in Application as a Registered user.");
$t.start();
try
{

//Precondition delete address
deleteAddress();	
//if User selected 'Use this address for Billing' checkbox
//Navigation till shipping page
navigateToShippingPage($item[0][0],$item[0][1]);
//shipping details
shippingAddress($Valid_Data,0);
//click on shipping continue button
_click($SHIPPING_SUBMIT_BUTTON);
//Billing details
BillingAddress($Valid_Data,0);
//selecting check box
_check($BILLING_ADD_TO_ADDRESS_CHECKBOX);
//Entering payment details
PaymentDetails($Valid_Data,4);
_check($BILLING_PAYMENT_SAVE_CARD_CHECKBOX);
//click on continue
_click($BILLING_CONTINUE_BUTTON);	
_click($BILLING_ADDRESS_SUBMIT);
if(_isVisible($BILLING_ADDRESS_CORRECTION_CLOSE_LINK))
{
_click($BILLING_ADDRESS_CORRECTION_CLOSE_LINK);
}
//Checking order is placed or not
_assertVisible($ORDER_CONFIRMATION_HEADING);
_assertVisible($ORDER_CONFIRMATION_DIV);

//verifying the address in account
_click(_link("my account"));
_click(_link("user_account", _in(_div("user-links"))));
_click(_div("Addresses"));
_assertEqual($Valid_Data[0][7],_getText($ADDRESSES_MINIADDRESS_TITLE));
_assertEqual($Valid_Data[0][1]+" "+$Valid_Data[0][2], _getText($ADDRESSES_MINIADDRESS_NAME));
_assertEqual($Valid_Data[0][11], _getText($ADDRESSES_MINIADDRESS_LOCATION));
//verifying the card details in account
_click(_link("Payment Settings"));
_assertContainsText($Valid_Data[4][7],$PAYMENT_PAYMENTLIST);

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

ClearCartItems();


var $t = _testcase("352522/352532/352529","Verify the functionality of the Checkbox 'Use this address for Billing' on shipping page in   application as a Registered user");
$t.start();
try
{

//Precondition delete address
deleteAddress();		
//delete if any existing addresses are there
_click(_link("user-account"));
_wait(2000);
_click($MY_ACCOUNT_LINK_LEFTNAV);
_click($LEFTNAV_ADDRESS_LINK);
while(_isVisible($ADDRESSES_DELETE_LINK))
	{
	_click($ADDRESSES_DELETE_LINK);
	_wait(3000);
	}	
//navigate to shipping page
navigateToShippingPage($Shipping_Page[0][1],1);
//entering the shipping address
shippingAddress($Shipping_addr,0);
//check the use this address for billing checkbox
//check add to address check box
_check($SHIPPING_ADDTOADDRESS_CHECKBOX);
_check($SHIPPING_BILLING_ADDRESS_CHECKBOX);
_click($SHIPPING_SUBMIT_BUTTON);
//address verification
//addressVerificationOverlay();
//verify the address prepopulation in billing page
//FIRST NAME
_assertEqual($Shipping_addr[0][1],_getText($BILLING_FN_TEXTBOX));
//Last name
_assertEqual($Shipping_addr[0][2],_getText($BILLING_LN_TEXTBOX));
//Address 1
_assertEqual($Shipping_addr[0][3],_getText($BILLING_ADDRESS1_TEXTBOX));
//Country
_assertEqual($Shipping_addr[0][5],_getSelectedText($BILLING_COUNTRY_DROPDOWN));
//State
_assertEqual($Shipping_addr[0][6],_getSelectedText($BILLING_STATE_DROPDOWN));
//City
_assertEqual($Shipping_addr[0][7],_getText($BILLING_CITY_TEXTBOX));
//Zipcode
_assertEqual($Shipping_addr[0][8],_getText($BILLING_ZIPCODE_TEXTBOX));
//Phone
_assertEqual($Shipping_addr[0][9],_getText($BILLING_NUMBER_TEXTBOX));
//enter an email ID
_setValue($BILLING_EMAIL_TEXTBOX,$uId);

//Entering payment details
PaymentDetails($Valid_Data,4);

//click on continue
_click($BILLING_CONTINUE_BUTTON);	
_click($BILLING_ADDRESS_SUBMIT);
if(_isVisible($BILLING_ADDRESS_CORRECTION_CLOSE_LINK))
{
_click($BILLING_ADDRESS_CORRECTION_CLOSE_LINK);
}

if(_isVisible($BILLING_ADDRESS_CORRECTION_CLOSE_LINK))
{
_click($BILLING_ADDRESS_CORRECTION_CLOSE_LINK);
}
//Checking order is placed or not
_assertVisible($ORDER_CONFIRMATION_HEADING);
_assertVisible($ORDER_CONFIRMATION_DIV);


//verifying the address in account
_click(_link("my account"));
_click(_link("user_account", _in(_div("user-links"))));
_click(_div("Addresses"));
//verify the presence of saved address
_assertVisible($ADDRESSES_MINIADDRESS_LOCATION);
var $adress=_getText($ADDRESSES_MINIADDRESS_LOCATION).toString().replace(",","");
_assertEqual($savedAddress,$adress);
//deleting the address
_click($ADDRESSES_DELETE_LINK);	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("352619","Verify the display of shipment section in Order Confirmation page in Application as a registered user, when the user places order with multi shipping.");
$t.start();
try
{
	//navigating to Shipping page
	var $proQty = 2;
	navigateToShippingPage($Order_Product[1][1],$proQty);
	var $shippingMethod=_extract(_getText($SHIPPING_RIGHT_NAV_SHIPPING_COST),"/Shipping(.*)[$]/",true).toString().trim();
	//click on yes button
	_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
	//adding addresses
	for(var $i=0;$i<$multishipAddress.length;$i++)
	{
		_click(_span("Add/Edit Address["+$i+"]"));
		addAddressMultiShip($multishipAddress,$i);
	}
	
	var $j=1;
		for(var $k=0;$k<$multishipAddress.length;$k++)
		{
			_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$k+"_addressList"),$j);
			$j++;
		}
		
	_click($SHIPPING_MULTIPLE_ADDRESS_SAVE_BUTTON);
	_click($BILLING_MULTI_SHIPPING_CONTINUE_BUTTON);
	//fill billing details
	BillingAddress($OCP_Address,0); 
	//entering Credit card Information
	PaymentDetails($Payment_data,0);
    $paymentType="Credit Card";
    _wait(3000,_isVisible($BILLING_CONTINUE_BUTTON));
    _click($BILLING_CONTINUE_BUTTON);
    _wait(10000,_isVisible($BILLING_ADDRESS_SUBMIT));
    
	//click on Place Order
	_click($BILLING_PAYMENT_PLACE_ORDER_BUTTON);
	_wait(2000);
	
	//verify the navigation to OCP page
	if(_isVisible($BILLING_ADDRESS_CORRECTION_CLOSE_LINK))
	{
		_click($BILLING_ADDRESS_CORRECTION_CLOSE_LINK);
	}
	//verify the addresses
	var $count=_count("_table","/order-shipment-table/");
	_log($count);
	for(var $i=0;$i<$count;$i++)
		{
		
		var $ExpShippingAddress=$multishipAddress[$i][2]+" "+$multishipAddress[$i][3]+" "+$multishipAddress[$i][4]+" "+$multishipAddress[$i][5]+" "+$multishipAddress[$i][8]+", "+$multishipAddress[$i][11]+" "+$multishipAddress[$i][9]+" "+$multishipAddress[$i][6]+" "+$multishipAddress[$i][10];
		var $ActShippingAddress=_getText($ORDER_CONFIRMATION_SHIPPING_ADDRESS);
		_log($ActShippingAddress);
			if($ActShippingAddress.indexOf($multishipAddress[$i][2])>=0)
			{
				_assertEqual($ExpShippingAddress,$ActShippingAddress);
			}
			else 
			{
				var $ActShippingAddress=_getText($ORDER_CONFIRMATION_SHIPPING_ADDRESS1);
				//var $ActShippingAddress=_getText(_div("order-shipment-address["+$i+"]"));	
				_assertEqual($ExpShippingAddress,$ActShippingAddress);
			}
		}
	
	
	//Verify the OCP page
	_assertEqual("Thank you for your order!", _getText(_heading1("/(.*)/", _in(_div("confirmation-message")))));
	//Billing address
	_assertEqual("Billing Address"+" "+"steve rogers 1706 N NAGLE AVE street21 CHICAGO, IL 60707-4018 United States Phone: 3333333333", _getText(_div("order-billing")));
	
	//Verify the shipping headein
	var $shippingHeading = _collect("_heading2","/(.*)/", _in(_div("order-shipments")));
	_assertEqual($proQty, $shippingHeading.length);
	var $j = 1;
	for (var $i = 0; $i < $shippingHeading.length; $i++) 
	{
		_assertEqual("Shipment No. "+$j, _getText($shippingHeading[$i]));
		$j++;
	}
	
	var $shippingSection = _collect("_div","order-shipment-table", _in(_div("order-shipments")));
	for (var $i = 0; $i < $shippingSection.length; $i++) 
	{
		//Verify the shipping method label	
		_assertEqual("Method:", _getText(_div("label", _in(_div("shipping-method", _in($shippingSection[$i]))))));
		_assertEqual("Standard Shipping", _getText(_div("value", _in(_div("shipping-method", _in($shippingSection[$i]))))));
		
		//Verify the shipping address in OCP
		_assertEqual("Shipping To"+" "+$multishipAddress[$i][12], _getText(_div("order-shipment-address", _in($shippingSection[$i]))));
		
		//Verify the product name
		_assertEqual($productNameInPDP, _getText(_div("name", _in(_div("line-item-details", _in($shippingSection[$i]))))));
		_assertEqual("1",_extract(_getText(_div("line-item-quantity", _in($shippingSection[$i]))),"/[ ](.*)/", true));
		_assertEqual($productPriceInPDP, _extract(_getText(_div("line-item-price", _in($shippingSection[$i]))), "/[ ](.*)/", true));
	}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();	

//Clean up
cleanup();

var $t=_testcase("352619","Verify the display of shipment section in Order Confirmation page in Application as a guest user, when the user places order with multi shipping.");
$t.start();
try
{
	//navigating to Shipping page
	var $proQty = 2;
	navigateToShippingPage($Order_Product[1][1],$proQty);
	var $shippingMethod=_extract(_getText($SHIPPING_RIGHT_NAV_SHIPPING_COST),"/Shipping(.*)[$]/",true).toString().trim();
	//click on yes button
	_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
	//adding addresses
	for(var $i=0;$i<$multishipAddress.length;$i++)
	{
		_click(_span("Add/Edit Address["+$i+"]"));
		addAddressMultiShip($multishipAddress,$i);
	}
	
	var $j=1;
		for(var $k=0;$k<$multishipAddress.length;$k++)
		{
			_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$k+"_addressList"),$j);
			$j++;
		}
		
	_click($SHIPPING_MULTIPLE_ADDRESS_SAVE_BUTTON);
	_click($BILLING_MULTI_SHIPPING_CONTINUE_BUTTON);
	//fill billing details
	BillingAddress($OCP_Address,0); 
	//entering Credit card Information
	PaymentDetails($Payment_data,0);
    $paymentType="Credit Card";
    _wait(3000,_isVisible($BILLING_CONTINUE_BUTTON));
    _click($BILLING_CONTINUE_BUTTON);
    _wait(10000,_isVisible($BILLING_ADDRESS_SUBMIT));
    
	//click on Place Order
	_click($BILLING_PAYMENT_PLACE_ORDER_BUTTON);
	_wait(2000);
	
	//verify the navigation to OCP page
	if(_isVisible($BILLING_ADDRESS_CORRECTION_CLOSE_LINK))
	{
		_click($BILLING_ADDRESS_CORRECTION_CLOSE_LINK);
	}
	//verify the addresses
	var $count=_count("_table","/order-shipment-table/");
	_log($count);
	for(var $i=0;$i<$count;$i++)
		{
		
		var $ExpShippingAddress=$multishipAddress[$i][2]+" "+$multishipAddress[$i][3]+" "+$multishipAddress[$i][4]+" "+$multishipAddress[$i][5]+" "+$multishipAddress[$i][8]+", "+$multishipAddress[$i][11]+" "+$multishipAddress[$i][9]+" "+$multishipAddress[$i][6]+" "+$multishipAddress[$i][10];
		var $ActShippingAddress=_getText($ORDER_CONFIRMATION_SHIPPING_ADDRESS);
		_log($ActShippingAddress);
			if($ActShippingAddress.indexOf($multishipAddress[$i][2])>=0)
			{
				_assertEqual($ExpShippingAddress,$ActShippingAddress);
			}
			else 
			{
				var $ActShippingAddress=_getText($ORDER_CONFIRMATION_SHIPPING_ADDRESS1);
				//var $ActShippingAddress=_getText(_div("order-shipment-address["+$i+"]"));	
				_assertEqual($ExpShippingAddress,$ActShippingAddress);
			}
		}
	
	
	//Verify the OCP page
	_assertEqual("Thank you for your order!", _getText(_heading1("/(.*)/", _in(_div("confirmation-message")))));
	//Billing address
	_assertEqual("Billing Address"+" "+"steve rogers 1706 N NAGLE AVE street21 CHICAGO, IL 60707-4018 United States Phone: 3333333333", _getText(_div("order-billing")));
	
	//Verify the shipping headein
	var $shippingHeading = _collect("_heading2","/(.*)/", _in(_div("order-shipments")));
	_assertEqual($proQty, $shippingHeading.length);
	var $j = 1;
	for (var $i = 0; $i < $shippingHeading.length; $i++) 
	{
		_assertEqual("Shipment No. "+$j, _getText($shippingHeading[$i]));
		$j++;
	}
	
	var $shippingSection = _collect("_div","order-shipment-table", _in(_div("order-shipments")));
	for (var $i = 0; $i < $shippingSection.length; $i++) 
	{
		//Verify the shipping method label	
		_assertEqual("Method:", _getText(_div("label", _in(_div("shipping-method", _in($shippingSection[$i]))))));
		_assertEqual("Standard Shipping", _getText(_div("value", _in(_div("shipping-method", _in($shippingSection[$i]))))));
		
		//Verify the shipping address in OCP
		_assertEqual("Shipping To"+" "+$multishipAddress[$i][12], _getText(_div("order-shipment-address", _in($shippingSection[$i]))));
		
		//Verify the product name
		_assertEqual($productNameInPDP, _getText(_div("name", _in(_div("line-item-details", _in($shippingSection[$i]))))));
		_assertEqual("1",_extract(_getText(_div("line-item-quantity", _in($shippingSection[$i]))),"/[ ](.*)/", true));
		_assertEqual($productPriceInPDP, _extract(_getText(_div("line-item-price", _in($shippingSection[$i]))), "/[ ](.*)/", true));
	}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();	

cleanup();