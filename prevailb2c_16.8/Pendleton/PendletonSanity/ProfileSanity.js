_include("../GenericLibrary/GlobalFunctions.js");

//variable declaration
var $color=$Account_Validation_createaccount[0][8];
var $color1=$Account_Validation_createaccount[1][8];

//deleting the user if exists
deleteUser();
deleteUser_Profile($NameEmail_Validation[8][3]);
SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);

//from util.GenericLibrary/BrowserSpecific.js
var $FirstName=$FName.toUpperCase();
var $LastName=$LName.toUpperCase();
_log($FirstName);
_log($LastName);

var $t = _testcase("367277","Verify the functionality related to 'Create Account' button in create account page of the application as an anonymous user");
$t.start();
try
{

//Navigate to create account page
_click($HEADER_LOGIN_LINK);
_click($LOGIN_CREATEACCOUNTNOW_BUTTON);

_assertVisible($PAGE_BREADCRUMB);
_assertEqual($Generic_Register[1][0], _getText($PAGE_BREADCRUMB));

//Navigation related to my account
_click($PAGE_BREADCRUMB_LINK);
_assertVisible($MY_ACCOUNT_HEADING);
_assertVisible($LOGIN_SECTION);

_click($HEADER_LOGIN_LINK);
_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
//heading
_assertVisible($REGISTER_HEADING);
//breadcrumb
_assertVisible($PAGE_BREADCRUMB);
_assertEqual($Generic_Register[1][0], _getText($PAGE_BREADCRUMB));
//Creating an account
createAccount();
//verify whether account got created or not
var $exp=$userData[$user][1]+" "+$userData[$user][2];
_assertEqual($exp,_extract(_getText(_div("pwheading-one")),"[|] (.*) [(]",true).toString());
_click($HEADER_USERACCOUNT_LINK);
_click($MY_ACCOUNT_LOGOUT_LINK);
_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
		
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("367299","Logout functionality of the application");
$t.start();
try
{

//Logout functionality	
logout();
//verify the functionality
_click($HEADER_USERACCOUNT_LINK);
_assertNotVisible($HEADER_LOGIN_LINK);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("367309","Verify the functionality related of 'LOGIN' button My Account login's page");
$t.start();
try
{

//navigate back to my account page
_click($HEADER_LOGIN_LINK);	
//login to the application
login();
_assertVisible($MY_ACCOUNT_LOGOUT_LINK);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("367287/367288/367286","Verify the functionality related to 'Apply' button of change password section in Edit Account page of the applicatio/Verify the validation related to Confirm New Password field in Edit Account page of the application/" +
		"Verify the validation related to Email field on Edit Account page in application as a  Registered user.");
$t.start();
try
{

//navigate to my account home page
_click($HEADER_USERACCOUNT_LINK);
_click($HEADER_MYACCOUNT_LINK);
_click($LEFTNAV_EDITINFORMATION_LINK);
//enter new values	
_setValue($EDITACCOUNT_FIRSTNAME_TEXTBOX, $NameEmail_Validation[8][1]);
_setValue($EDITACCOUNT_LASTNAME_TEXTBOX, $NameEmail_Validation[8][2]);
_setValue($EDITACCOUNT_EMAIL_TEXTBOX, $NameEmail_Validation[8][3]);
_setValue($EDITACCOUNT_CONFIRM_EMAIL_TEXTBOX, $NameEmail_Validation[8][4]);
_setValue($EDITACCOUNT_PASSWORD_TEXTBOX, $pwd);

//click on submit button
_click($EDITACCOUNT_SUBMIT_BUTTON); 

//verifying account is created or not 
_assertVisible($HEADER_USERACCOUNT_LINK);
_assertVisible(_div($Generic_Editaccount[5][0]+" "+$NameEmail_Validation[8][1]+" "+$NameEmail_Validation[8][2]+" "+$Generic_Editaccount[6][0]));
_assertVisible($MY_ACCOUNT_LOGOUT_LINK);
//verification in edit account page
_click($LEFTNAV_EDITINFORMATION_LINK);

if (_isChrome())
{
	
//resetting the data					 
_setValue($EDITACCOUNT_FIRSTNAME_TEXTBOX, $FirstName);
_setValue($EDITACCOUNT_LASTNAME_TEXTBOX, $LastName);
_setValue($EDITACCOUNT_EMAIL_TEXTBOX, $uId);
_setValue($EDITACCOUNT_CONFIRM_EMAIL_TEXTBOX, $uId);
_setValue($EDITACCOUNT_PASSWORD_TEXTBOX, $pwd);
_click($EDITACCOUNT_SUBMIT_BUTTON); 
_wait(3000);
_assertVisible($HEADER_USERACCOUNT_LINK);
_assertVisible(_div($Generic_Editaccount[5][0]+" "+$FirstName+" "+$LastName+" "+$Generic_Editaccount[6][0]));
_assertVisible($MY_ACCOUNT_LOGOUT_LINK);

//verification in edit account page
_click($LEFTNAV_EDITINFORMATION_LINK);
//updating the password
_setValue($EDITACCOUNT_CURRENTPASSWORD_TEXTBOX,$pwd);
_setValue($EDITACCOUNT_NEWPASSWORD_TEXTBOX,$NameEmail_Validation[8][5]);
_setValue($EDITACCOUNT_CONFIRMPASSWORD_TEXTBOX,$NameEmail_Validation[8][5]);
//change password button
_click($EDITACCOUNT_CHANGEPASSWORD_SUBMITBUTTON);
_assertVisible($HEADER_USERACCOUNT_LINK);
_assertVisible(_div($Generic_Editaccount[5][0]+" "+$FirstName+" "+$LastName+" "+$Generic_Editaccount[6][0]));
//logout
logout();
//click on login link
_click($HEADER_LOGIN_LINK);	
//logout from application and login with old and new password generated
Login_Account($uId,$pwd);
//error message should through
_assertEqual("Your account information is temporarily unavailable. We are working to resolve the issue, and apologize for the inconvenience.", _getText(_div("error-form")));
//login with changed password
Login_Account($uId,$NameEmail_Validation[8][5]);
_assertVisible($MY_ACCOUNT_LOGOUT_LINK);
//navigate to my account home page
_click($HEADER_USERACCOUNT_LINK);
_click($HEADER_MYACCOUNT_LINK);
_click($LEFTNAV_EDITINFORMATION_LINK);
//Reset the password values
_setValue($EDITACCOUNT_CURRENTPASSWORD_TEXTBOX,$NameEmail_Validation[8][5]);
_setValue($EDITACCOUNT_NEWPASSWORD_TEXTBOX,$pwd);
_setValue($EDITACCOUNT_CONFIRMPASSWORD_TEXTBOX,$pwd);
//change password button
_click($EDITACCOUNT_CHANGEPASSWORD_SUBMITBUTTON);
_wait(3000);
_assertVisible($HEADER_USERACCOUNT_LINK);
_assertVisible(_div($Generic_Editaccount[5][0]+" "+$FirstName+" "+$LastName+" "+$Generic_Editaccount[6][0]));
_assertVisible($MY_ACCOUNT_LOGOUT_LINK);

}
else
	{
	//resetting the data					 
	_setValue($EDITACCOUNT_FIRSTNAME_TEXTBOX, $FirstName);
	_setValue($EDITACCOUNT_LASTNAME_TEXTBOX, $LastName);
	_setValue($EDITACCOUNT_EMAIL_TEXTBOX, $uId);
	_setValue($EDITACCOUNT_CONFIRM_EMAIL_TEXTBOX, $uId);
	_setValue($EDITACCOUNT_PASSWORD_TEXTBOX, $pwd);
	_click($EDITACCOUNT_SUBMIT_BUTTON); 
	_wait(3000);
	_assertVisible($HEADER_USERACCOUNT_LINK);
	_assertVisible(_div($Generic_Editaccount[5][0]+" "+$FirstName+" "+$LastName+" "+$Generic_Editaccount[6][0]));
	_assertVisible($MY_ACCOUNT_LOGOUT_LINK);
	}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//Delete address
deleteAddress();

//UI with addresses
var $t = _testcase("367253/367252/367249", "Verify the UI of Addresses page of the application as a registered user having saved addresses");
$t.start();
try
{
	
//navigate to address page
_click($HEADER_MYACCOUNT_LINK);
_click($MY_ACCOUNT_ADDRESSES_OPTIONS); 

//close address overlay
_click($ADDRESSES_CREATENEW_ADDRESS_LINK);
_wait(5000, _isVisible($ADDRESSES_CREATEADDRESS_OVERLAY));
//Checking close functionality
_assertVisible($ADDRESSES_OVERLAY_CLOSEBUTTON);
_click($ADDRESSES_OVERLAY_CLOSEBUTTON);
_assertNotVisible($ADDRESSES_CREATEADDRESS_OVERLAY);

	
  for(var $i=0;$i<2;$i++)
	{
		_log($i);
		_click($ADDRESSES_CREATENEW_ADDRESS_LINK);
		 addAddress($Valid_Address,$i);
		 _click($ADDRESSES_OVERLAY_APPLYBUTTON);
		 _wait(4000);
	}
	
//heading
_assertVisible($ADDRESSES_ADDRESS_HEADING);
//breadcrumb
_assertVisible($PAGE_BREADCRUMB);
_assertEqual($Generic_Addresses[1][0], _getText($PAGE_BREADCRUMB));
//create link
_assertVisible($ADDRESSES_CREATENEW_ADDRESS_LINK);	
//left nav
_assertVisible($LEFTNAV_SECTION);

//VERIFY default address
_assertVisible($ADDRESSES_DEFAUTADDRESS_HEADINGTEXT);
_assertEqual($Valid_Address[0][1],_getText($ADDRESSES_DEFAUTADDRESS_TITLE));
_assertEqual($Valid_Address[0][2]+" "+$Valid_Address[0][3],_getText($ADDRESSES_DEFAUTADDRESS_NAME));
_assertEqual($Valid_Address[0][11], _getText($ADDRESSES_DEFAUTADDRESS_LOCATION));
_assertVisible($ADDRESSES_DELETE_LINK, _in($ADDRESSES_DEFAUTADDRESS));
_assertVisible($ADDRESSES_EDIT_LINK, _in($ADDRESSES_DEFAUTADDRESS));


//VERIFY saved address
_assertEqual($Valid_Address[1][1],_getText($ADDRESSES_SAVEDADDRESS_TITLE));
_assertEqual($Valid_Address[1][2]+" "+$Valid_Address[1][3],_getText($ADDRESSES_SAVEDADDRESS_NAME));
_assertEqual($Valid_Address[1][11], _getText($ADDRESSES_SAVEDADDRESS_LOCATION));
_assertVisible($ADDRESSES_SAVEDADDRESS_DELETELINK);
_assertVisible($ADDRESSES_SAVEDADDRESS_EDITLINK);
_assertVisible($ADDRESSES_SAVEDADDRESS_MAKEDEFAULTLINK);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
 $t.end();
 
//Delete address
 var $t = _testcase("367263/367231", "Verify the functionality related to 'Delete' link present below each of the saved addresses on addresses page/application behavior on deleting default address in addresses page of the application as a registered user");
 $t.start();
 try
 {
	 
 //Delete address
 deleteAddress();
 	
 }
 catch($e)
 {
 	_logExceptionAsFailure($e);
 }	
  $t.end();
 

  var $t=_testcase("367358","Verify Wishlist functionality for a Logged in user");
  $t.start();
  try
  {

  //navigate to wish list page
  _click($HEADER_WISHLIST_LINK);
  if (_isVisible($WISHLIST_SHIPPINGADDRESS_DROPDOWN))
  	{
  		_setSelected($WISHLIST_SHIPPINGADDRESS_DROPDOWN,0);
  	}

  //deleting if any addresses are present
  deleteAddress();

 //navigate to wish list page
 _click($HEADER_WISHLIST_LINK);
  
//add products to wish list by clicking on link Click here to start adding items.
  _click($WISHLIST_ADDINGITEM_LINK);
  //verify the navigation to home page
  _assertVisible($HOMEPAGE_BANNER_IMAGE);
  //add items
  search($Wishlist_Reg[0][0]);
  //select swatches
  selectSwatch();
  //prodname in PDP
  var $productName=_getText(_heading1("product-name"));
  //click on link add to wish list
  _click($WISHLIST_ADDTOWISHLIST_LINK);
  //navigate to wish list detail page
  _assertVisible($WISHLIST_FINDSOMEONES_WISHLIST);

  }
  catch($e)
  {      
      _logExceptionAsFailure($e);
  }
  $t.end();
  
  
  //############### Related to store locator ################
  
  var $t = _testcase("357936", "Verify the functionality of Back to Store in store locator results page both as an anonymous and a registered user./Verify the functionality of Radius Search (1) CTA in store locator page both as an anonymous and a registered user.");
  $t.start();
  try
  {
  	
  //click on store locator link
  _click($STORELOCATOR_LINK);
  //enter text and click on submit button 
  _setValue($STORELOCATOR_POSTAL_CODE_TEXTBOX,$storelocator[0][1]);
  _setSelected($STORELOCATOR_RADIUS_DROPDOWN,$storelocator[1][1]);
  //click on submit button
  _click($STORELOCATOR_SEARCHBUTTON_BELOW_RADIUSDROPDOWN);
  //store locator results page
  _assertContainsText($storelocator[0][1], _heading2("store-locator-header"))
  _assertVisible($STORELOCATOR_RESULTS_PAGE);
  _assertVisible($STORELOCATOR_RESULT_HEADING);
  _assertVisible($STORELOCATOR_BACKTO_SL_LINK);
  //store locator page
  _assertVisible($STORELOCATOR_STORE_NAME);
  _click($STORELOCATOR_STORE_NAME);
  _assertVisible($STORELOCATOR_BACKTO_SL_LINK);
  //back to store functionality
  _click($STORELOCATOR_BACKTO_SL_LINK);
  //verify the heading 
  _assertVisible($STORELOCATOR_HEADING);

  }
  catch($e)
  {
  	_logExceptionAsFailure($e);
  }	
  $t.end();
  
  var $t = _testcase("367344/367341/128535/367336","Verify the UI of payment settings page of the application as a registered user having saved credit card details/application behavior on click of apply button in add credit card overlay/Verify if the user is able to add a create card details in Add payment details overlay as a registered user");
  $t.start();
  try
  {
  	
  //Delete card
  DeleteCreditCard();
  //Pre conditions(save 1 credit card details)
  _click($PAYMENT_ADDCREDITCARD);
  
	//click on close link
	if(_isVisible($PAYMENT_CLOSEBUTTON))
		{
		_click($PAYMENT_CLOSEBUTTON);
		}
	//overlay should not be displayed
	closedOverlayVerification();
  
	//Click on add credit card overlay
  _click($PAYMENT_ADDCREDITCARD);
  _wait(3000)
  CreateCreditCard(0,$Createcard);
_wait(3000)
if (_isVisible($PAYMENT_ADDCARDOVERLAY))
	{		  
	  _assert(true, "Add a Credit Card overlay is displayed");		
	}
else
	{
	_assert(false, "Add a Credit Card overlay is not displayed");
	}

  //click on apply button
  _click($PAYMENT_APPLYBUTTON);
  //overlay got closed or not
  closedOverlayVerification();


  //verify the UI

  //Brad crumb
  _assertVisible(_link($Payment_Data_Profile[0][2], _in($PAGE_BREADCRUMB)));

  //heading
  _assertVisible($PAYMENT_CREDITCARD_INFORMATION_HEADING);
  _assertVisible($PAYMENT_ADDCREDITCARD );

  //verify whether credit card got saved or not and also UI
  _assertVisible($PAYMENT_PAYMENTLIST);
  var $saveInfo=_getText(_listItem("/first/")).split(" ");
  var $actualExpdate=$saveInfo[8]+""+$saveInfo[9];
  _assertEqual($Createcard[0][1],$saveInfo[0]);
  _assertEqual($Createcard[0][2],$saveInfo[1]);
  var $expDate=$Createcard[0][15]+"."+$Createcard[1][4]+"."+$Createcard[0][5];
  _assertEqual($expDate,$actualExpdate);
  _assertVisible($PAYMENT_DELETECARD_LINK);

  }
  catch($e)
  {
  	_logExceptionAsFailure($e);
  }	
  $t.end();

var $t = _testcase(" ", " ");
$t.start();
try
{
	

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();