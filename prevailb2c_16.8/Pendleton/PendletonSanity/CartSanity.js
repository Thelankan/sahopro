_include("../GenericLibrary/GlobalFunctions.js");


SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);

var $t = _testcase("352722", "Verify the functionality of 'Go straight to checkout' link in Mini cart in the application as an registered user");
$t.start();
try
{

//add item to cart
addToCartSearch($Minicart[2][0],$Minicart[0][4]);
//login to the application
_click($HEADER_LOGIN_LINK);
login();
//mousehover
_mouseOver($MINICART_LINK);	
//click on go strait to checkout
_click($MINICART_CHECKOUT);
//verify the navigation
//Should Nav to Shipping page
_assertVisible($SHIPPING_PAGE_HEADING);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("352774","Verify the navigation on click of 'Checkout' button from the cart page in the application as a anonymous user.");
$t.start();
try
{
	
//navigate back to cart
_click($MINICART_LINK);
//click on checkout button
_click($CART_CHECKOUT_BUTTON);
//Should Nav to Shipping page
_assertVisible($SHIPPING_PAGE_HEADING);
//nav to Home page
_click($LOGO_LINK);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("352751","Verify whether user can remove the line item from cart");
$t.start(); 
try
{
	
//navigate to cart
_click($MINICART_LINK);
//clear cart items
ClearCartItems();
//Verifying "Your Shopping Cart is Empty" message
_assertVisible($CART_EMPTY_HEADING);
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}		
$t.end();

var $t=_testcase("352753","Verify whether registered user can add item to Wish list/display of success message at line item after adding item to wishlist in the application");
$t.start();
try
{

//navigate to wish list page
clearWishList();
//Adding Item to the cart
 navigateToCartSearch($Item[0][0],$Item[0][1]);
//Click on add to wish list in cart page
_click($CART_ADDTO_WISHLIST_LINK);
//Verifying Success message
_assertVisible($CART_WISHLIST_SUCCESS_MESSAGE);
_assertEqual("This item has been added to your Wish List.", _getText($CART_WISHLIST_SUCCESS_MESSAGE));
_click(_link("user-account"));
_click(_link("user-wishlist"));
//Verifying respective product in wish list
_assertEqual($CProductName,_getText($WISHLIST_PRODUCTNAME));
//removing the wish list
clearWishList();
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

