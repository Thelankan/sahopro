_include("../GenericLibrary/GlobalFunctions.js");


SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);

var $t = _testcase("343298", "Verify the functionality related to  'pagination' on search result page   in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($SearchResult[0][0]);
	pagination();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("343334/343333","Verify the functionality by clicking on suggested word in No Search Results page both as guest and a registered user./Verify the display of misspelled words and did you mean in No Search Results page both as guest and a registered user.");
$t.start();
try
{

//Navigate to no search result page
search($SearchResult[0][3]);
//No search result images
_assertVisible(_paragraph($SearchResult[3][4]+" "+$SearchResult[0][3]));
_click(_link("no-hits-search-term-suggest"));
//navigate to particular category page
_assertContainsText($SearchResult[2][2], _span("breadcrumb-element breadcrumb-result-text"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("352994","Verify the application behavior on click of add to wishlist link in product detail page of the application as a guest user");
$t.start();
try
{
	
//navigate to PDP
search($PDP_Standard[0][4]);
var $PName=_getText($PLP_PRODUCTNAME_LINK);
//click on name link
_click($PLP_PRODUCTNAME_LINK);
//selecting swatches
selectSwatch();
//add to wish list
_click($PDP_ADDTO_WISHLIST_LINK);
//verify the navigation
_assertVisible(_heading1("My Account Login"));
_assertContainsText($PDP_Standard[0][1], $PAGE_BREADCRUMB);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("343293", "Verify the system's response on selection on Price filter from left nav  in the application both as an anonymous and a registered user");
$t.start();
try
{
	search($SearchResult[0][0]);
	var $PriceRange=_collect("_link","/refinement-link/",_in($PLP_LEFTNAV_PRICE_RANGE));
	var $PriceRangeValues=_collectAttributes("_link","/refinement-link/","sahiText",_in($PLP_LEFTNAV_PRICE_RANGE));
	for (var $k=0; $k< $PriceRange.length; $k++)
	{
		_click($PriceRange[$k]);
		_assertEqual($PriceRangeValues[$k]+" x", _getText($PLP_BREADCRUMB_REFINEMENT_VALUE));
		priceCheck($PriceRange[$k]);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase("353045","Verify the functionality of add all to bag button on product detail page of the application for a product set");
$t.start();
try
{	
	
_log("Configuration of product set is required");	
//navigate to PDP
navigateCatPage($PDP_Productset[0][0],$PDP_Productset[2][0]);
//click on name link
_click($PLP_PRODUCTNAME_LINK);	
var $totalProductsInSet=_collect("_div","/product-set-item product-bundle-item/",_in($PDP_SET_LIST));
var $QuantityBox=_collect("_textbox","/Quantity/",_in($PDP_SET_LIST));
var $totalQty=0;	
//fetch the quantity present in the all boxes in set 
for(var $i=0;$i<$totalProductsInSet.length;$i++)
  {		
	$totalQty=parseFloat($totalQty)+parseFloat(_getText($QuantityBox[$i]));
  }
//fetch quantity from mini cart before clicking
var $expCartQuantity=$totalQty;
//click on add all to cart
_click($PDP_PRODUCTSET_ADDALLTOCART_BUTTON);
var $actCartQuantity=_getText($MINICART_QUANTITY);
//verify the cart quantity
_assertEqual($expCartQuantity,$actCartQuantity);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/