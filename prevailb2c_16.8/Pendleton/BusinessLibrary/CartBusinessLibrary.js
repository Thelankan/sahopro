_includeOnce("../ObjectRepository/Generic_OR.sah");

var $CProductName;
var $CQuantity;
var $CItemBasicPrice;
var $CItemSalesPrice;
var $CItemStandardPrice;
var $CShippingcharge;
var $COrderTotal;

function ClearCartItems()
{	 
	if(_isVisible($HEADER_MINICART_LINK))
	{ 		
		_click($HEADER_MINICART_LINK);
		if(_isVisible($CART_COUPON_ROW))
	 		 {
	 			_click($CART_COUPON_REMOVE);
	 		 }	
		else
			{
	 			while(_isVisible($CART_REMOVE_LINK))
					{
		 				_click($CART_REMOVE_LINK);
					}
			}	
	}
}

//Navigate to cart
function navigateToCart($Item,$qty)
{
	//add item to cart
	addItemToCart($Item,$qty);
	//navigate to Cart page
	_click($MINICART_VIEWCART_LINK);
	//fetch item details
	$CProductName=_getText($CART_NAME);
	$CQuantity=_getText($CART_QTY_DROPDOWN);
	$CItemBasicPrice=_extract(_getText($CART_SALES_PRICE),"/[$](.*)/",true).toString();
	if(_isVisible($CART_LINEITEM_PRICE))
		{
			$CItemSalesPrice=_extract(_getText($CART_LINEITEM_PRICE),"/[$](.*)/",true).toString();
		}
	else if(_isVisible($CART_LINEITEM_DISCOUNT_PRICE))
		{
			$CItemSalesPrice=_extract(_getText($CART_LINEITEM_DISCOUNT_PRICE),"/[$](.*)/",true).toString();
		}
	$CShippingcharge=_extract(_getText($CART_SHIPPINGPRICE),"/[$](.*)/",true).toString();
	$cOrderSubtotal=_extract(_getText($CART_SUBTOTAL),"/[$](.*)/",true).toString();
	$COrderTotal=_extract(_getText($CART_ESTIMATED_TOTAL),"/[$](.*)/",true).toString();
}

//Navigate to cart
function navigateToCartSearch($Item,$qty)
{
	//add item to cart
	addToCartSearch($Item,$qty)
	//navigate to Cart page
	_click($MINICART_VIEWCART_LINK);
	//fetch item details
	$CProductName=_getText($CART_NAME);
	$CQuantity=_getText($CART_QTY_DROPDOWN);
	$CItemBasicPrice=_extract(_getText($CART_SALES_PRICE),"/[$](.*)/",true).toString();
	if(_isVisible($CART_LINEITEM_PRICE))
		{
			$CItemSalesPrice=_extract(_getText($CART_LINEITEM_PRICE),"/[$](.*)/",true).toString();
		}
	else if(_isVisible($CART_LINEITEM_DISCOUNT_PRICE))
		{
			$CItemSalesPrice=_extract(_getText($CART_LINEITEM_DISCOUNT_PRICE),"/[$](.*)/",true).toString();
		}
	$CShippingcharge=_extract(_getText($CART_SHIPPINGPRICE),"/[$](.*)/",true).toString();
	$cOrderSubtotal=_extract(_getText($CART_SUBTOTAL),"/[$](.*)/",true).toString();
	$COrderTotal=_extract(_getText($CART_ESTIMATED_TOTAL),"/[$](.*)/",true).toString();
}



function OrderTotalVerification($discount)
{
	var $TotCartItems=_count("_row","/cart-row/",_in(_div("primary")));
	var $LineItemQty;
	var $Price;
	var $DiscountPrice;	
	var $ExpItemTotal=0;
	var $ActItemTotal;
	var $ExpOrderSubTotal=0;
	var $ActOrderSubTotal=parseFloat(_extract(_getText($CART_SUBTOTAL),"/[$](.*)/",true));
	for(var $i=0; $i<$TotCartItems; $i++)
		{
			//line item Quantity
			$LineItemQty=_getText($CART_QTY_DROPDOWN, _in(_row("cart-row["+$i+"]")));
			//individual item price		
			$Price=parseFloat(_extract(_getText($CART_SALES_PRICE, _in(_row("cart-row["+$i+"]"))),"/[$](.*)/",true));
			if(_isVisible($CART_LINEITEM_PRICE,_in(_row("cart-row["+$i+"]"))))
				{
					$ExpItemTotal=$LineItemQty*$Price;
					$ExpItemTotal=round($ExpItemTotal, 2);
					$ActItemTotal=parseFloat(_extract(_getText($CART_LINEITEM_PRICE,_in(_row("cart-row["+$i+"]"))),"/[$](.*)/",true));
					_assertEqual($ExpItemTotal,$ActItemTotal);
				}		
			else
				{			
					$ExpItemTotal=$LineItemQty*$Price;
					$DiscountPrice=($discount/100)*$ExpItemTotal;
					$ExpItemTotal=$ExpItemTotal-$DiscountPrice;
					$ExpItemTotal=round($ExpItemTotal, 2);
					$ActItemTotal=parseFloat(_extract(_getText($CART_LINEITEM_DISCOUNT_PRICE,_in(_row("cart-row["+$i+"]"))),"/[$](.*)/",true));
					_assertEqual($ExpItemTotal,$ActItemTotal);			
				}
			//Item Total Price
			$ExpOrderSubTotal=$ExpOrderSubTotal+$ExpItemTotal;		
		}
	_assertEqual($ExpOrderSubTotal,$ActOrderSubTotal);
}

//Navigate to ILP
function navigateTOIL($excel)
{
	//navigate to cart page
	navigateToCart($excel[0][0],$excel[0][1]);
	//click on go straight to checkout to navigate to IL Page
	_click($MINICART_CHECKOUT);
}

function MiniCartProductCount()
{
var $prodcount_minicart=_count("_div","/mini-cart-product/",_in(_div("mini-cart-products")));
return $prodcount_minicart;
}