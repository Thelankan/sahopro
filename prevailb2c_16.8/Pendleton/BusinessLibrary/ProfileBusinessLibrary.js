_includeOnce("../ObjectRepository/Generic_OR.sah");
//******************************Login**************************

function login()
{	 
	_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX, $uId);
	_setValue($LOGIN_PASSWORD_TEXTBOX, $pwd);
	if(_isVisible($LOGIN_SUBMIT_BUTTON))
	{
		_click($LOGIN_SUBMIT_BUTTON);
	}
	else
	{
		_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	}
}

function Login_Account($email,$password)
{	 
	_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$email);
	_setValue($LOGIN_PASSWORD_TEXTBOX, $password);
	if(_isVisible($LOGIN_SUBMIT_BUTTON))
	{
		_click($LOGIN_SUBMIT_BUTTON);
	}
	else
	{
		_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	}
}

function logout()
{	
	
if (isMobile()) 
{
	_click(_submit("menu-toggle"));
	_wait(2000);
	if (_isVisible(_link("/user-logout/"))) 
	{
		_click(_link("/user-logout/"));
	}
}
	else
		{
		//logout from the applicationa
		if(_isVisible($HEADER_USERACCOUNT_LINK))
		{
			_click($HEADER_USERACCOUNT_LINK);
			_wait(3000);
			if(_isVisible($HEADER_LOGOUT_LINK))
			{		
				_click($HEADER_LOGOUT_LINK);	
			}
		}
		}
	
}

function CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode)
{
	_setValue($LOGIN_ORDERNUMBER_TEXTBOX,$OrderNumber);
	_setValue($LOGIN_ORDEREMAIL_TEXTBOX,$UserEmail);
	_setValue($LOGIN_ORDER_ZIPCODE_TEXTBOX,$Zipcode);
	_click($LOGIN_ORDER_SUBMITBUTTON);
}
//******************************Create Account**************************
function createAccount()
{
	_setValue($REGISTER_FIRSTNAME_TEXTBOX, $userData[$user][1]);
	_setValue($REGISTER_LASTNAME_TEXTBOX, $userData[$user][2]);
	_setValue($REGISTER_EMAIL_TEXTBOX, $userData[$user][3]);
	_setValue($REGISTER_CONFIRM_EMAIL_TEXTBOX, $userData[$user][4]);
	_setValue($REGISTER_PASSWORD_TEXTBOX, $userData[$user][5]);
	_setValue($REGISTER_CONFIRM_PASSWORD_TEXTBOX, $userData[$user][6]); 	 
	if(_isVisible($REGISTER_SUBMIT_BUTTON))
	{
		_click($REGISTER_SUBMIT_BUTTON); 
	} 	 
} 

//******************************Addresses**************************

function deleteAddress()
{
	_click($HEADER_USERACCOUNT_LINK);
	clearWishList();
	_click($LEFTNAV_ADDRESS_LINK);
	//deleting the existing addresses
	if(_isVisible($ADDRESSES_DELETE_LINK))
	{
		var $Totalno_Address =_count("_div","/mini-address-location/", _in($ADDRESSES_ADDRESSES_LIST))
		_log($Totalno_Address);
		
		    //deleting the existing addresses
		for(var $i=1; $i<=$Totalno_Address; $i++)
				{
					_click($ADDRESSES_DELETE_LINK);
					_wait(2000);
					_expectConfirm("/Do you want/",true);
					_wait(3000);
		        }
		  _log($i);
		  	_assertNotVisible($ADDRESSES_ADDRESSES_LIST)
			 _assertNotVisible($ADDRESSES_DELETE_LINK);

	}
else
	{
	 _assertNotVisible($ADDRESSES_ADDRESSES_LIST)
	 _assertNotVisible($ADDRESSES_DELETE_LINK);
	}	
	
		
//	while(_isVisible($ADDRESSES_DELETE_LINK))
//	{
//		_click($ADDRESSES_DELETE_LINK);
//		_expectConfirm("/Do you want/",true);
//	}
}

function addAddress($validations,$r)
{
	_setValue($ADDRESSES_ADDRESSNAME_TEXTBOX, $validations[$r][1]);
	_setValue($ADDRESSES_FIRSTNAME_TEXTBOX, $validations[$r][2]);
	_setValue($ADDRESSES_LASTNAME_TEXTBOX, $validations[$r][3]);
	_setValue($ADDRESSES_ADDRESS1_TEXTBOX, $validations[$r][4]);
	_setValue($ADDRESSES_ADDRESS2_TEXTBOX, $validations[$r][5]);
	_setSelected($ADDRESSES_COUNTRY_DROPDOWN,$validations[$r][6]);
	_setSelected($ADDRESSES_STATE_DROPDOWN, $validations[$r][7]);
	_setValue($ADDRESSES_CITY_TEXTBOX, $validations[$r][8]);
	_setValue($ADDRESSES_ZIPCODE_TEXTBOX, $validations[$r][9]);
	_setValue($ADDRESSES_PHONE_TEXTBOX,$validations[$r][10]);	
}


//******************************Wish List**************************


function clearWishList()
{
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_WISHLIST_LINK);
	if (_isVisible($WISHLIST_SHIPPINGADDRESS_DROPDOWN))
	{
		_setSelected($WISHLIST_SHIPPINGADDRESS_DROPDOWN,0);	 
	}
	while(_isVisible($WISHLIST_REMOVE_BUTTON))
	{
		_click($WISHLIST_REMOVE_BUTTON);
	} 
}

function deleteAddeditemsFromWishlistAccount()
{
	//login to the application
	_click($HEADER_LOGIN_LINK);
	//login to application for which account we have added item
	login();
	//clearing items from wishlist
	clearWishList();
	//logout from application
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_LOGOUT_LINK);
}

function additemsToWishListAccount($cat)
{
	
//login to the application
_click($HEADER_LOGIN_LINK);
//login to application for which account we want to add item
login();
//clear items from wish list
clearWishList();
//add items
search($cat);
//selecting swatches
$PDP_pName=_getText($PDP_PRODUCTNAME);
selectSwatch();
//click on wish list
_click($WISHLIST_ADDTOWISHLIST_LINK);
//navigate to wish list
_click($HEADER_WISHLIST_LINK);
//click on make this as public if it is not having public visibility
if (_isVisible($WISHLIST_MAKETHISPUBLIC_BUTTON))
{
	_click($WISHLIST_MAKETHISPUBLIC_BUTTON);
}
//logout from application
_click($HEADER_USERACCOUNT_LINK);
_click($HEADER_LOGOUT_LINK);
	
}

function WishlistProductCount()
{
	var $WISHLIST_PRODUCT_COUNT=_count("_div","/name/",_in($WISHLIST_ITEM_LIST));
	return $WISHLIST_PRODUCT_COUNT;	
}

//functions
function navigateToFindSomeOneWishlist()
{
	//navigating to wish list login page
	_navigateTo($URL[4][0]);
	//enter email id
	_setValue($WISHLIST_EMAIL_TEXTBOX,$uId);
	//click on find button present in find some one's wish list page
	_click($WISHLIST_FIND_BUTTON);	
}


//function
function emptyWishlistUI()
{
	
_assertVisible($PAGE_BREADCRUMB);
_assertVisible(_link($Wishlist_Login[0][0], _in($PAGE_BREADCRUMB)));
//_assertVisible(_paragraph("/(.*)/",_under(_div("form-inline"))));
//left nav section
var $Heading=_count("_span", "/(.*)/",_in(_div("secondary-navigation")));
var $Links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
//checking headings
for(var $i=0;$i<$Heading;$i++)
       {
              _assertVisible(_span($Wishlist_Login[$i][9]));                
       }
//checking links
for(var $i=0;$i<$Links;$i++)
       {
              _assertVisible(_link($Wishlist_Login[$i][10]));                
       }
//need help section
_assertVisible(_div("Need Help?"));
//left nav content asset(contact us)
_assertVisible($LEFTNAV_CONTENTASSET);

//fields
_assertVisible(_label($Wishlist_Login[2][7]));
_assertVisible(_label($Wishlist_Login[3][7]));
_assertVisible($WISHLIST_LASTNAME_TEXTBOX);
_assertEqual("", _getValue($WISHLIST_LASTNAME_TEXTBOX));
_assertVisible($WISHLIST_FIRSTNAME_TEXTBOX);
_assertEqual("", _getValue($WISHLIST_FIRSTNAME_TEXTBOX));
_assertVisible($WISHLIST_EMAIL_TEXTBOX);
_assertEqual("", _getValue($WISHLIST_EMAIL_TEXTBOX));
//shipping address drop down
_assertVisible(_label("Your Wish List Shipping Address"));
_assertVisible($WISHLIST_SHIPPINGADDRESS_DROPDOWN);
_assertEqual($Wishlist_Login[11][1], _getSelectedText($WISHLIST_SHIPPINGADDRESS_DROPDOWN));

}

//******************************Payment Information**************************
//credit card info
function CreateCreditCard($i,$Createcard, $boolean)
{

	_setValue($PAYMENT_NAMETEXTBOX,$Createcard[$i][1]);
	_setSelected($PAYMENT_CARDTYPE_DROPDOWN,$Createcard[$i][2]);
	_setValue($PAYMENT_NUMBER_TEXTBOX,$Createcard[$i][3]);
	_setSelected($PAYMENT_MONTH_DROPDOWN,$Createcard[$i][4]);
	_setSelected($PAYMENT_YEAR_DROPDOWN,$Createcard[$i][5]);

	if($boolean==true)
	{
		_setValue($PAYMENT_FIRSTNAME_TEXTBOX,$Createcard[$i][6]);
		_setValue($PAYMENT_LASTNAME_TEXTBOX,$Createcard[$i][7]);
		_setValue($PAYMENT_ADDRESS1_TEXTBOX,$Createcard[$i][8]);
		_setValue($PAYMENT_ADDRESS2_TEXTBOX,$Createcard[$i][9]);
		_setSelected($PAYMENT_COUNTRY_TEXTBOX,$Createcard[$i][10]);
		_setValue($PAYMENT_CITY_TEXTBOX,$Createcard[$i][11]);
		_setSelected($PAYMENT_STATE_TEXTBOX,$Createcard[$i][12]);
		_setValue($PAYMENT_ZIPCODE_TEXTBOX,$Createcard[$i][13]);
		_setValue($PAYMENT_PHONE_TEXTBOX,$Createcard[$i][14]);
		_setValue($PAYMENT_EMAIL_FIELD, $uId);
	}
}


function closedOverlayVerification()
{
	_assertNotVisible($PAYMENT_OVERLAY);
	//heading
	_assertVisible($PAYMENT_CREDITCARD_INFORMATION_HEADING);
	_assertVisible($PAYMENT_ADDCREDITCARD);           
}

function DeleteCreditCard()
{ 
	
//verifying the address in account
_click($HEADER_MYACCOUNT_LINK);
_click(_link("My Account"));
_click(_link("user_account"));
_click($MY_ACCOUNT_PAYMENTSETTING_OPTIONS);
_wait(5000);

while (_isVisible($PAYMENT_DELETECARD_LINK))
{
_click($PAYMENT_DELETECARD_LINK);
}

if(_isVisible($PAYMENT_DELETECARD_LINK))
	{
		var $Count = _count("_submit","/Delete/",_in(_list("payment-list")));
		_log($Count);
		//click on delete card link
		for(var $i=0;$i<$Count;$i++)
	      {
	     	   _byPassWaitMechanism(true);
			   _click($PAYMENT_DELETECARD_LINK); 
			   _expectConfirm("/Do you want to remove this credit card/",true);
			
				//click on ok in confirmation overlay
				
			   	_focusWindow();
			   // CTRL+R will reload this page using robot events
			    _wait(3000);   
				var $robot = new java.awt.Robot();
				$robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
				_wait(500);
				$robot.keyPress(java.awt.event.KeyEvent.VK_R);
				_wait(500);
				$robot.keyRelease(java.awt.event.KeyEvent.VK_R);
				_wait(500);
				$robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
				_wait(500);
	      }
}	
//verify whether deleted or not
_assertNotVisible($PAYMENT_PAYMENTLIST);
_assertNotVisible($PAYMENT_DELETECARD_LINK);
	
}



//########################## Static Pages Global Functions #############################

function RequestCatalog($sheet,$i)
{
	
//entering all the values in request a catalog
_setSelected($RC_TITLE_DROPDOWN,$sheet[$i][1]);
_setValue($RC_FIRSTNAME_TEXTBOX,$sheet[$i][2]);
_setValue($RC_MIDDLENAME_TEXTBOX,$sheet[$i][3]);
_setValue($RC_LASTNAME_TEXTBOX,$sheet[$i][4]);
_setSelected($RC_SUFFIX_DROPDOWN,$sheet[$i][5]);
_setValue($RC_ADDRESS1_TEXTBOX,$sheet[$i][6]);
_setValue($RC_ADDRESS2_TEXTBOX,$sheet[$i][7]);
_setValue($RC_ADDRESS2_APRTNUMBER_TEXTBOX,$sheet[$i][8]);
_setValue($RC_CITY_TEXTBOX,$sheet[$i][9]);
_setSelected($RC_STATE_TEXTBOX,$sheet[$i][10]);
_setValue($RC_ZIPCODE_TEXTBOX,$sheet[$i][11]);

}

//############# Left Nav links and headings count ###############

function HeadingCountInLeftNav()
{
	var $countOfHeadings=_collect("_span", "/(.*)/",_in(_div("secondary")));
	return $countOfHeadings;
	
}

function LinkCountInLeftNav()
{
	var $countOfLinks=_collect("_link", "/(.*)/",_in(_div("secondary")));
	return $countOfLinks;		
}

function LeftNavHeadingCount()
{

	var $leftNavHeading=_count("_span","toggle",_in(_div("secondary-navigation acc-leftnav")));
	return $leftNavHeading;

}

function LeftNavLinkCount()
{

	var $leftNavLinkCount=_count("_link","/(.*)/",_in(_div("secondary-navigation acc-leftnav")));
	return $leftNavLinkCount;

}