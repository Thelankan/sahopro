_includeOnce("../ObjectRepository/Generic_OR.sah");

var $widthBoolean=false;
var $colorBoolean=false;
var $sizeBoolean=false;
var $inseamBoolean=false;
var $waistBoolean=false;
var $pdpPrice;
var $quickViewPrice;
var $fitBoolean;

//******************************Search**************************
 //search related function
 function search($product)
 {
 	if(isMobile())
 		{
 			_navigateTo(document.getElementsByName("simpleSearch")[0].getAttribute("action") + "?q=" + $product);
 		}
 	else
 		{
 			_setValue($HEADER_SEARCH_TEXTBOX, $product);	
 			_click($HEADER_SEARCH_ICON);
 		}
 	
 }
  
//******************************PLP**************************
 
 function navigateRootCatPage($RootCat)
 {
	 var $ROOTCAT=categoryLink($RootCat);
	 _click($ROOTCAT);
	 _wait(2000);
 } 
 
function navigateCatPage($RootCat, $Category)
{
	//Click on root category
	var $ROOTCAT=categoryLink($RootCat);
	 _click($ROOTCAT);
	//Click on sub category
	 var $CAT=categoryLink($Category);
	 _click($CAT);
}

function navigateSubCatPage($RootCat, $Category, $SubCategory)
{
	//Click on root category
	var $ROOTCAT=categoryLink($RootCat);
	 _click($ROOTCAT);
	//Click on category
	 var $CAT=categoryLink($Category);
	 _click($CAT);
	//Click on sub category
	 var $SUBCAT=categoryLink($SubCategory);
	 _click($SUBCAT);
}


function navigateToBundlePDP($RootCat, $Category, $SubCategory)
{
	//Click on root category
	var $ROOTCAT=categoryLink($RootCat);
	 _click($ROOTCAT);
	//Click on category
	 var $CAT=categoryLink($Category);
	 _click($CAT);
	//Click on sub category
	 var $SUBCAT=categoryLink($SubCategory);
	 _click($SUBCAT);
	 //var $PName=_getText($PLP_PRODUCTNAME_LINK);	
	var $Products = _collectAttributes("_link","/name-link/", "sahiText", _in($PLP_PRODUCT_LIST));
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	for(var $i=0; $i<$Products.length;$i++)
		{
			if(!_isVisible($PDP_SET_LIST))
				{
					_click($PDP_NEXT_LINK);					
				}
			var $PName=$Products[$i];
		}
		
	return $PName;
}

function fetchingPricePDP()
{

if (_isVisible($PDP_PRODUCTPRICE))
{
	$pdpPrice=_extract(_getText($PDP_PRODUCTNAME),"/[$](.*)/",true);
}
else
{
	$pdpPrice=_extract(_getText($PDP_PRODUCT_STANDARED_PRICE),"/[$](.*)/",true);
}

return $pdpPrice;
	
}

function fetchingPriceQuickView()
{

if (_isVisible($PDP_PRODUCTPRICE))
{
	$quickViewPrice=_extract(_getText($PDP_PRODUCTNAME, _in(_div("QuickViewDialog"))),"/[$](.*)/",true);
}
else
{
	$quickViewPrice=_extract(_getText($PDP_PRODUCT_STANDARED_PRICE, _in(_div("QuickViewDialog"))),"/[$](.*)/",true);
}

return $pdpPrice;
	
}


//******************************PDP**************************

 /* function selectSwatch()
 {
		_wait(3000);
		//select swatches
		if(_isVisible($PDP_COLOR_SECTION))
		    {
		    $colorBoolean=true;
		    _wait(2000);
		    if(!_isVisible($PDP_COLOR_LISTITEM_SELECTED))
		           {
		           _click($PDP_COLOR_SWATCH);
		           }
		    }
		_wait(2000);
		if(_isVisible($PDP_SIZE_SECTION))
		    {
		    $sizeBoolean=true;
		    if(!_isVisible($PDP_SIZE_LISTITEM_SELECTED))
		           {
		           _click($PDP_SIZE_SWATCH);
		           }
		    }
		_wait(2000);
		if(_isVisible($PDP_WIDTH_SECTION))
		    {
		    $widthBoolean=true;
		           if(!_isVisible($PDP_WIDTH_LISTITEM_SELECTED))
		           {
		           _click($PDP_WIDTH_SWATCH);
		           }
		    }
 } */
 

function selectGiftCardSwatch()
{

if (_isVisible($GIFTCARD_SELECTAMOUNT_LABEL))
{
	
	if (!_isVisible($GIFTCARD_PRICEVALUE_LISTITEM_SELECTED))
	{
		_click($GIFTCARD_PRICEVALUE_SWATCH_SELECTABLE);
	}
	
}
	
if (_isVisible($GIFTCARD_SELECTDESIGN_LABEL))
{
	
	if (!_isVisible($GIFTCARD_SELECTDESIGN_LISTITEM_SELECTED))
		{
			_click($GIFTCARD_SELECTDESIGN_SWATCH_SELECTABLE);
		}
	
}
	
}

var $productSizeInPDP;
var $productColorInPDP;
function selectSwatch()
 {

					//Inseam swatch	
					if (_isVisible($PDP_INSEAM_LABEL))
					{
						 
					 $inseamBoolean=true;
					 _wait(2000);
					 if(!_isVisible($PDP_INSEAM_LISTITEM_SELECTED))
						 {
						 	_click($PDP_INSEAM_SWATCH);
						 }
					}
					
					//Size label
					if (_isVisible($PDP_SIZE_LABEL))
					{
						 
					 $sizeBoolean=true;
					 _wait(2000);
						 if(!_isVisible($PDP_SIZE_LISTITEM_SELECTED))
						 {
							 _click($PDP_SIZE_SWATCH);
						 }
						 $productSizeInPDP = _getText(_listItem("selectable selected", _in(_list("swatches size"))));
						 _log("$productSizeInPDP:-  "+ $productSizeInPDP);
					}
					
					//waist size
					if(_isVisible($PDP_WAIST_LABEL))
					{
					$widthBoolean=true;
					       if(!_isVisible($PDP_WAIST_LISTITEM_SELECTED))
					       {
					       _click($PDP_WAIST_SWATCH);
					       }
					}
					
					//Width label
					if(_isVisible($PDP_WIDTH_LABEL))
					{
					$waistBoolean=true;
					       if(!_isVisible($PDP_WIDTH_LISTITEM_SELECTED))
					       {
					       _click($PDP_WIDTH_SWATCH);
					       }
					}
					
					//color label
					if(_isVisible($PDP_COLOR_LABEL))
					{
					$colorBoolean=true;
					_wait(2000);
					if(!_isVisible($PDP_COLOR_LISTITEM_SELECTED))
					       {
					       _click($PDP_COLOR_SWATCH);
					       }
					$productColorInPDP = _getText(_span("selected-value", _in(_listItem("/color/"))));
					_log("$productColorInPDP:-  "+ $productColorInPDP);
					
					}
					
					//Fit swatch
					if(_isVisible($PDP_FIT_LABEL))
					{
					$fitBoolean=true;
					_wait(2000);
					if(!_isVisible($PDP_FIT_LISTITEM_SELECTED))
					       {
					       _click($PDP_FIT_SWATCH);
					       }
					}
					
					//Select Country Made
					if(_isVisible($PDP_COUNTRY_LABEL))
					{
						_setSelected(_select("va-CountryMade"), "1");
					}

}
	 
 function verifyNavigation($PName)
 {
 	_assertVisible($PDP_PAGE_BREADCRUMB);
 	_assertVisible(_span($PName, _in($PDP_PAGE_BREADCRUMB)));
 	_assertVisible($PDP_PRODUCTNAME);
 	_assertEqual($PName, _getText($PDP_PRODUCTNAME));	
 }
 
 var $productNameInPDP; 
 var $productQuantityInPDP;
 var $productSKUinPDP;
 var $productPriceInPDP;
 
function addToCartSearch($Item,$qty)
{
	//search product
	search($Item);
	//click on product name if it is visible
	if (_isVisible($PLP_PRODUCT_LIST))
		{
		_click($PLP_PRODUCTNAME_LINK);
		}
	//selecting swatches
	selectSwatch();
	$productNameInPDP = _getText($PDP_PRODUCTNAME);
	_log("$productNameInPDP:-  "+ $productNameInPDP);
	$productSKUinPDP = _getText(_span("/(.*)/", _in(_div("product-number"))));
	_log("$productSKUinPDP:-  "+ $productSKUinPDP);
	//set the quantity
	_setValue($PDP_QUANTITY_TEXTBOX,$qty);
	$productQuantityInPDP = _getText($PDP_QUANTITY_TEXTBOX);
	_log("$productQuantityInPDP:-  "+ $productQuantityInPDP);
	
	$productPriceInPDP = _getText(_span("price-sales"));
	_log("$productPriceInPDP:-  "+ $productPriceInPDP);
	//click on add to cart
	_click($PDP_ADDTOCART_BUTTON);
}

function addItemToCart($subCat,$i)
{
	//navigating to sub category
	_click(_link($subCat));
	//navigate to PDP
	_click($PLP_PRODUCTNAME_LINK);
	//selecting swatches
	selectSwatch();
	//set the quantity
	_setValue(_textbox("Quantity"),$i);
	//click on add to cart
	_click(_submit("/add-to-cart/"));
}

function addGiftCertificate($Data, $i)
{
	//click on gift certificate
	_click($FOOTER_GIFTCERTIFICATES_LINK);
	//enter the gift certificate details
	addGiftCertificate($Data, $i);
	//click on add to cart
	_click($GIFTCERTIFICATE_ADDTOCART);
}

function GiftCertificate($Data, $i)
{
	_setValue($GIFTCERTIFICATE_YOURNAME, $Data[$i][1]);
	_setValue($GIFTCERTIFICATE_FRIENDSNAME, $Data[$i][2]);
	_setValue($GIFTCERTIFICATE_FRIENDSMAIL, $Data[$i][3]);
	_setValue($GIFTCERTIFICATE_CONFIRM_FRIENDSMAIL, $Data[$i][4]);
	_setValue($GIFTCERTIFICATE_MESSAGE, $Data[$i][5]);
	_setValue($GIFTCERTIFICATE_AMOUNT, $Data[$i][6]);	
}
 


function VerificationOfArticles()
{
	
	//Article Count
	var $articleCount=_count("_link","content-title",_in(_list("folder-content-list")));
	if ($articleCount==6)
		{
		_assertVisible(_div("/View All Results/"));
		}
	else
		{
		_assertNotVisible(_div("/View All Results/"));
		}	
}
 

//--------Pagination function
function pagination()
{  
	if(_getText($PLP_RESULTHITS_TOP)==_getText($PLP_PAGINATION_TOP))
		{
			_assert(true,"Single Page is displayed");
			VerificationOfArticles();
		}
	else
		{
			if((_isVisible(_link("page-next")))&&(_isVisible(_link("page-last"))))
				{	
					_assert(_isVisible(_link("page-next")));
					_assert(_isVisible(_link("page-last")));
					_assertEqual(false,_isVisible(_link("page-previous")));
					_assertEqual(false,_isVisible(_link("page-first")));
					if(_isVisible(_link("page-last")))
						{
							_click(_link("page-last"));
						}
					_assert(_isVisible(_link("page-previous")));
					_assert(_isVisible(_link("page-first")));
					_assertEqual(false,_isVisible(_link("page-next")));
					_assertEqual(false,_isVisible(_link("page-last")));
					var $pages=_getText(_listItem("current-page"));
					if(_isVisible(_link("page-first")))
						{
							_click(_link("page-first"));
						}				
					for(var $j=1;$j<=$pages;$j++)
						{
							if($j==1)
								{
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);
								}
							else
								{
									_click(_link("page-next"));
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);					
								}				
						}
					for(var $j=$pages;$j>=1;$j--)
						{
							if($j==$pages)
								{
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);
								}
							else
								{
									_click(_link("page-previous"));
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);						
								}				
						}
					for(var $j=1;$j<=$pages;$j++)
						{
							if($j==1)
								{
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);
								}
							else
								{
									_click(_link("page-"+$j));
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);							
								}				
						}
				 }
			else
				 {
					var $pageDisp=_count("_listItem","/(.*)/",_in($PLP_PAGINATION_TOP));
					for(var $j=1;$j<=$pageDisp;$j++)
						{
							if($j==1)
								{
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);
								}
							else
								{
									_click(_link("page-"+$j));
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);					
								}				
						}				
				 }
		}
}

//--------Items per page function---------------
function ItemsperPage($itemsExpected)
{
	var $pagination=_getText($PLP_PAGINATION_TOP);
	var $ActTotalItems=_extract($pagination, /of (.*) Results/, true).toString();
	var $ExpTotalItems=0;
	var $itemsDisplayed=0;
	if(_getText($PLP_RESULTHITS_TOP)==_getText($PLP_PAGINATION_TOP))
		{
		
			$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));//_count("_div", "/product-tile/",_in(_list("search-result-items")));
			_assertEqual($itemsDisplayed,$ActTotalItems, "Single Page is displayed");		
		}
	else
		{
			if((_isVisible(_link("page-next")))&&(_isVisible(_link("page-last"))))
			{	
				
				while(_isVisible(_link("page-last")))
					{
						_click(_link("page-last"));
					}
				var $pages=_getText(_listItem("current-page"));
				while(_isVisible(_link("page-first")))
					{
						_click(_link("page-first"));
					}
				
				for(var $j=1;$j<=$pages;$j++)
					{
						if($j==1)
							{
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								_assertEqual($itemsExpected,$itemsDisplayed);
							}
						else if($j==$pages)
							{
								_click(_link("page-"+$j));
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								var $LastPageItems=$ActTotalItems%$itemsExpected;						
								_assertEqual($LastPageItems,$itemsDisplayed);
							}
						else 
							{
								_click(_link("page-"+$j));
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								_assertEqual($itemsExpected,$itemsDisplayed);
							}
						$ExpTotalItems=$ExpTotalItems+$itemsDisplayed;
					 }				
				}
			else
				{
					var $pageDisp=_count("_listItem","/(.*)/",_in($PLP_PAGINATION_TOP));
					for(var $j=1;$j<=$pageDisp;$j++)
					{
						if($j==1)
							{
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								_assertEqual($itemsExpected,$itemsDisplayed);
							}
						else if($j==$pageDisp)
							{
								_click(_link("page-"+$j));
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								var $LastPageItems=$ActTotalItems%$itemsExpected;						
								_assertEqual($LastPageItems,$itemsDisplayed);
							}
						else 
							{
								_click(_link("page-"+$j));
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								_assertEqual($itemsExpected,$itemsDisplayed);
							}
						$ExpTotalItems=$ExpTotalItems+$itemsDisplayed;
					 }				
				}
			_assertEqual($ExpTotalItems,$ActTotalItems);
		}
}


//Price filter
function priceCheck($Range)
{
	var $priceBounds = _extract(_getText($Range), "/[$](.*) - [$](.*)/",true).toString().split(",");
	var $minPrice = $priceBounds[0].toString();
	var $maxPrice = $priceBounds[1].toString();
	_log("$minPrice = " + $minPrice);
	_log("$maxPrice = " + $maxPrice);
	if(_getText($PLP_RESULTHITS_TOP)==_getText($PLP_PAGINATION_TOP))
	{
		var $prices = _collectAttributes("_span", "/product-sales-price/","sahiText");
		for (var $k = 0; $k < $prices.length; $k++) 
			{
				var $price = parseFloat(_extract($prices[$k],"/[$](.*)/",true)[0].replace(",",""));				
				_log("$price = " + $price);				
				if ($price < $minPrice) 
				{
					_assert(false, "Price " + $price + " is lesser than min price " + $minPrice);
				} 
				else if ($price > $maxPrice) 
				{
					_assert(false, "Price " + $price + " is greater than max price " + $maxPrice);
				}					
			}		
	}
	else
	{
		if((_isVisible(_link("page-next")))&&(_isVisible(_link("page-last"))))
			{
				if(_isVisible(_link("page-last")))
					{
						_click(_link("page-last"));
					}
				var $pages=_getText(_listItem("current-page"));
				if(_isVisible(_link("page-first")))
					{
						_click(_link("page-first"));
					}
			}
		else
			 {
				var $pages=_count("_listItem","/(.*)/",_in($PLP_PAGINATION_TOP));
			 }						
		for(var $j=1;$j<=$pages;$j++)
			{
				if(!$j==1)
					{
						_click(_link("page-next"));							
					}
				var $prices = _collectAttributes("_span", "/product-sales-price/","sahiText");
				for (var $k = 0; $k < $prices.length; $k++) 
					{
						var $price = parseFloat(_extract($prices[$k],"/[$](.*)/",true)[0].replace(",",""));				
						_log("$price = " + $price);				
						if ($price < $minPrice) 
						{
							_assert(false, "Price " + $price + " is lesser than min price " + $minPrice);
						} 
						else if ($price > $maxPrice) 
						{
							_assert(false, "Price " + $price + " is greater than max price " + $maxPrice);
						}					
					}
			}
	}
}

//--------Sort By function--------------
function sortBy($opt)
{ 
	if($opt=="Price Low To High")
	{	
		sortAllPage($opt,"_span", "/product-sales-price/");
	}     
else if($opt=="Price High To Low")
	{
		sortAllPage($opt,"_span","/product-sales-price/");
	}
else if($opt=="Product Name A - Z")
	{
		sortAllPage($opt,"_link","/name-link/");            
	}
else if($opt=="Product Name Z - A")
	{
		sortAllPage($opt,"_link","/name-link/");        
	}	    
}

function sortAllPage($opt,$tag,$field)
{
	var $arr = new Array();
	var $temp =new Array();
	if(_getText($PLP_RESULTHITS_TOP)==_getText($PLP_PAGINATION_TOP))
		{
			$arr = _collectAttributes($tag,$field,"sahiText");
			assertValues($opt,$arr);		
		}
	else
		{
			if((_isVisible(_link("page-next")))&&(_isVisible(_link("page-last"))))
			{	
				
				while(_isVisible(_link("page-last")))
					{
						_click(_link("page-last"));
					}
				var $pages=_getText(_listItem("current-page"));
				while(_isVisible(_link("page-first")))
					{
						_click(_link("page-first"));
					}					
					for(var $i=1;$i<=$pages;$i++)
						{
							if($i==1)
								{
									$temp = _collectAttributes($tag,$field,"sahiText");
									$arr.push($temp);
								}
							else
								{
									_click(_link("page-"+$i));
									$temp = _collectAttributes($tag,$field,"sahiText");
									$arr.push($temp);									
								}
						}
					assertValues($opt,$arr);
				}
			else
				{
					var $pageDisp=_count("_listItem","/(.*)/",_in($PLP_PAGINATION_TOP));
					for(var $i=1;$i<=$pageDisp;$i++)
						{
							if($i==1)
								{
									$temp = _collectAttributes($tag,$field,"sahiText");
									$arr.push($temp);								
								}
							else
								{
									_click(_link("page-"+$i));
									$temp = _collectAttributes($tag,$field,"sahiText");
									$arr.push($temp);																	
								}				
						}
					assertValues($opt,$arr);
				}
		}  	   
}

function assertValues($opt,$arr)
{
 	_log("Collected array-----:"+$arr);
    var $arrExp=$arr.sort();
	 if($opt=="Product Name Ascending" ||$opt=="Price Low To High")
		{
	 		$arrExp=$arr.sort();
		}
	 else if($t=="Product Name Descending" ||$opt=="Price High to Low")
		{
	  		$arrExp=$arr.sort().reverse();
		} 
	 _log("Sorted array-----:"+$arrExp);
    _assertEqualArrays($arrExp,$arr);
}

//Reveiew overlay
function ReviewValuesInOverlay($sheet,$i)
{

//click on review link
_click(_link("bv-radio-rating-4"));
//select value 	
_setValue($PDP_ROVERLAY_TITLE_TEXTBOX,$sheet[0][$i]);
_setValue($PDP_ROVERLAY_REVIEW_TEXTAREA,$sheet[1][$i]);
_setValue($PDP_ROVERLAY_NIKNAME_TEXTBOX,$sheet[2][$i]);
_setValue($PDP_ROVERLAY_USERLOCATION_TEXTBOX,$sheet[3][$i]);
_setValue($PDP_ROVERLAY_USER_EMAIL,$sheet[4][$i]);
_setSelected($PDP_ROVERLAY_GENDER_DROPDOWN,$sheet[5][$i]);
_setSelected($PDP_ROVERLAY_AGE_DROPDOWN,$sheet[6][$i]);
_setSelected($PDP_ROVERLAY_SHOP_DROPDOWN,$sheet[7][$i]);

}

