_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("Search_Data.xls");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


//##################### Search Suggestions ###########################
var $t=_testcase("338445","Verify the  behaviors when user enters keyword in the search field  and  press 'ENTER' or Taping on 'SEARCH ' icon for guest and authenticated user.");
$t.start();
try
{

//Navigate to search result page
search($SearchResult[0][0]);
//bread crumb
_assertVisible($SEARCH_RESULT_ITEMS);
_assertEqual($SearchResult[0][2]+" \""+$SearchResult[0][0]+"\"",_getText($PAGE_BREADCRUMB_RESULT_TEXT));
//Navigate to search result page
search($SearchResult[0][5]);
//Should navigate to no search result page
_assertEqual($SearchResult[1][2]+" "+$SearchResult[0][5],_getText(_paragraph("/(.*)/", _in($SEARCH_SUGGESTION_SECTION_HEADER))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("338473/338474","Verify the navigation related to  suggested static Page Links in Left Pane of search suggestion overlay  in application both as an  Anonymous and  Registered  user./Verify the UI of the Price and name displayed for the suggested products in search suggestion overlay in application both as an  Anonymous and  Registered  user..");
$t.start();
try
{

//Navigate to search result page
_setValue($HEADER_SEARCH_TEXTBOX,$SearchResult[0][0]);
//product name should display
_assertVisible($SEARCH_SUGGESTION_PRODUCTNAME);
//product price should display
_assertVisible($SEARCH_SUGGESTION_PRODUCTPRICE);
//click on any link in suggested page
_click($SEARCH_SUGGESTION_PRODUCT_NAMELINK);
//SHOULD NAVIGATE TO RESPECTIVE PAGE
_assertVisible($PDP_MAIN);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("338475","Verify the UI of search suggestion overlay when user types a Category name  in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{

//set value	
_setValue($HEADER_SEARCH_TEXTBOX,$SearchResult[0][0]);
//"Search for \" men \" ?"
_assertEqual($SearchResult[0][6]+" \" "+$SearchResult[2][2]+" \" ?", _getText($SEARCH_SUGGESTION_SEARCH_PHRASE));
_assertVisible($SEARCH_SUGGESTION_PHRASE);
_assertVisible($SEARCH_SUGGESTION_INTERSTING_PAGE);
//product count
var $searchSuggesionProductCount=SearchSuggestionProductCount();

for (var $i=0;$i<$searchSuggesionProductCount.lenght;$i++)
{

//ProductName
_assertVisible(_div("product-name["+$i+"]"));
//ProductPrice
_assertVisible(_div("product-price["+$i+"]"));

}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("338476","Verify the hover and on click functionality on  Product name and image Link in Right Pane of search suggestion overlay  in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{

//set value	
_setValue($HEADER_SEARCH_TEXTBOX,$SearchResult[0][0]);

//product count
var $searchSuggesionProductCount=SearchSuggestionProductCount();

for (var $i=0;$i<$searchSuggesionProductCount;$i++)
{
	
//ProductName
var $productName=_getText($SEARCH_SUGGESTION_PRODUCTNAME);
_click($SEARCH_SUGGESTION_PRODUCTNAME);
//Should navigate to PDP
_assertVisible($PDP_MAIN);
_assertEqual($productName,_getText($PDP_PRODUCTNAME));
//set value	
_setValue($HEADER_SEARCH_TEXTBOX,$SearchResult[0][0]);
//ProductPrice
var $productPrice=_getText($SEARCH_SUGGESTION_PRODUCTPRICE);
_click($SEARCH_SUGGESTION_PRODUCTPRICE);
_assertEqual($productPrice,_getText($PDP_PRODUCTPRICE));

}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("338477/338479","Verify the functionality of hover and click on  links in the right  Pane of search suggestion overlay in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{

//set value	ON SEARCH FIELD
_setValue($HEADER_SEARCH_TEXTBOX,$SearchResult[0][0]);
//SEARCH LINK IN LEFT PANE
var $searchLink=_getText($SEARCH_LINK_IN_LEFTPANE);
_click($SEARCH_LINK_IN_LEFTPANE);
//should navigate to category landing page
_assertEqual($searchLink,_getText($PAGE_BREADCRUMB_LINK));

//set value	ON SEARCH FIELD
_setValue($HEADER_SEARCH_TEXTBOX,$SearchResult[0][0]);
//Search link in left pane
var $searchLinkIntrestingPage=_getText($SEARCH_INTERSTINGPAGE_LINK_IN_LEFTPANE);
_click(_link($searchLinkIntrestingPage));
//Should navigate to category landing page
_assertEqual($searchLinkIntrestingPage,_getText($PAGE_BREADCRUMB_LINK));
	
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//##################### No Search Result Page #######################################

var $t = _testcase("343329/343330/343331", "Verify the navigation to search no result page from home page, the UI of search no result page and the UI of 'REFINE SEARCH' section on search no result page  left nav in application both as an  Anonymous and  Registered  user." +
		"Verify the navigation to search no result page from home page in application both  as guest and a registered user.");
$t.start();
try
{
	 //search a product 
	 search($SearchResult[0][3]);
	 //heading
	 _assertVisible($NOSEARCHRESULT_HEADING);
	 //text
	 _assertEqual($SearchResult[1][4],_getText($NOSEARCHRESULT_TEXT));
	 _assertVisible($NOSEARCHRESULT_CONTENTASSET);
	 //label
	 _assertVisible($NOSEARCHRESULT_TRYNEW_SEARCH_LABEL);
	 //try new search 
	 _assertVisible($NOSEARCHRESULT_TRYANEW_SEARCH);
	 _assertVisible($NOSEARCHRESULT_TRYANEW_SEARCH_GOBUTTON);
	 _assertVisible($NOSEARCHRESULT_NOHITS_FOOTER_CONTENTASSET);
	 //refine search
	 _assertVisible($PLP_REFINEMENT_HEADING);
	 _assertEqual($SearchResult[2][4], _getText($PLP_REFINEMENT_HEADING));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("343332", "Verify the UI of 'Refine search' section in No Search Results page  left Nav in application both as guest and a registered user.");
$t.start();
try
{
	search($SearchResult[0][3]);
	//var $Filters=_collect("_heading3","/toggle/",_in($PLP_REFINEMENT));
	var $Filters=_collect("_link", "/refinement-link/", _in(_div("refinement Category")));
	var $FilterNames=_collectAttributes("_link", "/refinement-link/","sahiText",_in(_div("refinement Category")));
	//var $FilterNames=_collectAttributes("_heading3","/toggle/","sahiText",_in($PLP_REFINEMENT));
	for(var $i=0;$i<$Filters.length;$i++)
		{	
			//click on filter
			_click($Filters[$i]);
			//filter will collapse
			var $list=swatchlist($FilterNames[$i]);
			_assertNotVisible($list);
			//click on filter
			_click($Filters[$i]);
			//filter will expand
			_assertNotVisible($list);
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("343334/343333","Verify the functionality by clicking on suggested word in No Search Results page both as guest and a registered user./Verify the display of misspelled words and did you mean in No Search Results page both as guest and a registered user.");
$t.start();
try
{

//Navigate to no search result page
search($SearchResult[0][3]);
//No search result images
_assertVisible(_paragraph($SearchResult[3][4]+" "+$SearchResult[0][3]));
_assertVisible(_paragraph($SearchResult[4][4]+" "+$SearchResult[0][0]+"'s?"));
_click(_link("no-hits-search-term-suggest"));
//navigate to particular category page
_assertContainsText($SearchResult[2][2], _span("breadcrumb-element breadcrumb-result-text"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("343336", "Verify the functionality of  Try new search GO button  in no Search Results page both as guest and a registered user.");
$t.start();
try
{	
	
 //Nosearch results navigation
 search($SearchResult[0][3]);
 //heading
 _assertVisible($NOSEARCHRESULT_HEADING);
 //try new search 
 _setValue($NOSEARCHRESULT_TRYANEW_SEARCH, $SearchResult[0][0]);
 _click($NOSEARCHRESULT_TRYANEW_SEARCH_GOBUTTON);
//bread crumb
_assertVisible($PAGE_BREADCRUMB);
_assertEqual($SearchResult[0][2]+" \""+$SearchResult[0][0]+"\"", _getText($PAGE_BREADCRUMB));
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("343337", "Verify the functionality related to  Try new search 'GO' button for invalid keyword on search no result page in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	
 //Nosearch results navigation
 search($SearchResult[0][3]);
 //heading
 _assertVisible($NOSEARCHRESULT_HEADING);
 //try new search 
 _setValue($NOSEARCHRESULT_TRYANEW_SEARCH,$SearchResult[2][2]);
 _click($NOSEARCHRESULT_TRYANEW_SEARCH_GOBUTTON);
 //new search result's heading
 _assertContainsText($SearchResult[0][0],$PAGE_BREADCRUMB);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

//################# Search Result Page ###################

var $t = _testcase("343289/125409/343300", "Verify the UI of 'REFINE SEARCH'  on search result page  left Nav and results display in application both as an  Anonymous and  Registered  user./");
$t.start();
try
{
	search($SearchResult[0][0]);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($SearchResult[0][2]+" \""+$SearchResult[0][0]+"\"", _getText($PAGE_BREADCRUMB));
	//UI
	//refinement header
	_assertVisible($SEARCHRESULT_REFINEMENT_HEADING);
	//slot
	_assertVisible($SEARCHRESULT_CONTENT_SLOT);
	//leftnav
	_assertVisible($PLP_REFINEMENT);
	//sort by drop down
	_assertVisible($PLP_SORTBY_DROPDOWN_TOP);
	_assertVisible($PLP_SORTBY_DROPDOWN_BOTTOM);
	//items per page
	_assertVisible($PLP_ITEMSPER_PAGE_DROPDOWN);
	_assertVisible($PLP_ITEMSPER_PAGE_DROPDOWN_BOTTOM);
	//Display toggle
	_assert(true,"To check toggle in prevail");
	_assertVisible($PLP_TOOGLE_GRID);
	_assertVisible($PLP_TOGGLE_GRID_BOTTOM);
	//Pagination
	_assertVisible($PLP_PAGINATION_TOP);
	_assertVisible($PLP_PAGINATION_BOTTOM);
	_assertVisible($PLP_RESULTHITS_TOP);
	_assertVisible($PLP_RESULTHITS_BOTTOM);
	//products
	_assertVisible($PLP_RESULT_ITEMS);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("343290", "Verify the functionality related to  'Color' section on search result page left Nav in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($SearchResult[0][0]);
	var $Colors = _collect("_link","/swatch/",_in($PLP_LEFTNAV_COLOR_SWATCHES));
	var $ColorNames=_collectAttributes("_link","/swatch/","sahiText",_in($PLP_LEFTNAV_COLOR_SWATCHES));
	var $UnSelectable=_collectAttributes("_listItem","/unselectable swatch/","sahiText",_in($PLP_LEFTNAV_COLOR_SWATCHES));
	for(var $k=0; $k<$Colors.length; $k++)
	{
		if($UnSelectable.indexOf($ColorNames[$k])>=0)
			{
				_assert(true, "Swatch is unselectable");
			}
		else
			{
			 	_click($Colors[$k]);
			    //clear link
			 	_assertVisible($PLP_LEFTNAV_COLOR_REFINEMENT_CLEAR_LINK);
			 	_assertEqual($ColorNames[$k]+" x",_getText($PLP_BREADCRUMB_REFINEMENT_VALUE));
			 	_click($PLP_BREADCRUMB_REFINEMENT_CLOSE_LINK);
			 	_wait(4000,!_isVisible($PLP_LEFTNAV_COLOR_REFINEMENT_CLEAR_LINK));
			}  
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}		
$t.end();

var $t = _testcase("343293", "Verify the system's response on selection on Price filter from left nav  in the application both as an anonymous and a registered user");
$t.start();
try
{
	search($SearchResult[0][0]);
	var $PriceRange=_collect("_link","/refinement-link/",_in($PLP_LEFTNAV_PRICE_RANGE));
	var $PriceRangeValues=_collectAttributes("_link","/refinement-link/","sahiText",_in($PLP_LEFTNAV_PRICE_RANGE));
	for (var $k=0; $k< $PriceRange.length; $k++)
	{
		_click($PriceRange[$k]);
		_assertEqual($PriceRangeValues[$k]+" x", _getText($PLP_BREADCRUMB_REFINEMENT_VALUE));
		priceCheck($PriceRange[$k]);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("343292","Verify the system's response on selection on size filter from left Nav in the application both as an anonymous and a registered user.");
$t.start();
try
{
	search($SearchResult[0][0]);
	//var $size=_collect("_link","/Refine by Size/",_in($PLP_LEFTNAV_SIZE_SWATCHES));
	var $sizeValues=_collectAttributes("_link","/Refine by Size/","sahiText",_in($PLP_LEFTNAV_SIZE_SWATCHES));
	for (var $k=0; $k< $sizeValues.length; $k++)
	{
		//_click($size[$k]);
		_click(_link($sizeValues[$k],_in(_div("refinement size"))));
		_assertEqual($sizeValues[$k]+" x", _getText($PLP_BREADCRUMB_REFINEMENT_VALUE));
		//de selecting size selected
		_click(_link($sizeValues[$k],_in(_div("refinement size"))));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("343291", "Verify the click/tap functionality  on  filter refinement heading and default behavior of  all  the  filter refinement.");
$t.start();
try
{
	
//Search result page
search($SearchResult[0][0]);
//expand collapse links
var $expandCollapseLinks=_count("_italic","/pwm-refine-toggle/",_in(_div("refinements")));
//checking on expand and collapse links
for (var $k=0; $k< $expandCollapseLinks; $k++)
{
	_click(_italic("pwm-refine-toggle["+$k+"]"));
	_assertVisible(_heading3("toggle-refine expanded["+$k+"]"));
}
			
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("343297", "Verify the UI for the sort and pagination section from search result page");
$t.start();
try
{
	
search($SearchResult[0][0]);
if(_isVisible($PLP_SORTBY_DROPDOWN_TOP) && _isVisible($PLP_SORTBY_DROPDOWN_BOTTOM))
{
	var $sort=_getText($PLP_SORTBY_DROPDOWN_TOP);
	var $j=0;
	for(var $i=1;$i<$sort.length;$i++)
	{
		_setSelected($PLP_SORTBY_DROPDOWN_TOP, $sort[$i]);
		//Validating the selection
		_assertEqual($sort[$i],_getSelectedText($PLP_SORTBY_DROPDOWN_TOP),"Selected option is displaying properly");
		_assertEqual($sort[$i],_getSelectedText($PLP_SORTBY_DROPDOWN_BOTTOM),"Selected option is displaying properly");
		if($sort.indexOf($Category[$j][1])>-1)
		{
			_setSelected($PLP_SORTBY_DROPDOWN_TOP, $Category[$j][1]);
			sortBy($Category[$j][1]);
			$j++;
		}
	}
}
else
{
	_assert(false, "sort by drop down is not present");
}
_click($HEADER_HOMELOGO);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("343308", "Verify the click functionality of the links in the 'Search Results Products and Articles' page");
$t.start();
try
{	 

//Navigate to articles configured search result page
_log("Click on the search result page where Articles are configured");
search($SearchResult[0][0]);
//Click on Products found link
var $currentPos=window.document.body.scrollTop;
_log($currentPos);
//click on products found link
_click($SEARCH_PAGEFOUND_LINK);
var $changedPos=window.document.body.scrollTop;
_log($changedPos);
//scroll position should change
if ($changedPos < $currentPos) {
	_log("Scroll bar has moved up");
}

//Click on Results found link
var $currentPos=window.document.body.scrollTop;
_log($currentPos);
//click on products found link
_click($SEARCH_RESULTSFOUND_LINK);
var $changedPos=window.document.body.scrollTop;
_log($changedPos);
//scroll position should change
if ($changedPos < $currentPos) {
	_log("Scroll bar has moved up");
}

//click on Individual Article name
var $articleName=_getText($SEARCH_INDIVUDIAL_ARTICLE_NAME);
_click($SEARCH_INDIVUDIAL_ARTICLE_NAME);
//Should navigate to configured page
_assertEqual($articleName,_getText($PAGE_BREADCRUMB_LINK));

//Search result page
search($SearchResult[0][0]);
//click on read more link in indivudial article page
_click($SEARCH_ARTICLE_READMORE_LINK);
_assertEqual($SearchResult[0][0], _getText($PAGE_BREADCRUMB_LINK));

	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("343298", "Verify the functionality related to  'pagination' on search result page   in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($SearchResult[0][0]);
	pagination();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

if(!isMobile())
{
	var $t = _testcase("343299", "Verify the pagination and Items per page drop down behavior  in search result in the application both as an anonymous and a registered user.");
	$t.start();
	try
	{
		search($SearchResult[0][0]);
		if(_isVisible(_select("grid-paging-header")) && _isVisible(_select("grid-paging-footer")))
		{	
			var $itemsDisp=_getText(_select("grid-paging-header"));
			for(var $i=0;$i<$itemsDisp.length;$i++)
			{
				_setSelected(_select("grid-paging-header"), $itemsDisp[$i]);
				//Validating the selection
				_assertEqual($itemsDisp[$i],_getSelectedText(_select("grid-paging-header")),"Selected option is displaying properly");
				_assertEqual($itemsDisp[$i],_getSelectedText(_select("grid-paging-footer")),"Selected option is displaying properly at footer");
				ItemsperPage($itemsDisp[$i]);
			}		
		}
		else
		{
			_assert(true, "View drop down is not present");
		}
		_click(_image("logo.png"));
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();
}

//################# Only Article Page ###########################

var $t=_testcase("343341","Verify the UI of Search Results Articles Only page in application for both as guest and a registered user.");
$t.start();
try
{

//navigate to only articles page
search($SearchResult[0][7]);
//error heading
_assertEqual($error[0][5], _getText($ERROR_PAGE_HEADING));
//No search result images
_assertVisible(_paragraph($SearchResult[3][4]+" "+$SearchResult[0][7]));
//Error messages
_assertVisible(_listItem($SearchResult[0][8]));
_assertVisible(_listItem($SearchResult[1][8]));
_assertVisible(_listItem($SearchResult[2][8]));
_assertVisible(_paragraph($SearchResult[3][8]));
_assertVisible(_div($SearchResult[4][8]));
_assertVisible($ERROR_PAGE_EMAIL_LINK);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("343342","Verify the functionality by clicking Other results link in Search Results Articles Only page  both for guest and registered user.");
$t.start();
try
{

//Click on 1 or other link	
_click($SEARCH_ARTICLE_ONOTHER_LINK);
_assertEqual($SearchResult[0][2]+" \""+$SearchResult[0][7]+"\"",_getText($PAGE_BREADCRUMB_RESULT_TEXT));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("343344","Verify the functionality of  Try a new search  GO button for valid keyword in Search Results Articles Only page  both as guest and a registered user.");
$t.start();
try
{
	
 //Nosearch results navigation
 search($SearchResult[0][7]);
 //try new search 
 _setValue($NOSEARCHRESULT_TRYANEW_SEARCH,$SearchResult[2][2]);
 _click($NOSEARCHRESULT_TRYANEW_SEARCH_GOBUTTON);
 //heading
 _assertVisible($NOSEARCHRESULT_HEADING);
	 
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("343304/343346/343314","Verify the UI  of the 'Search Results Products and Articles' page");
$t.start();
try
{

 //Nosearch results navigation
 search($SearchResult[0][0]);
 //ui OF PRODUCT ARTICLE PAGE
 _assertVisible($SEARCH_PAGEFOUND_LINK);
 _assertVisible($SEARCH_RESULTSFOUND_LINK);
 _assertVisible($SEARCH_SORT_DROPDOWN);
 _assertVisible($SEARCH_PAGINATION_DROPDOWN);
 _assertVisible($PLP_PAGINATION_TOP);
 _assertVisible($PLP_RESULTHITS_TOP);
 _assertVisible($SEARCH_RESULT_PRODUCTS_HEADING);
 _assertVisible($SEARCH_INDIVUDIAL_ARTICLE_NAME);
 
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("343305/343306","Verify the behavior of the related articles section in  the search results products and articles page.");
$t.start();
try
{

//Article Count
var $articleCount=_count("_link","content-title",_in(_list("folder-content-list")));

if ($articleCount==6)
	{
	_assertVisible(_div("/View All Results/"));
	}
else
	{
	_assertNotVisible(_div("/View All Results/"));
	}

//Click on view all link
_assert($articleCount>6);
_assertVisible(_link("/Back to Products/"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase("343295/343301","verify the behavior of the category filters and the Breadcrumb  in the application as a registered user./verify the behavior or the  search result message when user is making   multiple selection  from search refinements filters.");
$t.start();
try
{

//search a product	
search($SearchResult[0][0]);
	
//var $Filters=_collect("_heading3","/toggle/",_in($PLP_REFINEMENT));
var $FilterNames=_collectAttributes("_heading3","/toggle-refine/","sahiText",_in(_div("refinements")));
var $ExpectedApplied= new Array();
for(var $i=0;$i<$FilterNames.length;$i++)
	{
		_log($i);
		_log($FilterNames);
		if(_isVisible(_heading3($FilterNames[$i])))
			{
				//var $list=swatchlist($FilterNames[$i]);
				var $FilterOptions=_collect("_link","/swatch/",_in($FilterNames[$i]));
				var $FilterOptionNames=_collectAttributes("_link","/(.*)/","sahiText",_in($FilterNames[$i]));
				_click($FilterOptions[0]);	
				$ExpectedApplied.push($FilterOptionNames[0]+" x");
			}		
	}
	
var $ActualApplied=_collectAttributes("_span","breadcrumb-refinement-value","sahiText",_in(_div("breadcrumb")));
_assertEqual($ExpectedApplied.sort(),$ActualApplied.sort());
_click($HEADER_HOMELOGO);
	
//var $Filters=_collect("_heading3","/toggle/",_in($PLP_REFINEMENT));
	var $FilterNames=_collectAttributes("_heading3","/toggle/","sahiText",_in($PLP_REFINEMENT));
	_collectAttributes("_heading3","/toggle-refine/","sahiText",_in(_div("refinements")));
	var $ExpectedApplied= new Array();
	for(var $i=0;$i<$FilterNames.length;$i++)
		{
			_log($i);
			_log($FilterNames);
			if(_isVisible(_heading3($FilterNames[$i])))
				{
					//var $list=swatchlist($FilterNames[$i]);
					var $FilterOptions=_collect("_link","/swatch/",_in($FilterNames[$i]));
					var $FilterOptionNames=_collectAttributes("_link","/(.*)/","sahiText",_in($FilterNames[$i]));
					_click($FilterOptions[0]);	
					$ExpectedApplied.push($FilterOptionNames[0]+" x");
				}		
		}
	var $ActualApplied=_collectAttributes("_span","breadcrumb-refinement-value","sahiText",_in(_div("breadcrumb")));
	_assertEqual($ExpectedApplied.sort(),$ActualApplied.sort());
	_click($HEADER_HOMELOGO);
}
catch($e)
{
	_logExceptionAsFailure($e);
}		
$t.end();*/

/*var $t=_testcase(""," ");
$t.start();
try
{

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/
 cleanup();