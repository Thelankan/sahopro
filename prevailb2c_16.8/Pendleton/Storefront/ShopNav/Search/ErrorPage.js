_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("Search_Data.xls");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//Navigate to general error page url
_navigateTo($error[0][1]);

var $t=_testcase("343354/343359","Verify the UI of General error page for both as guest and a registered user.");
$t.start();
try
{

//UI elements
_assertVisible($ERROR_PAGE_HEADING);
_assertEqual($error[4][1], _getText($ERROR_PAGE_HEADING));
_assertVisible($ERROR_PAGE_CONTENTASSET);
_assertEqual($error[0][2], _getText($ERROR_PAGE_CONTENTASSET));
_assertVisible($ERROR_PAGE_SEARCH_TEXTBOX);
_assertVisible($ERROR_TRYNEW_SEARCH_LABEL);
_assertVisible($ERROR_SEARCH_GO_BUTTON);
_assertVisible($ERROR_TRYNEW_SEARCH_DIV);
//Error-service Content-Asset
_assertVisible(_paragraph($error[1][2]));
_assertVisible(_div($error[2][2]));
_assertVisible($ERROR_PAGE_FOOTER_SLOT);
_assertVisible($ERROR_PAGE_LEFTNAV);
_assertVisible($ERROR_PAGE_EMAIL_LINK);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("343356","Verify the functionality by clicking on Customer Service links in the left Nav for general error page");
$t.start();
try
{

var $leftNavLinks=_collectAttributes("_link","/(.*)/","sahiText",_in($ERROR_PAGE_LEFTNAV));

for (var $i=0;$i<$leftNavLinks.lenght;$i++)
{
	_click(_link($leftNavLinks[$i]));
	_assertEqual($leftNavLinks[$i], _getText($PAGE_BREADCRUMB));
	_navigateTo($error[0][1]);
}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("343357","Verify the functionality of  search site GO button for valid keyword in general error page  both as guest and a registered user.");
$t.start();
try
{

//Navigate to search result page
_navigateTo($error[0][1]);
//check Go functionality in no search result page
_setValue($ERROR_PAGE_SEARCH_TEXTBOX,$error[4][0]);
_click($ERROR_SEARCH_GO_BUTTON);
_assertContainsText($error[4][0], $ERROR_PAGE_BREADCRUMB_ELEMENT);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//#######################  404 Error Page #################################

//Navigate to general error page url
_navigateTo($error[1][1]);

var $t=_testcase("343347/343353","Verify the UI of 404 page in application for both as guest and a registered user.");
$t.start();
try
{

//page not found
_assertEqual($error[5][1], _getText($ERROR_PAGE_HEADING));
_assertVisible($ERROR_PAGE_CONTENTASSET);
_assertVisible($ERROR_PAGE_MESSAGE);
_assertEqual($error[3][2], _getText($ERROR_PAGE_MESSAGE));
_assertVisible($ERROR_TRYNEW_SEARCH_LABEL);
_assertVisible($ERROR_SEARCH_GO_BUTTON);
_assertVisible($ERROR_TRYNEW_SEARCH_DIV);
//Error-service Content-Asset
_assertVisible(_paragraph($error[1][2]));
_assertVisible(_div($error[2][2]));
_assertVisible($ERROR_PAGE_FOOTER_SLOT);
_assertVisible($ERROR_PAGE_LEFTNAV);
_assertVisible($ERROR_PAGE_EMAIL_LINK);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("343349","Verify the functionality of  Try new search GO button in 404 Page both as guest and a registered user.");
$t.start();
try
{

//Navigate to search result page
_navigateTo($error[0][1]);
//check Go functionality in no search result page
_setValue($ERROR_PAGE_SEARCH_TEXTBOX,$error[4][0]);
_click($ERROR_SEARCH_GO_BUTTON);
_assertContainsText($error[4][0], $ERROR_PAGE_BREADCRUMB_ELEMENT);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("343352","Click on customer service links in the left Nav for both as guest and a registered user.");
$t.start();
try
{

//Navigate to search result page
_navigateTo($error[0][1]);
//check Go functionality in no search result page
_setValue($ERROR_PAGE_SEARCH_TEXTBOX,$error[4][0]);
_click($ERROR_SEARCH_GO_BUTTON);
_assertContainsText($error[4][0], $ERROR_PAGE_BREADCRUMB_ELEMENT);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t=_testcase(" "," ");
$t.start();
try
{

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/