_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("Minicart.xls");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("352712/352716/352717/352713","Verify the  functionality of add to bag button in PDP as an anonymous and a registered user." +
		"Verify the display of QTY / Price in Mini-Bag Flyout as an anonymous and a registered user.");
$t.start();
try
{
	
//add item to cart
addToCartSearch($Minicart[0][6],$Minicart[0][4]);
//mini cart overlay
_mouseOver($MINICART_LINK);
var $pName=_getText($MINICART_PDP_PRODUCTNAME);
var $price=_getText($MINI_CART_SALES_PRICE).replace("$","");
//verify the UI of Mini cart 
//mini cart overlay
_mouseOver($MINICART_LINK);
_assertVisible($MINICART_CONTENT);
_assertVisible($MINICART_IMAGE_TOOGLE);
//name link
_assertEqual($pName,_getText($MINICART_PRODUCT_NAME));
_mouseOver($MINICART_LINK);
//image
_assertVisible($MINICART_PRODUCT_IMAGE);
_assertVisible($MINICART_PRODUCT_IMAGE_SECTION);
//attributes
if($colorBoolean)
	{
		_mouseOver($MINICART_LINK);
		_assertVisible($MINICART_COLOR_LABEL);
		_assertVisible($MINICART_COLOR_VALUE);
	}
if($sizeBoolean)
	{
		_mouseOver($MINICART_LINK);
		_assertVisible($MINICART_SIZE_LABEL);
		_assertVisible($MINICART_SIZE_VALUE);
	}
if($widthBoolean)
	{
		_mouseOver($MINICART_LINK);
		_assertVisible($MINICART_WIDTH_LABEL);
		_assertVisible($MINICART_WIDTH_VALUE);
	}
_mouseOver($MINICART_LINK);
//quantity
_assertVisible($MINICART_QUANTITY_LABEL);
_assertVisible($MINICART_PRICING_SECTION);

_mouseOver($MINICART_LINK);
//sub total
_assertVisible($MINICART_ORDERSUBTITLE_LABEL);
_assertVisible($MINICART_ORDERSUBTITLE_VALUE);
_assertVisible($MINICART_SUBTOTALS_SECTION);
//slot
_assertVisible($MINICART_PROMO_SLOT);
//view cart
_assertVisible($MINICART_OVERLAY_VIEWCART_LINK);
_assertVisible($MINICART_CHECKOUT);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352723/352719/125607", "Verify the calculation of 'Order Subtotal'  & Product total/Quantity/Total In the header in the mini cart in the application both as an anonymous and a registered user");
$t.start();
try
{
	
//add item to cart	
addToCartSearch($Minicart[1][0],$Minicart[1][4]);
//no.of items present in mini cart  //MiniCartProductCount()
var $totalSectionsInMiniCart= _count("_div","/mini-cart-name/",_in(_div("mini-cart-products")));
var $minicartQuantity=new Array();
var $minicartPrice=new Array();
var $minicartName=new Array();

//featching data from minicart
for(var $i=0;$i<$totalSectionsInMiniCart;$i++)
	{
		//_mouseOver($MINICART_OVERLAY_VIEWCART_LINK);
		_mouseOver($MINICART_LINK);
		$minicartQuantity[$i]=parseInt(_extract(_getText(_div("mini-cart-pricing["+$i+"]")),"/[:](.*)/",true));
		$minicartPrice[$i]=parseFloat(_extract(_getText(_div("mini-cart-pricing["+$i+"]")),"/[$](.*)/",true));
		//verifying the quantity
		//var $qty1=parseInt(_extract(_getText(_span("mini-cart-price",_near(_link($Minicart[$i][0])))),"/[:](.*)/",true));
		var $qty1=parseInt(_getText(_span("/value/",_in(_div("mini-cart-pricing["+$i+"]")))));
		_assertEqual($qty1,$Minicart[$i][5]);
	}

var $expQuantity=$minicartQuantity[0]+$minicartQuantity[1];
var $expSubTotal=$minicartPrice[0]+$minicartPrice[1];
_log("Total:"+$expSubTotal);
$expSubTotal=round($expSubTotal,2);

var $actSubTotal=parseFloat(_extract(_getText($MINICART_SUBTOTALS_SECTION),"/[$](.*)/",true));

_assertEqual($expSubTotal,$actSubTotal);
//verify the quantity in the header
var $QtyInHeader=_getText($MINICART_QUANTITY);
_assertEqual($expQuantity,$QtyInHeader);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("352724/352725/352726/352727","Verify the  functionality of Last Product Added in mini bag flyout as an guest and registered user./Verify the  functionality of Image Triange in mini bag flyout as an guest and registered user." +
		"Verify the  functionality of Scroll Bar in mini bag flyout as an guest and registered user.");
$t.start();
try
{

//Mouse hover on mini cart link
_mouseOver($MINICART_LINK);
//mouser hover on mini cart link
_click(_span("mini-cart-toggle fa fa-angle-right"));
//Image should be visible
_assertVisible(_image("/(.*)/", _in(_div("mini-cart-image[1]"))));
//Clicking on the triangle Icon
_click(_span("mini-cart-toggle fa fa-angle-down[1]"));
_assertNotVisible(_image("/(.*)/", _in(_div("mini-cart-image[1]"))));
//mouseOver on mini cart link
_mouseOver($MINICART_LINK);
//Scroll constant position
var $currentPos=window.document.body.scrollTop;
//check the scroll bar functionalities
_call(_div("mini-cart-products").scrollTop=100);
var $changedPos=window.document.body.scrollTop;
//checking if scroll is happening or not
if ($changedPos = $currentPos) {
	_log("Scroll is not present and we should add one more product");
}

//add item to cart
addToCartSearch($Minicart[1][0],$Minicart[1][4]);
var $currentPos=window.document.body.scrollTop;
//check the scroll bar functionalities
_call(_div("mini-cart-products").scrollTop=100);
var $changedPos=window.document.body.scrollTop;
//checking if scroll is happening or not
if ($changedPos < $currentPos) {
	_log("Scroll bar has moved up");
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352710/352727/352711", "Verify the UI of mini Cart on home page when multiple products are added to bag in the application both as an anonymous and a registered user");
$t.start();
try
{
//add item to cart
addToCartSearch($Minicart[2][0],$Minicart[0][4]);	
addToCartSearch($Minicart[3][0],$Minicart[0][4]);	
//verify the UI when multiple products are there
//no.of items present in mini cart
var $totalSectionsInMiniCart=_count("_div","/mini-cart-name/",_in(_div("mini-cart-products")));
//fetching data from mini cart
for(var $i=0;$i<$totalSectionsInMiniCart;$i++)
	{
	//mousehover
	_mouseOver($MINICART_LINK);
	//image
	if($i==0)
		{
	_assertVisible(_image("/(.*)/",_in(_div("mini-cart-image["+$i+"]"))));
		}
	else
		{
		_assertNotVisible(_image("/(.*)/",_in(_div("mini-cart-image["+$i+"]"))));
		}
	//name link
	_assertVisible(_link("/(.*)/",_in(_div("mini-cart-name["+$i+"]"))));
	//attributes
	_assertVisible(_div("mini-cart-attributes["+$i+"]"));
	//quantity and price
	_assertVisible(_div("mini-cart-pricing["+$i+"]"));
	}
_mouseOver($MINICART_LINK);
//slot
_assertVisible($MINICART_PROMO_SLOT);
//view cart
_assertVisible($MINICART_OVERLAY_VIEWCART_LINK);
_assertVisible($MINICART_CHECKOUT);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148614", "Verify the UI of mini Cart on home page when Product description is  expanded  in the application both as an anonymous and a registered user");
$t.start();
try
{
//when product description is in expanded mode
//image should be visible
_assertVisible($MINICART_PRODUCT_IMAGE);
//name link
_assertVisible($MINICART_PRODUCT_NAME);
//attributes
_assertVisible($MINICART_ATTRIBUTES);
//quantity and price
_assertVisible($MINICART_PRICING_SECTION);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352714","Verify the  functionality of Product Image in Mini-Bag Flyout as an anonymous and a registered user.");
$t.start();
try
{
	
//when product description is in collapsed mode
//click on toggle
_mouseOver($MINICART_LINK);
//click on image in mini cart
_click($MINICART_PRODUCT_IMAGE);
//Should remain in same page 
_assertVisible($MINICART_PRODUCT_IMAGE);
_assertVisible($MINICART_CONTENT);
//click on mini cart toggle
_click($MINICART_IMAGE_TOOGLE);
//image should not be visible
_assertNotVisible($MINICART_PRODUCT_IMAGE);
//name link
_assertVisible($MINICART_PRODUCT_NAME);
//attributes
_assertVisible($MINICART_ATTRIBUTES);
//quantity and price
_assertVisible($MINICART_PRICING_SECTION);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352715","Verify the  functionality of Product name in Mini-Bag Flyout as an anonymous and a registered user.");
$t.start();
try
{
	
//navigate to home page
_click($HEADER_HOMELOGO);	
//mousehover
_mouseOver($MINICART_LINK);
//get product name
var $pName=_getText($MINICART_PRODUCT_NAME);
//click on name link
_click($MINICART_PRODUCT_NAME);
//verify the navigation

//_assertVisible(_span("last"));
//_assertEqual($pName,_getText(_span("last")));

_assertVisible(_span($pName, _in($PAGE_BREADCRUMB)));

_assertVisible($MINICART_PDP_PRODUCTNAME);
_assertEqual($pName,_getText($MINICART_PDP_PRODUCTNAME));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352720","Verify the  functionality of View Bag button in Mini-Bag Flyout as an anonymous and a registered user.");
$t.start();
try
{
	
//mouse hover
_mouseOver($MINICART_LINK);
//click on view cart
_click($MINICART_OVERLAY_VIEWCART_LINK);
//cart navigation verification
_assertVisible($CART_TABLE);
_assertVisible($CART_ACTIONBAR);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352721","Verify the  functionality of Go Straight to Checkout button in Mini-Bag Flyout as an anonymous user.");
$t.start();
try
{
	
//mousehover
_mouseOver($MINICART_LINK);	
//click on go strait to checkout
_click($MINICART_CHECKOUT);
//verify the navigation
_assertVisible($CHECKOUT_LOGINPAGE_RETURNINGCUSTOMER_HEADING);
_assertVisible($CHECKOUT_LOGINPAGE_NEWCUSTOMER_HEADING);
_assertVisible($CHECKOUT_LOGINPAGE_LOGIN_SECTION);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352722", "Verify the functionality of 'Go straight to checkout' link in Mini cart in the application as an registered user");
$t.start();
try
{
	
//login to the application
_click($HEADER_LOGIN_LINK);
login();
//mousehover
_mouseOver($MINICART_LINK);	
//click on go strait to checkout
_click($MINICART_CHECKOUT);
//verify the navigation
//Should Nav to Shipping page
_assertVisible($SHIPPING_PAGE_HEADING);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//Clear cart itmes 
ClearCartItems();
var $t = _testcase("352679/352739/352717","Verify the functionality of QTY Field in bag page both as an anonymous and a registered user.");
$t.start();
try
{
	
//Add a product which is applicable for gift wrapping
navigateToCartSearch($giftWrap[0][0],$giftWrap[0][1]);
//update the quantity and check the price
_setValue($CART_QTY_DROPDOWN, "2");
//product quantity update button
_click($CART_UPDATE_BUTTON);
//Normal price
var $updatedPrice=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true);
_assertEqual($CItemBasicPrice*2,$updatedPrice);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase(" ", " ");
$t.start();
try
{
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase(" ", " ");
$t.start();
try
{
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/

cleanup();
