_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("Homepage.xls");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


var $t = _testcase("338457/338458/338460/338454/338456/338440/338442/338434/338872/338881","Verify the functionality of click on link of 'SIGN IN' and the links in its  drop down  in the Global header/Verify the UI of the 'SIGN IN' drop down in the Global header." +
		"[responsive]Verify the UI of Hamburger menu as an  authenticated user");
$t.start();
try
{

if (isMobile()){
_call($HEADER_MOBILE_HAMBURGER).click();
_assertVisible($HEADER_MOBILE_HAMBURGER_LINKS);
_assertVisible($HEADER_MOBILE_HAMBURGER_MENU);

var $utilityLinkCount=MobileCountofUtilityLinkCount();
_log($utilityLinkCount);

for (var $i=0;$i<$utilityLinkCount;$i++)
{

//utility links 
_click(_link($FooterLinks[$i][8]));
	
if ($i==0)
{
	_assertVisible($HEADER_SIGNINPAGE_HEADING);
}
else if ($i==1)
{
	_assertVisible($HEADER_CREATEACCOUNT_HEADING);	
}
else if ($i==2)
{
	_assertVisible($HEADER_STORELOCATOR_HEADING);
}
else
{
	_wait(3000);
	_popup("Sites-PWM-Site | Pendleton Woolen Mills")._assertVisible(_heading1("/Contact Us/"));
	_wait(3000);
	_popup("Sites-PWM-Site | Pendleton Woolen Mills").close();
}

}

}

else{
	//click on user account
	_assertVisible($HEADER_USERACCOUNT_LINK);
	_click($HEADER_USERACCOUNT_LINK);
	//Sign in and register link should display
	_assertVisible($HEADER_LOGINREGISTER_HEADING);
	_assertVisible($HEADER_LOGIN_LINK);
	_assertVisible($HEADER_REGISTER_LINK);
	//click on user account link again
	_click($HEADER_USERACCOUNT_LINK);
	//Sign in and register link should display
	_assertNotVisible($HEADER_LOGINREGISTER_HEADING);
	_assertNotVisible($HEADER_LOGIN_LINK);
	_assertNotVisible($HEADER_REGISTER_LINK);
	//checking navigation for all the links 
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_LOGIN_LINK);
	_assertVisible($HEADER_SIGNINPAGE_HEADING);
	//Should navigate to sign in page
	_click($HEADER_REGISTER_LINK);
	_assertVisible($HEADER_CREATEACCOUNT_HEADING);
	}

//Header links
_assertVisible($HEADER_CUSTOMERSERVICE_LINK);
_assertVisible($HEADER_STORELOCATOR_LINK);
_assertVisible($HEADER_SEARCH_TEXTBOX);
_assertVisible($HEADER_SEARCH_ICON);
_click($HEADER_USERACCOUNT_LINK);
_assertVisible($HEADER_LOGIN_LINK);
_assertVisible($HEADER_REGISTER_LINK);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//################### Footer Related Scripts ######################

var $t=_testcase("338462/338465/338471","Verify the application behavior on click or Tap on  static texts  from the first section of the footer  as unauthenticated and as a  registered user.");
$t.start();
try
{

var $FooterLinksFirstDiv=FooterLinksDiv1();
	
for (var $i=0;$i<$FooterLinksFirstDiv.length;$i++){
	_assertVisible(_span($FooterLinks[$i][5]));
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("338462/338466/338467","Verify the application behavior on click or tap  of links  from the third section of the footer  as unauthenticated and as a  registered user.");
$t.start();
try
{

//Footer link third section	
var $FooterLinksThirdtDiv=FooterLinksDiv3();
	
for (var $i=0;$i<$FooterLinksThirdtDiv.length;$i++){
	_assertVisible(_link($FooterLinks[$i][7]));
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("338462/338463/338467/338469","Verify the UI of global footer in the application both as an anonymous and a registered user./Verify the application behavior on click  or Tap on links  from the second section of the footer  as unauthenticated and as a  registered user.");
$t.start();
try
{


//Email textbox & Submit button
_assertVisible($FOOTER_EMAIL_FIELD);
_assertVisible($FOOTER_EMAIL_ICON);
	
//Footer link second section
var $FooterLinksSecondtDiv=FooterLinksDiv2();

for (var $i=0;$i<$FooterLinksSecondtDiv.length;$i++){
	_assertVisible(_link($FooterLinks[$i][6]));
}

//social media links
var $footerSocialMediaIcons=FooterSocialMediaIconsCount();

for(var $i=0; $i<$footerSocialMediaIcons; $i++){
	_assertVisible(_link($FooterLinks[$i][3]));
}

//enter email input field
_assertVisible($FOOTER_EMAIL_FIELD);
//click on email icon
_assertVisible($FOOTER_EMAIL_ICON);
//Social promotions
_assertEqual($FooterLinks[0][4], _getText(_div("social-promo")));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("338468/338464","Verify the functionality of sign up button  of the Email sign up section of footer, both as an anonymous and a registered user");
$t.start();
try
{
	//Nav to Home page
	_click($HEADER_HOMELOGO);
	for(var $i=0; $i<$emailsignup.length; $i++){
			//enter email input
			_setValue($FOOTER_EMAIL_FIELD,$emailsignup[$i][1]);
			//click on email icon
			_click($FOOTER_EMAIL_ICON);
			if($i==0 || $i==1 || $i==2){
					//Verifying Error message and Font Color of Error message
					_assertEqual($emailsignup[$i][2],_getText($FOOTER_EMAIL_ERROR_MESSAGE));
					_assertEqual($emailsignup[1][3],_style($FOOTER_EMAIL_ERROR_MESSAGE,"color"));			
				}
			else
				{
					//Valid
					_assertNotVisible($FOOTER_EMAIL_ERROR_MESSAGE);
				}
			}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("338470/338886","Verify the functionality of  and  on click  functionality on social icons at global header in the application");
$t.start();
try
{

//social media links
var $footerSocialMediaIcons=FooterSocialMediaIconsCount();

for(var $i=0; $i<$footerSocialMediaIcons; $i++){
	_assertVisible(_link($FooterLinks[$i][3]));
	var $titleAttribute=_getAttribute(_link($FooterLinks[$i][3]),"title");
	_assertEqual($FooterLinks[$i][9],$titleAttribute);
}

_log("################# Should check clicking functionality also #######################");

//_assert(false,"Should check clicking functionality also");

////Social Media icons clicking
//for(var $i=0; $i<$footerSocialMediaIcons; $i++){
//	_assertVisible(_link($FooterLinks[$i][3]));
//	_click(_link(_link($FooterLinks[$i][3])));
//}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//#################### Related to Global Navigation #########################

var $t=_testcase("338455/338447/338453","[Responsive]verify   the tap functionality on the category expand icon and category name of tier 1 and tier 2./[responsive]Verify the UI of the  global header  for guest and authenticated user.");
$t.start();
try
{

	if (isMobile()){
		_click($HEADER_MOBILE_HAMBURGER);
		var $categoruCollapsedLinks=MobilecategoryCollapsedLinksCount();
		for(var $i=0; $i<$categoruCollapsedLinks; $i++){
			_click(_italic("menu-item-toggle fa fa-plus["+$i+"]"));
			_assertVisible(_list("menu-vertical["+$i+"]"));
		}
	}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//#################### Related to Header #########################

var $t=_testcase("338443/338851","Verify the application behavior when user click or tap on Application Logo/Verify the UI of home page for both guest and register user");
$t.start();
try
{

//Display of the Application Logo and functionality
_assertVisible($HEADER_HOMELOGO);
_mouseOver($HEADER_HOME_LINK);
_assertEqual($HomePage[0][8],_getAttribute($HEADER_PENDLETON_IMAGE_LINK,"title"));
_click($HEADER_HOMELOGO);
//navigate to category page
_click($HEADER_SALE_CATEGORY_LINK);
//search result page
_assertVisible($SEARCHRESULT_CONTENT_PAGE);
//again click on home logo
_click($HEADER_HOMELOGO);
//should navigate to home page
_assertVisible($HOMEPAGE_HERO_IMAGE);
//Header links
_assertVisible($HEADER_CUSTOMERSERVICE_LINK);
_assertVisible($HEADER_STORELOCATOR_LINK);
_assertVisible($HEADER_SEARCH_TEXTBOX);
_assertVisible($HEADER_SEARCH_ICON);
//Should navigate to sign in page
_click($HEADER_USERACCOUNT_LINK);
//Sign in and register link should display
_assertVisible($HEADER_LOGINREGISTER_HEADING);
_assertVisible($HEADER_LOGIN_LINK);
_assertVisible($HEADER_REGISTER_LINK);


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("338444","Verify the behavior of   search  field  on entering and deleting the keyword from the search field as guest and authenticated user./Verify the behavior of   search  field  on entering and deleting the keyword from the search field as guest and authenticated user.");
$t.start();
try
{

for(var $i=0;$i<3;$i++)
{	
//search the product	
_setValue($HEADER_SEARCH_TEXTBOX,$HomePage[$i][9]);

//overlay should not dispaly if there are two letters
if ($i==0 || $i==1)
{
	//search result overlay should be displayed
	_assertNotVisible($SEARCH_SUGGESTION_OVERLAY);
}
else 
{
	//overlay should display if there are 3 letters
	_assertVisible($SEARCH_SUGGESTION_OVERLAY);
}

//click on outside of the overlay
_click($HEADER_MENU_SECTION);
_assertNotVisible($SEARCH_SUGGESTION_OVERLAY);

}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("338446/338448","Verify the functionality of 'Mini bag' icon at global header in the application both as an anonymous and a registered user./[responsive]Verify the functionality of 'Mini bag' icon at global header in the application both as an anonymous and a registered user.");
$t.start();
try
{

//click on mini cart button with no products
_click($MINICART_EMPTY_LINK);
//should navigate to empty cart page
_assertVisible($CART_EMPTY_HEADING);
//add products to cart
addToCartSearch($productQuantity[1][0],$productQuantity[0][2]);
//click on cart link
_click($HEADER_MINICART_LINK);
_assertVisible($CART_ROW);
//Nav to Home page
_click($HEADER_HOMELOGO);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("338442","Verify the application behavior on click of Store locator link for guest and authenticated user.");
$t.start();
try
{

//Nav to Home page
_click($HEADER_HOMELOGO);
//click on store Locator Link
_click($HEADER_STORELOCATOR_LINK);
//store Locator heading	
_assertVisible($HEADER_STORELOCATOR_HEADING);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//##################################### Home page Register User #############################

var $t=_testcase("338459/338461/338456/338440","Verify the application behavior  of  My Account Drop down in the header as registered user/[responsive]Verify the application behavior on click of links of  My Account section within the hamburger menu." +
"[Responsive]Verify the application behavior on click of  utility links in hamburger menu for guest user./Verify the system response on click of the  'CUSTOMER SERVICE' link for guest and authenticated user.");
$t.start();
try
{

if (isMobile()){
_click($HEADER_MOBILE_HAMBURGER);	
}
else{
//Login to the application
_click($HEADER_USERACCOUNT_LINK);
}

//Login to the application	
_click($HEADER_LOGIN_LINK);
//login to the application
login();
//log out heading 
_assertVisible($MY_ACCOUNT_LOGOUT_LINK);

//Verifying the navigation of all the links
var $userLinks=AccountDropDownLinks();

for (var $i=0;$i<$userLinks.length;$i++){

if (isMobile()){
_click($HEADER_MOBILE_HAMBURGER);
}
else{
//Click on user account link
_click($HEADER_USERACCOUNT_LINK);
}
//click on my account drop down links
_assertVisible(_link($HomePage[$i][3]));
_click(_link($HomePage[$i][3]));
_assertVisible(_div("/"+$HomePage[$i][4]+"/"));
}

}
catch($e)
{
_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("338880/338881","Verify the UI of body of home page for both guest and registered user/[Responsive_Mobile_Tablet]Verify the functionality of curalte imaes in home page for both guest and registered user");
$t.start();
try
{
	
//Login to the application	
_click($HEADER_LOGIN_LINK);
//login to the application
login();
//click on home page link
_click($HEADER_HOMELOGO);
//Homeoage banner image
_assertVisible(_div("banner-image"));
//home page 2nd section content slot
_assertVisible(_div("col1-slot product-col"));
_assertVisible(_div("col2-slot product-col"));
//3rd slot
_assertVisible(_div("homepage-large home-category-1"));
//4th slot
var $ContentSlotProductImages=ProductImagesinContentSlot();
for (var $i=0; $i<$ContentSlotProductImages; $i++)
	{
	_assertVisible(_div("product-image["+$i+"]", _in(_div("slot-4 float"))));
	}
//5th slot position
_assertVisible(_div("homepage-large home-category-1", _in(_div("html-slot-container[2]"))));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("338435/338851","Verify the change in UI of Global header in the application once user is logged into the application.");
$t.start();
try
{
	//Nav to Home page
	_click($HEADER_HOMELOGO);
	//promo slot
	_assertVisible($HEADER_PROMOTION_SLOT);
	//logo
	_assertVisible($HEADER_HOMELOGO);
	//search
	_assertVisible($HEADER_SEARCH_TEXTBOX);
	_assertVisible($HEADER_SEARCH_ICON);
	//cart
	_assertVisible($HEADER_MINICART_ICON_SECTION);
	//click on user account
	_click($HEADER_USERACCOUNT_LINK);
	//Ui of my account drop down
	_assertVisible($HEADER_MYACCOUNT_LINK);
	//Header links
	_assertVisible($HEADER_CUSTOMERSERVICE_LINK);
	_assertVisible($HEADER_STORELOCATOR_LINK);
	_assertVisible($HEADER_CHECKORDER_LINK);
	_assertVisible($HEADER_WISHLIST_LINK);
	_assertVisible($HEADER_SEARCH_TEXTBOX);
	_assertVisible($HEADER_SEARCH_ICON);
	_assertVisible($HEADER_LOGOUT_LINK);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("338890/338883/338885","Verify the font color of the  Logo / Announcement Bar/Verify the font Family and the font  size  for  the  description of  the content slots  with in home page.");
$t.start();
try
{

//color of the logo
UIVerification($HEADER_PENDLETON_IMAGE_LINK,true,false,true,false);
_assertEqual($HomePage[0][10],$color);
_assertEqual($HomePage[1][10],$fontfamily);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();




//#################### Mouse Hover functionality ############################

/*var $t=_testcase("338451/338484/338485/338449/338449/338486/338450","Verify the  on and click functionality on the Tier 2  and Tier 3 category.");
$t.start();
try
{

//focus window
_focus(_link("Mens", _in(_div("menu-desktop"))));
_mouseOverNative(_link("Mens", _in(_div("menu-desktop"))));
_assertVisible(_image("Men's Category Image[1]"));
//mega menu image
_assertVisible(_image("cat-landing-slotbanner-mens.jpg[1]"));
//click on category link
_click(_link("Clothing", _in(_listItem("/Clothing/"))));
//image
_assertNotVisible(_image("Men's Category Image[1]"));

//Nav to Home page
_click($HEADER_HOMELOGO);
var $RootCategory= _collectAttributes("_link", "/has-sub-menu/", "sahiText", _in($HEADER_CATEGORY_LEVEL_FIRST));
for(var $i=0;$i<$RootCategory.length;$i++)
{
	_click(_link("has-sub-menu["+$i+"]", _in($HEADER_CATEGORY_LEVEL_FIRST)));
	_assertVisible(_span("Shop "+$RootCategory[$i]));			  
}   
//Nav to Home page
_click($HEADER_HOMELOGO);
for(var $i=0;$i<$HomePage.length;$i++)
{
	_click(_link($HomePage[$i][1], _in($HEADER_CATEGORY_LEVEL_FIRST)));
	_assertContainsText($HomePage[$i][1], $PAGE_BREADCRUMB);  
}  
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("338451","Verify the  on   and click functionality on the Tier 2  and Tier 3 category.");
$t.start();
try
{

//focus window
_focus(_link("Mens", _in(_div("menu-desktop"))));
_mouseOverNative(_link("Mens", _in(_div("menu-desktop"))));
//mega menu image
_assertVisible(_image("cat-landing-slotbanner-mens.jpg[1]"));
//click on category link
_click(_link("Clothing", _in(_listItem("/Clothing/"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/


/*var $t=_testcase(" "," ");
$t.start();
try
{

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/

cleanup();