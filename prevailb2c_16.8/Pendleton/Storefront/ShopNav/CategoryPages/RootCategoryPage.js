_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("Category_Data.xls");

var $RootCat=$Category[0][0];
SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

var $RootCatName=$Category[3][0];

var $t = _testcase("124286", "Verify the UI of the root category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateRootCatPage($RootCat);
	//category refinement header
	_assertVisible($ROOTCATEGORY_REFINEMENT);
	_assertEqual("Shop "+$RootCatName, _getText($PLP_REFINEMENT_HEADING));
	//Category refinement section
	_assertVisible($HEADER_CATEGORY_LEVEL_FIRST);
	//Category banner
	_assertVisible($ROOTCATEGORY_BANNER);
	//SubCategory content slots
	_assertVisible($ROOTCATEGORY_SECONDARY_BANNER_CONTENT);	
	var $Rootcategory_tiles=_count("_div","category-tile",_in(_div("secondary-content")));	
	for(var $i=0;$i<$Rootcategory_tiles;$i++)
		{
			_assertVisible(_div("category-tile["+$i+"]"));	
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124288/148574", "Verify the display and functionality of category links under heading 'Shop <root category name>' in left nav  in the application both as an anonymous and a registered user");
$t.start();
try
{	
	_setAccessorIgnoreCase(true);
	//navigation of sub-category link.
	var $CategoryLinks=_collect("_link","/refinement-link /",_in($ROOTCATEGORY_REFINEMENT));
	var $CategoryNames=_collectAttributes("_listItem","/(.*)/","sahiText",_in($ROOTCATEGORY_REFINEMENT));
	for(var $i=0;$i<$CategoryLinks.length;$i++)
	{
		_click($CategoryLinks[$i]);
		var $ExpectedBreadcrumb=$RootCatName+" "+$CategoryNames[$i];
		_assertEqual($ExpectedBreadcrumb, _getText($PAGE_BREADCRUMB));
		_assertEqual($CategoryNames[$i], _getText($PLP_REFINEMENT_LINK_ACTIVE));
		navigateRootCatPage($RootCat);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


cleanup();

