_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("Category_Data.xls");

var $RootCatName=$Category[0][0];
var $CatName=$Category[1][0];
var $RootCatNameBreadcrumb=$Category[0][2];
var $CatNameBreadcrumb=$Category[1][2];

//site url's
SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("343361/343323/343328/343283","Verify the UI of the product tile in category landing page as a anonymous  and guest user./Verify the functionality of Cat-landing-slot banner in category landing page for both as guest and registered user.");
$t.start();
try
{

//Navigate to category landing page
navigateCatPage($RootCatName,$CatName);

_log("########### Configuration Should be done #############");

//count of products
var $countOfProducts=_count("_div","product-tile",_in(_list("search-result-items")));

for (var $i=0;$i<$countOfProducts;$i++)
{

//Product Name for every product should display
_assertVisible(_div("product-name["+$i+"]"));
_assertVisible(_div("product-pricing["+$i+"]"));
_assertVisible(_div("pwm-tile-rating["+$i+"]"));
//Swatch list
if (_isVisible(_list("swatch-list["+$i+"]")))
{	
var $countofcolorswatches=_count("_image","/swatch-image desktop-only/",_in(_list("swatch-list["+$i+"]")));
//Count of swatches for product
if ($countofcolorswatches<=6)
{
	_assert(true,"Lessthan 6 swatches are appearing");
}
else
{
	_assert(true,"MoreThan 6 swatches are appearing");
}
	
}
	
}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("343315/343366/343303/343316", "Verify the UI of the category landing page in the application both as guest and a registered user." +
		"Verify the Canonical URL and Schema code in category landing page");
$t.start();
try
{
	
//Navigate to category landing page
navigateCatPage($RootCatName,$CatName);
_set($CategoryUrl, window.location.href);
_assertEqual($Category[0][3]+""+$Category[0][2]+"-"+$Category[1][2],$CategoryUrl);
//bread crumb
_assertVisible($PAGE_BREADCRUMB);
var $ExpectedBreadcrumb=$Category[0][2]+" "+$Category[1][2];
_assertEqual($ExpectedBreadcrumb, _getText($PAGE_BREADCRUMB));
//category slot
_assertVisible($PLP_CATEGORY_BANNER);	
//Leftnav
_assertVisible($PLP_REFINEMENT_HEADING);
//sort by drp down
_assertVisible($PLP_REFINEMENT);
_assertVisible($PLP_REFINEMENT_LINK_ACTIVE);
//items per page
_assertVisible($PLP_SORTBY_DROPDOWN_TOP);
_assertVisible($PLP_SORTBY_DROPDOWN_BOTTOM);
//Display toggle
_assertVisible($PLP_TOOGLE_GRID);
_assertVisible($PLP_TOGGLE_GRID_BOTTOM);
//Pagination
_assertVisible($PLP_PAGINATION_TOP);
_assertVisible($PLP_PAGINATION_BOTTOM);
_assertVisible($PLP_RESULTHITS_TOP);
_assertVisible($PLP_RESULTHITS_BOTTOM);
//products
_assertVisible($PLP_RESULT_ITEMS);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("343317", "Verify the behavior of breadcrumb in category landing page in the application both as guest and registered user.");
$t.start();
try
{
	
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	//click on last bread crumb element
	_click($PAGE_BREADCRUMB_ELEMENT1);
	_assertContainsText($RootCatNameBreadcrumb, $PAGE_BREADCRUMB);
	var $ExpectedBreadcrumb=$Category[0][2]+" "+$Category[1][2];
	_assertEqual($ExpectedBreadcrumb, _getText($PAGE_BREADCRUMB));
	//click on first element
	_click($PAGE_BREADCRUMB_LINK);
	_assertEqual($CatNameBreadcrumb,_extract(_getText($PLP_REFINEMENT_HEADING),"/[p] (.*)/",true));
	_assertNotVisible($RootCatNameBreadcrumb,_in($PAGE_BREADCRUMB));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("343318/343320", "Verify the functionality on click of Text Refinements section in left Nav in the application both for guest and registered user." +
		"Verify the functionality of  Tier 2 / Tier 3 Category Refinements in left Nav for both guest and registered user.");
$t.start();
try
{
	navigateCatPage($RootCatName, $CatName);
	_setAccessorIgnoreCase(true);
	//the display of category name
	_assertVisible($PLP_CATEGORYLINKS_EXPANDED_SECTION);
	_assertEqual($Category[1][2], _getText($PLP_REFINEMENT_LINK_ACTIVE));
	//navigation of category link.
	var $Categories=_collectAttributes("_link","/refinement-link/","sahiText",_in($PLP_CATEGORY_REFINEMENT));
	for(var $i=0;$i<$Categories.length;$i++)
		{
			if($Categories[$i].indexOf(" ")>=0)
				{
					var $CategoryLink=$Categories[$i].substr(0,$Categories[$i].indexOf(' '));
				}
			else
				{
					var $CategoryLink=$Categories[$i];
				}
			var $CATEGORY=_link($CategoryLink, _in($PLP_CATEGORY_REFINEMENT));
			//click on category
			_click($CATEGORY);
			_assertContainsText($CategoryLink , $PAGE_BREADCRUMB);
			_assertEqual($CategoryLink, _getText($PLP_REFINEMENT_LINK_ACTIVE, _in($PLP_CATEGORY_REFINEMENT)));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("343322", "Verify On hover of Title for Refinements in category landing page in the application both as guest and registered user.");
$t.start();
try
{
	
//Navigate to category page
navigateCatPage($RootCatName, $CatName);	
//navigation of category link.
var $Categories=_collectAttributes("_link","/refinement-link/","sahiText",_in($PLP_SUBCATEGORY_REFINEMENT));
for(var $i=0;$i<$Categories.length;$i++)
{

var $CategoriesTitleAttributes=_extract(_getAttribute(_link("refinement-link ["+$i+"]",_in(_list("category-level-2"))),"title"),"/Go to Category[:] (.*)/",true);
_assertEqual($Categories[$i],$CategoriesTitleAttributes);

}
	
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("343325","Verify the UI for the sort and pagination section  of category landing page.");
$t.start();
try
{
	navigateCatPage($RootCatName, $CatName);
	if(_isVisible($PLP_SORTBY_DROPDOWN_TOP) && _isVisible($PLP_SORTBY_DROPDOWN_BOTTOM))
	{
		var $sort=_getText($PLP_SORTBY_DROPDOWN_TOP);
		var $j=0;
		for(var $i=1;$i<$sort.length;$i++)
		{
			_setSelected($PLP_SORTBY_DROPDOWN_TOP, $sort[$i]);
			//Validating the selection
			_assertEqual($sort[$i],_getSelectedText($PLP_SORTBY_DROPDOWN_TOP),"Selected option is displaying properly");
			_assertEqual($sort[$i],_getSelectedText($PLP_SORTBY_DROPDOWN_BOTTOM),"Selected option is displaying properly");
			if($sort.indexOf($Category[$j][1])>-1)
			{
				_setSelected($PLP_SORTBY_DROPDOWN_TOP, $Category[$j][1]);
				sortBy($Category[$j][1]);
				$j++;
			}
		}
	}
	else
	{
		_assert(false, "sort by drop down is not present");
	}
	_click($HEADER_HOMELOGO);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("343326","Verify the functionality related to  'pagination' on category landing page  in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	
	navigateCatPage($RootCatName, $CatName);
	pagination();
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

if(!isMobile())
{

var $t = _testcase("343327", "Verify the pagination and Items per page drop down in category landing page in the application both as an anonymous and a registered user.");
$t.start();
try
{
	navigateCatPage($RootCatName, $CatName);
	if(_isVisible($PLP_ITEMSPERPAGE_DROPDOWN_TOP) && _isVisible($PLP_ITEMSPERPAGE_DROPDOWN_BOTTOM))
		{	
			var $itemsDisp=_getText($PLP_ITEMSPERPAGE_DROPDOWN_TOP);
			for(var $i=0;$i<$itemsDisp.length;$i++)
				{
					_setSelected($PLP_ITEMSPERPAGE_DROPDOWN_TOP, $itemsDisp[$i]);
					//Validating the selection
					_assertEqual($itemsDisp[$i],_getSelectedText($PLP_ITEMSPERPAGE_DROPDOWN_TOP),"Selected option is displaying properly");
					_assertEqual($itemsDisp[$i],_getSelectedText($PLP_ITEMSPERPAGE_DROPDOWN_BOTTOM),"Selected option is displaying properly at footer");
					ItemsperPage($itemsDisp[$i]);
				}		
		}
	else
		{
			_assert(true, "View drop down is not present");
		}
	_click($HEADER_HOMELOGO);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

}


/*
var $t = _testcase("343323", " ");
$t.start();
try
{
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/
