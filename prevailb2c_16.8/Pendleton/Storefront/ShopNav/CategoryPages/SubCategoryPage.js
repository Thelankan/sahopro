_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("Category_Data.xls");


SiteURLs();
var $RootCatName=$Category[0][0];
var $CatName=$Category[1][0];
var $SubCatName=$Category[2][0];
_setAccessorIgnoreCase(true); 
cleanup();

var $t = _testcase("287586", "Verify the system response on click of sub category name in the application both as an anonymous and a registered user.");
$t.start();
try
{
	navigateSubCatPage($RootCatName, $CatName, $SubCatName);
	_assertVisible($PAGE_BREADCRUMB);
	var $ExpectedBreadcrumb=$RootCatName+" "+$CatName+" "+$SubCatName;
	_assertEqual($ExpectedBreadcrumb, _getText($PAGE_BREADCRUMB));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125646", "Verify the UI of third category grid page  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertContainsText($SubCatName, $PAGE_BREADCRUMB);
	var $ExpectedBreadcrumb=$RootCatName+" "+$CatName;
	_assertEqual($ExpectedBreadcrumb, _getText($PAGE_BREADCRUMB));
	//category slot
	_assertVisible($PLP_CATEGORY_BANNER);	
	//Leftnav
	_assertVisible($PLP_REFINEMENT_HEADING);
	//sort by drp down
	_assertVisible($PLP_REFINEMENT);
	_assertVisible($PLP_REFINEMENT_LINK_ACTIVE);
	//items per page
	_assertVisible($PLP_SORTBY_DROPDOWN_TOP);
	_assertVisible($PLP_SORTBY_DROPDOWN_BOTTOM);
	//Display toggle
	_assertVisible($PLP_TOOGLE_GRID);
	_assertVisible($PLP_TOGGLE_GRID_BOTTOM);
	//Pagination
	_assertVisible($PLP_PAGINATION_TOP);
	_assertVisible($PLP_PAGINATION_BOTTOM);
	_assertVisible($PLP_RESULTHITS_TOP);
	_assertVisible($PLP_RESULTHITS_BOTTOM);
	//products
	_assertVisible($PLP_RESULT_ITEMS);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("125634", "Verify the display of breadcrumb and its functionality in the third category grid page  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertContainsText($SubCatName, $PAGE_BREADCRUMB);
	var $expBreadCrumb=$RootCatName+" "+$CatName+" "+$SubCatName;
	//navigate to root category page
	var $BREADCRUMBLINK=CatLink($RootCatName);
	_click($BREADCRUMBLINK);
	//Bread crumb should not visible in Root category landing page
	_assertNotVisible($PAGE_BREADCRUMB);
	
	navigateSubCatPage($RootCatName, $CatName, $SubCatName);
	//navigate to category page
	var $BREADCRUMBLINK=CatLink($CatName);
	_click($BREADCRUMBLINK);
	var $ExpectedBreadcrumb=$RootCatName+" "+$CatName;
	_assertEqual($ExpectedBreadcrumb, _getText($PAGE_BREADCRUMB));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125637", "Verify the display of category links under heading Shop <cat name> in left nav in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage($RootCatName, $CatName, $SubCatName);
	_setAccessorIgnoreCase(true);
	_assertVisible($PLP_CATEGORYLINKS_EXPANDED_SECTION);
	_assertEqual($SubCatName, _getText(PLP_SUBCATEGORY_REFINEMENT_LINK_ACTIVE));
	//navigation of sub-category link.
	var $SubCategories=_collectAttributes("_link","/refinement-link/","sahiText",_in($PLP_SUBCATEGORY_REFINEMENT));
	for(var $i=0;$i<$SubCategories.length;$i++)
		{
			if($SubCategories[$i].indexOf(" ")>=0)
				{
					var $SubCategoryLink=$SubCategories[$i].substr(0,$SubCategories[$i].indexOf(' '));
				}
			else
				{
					var $SubCategoryLink=$SubCategories[$i];
				}
			var $SUBCATEGORY=_link($SubCategoryLink, _in($PLP_SUBCATEGORY_REFINEMENT));
			//click on category
			_click($SUBCATEGORY);
			_assertContainsText($SubCategoryLink , $PAGE_BREADCRUMB);
			_assertEqual($SubCategoryLink, _getText($PLP_SUBCATEGORY_REFINEMENT_LINK_ACTIVE));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125649", "Verify the system's response on click of 'list view icon' (or) grid view icon beside pagination  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage($RootCatName, $CatName, $SubCatName);
	//verifying navigation
	_assertVisible($PLP_RESULT_ITEMS);
	//click on toggle grid
	_click($PLP_TOOGLE_GRID);
	//verification
	_assertVisible(_byClassName("search-result-content wide-tiles", "DIV"));_assertVisible($CATEGORY_SEARCHRESULT_CONTENT);
	_click($CATEGORY_TOOGLE_GRID);
	_assertVisible(_byClassName("search-result-content wide-tiles", "DIV"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125640", "Verify the functionality of COLOR filter in the left nav of third category grid page  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	navigateSubCatPage($RootCatName, $CatName, $SubCatName);
	var $Colors = _collect("_link","/swatch/",_in($PLP_LEFTNAV_COLOR_SWATCHES));
	var $ColorNames=_collectAttributes("_link","/swatch/","sahiText",_in($PLP_LEFTNAV_COLOR_SWATCHES));
	var $UnSelectable=_collectAttributes("_listItem","/unselectable swatch/","sahiText",_in($PLP_LEFTNAV_COLOR_SWATCHES));
	for(var $k=0; $k<$Colors.length; $k++)
	{
		if($unSelectable.indexOf($ColorNames[$k])>=0)
			{
				_assert(true, "Swatch is unselectable");
			}
		else
			{
			 	_click($Colors[$k]);
			    //clear link
			 	_assertVisible($PLP_LEFTNAV_COLOR_REFINEMENT_CLEAR_LINK);
			 	_assertEqual($ColorNames[$k]+" x",_getText($PLP_BREADCRUMB_REFINEMENT_VALUE));
			 	_click($PLP_BREADCRUMB_REFINEMENT_CLOSE_LINK);
			 	_wait(4000,!_isVisible($PLP_LEFTNAV_COLOR_REFINEMENT_CLEAR_LINK));
			}  
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}		
$t.end();

var $t = _testcase("125643", "Verify the system's response on selection on Price filter from left nav  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage($RootCatName, $CatName, $SubCatName);
	var $PriceRange=_collect("_link","/refinement-link/",_in($PLP_LEFTNAV_PRICE_RANGE));
	var $PriceRangeValues=_collectAttributes("_link","/refinement-link/","sahiText",_in($PLP_LEFTNAV_PRICE_RANGE));
	for ($k=0; $k< $PriceRange.length; $k++)
	{
		_click($PriceRange[$k]);
		_assertEqual($PriceRangeValues[$k]+" x", _getText($PLP_BREADCRUMB_REFINEMENT_VALUE)); 	
		priceCheck($PriceRange[$k]);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125641", "Verify the functionality of SIZE filter in the left nav of third category grid page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage($RootCatName, $CatName);
	var $Size = _collect("_link","/swatch/",_in($PLP_LEFTNAV_SIZE_SWATCHES));
	var $SizeNames=_collectAttributes("_link","/swatch/","sahiText",_in($PLP_LEFTNAV_SIZE_SWATCHES));
	for(var $k=0; $k<$Size.length; $k++)
	{
		_click($Size[$k]);
		//clear link
		_assertVisible($PLP_LEFTNAV_SIZE_REFINEMENT_CLEAR_LINK);
		_assertEqual($SizeNames[$k]+" x",_getText($PLP_BREADCRUMB_REFINEMENT_VALUE));
		_click($PLP_BREADCRUMB_REFINEMENT_CLOSE_LINK);
		_wait(4000,!_isVisible($PLP_LEFTNAV_SIZE_REFINEMENT_CLEAR_LINK));
 	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("287633", "Verify the system's response on click of filter names displayed in left nav in the application both as an anonymous and a registered user.");
$t.start();
try
{
	navigateSubCatPage($RootCatName, $CatName, $SubCatName);
	var $Filters=_collect("_heading3","/toggle/",_in($PLP_REFINEMENT));
	var $FilterNames=_collectAttributes("_heading3","/toggle/","sahiText",_in($PLP_REFINEMENT));
	for(var $i=0;$i<$Filters.length;$i++)
		{	
			//click on filter
			_click($Filters[$i]);
			//filter will collapse
			var $list=swatchlist($FilterNames[$i]);
			_assertNotVisible($list);
			//click on filter
			_click($Filters[$i]);
			//filter will expand
			_assertNotVisible($list);
		}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("287634", "Verify whether filter get clear when user click on selected filter value in the application both as an anonymous and a registered user.");
$t.start();
try
{
	navigateSubCatPage($RootCatName, $CatName, $SubCatName);
	var $Filters=_collect("_heading3","/toggle/",_in($PLP_REFINEMENT));
	var $FilterNames=_collectAttributes("_heading3","/toggle/","sahiText",_in($PLP_REFINEMENT));
	for(var $i=0;$i<$Filters.length;$i++)
		{
			if(_isVisible($Filters[$i]))
				{
					var $list=swatchlist($FilterNames[$i]);
					var $FilterOptions=_collect("_link","/swatch/",_in($list));
					var $FilterOptionNames=_collectAttributes("_link","/(.*)/","sahiText",_in($list));
					var $unSelectableOption=_collectAttributes("_listItem","/unselectable swatch/","sahiText",_in($list));
					if($unSelectableOption.indexOf($FilterOptionNames[0])>=0)
						{
							_assert(true, "Swatch is unselectable");
						}
					else
						{
							_click($FilterOptions[0]);
							_assertVisible($PLP_REFINEMENT_CLEAR_LINK);
							_assertVisible($PLP_BREADCRUMB_REFINEMENT_VALUE);
							_assertEqual($FilterOptionNames[0]+" x", _getText($PLP_BREADCRUMB_REFINEMENT_VALUE));
							_click($FilterOptions[0]);
							_assertNotVisible($PLP_REFINEMENT_CLEAR_LINK);
							_assertNotVisible($PLP_BREADCRUMB_REFINEMENT_VALUE);
						}	
				}					
		}		
	_click($HEADER_HOMELOGO);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125638", "Verify the functionality after selecting multiple filters in third category grid page  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	navigateSubCatPage($RootCatName, $CatName, $SubCatName);
	var $Filters=_collect("_heading3","/toggle/",_in($PLP_REFINEMENT));
	var $FilterNames=_collectAttributes("_heading3","/toggle/","sahiText",_in($PLP_REFINEMENT));
	var $ExpectedApplied= new Array();
	for(var $i=0;$i<$Filters.length;$i++)
		{
			if(_isVisible($Filters[$i]))
				{
					var $list=swatchlist($FilterNames[$i]);
					var $FilterOptions=_collect("_link","/swatch/",_in($list));
					var $FilterOptionNames=_collectAttributes("_link","/(.*)/","sahiText",_in($list));
					_click($FilterOptions[0]);	
					$ExpectedApplied.push($FilterOptionNames[0]+" x");
				}		
		}
	var $ActualApplied=AppliedFiltersInBreadcrumb();
	_assertEqual($ExpectedApplied.sort(),$ActualApplied.sort());
	_click($HEADER_HOMELOGO);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();







/*
var $t = _testcase("125644", "Verify the pagination in category landing page in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage($RootCatName, $CatName, $SubCatName);
	pagination();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/
var $t = _testcase("125648", "Verify the display of products in third category grid page and details displayed for each item in category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage($RootCatName, $CatName, $SubCatName);
	var $Products = _collect("_div", "/product-tile/", _in($PLP_PRODUCT_LIST));
	for (var $i=0; $i<$Products.length; $i++)
	 {
		//image
		 _assertVisible($PLP_PRODUCT_IMAGE, _in($Products[$i]));
		 //image thumb link
		 _assertVisible($PLP_PRODUCT_THUMBLINK, _in($Products[$i]));
		 //product name
		 _assertVisible($PLP_PRODUCTNAME_LINK, _in($Products[$i]));
		 //sales price
		 _assertVisible($PLP_PRODUCT_SALESPRICE, _in($Products[$i]));
		 //ratings
		 _assertVisible($PLP_PRODUCT_RATINGS, _in($Products[$i]));
		 //checkbox
		 _assertVisible($PLP_COMPARE_CHECKBOX, _in($Products[$i]));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("287632", "Verify the system's response on click of Products links in sub category landing page in the application both as an anonymous and a registered user.");
$t.start();
try
{
	navigateSubCatPage($RootCatName, $CatName, $SubCatName);
	var $productName=_getText($PLP_PRODUCTNAME_LINK);
	var $productPrice = _getText($PLP_PRODUCT_SALESPRICE);
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	//verification
	_assertEqual($productName,_getText($PDP_PRODUCTNAME));
	_assertEqual($productPrice,_getText($PDP_PRODUCT_PRICE));
	
	navigateSubCatPage($RootCatName, $CatName);
	var $productName=_getText($PLP_PRODUCTNAME_LINK);
	var $productPrice = _getText($PLP_PRODUCT_SALESPRICE);
	//click on image
	_click($PLP_PRODUCT_THUMBLINK);
	//verification
	_assertEqual($productName,_getText($PDP_PRODUCTNAME));
	_assertEqual($productPrice,_getText($PDP_PRODUCT_PRICE));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125644", "Verify the pagination in sub category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage($RootCatName, $CatName, $SubCatName);
	pagination();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

if(!isMobile())
{
	var $t = _testcase("287631", "Verify the system's reponse on selecting value from 'Products per page' drop down in the application both as an anonymous and a registered user.");
	$t.start();
	try
	{
		navigateSubCatPage($RootCatName, $CatName, $SubCatName);
		if(_isVisible($PLP_ITEMSPERPAGE_DROPDOWN_TOP) && _isVisible($PLP_ITEMSPERPAGE_DROPDOWN_BOTTOM))
			{	
				var $itemsDisp=_getText($PLP_ITEMSPERPAGE_DROPDOWN_TOP);
				for(var $i=0;$i<$itemsDisp.length;$i++)
					{
						_setSelected($PLP_ITEMSPERPAGE_DROPDOWN_TOP, $itemsDisp[$i]);
						//Validating the selection
						_assertEqual($itemsDisp[$i],_getSelectedText($PLP_ITEMSPERPAGE_DROPDOWN_TOP),"Selected option is displaying properly");
						_assertEqual($itemsDisp[$i],_getSelectedText($PLP_ITEMSPERPAGE_DROPDOWN_BOTTOM),"Selected option is displaying properly at footer");
						ItemsperPage($itemsDisp[$i]);
					}		
			}
		else
			{
				_assert(true, "View drop down is not present");
			}
		_click($HEADER_HOMELOGO);
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();
}

var $t = _testcase("287635", "Verify the system's response on selection of any sort by value in the application both as an anonymous and a registered user.");
$t.start();
try
{
	navigateSubCatPage($RootCatName, $CatName, $SubCatName);
	if(_isVisible($PLP_SORTBY_DROPDOWN_TOP) && _isVisible($PLP_SORTBY_DROPDOWN_BOTTOM))
	{
		var $sort=_getText($PLP_SORTBY_DROPDOWN_TOP);
		var $j=0;
		for(var $i=1;$i<$sort.length;$i++)
		{
			_setSelected($PLP_SORTBY_DROPDOWN_TOP, $sort[$i]);
			//Validating the selection
			_assertEqual($sort[$i],_getSelectedText($PLP_SORTBY_DROPDOWN_TOP),"Selected option is displaying properly");
			_assertEqual($sort[$i],_getSelectedText($PLP_SORTBY_DROPDOWN_BOTTOM),"Selected option is displaying properly");
			if($sort.indexOf($Category[$j][1])>-1)
			{
				_setSelected($PLP_SORTBY_DROPDOWN_TOP, $Category[$j][1]);
				sortBy($Category[$j][1]);
				$j++;
			}
		}
	}
	else
	{
		_assert(false, "sort by drop down is not present");
	}
	_click($HEADER_HOMELOGO);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

if(!isMobile())
{
var $t=_testcase("320009","Verify the navigation related to Quickview overlay In the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage($RootCatName, $CatName);
	//fetching the product name
	var $PName=_getText($PLP_PRODUCTNAME_LINK);
	//mouse over on image
	_mouseOver($PLP_PRODUCT_THUMBLINK);
	//Quick view button
	//_assertVisible($QUICKVIEWBUTTON_LINK);
	_click($QUICKVIEWBUTTON_LINK);  
	//quick view overlay
	_assertVisible($QUICKVIEW_OVERLAY);
	_assertEqual($PName, _getText($QUICKVIEW_PRODUCTNAME));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();
}
cleanup();
