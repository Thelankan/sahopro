_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("QuickView.xls");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();

var $t=_testcase("353075","Verify the UI of a Quick view modal window both as guest and registered user");
$t.start();
try
{
	navigateCatPage($RootCatName, $CatName);
	//fetching the product name
	var $PName=_getText($SHOPNAV_PLP_NAMELINK);
	//mouse over on image
	_mouseOver($CATEGORY_THUMBLINK);
	//Quick view button
	_click($QUICKVIEWBUTTON_LINK); 
	//title
	_assertVisible($QUICKVIEW_OVERLAY_HEADING_TITLE);
	_assertEqual($Category[0][2], _getText($QUICKVIEW_OVERLAY_HEADING_TITLE));
	//close
	_assertVisible($QUICKVIEW_OVERLAY_CLOSE_BUTTON);
	//Product name
	_assertVisible($CATEGORY_PDP_PRODUCTNAME);
	//Main image
	_assertVisible($QUICKVIEW_PRODUCTIMAGE_MAIN);
	//Product number
	_assertVisible($QUICKVIEW_PRODUCTNUMBER);
	//view details link
	_assertVisible($QUICKVIEW_VIEWFULLDETAILS_LINK);
	//Product price
	_assertVisible($QUICKVIEW_PRODUCT_PRICE);
	//Previous & next links
	_assertVisible($QUICKVIEW_PREVIOUS_NEXT_NAVIGATION);
	//stock status
	_assertVisible($QUICKVIEW_PRODUCT_AVAILIBILITY);
	//Promo messages if any Promotions are applied
	if(_isVisible($QUICKVIEW_PROMOTION_CALLOUT_MESSAGE))
	{
	  _assertVisible($QUICKVIEW_PROMOTION_CALLOUT_MESSAGE);
	  _log(_getText($QUICKVIEW_PROMOTION_CALLOUT_MESSAGE));
	}
	else
	{
	  _log("No promotional message to display");
	}
	//variations
	_assertVisible($QUICKVIEW_PRODUCT_VARIATIONS);
	//quantity
	_assertVisible($QUICKVIEW_QUANTITY_BOX);
	_assertEqual("1", _getValue($QUICKVIEW_QUANTITY_BOX));
	//Add to cart
	_assertVisible($QUICKVIEW_ADDTO_CART_SECTION);
	//Alternate images
	_assertVisible($QUICKVIEW_PDP_PRODUCT_THUMBNAILS);
	//selecting variations
	selectSwatch();
	//add to wish list
	_assertVisible($QUICKVIEW_ADDTOWISHLIST_LINK);
	//add to gift registry
	_assertVisible($QUICKVIEW_ADD_PRODUCT_GIFTREGISTRY_LINK);
	
	//closing overlay
	_click($QUICKVIEW_OVERLAY_CLOSE_BUTTON);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("343364", "Verify the behavior of hover ,click   functionality on product tile and its respective elements in category landing page  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	
//click on quick view link
_click($QUICKVIEW_LINK_PLP);
var $quickviewProductName=_getText($QUICKVIEW_PRODUCTNAME);
fetchingPriceQuickView();
//click on product image
_click($PDP_PRODUCTNAME);
//SHOULD NAVIGATE TO RESPECTIVE PAGE
_assertVisible($PDP_MAIN);
_assertEqual($quickviewProductName,_getText($PDP_PRODUCTNAME));
//Navigate to category landing page
navigateCatPage($RootCatName,$CatName);
//click on quick view link
_click($QUICKVIEW_LINK_PLP);
var $quickviewProductName=_getText($QUICKVIEW_PRODUCTNAME);
//click on product image
_click($PDP_PRODUCTNAME);
//SHOULD NAVIGATE TO RESPECTIVE PAGE
_assertVisible($PDP_MAIN);
_assertEqual($quickviewProductName,_getText($PDP_PRODUCTNAME));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("353069","Verify the functionality of Quick view modal window both as guest and registered user.");
$t.start();
try
{

//navigate to catef=gory page
navigateCatPage($RootCatName, $CatName);
//fetching the product name
var $PName=_getText($SHOPNAV_PLP_NAMELINK);
//mouse over on image
_mouseOver($CATEGORY_THUMBLINK);
//Quick view button
_click($QUICKVIEWBUTTON_LINK);
//closing overlay
_assertVisible($QUICKVIEW_OVERLAY_CLOSE_BUTTON);
//click outside of overlay
_click($QUICKVIEW_OUTSIDEOF_OVERLAY);
//click on outside of overlay
_assertNotVisible($QUICKVIEW_OVERLAY_CLOSE_BUTTON);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("353097","Verify the display of price in Quick view modal window when a product has both list price and sale price configured");
$t.start();
try
{
	
//Quick view product price
_assertVisible($QUICKVIEW_PRODUCTPRICE);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

var $t=_testcase("353071/353072/353107","Verify the 'Add to bag' functionality in Quick view both as an anonymous and a registered user./Verify the display of product tile   on mouse hover  in Category landing page In the application both as an anonymous and a registered user" +
		"Verify the  behavior of the availability status  for In stock products in the Quick view modal windows a guest and a registered user.");
$t.start();
try
{
	navigateCatPage($RootCatName, $CatName);
	_mouseOver($CATEGORY_THUMBLINK);
	//quick view link
	_assertVisible($QUICKVIEWBUTTON_LINK);
	_click($QUICKVIEWBUTTON_LINK);
	var $Product=_getText($CATEGORY_PDP_PRODUCTNAME);
	//var $ProductPrice=_getText($QUICKVIEW_PRODUCT_PRICE);
	var $ProductPrice=_getText($CATEGORY_CART_SALES_PRICE); 
	//selecting variations
	selectSwatch();
	//Quick view availability message
	_assertVisible($QUICKVIEW_AVAILABILITY_MESSGAE);
	//click in add to cart
	_click($QUICKVIEW_ADDTO_CART);
	//navigate to cart
	_click($QUICKVIEW_MINICART_LINK);
	//Checking Product name
	_assertEqual($Product, _getText($CART_NAME_LINK));
	_assertEqual($ProductPrice,_getText($CATEGORY_CART_SALES_PRICE));
	//removing item from cart
	while(_isVisible($CATEGORY_CART_REMOVE_LINK))
	{
	_click($CATEGORY_CART_REMOVE_LINK);
	}
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

var $t=_testcase("353105","Verify the behavior of the 'QTY' form field in the Quick view modal windows a guest or as a authenticated user.");
$t.start();
try
{
	navigateCatPage($RootCatName, $CatName);
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	//quantity
	_assertVisible($QUICKVIEW_QUANTITY_BOX);
	_assertEqual("1", _getValue($QUICKVIEW_QUANTITY_BOX));
	for(var $i=0;$i<$Qty.length;$i++)
		{
			_setValue($QUICKVIEW_QUANTITY_BOX, $Qty[$i][1]);
			//selecting variations
			selectSwatch();
			//click in add to cart
			_click($QUICKVIEW_ADDTO_CART);
			//navigate to cart
			_click($QUICKVIEW_MINICART_LINK);
			if($i==2)
				{
				_assertEqual($Qty[$i][1],_getText($CATEGORY_CART_QTY_DROPDOWN));
				}
			else
				{
				_assertEqual($Qty[0][2],_getText($CATEGORY_CART_QTY_DROPDOWN));
				}
			while(_isVisible($CATEGORY_CART_REMOVE_LINK))
			{
			_click($CATEGORY_CART_REMOVE_LINK);
			}
			navigateCatPage($RootCatName, $CatName);
			_mouseOver($CATEGORY_THUMBLINK);
			_click($QUICKVIEWBUTTON_LINK);
		}
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

var $t = _testcase("353099","Verify the application behavior on selecting a size variant on Quick view modal window of the application");
$t.start();
try
{
	//Navigate ti quick view page
	navigateCatPage($RootCatName, $CatName);
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	
	_assertVisible($PDP_SIZE_LIST);
	var $Sizes =_collect("_link","/swatchanchor/",_in($PDP_SIZE_LIST));
	var $TotalSizes=$Sizes.length;
	var $j=1;
	//only one swatchanchor is there
	if($TotalSizes==1)
		{
			if(_isVisible($PDP_SIZE_LISTITEM_SELECTED))
				  {
					  var $title=_getAttribute($PDP_SWATCHANCHOR,_in($PDP_SIZE_LIST),"title").split(" ")[2];
					  //verify whether selected value is displaying or not
					  _assertVisible($PDP_SELECTED_SWATCH,_in($PDP_SIZE_LIST));
					  _assertEqual($title, _getText($PDP_SELECTED_SWATCH,_in($PDP_SIZE_LIST)));
				  }
			else
				  {
					  //click on swatch anchor
					  _click($PDP_SWATCHANCHOR,_in($PDP_SIZE_LIST));
					  var $title=_getAttribute($PDP_SWATCHANCHOR,_in($PDP_SIZE_LIST),"title");
					  //verify whether selected value is displaying or not
					  _assertVisible($PDP_SELECTED_SWATCH,_in($PDP_SIZE_LIST));
					  _assertEqual($title, _getText($PDP_SELECTED_SWATCH,_in($PDP_SIZE_LIST)));
				  }
		}
	//more than one swatchanchor is there
	else
	  {
			for (var $i=0; $i<$TotalSizes; $i++)
			    {
					  if($i==0 || $i==1)
						{
							var $Exp=_getText($PDP_SELECTABLE_SWATCH, _in($PDP_SIZE_LIST));
							_mouseOver($Sizes[$i]);
							var $ExpText=_getAttribute($Sizes[$i], "title").split(" ")[2];
							_assertEqual($Exp,$ExpText);
							_click($Sizes[$i]);
							var $Act=_getText($PDP_SELECTED_SWATCH, _in($PDP_SIZE_LIST));
							_assertEqual($Exp,$Act);
						}
					else
						{
							var $Exp=_getText(_listItem("selectable["+$j+"]", _in($PDP_SIZE_LIST)));
							_mouseOver($Sizes[$i]);
							var $ExpText=_getAttribute($Sizes[$i], "title").split(" ")[2];
							_assertEqual($Exp,$ExpText);
							_click($Sizes[$i]);
							var $Act=_getText($PDP_SELECTED_SWATCH, _in($PDP_SIZE_LIST));
							_assertEqual($Exp,$Act);
							$j++;
						}	
					 _assert(true, "Selected size is appropriate");	
				}
	  }     	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("353098","Verify the behavior of the Variation - Fit attribute in the product detail page as a guest and authenticated.");
$t.start();
try
{
	_assert(false,"configuration required");
	//Navigate ti quick view page
	navigateCatPage($RootCatName, $CatName);
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	//FIT LIST
	_assertVisible($PDP_FIT_LIST);
	var $fit=_collect("_link","/swatchanchor/",_in($PDP_FIT_LIST));
	var $TotalFitSizes=$fit.length;
	var $j=1;
	//only one swatchanchor is there
	if($TotalFitSizes==1)
		{
			if(_isVisible($PDP_FIT_LISTITEM_SELECTED))
				  {
					  var $title=_getAttribute($PDP_SWATCHANCHOR,_in($PDP_FIT_LIST),"title").split(" ")[2];
					  //verify whether selected value is displaying or not
					  _assertVisible($PDP_SELECTED_SWATCH,_in($PDP_FIT_LIST));
					  _assertEqual($title, _getText($PDP_SELECTED_SWATCH,_in($PDP_FIT_LIST)));
				  }
			else
				  {
					  //click on swatch anchor
					  _click($PDP_SWATCHANCHOR,_in($PDP_FIT_LIST));
					  var $title=_getAttribute($PDP_SWATCHANCHOR,_in($PDP_FIT_LIST),"title");
					  //verify whether selected value is displaying or not
					  _assertVisible($PDP_SELECTED_SWATCH,_in($PDP_FIT_LIST));
					  _assertEqual($title, _getText($PDP_SELECTED_SWATCH,_in($PDP_FIT_LIST)));
				  }
		}
	//more than one swatchanchor is there
	else
	  {
			for (var $i=0; $i<$TotalSizes; $i++)
			    {
					  if($i==0 || $i==1)
						{
							var $Exp=_getText($PDP_SELECTABLE_SWATCH, _in($PDP_FIT_LIST));
							_mouseOver($fit[$i]);
							var $ExpText=_getAttribute($fit[$i], "title").split(" ")[2];
							_assertEqual($Exp,$ExpText);
							_click($fit[$i]);
							var $Act=_getText($PDP_SELECTED_SWATCH, _in($PDP_FIT_LIST));
							_assertEqual($Exp,$Act);
						}
					else
						{
							var $Exp=_getText(_listItem("selectable["+$j+"]", _in($PDP_FIT_LIST)));
							_mouseOver($fit[$i]);
							var $ExpText=_getAttribute($fit[$i], "title").split(" ")[2];
							_assertEqual($Exp,$ExpText);
							_click($fit[$i]);
							var $Act=_getText($PDP_SELECTED_SWATCH, _in($PDP_FIT_LIST));
							_assertEqual($Exp,$Act);
							$j++;
						}	
					 _assert(true, "Selected size is appropriate");	
				}
	  }     	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("353100","Verify the functionality of size chart link on Quick view modal window of the application");
$t.start();
try
{
	_assert(false,"configuration required");
	//Navigate ti quick view page
	navigateCatPage($RootCatName, $CatName);
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	//verify the presence of size chart
	_assertVisible($PDP_SIZECHART_LINK);
	_click($PDP_SIZECHART_LINK);
	_wait(2000, _isVisible($PDP_SIZECHART_URL));
	if (_isVisible($PDP_SIZECHART_CONTENT))       
	     {
	       _assert(true, "Size chart dialogue is opened successfully");
	     }
	   else  
		 {
		   _assert(false, "Size chart window is not opened");
		 }
	_assertVisible($PDP_SIZECHART_CLOSE_BUTTON);
	_click($PDP_SIZECHART_CLOSE_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("353101/353102","verify the label of color or heading of color in  quickview modal window as a guest and a authenticated user.");
$t.start();
try
{
	navigateCatPage($PDP_Standard[0][0],$PDP_Standard[2][0]);
	var $PName=_getText($PLP_PRODUCTNAME_LINK);
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	//verify the select color functionality 	
	_assertVisible($PDP_COLOR_LIST);
	var $Colors=_collect("_link","/swatchanchor/",_in($PDP_COLOR_LIST));
	var $TotalColors=$Colors.length;
	var $k=1;
	//only one swatch anchor is there
	if($TotalColors==1)
		{	
			if(_isVisible($PDP_COLOR_LISTITEM_SELECTED))
				  {
					  var $title=_getAttribute($Colors[0],"title").split(" ")[2];
					  //verify whether selected value is displaying or not
					  _assertVisible($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST));
					  _assertEqual($title, _getText($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST)));
				  }
			else
				  {
					  //click on swatchanchor
					  _click($Colors[0]);
					  var $title=_getAttribute($Colors[0],"title");
					  //verify whether selected value is displaying or not
					  _assertVisible($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST));
					  _assertEqual($title, _getText($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST)));
				  }
		}
	//When there is more than one swatch anchor
	else
		{
			 for(var $i=0;$i<$TotalColors;$i++)
				{
					if($i==0 || $i==1)
						{
							var $Exp=_getText($PDP_SELECTABLE_SWATCH, _in($PDP_COLOR_LIST));
							_mouseOver($Colors[$i]);
							var $ExpText=_getAttribute($Colors[$i], "title");
							_assertEqual($Exp,$ExpText);
							 _click($Colors[$i]);
							var $Act=_getText($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST));
							_assertEqual($Exp,$Act);
						}
					else
						{
							var $Exp=_getText(_listItem("selectable["+$k+"]", _in($PDP_COLOR_LIST)));
							_mouseOver($Colors[$i]);
							var $ExpText=_getAttribute($Colors[$i], "title");
							_assertEqual($Exp,$ExpText);
							 _click($Colors[$i]);
							var $Act=_getText($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST));
							_assertEqual($Exp,$Act);
							$k++;
						}
					  }
				_assert(true, "Selected color is appropriate");
		}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("353070","Verify 'view full details' link in 'Quick view' overlay in the application both as an anonymous and a registered user");
$t.start();
try
{
	
navigateCatPage($RootCatName, $CatName);
_mouseOver($CATEGORY_THUMBLINK);
_click($QUICKVIEWBUTTON_LINK);
var $Product=_getText($CATEGORY_PDP_PRODUCTNAME);
//var $ProductPrice=_getText($QUICKVIEW_PRODUCT_PRICE);
var $ProductPrice=_getText($CATEGORY_CART_SALES_PRICE);
//click on view details link
_click($QUICKVIEW_VIEWFULLDETAILS_LINK);
//validating the navigation
_assertEqual($Product,_getText($CATEGORY_PDP_PRODUCTNAME));
_assertEqual($ProductPrice,_getText($CATEGORY_CART_SALES_PRICE));
//bread crumb
_assertVisible($PAGE_BREADCRUMB);
_assertContainsText($Product, $PAGE_BREADCRUMB);
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("353104","Verify the field validations for quantity text field on Quick view modal window of the application");
$t.start();
try
{
	navigateCatPage($RootCatName, $CatName);
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	//when no variants are selected verify the add to cart button
	_assertNotVisible($PDP_ADDTOCART_BUTTON);
	//Add to cart button should be in disabled state
	_assertVisible($PDP_ADDTOCART_DISABLED_BUTTON);
	//select the variants
	selectSwatch();
	//now verify the add to cart dispaly
	_assertVisible($PDP_ADDTOCART_BUTTON);
	//quantity box validation
	var $cartQuantity=0;
	for(var $j=0;$j<$Qty_Validations.length;$j++)
		{
			if($j==0)
				{
					_setValue($PDP_QUANTITY_TEXTBOX,"");
				}
			else
				{
					_setValue($PDP_QUANTITY_TEXTBOX,$Qty_Validations[$j][1]);
				}
			if($j==0)
				{
					_click($PDP_ADDTOCART_BUTTON);
					_wait(2000);
					var $actQuantity=_getText($MINICART_QUANTITY);
					//verifying the quantity
					_assertEqual("1",$actQuantity);
				}
			//out of stock
			if($j==3)
				{
					//not available message
					_assertVisible($PDP_PRODUCT_NOTAVAILABLE_MESSAGE);
					_assertEqual($Qty_Validations[3][2], _getText($PDP_PRODUCT_NOTAVAILABLE_MESSAGE));
					//add to cart should disable
					_assertNotVisible($PDP_ADDTOCART_DISABLED_BUTTON);
				}
			else
				{						
					var $cartQuantity=_getText($MINICART_QUANTITY);
					_log($cartQuantity);
					//click on add to cart button
					_click($PDP_ADDTOCART_BUTTON);
					//default quantity should add to cart
					var $expQuantity=parseFloat($cartQuantity)+parseInt($Qty_Validations[0][2]);
					var $actQuantity=_getText($MINICART_QUANTITY);
					//verifying the quantity
					_assertEqual($expQuantity,$actQuantity);
				}		
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("353106","Verify the Dynamic change  in the 'QTY' form field name for fabrics products   in quickview modal window  and verify its behavior.");
$t.start();
try
{

//search procuct
search($QuickView[0][3]);
var $producyNameInPdp=_getText($PDP_PRODUCTNAME);
//select the variants
selectSwatch();

for(var $i=0;$i<$TotalColors;$i++)
{
	
	//enter the yards
	_setValue(_textbox("Quantity", _in(_div("Yards"))),$QuickView[$i][4]);
	var $yardsQuantity=_getText(_textbox("Quantity", _in(_div("Yards"))));
	//click in add to cart
	_click($QUICKVIEW_ADDTO_CART);
	
	//error messages should display
	if ($i==0 && $i==2)
	{
	_assertVisible(_div("max-line-item"));
	_assertEqual($QuickView[0][5], _getText(_div("max-line-item")));
	}
	else
		{
		//navigate to cart
		_click($QUICKVIEW_MINICART_LINK);
		//Checking Product name
		_assertEqual($producyNameInPdp, _getText($CATEGORY_CART_NAME));
		_assertEqual($yardsQuantity,_getText($CART_QTY_DROPDOWN));
		}
	
}

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

//Login()
_click($QUICKVIEW_LOGIN_LINK);
login();

var $t=_testcase("353074","Verify the system's response on click on 'Add to wish list' link in Quick view in the application both as an anonymous and a registered user");
$t.start();
try
{
	
navigateCatPage($RootCatName, $CatName);
_mouseOver($CATEGORY_THUMBLINK);
_click($QUICKVIEWBUTTON_LINK);
var $Pname=_getText($CATEGORY_PDP_PRODUCTNAME);
//selecting swatches
selectSwatch();
//add to wish list
_assertVisible($QUICKVIEW_ADDTOWISHLIST_LINK);
_click($QUICKVIEW_ADDTOWISHLIST_LINK);
//bread crumb
_assertVisible($PAGE_BREADCRUMB);
_assertEqual($Category[0][9], _getText($PAGE_BREADCRUMB));
//heading
_assertVisible($QUICKVIEW_FINDSOMEONES_WISHLIST);
//Product
_assertVisible(_cell("/"+$Pname+"/"));
//removing item
_click($QUICKVIEW_WISHLIST_REMOVE_BUTTON, _in(_row("/"+$Pname+"/")));
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


//##################### Prevail scripts #########################
var $t=_testcase("125622","Verify the system's response on click on 'Add to Gift Registry' link in Quick view  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage($RootCatName, $CatName);
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	var $Pname=_getText($CATEGORY_PDP_PRODUCTNAME);
	//selecting swatches
	selectSwatch();
	//add to gift registry
	_assertVisible($QUICKVIEW_ADD_PRODUCT_GIFTREGISTRY_LINK);
	_click($QUICKVIEW_ADD_PRODUCT_GIFTREGISTRY_LINK);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Category[0][8],_getText($PAGE_BREADCRUMB));
	//heading
	_assertVisible($QUICKVIEW_GIFTREGISTRY_HEADING); 
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

var $t=_testcase("148587","Verify the system response on click of color swatch displayed on 'Quick view' overlay in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage($RootCatName, $CatName);
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	var $Product=_getText($CATEGORY_PDP_PRODUCTNAME);	
	var $colors =SwatchanchorCountColor();
	for(var $i=0;$i<$colors;$i++)
		{			
			var $a=_getAttribute(_image("/(.*)/", _in(_link("swatchanchor["+$i+"]", _in($QUICKVIEW_COLOR_LIST)))),"title");
			_click(_link($a, _in($QUICKVIEW_COLOR_LIST)));
			_assertEqual($a,_getText($QUICKVIEW_SELECTED_SWATCH,_in($QUICKVIEW_COLOR_LIST)));
			_assert(true, "Selected color is appropriate");
		}	
	//closing overlay
	_click($QUICKVIEW_OVERLAY_CLOSE_BUTTON);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


var $t=_testcase("126824","Verify 'Next' and 'Previous' links functionality in Quick view in category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage($RootCatName, $CatName);
	var $numberOfProducts = YouMightalsoLikeProductCount();
	var $prodNames= YouMightalsoLikeProducts();
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	for ($i=0; $i<$numberOfProducts-1; $i++)
	{
	  var $name = _getText($CATEGORY_PDP_PRODUCTNAME);
	  _assertEqual($prodNames[$i],$name);
	  _click($QUICKVIEW_NEXT_LINK);
	}
	for($i=$numberOfProducts-2; $i>0; $i--)
	{
	  _click($QUICKVIEW_PREVIOUS_LINK);
	  var $name = _getText($CATEGORY_PDP_PRODUCTNAME);
	  _assertEqual($prodNames[$i],$name);	  
	 }
	//closing overlay
	_click($QUICKVIEW_OVERLAY_CLOSE_BUTTON);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

//'Add to wish list' as guest
var $t=_testcase("125621","Verify the system's response on click on 'Add to wish list' link in Quick view in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage($RootCatName, $CatName);
	_mouseOver($CATEGORY_THUMBLINK);
	_click($QUICKVIEWBUTTON_LINK);
	//selecting swatches
	selectSwatch();
	//add to wish list
	_assertVisible($QUICKVIEW_ADDTOWISHLIST_LINK);
	_click($QUICKVIEW_ADDTOWISHLIST_LINK);
	//validating the navigation
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Category[0][4], _getText($PAGE_BREADCRUMB));
	//heading
	_assertVisible($QUICKVIEW_LOGIN_HEADING);
	_assertVisible($QUICKVIEW_FINDSOMEONES_WISHLIST);
	//wish list section
	_assertVisible($QUICKVIEW_GUEST_FINDSOMEONES_WISHLIST_SECTION);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

/*var $t=_testcase(" "," ");
$t.start();
try
{
	

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()*/


cleanup();
