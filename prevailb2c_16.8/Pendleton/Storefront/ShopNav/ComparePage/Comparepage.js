_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("ComparePage.xls");


SiteURLs();
cleanup();
_setAccessorIgnoreCase(true); 

var $t = _testcase("125458/125459","Verify the display of UI elements in Compare Bucket/navigation to Compare Bucket section  in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to subcat page	
	navigateCatPage($Compare[0][0],$Compare[1][0]);
	//add two items to items to compare
	for(var $i=0;$i<2;$i++)
		{
			_check(_checkbox("compare-check["+$i+"]"));
		}
	//verify whether compare section appeared or not
	_assertVisible($COMPARE_BUCKET_ITEMS);
	//verify the UI
	_assertVisible($COMPARE_BUCKET_HEADING);
	var $TotalProducts=totalNumberOfComparableProducts();
	_assertEqual($Compare[0][1],$TotalProducts);
	//compare items button
	_assertVisible($COMPARE_ITEMS_BUTTON);
	//clear all
	_assertVisible($COMPARE_CLEARALL_BUTTON);
	var $TotalCompareItems=ActiveProductsInCompareBucket();
	//remove link
	for(var $i=0;$i<$TotalCompareItems;$i++)
		{
			_assertVisible(_link("compare-item-remove["+$i+"]"));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125462","Verify the functionality of 'Clear All' button in Compare bucket section  in the application both as an anonymous and a registered user");
$t.start();
try
{
	//click on clear all button
	_click($COMPARE_CLEARALL_BUTTON);
	//compare section shouln't visible
	_assertNotVisible($COMPARE_BUCKET_ITEMS);
	_assertNotVisible($COMPARE_HEADING);
	_assertNotVisible($COMPARE_ITEMS_BUTTON);
	_assertNotVisible($COMPARE_CLEARALL_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125463","Verify the functionality of 'Comparison Grid' by adding more then 6 products to the Compare image widget box in the application both as an anonymous and a registered user");
$t.start();
try
{	
	//add 6 items for compare
	for(var $i=0;$i<8;$i++)
		{
			_check(_checkbox("compare-check["+$i+"]"));
			if($i==6)
				{
					//pop up should come click on cancel
					_expectConfirm("/This will remove/",false);
					//should remain in same page
					_assertVisible($COMPARE_BUCKET_ITEMS);
				}
			if($i==7)
				{
					//click on ok
					_expectConfirm("/This will remove/",true);
					//first item should replace 
					 _assertFalse($CATEGORY_COMPARE_CHECKBOX.checked);
				}
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("125460","Verify the functionality of 'Remove(X)' image in Compare image widget box  in the application both as an anonymous and a registered user");
$t.start();
try
{
	//fetch total no.of added compare items
	var $TotalCompareItems=ActiveProductsInCompareBucket();
	_log("$TotalCompareItems"+$TotalCompareItems);
	//Total check boxes
	//var $TotalCheckboxes=CompareCheckboxCount();
	var $CompareItemsBeforeRemoval=$TotalCompareItems;
	var $CompareItemsAfterRemoval=0;
	for(var $i=0;$i<$TotalCompareItems;$i++)
		{
			//click on remove link
			_click($COMPARE_ITEM_REMOVE_ICON);
			_wait(2000);
			$CompareItemsAfterRemoval=ActiveProductsInCompareBucket();
			//verify after click on remove link
			if($i==$TotalCompareItems-1)
				{
					//should navigate back to sub category
					_assertNotVisible($COMPARE_BUCKET_ITEMS);
					_assertContainsText($Compare[3][0], $PAGE_BREADCRUMB);
				}
			else
				{
					var $ExpectedProcucts=$CompareItemsBeforeRemoval-1;
					//Verifying compare items in compare section
					_assertEqual($ExpectedProcucts,$CompareItemsAfterRemoval);
					//verifying check boxes checked in Products display section
					_assertNotTrue(_checkbox("compare-check["+$i+"]").checked);
					$CompareItemsBeforeRemoval=$CompareItemsBeforeRemoval-1;
				}		
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125461/125464/125465/148597/287337","Verify the functionality of 'Compare Items' button/verify the Navvigation to/UI in Compare bucket section in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to subcat page	
	navigateCatPage($Compare[0][0],$Compare[1][0]);
	//add two items to items to compare
	for(var $i=0;$i<2;$i++)
		{
			_check(_checkbox("compare-check["+$i+"]"));
		}
	//verify whether compare section appeared or not
	_assertVisible($COMPARE_BUCKET_ITEMS);
	//fetch total no.of added compare items
	var $TotalCompareItems=ActiveProductsInCompareBucket();
	//fetching page bread crumb
	var $Pagebreadcrumb=_getText($PAGE_BREADCRUMB);
	//click on compare items button
	_click($COMPARE_ITEMS_BUTTON);
	//verify the navigation and UI
	_assertVisible($COMPARE_HEADING);
	//back to results links
	_assertVisible($COMPARE_BACKTORESULTS_LINK);
	_assertVisible($COMPARE_BACKTORESULTS_BOTTOM_LINK);
	//remove,name,images links
	for(var $i=0;$i<$TotalCompareItems;$i++)
		{
			//remove link
			_assertVisible($COMPARE_REMOVE, _in(_tableHeader("product["+$i+"]")));
			//Image
			_assertVisible($COMPARE_PRODUCT_IMAGE, _in(_tableHeader("product["+$i+"]")));
			//NAME
			_assertVisible($COMPARE_PRODUCT_NAME, _in(_tableHeader("product["+$i+"]")));
			//Price
			_assertVisible($COMPARE_PRODUCT_PRICE, _in(_tableHeader("product["+$i+"]")));
			//rating
			_assertVisible($COMPARE_PRODUCT_RATING, _in(_tableHeader("product["+$i+"]")));
			//add to cart
			_assertVisible($COMPARE_PRODUCT_ADDTOCART, _in(_tableHeader("product["+$i+"]")));
			//add to wish list
			_assertVisible($COMPSARE_ADDTOWISHLIST, _in(_tableHeader("product["+$i+"]")));
			//add to gift registry
			_assertVisible($COMPARE_ADDTOGIFTREGISTRY, _in(_tableHeader("product["+$i+"]")));
		}
	//Other compare items should not be visible
	_assertNotVisible($COMPARE_OTHERITEMS);
	_assertNotVisible($COMPARE_CATEGORY_LIST);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("125467/287293","Verify the functionality of 'Remove(X) ' image in Compare Products page in the application both as an anonymous and a registered user.");
$t.start();
try
{
	//products in compare page
	var $ProductInCompare=ProductCountInComparePage();
	var $ProductInCompareBefore=$ProductInCompare;
	for(var $i=$ProductInCompare-1;$i>=0;$i--)
		{
			if($i!=0)
				{
					//click on remove link
					_click($COMPARE_REMOVE);
					var $ExpectedProcucts=$ProductInCompareBefore-1;
				}					
			if($i==0 ||$ExpectedProcucts==1)
				{
					//should navigate back to sub category
					_assertVisible($PAGE_BREADCRUMB);
					_assertEqual($Pagebreadcrumb, _getText($PAGE_BREADCRUMB));
				}
			else
				{					
					_wait(5000);
					var $ProductInCompare=ProductCountInComparePage();
					_assertEqual($ExpectedProcucts,$ProductInCompare);
					$ProductInCompareBefore=$ProductInCompareBefore-1;
				}
		}
	//Should navigate back to PLP page
	_assertVisible($COMPARE_BUCKET_ITEMS);
	_assertVisible($SEARCHRESULT_SEARCHRESULT_ITEMS);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//click on clear all button
_click($COMPARE_CLEARALL_BUTTON);
//compare section shouln't visible
_assertNotVisible($COMPARE_BUCKET_ITEMS);


var $t = _testcase("125466","Verify the navigation of 'Back to Results' link in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to subcat page	
	navigateCatPage($Compare[0][0],$Compare[1][0]);
	//add two items to items to compare
	for(var $i=0;$i<2;$i++)
		{
			_check(_checkbox("compare-check["+$i+"]"));
		}
		
	//fetching page bread crumb
	var $Pagebreadcrumb=_getText($PAGE_BREADCRUMB);
	//click on compare items button
	_click($COMPARE_ITEMS_BUTTON);
	//click on back to results link
	_click($COMPARE_BACKTORESULTS_LINK);
	//verify the navigation back to PLP
	_assertEqual($Pagebreadcrumb, _getText($PAGE_BREADCRUMB));
	_assertVisible($COMPARE_BUCKET_ITEMS);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("148590","Verify the mouse hover functionality if user place the mouse over on any Product image in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to subcat page	
	navigateCatPage($Compare[0][0],$Compare[1][0]);
	//add two items to items to compare
	for(var $i=0;$i<2;$i++)
		{
			_check(_checkbox("compare-check["+$i+"]"));
		}
	//click on compare items button
	_click($COMPARE_ITEMS_BUTTON);
	var $ProductInCompare=ProductCountInComparePage();
	//verify the quick view functionality
	for(var $i=0;$i<$ProductInCompare;$i++)
		{			
			var $ProductName=_getText(_link("name-link["+$i+"]"));
			_mouseOver(_div("product-image["+$i+"]"));
			_wait(2000);
			//click on quick view overlay
			_click(_link("quickviewbutton",_div("product-image["+$i+"]")));
			_wait(2000);
			//verification
			//quickview overlay
			_assertVisible($QUICKVIEW_OVERLAY);
			//Product name
			_assertVisible($QUICKVIEW_PRODUCTNAME);
			_assertEqual($ProductName, _getText($QUICKVIEW_PRODUCTNAME));
			//close button
			_assertVisible($QUICKVIEW_OVERLAY_CLOSE_BUTTON);
			//close the overlay
			_click($QUICKVIEW_OVERLAY_CLOSE_BUTTON);
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148591","Verify the navigation if user click on any product image or product name link in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
	for(var $i=0;$i<$ProductInCompare;$i++)
		{
			var $ProductName=_getText(_link("name-link["+$i+"]"));
			//click on name link
			_click(_link("name-link["+$i+"]"));
			//verify the navigation
			_assertContainsText($ProductName, $PAGE_BREADCRUMB);
			_assertVisible($PDP_PRODUCTNAME);
			_assertEqual($ProductName, _getText($PDP_PRODUCTNAME));
			//navigate to subcat page	
			navigateCatPage($Compare[0][0],$Compare[1][0]);
			//click on compare items button
			_click($COMPARE_ITEMS_BUTTON);
			//click on product image link
			_click(_image("/(.*)/",_in(_div("product-image["+$i+"]"))));
			//verify the navigation
			_assertContainsText($ProductName, $PAGE_BREADCRUMB);
			_assertVisible($PDP_PRODUCTNAME);
			_assertEqual($ProductName, _getText($PDP_PRODUCTNAME));
			//navigate to subcat page	
			navigateCatPage($Compare[0][0],$Compare[1][0]);
			//click on compare items button
			_click($COMPARE_ITEMS_BUTTON);
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148593","Verify the functionality if user click on 'ADD TO CART' button in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to subcat page	
	navigateCatPage($Compare[0][0],$Compare[1][0]);
	//add two items to items to compare
	for(var $i=0;$i<2;$i++)
		{
			_check(_checkbox("compare-check["+$i+"]"));
		}
	//click on compare items button
	_click($COMPARE_ITEMS_BUTTON);
	var $ProductInCompare=ProductCountInComparePage();
	//verify the add to cart functionality
	for(var $i=0;$i<$ProductInCompare;$i++)
		{
			var $ProductName=_getText(_link("name-link["+$i+"]"));
			_wait(2000);
			//click on quick view overlay
			_click(_submit("Add to Cart["+$i+"]"));
			_wait(2000);
			//verify whether overlay opened or not 
			//quickview overlay
			_assertVisible($QUICKVIEW_OVERLAY);
			//Product name
			_assertVisible($QUICKVIEW_PRODUCTNAME);
			_assertEqual($ProductName, _getText($QUICKVIEW_PRODUCTNAME));
			//close button
			_assertVisible($QUICKVIEW_OVERLAY_CLOSE_BUTTON);
			//close the overlay
			_click($QUICKVIEW_OVERLAY_CLOSE_BUTTON);
		} 
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148595","Verify the application behavior on click of add to wish list link for a product in Compare Products page as a guest user.");
$t.start();
try
{
	for(var $i=0;$i<$ProductInCompare;$i++)
		{	
			//click on add to wish list link
			_click(_link("Add to Wishlist["+$i+"]"));
			//verify the navigation
			_assertContainsText($Compare[0][2], $PAGE_BREADCRUMB);
			_assertVisible($LOGIN_HEADING);
			//navigate to subcat page	
			navigateCatPage($Compare[0][0],$Compare[1][0]);
			//click on compare items button
			_click($COMPARE_ITEMS_BUTTON);
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("287340","Verify the application behavior on click of add to gift registry link for a product in Compare Products page as a guest user.");
$t.start();
try
{
	for(var $i=0;$i<$ProductInCompare;$i++)
		{	
		//click on add to wish list link
		_click(_link("Add to gift registry["+$i+"]"));
		//verify the navigation
		_assertContainsText($Compare[0][3], $PAGE_BREADCRUMB);
		_assertVisible($GIFTREGISTRY_LOGIN_HEADING);
		//navigate to subcat page	
		navigateCatPage($Compare[0][0],$Compare[1][0]);
		//click on compare items button
		_click($COMPARE_ITEMS_BUTTON);
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("287338/148596","Verify the display and functionality if user select any value from other compare items drop down in compare products page when products of different sub categories are added to compare bucket");
$t.start();
try
{
	var $ProductsCategory1=ProductCountInComparePage();
	//navigate to subcat page	
	navigateCatPage($Compare[0][0],$Compare[2][0]);
	//add two items to items to compare
	for(var $i=0;$i<2;$i++)
		{
			_check(_checkbox("compare-check["+$i+"]"));
		}
	//click on compare items button
	_click($COMPARE_ITEMS_BUTTON);
	var $ProductsCategory2=ProductCountInComparePage();
	//'Other Compare Items'
	_assertVisible($COMPARE_OTHERITEMS);
	_assertVisible($COMPARE_CATEGORY_LIST);
	//select the category1 from drop down
	_setSelected($COMPARE_CATEGORY_LIST,$Compare[1][0]);
	var $Category1ProductsDisplayed=ProductCountInComparePage();
	_assertEqual($ProductsCategory1,$Category1ProductsDisplayed);
	//select the category2 from drop down
	_setSelected($COMPARE_CATEGORY_LIST,$Compare[2][0]);
	var $Category2ProductsDisplayed=ProductCountInComparePage();
	_assertEqual($ProductsCategory2,$Category2ProductsDisplayed);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


navigateCatPage($Compare[0][0],$Compare[1][0]);
//click on clear all button
_click($COMPARE_CLEARALL_BUTTON);
//compare section shouln't visible
_assertNotVisible($COMPARE_BUCKET_ITEMS);
navigateCatPage($Compare[0][0],$Compare[2][0]);
//click on clear all button
_click($COMPARE_CLEARALL_BUTTON);
//compare section shouln't visible
_assertNotVisible($COMPARE_BUCKET_ITEMS);



//******************************************Register User 

//login to the application
_click($HEADER_LOGIN_LINK);
login();
//navigate to wish list and delete all added items 
clearWishList();
Delete_GiftRegistry();

var $t = _testcase("148594","Verify the application behavior on click of add to wish list link for a product in Compare Products page as a registered user.");
$t.start();
try
{
	//navigate to subcat page	
	navigateCatPage($Compare[0][0],$Compare[2][0]);
	//add two items to items to compare
	for(var $i=0;$i<2;$i++)
		{
			_check(_checkbox("compare-check["+$i+"]"));
		}
	//click on compare items button
	_click($COMPARE_ITEMS_BUTTON);
	var $ProductInCompare=ProductCountInComparePage();
	for(var $i=0;$i<$ProductInCompare;$i++)
		{	
			var $ProductName=_getText(_link("name-link["+$i+"]"));
			//click on add to wish list link
			_click(_link("Add to Wishlist["+$i+"]"));
			//verification
			_assertEqual($ProductName, _getText($WISHLIST_PRODUCTNAME));
			//remove product from wish list
			_click($WISHLIST_REMOVE_BUTTON);
			//navigate to subcat page	
			navigateCatPage($Compare[0][0],$Compare[2][0]);
			//click on compare items button
			_click($COMPARE_ITEMS_BUTTON);
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("287339","Verify the application behavior on click of add to gift registry link for a product in Compare Products page as a registered user.");
$t.start();
try
{

	//navigate to subcat page	
	navigateCatPage($Compare[0][0],$Compare[2][0]);
	//click on compare items button
	_click($COMPARE_ITEMS_BUTTON);
	for(var $i=0;$i<$ProductInCompare;$i++)
		{	
			var $pName=_getText(_link("name-link["+$i+"]"));
			//click on add to gift registry link
			_click(_link("Add to gift registry["+$i+"]"));
			//verify the navigation
			_assertVisible($CREATEGIFTREGISTRY_HEADING);
			//navigate to subcat page	
			navigateCatPage($Compare[0][0],$Compare[2][0]);
			//click on compare items button
			_click($COMPARE_ITEMS_BUTTON);
		}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


cleanup();
