_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("PDPage.xls");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


var $t = _testcase("124841","Verify the UI of Product detail page for bundled products in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to bundle product page
	var $PName=navigateToBundlePDP($PDP_ProductBundle[0][1], $PDP_ProductBundle[1][1], $PDP_ProductBundle[2][1]);
	//verify the UI of bundle product
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertVisible(_span($PName, _in($PAGE_BREADCRUMB)));
	//image
	_assertVisible($PDP_PRODUCT_IMAGE_SECTION);
	_assertVisible($PDP_HERO_IMAGE);
	//thumb nail
	_assertVisible($PDP_PRODUCT_THUMBNAILS);
	_assertVisible($PDP_PRODUCT_THUMBNAILS_SELECTED);
	//Product name
	_assertVisible($PDP_PRODUCTNAME);
	//SKU
	_assertVisible($PDP_PRODUCT_NUMBER);
	//next & previous
	_assertVisible($PDP_PRIMARY_NAV_CONTAINER);
	//fetch the total count present in the set
	var $totalProductsInSet=_collect("_div","/product-set-item product-bundle-item/",_in($PDP_SET_LIST));
	for(var $i=0;$i<$totalProductsInSet.length;$i++)
		{
			//image
			_assertVisible($PDP_SET_IMAGE, _in($totalProductsInSet[$i]));
			//name
			_assertVisible($PDP_SET_PRODUCT_NAME, _in($totalProductsInSet[$i]));
			//number
			_assertVisible($PDP_PRODUCT_NUMBER, _in($totalProductsInSet[$i]));
			//Price
			_assertVisible($PDP_PRICE_SECTION, _in($totalProductsInSet[$i]));
			_assertVisible($PDP_SALES_PRICE, _in($totalProductsInSet[$i]));	
			//Availability
			_assertVisible($PDP_AVAILABILITY_LABEL, _in($totalProductsInSet[$i]));
			_assertVisible($PDP_SELECT_AVAILABILITY_TEXT, _in($totalProductsInSet[$i]));
			//quantity
			_assertVisible($PDP_BUNDLE_QUANTITY, _in($totalProductsInSet[$i]));
			_assertVisible($PDP_BUNDLE_QUANTITY_VALUE, _in($totalProductsInSet[$i]));
			//extended warranty
			if(_isVisible(PDP_BUNDLE_EXTENDED_WARRANTY))
				{
					_assertVisible($PDP_BUNDLE_OPTION_DROPDOWN);
				}
		}
	//buy all
	_assertVisible($PDP_BUYALL_LABEL);
	//bundle product price
	_assertVisible($PDP_BUNDLE_PRODUCT_PRICE);
	//add to cart
	_assertVisible($PDP_ADDTOCART_BUTTON);
	//Add to wish list
	_assertVisible($PDP_ADDTO_WISHLIST_LINK);
	//add to gift registry
	_assertVisible($PDP_ADDTO_GIFTREGISTRY_LINK);
	//social share
	_assertVisible($PDP_GIGYA_SHARE);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124846","Verify the functionality of breadcrumbs on product detail page for bundled products in the application both as an anonymous and a registered user");
$t.start();
try
{
	var $PName=navigateToBundlePDP($PDP_ProductBundle[0][1], $PDP_ProductBundle[1][1], $PDP_ProductBundle[2][1]);
	var $ExpectedBreadCrumb=$PDP_ProductBundle[0][0]+" "+$PDP_ProductBundle[1][0]+" "+$PDP_ProductBundle[2][0]+" "+$PName;
	//verify the breadcrumb
	_assertEqual($ExpectedBreadCrumb,_getText($PDP_PAGE_BREADCRUMB));
	
	//click on root category
	var $BREADCRUMBLINK=CatLink($PDP_ProductBundle[0][0]);
	_click($BREADCRUMBLINK);
	//Bread crumb should not visible in Root category landing page
	_assertNotVisible($PAGE_BREADCRUMB);
	
	var $PName=navigateToBundlePDP($PDP_ProductBundle[0][1], $PDP_ProductBundle[1][1], $PDP_ProductBundle[2][1]);
	//navigate to category page
	var $BREADCRUMBLINK=CatLink($PDP_ProductBundle[1][0]);
	_click($BREADCRUMBLINK);
	var $ExpectedBreadCrumb=$PDP_ProductBundle[0][0]+" "+$PDP_ProductBundle[1][0];
	_assertEqual($ExpectedBreadCrumb, _getText($PAGE_BREADCRUMB));	
	
	var $PName=navigateToBundlePDP($PDP_ProductBundle[0][1], $PDP_ProductBundle[1][1], $PDP_ProductBundle[2][1]);
	//navigate to sub category page
	var $BREADCRUMBLINK=CatLink($PDP_ProductBundle[2][0]);
	_click($BREADCRUMBLINK);
	var $ExpectedBreadCrumb=$PDP_ProductBundle[0][0]+" "+$PDP_ProductBundle[1][0]+" "+$PDP_ProductBundle[2][0];
	_assertEqual($ExpectedBreadCrumb, _getText($PAGE_BREADCRUMB));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124848/287821","Verify the links previous and next on product detail page For bundled products in the application both as an anonymous and a registered user");
$t.start();
try
{	
	navigateSubCatPage($PDP_ProductBundle[0][1], $PDP_ProductBundle[1][1], $PDP_ProductBundle[2][1]);
	//fetching product names
	var $ProdNames = _collectAttributes("_link","/name-link/", "sahiText", _in($PLP_PRODUCT_LIST));
	//var $ProductName=_getText($PLP_PRODUCTNAME_LINK);
	//navigate to PD page on click of name link
	_click($PLP_PRODUCTNAME_LINK);
	for(var $i=0; $i<$numberOfProducts-1; $i++)
		  {
		    var $ProductName = _getText($PDP_PRODUCTNAME);
		    _assertEqual($prodNames[$i],$ProductName);
		    _click($PDP_NEXT_LINK);
		    //verify the presence of previous link
		    if($i!=$numberOfProducts-2)
		    	{
		    		_assertVisible($PDP_PREVIOUS_LINK);
		    	}
		  }
	  for(var $i=$numberOfProducts-2; $i>0; $i--)
		  {
			  _click($PDP_PREVIOUS_LINK);
			  //verify the presence of next link
			   if($i!=$numberOfProducts-2)
		    	{
			    	_assertVisible($PDP_NEXT_LINK);
		    	}
			   var $ProductName = _getText($PDP_PRODUCTNAME);
		       _assertEqual($prodNames[$i],$ProductName);
		  }	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124851","Verify the functionality of product names  on  Product detail page for all products in the bundle in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to bundle product page
	search($PDP_ProductBundle[0][0]);	
	var $ProductNamesInBundle=_collect("_link","/item-name/",_in($PDP_SET_LIST));	
	for(var $i=0;$i<$ProductNamesInBundle.length;$i++)
		{	
			//fetch the product name
			var $pName=_getText($ProductNamesInBundle[$i]);
			//click on name link
			_click($ProductNamesInBundle[$i]);
			//verify the navigation
			_assertVisible($PDP_PRODUCTNAME);
			_assertEqual($pName, _getText($PDP_PRODUCTNAME));
			//navigate to bundle product page
			search($PDP_ProductBundle[0][0]);
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("287824","Verify the application behavior on click of add to wishlist link in product detail page of the application for a product bundle as a guest user");
$t.start();
try
{
	//add to wish list
	_click($PDP_ADDTO_WISHLIST_LINK);
	//verify the navigation
	_assertVisible($LOGIN_HEADING);
	_assertContainsText($PDP_Standard[0][1], $PAGE_BREADCRUMB);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("287827","Verify the application behavior on click of add to gift registry link in product detail page of the application for a product bundle as a guest user");
$t.start();
try
{
	//navigate to bundle product page
	search($PDP_ProductBundle[0][0]);
	//fetch product name 
	var $PName=_getText(_heading1("product-name"));
	//add to gift registry
	_click($PDP_ADDTO_GIFTREGISTRY_LINK);
	//verify the navigation
	_assertContainsText($PDP_Standard[3][1], $PAGE_BREADCRUMB);
	_assertVisible($GIFTREGISTRY_LOGIN_HEADING);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();





ClearCartItems();
var $t = _testcase("124856","Verify the functionality of 'Add to cart' button on product detail  Page for all products in the set in the application both as an anonymous and a registered user");
$t.start();
try
{	
	//navigate to bundle product page
	search($PDP_ProductBundle[0][0]);
	//fetch product name 
	var $PName=_getText($PDP_PRODUCTNAME);
	var $productsInBundle=new Array();
	var $totalProducts=_count("_link","/item-name/",_in($PDP_SET_LIST));
	var $ProductNamesInBundle=_collect("_link","/item-name/",_in($PDP_SET_LIST));
	for(var $i=0;$i<$totalProducts;$i++)
		{	
			$productsInBundle[$i]=_getText($ProductNamesInBundle[$i]);
		}
	//click on add to cart
	_click($PDP_ADDTOCART_BUTTON);
	//verify the cart quantity
	var $actQuantity=parseInt(_getText($MINICART_QUANTITY));
	_assertEqual("1",$actQuantity);
	//verify weather same products are added or not
	_click($MINICART_VIEWCART_LINK);
	_assertEqual($PName,_getText($CART_NAME_LINK));
	var $actBundleproducts=_collect("_div","/name/",_in(_table("cart-table")));
	var $j=0;
	for(var $i=0;$i<$actBundleproducts;$i++)
		{
			_assertEqual($productsInBundle[$i],_getText($actBundleproducts[$j]));
			$j++;
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//Login()
_click($QUICKVIEW_LOGIN_LINK);
login();
var $t = _testcase("124858","Verify the link 'Add to wishlist' on product detail page for bundled products in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to bundle product page
	search($PDP_ProductBundle[0][0]);
	//fetch product name 
	var $PName=_getText(_heading1("product-name"));
	//add to wish list
	_click($PDP_ADDTO_WISHLIST_LINK);
	//verification
	_assertEqual($PName, _getText($WISHLIST_PRODUCTNAME));
	//remove product from wish list
	_click($WISHLIST_REMOVE_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124859","Verify the link 'Add to gift registry' on product detail page for bundled products in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to bundle product page
	search($PDP_ProductBundle[0][0]);	
	//add to gift registry
	_click($PDP_ADDTO_GIFTREGISTRY_LINK);
	//verify the navigation
	_assertVisible($CREATEGIFTREGISTRY_HEADING);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();