_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("Homepage.xls");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();

var $t=_testcase("353079/353076/353077/353092","Verify the functionality by clicking on buy now button in  Gift Card  page. both as guest and registered user/Verify the UI of  Gift Card  page both as guest and registered user" +
		"Verify the functionality by clicking on content slot in  Gift Card  page. both as guest and registered user");
$t.start();
try
{
	
//click on  gift card
_click(_link($giftCard[0][0], _in($FOOTER_CONTAINER)));
//UI of gift card page header section 
_assertVisible(_div("top-banner"));
//gift card content slots
_assertVisible($GIFTCARD_CONTENTSLOT1);
_assertVisible($GIFTCARD_CONTENTSLOT2);
//BUY NOW BUTTON
_assertVisible($GIFTCARD_BUYNOW_BUTTON);
//check balance button
_assertVisible($GIFTCARD_CHECKBALANCE_BUTTON);
//click on buy now button
_click($GIFTCARD_BUYNOW_BUTTON);
//should navigate to PDP
_assertVisible($PDP_PRODUCTNAME);
_assertEqual($giftCard[0][1], _getText($PDP_PRODUCTNAME));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("353085","Verify the functionality of Select Design in  Gift Card page both as guest and registered user");
$t.start();
try
{

//there should be only one card design that should appear as color swatch
var $SelectDesignCount=_count("_link","swatchanchor",_in(_list("swatches color")));
_assertEqual($giftCard[0][3],$SelectDesignCount);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("353081","Verify the UI of  Gift Card PDP both as guest and registered user");
$t.start();
try
{

//UI of gift card
_assertVisible($PDP_PRODUCT_NUMBER);
_assertVisible($PAGE_BREADCRUMB);
//bread crumb element
_assertVisible($PAGE_BREADCRUMB_LINK);
_assertEqual($giftCard[0][2], _getText($PAGE_BREADCRUMB_LINK));
//Bread crumb last element
_assertVisible($PAGE_BREADCRUMB_SPAN_TEXT);
_assertEqual($giftCard[1][2], _getText($PAGE_BREADCRUMB_SPAN_TEXT));
//swatch should select by default
_assertVisible($PDP_PRODUCT_THUMBNAILS_SELECTED);
//alternative images
_assertVisible($PDP_PRODUCT_THUMBNAILS);
//click on bread crumb link
var $breadCrumbGift=_getText($PAGE_BREADCRUMB_LINK, _in($PAGE_BREADCRUMB));
_assertEqual($giftCard[0][2],$breadCrumbGift);
//click on gifts bread crumb link
_click(_link($breadCrumbGift));
//search result page
_assertNotVisible($PAGE_BREADCRUMB_SPAN_TEXT);
_assertVisible($SEARCHRESULT_CONTENT_PAGE);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("353080/353090/353091/353092","Verify the functionality by clicking on Check Balance button in  Gift Card  page. both as guest and registered user" +
		"Verify the functionality on click on Close button in Check balance modal window both as guest and registered user/Verify the functionality of  Gift Card  page. data fields in Check balance modal window both as guest and registered user");
$t.start();
try
{

//click on  gift card
_click(_link($giftCard[0][0], _in($FOOTER_CONTAINER)));
//click on check balance button
_click($GIFTCARD_CHECKBALANCE_BUTTON);
_assertVisible($PDP_SIZECHART_URL);
//dialogue should open
_assertVisible(_div("dialog-container"));
_assertVisible(_div("Check Balance of Gift Card"));
_assertEqual("Check Balance of Gift Card", _getText(_div("giftbal-heading")));
_assertVisible(_div("Check Balance of Gift Card Please enter your gift card number and PIN below to see your current balance. � � Get Balance"));
//gift car no textbox
_assertVisible(_textbox("dwfrm_gift_cardno"));
//gift car pin textbox
_assertVisible(_textbox("dwfrm_gift_pin"));
//check balance button
_assertVisible(_submit("dwfrm_gift_submit"));
_assert(false,"Captcha is not displaying");
//close button
_assertVisible(_button("Close"));

//enter gift card number and pin
_setValue(_textbox("dwfrm_gift_cardno"),$giftCard[0][4]);
_assertEqual($giftCard[0][4],_textbox("dwfrm_gift_cardno"));
_setValue(_textbox("dwfrm_gift_pin"),$giftCard[1][4]);
_assertEqual($giftCard[1][4],_textbox("dwfrm_gift_pin"));

//click on close button
_click(_button("Close"));
//pop up should not display
_assertNotVisible(_div("dialog-container"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("353093","Verify the functionality of Get balance button in Check balance modal window both as guest and registered user");
$t.start();
try
{

//gift card number
_setValue(_textbox("dwfrm_gift_cardno"),$giftCard[0][4]);
//pin number
_setValue(_textbox("dwfrm_gift_pin"),$giftCard[1][4]);
//click on check balance button
_click(_submit("dwfrm_gift_submit"));
_assert(false,"Should verify the message");
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("353084","Verify the functionality of Select Denomination in  Gift Card  pages page both as guest and registered user");
$t.start();
try
{

//click on  gift card
_click(_link($giftCard[0][0], _in($FOOTER_CONTAINER)));
//click on buy now button
_click($GIFTCARD_BUYNOW_BUTTON);
//verify the select color functionality 	
_assertVisible($GIFTCARD_SELECTAMOUNT_LABEL);
var $priceSwatches=_collect("_link","/swatchanchor/",_in($GIFTCARD_PRICEVALUE_LIST));
var $TotalPriceSwatches=$priceSwatches.length;
var $k=1;
//only one swatch anchor is there
if($TotalPriceSwatches==1)
{	
		if(_isVisible($GIFTCARD_PRICEVALUE_LISTITEM_SELECTED))
		  {
			  var $title=_getAttribute($priceSwatches[0],"title").split(" ")[2];
			  //verify whether selected value is displaying or not
			  _assertVisible($GIFTCARD_PRICEVALUE_LISTITEM_SELECTED, _in($GIFTCARD_PRICEVALUE_LIST));
			  _assertEqual($title, _getText($GIFTCARD_PRICEVALUE_LISTITEM_SELECTED, _in($GIFTCARD_PRICEVALUE_LIST)));
		  }
	else
		  {
			  //click on swatchanchor
			  _click($priceSwatches[0]);
			  var $title=_getAttribute($priceSwatches[0],"title");
			  //verify whether selected value is displaying or not
			  _assertVisible($GIFTCARD_PRICEVALUE_LISTITEM_SELECTED, _in($GIFTCARD_PRICEVALUE_LIST));
			  _assertEqual($title, _getText($GIFTCARD_PRICEVALUE_LISTITEM_SELECTED, _in($GIFTCARD_PRICEVALUE_LIST)));
		  }
}
//When there is more than one swatch anchor
else
	{
		 for(var $i=0;$i<$TotalPriceSwatches;$i++)
			{
				if($i==0 || $i==1)
				{
					var $Exp=_getText($GIFTCARD_PRICEVALUE_SWATCH_SELECTABLE, _in($GIFTCARD_PRICEVALUE_LIST));
					_mouseOver($priceSwatches[$i]);
					var $ExpText=_getAttribute($priceSwatches[$i], "title");
					_assertEqual($Exp,$ExpText);
					_click($priceSwatches[$i]);
					var $Act=_getText($GIFTCARD_PRICEVALUE_LISTITEM_SELECTED, _in($GIFTCARD_PRICEVALUE_LIST));
					_assertEqual($Exp,$Act);
				}
			else
				{
					var $Exp=_getText(_listItem("selectable["+$k+"]", _in($GIFTCARD_PRICEVALUE_LIST)));
					_mouseOver($priceSwatches[$i]);
					var $ExpText=_getAttribute($priceSwatches[$i], "title");
					_assertEqual($Exp,$ExpText);
					_click($priceSwatches[$i]);
					var $Act=_getText($GIFTCARD_PRICEVALUE_LISTITEM_SELECTED, _in($GIFTCARD_PRICEVALUE_LIST));
					_assertEqual($Exp,$Act);
					$k++;
				}
			  }
		_assert(true, "Selected color is appropriate");
}	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("353086","Verify the functionality on click on 'Add to wish list' link in in  Gift Card  pages page both as guest and registered user");
$t.start();
try
{
	
//click on  gift card
_click(_link($giftCard[0][0], _in($FOOTER_CONTAINER)));
//click on buy now button
_click($GIFTCARD_BUYNOW_BUTTON);
//selecting swatches
selectGiftCardSwatch();
//add to wish list
_click($PDP_ADDTO_WISHLIST_LINK);
//verify the navigation
_assertVisible($LOGIN_HEADING);
_assertContainsText($PDP_Standard[0][1], $PAGE_BREADCRUMB);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//click on login link
_click($HEADER_LOGIN_LINK);
login();

var $t=_testcase("352995","Verify the application behavior on click of add to wishlist link in product detail page of the application as a registered user");
$t.start();
try
{
	
//click on  gift card
_click(_link($giftCard[0][0], _in($FOOTER_CONTAINER)));
//click on buy now button
_click($GIFTCARD_BUYNOW_BUTTON);
var $PName=_getText($PDP_PRODUCTNAME);
//selecting swatches
selectGiftCardSwatch();
//add to wish list
_click($PDP_ADDTO_WISHLIST_LINK);
//verification
_assertEqual($PName, _getText($WISHLIST_PRODUCTNAME));
//remove product from wish list
_click($WISHLIST_REMOVE_BUTTON);
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("353087","Verify the functionality of Product Tabs in  Gift Card  pages page both as guest and registered user");
$t.start();
try
{

//navigate to PDP
navigateCatPage($PDP_Standard[0][0],$PDP_Standard[2][0]);
var $PName=_getText($PLP_PRODUCTNAME_LINK);
//click on name link
_click($PLP_PRODUCTNAME_LINK);

//click and check all the tabs
var $tabNames=_collectAttributes("_label","/tab-label/","sahiText",_in($PDP_TAB_SECTION));
//four tabs should present
_assertEqual($PDP_Standard[4][2],$tabNames.length);

for(var $i=0;$i<$tabNames.length;$i++)
{

	//check the same is availabe or not
	_assertEqual($PDP_Standard[$i][2],$tabNames[$i]);
	//click on tabs
	_click(_label($tabNames[$i], _in($PDP_TAB_SECTION1)));
	//content should display
	_assertVisible(_div("tab-content["+$i+"]"));
	
}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase(" "," ");
$t.start();
try
{

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t=_testcase(" "," ");
$t.start();
try
{

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/