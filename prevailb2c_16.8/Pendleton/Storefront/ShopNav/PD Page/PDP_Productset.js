_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("Homepage.xls");


SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


//#################### ProductSet Scripts ########################


var $t = _testcase("353053/353022/353025/353023/353047","Verify the UI of a for product set's detail page in the application both as an anonymous and a registered user/Verify the UI of a for product set's detail page in the application both as an anonymous and a registered user" +
		"verify the behavior of the Product default Image for all the configured products of  a products set in its detail page./Verify the UI of promotions section in product details page of the application for a product set when an individual product qualifies for promotion");
$t.start();
try
{
	
navigateCatPage($PDP_Productset[0][0],$PDP_Productset[2][0]);
var $PName=_getText($PLP_PRODUCTNAME_LINK);
//click on name link
_click($PLP_PRODUCTNAME_LINK);
//verify the UI
//bread crumb
_assertVisible($PAGE_BREADCRUMB);
_assertVisible(_span($PName, _in($PAGE_BREADCRUMB)));
//image
_assertVisible($PDP_PRODUCT_IMAGE_SECTION);
_assertVisible($PDP_HERO_IMAGE);
//thumb nail
_assertVisible($PDP_PRODUCT_THUMBNAILS);
_assertVisible($PDP_PRODUCT_THUMBNAILS_SELECTED);
//Product name
_assertVisible($PDP_PRODUCTNAME);
//SKU
_assertVisible($PDP_PRODUCT_NUMBER);
//next & previous
_assertVisible($PDP_PRIMARY_NAV_CONTAINER);
//fetch the total count present in the set
var $totalProductsInSet=_collect("_div","/product-set-item product-bundle-item/",_in($PDP_SET_LIST));

for(var $i=0;$i<$totalProductsInSet.length;$i++)
	{
	
	//image
	_assertVisible($PDP_SET_IMAGE, _in($totalProductsInSet[$i]));
	//name
	_assertVisible($PDP_SET_PRODUCT_NAME, _in($totalProductsInSet[$i]));
	//number
	_assertVisible($PDP_PRODUCT_NUMBER, _in($totalProductsInSet[$i]));
	//Price
	_assertVisible($PDP_PRICE_SECTION, _in($totalProductsInSet[$i]));
	_assertVisible($PDP_SALES_PRICE, _in($totalProductsInSet[$i]));
	//swatches
	if(_isVisible($PDP_COLOR_LABEL, _in($totalProductsInSet[$i])))
	{
		_assertVisible($PDP_COLOR_LIST, _in($totalProductsInSet[$i]));
	}
	if(_isVisible($PDP_SIZE_LABEL, _in($totalProductsInSet[$i])))
	{
		_assertVisible($PDP_SIZE_LIST, _in($totalProductsInSet[$i]));
	}
	if(_isVisible($PDP_WIDTH_LABEL, _in($totalProductsInSet[$i])))
	{
		_assertVisible($PDP_WIDTH_LIST, _in($totalProductsInSet[$i]));
	}
	//Availability
	_assertVisible($PDP_AVAILABILITY_LABEL, _in($totalProductsInSet[$i]));
	_assertVisible($PDP_SELECT_AVAILABILITY_TEXT, _in($totalProductsInSet[$i]));
	//quantity
	_assertVisible($PDP_QUANTITY_LABEL, _in($totalProductsInSet[$i]));
	_assertVisible($PDP_QUANTITY_TEXTBOX, _in($totalProductsInSet[$i]));
	//add to cart without selecting swatches
	_assertVisible($PDP_ADDTOCART_DISABLED_BUTTON, _in($totalProductsInSet[$i]));
	//alternate images
	if(_isVisible($PDP_PRODUCT_THUMBNAILS, _in($totalProductsInSet[$i])))
	{
		_assertVisible($PDP_PRODUCT_THUMBNAILS_SELECTED, _in($totalProductsInSet[$i]));
		_assertVisible($PDP_PRODUCT_THUMBNAILS_SECTION, _in($totalProductsInSet[$i]));
	}
	else
		{
		_assert("alternate images are not present");
		}
	//add all to cart
	_assertVisible($PDP_PRODUCTSET_ADDALLTOCART_BUTTON);
	
}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("353034","Verify the functionality of size chart link on product set's product detail page of the application");
$t.start();
try
{
	
	//navigate to PDP
	navigateCatPage($PDP_Productset[0][0],$PDP_Productset[2][0]);
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	//verify the presence of size chart
	_assertVisible($PDP_SIZECHART_LINK);		
	var $totalProductsInSet=_collect("_div","/product-set-item product-bundle-item/",_in($PDP_SET_LIST));
	for(var $i=0;$i<$totalProductsInSet.length;$i++)
	  {	
		  //verify the presence of size chart
		  if(_isVisible($PDP_SIZECHART_CONTENT,_in(_$totalProductsInSet[$i])))
			  {
				  _click($PDP_SIZECHART_LINK,_in(_$totalProductsInSet[$i]));
					_wait(2000, _isVisible($PDP_SIZECHART_URL));
					if (_isVisible($PDP_SIZECHART_CONTENT,_in(_$totalProductsInSet[$i])))       
					     {
						_assert(true, "Size chart dialogue is opened successfully");
					     }
					   else  
						 {
						   _assert(false, "Size chart window is not opened");
						 }
					_assertVisible($PDP_SIZECHART_CLOSE_BUTTON);
					_click($PDP_SIZECHART_CLOSE_BUTTON);
			  }
		  else
			  {
			  	_log("size chart is not present");
			  }
	   }
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("353036/353035","Verify the behavior  of  color variant on product set's product set's detail page of the application/verify the label of color or heading of color on product set's product detail page  in the application as a guest and a authenticated user.");
$t.start();
try
{
	//navigate to PDP
	navigateSubCatPage($PDP_Productset[0][0],$PDP_Productset[1][0],$PDP_Productset[2][0]);
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	var $totalProductsInSet=_collect("_div","/product-set-item product-bundle-item/",_in($PDP_SET_LIST));
	var $k=1;
	for(var $i=0;$i<$totalProductsInSet.length;$i++)
		{
			if(_isVisible(PDP_COLOR_LABEL,_in($totalProductsInSet[$i])))
			  {	
				var $Colors=_count("_link","/swatchanchor/",_in($PDP_COLOR_LIST,_in($totalProductsInSet[$i])));
				var $TotalColors=$Colors.length;
				var $k=1;
				//only one swatchanchor is there
				if($TotalColors==1)
					{
						if(_isVisible($PDP_SIZE_LISTITEM_SELECTED,_in($totalProductsInSet[$i])))
							  {
								  var $title=_getAttribute($PDP_SWATCHANCHOR,_in($PDP_COLOR_LIST,_in($totalProductsInSet[$i])),"title").split(" ")[2];
								  //verify whether selected value is displaying or not
								  _assertVisible($PDP_SELECTED_SWATCH,_in($PDP_COLOR_LIST,_in($totalProductsInSet[$i])));
								  _assertEqual($title, _getText($PDP_SELECTED_SWATCH,_in($PDP_COLOR_LIST,_in($totalProductsInSet[$i]))));
							  }
						else
							  {
								  //click on swatch anchor
								  _click($PDP_SWATCHANCHOR,_in($PDP_COLOR_LIST,_in($totalProductsInSet[$i])));
								  var $title=_getAttribute($PDP_SWATCHANCHOR,_in($PDP_COLOR_LIST,_in($totalProductsInSet[$i])),"title").split(" ")[2];
								//verify whether selected value is displaying or not
								  _assertVisible($PDP_SELECTED_SWATCH,_in($PDP_COLOR_LIST,_in($totalProductsInSet[$i])));
								  _assertEqual($title, _getText($PDP_SELECTED_SWATCH,_in($PDP_COLOR_LIST,_in($totalProductsInSet[$i]))));
							  }
					}
				//more than one swatchanchor is there
				else
					  {
						for (var $j=0; $j<$TotalColors; $j++)
						    {
								  if($j==0 || $j==1)
									{
										var $Exp=_getText($PDP_SELECTABLE_SWATCH, _in($PDP_COLOR_LIST,_in($totalProductsInSet[$i])));
										_mouseOver($Colors[$j]);
										var $ExpText=_getAttribute($Colors[$j], "title").split(" ")[2];
										_assertEqual($Exp,$ExpText);
										_click($Colors[$j]);
										var $Act=_getText($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST,_in($totalProductsInSet[$i])));
										_assertEqual($Exp,$Act);
									}
								else
									{
										var $Exp=_getText(_listItem("selectable["+$k+"]", _in($PDP_COLOR_LIST,_in($totalProductsInSet[$i]))));
										_mouseOver($Colors[$j]);
										var $ExpText=_getAttribute($Colors[$j], "title").split(" ")[2];
										_assertEqual($Exp,$ExpText);
										_click($Colors[$j]);
										var $Act=_getText($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST,_in($totalProductsInSet[$i])));
										_assertEqual($Exp,$Act);
										$k++;
									}	
								 _assert(true, "Selected color is appropriate");	
							}
					  } 
			   }
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("353033","Verify the application behavior on selecting a size variant on product set's product detail page of the application");
$t.start();
try
{
	//navigate to PDP
	navigateSubCatPage($PDP_Productset[0][0],$PDP_Productset[1][0],$PDP_Productset[2][0]);
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	var $totalProductsInSet=_collect("_div","/product-set-item product-bundle-item/",_in($PDP_SET_LIST));
	
	for(var $i=0;$i<$totalProductsInSet.length;$i++)
	 {
		if(_isVisible($PDP_FIT_LABEL,_in($totalProductsInSet[$i])))
			{
				var $Sizes =_collect("_link","/swatchanchor/",_in($PDP_FIT_LIST,_in($totalProductsInSet[$i])));
				var $TotalSizes=$Sizes.length;			
				//verify the select size functionality
				var $k=1;
				//only one swatchanchor is there
				if($TotalSizes==1)
					{
						if(_isVisible($PDP_FIT_LISTITEM_SELECTED,_in($totalProductsInSet[$i])))
							  {
								  var $title=_getAttribute($PDP_SWATCHANCHOR,_in($PDP_FIT_LIST,_in($totalProductsInSet[$i])),"title").split(" ")[2];
								  //verify whether selected value is displaying or not
								  _assertVisible($PDP_SELECTED_SWATCH,_in($PDP_FIT_LIST,_in($totalProductsInSet[$i])));
								  _assertEqual($title, _getText($PDP_SELECTED_SWATCH,_in($PDP_FIT_LIST,_in($totalProductsInSet[$i]))));
							  }
						else
							  {
								  //click on swatch anchor
								  _click($PDP_SWATCHANCHOR,_in($PDP_FIT_LIST,_in($totalProductsInSet[$i])));
								  var $title=_getAttribute($PDP_SWATCHANCHOR,_in($PDP_FIT_LIST,_in($totalProductsInSet[$i])),"title").split(" ")[2];
								//verify whether selected value is displaying or not
								  _assertVisible($PDP_SELECTED_SWATCH,_in($PDP_FIT_LIST,_in($totalProductsInSet[$i])));
								  _assertEqual($title, _getText($PDP_SELECTED_SWATCH,_in($PDP_FIT_LIST,_in($totalProductsInSet[$i]))));
							  }
					}
				//more than one swatchanchor is there
				else
					  {
						for (var $j=0; $j<$TotalSizes; $j++)
						    {
								  if($j==0 || $j==1)
									{
										var $Exp=_getText($PDP_SELECTABLE_SWATCH, _in($PDP_FIT_LIST,_in($totalProductsInSet[$i])));
										_mouseOver($Sizes[$j]);
										var $ExpText=_getAttribute($Sizes[$j], "title").split(" ")[2];
										_assertEqual($Exp,$ExpText);
										_click($Sizes[$j]);
										var $Act=_getText($PDP_SELECTED_SWATCH, _in($PDP_FIT_LIST,_in($totalProductsInSet[$i])));
										_assertEqual($Exp,$Act);
									}
								else
									{
										var $Exp=_getText(_listItem("selectable["+$k+"]", _in($PDP_FIT_LIST,_in($totalProductsInSet[$i]))));
										_mouseOver($Sizes[$j]);
										var $ExpText=_getAttribute($Sizes[$j], "title").split(" ")[2];
										_assertEqual($Exp,$ExpText);
										_click($Sizes[$j]);
										var $Act=_getText($PDP_SELECTED_SWATCH, _in($PDP_FIT_LIST,_in($totalProductsInSet[$i])));
										_assertEqual($Exp,$Act);
										$k++;
									}	
								 _assert(true, "Selected size is appropriate");	
							}
					  } 
			}
		 }
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("353032","Verify the behavior of the Variation - Fit attribute in the product set's product detail page as a guest and authenticated.");
$t.start();
try
{
	//navigate to PDP
	navigateSubCatPage($PDP_Productset[0][0],$PDP_Productset[1][0],$PDP_Productset[2][0]);
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	var $totalProductsInSet=_collect("_div","/product-set-item product-bundle-item/",_in($PDP_SET_LIST));
	
	for(var $i=0;$i<$totalProductsInSet.length;$i++)
	 {
		if(_isVisible($PDP_SIZE_LABEL,_in($totalProductsInSet[$i])))
			{
				var $Sizes =_collect("_link","/swatchanchor/",_in($PDP_SIZE_LIST,_in($totalProductsInSet[$i])));
				var $TotalSizes=$Sizes.length;			
				//verify the select size functionality
				var $k=1;
				//only one swatchanchor is there
				if($TotalSizes==1)
					{
						if(_isVisible($PDP_SIZE_LISTITEM_SELECTED,_in($totalProductsInSet[$i])))
							  {
								  var $title=_getAttribute($PDP_SWATCHANCHOR,_in($PDP_SIZE_LIST,_in($totalProductsInSet[$i])),"title").split(" ")[2];
								  //verify whether selected value is displaying or not
								  _assertVisible($PDP_SELECTED_SWATCH,_in($PDP_SIZE_LIST,_in($totalProductsInSet[$i])));
								  _assertEqual($title, _getText($PDP_SELECTED_SWATCH,_in($PDP_SIZE_LIST,_in($totalProductsInSet[$i]))));
							  }
						else
							  {
								  //click on swatch anchor
								  _click($PDP_SWATCHANCHOR,_in($PDP_SIZE_LIST,_in($totalProductsInSet[$i])));
								  var $title=_getAttribute($PDP_SWATCHANCHOR,_in($PDP_SIZE_LIST,_in($totalProductsInSet[$i])),"title").split(" ")[2];
								//verify whether selected value is displaying or not
								  _assertVisible($PDP_SELECTED_SWATCH,_in($PDP_SIZE_LIST,_in($totalProductsInSet[$i])));
								  _assertEqual($title, _getText($PDP_SELECTED_SWATCH,_in($PDP_SIZE_LIST,_in($totalProductsInSet[$i]))));
							  }
					}
				//more than one swatchanchor is there
				else
					  {
						for (var $j=0; $j<$TotalSizes; $j++)
						    {
								  if($j==0 || $j==1)
									{
										var $Exp=_getText($PDP_SELECTABLE_SWATCH, _in($PDP_SIZE_LIST,_in($totalProductsInSet[$i])));
										_mouseOver($Sizes[$j]);
										var $ExpText=_getAttribute($Sizes[$j], "title").split(" ")[2];
										_assertEqual($Exp,$ExpText);
										_click($Sizes[$j]);
										var $Act=_getText($PDP_SELECTED_SWATCH, _in($PDP_SIZE_LIST,_in($totalProductsInSet[$i])));
										_assertEqual($Exp,$Act);
									}
								else
									{
										var $Exp=_getText(_listItem("selectable["+$k+"]", _in($PDP_SIZE_LIST,_in($totalProductsInSet[$i]))));
										_mouseOver($Sizes[$j]);
										var $ExpText=_getAttribute($Sizes[$j], "title").split(" ")[2];
										_assertEqual($Exp,$ExpText);
										_click($Sizes[$j]);
										var $Act=_getText($PDP_SELECTED_SWATCH, _in($PDP_SIZE_LIST,_in($totalProductsInSet[$i])));
										_assertEqual($Exp,$Act);
										$k++;
									}	
								 _assert(true, "Selected size is appropriate");	
							}
					  } 
			}
		 }
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("353049/353037","Verify the application behavior on mouse hover over text 'Details' in promotions section of product details page for a standard product" +
		"verify the behavior of the promotion message  for a product that is configured  to be a part of product set.");
$t.start();
try
{

//navigate to PDP
navigateSubCatPage($PDP_Productset[0][0],$PDP_Productset[1][0],$PDP_Productset[2][0]);
//click on name link
_click($PLP_PRODUCTNAME_LINK);
var $totalProductsInSet=_collect("_div","/product-set-item product-bundle-item/",_in($PDP_SET_LIST));

for(var $i=0;$i<$totalProductsInSet.length;$i++)
 {
	//hover on promotion
	_assertVisible($PDP_PROMOTION_CALLOUT_MESSGAE,_in($totalProductsInSet[$i]));
	_assertEqual($PDP_Standard[0][5],_getText($PDP_PROMOTION_CALLOUT_MESSGAE,_in($totalProductsInSet[$i])));

	//verifying the display of tool tip
	if(_getAttribute($PDP_PROMOTION_TOOLTIP,_in($totalProductsInSet[$i]),"aria-describedby")!=null)
	{
		_assert(true,"overlay is visible");
		//Text inside the tooltip
		_assertEqual($PDP_Standard[1][5], _getText($PDP_PROMOTION_TOOLTIP,_in($totalProductsInSet[$i])));
	}
	else
	{
		_assert(false,"Overlay is not visible and promotion is not configured");
	}
 }


}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("353039/353050","Verify the behavior of the 'QTY' form field in the product set's product detail page as a guest or as a authenticated user.");
$t.start();
try
{
	//navigate to PDP
	navigateCatPage($PDP_Productset[0][0],$PDP_Productset[2][0]);
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	var $totalProductsInSet=_collect("_div","/product-set-item product-bundle-item/",_in($PDP_SET_LIST));
	var $QuantityBox=_collect("_textbox","/Quantity/",_in($PDP_SET_LIST));
	//Quantity box validations	
	for(var $i=0;$i<$totalProductsInSet.length;$i++)
	  {	
		var $cartQuantity=0;
		for(var $j=0;$j<$Qty_Validations.length;$j++)
			{
				//checking default value
				_assertEqual("1",_getText($QuantityBox[$i]));
				if($j==0)
					{
						_setValue($QuantityBox[$i],"");
					}
				else
					{
						_setValue($QuantityBox[$i],$Qty_Validations[$j][1]);
					}
				if($j==0)
					{
						_click($PDP_ADDTOCART_BUTTON,_in($totalProductsInSet[$i]));
						var $actQuantity=_getText($MINICART_QUANTITY);
						//verifying the quantity
						_assertEqual("1",$actQuantity);
					}
				//out of stock
				if($j==3)
					{
						//not available message
						_assertVisible($PDP_PRODUCT_NOTAVAILABLE_MESSAGE,_in($totalProductsInSet[$i]));
						_assertEqual($Qty_Validations[3][2], _getText($PDP_PRODUCT_NOTAVAILABLE_MESSAGE,_in($totalProductsInSet[$i])));
						//add to cart should disable
						_assert(_getAttribute($PDP_ADDTOCART_BUTTON,_in($totalProductsInSet[$i]),"disabled"));
					}
				else
					{
						//fetch quantity from mini cart before clicking
						 $cartQuantity=_getText($MINICART_QUANTITY);
						_log($cartQuantity);
						//click on add to cart button
						_click($PDP_ADDTOCART_BUTTON,_in($totalProductsInSet[$i]));
						//default quantity should add to cart
						var $expQuantity=parseFloat($cartQuantity)+parseInt($Qty_Validations[0][2]);
						_log($expQuantity);
						var $actQuantity=_getText($MINICART_QUANTITY);
						//verifying the quantity
						_assertEqual($expQuantity,$actQuantity);
					}
			}
		ClearCartItems();
		//navigate to PDP
		navigateCatPage($PDP_Productset[0][0],$PDP_Productset[2][0]);
		//click on name link
		_click($PLP_PRODUCTNAME_LINK);
	 }
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("353042/353046/353055","Verify the functionality of add to bag button on product set's product detail page of the application/Verify the application behavior on click of product name of an individual product on product detail page of a product set");
$t.start();
try
{
	
navigateSubCatPage($PDP_Productset[0][0],$PDP_Productset[1][0],$PDP_Productset[2][0]);
//storing values
var $PName=_getText($PLP_PRODUCTNAME_LINK);
//click on name link
_click($PLP_PRODUCTNAME_LINK);
//fetch the total count present in the set
var $totalProductsInSet=_collect("_div","/product-set-item product-bundle-item/",_in($PDP_SET_LIST));
for(var $i=0;$i<$totalProductsInSet.length;$i++)
{
	var $pName=_getText($PDP_SET_PRODUCT_NAME, _in($totalProductsInSet[$i]));
	//click on name link
	_click($PDP_SET_PRODUCT_NAME, _in($totalProductsInSet[$i]));
	//verify the navigation to respective PDP
	_assertVisible($PDP_PRODUCTNAME);
	_assertEqual($pName, _getText($PDP_PRODUCTNAME));
	_assertVisible($PDP_AVAILABILITY_DIV);
	_log("################ According to Configuration Below Code will work it it is on stock always ##################");
	_assertVisible($PDP_INSTOCK_MESSAGE);
	//navigate to PDP
	navigateSubCatPage($PDP_Productset[0][0],$PDP_Productset[1][0],$PDP_Productset[2][0]);
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);	
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

ClearCartItems();
var $t = _testcase("353045","Verify the functionality of add all to bag button on product detail page of the application for a product set");
$t.start();
try
{	
	
//navigate to PDP
navigateCatPage($PDP_Productset[0][0],$PDP_Productset[2][0]);
//click on name link
_click($PLP_PRODUCTNAME_LINK);	
var $totalProductsInSet=_collect("_div","/product-set-item product-bundle-item/",_in($PDP_SET_LIST));
var $QuantityBox=_collect("_textbox","/Quantity/",_in($PDP_SET_LIST));
var $totalQty=0;	
//fetch the quantity present in the all boxes in set 
for(var $i=0;$i<$totalProductsInSet.length;$i++)
  {		
	$totalQty=parseFloat($totalQty)+parseFloat(_getText($QuantityBox[$i]));
  }
//fetch quantity from mini cart before clicking
var $expCartQuantity=$totalQty;
//click on add all to cart
_click($PDP_PRODUCTSET_ADDALLTOCART_BUTTON);
var $actCartQuantity=_getText($MINICART_QUANTITY);
//verify the cart quantity
_assertEqual($expCartQuantity,$actCartQuantity);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("353052","Verify the functionality of breadcrumbs on product set's detail page of the application for a product set");
$t.start();
try
{
	navigateSubCatPage($PDP_Productset[0][0],$PDP_Productset[1][0],$PDP_Productset[2][0]);
	//storing values
	var $SubCatBreadcrumb=_getText($PDP_PAGE_BREADCRUMB);
	var $PName=_getText($PLP_PRODUCTNAME_LINK);
	//bread crumb 
	var $ExpectedBreadCrumb=$SubCatBreadcrumb+" "+$PName;
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	//verify the breadcrumb
	_assertEqual($ExpectedBreadCrumb,_getText($PDP_PAGE_BREADCRUMB));
	
	//click on root category
	var $BREADCRUMBLINK=CatLink($PDP_Productset[0][0]);
	_click($BREADCRUMBLINK);
	//Bread crumb should not visible in Root category landing page
	_assertNotVisible($PAGE_BREADCRUMB);
	
	navigateSubCatPage($PDP_Productset[0][0],$PDP_Productset[1][0],$PDP_Productset[2][0]);
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	//navigate to category page
	var $BREADCRUMBLINK=CatLink($PDP_Productset[1][0]);
	_click($BREADCRUMBLINK);
	var $ExpectedBreadCrumb=$PDP_Productset[0][0]+" "+$PDP_Productset[1][0];
	_assertEqual($ExpectedBreadCrumb, _getText($PAGE_BREADCRUMB));	
	
	navigateSubCatPage($PDP_Productset[0][0],$PDP_Productset[1][0],$PDP_Productset[2][0]);
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	//navigate to sub category page
	var $BREADCRUMBLINK=CatLink($PDP_Productset[2][0]);
	_click($BREADCRUMBLINK);
	var $ExpectedBreadCrumb=$PDP_Productset[0][0]+" "+$PDP_Productset[1][0]+" "+$PDP_Productset[2][0];
	_assertEqual($ExpectedBreadCrumb, _getText($PAGE_BREADCRUMB));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//Clean UP
cleanup();

