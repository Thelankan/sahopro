_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("PDPage.xls");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


//################################## StandaredProduct Scripts ##############################

var $t = _testcase("352967/353010/352990/352969","Verify the UI of a  Product detail page for Standard product in the application both as an anonymous and a registered user" +
		"Verify the  behavior of the availability status  for Instock products in the Product detail page as a guest and a registered user.");
$t.start();
try
{
	
//navigateCatPage($PDP_Standard[0][0],$PDP_Standard[2][0]);
search($PDP_Standard[0][4]);
//Select name in PLP
if (_isVisible($PLP_PRODUCT_LIST))
{
	_click($PLP_PRODUCTNAME_LINK);
}
//var $PName=_getText($PLP_PRODUCTNAME_LINK);
//click on name link
//_click($PLP_PRODUCTNAME_LINK);
//verify the UI of variation product
//bread crumb
//_assertVisible($PAGE_BREADCRUMB);
//_assertVisible(_span($PName, _in($PAGE_BREADCRUMB)));
//image
_assertVisible($PDP_PRODUCT_IMAGE_SECTION);
_assertVisible($PDP_HERO_IMAGE);
//thumb nail
_assertVisible($PDP_PRODUCT_THUMBNAILS);
_assertVisible($PDP_PRODUCT_THUMBNAILS_SELECTED);
//Product name
_assertVisible($PDP_PRODUCTNAME);
//SKU
_assertVisible($PDP_PRODUCT_NUMBER);
//Price
_assertVisible($PDP_PRICE_SECTION);
_assertVisible($PDP_SALES_PRICE);
//next & previous
_assertVisible($PDP_PRIMARY_NAV_CONTAINER);
//swatches
if(_isVisible($PDP_COLOR_LABEL))
	{
		_assertVisible($PDP_COLOR_LIST);
	}
if(_isVisible($PDP_SIZE_LABEL))
	{
		_assertVisible($PDP_SIZE_LIST);
	}
if(_isVisible($PDP_WIDTH_LABEL))
	{
		_assertVisible($PDP_WIDTH_LIST);
	}
//Availability
_assertVisible($PDP_SELECT_AVAILABILITY_TEXT);

if (_isVisible($PDP_QUANTITY_LABEL))
{
	//quantity
	_assertVisible($PDP_QUANTITY_LABEL);
	_assertVisible($PDP_QUANTITY_TEXTBOX);
}
else
	{
	//yards
	_assertVisible($PDP_YARDS_LABEL);
	}

//add to cart without selecting swatches
//var $disabled=_getAttribute($PDP_ADDTOCART_DISABLED_BUTTON,"disabled");
//_assertEqual(true,$disabled);

//disabled add to cart button
_assertVisible(_button("button-fancy-large add-to-cart-disabled"));	
//Add to wish list should not be visible
_assertNotVisible($PDP_ADDTO_WISHLIST_LINK);
//select swatches
selectSwatch();
//availability
_assertVisible($PDP_INSTOCK_MESSAGE);
//Add to cart
_assertVisible($PDP_ADDTOCART_BUTTON);
//Add to wish list
_assertVisible($PDP_ADDTO_WISHLIST_LINK);
//you might also like
_assertVisible($PDP_YOUMIGHTALSOLIKE_SECTION);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("352985/352984","Verify the behavior  of  color variant on product detail page of the application/verify the label of color or heading of color in the application as a guest and a authenticated user.");
$t.start();
try
{
	navigateCatPage($PDP_Standard[0][0],$PDP_Standard[2][0]);
	var $PName=_getText($PLP_PRODUCTNAME_LINK);
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	//verify the select color functionality 	
	_assertVisible($PDP_COLOR_LIST);
	var $Colors=_collect("_link","/swatchanchor/",_in($PDP_COLOR_LIST));
	var $TotalColors=$Colors.length;
	var $k=1;
	//only one swatch anchor is there
	if($TotalColors==1)
		{	
			if(_isVisible($PDP_COLOR_LISTITEM_SELECTED))
				  {
					  var $title=_getAttribute($Colors[0],"title").split(" ")[2];
					  //verify whether selected value is displaying or not
					  _assertVisible($PDP_COLOR_LISTITEM_SELECTED);
					  _assertEqual($title, _getText($PDP_COLOR_LISTITEM_SELECTED));
				  }
			else
				  {
					  //click on swatchanchor
					  _click($Colors[0]);
					  var $title=_getAttribute($Colors[0],"title");
					  //verify whether selected value is displaying or not
					  _assertVisible($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST));
					  _assertEqual($title, _getText($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST)));
				  }
		}
	//When there is more than one swatch anchor
	else
		{
			 for(var $i=0;$i<$TotalColors;$i++)
				{
					if($i==0 || $i==1)
						{
							var $Exp=_getText($PDP_SELECTABLE_SWATCH, _in($PDP_COLOR_LIST));
							_mouseOver($Colors[$i]);
							var $ExpText=_getAttribute($Colors[$i], "title");
							_assertEqual($Exp,$ExpText);
							 _click($Colors[$i]);
							var $Act=_getText($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST));
							_assertEqual($Exp,$Act);
						}
					else
						{
							var $Exp=_getText(_listItem("selectable["+$k+"]", _in($PDP_COLOR_LIST)));
							_mouseOver($Colors[$i]);
							var $ExpText=_getAttribute($Colors[$i], "title");
							_assertEqual($Exp,$ExpText);
							 _click($Colors[$i]);
							var $Act=_getText($PDP_SELECTED_SWATCH, _in($PDP_COLOR_LIST));
							_assertEqual($Exp,$Act);
							$k++;
						}
					  }
				_assert(true, "Selected color is appropriate");
		}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("353001","Verify the UI of 'You May Also Like' in product details page of the application");
$t.start();
try
{
	
	//navigate to PDP
	navigateCatPage($PDP_Standard[0][0],$PDP_Standard[2][0]);
	var $PName=_getText($PLP_PRODUCTNAME_LINK);
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	//verify the presence of You may also like Section
	if(_isVisible($PDP_PRODUCT_RECOMMENDATIONS_SECTION))
		{
		
			//you might also like product count
			_assertEqual("4",_count("_div","/product-name/",_in(_list("search-result-items tiles-container"))));
		
			var $Recommendation=_collect("_link", "/thumb-link/", _in($PDP_YOUMIGHTALSOLIKE_PRODUCT_SECTION));
			for(var $i=0;$i<$Recommendation.length;$i++)
				{
					_assertVisible($PDP_RECOMMENDATION_IMAGE, _in($Recommendation[$i]));
					_assertVisible($PDP_RECOMMENDATION_PRODUCTNAME, _in($Recommendation[$i]));
					
					if(_isVisible($PDP_YOUMIGHTALSOLIKE_STANDARED_PRICE,_in($Recommendation[$i])))
						{
						_assertVisible($PDP_RECOMMENDATION_PRICE,_in($Recommendation[$i]));
						}
					else
						{
						_assertVisible($PDP_YOUMIGHTALSOLIKE_SALES_PRICE,_in($Recommendation[$i]));
						}
				}
		}
else
	{
	_assert(false,"recomendation section is not present for this product");
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("353002","Verify the behavior  of products of 'You May Also Like' section in the application .");
$t.start();
try
{
	
//navigate to PDP
navigateCatPage($PDP_Standard[0][0],$PDP_Standard[2][0]);
//count of you might also like products
var $CountOfProducts=_count("_span","name-link",_in($PDP_YOUMIGHTALSOLIKE_PRODUCT_SECTION));
for(var $i=0;$i<$CountOfProducts;$i++)
{
	
	_mouseOver(_link("thumb-link"));
	_assertNotVisible(_link("quickview"));
	var $productNameinYoumayalosLink=_getText($PDP_YOUMIGHTALSOLIKE_PRODUCT_NAME);
	_click($PDP_YOUMIGHTALSOLIKE_PRODUCT_NAME);
	//nAVIGATE TO pdp
	_assertVisible($PDP_MAIN);
	var $ProductNameInPdp=_getText($PDP_PRODUCTNAME);
	//Name should match
	_assertEqual($productNameinYoumayalosLink,$ProductNameInPdp);
		
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("353004/353005","Verify the UI of 'People Also Bought section' in product details page of the application");
$t.start();
try
{

//navigate to PDP
navigateCatPage($PDP_Standard[0][0],$PDP_Standard[2][0]);
var $PName=_getText($PLP_PRODUCTNAME_LINK);
//click on name link
_click($PLP_PRODUCTNAME_LINK);
//people also bought section
_assertVisible($PDP_PEOPLEALSOBOUGHT_SECTION);
_assertEqual("4",_count("_div","/product-name/",_in(_list("/search-result-items tiles-container/",_near($PDP_PEOPLEALSOBOUGHT_SECTION)))));
//verify the presence of You may also like Section
if(_isVisible($PDP_PEOPLEALSOBOUGHT_SECTION))
	{ 
		var $PeopleAlsoBought=_collect("_link", "/thumb-link/", _in(_list("/search-result-items tiles-container/",_near($PDP_PEOPLEALSOBOUGHT_SECTION))));
		for(var $i=0;$i<$PeopleAlsoBought.length;$i++)
			{
				_assertVisible($PDP_RECOMMENDATION_IMAGE, _in($PeopleAlsoBought[$i]));
				_assertVisible($PDP_RECOMMENDATION_PRODUCTNAME, _in($PeopleAlsoBought[$i]));
				
				if(_isVisible($PDP_YOUMIGHTALSOLIKE_STANDARED_PRICE,_in($PeopleAlsoBought[$i])))
					{
					_assertVisible($PDP_RECOMMENDATION_PRICE,_in($PeopleAlsoBought[$i]));
					}
				else
					{
					_assertVisible($PDP_YOUMIGHTALSOLIKE_SALES_PRICE,_in($PeopleAlsoBought[$i]));
					}
			}
	}

else
{
_assert(false,"recomendation section iin people also bought section");
}
	
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("352987/352988/353038","Verify the field validations for quantity text field on product detail page of the application");
$t.start();
try
{
	
//Navigate to category page
navigateCatPage($PDP_Standard[0][0],$PDP_Standard[2][0]);
var $PName=_getText($PLP_PRODUCTNAME_LINK);
//click on name link
_click($PLP_PRODUCTNAME_LINK);
//when no variants are selected verify the add to cart button
_assertNotVisible($PDP_ADDTOCART_BUTTON);
//select the variants
selectSwatch();
//now verify the add to cart dispaly
_assertVisible($PDP_ADDTOCART_BUTTON);
//Default Quantity Should be 1 always
_assertEqual($Qty_Validations[0][2], _getValue($PDP_QUANTITY_TEXTBOX));
//quantity box validation
var $cartQuantity=0;
for(var $j=0;$j<$Qty_Validations.length;$j++)
{
	
if($j==0)
{
	_setValue($PDP_QUANTITY_TEXTBOX,"");
}
else
{
	_setValue($PDP_QUANTITY_TEXTBOX,$Qty_Validations[$j][1]);
}
if($j==0)
{
	_click($PDP_ADDTOCART_BUTTON);
	_wait(2000);
	var $actQuantity=_getText($MINICART_QUANTITY);
	//verifying the quantity
	_assertEqual("1",$actQuantity);
}
//out of stock
if($j==3)
{
	//not available message
	_assertVisible($PDP_NOTAVAILABLE_MESSAGE);
	_assertEqual($Qty_Validations[3][2], _getText($PDP_NOTAVAILABLE_MESSAGE));
	//add to cart without selecting swatches
	var $disabled=_getAttribute($PDP_ADDTOCART_DISABLED_BUTTON,"disabled");
	_assertEqual(true,$disabled);
}
else
{						
	var $cartQuantity=_getText($MINICART_QUANTITY);
	_log($cartQuantity);
	//click on add to cart button
	_click($PDP_ADDTOCART_BUTTON);
	//default quantity should add to cart
	var $expQuantity=parseFloat($cartQuantity)+parseInt($Qty_Validations[0][2]);
	var $actQuantity=_getText($MINICART_QUANTITY);
	//verifying the quantity
	_assertEqual($expQuantity,$actQuantity);
}
	
}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("352968","Verify the functionality of breadcrumbs on product detail page of the application for a product set");
$t.start();
try
{	
	navigateSubCatPage($PDP_Standard[0][0],$PDP_Standard[1][0],$PDP_Standard[2][0]);
	//storing values
	var $SubCatBreadcrumb=_getText($PAGE_BREADCRUMB);
	var $PName=_getText($PLP_PRODUCTNAME_LINK);
	//bread crumb 
	var $ExpectedBreadCrumb=$SubCatBreadcrumb+" "+$PName;
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	//verify the breadcrumb
	_assertEqual($ExpectedBreadCrumb,_getText($PAGE_BREADCRUMB));
	
	//click on root category
	var $BREADCRUMBLINK=CatLink($PDP_Standard[0][10]);
	_click($BREADCRUMBLINK);
	//Bread crumb should not visible in Root category landing page
	_assertNotVisible($PAGE_BREADCRUMB);
	
	navigateSubCatPage($PDP_Standard[0][10],$PDP_Standard[1][10],$PDP_Standard[2][10]);
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	//navigate to category page
	var $BREADCRUMBLINK=CatLink($PDP_Standard[1][10]);
	_click($BREADCRUMBLINK);
	var $ExpectedBreadCrumb=$PDP_Standard[0][10]+" "+$PDP_Standard[1][10];
	_assertEqual($ExpectedBreadCrumb, _getText($PAGE_BREADCRUMB));	
	
	navigateSubCatPage($PDP_Standard[0][10],$PDP_Standard[1][10],$PDP_Standard[2][10]);
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	//navigate to sub category page
	var $BREADCRUMBLINK=CatLink($PDP_Standard[2][10]);
	_click($BREADCRUMBLINK);
	var $ExpectedBreadCrumb=$PDP_Standard[0][10]+" "+$PDP_Standard[1][10]+" "+$PDP_Standard[2][10];
	_assertEqual($ExpectedBreadCrumb, _getText($PAGE_BREADCRUMB));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("352980","Verify the application behavior on selecting a size variant on product detail page of the application");
$t.start();
try
{
	
	//navigate to PDP
	navigateCatPage($PDP_Standard[0][0],$PDP_Standard[2][0]);
	var $PName=_getText($PLP_PRODUCTNAME_LINK);
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	//size list
	_assertVisible($PDP_SIZE_LIST);
	var $Sizes =_collect("_link","/swatchanchor/",_in($PDP_SIZE_LIST));
	var $TotalSizes=$Sizes.length;
	var $j=1;
	//only one swatchanchor is there
	if($TotalSizes==1)
		{
			if(_isVisible($PDP_SIZE_LISTITEM_SELECTED))
				  {
					  var $title=_getAttribute($PDP_SWATCHANCHOR,_in($PDP_SIZE_LIST),"title").split(" ")[2];
					  _log("Color Swatch"+$title);
					  //verify whether selected value is displaying or not
					  _assertVisible(_link("/Select Size/",_in($PDP_SIZE_LIST)));
					  _assertEqual($title, _getText(_link("/Select Size/",_in($PDP_SIZE_LIST))));
				  }
			else
				  {
					  //click on swatch anchor
					  _click($PDP_SWATCHANCHOR,_in($PDP_SIZE_LIST));
					  var $title=_getAttribute($PDP_SWATCHANCHOR,_in($PDP_SIZE_LIST),"title");
					  _log("Color Swatch"+$title);
					  //verify whether selected value is displaying or not
					  _assertVisible(_link("/Select Size/",_in($PDP_SIZE_LIST)));
					  _assertEqual($title, _getText(_link("/Select Size/",_in($PDP_SIZE_LIST))));
				  }
		}
	//more than one swatchanchor is there
	else
	  {
			for (var $i=0; $i<$TotalSizes; $i++)
			    {
				
				var $Exp=_getText(_link("swatchanchor",_listItem("selectable["+$j+"]", _in($PDP_SIZE_LIST))));
				_getText(_link("swatchanchor",_in(_listItem("selectable[1]",_in($PDP_SIZE_LIST)))))
				_mouseOver($Sizes[$i]);
				var $ExpText=_getAttribute($Sizes[$i], "title").split(" ")[2];
				_assertEqual($Exp,$ExpText);
				_click($Sizes[$i]);
				var $Act=_getText(_link("swatchanchor",_in(_listItem("/selectable selected/",_in($PDP_SIZE_LIST)))));
				_assertEqual($Exp,$Act);
				
//					 if($i==0 || $i==1)
//						{
//							var $Exp=_getText(_link("swatchanchor",_in(_listItem("selectable",_in($PDP_SIZE_LIST)))));
//							_mouseOver($Sizes[$i]);
//							var $ExpText=_getAttribute($Sizes[$i], "title").split(" ")[2];
//							_assertEqual($Exp,$ExpText);
//							_click($Sizes[$i]);
//							var $Act=_getText(_link("swatchanchor",_in(_listItem("/selectable selected/",_in($PDP_SIZE_LIST)))));
//							_assertEqual($Exp,$Act);
//						}
//					else
//						{
//							var $Exp=_getText(_link("swatchanchor",_listItem("selectable["+$j+"]", _in($PDP_SIZE_LIST))));
//							_getText(_link("swatchanchor",_in(_listItem("selectable[1]",_in($PDP_SIZE_LIST)))))
//							_mouseOver($Sizes[$i]);
//							var $ExpText=_getAttribute($Sizes[$i], "title").split(" ")[2];
//							_assertEqual($Exp,$ExpText);
//							_click($Sizes[$i]);
//							var $Act=_getText(_link("swatchanchor",_in(_listItem("/selectable selected/",_in($PDP_SIZE_LIST)))));
//							_assertEqual($Exp,$Act);
//							$j++;
//						}	
//					 _assert(true, "Selected size is appropriate");	
				}
	  }
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("352978","Verify the behavior of the Variation - Fit attribute in the product detail page as a guest and authenticated.");
$t.start();
try
{
	_assert(false,"configuration required");
	_assertVisible($PDP_FIT_LIST);
	var $fit=_collect("_link","/swatchanchor/",_in($PDP_FIT_LIST));
	var $TotalFitSizes=$fit.length;
	var $j=1;
	//only one swatchanchor is there
	if($TotalFitSizes==1)
		{
			if(_isVisible($PDP_FIT_LISTITEM_SELECTED))
				  {
					  var $title=_getAttribute($PDP_SWATCHANCHOR,_in($PDP_FIT_LIST),"title").split(" ")[2];
					  //verify whether selected value is displaying or not
					  _assertVisible($PDP_SELECTED_SWATCH,_in($PDP_FIT_LIST));
					  _assertEqual($title, _getText($PDP_SELECTED_SWATCH,_in($PDP_FIT_LIST)));
				  }
			else
				  {
					  //click on swatch anchor
					  _click($PDP_SWATCHANCHOR,_in($PDP_FIT_LIST));
					  var $title=_getAttribute($PDP_SWATCHANCHOR,_in($PDP_FIT_LIST),"title");
					  //verify whether selected value is displaying or not
					  _assertVisible($PDP_SELECTED_SWATCH,_in($PDP_FIT_LIST));
					  _assertEqual($title, _getText($PDP_SELECTED_SWATCH,_in($PDP_FIT_LIST)));
				  }
		}
	//more than one swatchanchor is there
	else
	  {
			for (var $i=0; $i<$TotalSizes; $i++)
			    {
					  if($i==0 || $i==1)
						{
							var $Exp=_getText($PDP_SELECTABLE_SWATCH, _in($PDP_FIT_LIST));
							_mouseOver($fit[$i]);
							var $ExpText=_getAttribute($fit[$i], "title").split(" ")[2];
							_assertEqual($Exp,$ExpText);
							_click($fit[$i]);
							var $Act=_getText($PDP_SELECTED_SWATCH, _in($PDP_FIT_LIST));
							_assertEqual($Exp,$Act);
						}
					else
						{
							var $Exp=_getText(_listItem("selectable["+$j+"]", _in($PDP_FIT_LIST)));
							_mouseOver($fit[$i]);
							var $ExpText=_getAttribute($fit[$i], "title").split(" ")[2];
							_assertEqual($Exp,$ExpText);
							_click($fit[$i]);
							var $Act=_getText($PDP_SELECTED_SWATCH, _in($PDP_FIT_LIST));
							_assertEqual($Exp,$Act);
							$j++;
						}	
					 _assert(true, "Selected size is appropriate");	
				}
	  }     	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("352993/353008","Verify the functionality of add to bag button on product detail Page of the application/Verify the state of add to bag button on product detail page of the application");
$t.start();
try
{
	navigateCatPage($PDP_Standard[0][0],$PDP_Standard[2][0]);
	var $PName=_getText($PLP_PRODUCTNAME_LINK);
	//click on name link
	_click($PLP_PRODUCTNAME_LINK);
	//when no variants are selected verify the add to cart button
	_assertNotVisible($PDP_ADDTOCART_BUTTON);
	//select the variants
	selectSwatch();
	//now verify the add to cart dispaly
	_assertVisible($PDP_ADDTOCART_BUTTON);
	//quantity box validation
	var $cartQuantity=0;
	for(var $j=0;$j<$Qty_Validations.length;$j++)
		{
			if($j==0)
				{
					_setValue($PDP_QUANTITY_TEXTBOX,"");
				}
			else
				{
					_setValue($PDP_QUANTITY_TEXTBOX,$Qty_Validations[$j][1]);
				}
			if($j==0)
				{
					_click($PDP_ADDTOCART_BUTTON);
					_wait(2000);
					var $actQuantity=_getText($MINICART_QUANTITY);
					//verifying the quantity
					_assertEqual("1",$actQuantity);
				}
			//out of stock
			if($j==3)
				{
					//not available message
					_assertVisible($PDP_NOTAVAILABLE_MESSAGE);
					_assertEqual($Qty_Validations[3][2], _getText($PDP_NOTAVAILABLE_MESSAGE));
					//add to cart should disable
					_assertNotVisible($PDP_ADDTOCART_DISABLED_BUTTON);
				}
			else
				{						
					var $cartQuantity=_getText($MINICART_QUANTITY);
					_log($cartQuantity);
					//click on add to cart button
					_click($PDP_ADDTOCART_BUTTON);
					//default quantity should add to cart
					var $expQuantity=parseFloat($cartQuantity)+parseInt($Qty_Validations[0][2]);
					var $actQuantity=_getText($MINICART_QUANTITY);
					//verifying the quantity
					_assertEqual($expQuantity,$actQuantity);
				}		
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("352994","Verify the application behavior on click of add to wishlist link in product detail page of the application as a guest user");
$t.start();
try
{
	
//navigate to PDP
navigateCatPage($PDP_Standard[0][0],$PDP_Standard[2][0]);
var $PName=_getText($PLP_PRODUCTNAME_LINK);
//click on name link
_click($PLP_PRODUCTNAME_LINK);
//selecting swatches
selectSwatch();
//add to wish list
_click($PDP_ADDTO_WISHLIST_LINK);
//verify the navigation
_assertVisible($LOGIN_HEADING);
_assertContainsText($PDP_Standard[0][1], $PAGE_BREADCRUMB);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t = _testcase("353000/352996","Verify the  behavior on selection of  the product tabs in product details page of the application./Verify the UI of product tabs in product details page of the application");
$t.start();
try
{
	
//navigate to PDP
navigateCatPage($PDP_Standard[0][0],$PDP_Standard[2][0]);
var $PName=_getText($PLP_PRODUCTNAME_LINK);
//click on name link
_click($PLP_PRODUCTNAME_LINK);

//click and check all the tabs
var $tabNames=_collectAttributes("_label","/tab-label/","sahiText",_in($PDP_TAB_SECTION));
//four tabs should present
_assertEqual($PDP_Standard[4][2],$tabNames.length);

for(var $i=0;$i<$tabNames.length;$i++)
{

	//check the same is availabe or not
	_assertEqual($PDP_Standard[$i][2],$tabNames[$i]);
	//click on tabs
	_click(_label($tabNames[$i], _in($PDP_TAB_SECTION1)));
	//content should display
	_assertVisible(_div("tab-content["+$i+"]"));
	
}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("352975","Verify the behavior of the review stars in the product detail page as a guest and authenticated user.");
$t.start();
try
{
	
//navigate to PDP
navigateCatPage($PDP_Standard[0][0],$PDP_Standard[2][0]);
var $PName=_getText($PLP_PRODUCTNAME_LINK);
//click on name link
_click($PLP_PRODUCTNAME_LINK);
//visibility of satrs link
_assertVisible($PDP_PRODUCT_REVIEW_STARS);
//click on review satr's
var $currentPos=window.document.body.scrollTop;
_click($PDP_PRODUCT_REVIEW_STARS);
var $changedPos=window.document.body.scrollTop;

if ($changedPos < $currentPos) {
	_log("Scroll bar has moved down");
}
//Review heading
_assertVisible($PDP_PRODUCT_REVIEW_HEADING);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

//login to the application
//_click($HEADER_LOGIN_LINK);
//login();

var $t=_testcase("352995","Verify the application behavior on click of add to wishlist link in product detail page of the application as a registered user");
$t.start();
try
{
	
//navigate to PDP
navigateCatPage($PDP_Standard[0][0],$PDP_Standard[2][0]);
var $PName=_getText($PLP_PRODUCTNAME_LINK);
//click on name link
_click($PLP_PRODUCTNAME_LINK);
//selecting swatches
selectSwatch();
//add to wish list
_click($PDP_ADDTO_WISHLIST_LINK);
//verification
_assertEqual($PName, _getText($WISHLIST_PRODUCTNAME));
//remove product from wish list
_log("######### Delete button should present #################");
//_click($WISHLIST_REMOVE_BUTTON);
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("353049","Verify the application behavior on mouse hover over text 'Details' in promotions section of product details page for a standard product");
$t.start();
try
{

//search procuct
search($PDP_Standard[0][11]);
//Select name in PLP
if (_isVisible($PLP_PRODUCT_LIST))
{
	_click($PLP_PRODUCTNAME_LINK);
}

//hover on promotion
_assertVisible($PDP_PROMOTION_CALLOUT_MESSGAE);
_assertEqual($PDP_Standard[0][5],_getText($PDP_PROMOTION_CALLOUT_MESSGAE));
_mouseOver($PDP_PROMOTION_TOOLTIP);
//verifying the display of tool tip
if(_getAttribute($PDP_PROMOTION_TOOLTIP,"aria-describedby")!=null)
{
	_assert(true,"overlay is visible");
	//Text inside the tooltip
	_assertEqual($PDP_Standard[1][5], _getText($PDP_PROMOTION_TOOLTIP));
}
else
{
	_assert(false,"Overlay is not visible");
}

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


//############################################ Scripts Related to Size Chart #######################################

_log("############### configuration required ################");

var $t = _testcase("352982/353019/353020/353021","Verify the functionality of size chart link on product detail page of the application/[responsive]verify the UI of the  '1 page size chart' overlay in the application." +
		"verify the  behavior of expand and collapse button of size 'Tabbed size chart' overlay in the application.");
$t.start();
try
{

//navigate to PDP
//navigateCatPage($PDP_Standard[0][0],$PDP_Standard[2][0]);
search($PDP_Standard[3][4]);
//Select name in PLP
if (_isVisible($PLP_PRODUCT_LIST))
{
	_click($PLP_PRODUCTNAME_LINK);
}
//verify the presence of size chart
_assertVisible($PDP_SIZECHART_LINK);
_click($PDP_SIZECHART_LINK);
_wait(4000);
_wait(2000, _isVisible($PDP_SIZECHART_URL));
if (_isVisible($PDP_SIZECHART_CONTENT))       
 {
   _assert(true, "Size chart dialogue is opened successfully");
 }
   else  
	 {
	   _assert(false, "Size chart window is not opened");
	 }
_assertVisible($PDP_SIZECHART_CLOSE_BUTTON);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("353016/353014","verify the  behavior on click of close button from the  '1 page size chart'  overlay in the application.");
$t.start();
try
{

//click on close button
_click($PDP_SIZECHART_CLOSE_BUTTON);
_assertNotVisible($PDP_SIZECHART_CONTENT);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("353017/353015","verify the  behavior on  changing the tab selection and clicking of close button from the  'Tabbed size chart'  overlay in the application.");
$t.start();
try
{

//click on size chart link
_click($PDP_SIZECHART_LINK);
_wait(4000);
//click on all the tabs in size chart
var $tabs=_collectAttributes("_label","/(.*)/","sahiText",_in(_div("sizechart-tab")));
_log("all tabs"+$tabs);
//select tabs
for (var $i=0;$i<$tabs.length-1;$i++)
{
	
	_log("all tabs For Loop"+$tabs);
	//Selected tab should be active
	_click(_label($tabs[$i]));
	_assertVisible(_div("tab-content", _near(_label($tabs[$i]))));
	//selected tab should be same
	_assertEqual($PDP_Standard[$i][7],_getText($PDP_SIZECHART_LABEL_ACTIVE));
	
}

//click on close button
_click($PDP_SIZECHART_CLOSE_BUTTON);
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


//############################################ Scripts Related to review #######################################

var $t=_testcase("353058","Verify the behavior on click of 'Write a review button' in Reviews tabs in product details page both as guest and registered user");
$t.start();
try
{

//navigate to PDP
navigateCatPage($PDP_Standard[0][0],$PDP_Standard[2][0]);
var $PName=_getText($PLP_PRODUCTNAME_LINK);
//click on name link
_click($PLP_PRODUCTNAME_LINK);
//visibility of satrs link
_assertVisible($PDP_PRODUCT_REVIEW_STARS);
//click on review stars
_click($PDP_PRODUCT_REVIEW_STARS);

if ($PDP_WRITEAREVIEW_BUTTON)
{
	_click($PDP_WRITEAREVIEW_BUTTON);
}
else
	{
	//click on be first to write review link
	_click($PDP_BEFIRST_REVIEW_LINK, _in($PDP_BEFIRST_REVIEW_SECTION));
	}

//overlay should open
_assertVisible($PDP_REVIEW_OVERLAY);
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("353058/353059","Verify the behavior on click of 'Write a review button' in Reviews tabs in product details page both as guest and registered user");
$t.start();
try
{

//product review overlay
_assertVisible(_span("My Review"));
_assertVisible(_fieldset("bv-fieldset bv-fieldset-rating bv-radio-field bv-fieldset-active"));
_assertVisible(_span("Review Title*"));
_assertVisible(_span("Review*"));
_assertVisible(_span("Add Photo"));
_assertVisible(_button("Add Video"));
_assertVisible(_span("Would you recommend this product to a friend?"));
_assertVisible(_label("Yes"));
_assertVisible(_label("No"));
_assertVisible(_span("Location"));
_assertVisible(_span("Email*"));
_assertVisible(_span("What is your gender?"));
_assertVisible(_span("Age Range*"));
_assertVisible(_span("How often do you shop with us?"));
_assertVisible(_span("Would you recommend Pendleton Woolen Mills to a friend?"));
_assertVisible(_span("bv-fieldset-netpromoterscore-wrapper bv-fieldset-radio-wrapper bv-focusable"));
_assertVisible(_button("Post Review"));
_assertVisible(_span("Never"));
_assertVisible(_span("Definitely"));

_assertVisible($PDP_ROVERLAY_TITLE_TEXTBOX);
_assertVisible($PDP_ROVERLAY_REVIEW_TEXTAREA);
_assertVisible($PDP_ROVERLAY_USERLOCATION_TEXTBOX);
_assertVisible($PDP_ROVERLAY_USER_EMAIL);
_assertVisible($PDP_ROVERLAY_GENDER_DROPDOWN);
_assertVisible($PDP_ROVERLAY_AGE_DROPDOWN);
_assertVisible($PDP_ROVERLAY_SHOP_DROPDOWN);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("353060","Verify the functionality of Write your review page in Reviews tabs in product details page both as guest and registered user");
$t.start();
try
{

//entering all the values
ReviewValuesInOverlay($PDP_Standard,8);	
_assertVisible(_button($PDP_Standard[0][6], _in(_div("bv-mbox-lightbox-list"))));
_click(_button($PDP_Standard[0][6], _in(_div("bv-mbox-lightbox-list"))));
//Overlay Should not display
_assertNotVisible($PDP_REVIEW_OVERLAY);	

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("353061/353062/353063","Verify the UI of Review Preview page in Reviews tabs in product details page both as guest and registered user");
$t.start();
try
{

//click on name link
_click($PLP_PRODUCTNAME_LINK);
//visibility of satrs link
_assertVisible($PDP_PRODUCT_REVIEW_STARS);
//click on review stars
_click($PDP_PRODUCT_REVIEW_STARS);

if ($PDP_WRITEAREVIEW_BUTTON)
{
	_click($PDP_WRITEAREVIEW_BUTTON);
}
else
	{
	//click on be first to write review link
	_click($PDP_BEFIRST_REVIEW_LINK, _in($PDP_BEFIRST_REVIEW_SECTION));
	}

//overlay should open
_assertVisible($PDP_REVIEW_OVERLAY);
	
//entering all the values
ReviewValuesInOverlay($PDP_Standard,8);	
//Submission icon
_assertVisible(_span("bv-submission-icon"));
_assertVisible(_span("Your review was submitted!"));
_assertVisible($PDP_REVIEW_OVERLAY);
_assertVisible(_button("bv-submit-button"));
_click(_button("bv-submit-button"));
_assertNotVisible($PDP_REVIEW_OVERLAY);

}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

/*var $t=_testcase(" "," ");
$t.start();
try
{

	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();*/

cleanup();
