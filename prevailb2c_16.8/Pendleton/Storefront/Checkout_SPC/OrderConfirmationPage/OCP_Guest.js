_include("../../../GenericLibrary/GlobalFunctions.js");
_include("../../../GenericLibrary/BM_Functions.js");
_resource("OrderConfirmation.xls");
_resource("../Billing/BillingPage.xls");

deleteUser();

var $backgroundColor="rgb(255, 255, 255)";
var $color1="rgb(159, 149, 139)";

var $shippingTax;
var $tax;
var $subTotal;
var $Ordertotal;
var $paymentType;

var $sheetName;
var $rowNum;

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//var $OCP_Address1=$OCP_Address[0][1]+" "+$OCP_Address[0][2]+" "+$OCP_Address[0][3]+" "+$OCP_Address[0][4]+" "+$OCP_Address[0][7]+" "+$OCP_Address[1][6]+" "+$OCP_Address[0][8]+" "+$OCP_Address[0][5]+" "+$OCP_Address[0][9];
//var $shippingAddress="Shipping To "+$OCP_Address[0][11];
var $shippingAddress=$OCP_Address[0][11];
var $billingAddress="Billing Address "+$OCP_Address[0][13];

//var $t=_testcase("125787/125785","Verify the UI & navigation of the 'Order summery' section on Thank you for your order page in a application as a Guest user");
var $t=_testcase("352628/352634","Verify the UI of the Order Confirmation page in Application as a Anonymous user. / Verify the navigation to Order confirmation page as guest user");
$t.start();
try
{
	//navigating to Shipping page
	navigateToCartSearch($Order_Product[0][1],1);
	
	_click($CART_CHECKOUT_BUTTON);
	_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON); 
	shippingAddress($OCP_Address,0);
	//check use this for billing check box
	_check($SHIPPING_BILLING_ADDRESS_CHECKBOX);
	//Collect the shipping method 
	
	var $radioButtons = _collect("_radio","/(.*)/", _in(_div("shipping-method-list")));
	var $shippingMethodsLabel = _collect("_label","/(.*)/", _in(_div("shipping-method-list")));
	for (var $k = 0; $k < $radioButtons.length; $k++) 
	{
		var $radioButtonStatus = _getAttribute($radioButtons[$k], "checked");
		_log("$radioButtonStatus:-  "+ $radioButtonStatus);
		if ("true" == $radioButtonStatus || true == $radioButtonStatus ) 
		{
			var $temp = _getText($shippingMethodsLabel[$k]);
			var $shippingMethod = $temp.substring(0, $temp.indexOf(":"));
			_log("$shippingMethod:-  "+ $shippingMethod);
			break;
		}
	}
	
	if(isMobile() && !mobile.iPad())
	{
		$shippingTax=_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING),"/[$](.*)/",true).toString();
		$subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_PRICE_MOBILE),"/[$](.*)/",true).toString();
	}
	else
	{
		$shippingTax=_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING),"/[$](.*)/",true).toString();
		$subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_PRICE),"/[$](.*)/",true).toString();
	}
	_click($SHIPPING_SUBMIT_BUTTON);
	//Address Verification overlay
//	addressVerificationOverlay();
	
	BillingAddress($OCP_Address,0); 
	$tax=_extract(_getText($SHIPPING_RIGHT_NAV_SALES_TAX),"/[$](.*)/",true).toString();
	_log("$tax:-  "+ $tax);
	_assert(false,"Check Sales tax");
	$Ordertotal=_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/[$](.*)/",true).toString();
	
	//entering Credit card Information
	PaymentDetails($Payment_data,0);
	$paymentType="Credit Card";
	_wait(3000,_isVisible($BILLING_CONTINUE_BUTTON));
	_click($BILLING_CONTINUE_BUTTON);
	_wait(10000,_isVisible($BILLING_ADDRESS_SUBMIT));
	
	//Address Verification overlay
//	addressVerificationOverlay();
	//fetch order total
//	var $orderTot=_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/[ ].*[ ](.*)/",true);
	var $orderTot=$Ordertotal;
	_log("$orderTot:-  "+ $orderTot);
	//Placing order
	_click($BILLING_PAYMENT_PLACE_ORDER_BUTTON);
	
	if(_isVisible($BILLING_ADDRESS_CORRECTION_CLOSE_LINK))
	{
	_click($BILLING_ADDRESS_CORRECTION_CLOSE_LINK);
	}
	//verify the navigation to OCP page
	_assertVisible($ORDER_CONFIRMATION_HEADING);
	_assertEqual($OCP[0][0], _getText(_heading1("/(.*)/", _in(_div("confirmation-message")))));
	//verify the UI
	_assertVisible($ORDER_CONFIRMATION_ORDER_DATE_HEADING);
//	_assertVisible($ORDER_CONFIRMATION_ORDER_NUMBER);
//	var $orderID = _getText($ORDER_CONFIRMATION_ORDER_NUMBER);
//	_log("$orderID:- "+ $orderID);
//	_assertEqual($OCP[5][0], _getText($ORDER_CONFIRMATION_SECTIOIN_HEADING));
	
	//Payment Section
	_assertEqual("Payment Method", _getText(_div("label", _in(_div("order-payment-instruments")))));
	_assertEqual("Payment Total", _getText(_div("label", _in(_div("order-payment-summary")))));
	_assertEqual("Billing Address", _getText(_div("label", _in(_div("order-billing")))));
	
	//BILLING ADDRESS
	_assertVisible($ORDER_CONFIRMATION_BILLING_DIV);
	//Order Total
	_assertVisible($ORDER_CONFIRMATION_ORDER_TOTAL_SECTIOIN);
	//SHIPPING ADDRESS
	_assertVisible($ORDER_CONFIRMATION_SHIPPING_ADDRESS);
	//Return to Shopping
	_assertEqual($OCP[5][4], _getText($ORDER_CONFIRMATION_RETURN_TO_SHOPPING_LINK));
	//Create Account section
	_assertEqual($OCP[6][0], _getText($ORDER_CONFIRMATION_CREATE_ACCOUNT_HEADING));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("125786/148929","Verify the UI of the 'Shipment' section on Thank you for your order page in a application as a Guest user");
//var $t=_testcase("","");
$t.start();
try
{
	//verifying the UI of Shipment section
	//product name
	_assertVisible($ORDER_CONFIRMATION_PRODUCT_LINK);
	//full product details div
	_assertVisible($ORDER_CONFIRMATION_PRODUCT_DETAILS_DIV);

	if(!isMobile() || mobile.iPad())
	{
	if(_isIE())
		{
		_assertVisible($ORDER_CONFIRMATION_LINE_ITEM_PRICE1);
		_assertVisible($ORDER_CONFIRMATION_LINE_ITEM_QUANTITY1);
		}
	else
		{
		//quantity
		_assertVisible($ORDER_CONFIRMATION_LINE_ITEM_QUANTITY);
		//price
		_assertVisible($ORDER_CONFIRMATION_LINE_ITEM_PRICE);
		}
	}
	//verifying the shipping address
	_assertVisible($ORDER_CONFIRMATION_SHIPPING_ADDRESS);
	_assertEqual($OCP[0][2]+" "+$shippingAddress,_getText($ORDER_CONFIRMATION_SHIPPING_ADDRESS));
	//return shopping
	_assertVisible($ORDER_CONFIRMATION_RETURN_TO_SHOPPING_LINK);
	//shipping status and method
	//shipping status
//	_assertEqual($OCP[2][7], _getText($ORDER_CONFIRMATION_SHIPPING_STATUS_HEADING));
//	_assertVisible($ORDER_CONFIRMATION_SHIPPING_STATUS);
	//shipping method
//	_assertVisible($ORDER_CONFIRMATION_SHIPPING_METHOD_HEADING);
	_assertEqual($OCP[3][7], _getText($ORDER_CONFIRMATION_SHIPPING_METHOD_HEADING));
	_assertVisible($ORDER_CONFIRMATION_SHIPPING_METHOD);
	_assertEqual($shippingMethod, _getText($ORDER_CONFIRMATION_SHIPPING_METHOD));
	//entire div
	_assertVisible($ORDER_CONFIRMATION_SHIPPING_DIV);
	
}catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

	
//var $t=_testcase("140446/148932","Verify the details to be displayed in the 'SHIPPING TO' section on Thank you for your order page in a application as a Guest user");
var $t=_testcase("352633","Verify the details to be displayed in the 'SHIPPING TO' Section on Order Confirmation page in Application .");
$t.start();
try
{
	
	//verifying the shipping address
	_assertEqual($OCP[0][2]+" "+$shippingAddress,_getText($ORDER_CONFIRMATION_SHIPPING_ADDRESS));
	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


//var $t=_testcase("140447/125785/148933","Verify the details to be displayed in the 'BILLING ADDRESS' section on Thank you for your order page in a application as a Guest user");
var $t=_testcase("352637","Verify the details to be displayed in the 'BILLING ADDRESS' section on Thank you for your order page in a application as a Guest user");
$t.start();
try
{
	
	//verifying the billing address
	_assertEqual("/"+$billingAddress+"/i",_getText($ORDER_CONFIRMATION_BILLING_DIV).replace(",",""));

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


//var $t=_testcase("140448/140449/125785/148934","Verify the details to be displayed in the 'PAYMENT METHOD' section on Thank you for your order page in a application as a Guest user");
var $t=_testcase("352642","Verify the details to be displayed in the 'PAYMENT METHOD' Section on Order Confirmation page in Application as a Anonymous user.");
$t.start();
try
{
	_assertVisible($ORDER_CONFIRMATION_PAYMENT_DIV);
	_assertVisible($ORDER_CONFIRMATION_PAYMENT_TYPE);
	_assertEqual($OCP[9][0], _getText($ORDER_CONFIRMATION_PAYMENT_TYPE));


	if($paymentType=="Credit Card")
	{
//		if($CreditCardPayment==$BMConfig[1][4])
		{
//			var $exp=$Payment_data[0][7]+"."+$paypal[3][4]+"."+$paypal[2][5];
//			$sheetName=$paypal;
//			$rowNum=2;
		}
//		else
		{
			var $exp=$Payment_data[0][7]+"."+$Payment_data[1][4]+"."+$Payment_data[0][5];
			$sheetName=$Payment_data;
			$rowNum=0;
		}

		var $paymentDetails=new Array();
		$paymentDetails=_getText($ORDER_CONFIRMATION_PAYMENT_DIV).split(" ");
		var $num=$paymentDetails[4].split("*")[12];

		_assertVisible($ORDER_CONFIRMATION_PAYMENT_DIV);
//		_assertEqual($Payment_data[0][9]+" "+$orderTot, _getText($ORDER_CONFIRMATION_PAYMENT_DIV));
		_assertEqual($Payment_data[0][9]+" "+"Total:"+" $ "+$orderTot, _getText($ORDER_CONFIRMATION_PAYMENT_DIV));
	}
	else if($paymentType=="Pay Pal")
	{
		_assertVisible($ORDER_CONFIRMATION_AMOUNT_HEADING);
		var $OCPtotal=parseFloat(_extract(_getText($ORDER_CONFIRMATION_AMOUNT),"/[$](.*)/",true));
		_assertEqual($Ordertotal,$OCPtotal);
	}
	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("140450/148935","Verify the display of the 'PAYMENT TOTAL' section on Thank you for your order page in a application as a Guest user");
//var $t=_testcase("","Verify the display of the 'PAYMENT TOTAL' section on Thank you for your order page in a application as a Guest user");
$t.start();
try
{
	
	//all other values
	_assertVisible($ORDER_CONFIRMATION_SUBTOTAL_HEADING);
//	_assertVisible($ORDER_CONFIRMATION_SHIPPING_CHARGES_HEADING+" "+$shippingMethod);
	_assertVisible(_cell($OCP[2][1] + " "+$shippingMethod));
	_assertVisible($ORDER_CONFIRMATION_SALES_TAX);
	_assertVisible($ORDER_CONFIRMATION_ORDER_TOTAL_LABEL);
	var $columnNum = 4;
	if (_isVisible(_cell("TBD", _in(_row("order-sales-tax"))))) 
	{
		$columnNum = 5;
	}
	_assertEqual($OCP[$columnNum][1], _getText($ORDER_CONFIRMATION_ORDER_TOTAL_LABEL));
	_assertEqual($subTotal,_extract(_getText($SHIPPING_RIGHT_NAV_SUBTOTAL),"/[$](.*)/",true).toString());
	_assertEqual($shippingTax,_extract(_getText($SHIPPING_RIGHT_NAV_SHIPPING_COST),"/[$](.*)/",true).toString());
	_assertEqual($tax,_extract(_getText($SHIPPING_RIGHT_NAV_SALES_TAX),"/[$](.*)/",true).toString());
	_assertEqual($Ordertotal,_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/[$](.*)/",true).toString());

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


//var $t=_testcase("140445/148931","Verify the details to be displayed for product line 'ITEM' on Thank you for your order page in a application as a Guest user");
var $t=_testcase("352630","Verify the details to be displayed for product line 'ITEM' on Thank you for your order page in a application as a Guest user");
$t.start();
try
{
	//verifying the line item
	_assertVisible($ORDER_CONFIRMATION_PRODUCT_LINK);
	_assertEqual($CProductName, _getText($ORDER_CONFIRMATION_PRODUCT_LINK));
	//_assertVisible($ORDER_CONFIRMATION_ITEM_NUMBER_HEADING);
//	if(isMobile() && !mobile.iPad())
//	{
//		_assertVisible(_cell($CQuantity));
//		_assertVisible(_cell("/"+$COrderTotal+"/"));
//	}
//	else
	{
		_assertVisible($ORDER_CONFIRMATION_LINE_ITEM_PRICE1);
		var $expectedSubTot = _extract(_getText($ORDER_CONFIRMATION_LINE_ITEM_PRICE1), "/[$](.*)/", true).toString();
		_assertEqual($subTotal,$expectedSubTot);
		
		_assertVisible($ORDER_CONFIRMATION_LINE_ITEM_QUANTITY1);
		_assertEqual($CQuantity,_extract(_getText($ORDER_CONFIRMATION_LINE_ITEM_QUANTITY1),"/[ ](.*)/", true));
	}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


//var $t=_testcase("288377","Verify the UI of the Create account section in order confirmation page as an anonymous user");
var $t=_testcase("352627","Verify the UI of the Create account section in order confirmation page as an anonymous user");
$t.start();
try
{	
	//Create account heading
	_assertVisible($ORDER_CONFIRMATION_CREATE_ACCOUNT_HEADING);
	_assertEqual("Create Account", _getText($ORDER_CONFIRMATION_CREATE_ACCOUNT_HEADING));
	//sub heading
	_assertVisible(_paragraph($OCP[4][0]));
	//first name
	_assertVisible($REGISTER_FIRSTNAME);
	_assertVisible($REGISTER_FIRSTNAME_TEXTBOX);
	//last name
	_assertVisible($REGISTER_LASTNAME);
	_assertVisible($REGISTER_LASTNAME_TEXTBOX);
	//email
	_assertVisible($REGISTER_EMAIL);
	_assertVisible($REGISTER_EMAIL_TEXTBOX);
	//confirm email
	_assertVisible($REGISTER_CONFIRM_EMAIL);
	_assertVisible($REGISTER_CONFIRM_EMAIL_TEXTBOX);
	//password
	_assertVisible($REGISTER_PASSWORD);
	_assertVisible($REGISTER_PASSWORD_TEXTBOX);
	_assertVisible($REGISTER_PASSWORD_STATICTEXT);
	//confirm password
	_assertVisible($REGISTER_CONFIRM_PASSWORD);
	_assertVisible($REGISTER_CONFIRM_PASSWORD_TEXTBOX);
	//create account button
	_assertVisible($REGISTER_SUBMIT_BUTTON);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

//var $t=_testcase("125790/140443","Verify the navigation of the 'Product name' link on Thank you for your order page in a application as a Guest user");
var $t=_testcase("352640","Verify the functionality of Product name  on Order Confirmation page in Application as a Anonymous user.");
$t.start();
try
{	
	//clicking on product name link
	var $pName=_getText($ORDER_CONFIRMATION_PRODUCT_LINK);
	_click($ORDER_CONFIRMATION_PRODUCT_LINK);	
	//verify the navigation to PDP
	_assertVisible($PDP_MAIN);
	_assertVisible($PDP_PRODUCTNAME);
	_assertEqual($pName, _getText($PDP_PRODUCTNAME));
	if(!isMobile())
	{
		//bread crumb
		_assertVisible(_span($pName,_in($PAGE_BREADCRUMB)));
	}

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


//var $t=_testcase("148937/125889/125885/125886/125887/125904/125788/125889/148936","Verify the field validations for First Name/last name/email/confirm email/password/confirm password & valid data text box in right nav & 'Order Number' on Thank you for your order page in a application as a Guest user");
var $t=_testcase("352621/352622/352629/352624/352616/352626/352631","Verify the field validations for First Name/last name/email/confirm email/password/confirm password & valid data text box in right nav & 'Order Number' on Thank you for your order page in a application as a Guest user");
$t.start();
try
{	
	//navigating to OCP 
	//navigating to Shipping page
	navigateToShippingPage($Order_Product[0][1],1);
	shippingAddress($OCP_Address,0);
	//check use this for billing check box
	_check($SHIPPING_BILLING_ADDRESS_CHECKBOX);
	
	if(isMobile() && !mobile.iPad())
	{
		$shippingTax=_extract(_getText($SHIPPING_RIGHT_NAV_SHIPPING_COST),"/[$](.*)/",true).toString();
		$subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_PRICE_MOBILE),"/[$](.*)/",true).toString();
	}
	else
	{
		$shippingTax=_extract(_getText($SHIPPING_RIGHT_NAV_SHIPPING_COST),"/[$](.*)/",true).toString();
		$subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_PRICE),"/[$](.*)/",true).toString();
	}
	_click($SHIPPING_SUBMIT_BUTTON);
	//Address Verification overlay
//	addressVerificationOverlay();
	BillingAddress($OCP_Address,0);
	$tax=_extract(_getText($SHIPPING_RIGHT_NAV_SALES_TAX),"/[$](.*)/",true).toString();
	$Ordertotal=_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/[$](.*)/",true).toString();
	//entering Credit card Information
	PaymentDetails($Payment_data,0);
	_wait(3000,_isVisible($BILLING_CONTINUE_BUTTON));
	_click($BILLING_CONTINUE_BUTTON);
	_wait(10000,_isVisible($BILLING_ADDRESS_SUBMIT));
	   
	//click on Place Order
	_click($BILLING_PAYMENT_PLACE_ORDER_BUTTON);
	
	if(_isVisible($BILLING_ADDRESS_CORRECTION_CLOSE_LINK))
	{
		_click($BILLING_ADDRESS_CORRECTION_CLOSE_LINK);
	}
	
	//verify the Order Number
	_assertVisible($ORDER_CONFIRMATION_HEADING);
//validate the fields
for(var $i=0;$i<$Account_Validation.length;$i++)
	{
	_log("LOOP:- "+ $i);
		if($i==10)
		{
			createAccount();
			//verify whether account got created or not
//	 	   var $exp=$userData[$user][1]+" "+$userData[$user][2];
//	 	   _assertEqual($exp, _extract(_getText($MY_ACCOUNT_HEADING),"/[|] (.*) [(]/",true).toString());
	 	  _assertEqual("My Account"+" | "+$userData[$user][1]+" "+$userData[$user][2]+" "+"(Logout)", _getText(_div("pwheading-one")));
	 	 _assertEqual("My Account", _getText(_div("breadcrumb")));
	 	  
		}
	else
		{
		 _setValue($REGISTER_FIRSTNAME_TEXTBOX, $Account_Validation[$i][1]);
	 	 _setValue($REGISTER_LASTNAME_TEXTBOX, $Account_Validation[$i][2]);
	 	 _setValue($REGISTER_EMAIL_TEXTBOX, $Account_Validation[$i][3]);
	 	 _setValue($REGISTER_CONFIRM_EMAIL_TEXTBOX, $Account_Validation[$i][4]);
	 	 _setValue($REGISTER_PASSWORD_TEXTBOX, $Account_Validation[$i][5]);
	 	 _setValue($REGISTER_CONFIRM_PASSWORD_TEXTBOX, $Account_Validation[$i][6]);
	 	 if($i!=1)
	 		 {
	 		_click($REGISTER_SUBMIT_BUTTON);
	 		 }	
	 	 //blank field validation
	 	 if($i==0)
	 		 {
		 		  _assertEqual($backgroundColor, _style($REGISTER_FIRSTNAME_TEXTBOX, "background-color"));
		 		  _assertEqual($backgroundColor, _style($REGISTER_LASTNAME_TEXTBOX, "background-color"));
		 		  _assertEqual($backgroundColor, _style($REGISTER_EMAIL_TEXTBOX, "background-color"));
		 		  _assertEqual($backgroundColor, _style($REGISTER_CONFIRM_EMAIL_TEXTBOX, "background-color"));
		 		  _assertEqual($backgroundColor, _style($REGISTER_PASSWORD_TEXTBOX, "background-color"));
		 	  	  _assertEqual($backgroundColor, _style($REGISTER_CONFIRM_PASSWORD_TEXTBOX, "background-color"));
		 	  	  
		 	  	  verifyBorderColor($Account_Validation[2][8], $REGISTER_FIRSTNAME_TEXTBOX);
		 	  	verifyBorderColor($Account_Validation[2][8], $REGISTER_LASTNAME_TEXTBOX);
		 	  	verifyBorderColor($Account_Validation[2][8], $REGISTER_EMAIL_TEXTBOX);
		 	  	verifyBorderColor($Account_Validation[2][8], $REGISTER_CONFIRM_EMAIL_TEXTBOX);
		 	  	verifyBorderColor($Account_Validation[2][8], $REGISTER_PASSWORD_TEXTBOX);
		 	  	verifyBorderColor($Account_Validation[2][8], $REGISTER_CONFIRM_PASSWORD_TEXTBOX);
		 	  	
		 	  	
	 		 }
	 	 //Max characters
	 	 else if($i==1)
	 		 {
		 		_assertEqual($Account_Validation[0][9],_getText($REGISTER_FIRSTNAME_TEXTBOX).length);
		 		_assertEqual($Account_Validation[1][9],_getText($REGISTER_LASTNAME_TEXTBOX).length);
		 		_assertEqual($Account_Validation[2][9],_getText($REGISTER_EMAIL_TEXTBOX).length);
		 		_assertEqual($Account_Validation[3][9],_getText($REGISTER_CONFIRM_EMAIL_TEXTBOX).length);
		 		_assertEqual($Account_Validation[4][9],_getText($REGISTER_PASSWORD_TEXTBOX).length);
		 		_assertEqual($Account_Validation[5][9],_getText($REGISTER_CONFIRM_PASSWORD_TEXTBOX).length);
	 		 }
	 	 //invalid data
	 	 else if($i==2 || $i==3 ||$i==4 || $i==5)
	 		 {
		 		_assertEqual($Account_Validation[$i][7], _getText($ORDER_CONFIRMATION_EMAIL_ERROR_MESSAGE));
		 		 _assertEqual($Account_Validation[2][8], _style($ORDER_CONFIRMATION_EMAIL_ERROR_MESSAGE, "color"));	 	
		 		 
		 		_assertEqual($Account_Validation[$i][7], _getText($ORDER_CONFIRMATION_CONFIRMEMAIL_ERROR_MESSAGE));
		 		_assertEqual($Account_Validation[2][8], _style($ORDER_CONFIRMATION_CONFIRMEMAIL_ERROR_MESSAGE, "color"));	
	 		 }	 	 
	 	 //password less than 8 characters
	 	 else if($i==6)
	 		 {
		 		_assertVisible(_div($Account_Validation[$i][7]));
		 		_assertEqual($backgroundColor, _style($REGISTER_EMAIL_TEXTBOX, "background-color"));
		 		//_assertEqual($backgroundColor1,_style(_span($Account_Validation[1][10]), "color"));			 		 
		 		_assertVisible(_div($Account_Validation[$i][7], _in(_div("/ Confirm Password /"))));
		 		_assertEqual($backgroundColor, _style($REGISTER_CONFIRM_EMAIL_TEXTBOX, "background-color"));
		 		//_assertEqual($color1,_style(_span($Account_Validation[2][10]), "color"));
	 		 }
	 	 else if($i==7 || $i==8)
	 		 {			 		
	 		 	if($i==7)
	 		 		{
//	 		 		 	_assertEqual($color1, _style(_span($Account_Validation[0][10]), "color"));
		 		 		_assertEqual($Account_Validation[7][7], _getText(_div("/error-message/")));
		 		 		_assertEqual($Account_Validation[2][8], _style(_div("form-caption error-message"),"color"));
		 		 		_assertEqual($Account_Validation[2][8], _style(_span("Confirm Email"),"color"));
	 		 		}
	 		 	if($i==8)
	 		 		{
//		 		 		_assertEqual($color1, _style(_span($Account_Validation[2][10]), "color"));
		 		 		_assertEqual($Account_Validation[8][7], _getText(_div("/error-message/")));
		 		 		_assertEqual($Account_Validation[2][8], _style(_div("/error-message/"),"color"));
		 		 		_assertEqual($Account_Validation[2][8], _style(_span("Confirm Password"),"color"));
		 		 		
	 		 		}

	 		 }
	 	else if($i==9)
	 		{
			 	_assertVisible(_div($Account_Validation[$i][7]));
			 	_assertEqual($Account_Validation[$i][7], _getText(_div("/error-message/")));
			 	_assertEqual($Account_Validation[2][8], _style(_div("/error-message/"),"color"));
			 	_assertEqual($Account_Validation[2][8], _style(_span("Email"),"color"));
	 		}
		}
}

	//_click($LOGIN_BREADCRUMB_MYACCOUNT);
	_click($ORDER_CONFIRMATION_ORDER_HISTORY_LINK_HEADER);
	//verifying the order details
	_assertVisible($ORDER_CONFIRMATION_ORDER_HISTORY_ORDER_NUMBER); 
	//Verify the ORDER ID in ORDER HISTORY page
	_assertEqual($orderNum, _getText($ORDERS_ID));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();
cleanup();


//var $t=_testcase("125784","Verify the functionality of 'Return to Shopping' button on Thank you for your order page in a application as a Guest user");
var $t=_testcase("352617","Verify the functionality of 'Return to Shopping' button on Thank you for your order page in a application as a Guest user");
$t.start();
try
{
	//navigating to OCP 
	//navigating to Shipping page
	navigateToShippingPage($Order_Product[0][1],1);
	shippingAddress($OCP_Address,0);
	//check use this for billing check box
	_check($SHIPPING_BILLING_ADDRESS_CHECKBOX);
	if(isMobile() && !mobile.iPad())
	{
		$shippingTax=_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING),"/[$](.*)/",true).toString();
		$subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_PRICE_MOBILE),"/[$](.*)/",true).toString();
	}
	else
	{
		$shippingTax=_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING),"/[$](.*)/",true).toString();
		$subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_PRICE),"/[$](.*)/",true).toString();
	}
	_click($SHIPPING_SUBMIT_BUTTON);
	//Address Verification overlay
//	addressVerificationOverlay();
	BillingAddress($OCP_Address,0);
	$tax=_extract(_getText($SHIPPING_RIGHT_NAV_SALES_TAX),"/[$](.*)/",true).toString();
	$Ordertotal=_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/[$](.*)/",true).toString();
	//entering Credit card Information
	PaymentDetails($Payment_data,0);
    _wait(3000,_isVisible($BILLING_CONTINUE_BUTTON));
    _click($BILLING_CONTINUE_BUTTON);
    _wait(10000,_isVisible($BILLING_ADDRESS_SUBMIT));

    //Address Verification overlay
//	addressVerificationOverlay();
	
	//click on Place Order
	_click($BILLING_PAYMENT_PLACE_ORDER_BUTTON);
	
	//verify the navigation to OCP page
	if(_isVisible($BILLING_ADDRESS_CORRECTION_CLOSE_LINK))
	{
		_click($BILLING_ADDRESS_CORRECTION_CLOSE_LINK);
	}
	
	//click on returning customer link
	_click($ORDER_CONFIRMATION_RETURN_TO_SHOPPING_LINK);
	//Verify the Home page
	_assertVisible(_div("/homepage/"));
	_assertVisible(_div("home-main"));
	
//	//verify the navigation to recently visited PLP
//	_assertVisible($PAGE_BREADCRUMB);
//	_wait(2000);
//	//Verify the PDP page
//	_assertVisible($PDP_MAIN);
//	_assertEqual($productNameInPDP, _getText($PDP_PRODUCTNAME));
	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();
	
var $t=_testcase("125924","Verifying multiple shipping functionality in order confirmation page for guest user");
$t.start();
try
{
	//navigating to Shipping page
	var $proQty = 2;
	navigateToShippingPage($Order_Product[0][1],$proQty);
	var $shippingMethod=_extract(_getText($SHIPPING_RIGHT_NAV_SHIPPING_COST),"/Shipping(.*)[$]/",true).toString().trim();
	//click on yes button
	_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
	//adding addresses
	for(var $i=0;$i<$multishipAddress.length;$i++)
	{
		_click(_span("Add/Edit Address["+$i+"]"));
		addAddressMultiShip($multishipAddress,$i);
	}
	
	var $j=1;
		for(var $k=0;$k<$multishipAddress.length;$k++)
		{
			_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$k+"_addressList"),$j);
			$j++;
		}
		
	_click($SHIPPING_MULTIPLE_ADDRESS_SAVE_BUTTON);
	_click($BILLING_MULTI_SHIPPING_CONTINUE_BUTTON);
	//fill billing details
	BillingAddress($OCP_Address,0); 
	//entering Credit card Information
	PaymentDetails($Payment_data,0);
    $paymentType="Credit Card";
    _wait(3000,_isVisible($BILLING_CONTINUE_BUTTON));
    _click($BILLING_CONTINUE_BUTTON);
    _wait(10000,_isVisible($BILLING_ADDRESS_SUBMIT));
    
	//click on Place Order
	_click($BILLING_PAYMENT_PLACE_ORDER_BUTTON);
	_wait(2000);
	
	//verify the navigation to OCP page
	if(_isVisible($BILLING_ADDRESS_CORRECTION_CLOSE_LINK))
	{
		_click($BILLING_ADDRESS_CORRECTION_CLOSE_LINK);
	}
	//verify the addresses
	var $count=_count("_table","/order-shipment-table/");
	_log($count);
	for(var $i=0;$i<$count;$i++)
		{
		
		var $ExpShippingAddress=$multishipAddress[$i][2]+" "+$multishipAddress[$i][3]+" "+$multishipAddress[$i][4]+" "+$multishipAddress[$i][5]+" "+$multishipAddress[$i][8]+", "+$multishipAddress[$i][11]+" "+$multishipAddress[$i][9]+" "+$multishipAddress[$i][6]+" "+$multishipAddress[$i][10];
		var $ActShippingAddress=_getText($ORDER_CONFIRMATION_SHIPPING_ADDRESS);
		_log($ActShippingAddress);
			if($ActShippingAddress.indexOf($multishipAddress[$i][2])>=0)
			{
				_assertEqual($ExpShippingAddress,$ActShippingAddress);
			}
			else 
			{
				var $ActShippingAddress=_getText($ORDER_CONFIRMATION_SHIPPING_ADDRESS1);
				//var $ActShippingAddress=_getText(_div("order-shipment-address["+$i+"]"));	
				_assertEqual($ExpShippingAddress,$ActShippingAddress);
			}
		}
	
	//Verify the OCP page
	_assertEqual("Thank you for your order!", _getText(_heading1("/(.*)/", _in(_div("confirmation-message")))));
	//Billing address
	_assertEqual("Billing Address"+" "+"steve rogers 1706 N NAGLE AVE street21 CHICAGO, IL 60707-4018 United States Phone: 3333333333", _getText(_div("order-billing")));
	
	//Verify the shipping headein
	var $shippingHeading = _collect("_heading2","/(.*)/", _in(_div("order-shipments")));
	_assertEqual($proQty, $shippingHeading.length);
	var $j = 1;
	for (var $i = 0; $i < $shippingHeading.length; $i++) 
	{
		_assertEqual("Shipment No. "+$j, _getText($shippingHeading[$i]));
		$j++;
	}
	
	var $shippingSection = _collect("_div","order-shipment-table", _in(_div("order-shipments")));
	for (var $i = 0; $i < $shippingSection.length; $i++) 
	{
		//Verify the shipping method label	
		_assertEqual("Method:", _getText(_div("label", _in(_div("shipping-method", _in($shippingSection[$i]))))));
		_assertEqual("Standard Shipping", _getText(_div("value", _in(_div("shipping-method", _in($shippingSection[$i]))))));
		
		//Verify the shipping address in OCP
		_assertEqual("Shipping To"+" "+$multishipAddress[$i][12], _getText(_div("order-shipment-address", _in($shippingSection[$i]))));
		
		//Verify the product name
		_assertEqual($productNameInPDP, _getText(_div("name", _in(_div("line-item-details", _in($shippingSection[$i]))))));
		_assertEqual("1",_extract(_getText(_div("line-item-quantity", _in($shippingSection[$i]))),"/[ ](.*)/", true));
		_assertEqual($productPriceInPDP, _extract(_getText(_div("line-item-price", _in($shippingSection[$i]))), "/[ ](.*)/", true));
	}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();	


cleanup();

//-----------------------------------------
