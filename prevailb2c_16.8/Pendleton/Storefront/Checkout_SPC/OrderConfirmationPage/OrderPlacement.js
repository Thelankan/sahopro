_include("../../../GenericLibrary/GlobalFunctions.js");
_include("../../../GenericLibrary/BM_Functions.js");

//for deleting user
_setAccessorIgnoreCase(true);
SiteURLs();
cleanup();
var $OCP_Address1=$OCP_Address[0][13];


for (var $j = 0; $j < 2; $j++) 
{
	if ($j == 1) 
	{
		//Place order as a registered user
		_click($HEADER_LOGIN_LINK);
		login();
	}
	
	
	for (var $i = 0; $i < 4; $i++) 
	{
	
		_log("$i:-  "+ $i);
		_log("******* " +$Card_Details[$i][2]+" *******");
		//var $t=_testcase("125787/125785","Verify the UI & navigation of the 'Order summery' section on Thank you for your order page in a application as a Guest user");
		var $t=_testcase("352646/352649/352652/352655/352656/352658/352662/352665","Verify the UI of the Order Confirmation page in Application as a Anonymous user. / Verify the navigation to Order confirmation page as guest user");
		$t.start();
		try
		{
			$colorBoolean = false;
			$sizeBoolean=false;
			ClearCartItems();
			//navigating to Shipping page
			navigateToCartSearch($Order_Product[0][1],1);
			
			_click($CART_CHECKOUT_BUTTON);
			if (_isVisible($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON)) 
			{
				_log("GUEST USER");
				_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
			} 
			else 
			{
				_log("REGISTERED USER");
			}
			shippingAddress($OCP_Address,0);
			//check use this for billing check box
			_check($SHIPPING_BILLING_ADDRESS_CHECKBOX);
			//Collect the shipping method 
			
			var $radioButtons = _collect("_radio","/(.*)/", _in(_div("shipping-method-list")));
			var $shippingMethodsLabel = _collect("_label","/(.*)/", _in(_div("shipping-method-list")));
			for (var $k = 0; $k < $radioButtons.length; $k++) 
			{
				var $radioButtonStatus = _getAttribute($radioButtons[$k], "checked");
				_log("$radioButtonStatus:-  "+ $radioButtonStatus);
				if ("true" == $radioButtonStatus || true == $radioButtonStatus ) 
				{
					var $temp = _getText($shippingMethodsLabel[$k]);
					var $shippingMethod = $temp.substring(0, $temp.indexOf(":"));
					_log("$shippingMethod:-  "+ $shippingMethod);
					break;
				}
			}
			
			if(isMobile() && !mobile.iPad())
			{
				$shippingTax=_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING),"/[$](.*)/",true).toString();
				$subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_PRICE_MOBILE),"/[$](.*)/",true).toString();
			}
			else
			{
				$shippingTax=_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING),"/[$](.*)/",true).toString();
				$subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_PRICE),"/[$](.*)/",true).toString();
			}
			_click($SHIPPING_SUBMIT_BUTTON);
			//Address Verification overlay
	//		addressVerificationOverlay();
			
			BillingAddress($OCP_Address,0); 
			$tax=_extract(_getText($SHIPPING_RIGHT_NAV_SALES_TAX),"/[$](.*)/",true).toString();
			_log("$tax:-  "+ $tax);
			_assert(false,"Check Sales tax");
			$Ordertotal=_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/[$](.*)/",true).toString();
			
			//entering Credit card Information
			PaymentDetails($Card_Details,$i);
			$paymentType="Credit Card";
			_wait(3000,_isVisible($BILLING_CONTINUE_BUTTON));
			_click($BILLING_CONTINUE_BUTTON);
			_wait(10000,_isVisible($BILLING_ADDRESS_SUBMIT));
			
			//Address Verification overlay
	//		addressVerificationOverlay();
			//fetch order total
	//		var $orderTot=_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/[ ].*[ ](.*)/",true);
			var $orderTot=$Ordertotal;
			_log("$orderTot:-  "+ $orderTot);
			//Placing order
			_click($BILLING_PAYMENT_PLACE_ORDER_BUTTON);
			
			if(_isVisible($BILLING_ADDRESS_CORRECTION_CLOSE_LINK))
			{
			_click($BILLING_ADDRESS_CORRECTION_CLOSE_LINK);
			}
			
			//verify the navigation to OCP page
			_assertVisible($ORDER_CONFIRMATION_HEADING);
			_assertEqual($OCP[0][0], _getText(_heading1("/(.*)/", _in(_div("confirmation-message")))));
			//verify the UI
			_assertVisible($ORDER_CONFIRMATION_ORDER_DATE_HEADING);
	//		_assertVisible($ORDER_CONFIRMATION_ORDER_NUMBER);
	//		var $orderID = _getText($ORDER_CONFIRMATION_ORDER_NUMBER);
	//		_log("$orderID:- "+ $orderID);
	//		_assertEqual($OCP[5][0], _getText($ORDER_CONFIRMATION_SECTIOIN_HEADING));
			
			//Payment Section
			_assertEqual("Payment Method", _getText(_div("label", _in(_div("order-payment-instruments")))));
			_assertEqual("Payment Total", _getText(_div("label", _in(_div("order-payment-summary")))));
			_assertEqual("Billing Address", _getText(_div("label", _in(_div("order-billing")))));
			
			//BILLING ADDRESS
			_assertVisible($ORDER_CONFIRMATION_BILLING_DIV);
			//Order Total
			_assertVisible($ORDER_CONFIRMATION_ORDER_TOTAL_SECTIOIN);
			//SHIPPING ADDRESS
			_assertVisible($ORDER_CONFIRMATION_SHIPPING_ADDRESS);
			//Return to Shopping
			_assertEqual($OCP[5][4], _getText($ORDER_CONFIRMATION_RETURN_TO_SHOPPING_LINK));
			
			if ($j == 0) 
			{
				//Create Account section
				_assertEqual($OCP[6][0], _getText($ORDER_CONFIRMATION_CREATE_ACCOUNT_HEADING));
			} 
			else 
			{
				//Create Account section should not display for Registered user
				_assertNotVisible($ORDER_CONFIRMATION_CREATE_ACCOUNT_HEADING);
			}
			
			//Verify the LINE ITEM
			//Product NAme
			_assertVisible(_div("label", _in(_div("line-item-details"))));
			_assertEqual($productNameInPDP, _getText(_div("name", _in(_div("line-item-details")))));
			//SKU
			_assertEqual("Item No.:", _getText(_span("label", _in(_div("line-item-details")))));
			_assertEqual($productSKUinPDP, _getText(_span("value", _in(_div("line-item-details")))));
	
			//Size
			if ($sizeBoolean) 
			{
				_assertEqual("Size:", _getText(_span("label", _in(_div("/Size/", _in(_div("line-item-details")))))));
				_assertEqual($productSizeInPDP, _getText(_span("value", _in(_div("/Size/", _in(_div("line-item-details")))))));
			}
			//Color
			if ($colorBoolean) 
			{
			
				_assertEqual("color:", _getText(_span("label", _in(_div("/color/", _in(_div("line-item-details")))))));
				_assertEqual($productColorInPDP, _getText(_span("value", _in(_div("/color/", _in(_div("line-item-details")))))));
			}
			
			//Verify the Quantity
			_assertEqual($productQuantityInPDP, _extract(_getText(_div("line-item-quantity")), "/[ ](.*)/",true));
			
			//Verify the Price
			_assertEqual($productPriceInPDP, _extract(_getText(_div("line-item-price")), "/[ ](.*)/", true));
			
			//Verify the Payment details
	//		_assertEqual("Payment Method"+" "+"Credit Card"+" "+"Scott Lang"+" "+"Visa"+" "+"Credit Card Token : 85896360"+" ************4113"+" "+"Amount:"+" "+$orderTot, _getText(_div("order-payment-instruments")));
			_assertEqual($OCP[1][0]+" "+$OCP[8][0]+" "+$Card_Details[$i][1]+" "+$Card_Details[$i][2]+" "+$Card_Details[$i][7]+" "+"Amount:"+" "+$orderTot, _getText(_div("order-payment-instruments")));
			
			//Verify the billing address
			_assertEqual($OCP[1][6]+" "+$OCP_Address1, _getText(_div("order-billing")).replace(",",""));
			//Verify the Shipping address
			_assertEqual($OCP[0][2]+" "+$OCP_Address1.replace("Phone: ",""), _getText(_div("order-shipment-address")).replace(",",""));
			
		}
		catch($e)
		{      
		     _logExceptionAsFailure($e);
		}
		$t.end();

	}	//End of $i loop

}	//End of $j loop
cleanup();


var $t=_testcase("","Create a new account and save address and card details in profile, select the saved address and card details and place order");
$t.start();
try
{
	//create a new account
	_click(_link("user-register"));
	var $newEmail = newEmailId();
	createNewAccount($newEmail);
	
	//Navigate to Payment setting page
	_click(_link("/credit cards/", _in(_list("account-options"))));
	//Click on Add to Cart button
	_click(_link("/add-card/"));
	var $cardRow = 1;
	CreateCreditCard($cardRow, $Card_Details, false);
	_click(_submit("dwfrm_paymentinstruments_creditcards_create"));
	
	//Add address
	_click(_link("user_account"));
	_click(_link("/addresses/", _in(_list("account-options"))));
	//Click on Create address button
	_click(_link("/address-create/"));
	var $addressRow = 0;
	addAddress($Valid_Address, $addressRow);
	_click(_submit("dwfrm_profile_address_create"));
	
	_wait(3000);
	//navigating to Shipping page
	navigateToCartSearch($Order_Product[0][1],1);
	
	_click($CART_CHECKOUT_BUTTON);
	 //Verify the saved address dropdown in shipping page
	_assertVisible($SHIPPING_ADDRESS_DROPDOWN);
	//Verify the pre-populated address
	_assertEqual($Valid_Address[$addressRow][2], _getValue($SHIPPING_FN_TEXTBOX));
	_assertEqual($Valid_Address[$addressRow][3], _getValue($SHIPPING_LN_TEXTBOX));
	_assertEqual($Valid_Address[$addressRow][4], _getValue($SHIPPING_ADDRESS1_TEXTBOX));
	_assertEqual($Valid_Address[$addressRow][5], _getValue($SHIPPING_ADDRESS2_TEXTBOX));
	_assertEqual($Valid_Address[$addressRow][8], _getValue($SHIPPING_CITY_TEXTBOX));
	_assertEqual($Valid_Address[$addressRow][9], _getValue($SHIPPING_ZIPCODE_TEXTBOX));
	_assertEqual($Valid_Address[$addressRow][6], _getSelectedText($SHIPPING_COUNTRY_DROPDOWN));
//	_assertEqual($Valid_Address[$addressRow][7], _getSelectedText($SHIPPING_STATE_DROPDOWN));
	_assertEqual($Valid_Address[$addressRow][10], _getValue($SHIPPING_PHONE_TEXTBOX));

	
	//Un-check the Use this as my billing address checkbox
	_uncheck($SHIPPING_BILLING_ADDRESS_CHECKBOX);
	
	
	var $radioButtons = _collect("_radio","/(.*)/", _in($SHIPPING_METHOD_LIST));
	var $shippingMethodsLabel = _collect("_label","/(.*)/", _in($SHIPPING_METHOD_LIST));
	for (var $k = 0; $k < $radioButtons.length; $k++) 
	{
		var $radioButtonStatus = _getAttribute($radioButtons[$k], "checked");
		_log("$radioButtonStatus:-  "+ $radioButtonStatus);
		if ("true" == $radioButtonStatus || true == $radioButtonStatus ) 
		{
			var $temp = _getText($shippingMethodsLabel[$k]);
			var $shippingMethod = $temp.substring(0, $temp.indexOf(":"));
			_log("$shippingMethod:-  "+ $shippingMethod);
			break;
		}
	}
	
	if(isMobile() && !mobile.iPad())
	{
		$shippingTax=_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING),"/[$](.*)/",true).toString();
		$subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_PRICE_MOBILE),"/[$](.*)/",true).toString();
	}
	else
	{
		$shippingTax=_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING),"/[$](.*)/",true).toString();
		$subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_PRICE),"/[$](.*)/",true).toString();
	}
	_click($SHIPPING_SUBMIT_BUTTON);
	
	//Verify the prepoluted saved address in billing address form
	_assertVisible(_select("dwfrm_billing_addressList"));
	
	_assertEqual($Valid_Address[$addressRow][2], _getValue($BILLING_FN_TEXTBOX));
	_assertEqual($Valid_Address[$addressRow][3], _getValue($BILLING_LN_TEXTBOX));
	_assertEqual($Valid_Address[$addressRow][4], _getValue($BILLING_ADDRESS1_TEXTBOX));
	_assertEqual($Valid_Address[$addressRow][5], _getValue($BILLING_ADDRESS2_TEXTBOX));
	_assertEqual($Valid_Address[$addressRow][8], _getValue($BILLING_CITY_TEXTBOX));
	_assertEqual($Valid_Address[$addressRow][9], _getValue($BILLING_ZIPCODE_TEXTBOX));
	_assertEqual($Valid_Address[$addressRow][6], _getSelectedText($BILLING_COUNTRY_DROPDOWN));
//	_assertEqual($Valid_Address[6][7], _getSelectedText($BILLING_STATE_DROPDOWN));
	_assertEqual($Valid_Address[$addressRow][10], _getValue($BILLING_NUMBER_TEXTBOX));
	_assertEqual($newEmail, _getValue($BILLING_EMAIL_TEXTBOX));
	
	//Verify the saved credit card dropdown box
	_assertVisible(_select("creditCardList"));
	//Select the saved card
	_setSelected(_select("creditCardList"),1);
	_assertEqual($Card_Details[$cardRow][1], _getValue(_textbox("dwfrm_billing_paymentMethods_creditCard_owner")));
	_assertEqual($Card_Details[$cardRow][2], _getSelectedText(_select("dwfrm_billing_paymentMethods_creditCard_type")));
	_assertEqual($Card_Details[$cardRow][7], _getValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/")));
	_assertEqual($Card_Details[$cardRow][4], _getSelectedText(_select("dwfrm_billing_paymentMethods_creditCard_expiration_month")));
	_assertEqual($Card_Details[$cardRow][5], _getSelectedText(_select("dwfrm_billing_paymentMethods_creditCard_expiration_year")));
	_setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"), $Card_Details[$cardRow][6]);
	
	
	$tax=_extract(_getText($SHIPPING_RIGHT_NAV_SALES_TAX),"/[$](.*)/",true).toString();
	_log("$tax:-  "+ $tax);
	_assert(false,"Check Sales tax");
	$Ordertotal=_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/[$](.*)/",true).toString();
	//fetch order total
	var $orderTot=$Ordertotal;
	
	$paymentType="Credit Card";
	_wait(3000,_isVisible($BILLING_CONTINUE_BUTTON));
	_click($BILLING_CONTINUE_BUTTON);
	_wait(10000,_isVisible($BILLING_ADDRESS_SUBMIT));
	

	_log("$orderTot:-  "+ $orderTot);
	//Placing order
	_click($BILLING_PAYMENT_PLACE_ORDER_BUTTON);
	
	if(_isVisible($BILLING_ADDRESS_CORRECTION_CLOSE_LINK))
	{
	_click($BILLING_ADDRESS_CORRECTION_CLOSE_LINK);
	}
	//verify the navigation to OCP page
	_assertVisible($ORDER_CONFIRMATION_HEADING);
	_assertEqual($OCP[0][0], _getText(_heading1("/(.*)/", _in(_div("confirmation-message")))));
	//verify the UI
	_assertVisible($ORDER_CONFIRMATION_ORDER_DATE_HEADING);
//	_assertVisible($ORDER_CONFIRMATION_ORDER_NUMBER);
//	var $orderID = _getText($ORDER_CONFIRMATION_ORDER_NUMBER);
//	_log("$orderID:- "+ $orderID);
//	_assertEqual($OCP[5][0], _getText($ORDER_CONFIRMATION_SECTIOIN_HEADING));
	
	//Payment Section
	_assertEqual("Payment Method", _getText(_div("label", _in(_div("order-payment-instruments")))));
	_assertEqual("Payment Total", _getText(_div("label", _in(_div("order-payment-summary")))));
	_assertEqual("Billing Address", _getText(_div("label", _in(_div("order-billing")))));
	
	//BILLING ADDRESS
	_assertVisible($ORDER_CONFIRMATION_BILLING_DIV);
	//Order Total
	_assertVisible($ORDER_CONFIRMATION_ORDER_TOTAL_SECTIOIN);
	//SHIPPING ADDRESS
	_assertVisible($ORDER_CONFIRMATION_SHIPPING_ADDRESS);
	//Return to Shopping
	_assertEqual($OCP[5][4], _getText($ORDER_CONFIRMATION_RETURN_TO_SHOPPING_LINK));
	//Create Account section
	_assertNotVisible($ORDER_CONFIRMATION_CREATE_ACCOUNT_HEADING);
	
	
	//Verify the LINE ITEM
	//Product NAme
	_assertVisible(_div("label", _in(_div("line-item-details"))));
	_assertEqual($productNameInPDP, _getText(_div("name", _in(_div("line-item-details")))));
	//SKU
	_assertEqual("Item No.:", _getText(_span("label", _in(_div("line-item-details")))));
	_assertEqual($productSKUinPDP, _getText(_span("value", _in(_div("line-item-details")))));

	//Size
	if ($sizeBoolean) 
	{
	
		_assertEqual("Size:", _getText(_span("label", _in(_div("/Size/", _in(_div("line-item-details")))))));
		_assertEqual($productSizeInPDP, _getText(_span("value", _in(_div("/Size/", _in(_div("line-item-details")))))));
	}
	//Color
	if ($colorBoolean) 
	{
	
		_assertEqual("color:", _getText(_span("label", _in(_div("/color/", _in(_div("line-item-details")))))));
		_assertEqual($productColorInPDP, _getText(_span("value", _in(_div("/color/", _in(_div("line-item-details")))))));
	}
	
	//Verify the Quantity
	_assertEqual($productQuantityInPDP, _extract(_getText(_div("line-item-quantity")), "/[ ](.*)/",true));
	
	//Verify the Price
	_assertEqual($productPriceInPDP, _extract(_getText(_div("line-item-price")), "/[ ](.*)/", true));
	
	//Verify the Payment details
//		_assertEqual("Payment Method"+" "+"Credit Card"+" "+"Scott Lang"+" "+"Visa"+" "+"Credit Card Token : 85896360"+" ************4113"+" "+"Amount:"+" "+$orderTot, _getText(_div("order-payment-instruments")));
	_assertEqual($OCP[1][0]+" "+$OCP[8][0]+" "+$Card_Details[$cardRow][1]+" "+$Card_Details[$cardRow][2]+" "+$Card_Details[$cardRow][7]+" "+"Amount:"+" "+$orderTot, _getText(_div("order-payment-instruments")));
	
	//Verify the billing address
	var $addressString = $Valid_Address[$addressRow][12];
	_assertEqual($OCP[1][6]+" "+$addressString, _getText(_div("order-billing")));
	//Verify the Shipping address
	_assertEqual($OCP[0][2]+" "+$addressString.replace("Phone: ",""), _getText(_div("order-shipment-address")));
	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

cleanup();

/*
_log("******* PAYPAL *******");
var $t = _testcase("351508","Verify order placement with promotion and Paypal on billing page in Application as a guest user");
$t.start();
try
{
	
	ClearCartItems();
	//place order
	navigateToShippingPage($Order_PromoProduct[0][1],$Order_PromoProduct[0][3]);
	//shipping details
	shippingAddress($Order_Address,0);
	_click($SHIPPING_SUBMIT_BUTTON);
	//verify address verification overlay
	//address Verification Overlay
	if(_isVisible(_div("address-validation-dialog")))
	{
		_click(_submit("Continue"));
	}

	//Billing details
	BillingAddress($Order_Address,1);
	//Enter Coupon code
	_setValue($BILLING_COUPON_TEXTBOX, $Order_PromoProduct[0][2]);
	_click($BILLING_COUPON_APPLY_BUTTON);
	//Verify the applied promo
	_assert(false,"Verify the applied promo");
	var $totalAmountInBillingPage = _getText($ORDER_SUMMARY_TOTAL_AMOUNT);
	_log("$totalAmountInBillingPage:-  "+ $totalAmountInBillingPage);
	//Select the paypal payment method
	_click(_radio("is-PayPal"));
	//Click on billing continue button
	_click($BILLING_CONTINUE_BUTTON);
	paypal($Order_Paypal[0][0], $Order_Paypal[0][1]);
	//Verify the order review page
	verifyOrderReviewPage($totalAmountInBillingPage);
	//Placing order
	_click($ORDER_REVIEW_PLACE_ORDER_BUTTON);
	
	//Verify the Order Confirmation page
	verifyOrderConfirmationPage($Order_Generic[1][1], $totalAmountInBillingPage, $Order_Address[1][11], $Order_Address[0][11]);
	
	//Verify the product details
	verifyProductDetailInOCP($productNameInPDP, $selectedColorInPDP, $selectedSizeInPDP, $qtyInPDP, $proPriceInPDP);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


_log("******* EXPRESS PAYPAL *******");
SiteURLs();
cleanup();
var $t = _testcase("351760","Verify whether user is able to place the order using Paypal alone  on billing page in Application as a Anonymous user.");
$t.start();
try
{
	
	ClearCartItems();
	//place order
	navigateToCart($Order_PromoProduct[0][1],$Order_PromoProduct[0][3]);
	_wait(2000);
	//Click on Paypal button in CART page
	_click(_link("/checkout-paypal/"));
	//Enter paypal credentials
	paypal($Order_Paypal[0][0], $Order_Paypal[0][1]);
	_wait(3000);
	
	//Collect the prices in shipping page(FROM ORDER SUMMARY SECTION)
	var $subTotal=parseFloat(_extract(_getText($SHIPPING_RIGHT_NAV_SUBTOTAL),"/[$](.*)/",true));
	var $tax=parseFloat(_extract(_getText($SHIPPING_RIGHT_NAV_SALES_TAX),"/[$](.*)/",true).toString());
	_log("$tax:-  "+$tax);
	$shippingTax=parseFloat(_extract(_getText($SHIPPING_RIGHT_NAV_SHIPPING_COST),"/[$](.*)/",true).toString());
	_log("$shippingTax:-  "+$shippingTax);
	

	//shipping details
	shippingAddress($Order_Address,0);
	_click($SHIPPING_SUBMIT_BUTTON);
	//verify address verification overlay
	//address Verification Overlay
	if(_isVisible(_div("address-validation-dialog")))
	{
		_click(_submit("Continue"));
	}

	//Billing details
	BillingAddress($Order_Address,1);
	//Enter Coupon code
	_setValue($BILLING_COUPON_TEXTBOX, $Order_PromoProduct[0][2]);
	_click($BILLING_COUPON_APPLY_BUTTON);
	//Verify the applied promo
	_assert(false,"Verify the applied promo");
	var $totalAmountInBillingPage = _getText($ORDER_SUMMARY_TOTAL_AMOUNT);
	_log("$totalAmountInBillingPage:-  "+ $totalAmountInBillingPage);
	//Select the paypal payment method
	_click(_radio("is-PayPal"));
	//Click on billing continue button
	_click($BILLING_CONTINUE_BUTTON);
	paypal($Order_Paypal[0][0], $Order_Paypal[0][1]);
	//Verify the order review page
	verifyOrderReviewPage($totalAmountInBillingPage);
	//Placing order
	_click($ORDER_REVIEW_PLACE_ORDER_BUTTON);
	
	//Verify the Order Confirmation page
	verifyOrderConfirmationPage($Order_Generic[1][1], $totalAmountInBillingPage, $Order_Address[1][11], $Order_Address[0][11]);
	
	//Verify the product details
	verifyProductDetailInOCP($productNameInPDP, $selectedColorInPDP, $selectedSizeInPDP, $qtyInPDP, $proPriceInPDP);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
SiteURLs();
cleanup();
*/



