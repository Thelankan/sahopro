_include("../../../GenericLibrary/GlobalFunctions.js");
_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("OrderReview.xls");


var $sheetName;
var $rowNum;

	SiteURLs();
	_setAccessorIgnoreCase(true); 
//	cleanup();
	var $billingAddressWithoutPhoneNum = $Valid_Data_orderreview[0][1]+" "+$Valid_Data_orderreview[0][2]+" "+$Valid_Data_orderreview[0][3]+" "+$Valid_Data_orderreview[0][4]+" "+$Valid_Data_orderreview[0][7]+" "+$Valid_Data_orderreview[1][6]+" "+$Valid_Data_orderreview[0][8]+" "+$Valid_Data_orderreview[1][5];
	var $billingAddress=$billingAddressWithoutPhoneNum+" "+$Valid_Data_orderreview[0][9];
//	var $billingAddress=$Valid_Data_orderreview[0][1]+" "+$Valid_Data_orderreview[0][2]+" "+$Valid_Data_orderreview[0][3]+" "+$Valid_Data_orderreview[0][4]+" "+$Valid_Data_orderreview[0][7]+" "+$Valid_Data_orderreview[1][6]+" "+$Valid_Data_orderreview[0][8]+" "+$Valid_Data_orderreview[0][5];
	var $shippingAddress=$billingAddressWithoutPhoneNum+" "+"Method: "+$OrderReview[0][1];


//	var $t=_testcase("148892/288214/288218","Verify the UI of  'Summary' Page/UI of the help section in the Right pane/Total section displayed below the order Summary section in Application as a Anonymous user.");
	var $t=_testcase("352835","Verify the UI of  'Summary' Page/UI of the help section in the Right pane/Total section displayed below the order Summary section in Application as a Anonymous user.");
	$t.start();
	try
	{
		//Navigation till shipping page
		navigateToShippingPage($Order_Product[0][1],$Order_Product[0][2]);
		shippingAddress($Valid_Data_orderreview,0);
		if(isMobile() && !mobile.iPad())
		{
			$shippingTax=_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING),"/[$](.*)/",true).toString();
			$subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_PRICE_MOBILE),"/[$](.*)/",true).toString();
		}
		else
		{
			$shippingTax=_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING),"/[$](.*)/",true).toString();
			$subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_PRICE),"/[$](.*)/",true).toString();
		}
		_log("$shippingTax:-  "+ $shippingTax);
		_log("$subTotal:-  "+ $subTotal);
		
		//Collect the shipping method 
		var $radioButtons = _collect("_radio","/(.*)/", _in(_div("shipping-method-list")));
		var $shippingMethodsLabel = _collect("_div","/label-inline/", _in(_div("shipping-method-list")));
//			_collect("_label","radio-label", _in(_div("shipping-method-list")));
		for (var $k = 0; $k < $radioButtons.length; $k++) 
		{
			var $radioButtonStatus = _getAttribute($radioButtons[$k], "checked");
			_log("$radioButtonStatus:-  "+ $radioButtonStatus);
			if ("true" == $radioButtonStatus || true == $radioButtonStatus ) 
			{
				var $shippingMethodPrice;
				var $shippingtemp = false;
				if (_isVisible(_span("discount-shipping", _in($shippingMethodsLabel[$k])))) 
				{
					$shippingMethodPrice = _getText(_span("discount-shipping", _in($shippingMethodsLabel[$k])));
					$shippingtemp = true;
				}
				else if (_isVisible(_span("standard-shipping", _in($shippingMethodsLabel[$k])))) 
				{
					$shippingMethodPrice = _getText(_span("standard-shipping", _in($shippingMethodsLabel[$k])));
					$shippingtemp = true;
				}
				_log("$shippingMethodPrice:- "+ $shippingMethodPrice);
				var $label = _getText(_label("/(.*)/", _in($shippingMethodsLabel[$k])));
				var $shippingMethod = $label.substring(0, $label.indexOf(":"));
				_log("$shippingMethod:-  "+ $shippingMethod);
				break;
			}
		}
		
		_click($SHIPPING_SUBMIT_BUTTON);
		//Collect the shipping charge
		
		BillingAddress($Valid_Data_orderreview,0); 
		$tax=_extract(_getText($SHIPPING_RIGHT_NAV_SALES_TAX),"/[$](.*)/",true).toString();
		$Ordertotal=_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/[$](.*)/",true).toString();
		//entering Credit card Information
		var $rowNum = 4;
		PaymentDetails($Valid_Data_orderreview,$rowNum);
		$paymentType="Credit Card";
		_wait(1000);
		_click($BILLING_CONTINUE_BUTTON);
		
		//Address Verification overlay
		 
		if(!isMobile() || mobile.iPad())
		{
			//varify the navigation & UI of Order Summuary
			_assertVisible($CART_PRODUCT_HEADING);
			_assertVisible($CART_QUANTITY_HEADING);
			_assertVisible($CART_TOTAL_HEADING);
		}
		
		_assertVisible($ORDER_REVIEW_ORDER_TOTAL_TABLE_SUBTOTAL_ROW);
		_assertVisible($ORDER_REVIEW_ORDER_TOTAL_TABLE_SHIPPING_ROW);
		_assertVisible($ORDER_REVIEW_ORDER_TOTAL_TABLE_SALESTAX_ROW);
		_assertVisible($ORDER_REVIEW_ORDER_TOTAL_TABLE_TOTALAMOUNT_ROW);
		
//		_assertEqual($OrderSummuary[13][0], _getText(_cell("/(.*)/", _in($ORDER_REVIEW_ORDER_TOTAL_TABLE_SUBTOTAL_ROW))));
//		_assertEqual("/"+$OrderSummuary[14][0]+"/", _getText(_cell("/(.*)/", _in($ORDER_REVIEW_ORDER_TOTAL_TABLE_SHIPPING_ROW))));
//		_assertEqual($OrderSummuary[15][0], _getText(_cell("/(.*)/", _in($ORDER_REVIEW_ORDER_TOTAL_TABLE_SALESTAX_ROW))));
//		_assertEqual($OrderSummuary[16][0], _getText(_cell("/(.*)/", _in($ORDER_REVIEW_ORDER_TOTAL_TABLE_TOTALAMOUNT_ROW))));

		_assertEqual("Subtotal", _getText(_cell("/(.*)/", _in($ORDER_REVIEW_ORDER_TOTAL_TABLE_SUBTOTAL_ROW))));
		_assertEqual("Shipping"+" "+$shippingMethod, _getText(_cell("/(.*)/", _in($ORDER_REVIEW_ORDER_TOTAL_TABLE_SHIPPING_ROW))));
		_assertEqual("Sales Tax", _getText(_cell("/(.*)/", _in($ORDER_REVIEW_ORDER_TOTAL_TABLE_SALESTAX_ROW))));
		if (_isVisible(_row("/order-shipping-discount/"))) 
		{
			_assertEqual("Shipping Discount", _getText(_cell("/(.*)/", _in(_row("/order-shipping-discount/")))));
		}
		_assertEqual("/Order Total/", _getText(_cell("/(.*)/", _in($ORDER_REVIEW_ORDER_TOTAL_TABLE_TOTALAMOUNT_ROW))));

		//image
		_assertVisible($CART_IMAGE_CELL);
		//name
		_assertVisible($SHIPPING_PRODUCT_NAME);
		//qty
		_assertVisible($CART_QUANTITY_CELL);	
		//total price
		_assertVisible($CART_ITEM_TOTAL_CELL);
//		//edit link
//		_assertVisible($ORDER_REVIEW_EDIT_CART_LINK);
		//ORDER SUMMARY section
		if (!isMobile()) 
		{
			_assertVisible($ORDER_REVIEW_SUBTOTAL);
			_assertVisible($ORDER_REVIEW_SHIPPING);
			_assertVisible($ORDER_REVIEW_SALES_TAX);
			_assertVisible($ORDER_REVIEW_ORDER_TOTAL);
			_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING);
			_assertVisible($SHIPPING_RIGHT_NAV_SALES_TAX);
			//Order Summary 
			_assertVisible($SHIPPING_RIGHT_NAV_HEADING);
		}
		
		_assertVisible($SHIPPING_RIGHT_NAV_ORDER_TOTAL);
		
		_assertVisible($CART_SUBTOTAL);
		_assertVisible(_submit("submit"));

		//shipping address
		_assertVisible($ORDER_SUMMARY_SHIPPING_ADDRESS_HEADING);
//		_assertEqual($OrderSummuary[7][0], _getText($ORDER_SUMMARY_SHIPPING_ADDRESS_HEADING));
		_assertEqual("Edit Shipping Address", _getText($ORDER_SUMMARY_SHIPPING_ADDRESS_HEADING));
		_assertVisible($ORDER_SUMMARY_SHIPPING_ADDRESS_DETAILS);
		//billing address
		_assertEqual("Edit Billing Address", _getText($ORDER_REVIEW_ORDER_SUMMARY_BILLING_HEADING));
		_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_BILLING_DETAILS);
		//payment detais
		_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_PAYMENT_DETAILS);
		_assertEqual("Edit Payment Method", _getText($ORDER_REVIEW_ORDER_SUMMARY_PAYMENT_HEADING));
		
//		//Help section
//		_assertVisible($SHIPPING_ORDER_SUMMARY_HELP_SECTION);
//		_assertEqual($OrderReview[10][1], _getText($SHIPPING_ORDER_SUMMARY_HELP_SECTION));
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();

	var $t=_testcase("125831","Verify the active &amp; de-active Sections on Summary Page in Application as a Guest user");
//	var $t=_testcase(""," ");
	$t.start();
	try
	{
//		fetch the count of active tabs	
		var $tabs=_collect("_link","/(.*)/", _in(_div("/checkout-progress-indicator/")));
		_log("$tabs.length:-  "+ $tabs.length);
		for(var $i=0; $i < $tabs.length; $i++)
		{
			_log("LOOP:-  "+ $i);
			_assertEqual($OrderReview[$i][9], _getText($tabs[$i]));
		}
		//Verify the Inactive and active tabs
		_assertEqual($OrderReview[0][9], _getText($CHECKOUT_STEP1_INACTIVE_BREADCRUMB));
		_assertEqual($OrderReview[1][9], _getText($CHECKOUT_STEP2_INACTIVE_BREADCRUMB));
		_assertEqual($OrderReview[2][9], _getText($CHECKOUT_STEP3_ACTIVE_BREADCRUMB));
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();

	
//	var $t=_testcase("148926","Verify the details to be displayed for the cart line items on Summary Page in Application as a Anonymous user.");
	var $t=_testcase("352604","Verify the details to be displayed for the cart line items on Summary Page in Application as a Anonymous user.");
	$t.start();
	try
	{
		//verifying the cart line items
		_assertVisible($CART_NAME_LINK);
		_assertEqual($productNameInPDP, _getText($CART_NAME_LINK));
		//_assertVisible(_image("/(.*)/",_in(_row("cart-row  first "))));
		
		var $subTotalInConfirmationPage = parseFloat(_extract(_getText(_cell("item-total")),"/[$](.*)/", true));
		_assertEqual($subTotalInConfirmationPage,parseFloat(_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SUBTOTAL),"/[$](.*)/",true)));
		if(!isMobile())
		{
			_assertVisible($CART_QUANTITY_HEADING);
			_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_SUBTOTAL);
		}
		_assertVisible($CART_QUANTITY_CELL);
//		_assertEqual($productQuantityInPDP,_extract(_getText($CART_QUANTITY_CELL),"/(.*)[ ]/",true));
		var $expectedQty;
		if (_getText($CART_QUANTITY_CELL).length > 3) 
		{
			$expectedQty = _extract(_getText($CART_QUANTITY_CELL), "/(.*)[ ]/", true);
		}
		else 
		{
			$expectedQty = _getText($CART_QUANTITY_CELL);
		}
		
		_assertEqual($productQuantityInPDP,$expectedQty);
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();

//	var $t=_testcase("125827","Verify the 'ORDER SUMMARY'Section in the right nav on Summary Page in Application as a Anonymous user.");
	var $t=_testcase("352605","Verify the 'ORDER SUMMARY' Section in the right Nav on Summary Page in Application as a Registered user.");
	$t.start();
	try
	{
		//Verify the 'ORDER SUMMARY'section
		if (!isMobile()) 
		{
			_assertVisible($ORDER_REVIEW_SUBTOTAL);
			_assertVisible($ORDER_REVIEW_SHIPPING);
			_assertVisible($ORDER_REVIEW_SALES_TAX);
			_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_ORDER_TOTAL);
		}
		
		_assertEqual($subTotal,_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SUBTOTAL),"/[$](.*)/",true).toString());
		if ($shippingtemp) 
		{
			_assertEqual($shippingMethodPrice,_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING),"/[$](.*)/",true).toString());
		}
		_assertEqual($tax,_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SALES_TAX),"/[$](.*)/",true).toString());
		var $discountAmount = 0;
		if (_isVisible(_row("/order-shipping-discount/"))) 
		{
			$discountAmount = parseFloat(_extract(_getText(_row("/order-shipping-discount/")), "/[$](.*)/", true));	
			_log("$discountAmount:-  "+ $discountAmount);
		}
		
		var $expectedOrderToT = parseFloat($subTotal)+parseFloat($shippingTax)+parseFloat($tax)-$discountAmount;
		_log("$expectedOrderToT:-  "+ $expectedOrderToT);
		
		_assertEqual($expectedOrderToT,_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_ORDER_TOTAL),"/[$](.*)/",true).toString());
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();

//	var $t=_testcase("125833/288204","Verify the details to be displayed in the Shipping address Section/ in right nav on Summary Page in Application as a Anonymous user.");
	var $t=_testcase("352606","Verify the details to be displayed in the Shipping address Section/ in right nav on Summary Page in Application as a Anonymous user.");
	$t.start();
	try
	{
		//verifying the shipping address
		_assertVisible($ORDER_SUMMARY_SHIPPING_ADDRESS_HEADING);
		_assertVisible($ORDER_SUMMARY_SHIPPING_ADDRESS_DETAILS);
		var $ShippingAddress=_getText($ORDER_SUMMARY_SHIPPING_ADDRESS_DETAILS).replace(",","");
		_log("$ShippingAddress:-  "+ $ShippingAddress);
		_assertEqual($billingAddressWithoutPhoneNum+" "+"Method:"+" "+$shippingMethod,$ShippingAddress);
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();

//	var $t=_testcase("125837/288206","Verify the details to be displayed in the Billing address Section in right nav on Summary Page in Application as a Anonymous user.");
	var $t=_testcase("352611","Verify the details to be displayed in the Billing address Section in right nav on Summary Page in Application as a Anonymous user.");
	$t.start();
	try
	{
		//verifying the billing address
		_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_BILLING_DETAILS);
		var $BillingAddress = _getText($ORDER_REVIEW_ORDER_SUMMARY_BILLING_DETAILS).replace(",","");
		_assertEqual($billingAddressWithoutPhoneNum, $BillingAddress);
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();


//	var $t=_testcase("125839/288212","Verify the details to be displayed in the Payment method Section in right nav on Summary Page in Application as a Anonymous user.");
	var $t=_testcase("352614","Verify the details to be displayed in the Payment method Section in right nav on Summary Page in Application as a Anonymous user.");
	$t.start();
	try
	{
		//verifying the payment details section
		_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_PAYMENT_HEADING);
		_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_PAYMENT_DETAILS);
		var $payment_Details=_getText($ORDER_REVIEW_ORDER_SUMMARY_PAYMENT_DETAILS);
		_log("$payment_Details:-  "+ $payment_Details);
		var $ExpCreditCard_Details=$OrderReview[1][1]+" "+$Valid_Data_orderreview[$rowNum][7]+" "+$OrderReview[3][1]+" $"+$Ordertotal;
		_assertEqual($ExpCreditCard_Details,$payment_Details);
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();

//	var $t=_testcase("125841/288264/288287","Verify the Edit link functionality for shipping address Section in right nav/UI of the Shipping Accordion/if the Use this a billing address selection is not changed when reached through Order Review page on Summary Page in Application as a Anonymous user.");
	var $t=_testcase("352603","Verify the Edit link functionality for shipping address Section in right nav/UI of the Shipping Accordion/if the Use this a billing address selection is not changed when reached through Order Review page on Summary Page in Application as a Anonymous user.");
	$t.start();
	try
	{
		//clicking on edit link present in shipping address section
		_click($ORDER_SUMMARY_SHIPPING_ADDRESS_EDIT_LINK);
		//verify the navigation
		_assertVisible($SHIPPING_PAGE_HEADER);
		//Verify the breadcrumb
		//Verify the Inactive and active tabs
		_assertEqual($OrderReview[0][9], _getText($CHECKOUT_STEP1_ACTIVE_BREADCRUMB));
		_assertEqual($OrderReview[1][9], _getText($CHECKOUT_STEP2_INACTIVE_BREADCRUMB));
		_assertEqual($OrderReview[2][9], _getText($CHECKOUT_STEP3_INACTIVE_BREADCRUMB));
		
		//Use this address checkbox should be unchecked
		_assertTrue($SHIPPING_BILLING_ADDRESS_CHECKBOX.checked);
		//select shipping method section
		_assertVisible($SHIPPING_METHOD_HEADING);
		//summary section
		_assertVisible($SHIPPING_RIGHT_NAV_HEADING);
		//shipping address
		_assertVisible($ORDER_SUMMARY_SHIPPING_ADDRESS_HEADING);
		_assertVisible($ORDER_SUMMARY_SHIPPING_ADDRESS_DETAILS);
		//billing address
		_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_BILLING_HEADING);
		_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_BILLING_DETAILS);
		//payment detais
		_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_PAYMENT_HEADING);
		_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_PAYMENT_DETAILS);	

	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();

//	var $t=_testcase("125843/288309","Verify the Edit link functionality for the billing address Section in right nav/Billing address values entered are pre populated when billing accordion is expanded on Summary Page in Application as a Anonymous user.");
	var $t=_testcase("352607","Verify the Edit link functionality for the billing address Section in right nav/Billing address values entered are pre populated when billing accordion is expanded on Summary Page in Application as a Anonymous user.");
	$t.start();
	try
	{
		//navigate to summuary page
		shippingAddress($Valid_Data_orderreview,0);
		_click($SHIPPING_SUBMIT_BUTTON);	
		
		BillingAddress($Valid_Data_orderreview,0); 
		//entering Credit card Information
		PaymentDetails($paypal,2);
		PaymentDetails($Valid_Data_orderreview,4);
		_click($BILLING_CONTINUE_BUTTON);
		
		//click on edit link present in billing section
		_click($ORDER_SUMMARY_BILLING_EDIT);
		//verify the navigation
		_assertVisible($BILLING_PAGE_HEADING);
		
		//Verify the breadcrumb
		_assertEqual($OrderReview[0][9], _getText($CHECKOUT_STEP1_INACTIVE_BREADCRUMB));
		_assertEqual($OrderReview[1][9], _getText($CHECKOUT_STEP2_ACTIVE_BREADCRUMB));
		_assertEqual($OrderReview[2][9], _getText($CHECKOUT_STEP3_INACTIVE_BREADCRUMB));
		
		//Billing address prepopulation
		_assertEqual($Valid_Data_orderreview[0][1],_getText($BILLING_FN_TEXTBOX));
		_assertEqual($Valid_Data_orderreview[0][2],_getText($BILLING_LN_TEXTBOX));
		_assertEqual($Valid_Data_orderreview[0][3],_getText($BILLING_ADDRESS1_TEXTBOX));
		_assertEqual($Valid_Data_orderreview[0][4],_getText($BILLING_ADDRESS2_TEXTBOX));
		_assertEqual($Valid_Data_orderreview[1][5],_getSelectedText($BILLING_COUNTRY_DROPDOWN));
		_assertEqual($Valid_Data_orderreview[0][6],_getSelectedText($BILLING_STATE_DROPDOWN));
		_assertEqual($Valid_Data_orderreview[0][7],_getText($BILLING_CITY_TEXTBOX));
		_assertEqual($Valid_Data_orderreview[0][8],_getText($BILLING_ZIPCODE_TEXTBOX));
		_assertEqual($Valid_Data_orderreview[0][9],_getText($BILLING_NUMBER_TEXTBOX));
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();
	


//	var $t=_testcase("125845/288328","Verify the Edit link functionality for Payment Method Section in right nav/user entered data in the payment section of billing page is pre populated on Summary Page in Application as a Anonymous user.");
	var $t=_testcase("352612","Verify the Edit link functionality for Payment Method Section in right nav/user entered data in the payment section of billing page is pre populated on Summary Page in Application as a Anonymous user.");
	$t.start();
	try
	{
		navigateToOrderReview();
		//click on edit link present in payment deatils section
		_click($ORDER_REVIEW_ORDER_SUMMARY_PAYMENT_HEADING_EDIT_LINK);
		//verify the navigation to billing page
		_assertVisible($BILLING_PAGE_HEADING);
		//payment details prepopulation
		_assertEqual($Valid_Data_orderreview[4][1], _getValue($BILLING_PAYMENT_OWNER_NAME_TEXTBOX));
		_assertEqual($Valid_Data_orderreview[4][2], _getSelectedText($BILLING_PAYMENT_CARD_TYPE_DROPDOWN));
		_assertEqual($Valid_Data_orderreview[4][8], _getValue($BILLING_PAYMENT_CARD_NUMBER_TEXTBOX));
		_assertEqual($Valid_Data_orderreview[4][4], _getSelectedText($BILLING_PAYMENT_EXPIRY_MONTH_TEXTBOX1));
		_assertEqual($Valid_Data_orderreview[4][5], _getSelectedText($BILLING_PAYMENT_EXPIRY_YEAR_TEXTBOX));
		_assertEqual($Valid_Data_orderreview[4][10], _getValue($BILLING_PAYMENT_CVN_TEXTBOX));
		
//		_assertEqual("Payment", _getText($CHECKOUT_OPENED_SECTION_HEADING));
		_assertVisible($BILLING_CONTINUE_BUTTON);
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();


//	var $t=_testcase("125847","Verify the Edit link displayed in Order Summary Section in the SUMMARY Section for Anonymous user.");
	var $t=_testcase("125847","Verify the Edit link displayed in Order Summary Section in the SUMMARY Section for Anonymous user.");
	$t.start();
	try
	{
		//navigate to summary page
		navigateToOrderReview();
		//click on edit link present in order Summuary section
		_click(_link("section-header-note", _in(_div("secondary"))));
		//cart navigation verification
		_assertVisible($CART_TABLE);
		_assertVisible($CART_ACTIONBAR);
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();

	var $t=_testcase("352601","Verify the functionality of 'Edit cart' link next to SUBMIT ORDER button on Summary Page in Application.");
	$t.start();
	try
	{
		navigateToOrderReview();
		//verify the Edit cart link
		_assertEqual($OrderReview[13][0], _getText($ORDER_REVIEW_EDIT_CART_LINK));
		//click on edit cart link
		_click($ORDER_REVIEW_EDIT_CART_LINK);
		//cart navigation verification
		_assertVisible($CART_TABLE);
		_assertVisible($CART_ACTIONBAR);
		
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();
	
//	var $t=_testcase("288225","Verify the UI of the Sign in section when expanded from Order Review page as a guest user.");
	var $t=_testcase("288225","Verify the UI of the Sign in section when expanded from Order Review page as a guest user.");
	$t.start();
	try
	{
		//Click on Checkout button in cart page
		_click($MINICART_CHECKOUT);
		
		_assertNotVisible($SHIPPING_PAGE_HEADER);
		//Verify the Guests or New Customers heading
		_assertEqual($IMLogin_Data[0][6], _getText(_heading2("login-heading")));
		_assertEqual($IMLogin_Data[1][6], _getText(_paragraph("/(.*)/", _in(_div("/login-box-content/")))));
		//UI of Sign in section
		_assertEqual($IMLogin_Data[4][6], _getText($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON));
		
		//Create account section
		_assertEqual($IMLogin_Data[3][6], _getText(_span("/(.*)/", _in(_submit("dwfrm_login_register")))));

		//Log in section
		_assertVisible($CHECKOUT_LOGINPAGE_LOGIN_SECTION);
		_assertEqual($IMLogin_Data[2][6], _getText(_div("login-heading")));
		
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();
	
//	var $t=_testcase("288233/288278","Verify if the user is able to expand the Shipping accordion when/shipping address form is pre filled in SPC Order review page as a guest user.");
	var $t=_testcase("","Verify if the user is able to expand the Shipping accordion when/shipping address form is pre filled in SPC Order review page as a guest user.");
	$t.start();
	try
	{
		navigateToOrderReview();
		//Click on Shipping in breadcrumb
		_click(_link("/Shipping/", _in(_div("breadcrumb checkout-progress-indicator"))));
		//Values prepopulated
		_assertEqual($Valid_Data_orderreview[0][1], _getValue($SHIPPING_FN_TEXTBOX));
		_assertEqual($Valid_Data_orderreview[0][2], _getValue($SHIPPING_LN_TEXTBOX));
		_assertEqual($Valid_Data_orderreview[0][3], _getValue($SHIPPING_ADDRESS1_TEXTBOX));
		_assertEqual($Valid_Data_orderreview[0][7], _getValue($SHIPPING_CITY_TEXTBOX));
		_assertEqual($Valid_Data_orderreview[0][8], _getValue($SHIPPING_ZIPCODE_TEXTBOX));
		_assertEqual($Valid_Data_orderreview[1][5], _getSelectedText($SHIPPING_COUNTRY_DROPDOWN));
		_assertEqual($Valid_Data_orderreview[0][6], _getSelectedText($SHIPPING_STATE_DROPDOWN));
		_assertEqual($Valid_Data_orderreview[0][9], _getValue($SHIPPING_PHONE_TEXTBOX));
		//Billing section should be closed
		_assertNotVisible($BILLING_PAGE_HEADING);
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();
	
	ClearCartItems();
//	var $t=_testcase("288297","Verify if the user selected shipping method is not altered when shipping accordion is loaded from order review page.");
	var $t=_testcase("","Verify if the user selected shipping method is not altered when shipping accordion is loaded from order review page.");
	$t.start();
	try
	{
		navigateToShippingPage($Order_Product[0][1],$Order_Product[0][2]);
		shippingAddress($Valid_Data_orderreview,0);
		//No of shipping methods
		var $TotalShippingMethods=_count("_radio","/shipping-method/");
		//Last shipping method
		$TotalShippingMethods=$TotalShippingMethods-1;
		_click(_radio("input-radio["+$TotalShippingMethods+"]", _in($SHIPPING_METHOD_HEADING)));
		_click($SHIPPING_SUBMIT_BUTTON);
		//address verification overlay
		
		BillingAddress($Valid_Data_orderreview,0);
		PaymentDetails($Valid_Data_orderreview,4);
		_wait(1000);
		_click($BILLING_CONTINUE_BUTTON);
		//Address Verification overlay
		
		//Navigate to SHipping page to verify the Shipping method
		//Click on SHIPPING heading
		_click(_link("/Shipping/", _in(_div("breadcrumb checkout-progress-indicator"))));
		//Verify the shipping method selected
		_assert(_radio("input-radio["+$TotalShippingMethods+"]", _in($SHIPPING_METHOD_HEADING)).checked);
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();
	
//	var $t=_testcase("288269","Verify the UI of the Payment acordion wth expanded from Order review page as a guest user.");
	var $t=_testcase("","Verify the UI of the Payment acordion wth expanded from Order review page as a guest user.");
	$t.start();
	try
	{
		navigateToOrderReview();
		//click on Billing accordian
		_click(_link("Billing", _in(_div("breadcrumb checkout-progress-indicator"))));
		//UI of billing page
		//Billing form Heading
		_assertVisible($BILLING_PAGE_HEADING);
		//Payment fields
		_assertVisible($BILLING_PAYMENT_HEADING);
		//Order summary section
		_assertVisible($SHIPPING_RIGHT_NAV_HEADING);
		//shipping address
		_assertVisible($ORDER_SUMMARY_SHIPPING_ADDRESS_HEADING);
		_assertVisible($ORDER_SUMMARY_SHIPPING_ADDRESS_DETAILS);
		//billing address
		_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_BILLING_HEADING);
		_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_BILLING_DETAILS);
		//payment detais
		_assertNotVisible($ORDER_REVIEW_ORDER_SUMMARY_PAYMENT_HEADING);
		_assertNotVisible($ORDER_REVIEW_ORDER_SUMMARY_PAYMENT_DETAILS);	
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();

//	var $t=_testcase("125829","Verify the functionality of 'Place Order' button on Summary Page in Application as a Anonymous user.");
	var $t=_testcase("352610","Verify the functionality of 'Place Order' button on Summary Page in Application as a Anonymous user.");
	$t.start();
	try
	{
		navigateToOrderReview();
		//Click on Place Order
		_click(_submit("submit"));
		if(_isVisible($BILLING_ADDRESS_CORRECTION_CLOSE_LINK))
		{
			_click($BILLING_ADDRESS_CORRECTION_CLOSE_LINK);
		}
		//verify the navigation to thank you page
		_assertVisible(_heading1($OrderReview[0][5]));
//		_assertVisible($ORDER_CONFIRMATION_ORDER_NUMBER);
//		_assertVisible($ORDER_CONFIRMATION_ORDER_NUMBER_HEADING);
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();
	
	ClearCartItems();
	
	
	var $t=_testcase("352613","Verify the functionality of progress indicator on Order Review page in Application.");
	$t.start();
	try
	{
		//Navigation till shipping page
		navigateToShippingPage($Order_Product[0][1],$Order_Product[0][2]);
		//Verify the breadcrumb
		_assertNotVisible($CHECKOUT_STEP1_INACTIVE_BREADCRUMB);
		_assertNotVisible($CHECKOUT_STEP2_ACTIVE_BREADCRUMB);
		_assertNotVisible($CHECKOUT_STEP3_INACTIVE_BREADCRUMB);
		
		_assertEqual($OrderReview[0][9], _getText(_div("step-1 active", _in($CHECKOUT_PROGRESS_INDICATOR))));
		_assertEqual($OrderReview[1][9], _getText(_div("step-2 inactive", _in($CHECKOUT_PROGRESS_INDICATOR))));
		_assertEqual($OrderReview[2][9], _getText(_div("step-3 inactive", _in($CHECKOUT_PROGRESS_INDICATOR))));
		
		//Enter shipping address
		shippingAddress($Valid_Data_orderreview,0);
		if(isMobile() && !mobile.iPad())
		{
			$shippingTax=_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING),"/[$](.*)/",true).toString();
			$subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_PRICE_MOBILE),"/[$](.*)/",true).toString();
		}
		else
		{
			$shippingTax=_extract(_getText($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING),"/[$](.*)/",true).toString();
			$subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_PRICE),"/[$](.*)/",true).toString();
		}
		_log("$shippingTax:-  "+ $shippingTax);
		_log("$subTotal:-  "+ $subTotal);
		
		//Collect the shipping method 
		var $radioButtons = _collect("_radio","/(.*)/", _in(_div("shipping-method-list")));
		var $shippingMethodsLabel = _collect("_div","/label-inline/", _in(_div("shipping-method-list")));
//			_collect("_label","radio-label", _in(_div("shipping-method-list")));
		for (var $k = 0; $k < $radioButtons.length; $k++) 
		{
			var $radioButtonStatus = _getAttribute($radioButtons[$k], "checked");
			_log("$radioButtonStatus:-  "+ $radioButtonStatus);
			if ("true" == $radioButtonStatus || true == $radioButtonStatus ) 
			{
				var $shippingMethodPrice;
				if (_isVisible(_span("discount-shipping", _in($shippingMethodsLabel[$k])))) 
				{
					$shippingMethodPrice = _getText(_span("discount-shipping", _in($shippingMethodsLabel[$k])));
				}
				else if (_isVisible(_span("standard-shipping", _in($shippingMethodsLabel[$k])))) 
				{
					$shippingMethodPrice = _getText(_span("standard-shipping", _in($shippingMethodsLabel[$k])));
				}
				_log("$shippingMethodPrice:- "+ $shippingMethodPrice);
				var $label = _getText(_label("/(.*)/", _in($shippingMethodsLabel[$k])));
				var $shippingMethod = $label.substring(0, $label.indexOf(":"));
				_log("$shippingMethod:-  "+ $shippingMethod);
				break;
			}
		}
		//Click on CONTINUE button in Shipping page
		_click($SHIPPING_SUBMIT_BUTTON);
		//Verify the breadcrumb
		_assertEqual($OrderReview[0][9], _getText($CHECKOUT_STEP1_INACTIVE_BREADCRUMB));
		_assertNotVisible($CHECKOUT_STEP3_INACTIVE_BREADCRUMB);
		_assertEqual($OrderReview[1][9], _getText($CHECKOUT_STEP2_ACTIVE_BREADCRUMB));
		_assertEqual($OrderReview[2][9], _getText(_div("step-3 inactive", _in($CHECKOUT_PROGRESS_INDICATOR))));

		//Enter Billing address
		BillingAddress($Valid_Data_orderreview,0); 
		$tax=_extract(_getText($SHIPPING_RIGHT_NAV_SALES_TAX),"/[$](.*)/",true).toString();
		$Ordertotal=_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/[$](.*)/",true).toString();
		//entering Credit card Information
		var $rowNum = 4;
		PaymentDetails($Valid_Data_orderreview,$rowNum);
		$paymentType="Credit Card";
		_wait(1000);
		//Click on Continue button in Billing page
		_click($BILLING_CONTINUE_BUTTON);
		
		//Verify the breadcrumb
		_assertEqual($OrderReview[0][9], _getText($CHECKOUT_STEP1_INACTIVE_BREADCRUMB));
		_assertEqual($OrderReview[1][9], _getText($CHECKOUT_STEP2_INACTIVE_BREADCRUMB));
		_assertNotVisible($CHECKOUT_STEP3_ACTIVE_BREADCRUMB);
		_assertEqual($OrderReview[2][9], _getText(_div("step-3 active", _in($CHECKOUT_PROGRESS_INDICATOR))));
		
		if(!isMobile() || mobile.iPad())
		{
			//varify the navigation & UI of Order Summuary
			_assertVisible($CART_PRODUCT_HEADING);
			_assertVisible($CART_QUANTITY_HEADING);
			_assertVisible($CART_TOTAL_HEADING);
		}
		
		_assertVisible($ORDER_REVIEW_ORDER_TOTAL_TABLE_SUBTOTAL_ROW);
		_assertVisible($ORDER_REVIEW_ORDER_TOTAL_TABLE_SHIPPING_ROW);
		_assertVisible($ORDER_REVIEW_ORDER_TOTAL_TABLE_SALESTAX_ROW);
		_assertVisible($ORDER_REVIEW_ORDER_TOTAL_TABLE_TOTALAMOUNT_ROW);
		
//		_assertEqual($OrderSummuary[13][0], _getText(_cell("/(.*)/", _in($ORDER_REVIEW_ORDER_TOTAL_TABLE_SUBTOTAL_ROW))));
//		_assertEqual("/"+$OrderSummuary[14][0]+"/", _getText(_cell("/(.*)/", _in($ORDER_REVIEW_ORDER_TOTAL_TABLE_SHIPPING_ROW))));
//		_assertEqual($OrderSummuary[15][0], _getText(_cell("/(.*)/", _in($ORDER_REVIEW_ORDER_TOTAL_TABLE_SALESTAX_ROW))));
//		_assertEqual($OrderSummuary[16][0], _getText(_cell("/(.*)/", _in($ORDER_REVIEW_ORDER_TOTAL_TABLE_TOTALAMOUNT_ROW))));

		_assertEqual("Subtotal", _getText(_cell("/(.*)/", _in($ORDER_REVIEW_ORDER_TOTAL_TABLE_SUBTOTAL_ROW))));
		_assertEqual("Shipping"+" "+$shippingMethod, _getText(_cell("/(.*)/", _in($ORDER_REVIEW_ORDER_TOTAL_TABLE_SHIPPING_ROW))));
		_assertEqual("Sales Tax", _getText(_cell("/(.*)/", _in($ORDER_REVIEW_ORDER_TOTAL_TABLE_SALESTAX_ROW))));
		if (_isVisible(_row("/order-shipping-discount/"))) 
		{
			_assertEqual("Shipping Discount", _getText(_cell("/(.*)/", _in(_row("/order-shipping-discount/")))));
		}
		_assertEqual("/Order Total/", _getText(_cell("/(.*)/", _in($ORDER_REVIEW_ORDER_TOTAL_TABLE_TOTALAMOUNT_ROW))));

		//image
		_assertVisible($CART_IMAGE_CELL);
		//name
		_assertVisible($SHIPPING_PRODUCT_NAME);
		//qty
		_assertVisible($CART_QUANTITY_CELL);	
		//total price
		_assertVisible($CART_ITEM_TOTAL_CELL);
//		//edit link
//		_assertVisible($ORDER_REVIEW_EDIT_CART_LINK);
		//ORDER SUMMARY section
		if (!isMobile()) 
		{
			_assertVisible($ORDER_REVIEW_SUBTOTAL);
			_assertVisible($ORDER_REVIEW_SHIPPING);
			_assertVisible($ORDER_REVIEW_SALES_TAX);
			_assertVisible($ORDER_REVIEW_ORDER_TOTAL);
			_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_SHIPPING);
			_assertVisible($SHIPPING_RIGHT_NAV_SALES_TAX);
			//Order Summary 
			_assertVisible($SHIPPING_RIGHT_NAV_HEADING);
		}
		
		_assertVisible($SHIPPING_RIGHT_NAV_ORDER_TOTAL);
		
		_assertVisible($CART_SUBTOTAL);
		_assertVisible(_submit("submit"));

		//shipping address
		_assertVisible($ORDER_SUMMARY_SHIPPING_ADDRESS_HEADING);
		_assertEqual("Edit Shipping Address", _getText($ORDER_SUMMARY_SHIPPING_ADDRESS_HEADING));
		_assertVisible($ORDER_SUMMARY_SHIPPING_ADDRESS_DETAILS);
		//billing address
		_assertEqual("Edit Billing Address", _getText($ORDER_REVIEW_ORDER_SUMMARY_BILLING_HEADING));
		_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_BILLING_DETAILS);
		//payment detais
		_assertVisible($ORDER_REVIEW_ORDER_SUMMARY_PAYMENT_DETAILS);
		_assertEqual("Edit Payment Method", _getText($ORDER_REVIEW_ORDER_SUMMARY_PAYMENT_HEADING));
		
//		//Help section
//		_assertVisible($SHIPPING_ORDER_SUMMARY_HELP_SECTION);
//		_assertEqual($OrderReview[10][1], _getText($SHIPPING_ORDER_SUMMARY_HELP_SECTION));
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();
	
	
	

	ClearCartItems();
	SiteURLs();
	var $t=_testcase("125835","Verify the details to be displayed in the Shipping address Section in right nav when Anonymous user selects the Multiple shipping address on Summary Page in Application");
	$t.start();
	try
	{
		var $proName = new Array();
		navigateToShippingPage($OrderReview[0][7],$OrderReview[0][8]);
		$proName.push($productNameInPDP);
		_log("$proName:- "+ $proName);
		navigateToShippingPage($OrderReview[1][7],$OrderReview[0][8]);
		$proName.push($productNameInPDP);
		_log("$proName:- "+ $proName);
		
		//ship to multiple addreses
		_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
//						_click(_submit("dwfrm_singleshipping_shipToMultiple"));
		var $addrCount=0;
		//adding addresses
		var $addAddressButton = _collect("_span","edit", _in(_table("item-list")));
		for(var $i=0;$i<$addAddressButton.length;$i++)
		{
			_click($addAddressButton[$i]);
			addAddressMultiShip($Address,$i);
			$addrCount++;
		}
		//Select Addresses from each drop down
		var $j=1;
		for(var $k=0;$k<$addAddressButton.length;$k++)
		{
			_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$k+"_addressList"),$j);
			$j++;
		}
		//selecting addresses
		var $itemCount=_count("_select", "/dwfrm_multishipping_addressSelection_quantityLineItems/",_in($SHIPPING_CART_ITEMS));
		var $c1=1;
		var $j=1;
		for(var $i=0;$i<$itemCount;$i++)
		{   
			if($c1>$addrCount)
			{
				$c1=1;
			}
			$c1++; 
			$j=$j+2;
		}
		//click on continue
		_click($SHIPPING_MULTIPLE_ADDRESS_SAVE_BUTTON);
		//Navigating to billing
		_click($BILLING_MULTI_SHIPPING_CONTINUE_BUTTON);
		BillingAddress($Valid_Data_orderreview,0); 
		//entering Credit card Information
//		PaymentDetails($paypal,2);
		PaymentDetails($Valid_Data_orderreview,4);
		_click($BILLING_CONTINUE_BUTTON);

		
		//Verify the bread crumb
		var $inActiveBreadCrumb = _collectAttributes("_div","/inactive/","sahiText",_in(_div("/checkout-progress-indicator/")));
		for (var $i = 0; $i < $inActiveBreadCrumb.length; $i++) 
		{
			_assertEqual($OrderReview[$i][11], $inActiveBreadCrumb[$i]);
		}
		//Verify the active breadcrumb
		_assertEqual($OrderReview[3][11], _getText(_link("/(.*)/", _in(_div("step-4 active")))));
		
		
		var $shipmentLabel = _collect("_div","shipment-label", _in(_table("cart-table")));
		var $j =1;
		for (var $i = 0; $i < $shipmentLabel.length; $i++) 
		{
			_assertEqual("Shipment No. "+$j, _getText($shipmentLabel[$i]));
			$j++;
		}
		var $productNames = _collect("_div","name", _in(_table("cart-table")));
		for (var $i = 0; $i < $productNames.length; $i++) 
		{
			//Verify the product name
			_assertEqual($proName[$i], _getText($productNames[$i]));
		}
		
		var $shippingAddressSection = _collect("_div","/mini-shipment/", _in(_div("secondary")));
		for (var $i = 0; $i < $shippingAddressSection.length; $i++) 
		{
			_assertEqual($Address[$i][12], _getText(_div("address", _in($shippingAddressSection[$i]))));
			_assertEqual("Edit Shipping Address", _getText(_heading3("section-header", _in($shippingAddressSection[$i]))));
		}
		
	}
	catch($e)
	{      
		_logExceptionAsFailure($e);
	}
	$t.end();


	cleanup();

	

//with existing data
function navigateToOrderReview()
{
	_click($MINICART_CHECKOUT);
	_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
	_click($SHIPPING_SUBMIT_BUTTON);
	//address verification over lay
	
	//entering Credit card Information
	PaymentDetails($Valid_Data_orderreview,4);
	_wait(1000);
	_click($BILLING_CONTINUE_BUTTON);
	//Address Verification overlay
	
}