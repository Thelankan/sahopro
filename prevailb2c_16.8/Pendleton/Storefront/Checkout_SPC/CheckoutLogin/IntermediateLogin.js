_include("../../../GenericLibrary/GlobalFunctions.js");

var $color=$ItemILP[0][2];
var $emailID=$uId;

//for deleting user
deleteUser();
_setAccessorIgnoreCase(true); 
SiteURLs();
cleanup();

//**
//verifies whether five page or single page checkout
 var $t = _testcase("352693", "Verify the navigation to 'Checkout- Intermediate Login' page from mini cart page in Application as an Anonymous user.");
$t.start();
try
{
	//navigate to cart page
	navigateToCartSearch($ItemILP[0][0],$ItemILP[0][1]);
	//click on go straight to checkout
	_click($CART_CHECKOUT_BUTTON_BOTTOM);
	//validating IM login page
	_assertVisible($CHECKOUT_LOGINPAGE_RETURNINGCUSTOMER_HEADING);
	_assertVisible($CHECKOUT_LOGINPAGE_NEWCUSTOMER_HEADING);
	_assertVisible($CHECKOUT_LOGINPAGE_LOGIN_SECTION);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("289927/289935/289947/352691/352708", "Verify UI of the Checkout as guest section and create account section/UI of the Returning customer section/UI of the Right Pane /UI of the Order Summary sectionof the Checkout login page as a guest user/display of tabs/UI of the Social Login section/load of Sign In Section on page load  in checkout Login Page as a guest user .");
$t.start();
try
{
	//Verifying the UI of returning customer section
	_assertEqual("Guests or New Customers", _getText(_heading2("login-heading")));
	
	//Returning customer heading
	_assertVisible($CHECKOUT_LOGINPAGE_RETURNINGCUSTOMER_HEADING);
	//Paragraph with static text
	_assertVisible($CHECKOUT_LOGINPAGE_RETURNUS_TEXT);
	//email Address
	_assertVisible($CHECKOUT_LOGINPAGE_EMAIL_ADDRESS_TEXT);
	_assertVisible($CHECKOUT_LOGINPAGE_EMAIL_ADDRESS_TEXTBOX);
	_assertEqual("", _getValue($CHECKOUT_LOGINPAGE_EMAIL_ADDRESS_TEXTBOX));
	//password
	_assertVisible($CHECKOUT_LOGINPAGE_PASSWORD_TEXT);
	_assertVisible($CHECKOUT_LOGINPAGE_LOGIN_PASSWORD_TEXTBOX);
	_assertEqual("", _getValue($CHECKOUT_LOGINPAGE_LOGIN_PASSWORD_TEXTBOX));
	//remember me
	_assertVisible($CHECKOUT_LOGINPAGE_LOGIN_REMEMBERMETEXT);
	_assertVisible($CHECKOUT_LOGINPAGE_REMEMBEMETEXTBOX);
	_assertNotTrue($CHECKOUT_LOGINPAGE_REMEMBEMETEXTBOX.checked);
	//login button
	_assertVisible($CHECKOUT_LOGINPAGE_LOGIN_SUBMIT_BUTTON);
	//forgot password
	_assertVisible($CHECKOUT_LOGINPAGE_PASSWORD_RESET);
	_assertEqual($IMLogin_Data[4][0], _getText($CHECKOUT_LOGINPAGE_PASSWORD_RESET));
	
	//UI of New customer section
	_assertVisible($CHECKOUT_LOGINPAGE_NEWCUSTOMER_HEADING);
	_assertVisible($CHECKOUT_LOGINPAGE_NEWCUSTOMER_PARAGRAPH1);
	_assertVisible($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
	_assertVisible($CHECKOUT_LOGINPAGE_NEWCUSTOMER_PARAGRAPH2);
	_assertVisible($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//**
var $t = _testcase("352709/352703", "Verify the UI of 'forgot password?' overlay on click of 'forgot password link' on Intermediate Login page in Application  as an Anonymous user/UI of the forgot Password overlay when launched from SPC login page");
$t.start();
try
{
	
//click on forgot password link
_click($CHECKOUT_LOGINPAGE_PASSWORD_RESET);
//verify the overlay appearance
_assertVisible($CHECKOUT_LOGINPAGE_PASSWORDRESET_DIALOG);
_assertVisible($CHECKOUT_LOGINPAGE_RESETPASSWORD_HEADING);
_assertVisible($CHECKOUT_LOGINPAGE_LOGIN_FORGETPASSWORD_TEXTBOX);
_assertVisible($CHECKOUT_LOGINPAGE_FORGETPASSWORD_SEND_BUTTON);
_assertVisible($CHECKOUT_LOGINPAGE_FORGETPASSWORD_CANCEL_BUTTON);
	
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end(); 
 

var $t = _testcase("352706/352704/352705/352701", "Verify the Validation of 'Email address' field in forgot password overlay/functionality of 'Forgot password' link on intermediate login page in Prevail application as an Anonymous user/UI of forgot password confirmation overlay/functionality of 'X(Close)' button of an overlay in Application  as an Anonymous user.");
$t.start();
try
{
	//validating the email id & password field's
	for(var $i=0;$i<$ForgotPassword_Validation.length;$i++)
		{

			if($i==5)
				{
					_setValue($CHECKOUT_LOGINPAGE_LOGIN_FORGETPASSWORD_TEXTBOX,$ForgotPassword_Validation[$i][1]);
				}
				else
					{
						_setValue($CHECKOUT_LOGINPAGE_LOGIN_FORGETPASSWORD_TEXTBOX,$ForgotPassword_Validation[$i][1]);
						_click($CHECKOUT_LOGINPAGE_FORGETPASSWORD_SEND_BUTTON);
						_wait(2000);
					}
		
		if (isMobile())
			{
			_wait(3000);
			}
		//blank
		if($i==0)
			{
			//username
			_assertEqual($ForgotPassword_Validation[$i][4],_style($CHECKOUT_LOGINPAGE_LOGIN_FORGETPASSWORD_TEXTBOX,"border-color"));
			_assertVisible(_span($ForgotPassword_Validation[$i][3]));
			}
		
		//in valid email
		else if($i==1 || $i==2 || $i==3 || $i==4 ||$i==6 ||$i==7 ||$i==8)
			{
			_assertEqual($ForgotPassword_Validation[3][3], _getText($CHECKOUT_LOGINPAGE_FORGETPASSWORD_EMAIL_ERRORMESSAGE));
			}
		
			
		//More Number of characters in email field
		else if ($i==5)
			{
			_assertEqual($ForgotPassword_Validation[5][2],_getText($CHECKOUT_LOGINPAGE_LOGIN_FORGETPASSWORD_TEXTBOX).length);
			}
		//valid
		else
			{
			if (isMobile())
				{
			_wait(3000,_isVisible($MY_ACCOUNT_OPTIONS));
				}
			//verify the navigation
			_assertVisible(_div("Thank You!"));
			_assertVisible(_paragraph("We've sent you an email with a link to reset your password. It might take a few minutes to reach your inbox. Please check your junk mail or spam if you don�t receive it."));
			_assertVisible(_link("Start Shopping"));
			//closing the dialog
			_click($CHECKOUT_LOGINPAGE_FORGETPASSWORD_CANCEL_BUTTON);
			_wait(2000);
			_assertNotVisible($CHECKOUT_LOGINPAGE_PASSWORDRESET_DIALOG);
			}
		}
	
	
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352692","Verify the functionality of Checkout as Guest button in Intermediate Login page of SPC.");
$t.start();
try
{
	//click on go straight to checkout
	_assertVisible($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
	_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
	
	//verify the navigation
	_assertEqual("Step 1: Shipping", _getText(_div("step-1 active")));
	_assertVisible($SHIPPING_PAGE_HEADER);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//**
var $t = _testcase("126709/352697/352699/352698/352700","Verify the validation of 'Email address'/ 'Password' field and Verify the functionality of  'Login' button in the Intermediate Login page in Prevail application as an Anonymous user.");
$t.start();
try
{
	//navigate to Cart page
	_click($MINICART_LINK);
	_click($CART_CHECKOUT_BUTTON_BOTTOM);

	//validating the email id & password field's
	for(var $i=0;$i<$IMLogin_Validation.length;$i++)
	{
	if($i==5)
		{
		login();
		}
	else
		{
			_setValue($CHECKOUT_LOGINPAGE_EMAIL_ADDRESS_TEXTBOX,$IMLogin_Validation[$i][1]);
			_setValue($CHECKOUT_LOGINPAGE_LOGIN_PASSWORD_TEXTBOX,$IMLogin_Validation[$i][2]);
			_click($CHECKOUT_LOGINPAGE_LOGIN_SUBMIT_BUTTON);
		}
	//blank
	if($i==0)
		{
			//username
			_assertVisible(_span($IMLogin_Validation[$i][3]));
			_assertEqual($color,_style($CHECKOUT_LOGINPAGE_EMAIL_ADDRESS_TEXTBOX,"border-color"));		
			//password
			_assertVisible(_span($IMLogin_Validation[$i][4]));
			_assertEqual($color,_style($CHECKOUT_LOGINPAGE_LOGIN_PASSWORD_TEXTBOX,"border-color"));
		}
	//valid
	else if($i==5)
		{
			_assertVisible(_heading2("Logged-in as "+$FName+" "+$LName));
			_assertEqual("STEP 1: Shipping", _getText($CHECKOUT_SHIPPING_SECTION_ACTIVETAB));
			_assertVisible($SHIPPING_PAGE_HEADER);
		}
	else if($i==2)
		{
			_assertVisible(_span($IMLogin_Validation[$i][3]));
			_assertEqual($IMLogin_Validation[1][5],_style(_span($IMLogin_Validation[$i][3]),"color"));
			_assertEqual($IMLogin_Validation[0][5],_style($CHECKOUT_LOGINPAGE_EMAIL_ADDRESS_TEXTBOX,"border-color"));
		}
	//invalid 
	else
		{
			_assertVisible(_div($IMLogin_Validation[$i][3]));
		}	
	}
ClearCartItems();

//logout from application
_click($HEADER_USERACCOUNT_LINK);
_click($HEADER_LOGOUT_LINK);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//**
var $t = _testcase("352694","Verify the functionality of 'Create an Account' button in Intemediate Login page under New Customer section for Guest user");
$t.start();
try
{
	
//navigate to cart page
navigateToCartSearch($ItemILP[0][0],$ItemILP[0][1]);
//click on go straight to checkout to navigate to IL Page
_click($CART_CHECKOUT_BUTTON_BOTTOM);

_click($CHECKOUT_LOGINPAGE_CREATEACCOUNTNOW_BUTTON);
//verify the navigation to create account page
_assertVisible($REGISTER_HEADING);
_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
//fill all the details
createAccount();
//Shipping page header
_assertVisible($SHIPPING_PAGE_HEADER);
shippingAddress($OCP_Address,0);
//check use this for billing check box
_check($SHIPPING_BILLING_ADDRESS_CHECKBOX);


}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
