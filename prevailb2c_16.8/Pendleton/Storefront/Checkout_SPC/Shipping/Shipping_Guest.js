_include("../../../GenericLibrary/BM_Functions.js");
_include("../../../GenericLibrary/GlobalFunctions.js");


_resource("Shipping.xls");
//********************************************Pendleton**********************************************************************************
SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();
var $t = _testcase("352538","Verify the navigation to 'SHIPPING' page in Application as a guest user");
$t.start();
try
{	
	//add items to cart
	navigateToCartSearch($Shipping_Page[0][1],1);
	
	//click on go strait to checkout link
	_mouseOver($MINICART_LINK);
	_click($MINICART_CHECKOUT);
	_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
	//verify the navigation
	verifyNavigationToShippingPage();	
	_click(_link("Edit", _in(_div("secondary"))));
	_setValue(_numberbox("input-text"), "2");
	_wait(3000);
	_click($CART_UPDATE_BUTTON);
	_wait(3000);
	_click(_submit("button-fancy-large", _in(_div("cart-actions"))));
	_click($SHIPPING_GUEST);
	var $UpdatedCart=_extract(_getText(_div("mini-cart-pricing", _in(_div("checkout-mini-cart")))),"/[ ](.*)[ ]/",true);
	_assertEqual("2", $UpdatedCart);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
ClearCartItems();
var $t = _testcase("352505/352540/352512/352513","Verify the UI of the Shipping page for a guest user in the application.");
$t.start();
try
{
	//add items to cart
	navigateToCartSearch($Shipping_Page[0][1],2);
	_click($CART_CHECKOUT_BUTTON_BOTTOM);	
	_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
	//verify the navigation
	verifyNavigationToShippingPage();
	//verify the UI of shipping page
	//Heading
	_assertVisible($SHIPPING_PAGE_HEADER);
	//required msg
	_assertVisible($SHIPPING_REQUIRED);
	//first name
	_assertVisible($SHIPPING_FIRST_NAME);
	_assertVisible($SHIPPING_FN_TEXTBOX);
	//last name
	_assertVisible($SHIPPING_LAST_NAME);
	_assertVisible($SHIPPING_LN_TEXTBOX);
	//address1
	_assertVisible($SHIPPING_ADDRESS_ONE);
	_assertVisible($SHIPPING_ADDRESS1_TEXTBOX);
	//address2
	_assertVisible($SHIPPING_ADDRESS_TWO);
	_assertVisible($SHIPPING_ADDRESS2_TEXTBOX);
	//country
	_assertVisible($SHIPPING_COUNTRY);
	_assertVisible($SHIPPING_COUNTRY_DROPDOWN);
	//default value
	_click($SHIPPING_ADDRESS_TOOLTIP);
	//state
	_assertVisible($SHIPPING_STATE);
	_assertVisible($SHIPPING_STATE_DROPDOWN);
	//default value

	//city
	_assertVisible($SHIPPING_CITY);
	_assertVisible($SHIPPING_CITY_TEXTBOX);
	//zip
	_assertVisible($SHIPPING_ZIPCODE);
	_assertVisible($SHIPPING_ZIPCODE_TEXTBOX);
	//phone number
	_assertVisible($SHIPPING_PHONE_NUMBER);
	_assertVisible($SHIPPING_PHONE_TEXTBOX);
	_assertVisible($SHIPPING_PHONE_EXAMPLE);
	//add to address book
	_assertNotVisible($SHIPPING_ADDTOADDRESS);
	_assertNotVisible($SHIPPING_ADDTOADDRESS_CHECKBOX);
	//use this billing address for registered user
	_assertVisible($SHIPPING_BILLING_ADDRESS);
	_assertVisible($SHIPPING_BILLING_ADDRESS_CHECKBOX);
	_assertTrue($SHIPPING_BILLING_ADDRESS_CHECKBOX.checked);

	if(!isMobile() || mobile.iPad())
	{
		//tooltip links
		_assertVisible($SHIPPING_ADDRESS_TOOLTIP);
		_assertVisible($SHIPPING_PHONE_TOOLTIP);
	}
	//Is this a gift section
	_assertVisible($SHIPPING_GIFT_TEXT);
	_assertVisible(_radio("true"));
	_assertNotTrue(_radio("true").checked);
	_assertVisible(_radio("false"));
	_assert(_radio("false").checked);

	//_assertVisible(_radio("is-gift-yes"));
	//_assertNotTrue(_radio("is-gift-yes").checked);
	//_assertVisible(_radio("is-gift-no"));
	//_assert(_radio("is-gift-no").checked);

	//select shipping method section
	_assertVisible($SHIPPING_METHOD_HEADING);

	var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in($SHIPPING_METHOD_HEADING));
	for(var $i=0;$i<$totalShippingMethods;$i++)
	{
		//shipping methods
		_assertVisible(_div("form-row form-indent label-inline["+$i+"]"));
	}
	//continue button
	_assertVisible($SHIPPING_SUBMIT_BUTTON);
	//summary section
	_assertVisible($SHIPPING_RIGHT_NAV_HEADING);
	_assertVisible($SHIPPING_RIGHT_NAV);	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("352508/352537","Verify the display of 'Do you want to ship to multiple addresses?' when user has added single item to the cart from Checkout Shipping page in Application as a guest  user");
$t.start();
try
{
	
	//Ship to multiple addresses section should not be displayed
	_assertVisible($SHIPPING_MULTIPLE_ADDRESS);
	_assertVisible($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
	//click
	_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
	_assertNotVisible($SHIPPING_MULTIPLE_ADDRESS);
	//Click on Ship to Single Address yes button 
	_assertVisible($SHIPPING_SINGLE_ADDRESS_TEXT);
	_click($SHIPPING_SINGLE_ADDRESS_BUTTON);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
var $t = _testcase("128336","Verify the navigation of 'product name' in 'Do you want to ship to multiple addresses?' Section on shipping page in Prevail application as a Anonymous user");
$t.start();
try
{

	//fetch the product name
	var $productName=_getText($SHIPPING_SUMMARY_PRODUCT_NAME_MOBILE);
	//click on name link
	_click($SHIPPING_SUMMARY_PRODUCT_NAME_MOBILE);
	//verify the navigation to pdp page
	if(!isMobile())
	{
		_assertVisible($SHIPPING_BREADCRUMB);
	}
	_assertContainsText($productName, $SHIPPING_BREADCRUMB);
	_assertVisible($PDP_PRODUCTNAME);
	_assertEqual($productName, _getText($PDP_PRODUCTNAME));
	//navigate to shipping page
	//
	_click(_link("View Bag"));
	//_click($MINICART_OVERLAY_VIEWCART_LINK);
	_click($CART_CHECKOUT_BUTTON_BOTTOM);
	_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON); 
	//click on yes button to reset back
	//_click($SHIPPING_SINGLE_ADDRESS_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
cleanup();
var $t = _testcase("352519/352510 /352511/352512/352519/352520/352521/352517/352518","Verify the field validations for 'First Name'/last name/address1/address2/city/zip code/phone number text box on shipping page in Application as a guest user");
$t.start();
try
{
	//add items to cart
	navigateToCartSearch($Shipping_Page[0][1],1);
	_click($CART_CHECKOUT_BUTTON_BOTTOM);	
	_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON); 
	//verify the navigation
	//_assertVisible($SHIPPING_PAGE_HEADING);
	_assertVisible($SHIPPING_PAGE_HEADER);
	//validating the fields
	for(var $i=0;$i<$Validation.length;$i++)
	{
		if($i==0)
		{
			_setValue($SHIPPING_FN_TEXTBOX,"");
			_setValue($SHIPPING_LN_TEXTBOX,"");
			_setValue($SHIPPING_ADDRESS1_TEXTBOX,"");
			_setValue($SHIPPING_ADDRESS2_TEXTBOX,"");
			_setSelected($SHIPPING_COUNTRY_DROPDOWN,"");
			_setSelected($SHIPPING_STATE_DROPDOWN, "");
			_setValue($SHIPPING_CITY_TEXTBOX,"");
			_setValue($SHIPPING_ZIPCODE_TEXTBOX, ""); 
			_setValue($SHIPPING_PHONE_TEXTBOX,"");
		}
		else
		{
			_setValue($SHIPPING_FN_TEXTBOX, $Validation[$i][1]);
			_setValue($SHIPPING_LN_TEXTBOX, $Validation[$i][2]);
			_setValue($SHIPPING_ADDRESS1_TEXTBOX, $Validation[$i][3]);
			_setValue($SHIPPING_ADDRESS2_TEXTBOX,$Validation[$i][4]);
			_setSelected($SHIPPING_COUNTRY_DROPDOWN,$Validation[$i][5]);
			_setSelected($SHIPPING_STATE_DROPDOWN, $Validation[$i][6]);
			_setValue($SHIPPING_CITY_TEXTBOX, $Validation[$i][7]);
			_setValue($SHIPPING_ZIPCODE_TEXTBOX, $Validation[$i][8]); 
			_setValue($SHIPPING_PHONE_TEXTBOX,$Validation[$i][9]);
		}
		//click on continue button
		_click($SHIPPING_SUBMIT_BUTTON);
		//with blank data
		if($i==0)
		{
			_assert($SHIPPING_SUBMIT_BUTTON.disabled);
		}
		//checking the max length
		if($i==1)
		{
			//Fname,Lname Should not highlight
			_assertNotEqual($Validation[0][10],_style($SHIPPING_FN_TEXTBOX,"background-color"));
			_assertNotEqual($Validation[0][10],_style($SHIPPING_LN_TEXTBOX,"background-color"));
			//for address label verification
			_assertVisible(_label("/"+$Shipping_Page[8][0]+"/"));
			_assertVisible(_label("/"+$Shipping_Page[9][0]+"/"));
			//verifying the length of field's first name,last name,add1,addr2,phone number,City 
			_assertEqual($Validation[0][12],_getText($SHIPPING_FN_TEXTBOX).length);
			_assertEqual($Validation[1][12],_getText($SHIPPING_LN_TEXTBOX).length);
			_assertEqual($Validation[2][12],_getText($SHIPPING_ADDRESS1_TEXTBOX).length);
			_assertEqual($Validation[2][12],_getText($SHIPPING_ADDRESS2_TEXTBOX).length);
			_assertEqual($Validation[0][11],_getText($SHIPPING_CITY_TEXTBOX).length);
			_assertEqual($Validation[0][12],_getText($SHIPPING_PHONE_TEXTBOX).length);
			//Zip Code max length
			_assertEqual($Validation[1][11],_getText($SHIPPING_ZIPCODE_TEXTBOX).length); 
		}
		//numeric,special chars,combination of both,alphabets data in city,phone number,Zip code field's
		if($i==2 || $i==3 || $i==4 || $i==5)
		{
			//Zip Code
			_assertEqual("rgb(169, 17, 24)", _style($SHIPPING_ZIPCODE_ERROR,"color"));
			_assertEqual($Validation[4][10],_style(_span($Validation[2][11]),"color"));
			_assertVisible(_span($Validation[2][11])); 
			//Phone Number
			//_assertEqual($Validation[6][10],_style($SHIPPING_PHONE_TEXTBOX,"background-color"));
			_assertEqual($Validation[4][10],_style(_span($Validation[2][10]),"color"));
			_assertVisible(_span($Validation[2][10]));
			//Continue button should be disabled
			_assert($SHIPPING_SUBMIT_BUTTON.disabled);
		}
		//state field validation
		if($i==6)
		{
			//Erro msg
//			_assertVisible(_span($Validation[7][10]));
//			//Background color
//			_assertEqual($Validation[6][10],_style(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"),"background-color"));
//			//border color
//			_assertEqual($Validation[4][10],_style(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"),"border-top-color"));
			_assert($SHIPPING_SUBMIT_BUTTON.disabled);
		}
		//with valid data
		if($i==7)
		{
			//address verification overlay
			//addressVerificationOverlay();
			//Should navigate to billing page
			_assertVisible($BILLING_PAGE_HEADING);
			_assertVisible($BILLING_PAYMENT_PLACE_ORDER_BUTTON);
		}
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
cleanup();
var $t = _testcase("352522/148946","Verify the functionality of the Checkbox 'Use this address for Billing' on shipping page in Prevail application as a guest user");
$t.start();
try
{
	navigateToShippingPage($Shipping_Page[0][1],1);
	//entering the shipping address
	shippingAddress($Shipping_addr,0);
	//check the use this address for billing checkbox
	_check($SHIPPING_BILLING_ADDRESS_CHECKBOX);
	_click($SHIPPING_SUBMIT_BUTTON);
	//address verification overlay
	//addressVerificationOverlay();
	//verify the address prepopulation in billing page
	//first name
	_assertEqual($Shipping_addr[0][1],_getText($BILLING_FN_TEXTBOX));
	//last name
	_assertEqual($Shipping_addr[0][2],_getText($BILLING_LN_TEXTBOX));
	//address1
	_assertEqual($Shipping_addr[0][3],_getText($BILLING_ADDRESS1_TEXTBOX));
	//country
	_assertEqual($Shipping_addr[0][5],_getSelectedText($BILLING_COUNTRY_DROPDOWN));
	//State
	_assertEqual($Shipping_addr[0][6],_getSelectedText($BILLING_STATE_DROPDOWN));
	//City
	_assertEqual($Shipping_addr[0][7],_getText($BILLING_CITY_TEXTBOX));
	//Zipcode
	_assertEqual($Shipping_addr[0][8],_getText($BILLING_ZIPCODE_TEXTBOX));
	//Phone number
	_assertEqual($Shipping_addr[0][9],_getText($BILLING_NUMBER_TEXTBOX));
	//navigating back to shipping page
	_click($BILLING_SHIPPING_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352542/352531","Verify the functionality of 'Is this a gift?:' radio buttons on shipping page in Prevail application as a guest user");
$t.start();
try
{
	_click(_link("Edit", _in(_div("mini-shipment order-component-block  first "))));
	
	
	//verify the navigation
	_assertEqual("Step 1: Shipping", _getText($CHECKOUT_SHIPPING_SECTION_ACTIVETAB));

	//verifying the is this gift order
	_assertVisible($SHIPPING_GIFT_TEXT);

	//by default no should be selected
	_assert(_radio("false").checked);
	//selecting yes button
	_click(_radio("true"));
	//verifying the message text field
	_assertVisible($SHIPPING_MESSAGE);
	_assertVisible($SHIPPING_MESSAGE_TEXTAREA);
	//selecting no radio button
	_click(_radio("false"));
	//verify the display of message box
	_assertEqual(false,_isVisible($SHIPPING_GIFT_DIV));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("352523","Verify the validation of 'Message' field on  shipping page in Prevail application as a guest user");
$t.start();
try
{
	//selecting is this gift yes button
	//selecting yes button
	_click(_radio("true"));
	//verifying the message text field
	_assertVisible($SHIPPING_MESSAGE);
	_assertVisible($SHIPPING_MESSAGE_TEXTAREA);
	//enter more than 250 characters in textbox and verify the max length
	_setValue($SHIPPING_MESSAGE_TEXTAREA,$Shipping_Page[2][8]);
	//Verify that no more than 250 characters are present in Message field
	_assertEqual($Shipping_Page[3][8],_getText($SHIPPING_MESSAGE_TEXTAREA).length);
	_setValue($SHIPPING_MESSAGE_TEXTAREA2, $Shipping_Page[2][8]);
	_assertEqual($Shipping_Page[3][8],_getText($SHIPPING_MESSAGE_TEXTAREA2).length);
	_setValue($SHIPPING_MESSAGE_TEXTAREA3, $Shipping_Page[2][8]);
	_assertEqual($Shipping_Page[3][8],_getText($SHIPPING_MESSAGE_TEXTAREA3).length);
	
	_setValue($SHIPPING_MESSAGE_TEXTAREA4, $Shipping_Page[2][8]);
	_assertEqual($Shipping_Page[3][8],_getText($SHIPPING_MESSAGE_TEXTAREA4).length);
	
	var $TOTChar=$Shipping_Page[3][8]*4;
	_assertEqual($TOTChar,$Shipping_Page[5][8]);
	//No of characters displayed below the field.
	//_assertVisible($SHIPPING_MESSAGE_TEXTAREA_STATICTEXT);
	//_assertVisible($SHIPPING_MESSAGE_TEXTAREA_STATICTEXT_DIV);			
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352525/352526","Verify the functionality of radio buttons under 'SELECT SHIPPING METHOD' Section on shipping page in Application as a Registered user and a guest user.");
$t.start();
try
{
	//verifying the UI of Shipping section
	_assertVisible($SHIPPING_METHOD_HEADING);
	var $totalToolTips=_count("_link","tooltip",_in($SHIPPING_METHOD_HEADING));
	var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in($SHIPPING_METHOD_HEADING));
	var $tot_ShippingMethodLable=_collect("_label","/(.*)/",_in($SHIPPING_METHOD_HEADING));
	_assertEqual($totalShippingMethods,$totalToolTips);
	for(var $i=0;$i<$totalShippingMethods;$i++)
	{
		//shipping methods
		_assertVisible(_div("form-row form-indent label-inline["+$i+"]"));
		_assertVisible(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
		
		//mouse overing on the tooltips
		_mouseOver(_link("tooltip",_in(_div("form-row form-indent label-inline["+$i+"]"))));
		//verifying the display of tool tip
		//_assertVisible($SHIPPING_METHOD_TOOLTIP);
		
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();