_include("../../../GenericLibrary/BM_Functions.js");
_include("../../../GenericLibrary/GlobalFunctions.js");

_resource("Shipping.xls");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();
_click(_link("user-login"));
login();
//deleting the existing addresses
deleteAddress();

var $savedAddress=$Shipping_addr[0][11]+" "+$Shipping_addr[0][7]+" "+$Shipping_addr[0][1]+" "+$Shipping_addr[0][2]+" "+$Shipping_addr[0][3]+" "+$Shipping_addr[0][4]+" "+$Shipping_addr[0][7]+" "+$Shipping_addr[1][6]+" "+$Shipping_addr[0][8]+" "+$Shipping_addr[0][5]+" "+"Phone: "+$Shipping_addr[0][9]+" "+$Shipping_addr[1][11];


var $t = _testcase("124468","Verify the navigation to  'SHIPPING' page in Application as a Registered  user");
$t.start();
try
{
	//add items to cart
	navigateToCartSearch($Shipping_Page[0][1],1);
	//click on go strait to checkout link
	_click($CART_CHECKOUT_BUTTON);
	//verify the navigation
	//_assertVisible($SHIPPING_PAGE_HEADING);
	_assertVisible($SHIPPING_PAGE_HEADER);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
	

var $t = _testcase("352506/352828 ","Verify the UI of shipping address form in Application as a Registered user when the user have no saved addresses.");
$t.start();
try
{
	//verify the UI of shipping page
	//Heading
	_assertVisible($SHIPPING_PAGE_HEADER);
	//required msg
	_assertVisible($SHIPPING_REQUIRED);
	//Address list drop down
	_assertNotVisible($SHIPPING_ADDRESS_DROPDOWN);
	//first name
	_assertVisible($SHIPPING_FIRST_NAME);
	_assertVisible($SHIPPING_FN_TEXTBOX);
	//last name
	_assertVisible($SHIPPING_LAST_NAME);
	_assertVisible($SHIPPING_LN_TEXTBOX);
	//address1
	_assertVisible($SHIPPING_ADDRESS_ONE);
	_assertVisible($SHIPPING_ADDRESS1_TEXTBOX);
	//address2
	_assertVisible($SHIPPING_ADDRESS_TWO);
	_assertVisible($SHIPPING_ADDRESS2_TEXTBOX);
	//country
	_assertVisible($SHIPPING_COUNTRY);
	_assertVisible($SHIPPING_COUNTRY_DROPDOWN);
	//default value
	_assertEqual($Shipping_Page[1][4], _getSelectedText($SHIPPING_COUNTRY_DROPDOWN));
	//state
	_assertVisible($SHIPPING_STATE);
	_assertVisible($SHIPPING_STATE_DROPDOWN);
	//default value
	_assertEqual($Shipping_Page[0][5], _getSelectedText($SHIPPING_STATE_DROPDOWN));
	//city
	_assertVisible($SHIPPING_CITY);
	_assertVisible($SHIPPING_CITY_TEXTBOX);
	//zip
	_assertVisible($SHIPPING_ZIPCODE);
	_assertVisible($SHIPPING_ZIPCODE_TEXTBOX);
	//phone number
	_assertVisible($SHIPPING_PHONE_NUMBER);
	_assertVisible($SHIPPING_PHONE_TEXTBOX);
	_assertVisible($SHIPPING_PHONE_EXAMPLE);
	//add to address book
	_assertVisible($SHIPPING_ADDTOADDRESS);
	_assertVisible($SHIPPING_ADDTOADDRESS_CHECKBOX);
	_assertNotTrue($SHIPPING_ADDTOADDRESS_CHECKBOX.checked);
	//use this billing address for registered user
	_assertVisible($SHIPPING_BILLING_ADDRESS);
	_assertVisible($SHIPPING_BILLING_ADDRESS_CHECKBOX);
	_assertTrue($SHIPPING_BILLING_ADDRESS_CHECKBOX.checked);
	
	if(!isMobile() || mobile.iPad())
	{
		//tooltip links
		_assertVisible($SHIPPING_ADDRESS_TOOLTIP);
		_assertVisible($SHIPPING_PHONE_TOOLTIP);
	}
	
	//Is this a gift section
	_assertVisible($SHIPPING_GIFT_TEXT);
	_assertVisible(_radio("true"));
	_assertNotTrue(_radio("true").checked);
	_assertVisible(_radio("false"));
	_assert(_radio("false").checked);

	//Is this a gift section
	//_assertVisible(_label("/"+$Shipping_Page[2][0]+"/"));
	//_assertVisible(_radio("is-gift-yes"));
	//_assertNotTrue(_radio("is-gift-yes").checked);
	//_assertVisible(_radio("is-gift-no"));
	//_assert(_radio("is-gift-no").checked);
	
	//select shipping method section
	_assertVisible($SHIPPING_METHOD_HEADING);
	var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_fieldset($Shipping_Page[3][0])));
	for(var $i=0;$i<$totalShippingMethods;$i++)
	{
		//shipping methods
		_assertVisible(_div("form-row form-indent label-inline["+$i+"]"));
	}
	//continue button
	_assertVisible($SHIPPING_SUBMIT_BUTTON);
	//summary section
	_assertVisible($SHIPPING_RIGHT_NAV_HEADING);
	_assertVisible($SHIPPING_RIGHT_NAV);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124471","Verify the functionality of 'Select an Address:' drop down from Checkout Shipping page in Application as a Registered  user");
$t.start();
try
{
//navigate address page
_click(_link("user-account"));
_wait(2000);
_click($MY_ACCOUNT_LINK_LEFTNAV);
_click($LEFTNAV_ADDRESS_LINK);
//deleting the existing addresses
deleteAddress();
//create new address
_click($ADDRESSES_CREATENEW_ADDRESS_LINK);
//adding address
addAddress($addr_data,0);
_click($ADDRESSES_OVERLAY_APPLYBUTTON);
_wait(3000);
//navigate to shipping page
_click($MINICART_CHECKOUT);
//verify the drop down
_assertVisible(_fieldset($Shipping_Page[11][0]));
_assertVisible(_label($Shipping_Page[7][0]));
_assertVisible($SHIPPING_ADDRESS_DROPDOWN);
//selecting the address from drop down
_setSelected($SHIPPING_ADDRESS_DROPDOWN, "1");
//verify the pre population
var $defaultAddress=new Array();
$defaultAddress=$addr_data[0][2]+$addr_data[0][3]+$addr_data[0][4]+$addr_data[0][5]+$addr_data[0][6]+$addr_data[0][7]+$addr_data[0][8]+$addr_data[0][9]+$addr_data[0][10];
var $address=new Array();
$address=[_getText($SHIPPING_FN_TEXTBOX),_getText($SHIPPING_LN_TEXTBOX),_getText($SHIPPING_ADDRESS1_TEXTBOX),_getText($SHIPPING_ADDRESS2_TEXTBOX),_getText($SHIPPING_CITY_TEXTBOX),_getText($SHIPPING_ZIPCODE_TEXTBOX),_getSelectedText($SHIPPING_COUNTRY_DROPDOWN),_getSelectedText($SHIPPING_STATE_DROPDOWN),_getText($SHIPPING_PHONE_TEXTBOX)];
for(var $i=0;$i<$address.length;$i++)
	{
	if($defaultAddress.indexOf($address[$i])>=0)
		{
			_assert(true, $address[$i]+" data is Prepopulated");
		}
	else
		{
		_assert(false, $address[$i]+" data is not Prepopulated");
		}
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("352507/352829","Verify the UI of shipping address form in Application as a Registered user when the user have saved addresses.");
$t.start();
try
{
	//verify the UI of shipping page
	//Heading
	_assertVisible($SHIPPING_PAGE_HEADER);
	//required msg
	_assertVisible($SHIPPING_REQUIRED);
	//Address list drop down
	_assertVisible($SHIPPING_ADDRESS_DROPDOWN);
	//first name
	_assertVisible($SHIPPING_FIRST_NAME);
	_assertVisible($SHIPPING_FN_TEXTBOX);
	//last name
	_assertVisible($SHIPPING_LAST_NAME);
	_assertVisible($SHIPPING_LN_TEXTBOX);
	//address1
	_assertVisible($SHIPPING_ADDRESS_ONE);
	_assertVisible($SHIPPING_ADDRESS1_TEXTBOX);
	//address2
	_assertVisible($SHIPPING_ADDRESS_TWO);
	_assertVisible($SHIPPING_ADDRESS2_TEXTBOX);
	//country
	_assertVisible($SHIPPING_COUNTRY);
	_assertVisible($SHIPPING_COUNTRY_DROPDOWN);
	//default value
	_assertEqual($Shipping_Page[1][4], _getSelectedText($SHIPPING_COUNTRY_DROPDOWN));
	//state
	_assertVisible($SHIPPING_STATE);
	_assertVisible($SHIPPING_STATE_DROPDOWN);
	//default value
	_assertEqual($Shipping_Page[0][5], _getSelectedText($SHIPPING_STATE_DROPDOWN));
	//city
	_assertVisible($SHIPPING_CITY);
	_assertVisible($SHIPPING_CITY_TEXTBOX);
	//zip
	_assertVisible($SHIPPING_ZIPCODE);
	_assertVisible($SHIPPING_ZIPCODE_TEXTBOX);
	//phone number
	_assertVisible($SHIPPING_PHONE_NUMBER);
	_assertVisible($SHIPPING_PHONE_TEXTBOX);
	_assertVisible($SHIPPING_PHONE_EXAMPLE);
	//add to address book
	_assertVisible($SHIPPING_ADDTOADDRESS);
	_assertVisible($SHIPPING_ADDTOADDRESS_CHECKBOX);
	_assertNotTrue($SHIPPING_ADDTOADDRESS_CHECKBOX.checked);
	//use this billing address for registered user
	_assertVisible($SHIPPING_BILLING_ADDRESS);
	_assertVisible($SHIPPING_BILLING_ADDRESS_CHECKBOX);
	_assertTrue($SHIPPING_BILLING_ADDRESS_CHECKBOX.checked);
	
	if(!isMobile() || mobile.iPad())
	{
		//tooltip links
		_assertVisible($SHIPPING_ADDRESS_TOOLTIP);
		_assertVisible($SHIPPING_PHONE_TOOLTIP);
	}
	
	//Is this a gift section
	_assertVisible($SHIPPING_GIFT_TEXT);
	_assertVisible(_radio("true"));
	_assertNotTrue(_radio("true").checked);
	_assertVisible(_radio("false"));
	_assert(_radio("false").checked);

	//Is this a gift section
	//_assertVisible(_label("/"+$Shipping_Page[2][0]+"/"));
	//_assertVisible(_radio("is-gift-yes"));
	//_assertNotTrue(_radio("is-gift-yes").checked);
	//_assertVisible(_radio("is-gift-no"));
	//_assert(_radio("is-gift-no").checked);
	
	//select shipping method section
	_assertVisible($SHIPPING_METHOD_HEADING);
	var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_fieldset($Shipping_Page[3][0])));
	for(var $i=0;$i<$totalShippingMethods;$i++)
	{
		//shipping methods
		_assertVisible(_div("form-row form-indent label-inline["+$i+"]"));
	}
	//continue button
	_assertVisible($SHIPPING_SUBMIT_BUTTON);
	//summary section
	_assertVisible($SHIPPING_RIGHT_NAV_HEADING);
	_assertVisible($SHIPPING_RIGHT_NAV);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

ClearCartItems();
var $t = _testcase("352508","Verify the display of 'Do you want to ship to multiple addresses?' when user has added single item to the cart from Checkout Shipping page in Application as a Registered  user");
$t.start();
try
{
//adding two item's to cart
navigateToCartSearch($Shipping_Page[0][1],1);
var $cartItems=_getText($MINICART_QUANTITY);	
//verify whether do want to ship to multiple addresses is there or not
if($cartItems==1)
	{
	_assertNotVisible($SHIPPING_MULTIPLE_ADDRESS);
	_assertNotVisible($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
	}
else
	{
	_assert(false,"more than one item is there in cart");
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


ClearCartItems();
var $t = _testcase("352539","Verify the display of 'Do you want to ship to multiple addresses?' when user has added morethan one item to the cart from Checkout Shipping page in Application as a Registered  user");
$t.start();
try
{
//adding two item's to cart
	navigateToCartSearch($Shipping_Page[0][1],2);	
//navigate to shipping page
_click($MINICART_CHECKOUT);
//verify whether do want to ship to multiple addresses
_assertVisible($SHIPPING_MULTIPLE_ADDRESS);
_assertVisible($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


ClearCartItems();
var $t = _testcase("352539","Verify the display of  'Do you want to ship to multiple addresses?' when user has added more than 1 item to the cart  from Checkout Shipping page in Application as a Registered user");
$t.start();
try
{
//adding item to cart related to suits category
navigateToCartSearch($Shipping_Page[0][1],2);	
//adding item to cart related to another category
//navigate to shipping page
_click($MINICART_CHECKOUT);
//verify whether do want to ship to multiple addresses
_assertVisible($SHIPPING_MULTIPLE_ADDRESS);
_assertVisible($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352524","Verify the navigation of 'Yes' button on Do you want to ship to multiple addresses option from Checkout Shipping page in Application as a Registered user");
$t.start();
try
{
	//click on YES button on Do you want to ship to multiple addresses
	_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
	//verify the functionality
	_assertVisible($SHIPPING_SINGLE_ADDRESS_BUTTON);
	_assertEqual("/"+$Shipping_Page[0][2]+"/", _getText($SHIPPING_SINGLE_ADDRESS_TEXT));
	//verify the line items
	_assertVisible($SHIPPING_CART_ITEMS);
	_assertVisible($MULTISHIPPING_LINE_ITEM_FIRST);
	_assertVisible($MULTISHIPPING_LINE_ITEM_SECOND);
	var $addrCount=0;
	//adding addresses
	for(var $i=0;$i<$Address.length;$i++)
	{
		_click(_span("Add/Edit Address["+$i+"]"));
		addAddressMultiShip($Address,$i);
		$addrCount++;
	}
	//select addresses from drop down 
	var $dropDowns=_count("_select","/dwfrm_multishipping_addressSelection_quantityLineItems/",_in($SHIPPING_CART_ITEMS));
	var $c1=1;
	var $j=1;
	for(var $i=0;$i<$dropDowns;$i++)
		{
		   if($c1>$addrCount)
		    {
		      $c1=1;
		    }
		_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"),$j);
		$j++;
		}
	_click($SHIPPING_MULTIPLE_ADDRESS_SAVE_BUTTON);
	var $count=_count("_table","/item-list/",_in($CHECKOUT_MULTISHIP));

	for(var $i=0;$i<$count;$i++)
		{
		//verify the 'is this a gift' section 
		_assertVisible(_label($Shipping_Page[0][8]+"["+$i+"]"));
		//by default no should select
		_assertVisible(_radio("false["+$i+"]"));
		_assert(_radio("false["+$i+"]").checked);
		//selecting yes button
		_click(_radio("true["+$i+"]"));
		//verifying the message text field
		_assertVisible(_div($Shipping_Page[1][8]+"["+$i+"]"));
		_assertVisible(_textarea("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMessage"));
		//selecting no radio button
		_click(_radio("false["+$i+"]"));
		//verify the display of message box
		_assertEqual(false,_isVisible($SHIPPING_GIFT_DIV));
		//message field validation
		_click(_radio("true["+$i+"]"));
		//verifying the message text field
		_assertVisible(_div($Shipping_Page[1][8]+"["+$i+"]"));
		_assertVisible(_textarea("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMessage"));
		//enter more than 150 characters in textbox and verify the max length
		_setValue(_textarea("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMessage"),$Shipping_Page[2][8]);
		_assertEqual($Shipping_Page[3][8],_getText(_textarea("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMessage")).length);
		
		_click(_radio("false["+$i+"]"));
		}
	//click on yes button to reset back
	//_click($SHIPPING_SINGLE_ADDRESS_BUTTON);
	}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


ClearCartItems();
//add items to cart
navigateToCartSearch($Shipping_Page[0][1],1);
_click($MINICART_CHECKOUT);
//verify the navigation
_assertVisible($SHIPPING_SUBMIT_BUTTON);
_assertVisible($SHIPPING_PAGE_HEADER);

var $t = _testcase("124480/124625/128330/148860/148861/148862/148864/148865/148866","Verify the field validations for 'First Name'/last name/address1/address2/city/zip code/State/phone number text box on shipping page in Application as a Registered user");
$t.start();
try
{
//validating the fields
	//validating the fields
	for(var $i=0;$i<$Validation.length;$i++)
	{
		if($i==0)
		{
			_setValue($SHIPPING_FN_TEXTBOX,"");
			_setValue($SHIPPING_LN_TEXTBOX,"");
			_setValue($SHIPPING_ADDRESS1_TEXTBOX,"");
			_setValue($SHIPPING_ADDRESS2_TEXTBOX,"");
			_setSelected($SHIPPING_COUNTRY_DROPDOWN,"");
			_setSelected($SHIPPING_STATE_DROPDOWN, "");
			_setValue($SHIPPING_CITY_TEXTBOX,"");
			_setValue($SHIPPING_ZIPCODE_TEXTBOX, ""); 
			_setValue($SHIPPING_PHONE_TEXTBOX,"");
		}
		else
		{
			_setValue($SHIPPING_FN_TEXTBOX, $Validation[$i][1]);
			_setValue($SHIPPING_LN_TEXTBOX, $Validation[$i][2]);
			_setValue($SHIPPING_ADDRESS1_TEXTBOX, $Validation[$i][3]);
			_setValue($SHIPPING_ADDRESS2_TEXTBOX,$Validation[$i][4]);
			_setSelected($SHIPPING_COUNTRY_DROPDOWN,$Validation[$i][5]);
			_setSelected($SHIPPING_STATE_DROPDOWN, $Validation[$i][6]);
			_setValue($SHIPPING_CITY_TEXTBOX, $Validation[$i][7]);
			_setValue($SHIPPING_ZIPCODE_TEXTBOX, $Validation[$i][8]); 
			_setValue($SHIPPING_PHONE_TEXTBOX,$Validation[$i][9]);
		}
		//click on continue button
		_click($SHIPPING_SUBMIT_BUTTON);
		//with blank data
		if($i==0)
		{
			_assert($SHIPPING_SUBMIT_BUTTON.disabled);
		}
		//checking the max length
		if($i==1)
		{
			//Fname,Lname Should not highlight
			_assertNotEqual($Validation[0][10],_style($SHIPPING_FN_TEXTBOX,"background-color"));
			_assertNotEqual($Validation[0][10],_style($SHIPPING_LN_TEXTBOX,"background-color"));
			//for address label verification
			_assertVisible(_label("/"+$Shipping_Page[8][0]+"/"));
			_assertVisible(_label("/"+$Shipping_Page[9][0]+"/"));
			//verifying the length of field's first name,last name,add1,addr2,phone number,City 
			_assertEqual($Validation[0][12],_getText($SHIPPING_FN_TEXTBOX).length);
			_assertEqual($Validation[1][12],_getText($SHIPPING_LN_TEXTBOX).length);
			_assertEqual($Validation[2][12],_getText($SHIPPING_ADDRESS1_TEXTBOX).length);
			_assertEqual($Validation[2][12],_getText($SHIPPING_ADDRESS2_TEXTBOX).length);
			_assertEqual($Validation[0][11],_getText($SHIPPING_CITY_TEXTBOX).length);
			_assertEqual($Validation[0][12],_getText($SHIPPING_PHONE_TEXTBOX).length);
			//Zip Code max length
			_assertEqual($Validation[1][11],_getText($SHIPPING_ZIPCODE_TEXTBOX).length); 
		}
		//numeric,special chars,combination of both,alphabets data in city,phone number,Zip code field's
		if($i==2 || $i==3 || $i==4 || $i==5)
		{
			//Zip Code
			_assertEqual("rgb(169, 17, 24)", _style($SHIPPING_ZIPCODE_ERROR,"color"));
			_assertEqual($Validation[4][10],_style(_span($Validation[2][11]),"color"));
			_assertVisible(_span($Validation[2][11])); 
			//Phone Number
			//_assertEqual($Validation[6][10],_style($SHIPPING_PHONE_TEXTBOX,"background-color"));
			_assertEqual($Validation[4][10],_style(_span($Validation[2][10]),"color"));
			_assertVisible(_span($Validation[2][10]));
			//Continue button should be disabled
			_assert($SHIPPING_SUBMIT_BUTTON.disabled);
		}
		//state field validation
		if($i==6)
		{
			//Erro msg
//			_assertVisible(_span($Validation[7][10]));
//			//Background color
//			_assertEqual($Validation[6][10],_style(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"),"background-color"));
//			//border color
//			_assertEqual($Validation[4][10],_style(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"),"border-top-color"));
			_assert($SHIPPING_SUBMIT_BUTTON.disabled);
		}
		//with valid data
		if($i==7)
		{
			//address verification overlay
			//addressVerificationOverlay();
			//Should navigate to billing page
			_assertVisible($BILLING_PAGE_HEADING);
			_assertVisible($BILLING_PAYMENT_PLACE_ORDER_BUTTON);
		}
	}
}

//In Progress



catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


ClearCartItems();
var $t = _testcase("352522","Verify the functionality of the Checkbox 'Use this address for Billing' on shipping page in   application as a Registered user");
$t.start();
try
{
//navigate to shipping page
navigateToShippingPage($Shipping_Page[0][1],1);
//entering the shipping address
shippingAddress($Shipping_addr,0);
//check the use this address for billing checkbox
_check($SHIPPING_BILLING_ADDRESS_CHECKBOX);
_click($SHIPPING_SUBMIT_BUTTON);
//address verification
//addressVerificationOverlay();
//verify the address prepopulation in billing page
//FIRST NAME
_assertEqual($Shipping_addr[0][1],_getText($BILLING_FN_TEXTBOX));
//Last name
_assertEqual($Shipping_addr[0][2],_getText($BILLING_LN_TEXTBOX));
//Address 1
_assertEqual($Shipping_addr[0][3],_getText($BILLING_ADDRESS1_TEXTBOX));
//Country
_assertEqual($Shipping_addr[0][5],_getSelectedText($BILLING_COUNTRY_DROPDOWN));
//State
_assertEqual($Shipping_addr[0][6],_getSelectedText($BILLING_STATE_DROPDOWN));
//City
_assertEqual($Shipping_addr[0][7],_getText($BILLING_CITY_TEXTBOX));
//Zipcode
_assertEqual($Shipping_addr[0][8],_getText($BILLING_ZIPCODE_TEXTBOX));
//Phone
_assertEqual($Shipping_addr[0][9],_getText($BILLING_NUMBER_TEXTBOX));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("352523 ","Verify the functionality of 'Is this a gift?:' radio buttons on shipping page in   application as a Registered user");
$t.start();
try
{

	//navigating back to shipping page
	_click($CHECKOUT_INDICATOR_SHIPPING_LINK);

	//verifying the is this gift order
	_assertVisible(_label("/"+$Shipping_Page[0][8]+"/"));

	//by default no should be selected
	_assert(_radio("false").checked);
	//selecting yes button
	_click(_radio("true"));
	//verifying the message text field
	_assertVisible(_label($Shipping_Page[1][8]));
	_assertVisible($SHIPPING_MESSAGE_TEXTAREA);
	//selecting no radio button
	_click(_radio("false"));
	//verify the display of message box
	_assertEqual(false,_isVisible($SHIPPING_GIFT_DIV));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352524","Verify the validation of 'Message' field on  shipping page in   application as a Registered user");
$t.start();
try
{

	//selecting is this gift yes button
	//selecting yes button
	_click(_radio("true"));
	//verifying the message text field
	_assertVisible($SHIPPING_MESSAGE);
	_assertVisible($SHIPPING_MESSAGE_TEXTAREA);
	//enter more than 250 characters in textbox and verify the max length
	_setValue($SHIPPING_MESSAGE_TEXTAREA,$Shipping_Page[2][8]);
	//Verify that no more than 250 characters are present in Message field
	_assertEqual($Shipping_Page[3][8],_getText($SHIPPING_MESSAGE_TEXTAREA).length);
	_setValue($SHIPPING_MESSAGE_TEXTAREA2, $Shipping_Page[2][8]);
	_assertEqual($Shipping_Page[3][8],_getText($SHIPPING_MESSAGE_TEXTAREA2).length);
	_setValue($SHIPPING_MESSAGE_TEXTAREA3, $Shipping_Page[2][8]);
	_assertEqual($Shipping_Page[3][8],_getText($SHIPPING_MESSAGE_TEXTAREA3).length);
	
	_setValue($SHIPPING_MESSAGE_TEXTAREA4, $Shipping_Page[2][8]);
	_assertEqual($Shipping_Page[3][8],_getText($SHIPPING_MESSAGE_TEXTAREA4).length);
	
	var $TOTChar=$Shipping_Page[3][8]*4;
	_assertEqual($TOTChar,$Shipping_Page[5][8]);
	//No of characters displayed below the field.
	//_assertVisible($SHIPPING_MESSAGE_TEXTAREA_STATICTEXT);
	//_assertVisible($SHIPPING_MESSAGE_TEXTAREA_STATICTEXT_DIV);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352525 ","Verify the UI of 'SELECT SHIPPING METHOD' Section on shipping page in   application as a Registered user");
$t.start();
try
{
	//verifying the UI of Shipping section
	_assertVisible(_fieldset($Shipping_Page[3][0]));
	var $totalToolTips=_count("_link","tooltip",_in(_fieldset($Shipping_Page[3][0])));
	var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_fieldset($Shipping_Page[3][0])));
	_assertEqual($totalShippingMethods,$totalToolTips);
	for(var $i=0;$i<$totalShippingMethods;$i++)
	{
	//shipping methods
	_assertVisible(_div("form-row form-indent label-inline["+$i+"]"));
	_assertVisible(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
	/*
	//mousehovering on the tooltips
	_mouseOver($TOOLTIP,_in(_div("form-row form-indent label-inline["+$i+"]"))));
	//verifying the display of tool tip
	_assertVisible(_div("small tooltip-shipping"));
	*/	
}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352526 /128330","Verify the functionality of radio buttons under 'SELECT SHIPPING METHOD' Section on shipping page in   application as a Registered user");
$t.start();
try
{
	//verify the functionality of radio buttons
	var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_fieldset($Shipping_Page[3][0])));
	var $count=0;
	for(var $i=0;$i<$totalShippingMethods;$i++)
		{
		//selecting radio buttons
		_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
		//verify the selection
		_assert(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked);
		//uncheck the checkbox
		_uncheck(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
		//verify the deselection
		_assert(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked);
		for(var $j=0;$j<$totalShippingMethods;$j++)
			{
			if(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked)
				{
				$count++;
				}
			}
		}		
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124632","Verify the functionality of 'EDIT' link in order summary Section in right nav on shipping page in   application as a Registered user");
$t.start();
try
{
//navigating to shipping page
if(!isMobile() || mobile.iPad())
{
var $productName=_getText($SHIPPING_SUMMARY_PRODUCT_NAME);
//clicking on edit link present in the order Summuary section
_click($SHIPPING_SUMMARY_EDIT_MOBILE);
}
else
{
var $productName=_getText($SHIPPING_SUMMARY_PRODUCT_NAME);
//clicking on edit link present in the order Summuary section
_click($SHIPPING_SUMMARY_EDIT_DESKTOP);
}
//cart navigation verification
_assertVisible($CART_TABLE);
_assertVisible($CART_ACTIONBAR);
_assertVisible($CART_NAME);
_assertEqual($productName,_getText($SHIPPING_PRODUCT_NAME));
//fetch the total items in cart
var $total=_count("_row","/cart-row/",_in(_table("cart-table")));
for(var $i=0;$i<$total;$i++)
	{	
	//updating the quantity
	_setValue($CART_QTY_DROPDOWN,$Shipping_Page[5][5]);
	}
//click on update cart
_click($CART_UPDATE_BUTTON);
_wait(5000,_isVisible($MINICART_CHECKOUT));
//navigate to shipping page
_click($MINICART_CHECKOUT);

//verify the cart updation
for(var $i=0;$i<$total;$i++)
{
_assertEqual($Shipping_Page[5][5], _getText(_span("value",_in(_div("mini-cart-pricing["+$i+"]",_in($SHIPPING_RIGHT_NAV))))));
}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352541 ","Verify the navigation of 'product name' in order summary Section in right nav on shipping page in   application as a Registered user");
$t.start();
try
{
//clicking on product name in shipping page
if(!isMobile() || mobile.iPad())
	{
var $productName=_getText($SHIPPING_SUMMARY_PRODUCT_NAME_MOBILE);
_click($SHIPPING_SUMMARY_PRODUCT_NAME_MOBILE);
	}
else
	{
	var $productName=_getText($SHIPPING_SUMMARY_PRODUCT_NAME);
	_click($SHIPPING_SUMMARY_PRODUCT_NAME);
	}
//verify the navigation to respective PD Page
_assertVisible($PAGE_BREADCRUMB);
if(!isMobile())
	{
_assertVisible($PAGE_BREADCRUMB_ELEMENT);
	}
//_assertEqual($productName, _getText($PAGE_BREADCRUMB_ELEMENT));
_assertEqual($productName, _getText($SHIPPING_BREADCRUMB_TEXT));

_assertVisible($PDP_PRODUCTNAME);
_assertEqual($productName, _getText($PDP_PRODUCTNAME));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//navigate to shipping page
navigateToShippingPage($Shipping_Page[0][1],1);

if(!isMobile())
{
var $t = _testcase("124637/289219","Verify the mouse hover link functionality for 'APO/FPO and Why is this required?' on shipping page in   application as a Registered user");
$t.start();
try
{
	//mouse hover on apo/Fpo tool tip
	 _mouseOver($SHIPPING_TOOLTIP);
                    //verifying the display of tool tip
                    if(_getAttribute($SHIPPING_TOOLTIP,"aria-describedby")!=null)
                          {
                                 _assert(true);
                          }
                    else
                          {
                                 _assert(false);
                          }


//mouse hovering on why is this required tool tip
_mouseOver($SHIPPING_TOOLTIP1);

                    //verifying the display of tool tip
                    if(_getAttribute($SHIPPING_TOOLTIP1,"aria-describedby")!=null)
                          {
                                 _assert(true);
                          }
                    else
                          {
                                 _assert(false);
                          }

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
}



var $t = _testcase("352525","Verify the functionality of shipping charge and country selected on shipping page in   application as a Registered user");
$t.start();
try
{
	//changing the shipping method 
	var $totalradio=_count("_radio","/dwfrm_singleshipping_shippingAddress_shippingMethodID/");
	if($totalradio!=0)
		{
			for(var $i=0;$i<$totalradio;$i++)
				{
				_click(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
				//verify wehter added in order summuary section or not
				_assertVisible(_label("/"+$Shipping_Page[$i][10]+"/"));
				}
		}
	else
		{
			_log(false,"Need to configure Shipping Methods");
		}/*
	//selecting the Country as Canada
	_setSelected($SHIPPING_COUNTRY_DROPDOWN,$Shipping_Page[3][4]);
	//verifying the state field
	if(isMobile())
		{
		_assertVisible(_label("/"+$Shipping_Page[5][6]+"/"));
		}
	else
		{
	_assertVisible(_label("/"+$Shipping_Page[5][6]+"/",_leftOf($SHIPPING_STATE_DROPDOWN)));
		}*/
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352526","Verify the display of 'Details' tool tip in select shipping option Section on shipping page in   application as a Registered user");
$t.start();
try
{
//verifying the tooltips
var $totalToolTips=_count("_link","tooltip",_in(_fieldset($Shipping_Page[3][0])));
var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_fieldset($Shipping_Page[3][0])));
_assertEqual($totalShippingMethods,$totalToolTips);
var $Tooltips=_collect("_link","/tooltip/", _in(_fieldset("Select Shipping Method")));
for(var $i=0;$i<$totalShippingMethods;$i++)
	{
	 //mousehovering on the tooltips
                     _mouseOver($Tooltips[$i]);
                     //verifying the display of tool tip
                     if(_getAttribute($Tooltips[$i],"aria-describedby")!=null)
                           {
                                  _assert(true);
                           }
                     else
                           {
                                  _assert(false);
                           }
	}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("352532/352529","Verify the functionality of 'CONTINUE >' button/Verify the functionality of the Checkbox 'Add to Address Book' on shipping page in   application as a Registered user");
$t.start();
try
{
	//delete if any existing addresses are there
	_click(_link("user-account"));
	_wait(2000);
	_click($MY_ACCOUNT_LINK_LEFTNAV);
	_click($LEFTNAV_ADDRESS_LINK);
	while(_isVisible($ADDRESSES_DELETE_LINK))
		{
		_click($ADDRESSES_DELETE_LINK);
		_wait(3000);
		}
	//navigate to shipping page
	_click($GO_STRAIGHT_TO_CHECKOUT);
	//enter data in all optinal fields
	_setValue($SHIPPING_FN_TEXTBOX, "");
	_setValue($SHIPPING_LN_TEXTBOX, "");
	_setValue($SHIPPING_ADDRESS1_TEXTBOX,"");
	_setSelected($SHIPPING_COUNTRY_DROPDOWN,"");
	_setSelected($SHIPPING_STATE_DROPDOWN, "");
	_setValue($SHIPPING_CITY_TEXTBOX, "");
	_setValue($SHIPPING_ZIPCODE_TEXTBOX, "");
	_setValue($SHIPPING_PHONE_TEXTBOX,"");
	_setValue($SHIPPING_ADDRESS2_TEXTBOX,$Shipping_addr[0][4]);
	//click on continue button
	_click($SHIPPING_SUBMIT_BUTTON);
	//verify the display
	_assert($SHIPPING_SUBMIT_BUTTON.disabled);
	//enter all required data except optional data
	shippingAddress($Shipping_addr,0);
	//check add to address check box
	_check($SHIPPING_ADDTOADDRESS_CHECKBOX);
	//click on continue button
	_click($SHIPPING_SUBMIT_BUTTON);
	
	//verify the navigation to billing page
	_assertVisible(_fieldset("/"+$Shipping_Page[0][3]+"/"));
	_assertVisible($BILLING_CONTINUE_BUTTON);
	//navigate to my account page
	_assertVisible($USERACCOUNT_LINK);
	_click($MY_ACCOUNT_LINK);
	_click($LEFTNAV_ADDRESS_LINK);
	//verify the presence of saved address
	_assertVisible($ADDRESSES_MAKEDEFAULT_LINK);
	_assertVisible($ADDRESS_MINIADDRESS_LOCATION);
	var $adress=_getText($ADDRESSES_MAKEDEFAULT_LINK).toString().replace(",","");
	_assertEqual($savedAddress,$adress);
	//deleting the address
	_click($ADDRESSES_DELETE_LINK);	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

ClearCartItems(); 
var $t = _testcase("352536","Verify the calculation functionality in Order Summary pane on shipping page in   application as a Registered user");
$t.start();
try
{
//navigating to Shipping page
navigateToShippingPage($Shipping_Page[0][1],1);
//verifying the order summury pane
if(!isMobile() || mobile.iPad())
{
_assertVisible(_div($CProductName,_in($SHIPPING_RIGHT_NAV_MOBILE)));
//_assertVisible(_span("/"+$Shipping_Page[1][3]+"/",_in($SHIPPING_RIGHT_NAV_MOBILE)));
//_assertEqual($CQuantity, _getText($SHIPPING_RIGHT_NAV_VALUE,_in($SHIPPING_RIGHT_NAV_MOBILE)));
}
else
{
_assertVisible(_div($CProductName,_in($SHIPPING_RIGHT_NAV)));
_assertVisible(_span("/"+$Shipping_Page[1][3]+"/",_in($SHIPPING_RIGHT_NAV)));
//_assertEqual($CQuantity, _getText($SHIPPING_RIGHT_NAV_VALUE));
}
_assertEqual($cOrderSubtotal,_extract(_getText($SHIPPING_RIGHT_NAV_SUBTOTAL),"/[$](.*)/",true).toString());
if(isMobile() && !mobile.iPad())
{
_assertEqual($CShippingcharge,_extract(_getText($SHIPPING_RIGHT_NAV_SHIPPING_COST),"/[$](.*)/",true).toString());
}
else
{
_assertEqual($CShippingcharge,_extract(_getText($SHIPPING_RIGHT_NAV_SHIPPING_COST),"/[$](.*)/",true).toString());
}
//_assertEqual($tax,_extract(_getText($SHIPPING_RIGHT_NAV_SALES_TAX),"/[$](.*)/",true).toString());
_assertEqual($COrderTotal,_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/[$](.*)/",true).toString());	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



//navigate to shipping page
_click($MINICART_CHECKOUT);
//var $t=_testcase("289180","Verify the UI of checkout 'SHIPPING' page in Application as a registered user.");
//$t.start();
//try
//{
//	//sign in section
//	//_assertVisible($SHIPPING_LOGGEDIN_HEADER);
//	//shipping section
//	_assertVisible($CHECKOUT_SHIPPING_SECTION);
//	//Header
//	_assertVisible($SHIPPING_PAGE_HEADER);
//	_assertVisible($CHECKOUT_SHIPPING_CONTENT);
//	//Shipping method
//	_assertVisible($SHIPPING_METHOD_HEADING);
//	_assertVisible($SHIPPING_METHOD_LIST);
//	//Gift msg section
//	_assertVisible($SHIPPING_GIFT_TEXT);
//	//Payment
//	_assertVisible($CHECKOUT_PAYMENT_SECTION);
//	//order review
//	_assertVisible($CHECKOUT_ORDER_REVIEW_SECTION);
//	//Help section
//	_assertVisible($SHIPPING_ORDER_SUMMARY_HELP_SECTION);
//	//order summary
//	_assertVisible($SHIPPING_ORDER_SUMMARY_SECTION);
//}
//catch($e)
//{      
//	_logExceptionAsFailure($e);
//}
//$t.end();

//var $t=_testcase("289663/289775","Verify if the Logged-In as <Firstname Lastname> tab/ accordion is in active state when user is/functionality of Sign in sections Continue button in Shipping page as a Registered user");
//$t.start();
//try
//{
//	//Click on logged in heading
//	_click($SHIPPING_LOGGEDIN_HEADER);
//	//logged in section should be opened
//	_assertVisible($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
//	//click on continue
//	_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
//	//login section should be closed
//	_assertNotVisible($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
//	//click on tab
//	_click($SHIPPING_LOGGEDIN_DIV);
//	//logged in section should be opened
//	_assertVisible($CHECKOUT_SHIPPING_CONTENT);
//}
//catch($e)
//{      
//	_logExceptionAsFailure($e);
//}
//$t.end();
//
//var $t=_testcase("289691","Verify the UI of the Sign in section when expanded from shipping page as a registered user.");
//$t.start();
//try
//{
//	//Logout button
//	_assertVisible($SHIPPING_LOGGEDIN_SECTION_LOGOUT);
//	//Continue button
//	_assertVisible($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
//}
//catch($e)
//{      
//	_logExceptionAsFailure($e);
//}
//$t.end();
//
////click on continue
//_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
//
//var $t=_testcase("289783","Verify if the user is able to expand the Payment accordion when in SPC shipping page as a Registered user.");
//$t.start();
//try
//{
//	//Click on payment accordian
//	_click($CHECKOUT_LOGINPAGE_SHIPPING_CHECKOUTTAB);
//	//shipping section
//	_assertVisible($CHECKOUT_SHIPPING_SECTION);
//	//Payment section should not be displayed
//	_assertNotVisible($BILLING_PAGE_HEADING);
//}
//catch($e)
//{      
//	_logExceptionAsFailure($e);
//}
//$t.end();
//
//var $t=_testcase("289787","Verify if the user is able to expand the Order Review accordion when in SPC shipping page as a registered user.");
//$t.start();
//try
//{
//	//Click on payment accordian
//	_click($ORDER_REVIEW_HEADING);
//	//shipping section
//	_assertVisible($CHECKOUT_SHIPPING_SECTION);
//	//Payment section should not be displayed
//	_assertNotVisible($ORDER_REVIEW_ITEM_ROW);
//}
//catch($e)
//{      
//	_logExceptionAsFailure($e);
//}
//$t.end();
//
//var $t=_testcase("289768","Verify the functionality of Sign in sections Logout link as a Registered user.");
//$t.start();
//try
//{
//	//Click on logged in heading
//	_click($SHIPPING_LOGGEDIN_HEADER);
//	//logged in section should be opened
//	_assertVisible($CHECKOUT_SHIPPING_CONTENT);
//	//click on logout link
//	_click($SHIPPING_LOGGEDIN_SECTION_LOGOUT);
//	//My account login page
//	_assertVisible($LOGIN_HEADING);
//}
//catch($e)
//{      
//	_logExceptionAsFailure($e);
//}
//$t.end();


cleanup();

//}