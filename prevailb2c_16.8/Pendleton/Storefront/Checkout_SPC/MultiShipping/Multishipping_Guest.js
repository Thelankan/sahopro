_include("../../../GenericLibrary/GlobalFunctions.js");
_include("../../../GenericLibrary/GlobalFunctions.js");



//verifies whether five page or single page checkout

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//_log("object repo"+$MINICART_CHECKOUT);

//***************************************************Pendleton Scripts*******************************************************************************************

var $t = _testcase("352800/352801/","Verify the functionality of Ship to Multiple Addresses yes button in shipping page/Verify the functionality of Ship to single Addresses yes button in shipping page/");
$t.start();
try
{
	//navigate to shipping page
	//navigateToCart($SPC_Multishipping[0][11],$SPC_Multishipping[0][8]); 
	navigateToCartSearch($SPC_Multishipping[0][11],$SPC_Multishipping[0][8]);
	//click on go strait to checkout link
	_click($MINICART_CHECKOUT);
	_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON); 
	//click on yes button 'Do you want multishipping' 
	_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);	
	//verify the UI
	_assertVisible($SHIPPING_SINGLE_ADDRESS_TEXT);	
	var $count=_count("_row","/cart-row/",_in(_div("checkoutmultishipping")));
	for(var $i=0;$i<$count;$i++)
		{
			_assertVisible(_link("/(.*)/",_in(_div("name["+$i+"]"))));
			_assertVisible(_div("/Price/",_in(_div("product-list-item["+$i+"]"))));
			_assertVisible(_div("/Color/",_in(_div("product-list-item["+$i+"]"))));
			_assertVisible(_div("/Size/",_in(_div("product-list-item["+$i+"]"))));
			_assertVisible(_div("/Width/",_in(_div("product-list-item["+$i+"]"))));
			_assertVisible(_div("/Item No/",_in(_div("product-list-item["+$i+"]"))));
			_assertVisible(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"));
			_assertVisible(_span("Add/Edit Address["+$i+"]"));
			//Verify the functionality of Shipping Address Selection in shipping page as guest user
			_assertEqual("Sorry, no addresses are available for this selection.", _getSelectedText(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList")));
		}
	//SHIPPING LOCATION
	_assertVisible($SHIPPING_MULTIPLE_ADDRESS_SHIPPINGLOCATION_HEADERTAB);
	
	//continue to shipping button
	_assertVisible($SHIPPING_MULTIPLE_ADDRESS_SAVE_BUTTON);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("352811/352802","Verify the functionality of Shipping Address Selection in shipping page as guest user");
$t.start();
try
{
	//order summary section
	//_assertVisible($SHIPPING_ORDER_SUMMARY_SECTION);
	//_assertVisible($SHIPPING_ORDER_SUMMARY_HELP_SECTION);
	//checkout indeicator
	_assertEqual("Step 1: Shipping Addresses", _getText(_div("active")));
	_assertEqual("Step 2: Shipping Methods", _getText($CHECKOUT_BILLING_SECTION_INACTIVETAB2));
	_assertEqual("Step 3: Billing", _getText($CHECKOUT_PLACEORDER_SECTION_INACTIVETAB2));
	_assertEqual("Step 4: Place Order", _getText(_div("step-4 inactive")));

	
	//Click on Ship to Single Address yes button 
	_assertVisible($SHIPPING_SINGLE_ADDRESS_TEXT);
	_click($SHIPPING_SINGLE_ADDRESS_BUTTON);
	//9. Application is navigated back to the Multiple Addresses page.
	_assertVisible($SHIPPING_MULTIPLE_ADDRESS);
	_assertNotVisible($SHIPPING_SINGLE_ADDRESS_TEXT);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
cleanup();
var $t = _testcase("352817/352818/352805 ","Verify the navigation/UI on click of 'Add/Edit Address' overlay on Multi Shipping page in Application as a guest  user/erify the functionality of Add/Edit Address Modal on Multi Shipping page in Application as guest user.");
$t.start();
try
{ 
//navigate to shipping page
	navigateToCartSearch($SPC_Multishipping[0][11],$SPC_Multishipping[0][8]);
//click on go strait to checkout link
_click($MINICART_CHECKOUT);
_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON); 
//click on yes button 'Do you want multishipping' 
_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);		
var $count=_count("_row","/cart-row/");
for(var $i=0;$i<$count;$i++)
	{
		_click(_span("Add/Edit Address["+$i+"]"));
		//verify the navigation
		_assertVisible($DIALOG_OVERLAY);
		//verify the UI
		_assert(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_HEADING));
		_assert(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_CLOSE_BUTTON));
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_SELECTADDRESS_LABEL);
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_SELECTADDRESS_DROPDOWN);
		//first name	
		_assert(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_FIRSTNAME_LABEL));
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_FIRSTNAME_TEXTBOX);
		//last name
		_assert(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_LASTNAME_LABEL));
		_assert(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_LASTNAME_TEXTBOX));
		//address1
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS1_LABEL);
		_assert(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS1_TEXTBOX));
		//address2
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS2_LABEL);
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS2_TEXTBOX);
		//Country
		_assert(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_COUNTRY_LABEL));
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_COUNTRY_TEXTBOX);
		//State
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_STATE_LABEL);
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_STATE_TEXTBOX);
		//City
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_CITY_LABEL);
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_CITY_TEXTBOX);
		//Zip Code
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_POSTAL_LABEL);
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_POSTAL_TEXTBOX);  
		//Phone
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_LABEL);
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_TEXTBOX);
		if(!isMobile())
		{
		_assert(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS_TOOLTIP));
		}
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_SAVE_BUTTON);
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_CANCEL_BUTTON);
		_click($MULTISHIPPING_EDITADDRESSOVERLAY_CANCEL_BUTTON);

	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
//*********************************************************validation**********************************
var $t = _testcase("352814/352815", "Verify the validation related to 'Address Name','First Name','Last  Name','Address 1','Address 2','City','Phone',country and state fields functionality in the Add/Edit address overlay of multi shipping page as a Guest user.");
$t.start();
try
{
	
	//_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);	
	for(var $i=0;$i<$EditAddress_Validation.length;$i++)
		{
			_log($i);
			_click($MULTISHIPPING_EDITADDRESS_LINK);
			_wait(5000, _isVisible($DIALOG_OVERLAY));
			 if(_isVisible($DIALOG_OVERLAY))
				 {
				 addAddressMultiShip($EditAddress_Validation,$i);
					
					 //blank field validation
					 if($i==0)
						 {
						 	_click($MULTISHIPPING_EDITADDRESSOVERLAY_SAVE_BUTTON);
							//_assertEqual($EditAddress_Validation[0][15], _style($ADDRESSES_ADDRESSNAME_TEXTBOX, "background-color"));
							//_assertEqual($EditAddress_Validation[0][15], _style($MULTISHIPPING_EDITADDRESSOVERLAY_FIRSTNAME_TEXTBOX, "background-color"));
							//_assertEqual($EditAddress_Validation[0][15], _style($MULTISHIPPING_EDITADDRESSOVERLAY_LASTNAME_TEXTBOX, "background-color"));
							//_assertEqual($EditAddress_Validation[0][15], _style($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS1_TEXTBOX, "background-color"));
							//_assertEqual($EditAddress_Validation[0][15], _style($ADDRESSES_COUNTRY_DROPDOWN, "background-color"));
							//_assertEqual($EditAddress_Validation[0][15], _style($MULTISHIPPING_EDITADDRESSOVERLAY_STATE_TEXTBOX, "background-color"));
							//_assertEqual($EditAddress_Validation[0][15], _style($MULTISHIPPING_EDITADDRESSOVERLAY_CITY_TEXTBOX, "background-color"));
							//_assertEqual($EditAddress_Validation[0][15], _style($MULTISHIPPING_EDITADDRESSOVERLAY_POSTAL_TEXTBOX, "background-color"));
					      //  _assertEqual($EditAddress_Validation[0][15], _style($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_TEXTBOX, "background-color")); 
					       // _click($MULTISHIPPING_EDITADDRESSOVERLAY_CLOSE_BUTTON);
						 	_assertEqual("This field is required.", _getText($MULTISHIPPING_EDITADDRESSOVERLAY_FIRSTNAME_ERROR));
						 	_assertEqual("This field is required.", _getText($MULTISHIPPING_EDITADDRESSOVERLAY_LASTNAME_ERROR));
						 	_assertEqual("This field is required.", _getText($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS1_ERROR));
						 	_assertEqual("This field is required.", _getText($MULTISHIPPING_EDITADDRESSOVERLAY_STATE_ERROR));
						 	_assertEqual("This field is required.", _getText($MULTISHIPPING_EDITADDRESSOVERLAY_CITY_ERROR));
						 	_assertEqual("This field is required.", _getText($MULTISHIPPING_EDITADDRESSOVERLAY_POSTAL_ERROR));
						 	_assertEqual("This field is required.", _getText($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_ERROR));
						 	_assertEqual("rgb(169, 17, 24)", _style($MULTISHIPPING_EDITADDRESSOVERLAY_FIRSTNAME_ERROR,"color"));
						 	_assertEqual("rgb(169, 17, 24)", _style($MULTISHIPPING_EDITADDRESSOVERLAY_LASTNAME_ERROR,"color"));
						 	_assertEqual("rgb(169, 17, 24)", _style($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS1_ERROR,"color"));
						 	_assertEqual("rgb(169, 17, 24)", _style($MULTISHIPPING_EDITADDRESSOVERLAY_STATE_ERROR,"color"));
						 	_assertEqual("rgb(169, 17, 24)", _style($MULTISHIPPING_EDITADDRESSOVERLAY_CITY_ERROR,"color"));
						 	_assertEqual("rgb(169, 17, 24)", _style($MULTISHIPPING_EDITADDRESSOVERLAY_POSTAL_ERROR,"color"));
						 	_assertEqual("rgb(169, 17, 24)", _style($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_ERROR,"color"));

						 }
					 //cancel functionality without entering values
					 else if($i==1)
						 {
						 	
						 	_click($MULTISHIPPING_EDITADDRESSOVERLAY_CLOSE_BUTTON);
						 	_assertNotVisible($DIALOG_OVERLAY);
						 	_assertNotVisible($MULTISHIPPING_EDITADDRESSOVERLAY_SELECTADDRESS_DROPDOWN);
						 }
					 //Special characters, Phone number inavlid format and alphanumeric value
					 else if($i==2 || $i==3 ||$i==5)
						 {
						
						 	_click($MULTISHIPPING_EDITADDRESSOVERLAY_SAVE_BUTTON);
						 	_assertVisible(_span($EditAddress_Validation[$i][14]));
						 	 if(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_ERROR))
								{
									_assertEqual($EditAddress_Validation[$i][12],_getText($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_ERROR));
								}
						 	 else if(_isVisible(_span($EditAddress_Validation[$i][12])))
						 		 {
						 		 _assertVisible(_span($EditAddress_Validation[$i][12]));
						 		 }
			            
						 	
						 	 _click($MULTISHIPPING_EDITADDRESSOVERLAY_CLOSE_BUTTON);
						 }
					 //Max characters
					 else if($i==4)
						 {
						 		
						 		 //_assertEqual($EditAddress_Validation[0][13],_getText($ADDRESSES_ADDRESSNAME_TEXTBOX).length);  
								 _assertEqual($EditAddress_Validation[0][13],_getText($MULTISHIPPING_EDITADDRESSOVERLAY_FIRSTNAME_TEXTBOX).length);
								 _assertEqual($EditAddress_Validation[1][13],_getText($MULTISHIPPING_EDITADDRESSOVERLAY_LASTNAME_TEXTBOX).length);
								 _assertEqual($EditAddress_Validation[3][13],_getText($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS1_TEXTBOX).length);
								 _assertEqual($EditAddress_Validation[4][13],_getText($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS2_TEXTBOX).length);
								 _assertEqual($EditAddress_Validation[8][13],_getText($MULTISHIPPING_EDITADDRESSOVERLAY_CITY_TEXTBOX).length);  
								 _assertEqual($EditAddress_Validation[1][13],_getText($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_TEXTBOX).length); 
								 _click($MULTISHIPPING_EDITADDRESSOVERLAY_CLOSE_BUTTON);
						 }
					//cancel functionality after entering values
					 else if($i==6)
						 {
							_click($MULTISHIPPING_EDITADDRESSOVERLAY_CANCEL_BUTTON);
						 	_assertNotVisible($DIALOG_OVERLAY);
						 	_assertNotVisible($MULTISHIPPING_EDITADDRESSOVERLAY_SELECTADDRESS_DROPDOWN);					 		
						 }
					 
					 else if($i==8)
						 {
						 _call(window.history.back());
							 _click($MULTISHIPPING_EDITADDRESSOVERLAY_SAVE_BUTTON);
								 	 
						 	 if(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_ERROR_MESSAGE))
								{
									_assertEqual($EditAddress_Validation[$i][11],_getText($MULTISHIPPING_EDITADDRESSOVERLAY_ERROR_MESSAGE));
								}
//			                else
//								{
//								    _assert(false, "Error message for Address name field is not displayed as expected");
//								}
							 
						 	 _click($MULTISHIPPING_EDITADDRESSOVERLAY_CLOSE_BUTTON);
						 }
					 else
						 {
						 _setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i1_addressList"), "1");
						 _click(_submit("Continue to Shipping"));
						 
						 _assertEqual($EditAddress_Validation[$i][11], _getText(_div("address", _in(_div("mini-shipment order-component-block  first ")))));

//						   //verify the saved address
//						 	_click($MULTISHIPPING_EDITADDRESSOVERLAY_SAVE_BUTTON);
//						 	_assertEqual($EditAddress_Validation[$i][1],_getText($ADDRESSES_MINIADDRESS_TITLE));
//						 	_assertEqual($EditAddress_Validation[$i][2]+" "+$EditAddress_Validation[$i][3],_getText($ADDRESSES_MINIADDRESS_NAME));
//						 	_assertEqual($EditAddress_Validation[$i][11], _getText($ADDRESS_MINIADDRESS_LOCATION));						 
						 }					 
				 }
			 else
				 {
					 _assert(false,"Add address Overlay is not displaying"); 
				 }			
		} 
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();



var $t = _testcase("128321","Verify the functionality of 'Save' button in the  'Add or Edit Addresses' overlay  on Multi Shipping page  as a Anonymous user");
$t.start();
try
{	
//navigate to shipping page
_click($MINICART_CHECKOUT);
_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON); 
//click on yes button 'Do you want multishipping' 
_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);	
//click on add/edit link
_click($MULTISHIPPING_EDITADDRESS_LINK);
//enter the address
_setValue($MULTISHIPPING_EDITADDRESSOVERLAY_FIRSTNAME_TEXTBOX, $addr_data2[0][1]);
_setValue($MULTISHIPPING_EDITADDRESSOVERLAY_LASTNAME_TEXTBOX,$addr_data2[0][2]);
_setValue($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS1_TEXTBOX, $addr_data2[0][3]);
_setValue($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS2_TEXTBOX, $addr_data2[0][4]);
_setSelected($MULTISHIPPING_EDITADDRESSOVERLAY_COUNTRY_TEXTBOX, $addr_data2[0][5]);
_setSelected($MULTISHIPPING_EDITADDRESSOVERLAY_STATE_TEXTBOX, $addr_data2[0][6]);
_setValue($MULTISHIPPING_EDITADDRESSOVERLAY_CITY_TEXTBOX, $addr_data2[0][7]);
_setValue($MULTISHIPPING_EDITADDRESSOVERLAY_POSTAL_TEXTBOX, $addr_data2[0][8]);  
_setValue($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_TEXTBOX, $addr_data2[0][9]);
//click on save button
_click($MULTISHIPPING_EDITADDRESSOVERLAY_SAVE_BUTTON);
//verify the address prepopulation in dropdown
var $count=_count("_table","/item-list/",_in(_div("checkoutmultishipping")));
for(var $i=0;$i<$count;$i++)
	{
	_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"),"1");
	var $length=_getText(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList")).length;
	var $savedAddress=_getText(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"))[$length-1].split(",");
	//verifying the address
	_assertEqual($addr_data2[0][1]+" "+$addr_data2[0][2],$savedAddress[0]);
	_assertEqual($addr_data2[0][3],$savedAddress[1]);
	_assertEqual($addr_data2[0][7],$savedAddress[2]);
	_assertEqual($addr_data2[1][6],$savedAddress[3]);
	_assertEqual($addr_data2[0][8],$savedAddress[4]);
	//_assertEqual($addr_data2[0][9],$savedAddress[5]);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


ClearCartItems();
var $t = _testcase("352823 /352824 /352822 /352819/352813 ","verify the number of line items displayed in order summary section of Multi shipping page when one product with 2 qty is added to basket and 'Continue button functionality' below the order total Section on Multi Shipping page in Application as a Registered  user");
$t.start();
try
{
	//navigate to shipping page
	navigateToCartSearch($SPC_Multishipping[0][11],$SPC_Multishipping[0][8]);
	//click on go strait to checkout link
	_click($MINICART_CHECKOUT);
	_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON); 
	//click on yes button 'Do you want multishipping' 
	_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
	
	//no of products in order summary
	var $noOfProducts=_count("_div","/mini-cart-product/",_in(_div("nav summary")));
	_assertEqual($SPC_Multishipping[1][4],$noOfProducts);
	//Qty
	var $SHIPPING_RIGHT_NAV_QUANT=_extract(_getText(_div("mini-cart-pricing", _in(_div("checkout-mini-cart")))),"/[: ](.*)[ $]/",true);
	_assertEqual($SPC_Multishipping[0][8],_getText($SHIPPING_RIGHT_NAV_QUANT));
	var $addrCount=0;
	//adding addresses
	for(var $i=0;$i<$addr_data1.length;$i++)
	{
		_click(_span("Add/Edit Address["+$i+"]"));
		addAddressMultiShip($addr_data1,$i);
		//$addrCount++;
	}
	//click on continue
	_click($SHIPPING_MULTIPLE_ADDRESS_SAVE_BUTTON);
	
	//verify navigation of shipment method page
	_assertVisible($MULTISHIPPING_SELECTSHIPPINGMETHOD_DROPDOWN);
	_assertEqual("/Shipping Methods/", _getText($CHECKOUT_SHIPPING_SECTION_ACTIVETAB2));
	var $ShippingAddress=_collect("_cell","/item-shipping-address/", _in($CHECKOUT_MULTISHIP));
	
	for (var $i = 0; $i < $ShippingAddress.length; $i++) 
	{
		_assertEqual($addr_data1[$i][13],_getText($ShippingAddress[$i]));
		_check(_radio("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_isGift"));
		_setValue(_textbox("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMsgLine1"), "Gift For You");
	}
	_click(_submit("button-fancy-large"));
	
	BillingAddress($addr_data2, 0);
	PaymentDetails($Payment_Data, 0);
	//click on the continue
	_click(_submit("dwfrm_billing_save"));
	//_assert(false,"needs to scripts not navigating to order review page");
	_assertEqual("Shipping test test 1706 N NAGLE AVE Illinois, IL 12345 United States", _getText(_div("details", _in(_div("mini-billing-address order-component-block")))));
	_assertEqual($Payment_Data[0][1], _getText(_div("cc-owner", _in(_div("mini-payment-instrument order-component-block  first ")))));
	_assertEqual($Payment_Data[0][2], _getText(_div("cc-type", _in(_div("mini-payment-instrument order-component-block  first ")))));
	_assertEqual($Payment_Data[0][7], _getText(_div("cc-number", _in(_div("mini-payment-instrument order-component-block  first ")))));
	_assertEqual($Payment_Data[0][8], _getText(_div("cc-exp", _in(_div("mini-payment-instrument order-component-block  first ")))));
	_assertEqual("Place Your Order", _getText(_submit("button-fancy-large")));

	
	var $MultiShip_Price=_collect("_cell","/item-total/", _in(_table("cart-table")));
	var $prodPrice=0;
	for (var $i = 0; $i < $MultiShip_Price.length; $i++)
	{
		var $ItemPrice=parseFloat(_extract(_getText($MultiShip_Price[$i]),"/[$](.*)/",true));
		$prodPrice=$prodPrice+$ItemPrice;
		_log($prodPrice);
		
	}
	//verify the sub total 
	var $Exp_OrderTot=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true);
	
	_assertEqual($prodPrice,$Exp_OrderTot);
	//extraxt Shipping cost
	var $ShippingCharge=parseFloat(_extract(_getText(_row("order-shipping")),"/[$](.*)/",true));
	var $Exp_orderTotal=$prodPrice+$ShippingCharge;
	var $Actual_orderTotal=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
	_assertEqual($Exp_orderTotal, $Actual_orderTotal);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("352813","Verify the UI of the Order Review section in Multi shipping Page of Select Shipping method section");
$t.start();
try
{
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
