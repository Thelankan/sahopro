_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("SPC_Multishipping.xls");

SiteURLs();
cleanup();

//Login to the Application
_click(_link("user-login"));
 login();

//*****************************************************************************Pendleton scrits***************************************
var $t = _testcase("352800/352801","Verify the functionality of Ship to Multiple Addresses yes button in shipping page/Verify the functionality of Ship to single Addresses yes button in shipping page/");
$t.start();
try
{
	//navigate to shipping page
	navigateToCartSearch($SPC_Multishipping[0][1],$SPC_Multishipping[0][8]); 
	//click on go strait to checkout link
	_click($MINICART_CHECKOUT);
	//_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON); 
	//click on yes button 'Do you want multishipping' 
	_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);	
	//verify the UI
	_assertVisible($SHIPPING_SINGLE_ADDRESS_TEXT);	
	var $count=_count("_row","/cart-row/",_in(_div("checkoutmultishipping")));
	for(var $i=0;$i<$count;$i++)
		{
			_assertVisible(_link("/(.*)/",_in(_div("name["+$i+"]"))));
			_assertVisible(_div("/Price/",_in(_div("product-list-item["+$i+"]"))));
			_assertVisible(_div("/Color/",_in(_div("product-list-item["+$i+"]"))));
			_assertVisible(_div("/Size/",_in(_div("product-list-item["+$i+"]"))));
			//_assertVisible(_div("/Width/",_in(_div("product-list-item["+$i+"]"))));
			_assertVisible(_div("/Item No/",_in(_div("product-list-item["+$i+"]"))));
			_assertVisible(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"));
			_assertVisible(_span("Add/Edit Address["+$i+"]"));
			//Verify the functionality of Shipping Address Selection in shipping page as guest user
			_assertEqual($SPC_Multishipping[1][0], _getSelectedText(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList")));
		}
	//SHIPPING LOCATION
	_assertVisible($SHIPPING_MULTIPLE_ADDRESS_SHIPPINGLOCATION_HEADERTAB);
	
	//continue to shipping button
	_assertVisible($SHIPPING_MULTIPLE_ADDRESS_SAVE_BUTTON);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("352812 /352802","Verify the functionality of Shipping Address Selection in shipping page as guest user");
$t.start();
try
{
	//order summary section
	_assertVisible($SHIPPING_ORDER_SUMMARY_SECTION);
	_assertVisible($SHIPPING_ORDER_SUMMARY_HELP_SECTION);
	//checkout indicator
	_assertEqual("Step 1: Shipping Addresses", _getText(_div("active")));
	_assertEqual("Step 2: Shipping Methods", _getText(_div("step-2 inactive")));
	_assertEqual("Step 3: Billing", _getText(_div("step-3 inactive")));
	_assertEqual("Step 4: Place Order", _getText(_div("step-4 inactive")));
	
	//Click on Ship to Single Address yes button 
	_assertVisible($SHIPPING_SINGLE_ADDRESS_TEXT);
	_click($SHIPPING_SINGLE_ADDRESS_BUTTON);
	//9. Application is navigated back to the Multiple Addresses page.
	_assertVisible($SHIPPING_MULTIPLE_ADDRESS);
	_assertNotVisible($SHIPPING_SINGLE_ADDRESS_TEXT);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//clear cart items
ClearCartItems();

var $t = _testcase("352817/352818/352805 ","Verify the navigation/UI on click of 'Add/Edit Address' overlay on Multi Shipping page in Application as a guest  user/erify the functionality of Add/Edit Address Modal on Multi Shipping page in Application as guest user.");
$t.start();
try
{ 
//navigate to shipping page
navigateToCartSearch($SPC_Multishipping[0][1],$SPC_Multishipping[0][8]); 
//click on go strait to checkout link
_click($MINICART_CHECKOUT);
//_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON); 
//click on yes button 'Do you want multishipping' 
_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);		
var $count=_count("_row","/cart-row/");
for(var $i=0;$i<$count;$i++)
	{
		_click(_span("Add/Edit Address["+$i+"]"));
		//verify the navigation
		_assertVisible($DIALOG_OVERLAY);
		//verify the UI
		_assert(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_HEADING));
		_assert(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_CLOSE_BUTTON));
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_SELECTADDRESS_LABEL);
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_SELECTADDRESS_DROPDOWN);
		//first name	
		_assert(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_FIRSTNAME_LABEL));
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_FIRSTNAME_TEXTBOX);
		//last name
		_assert(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_LASTNAME_LABEL));
		_assert(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_LASTNAME_TEXTBOX));
		//address1
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS1_LABEL);
		_assert(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS1_TEXTBOX));
		//address2
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS2_LABEL);
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS2_TEXTBOX);
		//Country
		_assert(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_COUNTRY_LABEL));
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_COUNTRY_TEXTBOX);
		//State
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_STATE_LABEL);
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_STATE_TEXTBOX);
		//City
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_CITY_LABEL);
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_CITY_TEXTBOX);
		//Zip Code
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_POSTAL_LABEL);
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_POSTAL_TEXTBOX);  
		//Phone
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_LABEL);
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_TEXTBOX);
		if(!isMobile())
		{
		_assert(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS_TOOLTIP));
		}
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_SAVE_BUTTON);
		_assertVisible($MULTISHIPPING_EDITADDRESSOVERLAY_CANCEL_BUTTON);
		_click($MULTISHIPPING_EDITADDRESSOVERLAY_CANCEL_BUTTON);

	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
//*********************************************************validation**********************************
var $t = _testcase("352814/352815", "Verify the validation related to 'Address Name','First Name','Last  Name','Address 1','Address 2','City','Phone',country and state fields functionality in the Add/Edit address overlay of multi shipping page as a Guest user.");
$t.start();
try
{
	
	//_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);	
	for(var $i=0;$i<$EditAddress_Validation.length;$i++)
		{
			_log($i);
			_click($MULTISHIPPING_EDITADDRESS_LINK);
			_wait(5000, _isVisible($DIALOG_OVERLAY));
			 if(_isVisible($DIALOG_OVERLAY))
				 {
				 addAddressMultiShip($EditAddress_Validation,$i);
					
					 //blank field validation
					 if($i==0)
						 {
						 	_click($MULTISHIPPING_EDITADDRESSOVERLAY_SAVE_BUTTON);
							//_assertEqual($EditAddress_Validation[0][15], _style($ADDRESSES_ADDRESSNAME_TEXTBOX, "background-color"));
							//_assertEqual($EditAddress_Validation[0][15], _style($MULTISHIPPING_EDITADDRESSOVERLAY_FIRSTNAME_TEXTBOX, "background-color"));
							//_assertEqual($EditAddress_Validation[0][15], _style($MULTISHIPPING_EDITADDRESSOVERLAY_LASTNAME_TEXTBOX, "background-color"));
							//_assertEqual($EditAddress_Validation[0][15], _style($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS1_TEXTBOX, "background-color"));
							//_assertEqual($EditAddress_Validation[0][15], _style($ADDRESSES_COUNTRY_DROPDOWN, "background-color"));
							//_assertEqual($EditAddress_Validation[0][15], _style($MULTISHIPPING_EDITADDRESSOVERLAY_STATE_TEXTBOX, "background-color"));
							//_assertEqual($EditAddress_Validation[0][15], _style($MULTISHIPPING_EDITADDRESSOVERLAY_CITY_TEXTBOX, "background-color"));
							//_assertEqual($EditAddress_Validation[0][15], _style($MULTISHIPPING_EDITADDRESSOVERLAY_POSTAL_TEXTBOX, "background-color"));
					      //  _assertEqual($EditAddress_Validation[0][15], _style($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_TEXTBOX, "background-color")); 
					       // _click($MULTISHIPPING_EDITADDRESSOVERLAY_CLOSE_BUTTON);
						 	_assertEqual("This field is required.", _getText($MULTISHIPPING_EDITADDRESSOVERLAY_FIRSTNAME_ERROR));
						 	_assertEqual("This field is required.", _getText($MULTISHIPPING_EDITADDRESSOVERLAY_LASTNAME_ERROR));
						 	_assertEqual("This field is required.", _getText($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS1_ERROR));
						 	_assertEqual("This field is required.", _getText($MULTISHIPPING_EDITADDRESSOVERLAY_STATE_ERROR));
						 	_assertEqual("This field is required.", _getText($MULTISHIPPING_EDITADDRESSOVERLAY_CITY_ERROR));
						 	_assertEqual("This field is required.", _getText($MULTISHIPPING_EDITADDRESSOVERLAY_POSTAL_ERROR));
						 	_assertEqual("This field is required.", _getText($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_ERROR));
						 	_assertEqual("rgb(169, 17, 24)", _style($MULTISHIPPING_EDITADDRESSOVERLAY_FIRSTNAME_ERROR,"color"));
						 	_assertEqual("rgb(169, 17, 24)", _style($MULTISHIPPING_EDITADDRESSOVERLAY_LASTNAME_ERROR,"color"));
						 	_assertEqual("rgb(169, 17, 24)", _style($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS1_ERROR,"color"));
						 	_assertEqual("rgb(169, 17, 24)", _style($MULTISHIPPING_EDITADDRESSOVERLAY_STATE_ERROR,"color"));
						 	_assertEqual("rgb(169, 17, 24)", _style($MULTISHIPPING_EDITADDRESSOVERLAY_CITY_ERROR,"color"));
						 	_assertEqual("rgb(169, 17, 24)", _style($MULTISHIPPING_EDITADDRESSOVERLAY_POSTAL_ERROR,"color"));
						 	_assertEqual("rgb(169, 17, 24)", _style($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_ERROR,"color"));

						 }
					 //cancel functionality without entering values
					 else if($i==1)
						 {
						 	
						 	_click($MULTISHIPPING_EDITADDRESSOVERLAY_CLOSE_BUTTON);
						 	_assertNotVisible($DIALOG_OVERLAY);
						 	_assertNotVisible($MULTISHIPPING_EDITADDRESSOVERLAY_SELECTADDRESS_DROPDOWN);
						 }
					 //Special characters, Phone number inavlid format and alphanumeric value
					 else if($i==2 || $i==3 ||$i==5)
						 {
						
						 	_click($MULTISHIPPING_EDITADDRESSOVERLAY_SAVE_BUTTON);
						 	_assertVisible(_span($EditAddress_Validation[$i][14]));
						 	 if(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_ERROR))
								{
									_assertEqual($EditAddress_Validation[$i][12],_getText($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_ERROR));
								}
						 	 else if(_isVisible(_span($EditAddress_Validation[$i][12])))
						 		 {
						 		 _assertVisible(_span($EditAddress_Validation[$i][12]));
						 		 }
			            
						 	
						 	 _click($MULTISHIPPING_EDITADDRESSOVERLAY_CLOSE_BUTTON);
						 }
					 //Max characters
					 else if($i==4)
						 {
						 		
						 		 //_assertEqual($EditAddress_Validation[0][13],_getText($ADDRESSES_ADDRESSNAME_TEXTBOX).length);  
								 _assertEqual($EditAddress_Validation[0][13],_getText($MULTISHIPPING_EDITADDRESSOVERLAY_FIRSTNAME_TEXTBOX).length);
								 _assertEqual($EditAddress_Validation[1][13],_getText($MULTISHIPPING_EDITADDRESSOVERLAY_LASTNAME_TEXTBOX).length);
								 _assertEqual($EditAddress_Validation[3][13],_getText($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS1_TEXTBOX).length);
								 _assertEqual($EditAddress_Validation[4][13],_getText($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS2_TEXTBOX).length);
								 _assertEqual($EditAddress_Validation[8][13],_getText($MULTISHIPPING_EDITADDRESSOVERLAY_CITY_TEXTBOX).length);  
								 _assertEqual($EditAddress_Validation[1][13],_getText($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_TEXTBOX).length); 
								 _click($MULTISHIPPING_EDITADDRESSOVERLAY_CLOSE_BUTTON);
						 }
					//cancel functionality after entering values
					 else if($i==6)
						 {
							_click($MULTISHIPPING_EDITADDRESSOVERLAY_CANCEL_BUTTON);
						 	_assertNotVisible($DIALOG_OVERLAY);
						 	_assertNotVisible($MULTISHIPPING_EDITADDRESSOVERLAY_SELECTADDRESS_DROPDOWN);					 		
						 }
					 
					 else if($i==8)
						 {
						 _call(window.history.back());
							 _click($MULTISHIPPING_EDITADDRESSOVERLAY_SAVE_BUTTON);
								 	 
						 	 if(_isVisible($MULTISHIPPING_EDITADDRESSOVERLAY_ERROR_MESSAGE))
								{
									_assertEqual($EditAddress_Validation[$i][11],_getText($MULTISHIPPING_EDITADDRESSOVERLAY_ERROR_MESSAGE));
								}
//			                else
//								{
//								    _assert(false, "Error message for Address name field is not displayed as expected");
//								}
							 
						 	 _click($MULTISHIPPING_EDITADDRESSOVERLAY_CLOSE_BUTTON);
						 }
					 else
						 {
						 _setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i1_addressList"), "1");
						 _click(_submit("Continue to Shipping"));
						 
						 _assertEqual($EditAddress_Validation[$i][11], _getText(_div("address", _in(_div("mini-shipment order-component-block  first ")))));

//						   //verify the saved address
//						 	_click($MULTISHIPPING_EDITADDRESSOVERLAY_SAVE_BUTTON);
//						 	_assertEqual($EditAddress_Validation[$i][1],_getText($ADDRESSES_MINIADDRESS_TITLE));
//						 	_assertEqual($EditAddress_Validation[$i][2]+" "+$EditAddress_Validation[$i][3],_getText($ADDRESSES_MINIADDRESS_NAME));
//						 	_assertEqual($EditAddress_Validation[$i][11], _getText($ADDRESS_MINIADDRESS_LOCATION));						 
						 }					 
				 }
			 else
				 {
					 _assert(false,"Add address Overlay is not displaying"); 
				 }			
		} 
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();

//clean up
cleanup();

var $t = _testcase("352807/352808", "Verify whether shipping addresses are prepopulated in the dropdown / display of Place holder text in Select an address drop down of multi shipping in SPC checkout pages/on Multi Shipping page in Application as a Registered user");
$t.start();
try
{
	
	//Login to the Application
	_click(_link("user-login"));
	 login();
	
	//navigate to address page
	_click($HEADER_MYACCOUNT_LINK);
	_click($MY_ACCOUNT_ADDRESSES_OPTIONS); 
	
  for(var $i=0;$i<2;$i++)
	{
		_log($i);
		_click($ADDRESSES_CREATENEW_ADDRESS_LINK);
		_wait(2000);
		 addAddress($Valid_Address,$i);
		 _click($ADDRESSES_OVERLAY_APPLYBUTTON);
		 _wait(4000);

	}
	//navigate to cart page
	navigateToCartSearch($SPC_Multishipping[0][1],2);	
	//click on go strait to checkout link
	_click($MINICART_CHECKOUT);
	//click on yes button 'Do you want multishipping' 
	if(_isVisible($SHIPPING_MULTISHIPPING_TEXT))
	{
		_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
	}
	//verify the presence of shipping address drop downs
	var $count=_count("_div","/name/",_in(_table("item-list")));
	for(var $i=0;$i<$count;$i++)
		{
			//select an address place holder
		    _assertEqual("Select an address", _getSelectedText(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList")));
		    
    		_assertVisible(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"));
			_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"), "/"+$addr_data[0][1]+"/");
			//verify whether the addresses are present or not
			//verifying first address
			var $savedAddress=_getSelectedText(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList")).split(",");
			_assertEqual("("+$addr_data[0][1]+") "+$addr_data[0][4],$savedAddress[0]);
			_assertEqual($addr_data[0][8],$savedAddress[1]);
			_assertEqual($addr_data[0][12],$savedAddress[2]);
			_assertEqual($addr_data[0][9],$savedAddress[3]);
			//verifying the second address
			_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"), "/"+$addr_data[1][1]+"/");
			var $savedAddress1=_getSelectedText(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList")).split(",");
			_assertEqual("("+$addr_data[1][1]+") "+$addr_data[1][4],$savedAddress1[0]);
			_assertEqual($addr_data[1][8],$savedAddress1[1]);
			_assertEqual($addr_data[1][12],$savedAddress1[2]);
			_assertEqual($addr_data[1][9],$savedAddress1[3]);
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

deleteAddress();

var $t = _testcase("352810 /125956","Verify the functionality of Add to Address book checkbox in the overlay and functionality of 'Save' button in the  'Add or Edit Addresses' overlay  on Multi Shipping page in Prevail application as a Anonymous user");
$t.start();
try
{
	//navigate to cart page
	navigateToCartSearch($SPC_Multishipping[0][1],2);	
	//click on go strait to checkout link
	_click($MINICART_CHECKOUT);
	//navigate to shipping page
	_click($MINICART_CHECKOUT);
	if(_isVisible($SHIPPING_MULTIPLE_ADDRESS_BUTTON))
		{
	//click on yes button 'Do you want multishipping' 
	_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
		}
//click on add/edit link
_click($MULTISHIPPING_EDITADDRESS_LINK);
//enter the address
_setValue($MULTISHIPPING_EDITADDRESSOVERLAY_FIRSTNAME_TEXTBOX, $addr_data2[0][1]);
_setValue($MULTISHIPPING_EDITADDRESSOVERLAY_LASTNAME_TEXTBOX,$addr_data2[0][2]);
_setValue($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS1_TEXTBOX, $addr_data2[0][3]);
_setValue($MULTISHIPPING_EDITADDRESSOVERLAY_ADDRESS2_TEXTBOX, $addr_data2[0][4]);
_setSelected($MULTISHIPPING_EDITADDRESSOVERLAY_COUNTRY_TEXTBOX, $addr_data2[0][5]);
_setSelected($MULTISHIPPING_EDITADDRESSOVERLAY_STATE_TEXTBOX, $addr_data2[0][6]);
_setValue($MULTISHIPPING_EDITADDRESSOVERLAY_CITY_TEXTBOX, $addr_data2[0][7]);
_setValue($MULTISHIPPING_EDITADDRESSOVERLAY_POSTAL_TEXTBOX, $addr_data2[0][8]); 
_setValue($MULTISHIPPING_EDITADDRESSOVERLAY_PHONE_TEXTBOX, $addr_data2[0][9]);
//click on add to address book check box
_check($MULTISHIPPING_EDITADDRESSOVERLAY_ADDTOADDRESS_CHECKBOX);
//click on save button
_click($MULTISHIPPING_EDITADDRESSOVERLAY_SAVE_BUTTON);
//verify the address pre population in drop down
var $count=_count("_div","/name/",_in(_table("item-list")));
for(var $i=0;$i<$count;$i++)
	{
	_setSelecetd(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"),"1");
	var $length=_getText(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList")).length;
	var $savedAddress=_getText(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"))[$length-1].split(",");
	//verifying the address
	_assertEqual("/"+$addr_data2[0][7]+"/",$savedAddress[0]);
	_assertEqual($addr_data2[0][4],$savedAddress[1]);
	_assertEqual($addr_data2[0][7],$savedAddress[2]);
	_assertEqual($addr_data2[1][6],$savedAddress[3]);
	_assertEqual($addr_data2[0][8],$savedAddress[4]);
	//_assertEqual($addr_data2[0][9],$savedAddress[5]);
	}
//navigate to addresses and verify whether address got saved or not
_click($HEADER_MYACCOUNT_LINK);
//click on addresses
_click($MY_ACCOUNT_ADDRESSES_OPTIONS);
//verify the saved address
var $address= $addr_data2[0][7]+" "+$addr_data2[0][1]+" "+$addr_data2[0][2]+" "+$addr_data2[0][3]+" "+$addr_data2[0][4]+" "+$addr_data2[0][7]+", "+$addr_data2[1][6]+" "+$addr_data2[0][8]+" "+$addr_data2[0][5]+" Phone: "+$addr_data2[0][9];
_assertVisible(_listItem("/"+$address+"/"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

