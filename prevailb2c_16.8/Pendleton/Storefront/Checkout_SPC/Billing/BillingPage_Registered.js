_include("../../../GenericLibrary/BM_Functions.js");
_include("../../../GenericLibrary/GlobalFunctions.js");
_include("../../../ObjectRepository/Checkout/Shipping_OR.sah");
_include("../../../ObjectRepository/Checkout/Billing_OR.sah");
_include("../../../ObjectRepository/Checkout/Checkout_Login_OR.sah");
_include("../../../ObjectRepository/ShopNav/HomePage_OR.sah");

_resource("BillingPage.xls");

var $sheetName;
var $rowNum;
var $CProductName;
var $CQuantity;


SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//Login
_click($HEADER_LOGIN_LINK);
login();
//removing item from cart
ClearCartItems();
deleteAddress();
//DeleteCreditCard();
//var $UserEmail=$uId;

var $billingAddress=$Valid_Data[0][1]+" "+$Valid_Data[0][2]+" "+$Valid_Data[0][3]+" "+$Valid_Data[0][4]+" "+$Valid_Data[0][7]+", "+$Valid_Data[1][6]+" "+$Valid_Data[0][8]+" "+$Valid_Data[1][5]+" "+$Valid_Data[0][9];
var $billingAddress2=$Valid_Data[2][1]+" "+$Valid_Data[2][2]+" "+$Valid_Data[2][3]+" "+$Valid_Data[2][4]+" "+$Valid_Data[2][7]+", "+$Valid_Data[2][6]+" "+$Valid_Data[2][8]+" "+$Valid_Data[2][5];

var $t=_testcase("352568","Verify the navigation to Checkout Billing page in Application as a Registered user.");
$t.start();
try
{
	
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping details
	shippingAddress($Valid_Data,0);
	var $shippingMethod=_extract(_getText($SHIPPING_RIGHT_NAV_SHIPPING_COST),"/Shipping(.*)[$]/",true).toString().trim();
	_click($SHIPPING_SUBMIT_BUTTON);
	//verify the navigation to Billing page
	_assertVisible(_div("/"+$Generic[0][1]+"/"));
	_assertVisible(_fieldset("/"+$Generic[6][0]+"/"));
	_assertVisible($BILLING_CONTINUE_BUTTON);
	
}
catch($e)	
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("287893/352584/352586/352575","Verify the UI of 'BILLING' page,'ENTER GIFT CERTIFICATE OR COUPON/DISCOUNT CODES' section and 'SELECT PAYMENT METHOD' and 'ORDER SUMMARY' section/UI of SELECT PAYMENT METHOD Section with no saved cards/no saved address in an Application as a Registered user.");
$t.start();
try
{
	
//verify the UI of Billing page
//Shipping accordion section
_assertVisible(_div("Step 1: Shipping"));
//Heading
_assertVisible($BILLING_PAGE_HEADING);
//required indicator
_assertVisible($BILLING_REQUIRED_INDICATOR);
//fields
_assertNotVisible($BILLING_SELECT_ADDRESS_DROPDOWN);
_assertVisible($BILLING_FN);
_assertVisible($BILLING_FN_TEXTBOX); 
_assertVisible($BILLING_LN);
_assertVisible($BILLING_LN_TEXTBOX);
_assertVisible($BILLING_ADDRESS1);
_assertVisible($BILLING_ADDRESS1_TEXTBOX);
_assertVisible($BILLING_ADDRESS2);
_assertVisible($BILLING_ADDRESS2_TEXTBOX);
//APO/FPO
_assertVisible($BILLING_APOFPO_TOOLTIP);
_assertVisible($BILLING_STATE);
_assertVisible($BILLING_STATE_DROPDOWN);
_assertVisible($BILLING_CITY);
_assertVisible($BILLING_CITY_TEXTBOX);
_assertVisible($BILLING_ZIPCODE);
_assertVisible($BILLING_ZIPCODE_TEXTBOX);
_assertVisible($BILLING_PHONE);
_assertVisible($BILLING_NUMBER_TEXTBOX);
//why is this required
_assertVisible($BILLING_NUMBER_TOOLTIP);
//Example phn number format
_assertVisible($BILLING_PHNNUMBER_EX);
_assertVisible($BILLING_EMAIL);
_assertVisible($BILLING_EMAIL_TEXTBOX);
//checkbox
_assertVisible($BILLING_ADD_TO_EMAIL_LIST_CHECKBOX);
//Always it should be checked 
_assertTrue($BILLING_ADD_TO_EMAIL_LIST_CHECKBOX.checked);
_assertVisible($BILLING_ADD_TO_ADDRESS_CHECKBOX);
_assertNotTrue($BILLING_ADD_TO_ADDRESS_CHECKBOX.checked);
//Privacy policy
_assertVisible($BILLING_PRIVACY_POLICY);
//ENTER GIFT CERTIFICATE OR COUPON/DISCOUNT CODES section
_assertVisible(_span($Generic[1][0]));
_assertVisible($BILLING_COUPON_TEXTBOX);
_assertVisible($BILLING_COUPON_APPLY_BUTTON);
//Gift certificate section
_assertVisible($BILLING_GIFT_CERTIFICATE_APPLY_BUTTON);
//Payment section
_assertVisible($BILLING_PAYMENT_CREDIT_CARD_LABEL);
_assertVisible($BILLING_PAYMENT_CREDIT_CARD_RADIO_BUTTON);
_assert($BILLING_PAYMENT_CREDIT_CARD_RADIO_BUTTON.checked);
//Payment fields
_assertVisible($BILLING_PAYMENT_HEADING);
//select drop down
_assertNotVisible($BILLING_PAYMENT_CREDIT_CARD_DROPDOWN);
_assertVisible($BILLING_PAYMENT_NAME_ON_CARD);
_assertVisible($BILLING_PAYMENT_OWNER_NAME_TEXTBOX);
_assertVisible($BILLING_PAYMENT_CARD_TYPE);
_assertVisible($BILLING_PAYMENT_CARD_TYPE_DROPDOWN);
_assertVisible($BILLING_PAYMENT_CARD_NUMBER);
_assertVisible($BILLING_NUMBER_EX);
//_assertEqual($Generic[1][1], _getText(_span("form-caption", _in(_div("PaymentMethod_CREDIT_CARD")))));
_assertVisible($BILLING_PAYMENT_CARD_NUMBER_TEXTBOX);
_assertVisible($BILLING_PAYMENT_EXPIRATION_LABEL);
_assertVisible($BILLING_PAYMENT_EXPIRY_MONTH_TEXTBOX1);
_assertVisible($BILLING_PAYMENT_EXPIRY_YEAR_TEXTBOX);
_assertVisible($BILLING_PAYMENT_SECURITY_CODE);
_assertVisible($BILLING_PAYMENT_CVN_TEXTBOX);
_assertVisible($BILLING_CONTINUE_BUTTON);
//Order summary section
_assertVisible($SHIPPING_RIGHT_NAV_HEADING);
_assertVisible($BILLING_ORDER_SUMMARY_EDIT_LINK);
//Shipping address section
_assertVisible($ORDER_SUMMARY_SHIPPING_ADDRESS_HEADING);
//Click on expand link 
if (_isVisible(_span("mini-cart-toggle fa fa-angle-right")))
{
	_click(_span("mini-cart-toggle fa fa-angle-right"));
}
_assertVisible($SHIPPING_SUMMARY_PRODUCT_IMAGE);
_assertVisible($SHIPPING_SUMMARY_PRODUCT_NAME);
_assertVisible($SHIPPING_RIGHT_NAV_SUBTOTAL);
_assertVisible($SHIPPING_RIGHT_NAV_SHIPPING_COST);
_assertVisible($SHIPPING_RIGHT_NAV_SALES_TAX);
_assertVisible($SHIPPING_RIGHT_NAV_ORDER_TOTAL);
//Order review heading
_assertVisible(_div("Step 3: Place Order"));
	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

if(!isMobile())
{
	
var $t = _testcase("288086/288087/352591","Verify the functionality of Why is this required? link/APO/FPO link on billing page in Application as an registered user.");
$t.start();
try
{
	
//mouse hover on apo/Fpo tool tip
_mouseOver($SHIPPING_ADDRESS_TOOLTIP);
//verifying the display of tool tip
if(_getAttribute($SHIPPING_ADDRESS_TOOLTIP,"aria-describedby")!=null)
{
	_assert(true);
	//Text inside the tooltip
	_assertEqual($Generic[7][0], _getText($SHIPPING_ADDRESS_TOOLTIP));
}
else
{
	_assert(false);
}

//mouse hovering on why is this required tool tip
_mouseOver($SHIPPING_PHONE_TOOLTIP_LINK);
//verifying the display of tool tip
if(_getAttribute($SHIPPING_PHONE_TOOLTIP_LINK,"aria-describedby")!=null)
{
	_assert(true);
	//text inside the tooltip
	_assertEqual($Generic[8][0], _getText($SHIPPING_PHONE_TOOLTIP_LINK));
}
else
{
	_assert(false);
}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
	
	var $t = _testcase("288135","Verify the functionality of Help link on billing page in Application as an registered user.");
	$t.start();
	try
	{
		//mouse hover on apo/Fpo tool tip
		_mouseOver($BILLING_COUPON_TOOLTIP);
		//verifying the display of tool tip
		if(_getAttribute($BILLING_COUPON_TOOLTIP,"aria-describedby")!=null)
		{
			_assert(true);
			//Text inside the tooltip
			_assertEqual($Generic[9][0], _getText($BILLING_COUPON_TOOLTIP));
		}
		else
		{
			_assert(false);
		}

		//mouse hovering on why is this required tool tip
		_mouseOver($BILLING_GIFT_CERTIFICATE_TOOLTIP);
		//verifying the display of tool tip
		if(_getAttribute($BILLING_GIFT_CERTIFICATE_TOOLTIP,"aria-describedby")!=null)
		{
			_assert(true);
			//text inside the tooltip
			_assertEqual($Generic[10][0], _getText($BILLING_GIFT_CERTIFICATE_TOOLTIP));
		}
		else
		{
			_assert(false);
		}
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();
}

var $t=_testcase("124451","Verify the UI for Shipping address &amp;amp; Order Summary display in billing accordion by selecting one Shipping address for a particular product as a registered user");
$t.start();
try
{
	//Shipping Address Section
	_assertVisible($ORDER_SUMMARY_SHIPPING_ADDRESS_HEADING);
	var $shippingAddress=$billingAddress+" "+"Method: "+$shippingMethod;
	_assertEqual($shippingAddress,_getText($ORDER_SUMMARY_SHIPPING_ADDRESS_DETAILS));	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("124453","Verify the UI for Order Summary, Shipping, billing address &amp;amp; Payment details in the right side of billing page When user navigate back to billing page from Order review page as a registered user");
$t.start();
try
{
	
//fill the billing details
BillingAddress($Valid_Data,0);
//fill the payment details	
PaymentDetails($Valid_Data,5);
_click($BILLING_CONTINUE_BUTTON);
//come back to billing page
_click($ORDER_SUMMARY_BILLING_EDIT);
//verify the UI of Order Summary Section
verifyOrderSummary("Single",$taxService,$item[0][1]);
//Shipping Address Section
_assertVisible($ORDER_SUMMARY_SHIPPING_ADDRESS_HEADING);
var $shippingAddress=$billingAddress+" "+"Method: "+$shippingMethod;
_assertEqual($shippingAddress,_getText($ORDER_SUMMARY_SHIPPING_ADDRESS_DETAILS));	
//verify the billing address
_assertVisible($BILLING_PAGE_HEADER);
_assertEqual($billingAddress,_getText($ORDER_SUMMARY_BILLING_ADDRESS));
//billing continue button
_click($BILLING_CONTINUE_BUTTON);
//payment details
_assertVisible($BILLING_PAYMENT_EDIT_PAYMENT);
//verifying the payment details section
if(!_isSafari())
	{
	_assertVisible($BILLING_PAYMENT_EDIT_PAYMENT);
	_assertVisible($BILLING_PAYMENT_DETAILS_SECTION);
	var $payment_Details=_getText($BILLING_PAYMENT_DETAILS_SECTION);
	
	}
else
	{
	_assertVisible($BILLING_PAYMENT_EDIT_PAYMENT);
	_assertVisible($BILLING_PAYMENT_EDIT_PAYMENT_DETAILS);
	var $payment_Details=_extract(_getText($BILLING_PAYMENT_EDIT_DETAILS),"/(.*) E/",true);;
	
	}

	var $ExpCreditCard_Details=$OrderSummuary[1][1]+" "+$Valid_Data[5][7];
	_assertContainsText($ExpCreditCard_Details,$BILLING_PAYMENT_EDIT_DETAILS);
		
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("288140/289885","Verify the functionality of 'EDIT' link displayed in shipping address section in right nav on billing page in Application as a registered user." +
"Verify if the Use this a billing address selection is not changed when user reaches the shipping accordion from billing as a registered user.");
$t.start();
try
{
	
//back to shipping page
_click($BILLING_SHIPPING_ACCORDION);
var $shippingMethod=_extract(_getText($SHIPPING_RIGHT_NAV_SHIPPING_COST),"/Shipping(.*)[$]/",true).toString().trim();
//shipping details
shippingAddress($Valid_Data,2);
//selecting use this billing address
_check($SHIPPING_BILLING_ADDRESS_CHECKBOX);
_click($SHIPPING_SUBMIT_BUTTON);
var $shippingAddress2=$billingAddress2+" "+"Method: "+$shippingMethod;
_assertEqual($shippingAddress2,_getText(_div("details", _in(_div("mini-shipment order-component-block  first ")))));
//navigate back to shipping page
_click($BILLING_SHIPPING_ACCORDION);
//shipping details
shippingAddress($Valid_Data,0);
_click($SHIPPING_SUBMIT_BUTTON);
//Billing details
//verify the address prepopulation in billing page
_assertEqual($Valid_Data[0][1],_getText($BILLING_FN_TEXTBOX));
_assertEqual($Valid_Data[0][2],_getText($BILLING_LN_TEXTBOX));
_assertEqual($Valid_Data[0][3],_getText($BILLING_ADDRESS1_TEXTBOX));
_assertEqual($Valid_Data[0][4],_getText($BILLING_ADDRESS2_TEXTBOX));
_assertEqual($Valid_Data[1][5],_getSelectedText($BILLING_COUNTRY_DROPDOWN));
_assertEqual($Valid_Data[0][6],_getSelectedText($BILLING_STATE_DROPDOWN));
_assertEqual($Valid_Data[0][7],_getText($BILLING_CITY_TEXTBOX));
_assertEqual($Valid_Data[0][8],_getText($BILLING_ZIPCODE_TEXTBOX));
_assertEqual($Valid_Data[0][9],_getText($BILLING_NUMBER_TEXTBOX));
	
}
catch($e)
{      
_logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("288139/289896/352571","Verify the functionality of 'EDIT' link displayed next to shipping method in order summary Section in right nav on billing page in Application as a registered user./" +
"Verify if the user selected shipping method is not altered when shipping accordion is loaded from billing page as guest user.");
$t.start();
try
{
	//Shipping address edit
	_click($ORDER_SUMMARY_SHIPPING_ADDRESS_EDIT_LINK);
	var $TotalShippingMethods=_count("_radio","input-radio");
	var $i=$TotalShippingMethods-1;
	//Select last shipping method
	var $ShippingMethodName=_getText(_div("form-row form-indent label-inline["+"1"+"]")).split(":")[0];
	var $ShippingCharge=_extract(_getText(_div("form-row form-indent label-inline["+"1"+"]")).split(":")[1].toString(),"/[ ](.*)[(]/",true);
	_click(_radio("input-radio["+"1"+"]"));
	//shipping details
	shippingAddress($Valid_Data,0);
	_click($SHIPPING_SUBMIT_BUTTON);
	//shipping method name
	var $ShippingSectionShippingMethodName=_extract(_getText(_div("minishipments-method")),"/[:][ ](.*)/",true);
	//Sipping method name in order summary
	var $OrderSummaryShippingMethodName=_extract(_getText($SHIPPING_RIGHT_NAV_SHIPPING_COST),"/Shipping[ ](.*)[ ][$]/",true);
	//Shipping charge in order summary
	var $OrderSummaryShippingCharge=_extract(_getText($SHIPPING_RIGHT_NAV_SHIPPING_COST),"/.*[ ](.*)/",true);
	//compare the shipping method
	_assertEqual($ShippingMethodName,$ShippingSectionShippingMethodName);
	_assertEqual($ShippingMethodName,$OrderSummaryShippingMethodName);
	//compare the shipping charge
	_assertEqual($ShippingCharge,$OrderSummaryShippingCharge);
	//navigate back to shipping page
	_click($BILLING_SHIPPING_ACCORDION);
	//Shipping method selected
	_assert(_radio("input-radio["+"1"+"]").checked);
	//shipping details
	shippingAddress($Valid_Data,0);
	_click($SHIPPING_SUBMIT_BUTTON);
	//address verification over lay
	 
}
catch($e)
{      
	_logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("352547/352549/352551/352556","Verify 'First Name','Last Name','Address 1','Address 2','City', and 'Phone'  Field validation on billing page and functionality of 'CONTINUE' button in Application as a Registered user.");
$t.start();
try
{
	
for(var $i=0;$i<$Billing_Data.length;$i++)
{
if($i!=3)
{
if($i==0)
{
_setValue($BILLING_FN_TEXTBOX, "");
_setValue($BILLING_LN_TEXTBOX, "");
_setValue($BILLING_ADDRESS1_TEXTBOX, "");
_setValue($BILLING_ADDRESS2_TEXTBOX,"");
_setSelected($BILLING_COUNTRY_DROPDOWN,"");
_setSelected($BILLING_STATE_DROPDOWN, "");
_setValue($BILLING_CITY_TEXTBOX, "");
_setValue($BILLING_ZIPCODE_TEXTBOX, "");
_setValue($BILLING_NUMBER_TEXTBOX,"");
_setValue($BILLING_EMAIL_TEXTBOX,"");
}
BillingAddress($Billing_Data,$i);
if($i==9)
	{
	_setValue($BILLING_EMAIL_TEXTBOX,$Billing_Data[9][10]);
	}
else
	{
	_setValue($BILLING_EMAIL_TEXTBOX,$Billing_Data[$i][10]);
	}			
//fill payment details
PaymentDetails($Valid_Data,4);
	//Blank field validation
if($i==0)
	{
	//_click($BILLING_CONTINUE_BUTTON);
	_assert($BILLING_CONTINUE_BUTTON.disabled);             
	}
//max characters
else if($i==1)
   {	
	_assertEqual($Billing_Data[0][12],_getText($BILLING_FN_TEXTBOX).length);
	_assertEqual($Billing_Data[1][12],_getText($BILLING_LN_TEXTBOX).length);
	_assertEqual($Billing_Data[2][12],_getText($BILLING_ADDRESS1_TEXTBOX).length);
	_assertEqual($Billing_Data[3][12],_getText($BILLING_ADDRESS2_TEXTBOX).length);
	_assertEqual($Billing_Data[4][12],_getText($BILLING_CITY_TEXTBOX).length);
	_assertEqual($Billing_Data[7][12],_getText($BILLING_ZIPCODE_TEXTBOX).length);
	_assertEqual($Billing_Data[5][12],_getText($BILLING_NUMBER_TEXTBOX).length);
   }
//special characters
else if($i==4)
	{
		_click($BILLING_CONTINUE_BUTTON);
		_assertVisible(_span($Billing_Data[$i][13]));
		_assertVisible(_span($Billing_Data[$i][14]));
		_assertVisible(_span($Billing_Data[$i][15]));
	}
else if($i==5)
	{
	_assertVisible(_div($Billing_Data[$i][13]));
	_assertEqual($Billing_Data[8][12],_style(_div($Billing_Data[$i][13]),"color"));
	}
//valid
else if($i==9)
	{	
			_click($BILLING_CONTINUE_BUTTON);
			_assert(_isVisible($BILLING_PAYMENT_PLACE_ORDER_BUTTON));
			_assert(_isVisible($ORDER_REVIEW_SUMMARY_FOOTER));
	}	
//Invalid data
	else
		{
			_click($BILLING_CONTINUE_BUTTON);
			_assertVisible(_span($Billing_Data[$i][13]));
			if($i!=5 && $i!=8)
			{
			_assertVisible(_span($Billing_Data[$i][14]));
			}
		}			
}
}
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148878/148879/352553","Verify the validation of 'Country' and 'state' field validation on billing page in Application as a Registered user.");
$t.start();
try
{
	
	//navigating back to billing
	_click(_link("/Billing/",_in(_div("/checkout-progress-indicator/"))));
//	 for(var $i=0;$i<$State_Country.length;$i++)
//		{ 
		 _assertEqual($State_Country[0][11],_getText($BILLING_STATE_DROPDOWN).toString());
	//	}
		 
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("148887","Verify the Field validation of Expiration  Date on billing page in Application as a Registered user.");
$t.start();
try
{
	var $year=_getText($BILLING_PAYMENT_EXPIRY_YEAR_TEXTBOX);
	var $length=_getText($BILLING_PAYMENT_EXPIRY_YEAR_TEXTBOX).length;
	var $currentYear=parseInt(new Date().toString().split(" ")[3]);
	var $nextYear=$currentYear+1;
	var $count=0;
	for(var $i=0;$i<$length;$i++){
	if($i==0)
		{
			_assert($year[$i]==$currentYear);
		}
	else
		{
			_assert($year[$i]==$nextYear);
			$nextYear=$nextYear+1;
			$count++;
		}
	}
	if($count==10)
		{
			_log("Total No.of years are Current year+10");
		}
	else
		{
			_log("Total No.of years are not Current year+10");
		}	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

if(!isMobile())
{
	var $t=_testcase("352581","Verify the functionality of '( What is this?)' link on billing page in Application as a Anonymous user.");
	$t.start();
	try
	{
		_assertVisible($BILLING_PAYMENT_CVN_TOOLTIP_LINK);
		_mouseOver($BILLING_PAYMENT_CVN_TOOLTIP_LINK);
		//verifying the display of tool tip
        if(_getAttribute($BILLING_PAYMENT_CVN_TOOLTIP_LINK,"aria-describedby")!=null)
              {
                     _assert(true);
              }
        else
              {
                     _assert(false);
              }
	}
	catch($e)
	{      
	       _logExceptionAsFailure($e);
	}
	$t.end();
}


ClearCartItems();
//adding item to cart
addToCartSearch($item[0][0],$item[0][2]);
//navigate to billing
navigateToBillingPageWithData();

var $t=_testcase("352560","Verify the navigation of 'See Privacy Policy' link on billing page in Application as a Registered user.");
$t.start();
try
{
	_assertVisible($BILLING_PRIVACY_POLICY);
	//click on privacy policy
	_click($BILLING_PRIVACY_POLICY);
	_assertVisible($BILLING_PRIVACY_POLICY_DIALOG);
	_assertVisible($BILLING_PRIVACY_POLICY_DIALOG_CONTAINER);
	_assertVisible(_heading1($Generic[3][1]));
	_assertEqual($Generic[3][1], _getText($BILLING_PRIVACY_POLICY_HEADER));
	//closing overlay
	_click($BILLING_PRIVACY_POLICY_CLOSE_BUTTON);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("352570","Verify the functionality of 'EDIT' link in order summary Section in right nav on billing page in Application as a Registered user.");
$t.start();
try
{
	//Order summary section
	_assertVisible($SHIPPING_RIGHT_NAV_HEADING);
	_assertVisible($BILLING_ORDER_SUMMARY_EDIT_LINK);
	_click($BILLING_ORDER_SUMMARY_EDIT_LINK);
	//verify the navigation to cart page
	 _assertVisible($CART_TABLE);
	 _assertVisible($CART_ACTIONBAR);		
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


//removing item from cart
ClearCartItems();
//deleting the addresses and card details from profile
_click($HEADER_USERACCOUNT_LINK);
_click($HEADER_MYACCOUNT_LINK);
//Click on link addresses
_click($MY_ACCOUNT_ADDRESSES_OPTIONS); 
deleteAddress();
DeleteCreditCard();

var $t=_testcase("352545/352585/352558","Verify the functionality of 'Add to Address Book' check box and functionality of 'Save this card' checkbox on billing page in Application as a Registered user.");
$t.start();
try
{
	
//if User selected 'Use this address for Billing' checkbox
//Navigation till shipping page
navigateToShippingPage($item[0][0],$item[0][1]);
//shipping details
shippingAddress($Valid_Data,0);
_click($SHIPPING_SUBMIT_BUTTON);
//Billing details
BillingAddress($Valid_Data,0);	
//selecting check box
_check($BILLING_ADD_TO_ADDRESS_CHECKBOX);
PaymentDetails($Valid_Data,4);
_check($BILLING_PAYMENT_SAVE_CARD_CHECKBOX);
//click on continue
_click($BILLING_CONTINUE_BUTTON);	
_click($BILLING_ADDRESS_SUBMIT);
if(_isVisible($BILLING_ADDRESS_CORRECTION_CLOSE_LINK))
{
_click($BILLING_ADDRESS_CORRECTION_CLOSE_LINK);
}
//Checking order is placed or not
_assertVisible($ORDER_CONFIRMATION_HEADING);
_assertVisible($ORDER_CONFIRMATION_DIV);

//verifying the address in account
_click(_link("my account"));
_click(_link("user_account", _in(_div("user-links"))));
_click(_div("Addresses"));
_assertEqual($Valid_Data[0][7],_getText($ADDRESSES_MINIADDRESS_TITLE));
_assertEqual($Valid_Data[0][1]+" "+$Valid_Data[0][2], _getText($ADDRESSES_MINIADDRESS_NAME));
_assertEqual($Valid_Data[0][11], _getText($ADDRESSES_MINIADDRESS_LOCATION));
//verifying the card details in account
_click(_link("Payment Settings"));
_assertContainsText($Valid_Data[4][7],$PAYMENT_PAYMENTLIST);
 	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

//Navigation till shipping page
navigateToShippingPage($item[0][0],$item[0][1]);
//shipping details
shippingAddress($Valid_Data,0);
_click($SHIPPING_SUBMIT_BUTTON);

var $t=_testcase("287892/352597/352582","Verify the UI of BILLING address form in Application with saved addresses as a registered user.");
$t.start();
try
{

	//required indicator
	_assertVisible($BILLING_REQUIRED_INDICATOR);
	_assertVisible(_div("step-1 inactive"));
	_assertVisible(_div("step-3 inactive"));
	//fields
	_assertVisible($BILLING_SELECT_ADDRESS_DROPDOWN);
	_assertVisible($BILLING_FN);
	_assertVisible($BILLING_FN_TEXTBOX); 
	_assertVisible($BILLING_LN);
	_assertVisible($BILLING_LN_TEXTBOX);
	_assertVisible($BILLING_ADDRESS1);
	_assertVisible($BILLING_ADDRESS1_TEXTBOX);
	_assertVisible($BILLING_ADDRESS2);
	_assertVisible($BILLING_ADDRESS2_TEXTBOX);
	//APO/FPO
	_assertVisible($BILLING_APOFPO_TOOLTIP);
	_assertVisible($BILLING_STATE);
	_assertVisible($BILLING_STATE_DROPDOWN);
	_assertVisible($BILLING_CITY);
	_assertVisible($BILLING_CITY_TEXTBOX);
	_assertVisible($BILLING_ZIPCODE);
	_assertVisible($BILLING_ZIPCODE_TEXTBOX);
	_assertVisible($BILLING_PHONE);
	_assertVisible($BILLING_NUMBER_TEXTBOX);
	//why is this required
	_assertVisible($BILLING_NUMBER_TOOLTIP);
	//Example phn number format
	_assertVisible($BILLING_PHNNUMBER_EX);
	_assertVisible($BILLING_EMAIL);
	_assertVisible($BILLING_EMAIL_TEXTBOX);
	//checkbox
	_assertVisible($BILLING_ADD_TO_EMAIL_LIST_CHECKBOX);
	_assertTrue($BILLING_ADD_TO_EMAIL_LIST_CHECKBOX.checked);
	_assertVisible($BILLING_ADD_TO_ADDRESS_CHECKBOX);
	_assertNotTrue($BILLING_ADD_TO_ADDRESS_CHECKBOX.checked);
	//Privacy policy
	_assertVisible($BILLING_PRIVACY_POLICY);
	//ENTER GIFT CERTIFICATE OR COUPON/DISCOUNT CODES section
	_assertVisible(_span($Generic[1][0]));
	_assertVisible($BILLING_COUPON_TEXTBOX);
	_assertVisible($BILLING_COUPON_APPLY_BUTTON);
	//Gift certificate section
	_assertVisible($BILLING_GIFT_CERTIFICATE_APPLY_BUTTON);
	//Payment section
	_assertVisible($BILLING_PAYMENT_CREDIT_CARD_LABEL);
	_assertVisible($BILLING_PAYMENT_CREDIT_CARD_RADIO_BUTTON);
	_assert($BILLING_PAYMENT_CREDIT_CARD_RADIO_BUTTON.checked);
	//Payment fields
	_assertVisible($BILLING_PAYMENT_HEADING);
	//select drop down
	_assertNotVisible($BILLING_PAYMENT_CREDIT_CARD_DROPDOWN);
	_assertVisible($BILLING_PAYMENT_NAME_ON_CARD);
	_assertVisible($BILLING_PAYMENT_OWNER_NAME_TEXTBOX);
	_assertVisible($BILLING_PAYMENT_CARD_TYPE);
	_assertVisible($BILLING_PAYMENT_CARD_TYPE_DROPDOWN);
	_assertVisible($BILLING_PAYMENT_CARD_NUMBER);
	_assertVisible($BILLING_NUMBER_EX);
	//_assertEqual($Generic[1][1], _getText(_span("form-caption", _in(_div("PaymentMethod_CREDIT_CARD")))));
	_assertVisible($BILLING_PAYMENT_CARD_NUMBER_TEXTBOX);
	_assertVisible($BILLING_PAYMENT_EXPIRATION_LABEL);
	_assertVisible($BILLING_PAYMENT_EXPIRY_MONTH_TEXTBOX1);
	_assertVisible($BILLING_PAYMENT_EXPIRY_YEAR_TEXTBOX);
	_assertVisible($BILLING_PAYMENT_SECURITY_CODE);
	_assertVisible($BILLING_PAYMENT_CVN_TEXTBOX);	
	_assertVisible($BILLING_CONTINUE_BUTTON);
	
	//Order summary section
	_assertVisible($SHIPPING_RIGHT_NAV_HEADING);
	_assertVisible($BILLING_ORDER_SUMMARY_EDIT_LINK);
	//Shipping address section
	_assertVisible($ORDER_SUMMARY_SHIPPING_ADDRESS_HEADING);
	//Click on expand link 
	if (_isVisible($BILLING_MINICART_EXPANDED))
	{
		_click($BILLING_MINICART_TOGGLE);
	}
	_assertVisible($SHIPPING_SUMMARY_PRODUCT_NAME);
	_assertVisible($SHIPPING_RIGHT_NAV_SUBTOTAL);
	_assertVisible($SHIPPING_RIGHT_NAV_SHIPPING_COST);
	_assertVisible($SHIPPING_RIGHT_NAV_SALES_TAX);
	_assertVisible($SHIPPING_RIGHT_NAV_ORDER_TOTAL);

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("352599","Verify the Scenario if user select 'Choose an Address' from the drop down on billing page in Application as a Registered user.");
$t.start();
try
{
	
	//if User selected 'Use this address for Billing' checkbox
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping details
	shippingAddress($Valid_Data,0);
	_click($SHIPPING_SUBMIT_BUTTON);
	//Billing details
	//selecting from drop down
	_setSelected($BILLING_SELECT_ADDRESS_DROPDOWN,1);
	//verify the address prepopulation in billing page
	_assertEqual($Valid_Data[0][1],_getText($BILLING_FN_TEXTBOX));
	_assertEqual($Valid_Data[0][2],_getText($BILLING_LN_TEXTBOX));
	_assertEqual($Valid_Data[0][3],_getText($BILLING_ADDRESS1_TEXTBOX));
	_assertEqual($Valid_Data[0][4],_getText($BILLING_ADDRESS2_TEXTBOX));
	_assertEqual($Valid_Data[1][5],_getSelectedText($BILLING_COUNTRY_DROPDOWN));
	_assertEqual($Valid_Data[0][6],_getSelectedText($BILLING_STATE_DROPDOWN));
	_assertEqual($Valid_Data[0][7],_getText($BILLING_CITY_TEXTBOX));
	_assertEqual($Valid_Data[0][8],_getText($BILLING_ZIPCODE_TEXTBOX));
	_assertEqual($Valid_Data[0][9],_getText($BILLING_NUMBER_TEXTBOX));	
	//Payment section
	//selecting from drop down
	_setSelected($BILLING_PAYMENT_CREDIT_CARD_DROPDOWN,1);
	_assertEqual($Valid_Data[4][1],_getText($BILLING_PAYMENT_OWNER_NAME_TEXTBOX));
	_assertEqual($Valid_Data[4][2],_getSelectedText($BILLING_PAYMENT_CARD_TYPE_DROPDOWN));
	_assertEqual($Valid_Data[4][4],_getSelectedText($BILLING_PAYMENT_EXPIRY_MONTH_TEXTBOX));
	_assertEqual($Valid_Data[4][5],_getSelectedText($BILLING_PAYMENT_EXPIRY_YEAR_TEXTBOX1));
	//checking cvv and entering cvv
	_assertEqual("", _getValue($BILLING_PAYMENT_CVN_TEXTBOX));
	_setValue($BILLING_PAYMENT_CVN_TEXTBOX, $Valid_Data[4][6]);	
	_click($BILLING_CONTINUE_BUTTON);
	 
	//Placing order
	_click($BILLING_ADDRESS_SUBMIT);
	if(_isVisible($BILLING_ADDRESS_CORRECTION_CLOSE_LINK))
	{
	_click($BILLING_ADDRESS_CORRECTION_CLOSE_LINK);
	}
	//Checking order is placed or not
	_assertVisible($ORDER_CONFIRMATION_HEADING);
	_assertVisible($ORDER_CONFIRMATION_DIV);
	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("148888/148889/148890/352580/352577/352578","Verify the Field validation of Card Number,Security Code,'Name on Card'and 'Type' on billing page in Application as a Anonymous user.");
$t.start();
try
{
	
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping details
	shippingAddress($Valid_Data,0);
	_click($SHIPPING_SUBMIT_BUTTON);
	//enter billing Address
	BillingAddress($Valid_Data,0); 
	//validating the payment fields
	for(var $i=0;$i<$PaymentValidation.length;$i++)
	{
		if($i==0)
		{
			_setValue($BILLING_PAYMENT_OWNER_NAME_TEXTBOX, "");
			_setValue($BILLING_PAYMENT_CARD_NUMBER_TEXTBOX,"");
			_setValue($BILLING_PAYMENT_CVN_TEXTBOX,"");
		}
		else
		{
			PaymentDetails($PaymentValidation,$i)
		}	
		//blank field validation
		if($i==0)
		{
			_click($BILLING_CONTINUE_BUTTON);
			_assertEqual($color,_style($BILLING_PAYMENT_OWNER_NAME_TEXTBOX,"border-color"));
			_assertEqual($color,_style($BILLING_PAYMENT_CARD_NUMBER_TEXTBOX,"border-color"));
			_assertEqual($color,_style($BILLING_PAYMENT_CVN_TEXTBOX,"border-color"));
		}
		//Max
		else  if($i==1)
		{
			_assertEqual($PaymentValidation[0][9],_getText($BILLING_PAYMENT_OWNER_NAME_TEXTBOX).length);
			_assertEqual($PaymentValidation[1][9],_getText($BILLING_PAYMENT_CARD_NUMBER_TEXTBOX).length);
			_assertEqual($PaymentValidation[2][9],_getText($BILLING_PAYMENT_CVN_TEXTBOX).length);
		}
		//invalid credit card number
		else if($i>=2 && $i<=11)
		{
			_click($BILLING_CONTINUE_BUTTON);
			//verify card num error msg
			_assert(_isVisible(_div($PaymentValidation[$i][7])));
			_assertEqual($PaymentValidation[0][10],_style(_div($PaymentValidation[$i][7]),"color"));
		}
		//Invalid CVV
		else if($i>=12 && $i<=19)
		{
			_click($BILLING_CONTINUE_BUTTON);
			//verify text box color    		
			_assertEqual($PaymentValidation[1][10],_style(_div("form-row cvn required error"),"color"));
		}
		else if($i>=20 && $i<=26)
		{
			_click($BILLING_CONTINUE_BUTTON);
			//verify card num error msg
			_assert(_isVisible(_div($PaymentValidation[$i][7])));
			_assertEqual($PaymentValidation[0][10],_style(_div($PaymentValidation[$i][7]),"color"));
		}
		//expiration year
		else //if($i==27)
		{	
			_click($BILLING_CONTINUE_BUTTON);
			//verify card num error msg
			_assert(_isVisible(_div($PaymentValidation[$i][7])));
			_assertEqual($PaymentValidation[0][10],_style(_div($PaymentValidation[$i][7]),"color"));
		}
	}
}
catch($e)
{      
	_logExceptionAsFailure($e);
}
$t.end();

//removing item from cart
ClearCartItems();

/*var $t=_testcase("124452","Verify the UI for  different Shipping address &amp; Order Summary display in billing accordion by selecting two different Shipping address for same item with different quantity");
$t.start();
try
{
//Navigation till shipping page
navigateToShippingPage($item[0][0],"2");
var $shippingMethod=_extract(_getText(_row("/order-shipping  first/")),"/Shipping(.*)[$]/",true).toString().trim();
//click on yes button
_click($SHIPPING_MULTIPLE_ADDRESS_BUTTON);
//adding addresses
for(var $i=0;$i<$Address.length;$i++)
{
	_click(_span("Add/Edit Address["+$i+"]"));
	addAddressMultiShip($Address,$i);
}

//Select Addresses from each drop down
var $j=1;
for(var $k=0;$k<$Address.length;$k++)
{
	_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$k+"_addressList"),$j);
	$j++;
}

//address save 
_click($SHIPPING_MULTIPLE_ADDRESS_SAVE_BUTTON);
_wait(5000,_isVisible($BILLING_MULTI_SHIPPING_CONTINUE_BUTTON));
//shipping options
_click($BILLING_MULTI_SHIPPING_CONTINUE_BUTTON);
$tax=parseFloat(_extract(_getText($SHIPPING_RIGHT_NAV_SALES_TAX),"/[$](.*)/",true).toString());
$shippingTax=parseFloat(_extract(_getText($SHIPPING_RIGHT_NAV_SHIPPING_COST),"/[$](.*)/",true).toString());
$orderTotal=parseFloat(_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/[$](.*)/",true).toString());
//verify the ordersummary
verifyOrderSummary("Multishipment",$taxService,$item[0][2]);
//verify the shipping addresses
var $numOfShipments=_count("_div","/mini-shipment order-component-block/",_in(_div("secondary")));
_log($numOfShipments);
for(var $i=0;$i<$numOfShipments;$i++)
	{
		var $billingAddress=$Address[$i][2]+" "+$Address[$i][3]+" "+$Address[$i][4]+" "+$Address[$i][5]+" "+$Address[$i][8]+", "+$Address[$i][11]+" "+$Address[$i][9]+" "+$Address[$i][6]+" "+$Address[$i][10];
		var $ExpShippingAddress=$billingAddress+" "+"Method: "+$shippingMethod;
		var $ActShippingAddress=_getText($ORDER_SUMMARY_SHIPPING_ADDRESS_DETAILS);
		if($ActShippingAddress.indexOf($Address[$i][2])>=0)
		{
		_assertEqual($ExpShippingAddress,$ActShippingAddress);
		}
		else
		{
		var $ActShippingAddress=_getText($BILLING_ADDRESS_ACTUAL_SHIPPING_ADDRESS);
		_assertEqual($ExpShippingAddress,$ActShippingAddress);
		}
	}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();*/


//################## Coupon and Gift card related #########################

var $t=_testcase("352563","Verify the field validations in 'Redeem Gift Certificate ' textbox, functionality by entering Valid Gift card number and pincode number,functionality whether user can remove the applied gift card, functionality of 'Check balance' link, functionality of Apply buttton in Gift card Section on billing page in Application as a Registered user.");
$t.start();
try
{
	
   //validations 
   for(var $i=0;$i<$GC_Validation.length;$i++)
          {
          //valid data, Apply and remove functionality
      if($i==4)
             {
    	  
    	  	//valid $25 GC
             _setValue($BILLING_GIFT_CERTIFICATE_CODE,$GC[0][0]);
             _setValue($BILLING_GIFT_CERTIFICATE_PIN,$GC[0][1]);                
             _click($BILLING_GIFT_CERTIFICATE_APPLY_BUTTON);
             //valid data 
             _assertVisible($BILLING_GIFT_CERTIFICATE_SUCCESS_MESSAGE);
             //enter valid billing address
             BillingAddress($Valid_Data,0);
             //enter credit card details
   		  	PaymentDetails($Valid_Data,4);
           //click on continue
           _click($BILLING_CONTINUE_BUTTON);
           var $ActGCAmount=parseFloat(_extract(_getText($BILLING_GIFT_CERTIFICATE_AMOUNT), "/Amount: [$](.*)/",true).toString());
             if(isIE11())
           	  {
           	  var $OrderTotal=parseFloat(_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/Order Total:[$](.*)/",true).toString());
           	  }
             else
           	  {
           	  var $OrderTotal=parseFloat(_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/Order Total: [$](.*)/",true).toString());
           	  }
             var $ExpOrderBal=$OrderTotal-$ActGCAmount;
             _log($ExpOrderBal);
             $ExpOrderBal=round($ExpOrderBal,2);
             _log($ExpOrderBal);
             var $ActOrderBal=parseFloat(_extract(_getText($BILLING_GIFT_CERTIFICATE_BALANCE), "/Amount: [$](.*)/",true).toString());
             _assertEqual($ExpOrderBal,$ActOrderBal);
             
             //redirecting to billing
             _click(_link("Billing"));
             //remove button
             _assertVisible($BILLING_GIFT_CERTIFICATE_REMOVE_LINK);
             _click($BILLING_GIFT_CERTIFICATE_REMOVE_LINK);
             _assertNotVisible($BILLING_GIFT_CERTIFICATE_SUCCESS_MESSAGE);
             _assertNotVisible($BILLING_GIFT_CERTIFICATE_REMOVE_LINK);      
             }
      else
             {
             _setValue($BILLING_GIFT_CERTIFICATE_CODE,$GC_Validation[$i][1]);
             _setValue($BILLING_GIFT_CERTIFICATE_PIN,$GC_Validation[$i][2]);
               if($i==5)
                   {
                          _click($BILLING_GIFT_CERTIFICATE_CHECK_BALANCE);
                          //Checking balance without apllying GC
                          _assertVisible(_div("Your current gift certificate balance is $"+$GC_Validation[$i][3]));
                   }
             //Invalid
             else if($i==0)
                   {
                          _click($BILLING_GIFT_CERTIFICATE_APPLY_BUTTON);
                          _assertVisible(_div($GC_Validation[0][4]));
                          _assertEqual($GC_Validation[0][5],_style(_div($GC_Validation[0][4]),"color"));
                   }
             else
                   {
                   _click($BILLING_GIFT_CERTIFICATE_APPLY_BUTTON);
                   var $errorMsg="Gift Card \""+$GC_Validation[$i][1]+"\" could not be found.";
                   _assertVisible(_div($errorMsg));
                   _assertEqual($GC_Validation[0][5],_style(_div($errorMsg),"color"));
                   }
             
             } 
      }
       
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


ClearCartItems();
//adding item to cart
addItemToCart($item[0][0],$item[0][2]);
//navigate to billing
navigateToBillingPageWithData();

var $t=_testcase("288146","Verify the functionality when users enters a Coupon code and hit the 'ENTER' Key as registered user");
$t.start();
try
{
	
_setValue($BILLING_COUPON_TEXTBOX,$CouponValidations[2][1]);
//Hit enter key
 _focusWindow();
//_keyPress(document.body, [13,13]);
 _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
_assert(_isVisible($BILLING_COUPON_REDEMPTION));
var $sucessMsg="Promo Code "+$CouponValidations[2][1]+" has been added to your order and was applied.";
	
}
catch($e)
{      
     _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("352561/352594","Verify the Field validation & functionality of 'Enter coupon code' field/ub total / Order Total/ Discount/ Shipping gets updated after applying the valid coupon code on billing page in Application as a Registered user");
$t.start();
try{
	
//validations 
for(var $i=0;$i<$CouponValidations.length;$i++)
	{
	//Navigation till shipping page
	search($CouponValidations[$i][0]);
	_click($PDP_ADDTOCART_BUTTON);
	_click($MINICART_OVERLAY_VIEWCART_LINK);
	//navigating to shipping page
	_click($CART_CHECKOUT_BUTTON);
	_wait(4000);
	if(_isVisible($LOGIN_SECTION))
		{
		_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
		}
	//shipping details
	shippingAddress($Valid_Data,0);
	_click($SHIPPING_SUBMIT_BUTTON);
	var $subtotalBeforeApplyingCoupon;	
	var $ordertotalBeforeApplyingCoupon;
	if($i==2)
		{
			$subtotalBeforeApplyingCoupon=parseFloat(_extract(_getText($SHIPPING_RIGHT_NAV_SUBTOTAL),"/[$](.*)/",true));
			$ordertotalBeforeApplyingCoupon=parseFloat(_extract(_getText($SHIPPING_RIGHT_NAV_ORDER_TOTAL),"/[$](.*)/",true));
		}
	_setValue($BILLING_COUPON_TEXTBOX,$CouponValidations[$i][1]);
	_click($BILLING_COUPON_APPLY_BUTTON);
	//invalid
	if($i==0 ||$i==1 ||$i==2 || $i==3 ||$i==4)
		{
			_assertVisible(_div($CouponValidations[$i][2]));
			_assertEqual($CouponValidations[0][4],_style(_div($CouponValidations[$i][2]),"color"));
		}

	//valid coupon
	if($i==5 || $i==6)
		{
			_assert(_isVisible($BILLING_COUPON_REDEMPTION));
			var $sucessMsg="Promo Code "+$CouponValidations[$i][2]+" has been added to your order and was applied.";
			//enter valid billing address
			BillingAddress($Valid_Data,0);
			//enter credit card details
		  	PaymentDetails($Valid_Data,4);
			//click on continue
			_click($BILLING_CONTINUE_BUTTON);	
				}
			//verify wehter valid Coupon got applied or not
			_assertVisible($BILLING_COUPON_APPLIED_MESSAGE);
			_assertVisible($BILLING_COUPON_APPLIED);
			_assertVisible($BILLING_COUPON_NUMBER_APPLIED);
			if(isMobile())
				{
					_assertVisible($BILLING_COUPON_PRODUCT_MOBILE);
				}
			else
				{
					_assertVisible($BILLING_COUPON_PRODUCT_DESKTOP);
				}
			_assertVisible($BILLING_COUPON_DISCOUNT_VALUE);
			var $discountPrice=parseFloat(_extract(_getText($BILLING_COUPON_DISCOUNT_VALUE),"/[$](.*)/",true).toString());
			var $expDiscountPrice=$subtotalBeforeApplyingCoupon-$discountPrice;
			var $actDiscountPrice=parseFloat(_getText($CART_ITEM_TOTAL_CELL).replace("$",""));
			//verifying the price
			_assertEqual($expDiscountPrice,$actDiscountPrice);
			//verify whether subtotal section got applied or not
			var $subTotal=_extract(_getText($SHIPPING_RIGHT_NAV_SUBTOTAL),"/[$](.*)/",true);
			_assertEqual($actDiscountPrice,$subTotal);		
			_click($CHECKOUT_INDICATOR_BILLING_LINK);	
		}

	//clear cart items
	ClearCartItems();
			
}catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

cleanup();

function navigateToBillingPageWithData()
{
	_click($MINICART_CHECKOUT);
	_click($SHIPPING_SUBMIT_BUTTON);
	//address verification over lay
	_assertVisible(_fieldset("/"+$Generic[6][0]+"/"));	
}