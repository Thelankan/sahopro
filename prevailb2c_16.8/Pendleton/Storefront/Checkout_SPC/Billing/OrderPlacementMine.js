_include("../../../GenericLibrary/BM_Functions.js");
_include("../../../GenericLibrary/GlobalFunctions.js");
_include("../../../ObjectRepository/Checkout/Shipping_OR.sah");
_include("../../../ObjectRepository/Checkout/Billing_OR.sah");
_include("../../../ObjectRepository/Checkout/Checkout_Login_OR.sah");
_include("../../../ObjectRepository/ShopNav/HomePage_OR.sah");

_resource("BillingPage.xls");

var $pvld=_readExcelFile("BillingPage.xls","PaymentValidData");

var $sheetName;
var $rowNum;
var $CProductName;
var $CQuantity;


SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t=_testcase("352568","Verify the navigation to Checkout Billing page in Application as a Registered user.");
$t.start();
try
{
	
	for (var $i=0;$i<$pvld.length;$i++)
		{
		//Navigation till shipping page
		navigateToShippingPage($productQuantity[$i][0],$item[0][1]);
		//shipping details
		shippingAddress($Valid_Data,0);
		_click($SHIPPING_SUBMIT_BUTTON);
		//fill the billing details
		BillingAddress($Valid_Data,0);
		//fill the payment details	
		PaymentDetails($pvld,$i);
		//click on continue
		_click($BILLING_CONTINUE_BUTTON);	
		_click($BILLING_ADDRESS_SUBMIT);
		if(_isVisible($BILLING_ADDRESS_CORRECTION_CLOSE_LINK))
		{
		_click($BILLING_ADDRESS_CORRECTION_CLOSE_LINK);
		}
		//Checking order is placed or not
		_assertVisible($ORDER_CONFIRMATION_HEADING);
		_assertVisible($ORDER_CONFIRMATION_DIV);
		}

	
}
catch($e)	
{      
       _logExceptionAsFailure($e);
}
$t.end();