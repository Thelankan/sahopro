_include("../../GenericLibrary/GlobalFunctions.js");
_resource("StaticPages.xls");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

var $contactusPage=_getAttribute(_link("CUSTOMER SERVICE"),"href");
var $requestaCatalogPage=_getAttribute(_link("REQUEST A CATALOG"),"href");


var $t = _testcase("357950/357951/357967/357989","Verify the UI of contact us page both as  an anonymous and a registered user./Verify the functionality of Page Breadcrumb in contact us page both as an anonymous and a registered user.");
$t.start();
try
{

//click on contact us page
//_click($CUSTOMERSERVICE_LINK);
UrlNavigation($contactusPage);
//Bread crumb
_assertVisible($CONTACTUS_BANNER);
_assertEqual("Customer Service", _getText($PAGE_BREADCRUMB));
_assertVisible($PAGE_BREADCRUMB);
_assertVisible($CONTACTUS_HEADING);
_assertVisible($CONTACTUS_TEXT_PARAGRAPH);
_assertVisible($CONTACTUS_EMAIL_TEXTBOX);
_assertVisible($CONTACTUS_SUBJECT_DROPDOWN);
_assertVisible($CONTACTUS_MESSAGE_FIELD);
_assertVisible(_div("store-info"));
_assertVisible(_div("footer-container"));
_assertVisible(_div("secondary"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124634/288531", "Verify the UI  of 'My Account home page/ UI of left navigation pane  in my account landing page of the application as a  Registered user");
$t.start();
try
{
	
//click on request catalog link
//_click($RC_LINK);
UrlNavigation($requestaCatalogPage);
//bread crumb
_assertVisible($PAGE_BREADCRUMB);
_assertVisible($MY_ACCOUNT_LINK_LEFTNAV, _in($PAGE_BREADCRUMB));
_assertVisible($MY_ACCOUNT_HEADING);
//logged in or not
_assertVisible($HEADER_USERACCOUNT_LINK);

//left nav section
var $Leftnavlinks=HeadingCountInLeftNav();
var $LeftnavlinkNames=LinkCountInLeftNav();

//UI of  leftnavlinks
for(var $i=0;$i<$Leftnavlinks.length;$i++)
	{
		var $Expected_Leftnavlink = _span($staticPages[$i][0]);
		
		_assertVisible($Expected_Leftnavlink);			
	}

//UI of leftnavlinks Names
for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
	{
	    var $Expected_LeftnavlinkNames = _span($staticPages[$i][1]);
		_assertVisible($Expected_LeftnavlinkNames);			
	}

_assertVisible($LEFTNAV_NEEDHELP_HEADING);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("358004/357987","Verify the functionality of Left Hand Navigation Pane Content Asset  both as an anonymous and a registered user.");
$t.start();
try
{

	
//left nav links	
var $LeftnavlinkNames=LinkCountInLeftNav();

for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
{
	
//navigate to my account home page
UrlNavigation($contactusPage);

//click on left nav links 
_click($LeftnavlinkNames[$i]);

var $Expected_LeftnavHeading = _heading1("/"+$staticPages[$i][2]+"/");

//verify the navigation
_assertVisible($Expected_LeftnavHeading);
_assertContainsText($staticPages[$i][2], $PAGE_BREADCRUMB);


		
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//###################### Related to CustomerService [Contact Us] link ##############################################

var $t = _testcase("357954/357971/357968/357984","Verify the functionality of Left Hand Navigation Pane Content Asset  both as  an anonymous and a registered user.");
$t.start();
try
{

//click on contact us page
//_click($CUSTOMERSERVICE_LINK);
UrlNavigation($requestaCatalogPage);
//click on customer service link	
var $customerServiceURL=_getAttribute($CUSTOMERSERVICE_LINK,"href");
//left nav links	
var $LeftnavlinkNames=LinkCountInLeftNav();

for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
{
	
	//click on contact us page
	_click($CUSTOMERSERVICE_LINK);	
	
	//click on left nav links 
	_click($LeftnavlinkNames[$i]);
	
	var $Expected_LeftnavHeading = _heading1("/"+$staticPages[$i][2]+"/");
	
	//verify the navigation
	_assertVisible($Expected_LeftnavHeading);
	_assertContainsText($staticPages[$i][2], $PAGE_BREADCRUMB);
	
	//Should be in same page
	_click($PAGE_BREADCRUMB);
	_assertVisible(_heading1("/"+$staticPages[$i][2]+"/"));
	
}

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357958/357967","Verify the display of *Required Resource Message in contact us page both as an anonymous and a registered user." +
		"Verify the UI of static content pages both as  an anonymous and a registered user.");
$t.start();
try
{

_assertVisible($CONTACTUS_EMAIL_LABEL);
_assertVisible($CONTACTUS_SUBJECT_LABEL);
_assertVisible($CONTACTUS_MESSGAE_LABEL);
_assertVisible($CONTACTUS_EMAIL_TEXTBOX);
_assertVisible($CONTACTUS_SUBJECT_DROPDOWN);
_assertVisible($CONTACTUS_MESSAGE_FIELD);
_domain("www.google.com")._assertVisible($CAPTCHA_CHECKMARK);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357959","Verify the functionality of Customer Email Address Form Field in contact us page both as  an anonymous and a registered user.");
$t.start();
try
{
	
//enter the value	
_setValue($CONTACTUS_EMAIL_TEXTBOX,$requestCatalog[0][13]);
_click($CONTACTUS_TEXT_SUBMIT_BUTTON);
//error Message should display
_assertEqual($requestCatalog[4][12], _getText($CONTACTUS_EMAIL_ERROR));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357960","Verify the functionality of Contact Form Subject Form Field in contact us page both as  an anonymous and a registered user.");
$t.start();
try
{
	
//Contact us subject dropdown	
_assertEqual($staticPages[0][4],_getSelectedText($CONTACTUS_SUBJECT_DROPDOWN));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357961","Verify the functionality of Contact Us Message Form Field in contact us page both as  an anonymous and a registered user.");
$t.start();
try
{
	
//message text box enter 1000 characters
_setValue($CONTACTUS_MESSAGE_FIELD,$staticPages[1][4]);
//check 1000 characters are entered or not
_assertEqual($staticPages[1][5],_getText($CONTACTUS_MESSAGE_FIELD).length);
//enter More than 1000 characters
_setValue($CONTACTUS_MESSAGE_FIELD,$staticPages[2][4]);
//still it should accept 1000 characters only not more than that
_assertEqual($staticPages[1][5],_getText($CONTACTUS_MESSAGE_FIELD).length);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357963","Verify the functionality of Submit CTA in contact us page both as  an anonymous and a registered user.");
$t.start();
try
{
	
//enter email address
_setValue($CONTACTUS_EMAIL_TEXTBOX,$requestCatalog[1][0]);
_setSelected($CONTACTUS_SUBJECT_DROPDOWN,$requestCatalog[1][1]);
_setValue($CONTACTUS_MESSAGE_FIELD,$staticPages[1][4]);
//click on submit
_click($CONTACTUS_SUBMIT_BUTTON);
//check for empty values
_setValue($CONTACTUS_EMAIL_TEXTBOX,$requestCatalog[0][13]);
_setValue($CONTACTUS_MESSAGE_FIELD,$requestCatalog[0][13]);
//click on submit
_click($CONTACTUS_SUBMIT_BUTTON);
//error messages
_assertEqual($requestCatalog[4][12], _getText($CONTACTUS_EMAIL_ERROR));
_assertEqual($requestCatalog[4][13], _getText($CONTACTUS_MESSAGE_ERROR));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357956","Verify the functionality of Contact Us Page Content Asset  both as  an anonymous and a registered user.");
$t.start();
try
{
	_click($WHOLESALEINQURY_LINK);	
	_assertVisible($CONTACTUS_WHOLESALE_ENQUIRY);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357990","Verify the functionality of Wholesale Inquiry iFrame in whole inquiry page");
$t.start();
try
{
	
//enter all the values
_domain("docs.google.com")._check($WHOLESALE_ENQUIRE_CHECKBOX);
_domain("docs.google.com")._setValue($WHOLESALE_YOURNAME_TEXTBOX,$staticPages[0][7]);
_domain("docs.google.com")._setValue($WHOLESALE_RETAIL_TEXTBOX,$staticPages[1][7]);
_domain("docs.google.com")._setValue($WHOLESALE_RETAIL_STREETADDRESS,$staticPages[2][7]);
_domain("docs.google.com")._setValue($WHOLESALE_CITY_TEXTBOX,$staticPages[3][7]);
_domain("docs.google.com")._setValue($WHOLESALE_STATE_TEXTBOX,$staticPages[4][7]);
_domain("docs.google.com")._setValue($WHOLESALE_COUNTRY_TEXTBOX,$staticPages[5][7]);
_domain("docs.google.com")._setValue($WHOLESALE_POSTAL_TEXTBOX,$staticPages[6][7]);
_domain("docs.google.com")._setValue($WHOLESALE_PHONENUMBER_TEXTBOX,$staticPages[7][7]);
_domain("docs.google.com")._setValue($WHOLESALE_EMAILADDRESS_TEXTBOX,$staticPages[8][7]);
_domain("docs.google.com")._setValue($WHOLESALE_STOREWEB_ADDRESS,$staticPages[9][7]);
_domain("docs.google.com")._setValue($WHOLESALE_COMMENTS_TEXTBOX,$staticPages[10][7]);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//############################## Related to errors and alerts #################################### 

var $t = _testcase("357978/357979/357980/357981","Verify the functionality of Mandatory  Form Fields as guest and registered user/Verify the validation of Required Form Field as guest and registered user");
$t.start();
try
{
	
//Add to cart button
navigateToCart($productQuantity[0][0],$productQuantity[0][2]);
//cliclk on checkout button
_click($CART_CHECKOUT_BUTTON);
//click on guest checkout
_click($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
//enter empty values
_setValue($SHIPPING_FN_TEXTBOX,"");
_setValue($SHIPPING_LN_TEXTBOX,"");
_setValue($SHIPPING_ADDRESS1_TEXTBOX,"");
_setValue($SHIPPING_ADDRESS2_TEXTBOX,"");
_setSelected($SHIPPING_COUNTRY_DROPDOWN,"");
_setSelected($SHIPPING_STATE_DROPDOWN,"Select...");
_setValue($SHIPPING_CITY_TEXTBOX,"");
_setValue($SHIPPING_ZIPCODE_TEXTBOX, ""); 
_setValue($SHIPPING_PHONE_TEXTBOX,"");
//shipping continue button should disable
_assert($SHIPPING_SUBMIT_BUTTON.disabled);
//background color
_assertEqual($staticPages[0][5],_style($SHIPPING_FN_TEXTBOX,"border-color"));
_assertEqual($staticPages[0][5],_style($SHIPPING_LN_TEXTBOX,"border-color"));
_assertEqual($staticPages[0][5],_style($SHIPPING_ADDRESS1_TEXTBOX,"border-color"));
_assertEqual($staticPages[0][5],_style($SHIPPING_CITY_TEXTBOX,"border-color"));
_assertEqual($staticPages[0][5],_style($SHIPPING_ZIPCODE_TEXTBOX,"border-color"));
_assertEqual($staticPages[0][5],_style($SHIPPING_STATE_DROPDOWN,"border-color"));
_assertEqual($staticPages[0][5],_style($SHIPPING_PHONE_TEXTBOX,"border-color"));
//Error messages comparison
_assertEqual($requestCatalog[4][12],_getText($SHIPPING_FN_ERROR));
_assertEqual($requestCatalog[4][12],_getText($SHIPPING_LN_ERROR));
_assertEqual($requestCatalog[4][12],_getText($SHIPPING_ADDRESS1_ERROR));
_assertEqual($requestCatalog[4][12],_getText($SHIPPING_CITY_ERROR));
_assertEqual($requestCatalog[4][12],_getText($SHIPPING_ZIPCODE_ERROR));
_assertEqual($requestCatalog[4][12],_getText($SHIPPING_STATE_ERROR));
_assertEqual($requestCatalog[4][12],_getText($SHIPPING_PHONE_ERROR));
									   
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//###################### Related to forget password ###########################

var $t = _testcase("357904/357906","Verify the UI of Request to reset your password Modal window as an guest user.");
$t.start();
try
{

//user account link
_click($HEADER_USERACCOUNT_LINK);
//click on log in link
_click($HEADER_LOGIN_LINK);
//click on password reset link
_click(_link("password-reset"));
_assertVisible(_div("Reset Your Password"));

//Dialogue should be visible
_assertVisible(_div("dialog-container"));
_assertVisible(_div("/Reset Your Password/"));
_assertEqual("Provide your account email address to reset your password.", _getText(_paragraph("pwd-text")));
_assertVisible(_textbox("dwfrm_requestpassword_email"));
_assertVisible(_submit("Send"));
_assertVisible(_button("Close"));
_assertVisible(_label("� Email"));

//set an empty value
_setValue(_textbox("dwfrm_requestpassword_email"),$requestCatalog[0][13]);
//border color and error message
_assertEqual($staticPages[0][5],_style(_textbox("dwfrm_requestpassword_email"),"border-color"));

//enter the email in overlay
_setValue(_textbox("dwfrm_requestpassword_email"),$staticPages[1][0]);
_click(_submit("Send"));
//success message
_assertVisible(_div("Thank You!"));
_click(_link("Start Shopping"));
_assertVisible(_div("homepage-hero"));
_assertEqual($staticPages[0][6], _getText(_paragraph("pwd-text")));
_assertVisible(_button("Close"));

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357905","Verify the functionality of Request to Reset your Password Form Field as an guest user.");
$t.start();
try
{
	
//user account link
_click($HEADER_USERACCOUNT_LINK);
//click on log in link
_click($HEADER_LOGIN_LINK);
//click on password reset link
_click(_link("password-reset"));
_assertVisible(_div("Reset Your Password"));
//Dialogue should be visible
_assertVisible(_div("dialog-container"));
//click on close button
_click(_button("Close"));
//should not be visible
_assertNotVisible(_div("Reset Your Password"));
//Dialogue should be visible
_assertNotVisible(_div("dialog-container"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//###################### Related to reset password migrated customers ###########################

_log("################################ Has Customer Migrated to DW checkbox should check in BM ###################################");

_log("UN:DWpendletonreset@gmail.com ,PWD: Pendleton@12");

var $t = _testcase("357914/357916/357917/357918/","Verify the UI of Request to reset your password Modal window for Migrated Customers as an guest user.");
$t.start();
try
{
	
//Enter valid credentials
_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$staticPages[0][8]);
_setValue($LOGIN_PASSWORD_TEXTBOX,$staticPages[1][8]);
_click($LOGIN_SUBMIT_BUTTON);
//reset password overlay should open
_assertVisible(_div("Reset Your Password"));
//Dialogue should be visible
_assertVisible(_div("dialog-container"));
//Dialogue should be visible
_assertVisible(_div("dialog-container"));
_assertVisible(_div("/Reset Your Password/"));
_assertEqual("Provide your account email address to reset your password.", _getText(_paragraph("pwd-text")));
_assertVisible(_textbox("dwfrm_requestpassword_email"));
_assertVisible(_submit("Send"));
_assertVisible(_button("Close"));
_assertVisible(_label("� Email"));
//click on close button
_click(_button("Close"));
//should not be visible
_assertNotVisible(_div("Reset Your Password"));
//Dialogue should be visible
_assertNotVisible(_div("dialog-container"));

//Enter valid credentials
_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$staticPages[0][8]);
_setValue($LOGIN_PASSWORD_TEXTBOX,$staticPages[1][8]);
_click($LOGIN_SUBMIT_BUTTON);

//set an empty value
_setValue(_textbox("dwfrm_requestpassword_email"),$requestCatalog[0][13]);
//border color and error message
_assertEqual($staticPages[0][5],_style(_textbox("dwfrm_requestpassword_email"),"border-color"));

//enter the email in overlay
_setValue(_textbox("dwfrm_requestpassword_email"),$staticPages[1][0]);
_click(_submit("Send"));
//success message
_assertVisible(_div("Thank You!"));
_click(_link("Start Shopping"));
_assertVisible(_div("homepage-hero"));
_assertEqual($staticPages[0][6], _getText(_paragraph("pwd-text")));
_assertVisible(_button("Close"));
//click on close button
_click(_button("Close"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357915","Verify the functionality of Request to Reset your Password Form Field as an guest user.");
$t.start();
try
{

	
//Enter valid credentials
_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$staticPages[0][8]);
_setValue($LOGIN_PASSWORD_TEXTBOX,$staticPages[1][8]);
_click($LOGIN_SUBMIT_BUTTON);
	
//validating the email id & password field's
for(var $i=0;$i<$ForgotPassword_Validation.length;$i++)
	{

	if($i==5)
		{
			_setValue($LOGIN_FORGETPASSWORD_TEXTBOX,$ForgotPassword_Validation[$i][1]);
		}
	else
		{
			_setValue($LOGIN_FORGETPASSWORD_TEXTBOX,$ForgotPassword_Validation[$i][1]);
			_click($LOGIN_FORGETPASSWORD_SEND_BUTTON);
			_wait(2000);
		}
		
	if (isMobile())
		{
		_wait(3000);
		}
	
	//blank
	if($i==0)
		{
		//username
		_assertEqual($ForgotPassword_Validation[$i][4],_style($LOGIN_FORGETPASSWORD_TEXTBOX,"background-color"));
		_assertVisible(_span($ForgotPassword_Validation[$i][3]));
		}
	
	//in valid email
	else if($i==1 || $i==2 || $i==3 || $i==4 ||$i==6 ||$i==7 ||$i==8)
	{
		_assertEqual($ForgotPassword_Validation[3][3], _getText($LOGIN_FORGETPASSWORD_EMAIL_ERRORMESSAGE));
	}
	
		
	//More Number of characters in email field
	else if ($i==5)
	{
		_assertEqual($ForgotPassword_Validation[5][2],_getText($LOGIN_FORGETPASSWORD_TEXTBOX).length);
	}
//valid
else
	{
		if (isMobile())
		{
			_wait(3000,_isVisible($MY_ACCOUNT_OPTIONS));
		}
			//verify the navigation
			_assertVisible($LOGIN_RESETYOURPASSWORD_CONFIRMATIONTEXT);
			_assertVisible($LOGIN_FORGETPASSWORD_CLOSEBUTTON);
			_assertVisible($LOGIN_FORGETPASSWORD_OVERLAY_TEXT);
			_assertVisible($HEADER_HOME_LINK);
			//closing the dialog
			_click($LOGIN_FORGETPASSWORD_CLOSEBUTTON);
			_wait(2000);
	}
}
	
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase(" ", " ");
$t.start();
try
{
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//Login to the application	
_click($HEADER_LOGIN_LINK);
//login to the application
login();

var $t = _testcase("357907/357908/357913/357913/357923","Verify the functionality of Password Reset Acknowledgement Resource Message as an guest user./Verify the UI of Password Reset Email Content Asset" +
"[Responsive]:Verify the UI of Request to reset your password Modal window as an guest user.");
$t.start();
try
{

//user account link
_click($HEADER_USERACCOUNT_LINK);
//click on log in link
_click($HEADER_LOGIN_LINK);
//click on password reset link
_click(_link("password-reset"));
_assertVisible(_div("Reset Your Password"));

//Dialogue should be visible
_assertVisible(_div("dialog-container"));
_assertVisible(_div("/Reset Your Password/"));
_assertEqual("Provide your account email address to reset your password.", _getText(_paragraph("pwd-text")));
_assertVisible(_textbox("dwfrm_requestpassword_email"));
_assertVisible(_submit("Send"));
_assertVisible(_button("Close"));
_assertVisible(_label("� Email"));

//set an empty value
_setValue(_textbox("dwfrm_requestpassword_email"),$requestCatalog[0][13]);
//border color and error message
_assertEqual($staticPages[0][5],_style(_textbox("dwfrm_requestpassword_email"),"border-color"));

//enter the email in overlay
_setValue(_textbox("dwfrm_requestpassword_email"),$staticPages[1][0]);
_click(_submit("Send"));
//success message
_assertVisible(_div("Thank You!"));
_click(_link("Start Shopping"));
_assertVisible(_div("homepage-hero"));
_assertEqual($staticPages[0][6], _getText(_paragraph("pwd-text")));
_assertVisible(_button("Close"));

}
catch($e)
{
_logExceptionAsFailure($e);
}	
$t.end();

logout();

var $t = _testcase("357974/357975","Verify the functionality of Share through Email CTA in wishlist page as registered user");
$t.start();
try
{

//Add items to wishlist
additemsToWishListAccount($productQuantity[0][0]);
//Login to the application	
_click($HEADER_LOGIN_LINK);
//login to the application
login();
//click on wishlist link
_click($HEADER_USERACCOUNT_LINK);
//click on wishlist link
_click(_link("Wish List"));
//make it public
if (_isVisible(_submit("dwfrm_wishlist_setListPublic")))	
{
	_click(_submit("dwfrm_wishlist_setListPublic"));
}
//email icon should display
_assertVisible(_link("Email"));
_assertVisible(_link("share-icon"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase(" ", " ");
$t.start();
try
{
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/