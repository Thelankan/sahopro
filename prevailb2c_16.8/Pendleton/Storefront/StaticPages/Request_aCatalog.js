_include("../../GenericLibrary/GlobalFunctions.js");
_resource("StaticPages.xls");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

var $requestaCatalogPage=_getAttribute(_link("REQUEST A CATALOG"),"href");

var $t = _testcase("358013/358014/358015/358016/358018/358019/358000/358020/358027/358028/358011","Verify the functionality of Customer First Name Form Field in Request a Catalog page both as  an anonymous and a registered user." +
		"Verify the functionality of Title Form Field  in Request a Catalog page  both as  an anonymous and a registered user.");
$t.start();
try
{

//click on request catalog link
_click($RC_LINK);
_assertVisible(_div("Request a catalog � required"));
_assertVisible(_div("Please Select Catalog(s)"));
_assertVisible(_div("catalog-request-page-banner accounts-banner"));
_assertEqual("customer service Request a Catalog", _getText(_div("breadcrumb")));

//entering all the values in request a catalog
_assertVisible(_setSelected($RC_TITLE_DROPDOWN));
_assertVisible(_setValue($RC_FIRSTNAME_TEXTBOX));
_assertVisible(_setValue($RC_MIDDLENAME_TEXTBOX));
_assertVisible(_setValue($RC_LASTNAME_TEXTBOX));
_assertVisible(_setSelected($RC_SUFFIX_DROPDOWN));
_assertVisible(_setValue($RC_ADDRESS1_TEXTBOX));
_assertVisible(_setValue($RC_ADDRESS2_TEXTBOX));
_assertVisible(_setValue($RC_ADDRESS2_APRTNUMBER_TEXTBOX));
_assertVisible(_setValue($RC_CITY_TEXTBOX));
_assertVisible(_setSelected($RC_STATE_TEXTBOX));
_assertVisible(_setValue($RC_ZIPCODE_TEXTBOX));
//Entering values	
RequestCatalog($Category,0);
//first name value
_assertEqual($requestCatalog[0][2],_getText($RC_FIRSTNAME_TEXTBOX));
_assertEqual($requestCatalog[2][2],_getText($RC_LASTNAME_TEXTBOX));
_assertEqual($requestCatalog[0][6],_getText($RC_ADDRESS1_TEXTBOX));
_assertEqual($requestCatalog[0][7],_getText($RC_ADDRESS2_TEXTBOX));
_assertEqual($requestCatalog[0][8],_getText($RC_ADDRESS2_APRTNUMBER_TEXTBOX));
_assertEqual($requestCatalog[0][9],_getText($RC_CITY_TEXTBOX));
_assertEqual($requestCatalog[0][10],_getText($RC_STATE_TEXTBOX));
_assertEqual($requestCatalog[0][11],_getText($RC_ZIPCODE_TEXTBOX));
//click on submit button in request catalog
_click($RC_SUBMIT_BUTTON);
//success message should display
_assertEqual($requestCatalog[0][12], _getText(_div("confirm-catalog-request")));

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("358021/358022","Verify the functionality of City Form Field in Request a Catalog page both as  an anonymous and a registered user." +
		"Verify the functionality of State Form Field in Request a Catalog page both as  an anonymous and a registered user.");
$t.start();
try
{

//click on request catalog link
_click($RC_LINK);	
//Request a catalog	
RequestCatalog($Category,4);
//display the error messages
_assertEqual($requestCatalog[4][12], _getText(_span("dwfrm_catalogrequest_firstname-error")));
_assertEqual($requestCatalog[4][12], _getText(_span("dwfrm_catalogrequest_lastname-error")));
_assertEqual($requestCatalog[4][12], _getText(_span("dwfrm_catalogrequest_address1-error")));
_assertEqual($requestCatalog[4][12], _getText(_span("dwfrm_catalogrequest_city-error")));
_assertEqual($requestCatalog[4][12], _getText(_span("dwfrm_catalogrequest_stateslist_states_state-error")));
_assertEqual($requestCatalog[4][12], _getText(_span("dwfrm_catalogrequest_zipcode-error")));
		
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("358027","Verify the functionality of Re-Enter Email Address Form Field in Request a Catalog page both as  an anonymous and a registered user.");
$t.start();
try
{
	
//click on request catalog link
_click($RC_LINK);
//Not entering all the values in request a catalog
_setSelected($RC_TITLE_DROPDOWN,$sheet[$i][1]);
_setValue($RC_FIRSTNAME_TEXTBOX,$sheet[$i][2]);
_setValue($RC_MIDDLENAME_TEXTBOX,$sheet[$i][3]);
_setValue($RC_LASTNAME_TEXTBOX,$sheet[$i][4]);
_setSelected($RC_SUFFIX_DROPDOWN,$sheet[$i][5]);
_setValue($RC_ADDRESS1_TEXTBOX,$sheet[$i][6]);
_setValue($RC_ADDRESS2_TEXTBOX,$sheet[$i][7]);
_setValue($RC_ADDRESS2_APRTNUMBER_TEXTBOX,$sheet[$i][8]);
_setValue($RC_CITY_TEXTBOX,$sheet[$i][9]);
_setValue($RC_ZIPCODE_TEXTBOX,$sheet[$i][11]);
//should verify that button should disabled
_assertEqual(true,_getAttribute(_submit("dwfrm_catalogrequest_send"),"disabled"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("358026/358025","Verify the functionality of Re-Enter Email Address Form Field in Request a Catalog page both as  an anonymous and a registered user." +
		"Verify the functionality of Contact Email Address Form Field in Request a Catalog page both as  an anonymous and a registered user.");
$t.start();
try
{

//enter the value	
_setValue($RC_EMAIL_TEXTBOX,$requestCatalog[0][0]);
_setValue($RC_EMAIL_CONFIRM_TEXTBOX,$requestCatalog[0][0]);
//Comparing the Values
_assertEqual($requestCatalog[0][0],_getText($RC_EMAIL_TEXTBOX));
_assertEqual($requestCatalog[0][0],_getText($RC_EMAIL_CONFIRM_TEXTBOX));
//in valid email format
_assertEqual($requestCatalog[3][0],_getText($RC_EMAIL_TEXTBOX));
_assertEqual($requestCatalog[3][0],_getText($RC_EMAIL_CONFIRM_TEXTBOX));
_click($RC_SUBMIT_BUTTON);
//error Message should display
_assertEqual($requestCatalog[3][1], _getText($RC_EMAIL_ERROR_MESSAGE));
_assertEqual($requestCatalog[3][1], _getText($RC_EMAIL_CONFIRM_ERROR_MESSAGE));
//re-enter the value
_setValue($RC_EMAIL_TEXTBOX,$requestCatalog[1][0]);
_setValue($RC_EMAIL_CONFIRM_TEXTBOX,$requestCatalog[1][0]);
//Comparing the Values
_assertEqual($requestCatalog[1][0],_getText($RC_EMAIL_TEXTBOX));
_assertEqual($requestCatalog[1][0],_getText($RC_EMAIL_CONFIRM_TEXTBOX));
//check the sign up check box
_check(_checkbox("dwfrm_catalogrequest_signup"));
//email fields will become mandatory
_assertVisible(_textbox("input-text valid email required",_in(_div("Email"))));
_assertVisible(_textbox("input-text valid email required",_in(_div("Re-enter Email"))));
//enter the value	
_setValue($RC_EMAIL_TEXTBOX,$requestCatalog[0][13]);
_setValue($RC_EMAIL_CONFIRM_TEXTBOX,$requestCatalog[0][13]);
_click($RC_SUBMIT_BUTTON);
//error Message should display
_assertEqual($requestCatalog[4][12], _getText($RC_EMAIL_ERROR_MESSAGE));
_assertEqual($requestCatalog[4][12], _getText($RC_EMAIL_CONFIRM_ERROR_MESSAGE));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("358006/358010","Verify the functionality of Request A Catalog Page Content Content Asset both as  an anonymous and a registered user./Verify the functionality of Catalog Image Content Asset in Request a Catalog page both as  an anonymous and a registered user.");
$t.start();
try
{

//Catalog Count	
var $catalogCount=_count("_div","/cat-img/",_in(_div("catalog-images")));
//

for (var $i=0;$i<$catalogCount;$i++)
{

//Entering values	
RequestCatalog($Category,"0");
_assertVisible(_checkbox("dwfrm_catalogrequest_cat["+$i+"]"));
//Enter proper data in request catalog
if (_getAttribute(_checkbox("dwfrm_catalogrequest_cat["+$i+"]"),"value")==true)
{
	_uncheck(_checkbox("dwfrm_catalogrequest_cat["+$i+"]"));
}
//click on submit button in request catalog
_click($RC_SUBMIT_BUTTON);
//Error message should display
_assertEqual($requestCatalog[1][12], _getText(_span("catalogerror")));

//Check the check box
if (_getAttribute(_checkbox("dwfrm_catalogrequest_cat["+$i+"]"),"value")==false)
{
	_check(_checkbox("dwfrm_catalogrequest_cat["+$i+"]"));
}
//click on submit button in request catalog
_click($RC_SUBMIT_BUTTON);
//Error Message should not display
_assertNotVisible(_span("catalogerror"));
//success message should display
_assertEqual($requestCatalog[0][12], _getText(_div("confirm-catalog-request")));

}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("358008","Verify the display of *Required Resource Message in Request A Catalog Page both as an anonymous and a registered user.");
$t.start();
try
{

//Required Fields	
_assertVisible($RC_FIRSTNAME_LABEL);
_assertVisible($RC_LASTNAME_LABEL);
_assertVisible($RC_ADDRESS1_LABEL);
_assertVisible($RC_CITY_LABEL);
_assertVisible($RC_STATE_LABEL);
_assertVisible($RC_ZIPCODE_LABEL);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("358023/358024","Verify the functionality of Zip code Form Field in Request a Catalog page both as an anonymous and a registered user.");
$t.start();
try
{
	
//click on request catalog link
_click($RC_LINK);
//set less than 5 characters
_setValue($RC_ZIPCODE_TEXTBOX,$requestCatalog[1][11]);
_click($RC_SUBMIT_BUTTON);
_assertEqual($requestCatalog[2][12], _getText(_span("dwfrm_catalogrequest_zipcode-error")));
//enter more than 5 characters
_setValue($RC_ZIPCODE_TEXTBOX,$requestCatalog[1][11]);
_assertEqual($requestCatalog[0][11],_getText($RC_ZIPCODE_TEXTBOX));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("358001/357984","Verify the functionality of Page Breadcrumb in Request a Catalog page as guest and registered user");
$t.start();
try
{

//click on bread crumb	
_click($PAGE_BREADCRUMB);
//Should be in same page
_click(_div("breadcrumb"));
_assertVisible(_div("Request a catalog � required"));
//click on customer service link in bread crumb
_click(_link("Customer Service", _in(_div("breadcrumb"))));
_assertVisible(_heading1("Welcome to Customer Service"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase(" ", " ");
$t.start();
try
{
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase(" ", " ");
$t.start();
try
{
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/
