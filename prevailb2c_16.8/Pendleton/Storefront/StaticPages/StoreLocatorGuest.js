_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("QuickView.xls");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


var $t = _testcase("357926/357924/357925","[Responsive]:Verify the UI  of store locator results found page both as an anonymous and a registered user.");
$t.start();
try
{
	
//click on store locator link
_click($STORELOCATOR_LINK);
//verify the heading 
_assertVisible($STORELOCATOR_HEADING);
//bread crumb for store locator 
_assertEqual($storelocator[0][0], _getText($PAGE_BREADCRUMB));
//store locator heading
_assertVisible($STORELOCATOR_POSTAL_CODE_TEXTBOX);
_assertVisible($STORELOCATOR_PARAGRAPH_HEADING);
_assertVisible($STORELOCATOR_RADIUS_DROPDOWN);
_assertVisible($STORELOCATOR_STATE_DROPDOWN);
_assertVisible($STORELOCATOR_OR_TEXT);
_assertVisible($STORELOCATOR_STATE_DROPDOWN);
_assertVisible($STORELOCATOR_SEARCHBUTTON_BELOW_STATEDROPDOWN);
_assertVisible($STORELOCATOR_RESULT_STORE_EVENTS);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357927","Verify the functionality of  Store Types Form Field in store locator page both as an anonymous and a registered user.");
$t.start();
try
{
	
var $countOfCheckboxes=_count("_checkbox","/input-checkbox/",_in(_div("store-types")));

for (var $i=0;$i<$countOfCheckboxes;$i++)
{
	//verify the presence of checkbox
	_assertVisible(_checkbox("dwfrm_storelocator_store"+"["+$i+"]"));
	//check whether it is checked by default or not
	_checkbox("dwfrm_storelocator_store"+"["+$i+"]").checked;
}

for (var $i=0;$i<$countOfCheckboxes;$i++)
{
	//check whether it is checked by default or not
	_uncheck(_checkbox("dwfrm_storelocator_store"+"["+$i+"]"));
	_assertEqual(false,_checkbox("dwfrm_storelocator_store"+"["+$i+"]").checked);
}


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357943/357928/357932/357938","Verify the functionality of Store Map CTA in store locator results found page both as an anonymous and a registered user.");
$t.start();
try
{

//set values in store locator
_setValue($STORELOCATOR_POSTAL_CODE_TEXTBOX,$storelocator[0][1]);
_setSelected($STORELOCATOR_RADIUS_DROPDOWN,$storelocator[1][1]);
//postal code should accept
_assertEqual($storelocator[0][1],_getText($STORELOCATOR_POSTAL_CODE_TEXTBOX));
//should not accept more than 5 chars
_setValue($STORELOCATOR_POSTAL_CODE_TEXTBOX,$storelocator[0][2]);
_assertEqual($storelocator[0][1],_getText($STORELOCATOR_POSTAL_CODE_TEXTBOX));
//invalid zip code
_setValue($STORELOCATOR_POSTAL_CODE_TEXTBOX,$storelocator[1][2]);
//click on submit button
_click($STORELOCATOR_SEARCHBUTTON_BELOW_RADIUSDROPDOWN);
//error message should display
_assertEqual($storelocator[0][3],_getText($STORELOCATOR_ZIPCODE_ERROR_MESSAGE));
//select state
_setSelected($STORELOCATOR_STATE_DROPDOWN,$storelocator[2][1]);
_click($STORELOCATOR_SEARCHBUTTON_BELOW_STATEDROPDOWN);
//store locator HEADING
_assertVisible($STORELOCATOR_RESULTS_PAGE);
_assertVisible($STORELOCATOR_RESULTS_PAGE);
_assertVisible($STORELOCATOR_RESULT_HEADING);
_assertVisible($STORELOCATOR_BACKTO_SL_LINK);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357936/357939/357930/357935/357937", "Verify the functionality of Back to Store in store locator results page both as an anonymous and a registered user./Verify the functionality of Radius Search (1) CTA in store locator page both as an anonymous and a registered user.");
$t.start();
try
{
	
//click on store locator link
_click($STORELOCATOR_LINK);
//enter text and click on submit button 
_setValue($STORELOCATOR_POSTAL_CODE_TEXTBOX,$storelocator[0][1]);
_setSelected($STORELOCATOR_RADIUS_DROPDOWN,$storelocator[1][1]);
//click on submit button
_click($STORELOCATOR_SEARCHBUTTON_BELOW_RADIUSDROPDOWN);
//store locator results page
_assertContainsText($storelocator[0][1], $STORELOCATOR_CELL_ADDRESS);
_assertVisible($STORELOCATOR_RESULTS_PAGE);
_assertVisible($STORELOCATOR_RESULT_HEADING);
_assertVisible($STORELOCATOR_BACKTO_SL_LINK);
//store locator page
_assertVisible($STORELOCATOR_STORE_NAME);
_click($STORELOCATOR_STORE_NAME);
_assertVisible($STORELOCATOR_BACKTO_SL_LINK);
//back to store functionality
_click($STORELOCATOR_BACKTO_SL_LINK);
//verify the heading 
_assertVisible($STORELOCATOR_HEADING);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357941","Verify the functionality of Product Attributes  in store locator results found page both as an anonymous and a registered user.");
$t.start();
try
{
	
//Store locator UI verification
_assertVisible($STORELOCATOR_RESULT_STORENAME);
_assertVisible($STORELOCATOR_RESULT_ADDRESS);
_assertVisible($STORELOCATOR_RESULT_DIRECTIONS);
_assertVisible($STORELOCATOR_STORE_NAME);
_assertVisible($STORELOCATOR_MAP_LINK);
//Store Events
_assertVisible(_div("str-evnts"));
_assertVisible(_div("store-events"));
//Store Address
_assertVisible(_div("str-hours"));
//store hours
_assertVisible(_div("store-hours"));

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357943/357949","Verify the functionality of Store Map CTA in store locator results found page both as an anonymous and a registered user.");
$t.start();
try
{

//enter text and click on submit button 
_setValue($STORELOCATOR_POSTAL_CODE_TEXTBOX,$storelocator[0][1]);
_setSelected($STORELOCATOR_RADIUS_DROPDOWN,$storelocator[1][1]);
//click on submit button
_click($STORELOCATOR_SEARCHBUTTON_BELOW_RADIUSDROPDOWN);
//click on map link
_click($STORELOCATOR_MAP_LINK);
//click on map link
_popup("10 Presidential Way - Google Maps")._assertVisible($STORELOCATOR_MAP_DIRECTIONS);
_popup("10 Presidential Way - Google Maps").close();
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357931","Verify the functionality of State Search Form Field in store locator page both as an anonymous and a registered user.");
$t.start();
try
{
	
//click on store locator link
_click($STORELOCATOR_LINK);
//all Us states should display
_assertEqual($storelocator[0][4], _getText($STORELOCATOR_STATE_DROPDOWN));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase(" ", " ");
$t.start();
try
{
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*
var $t = _testcase(" ", " ");
$t.start();
try
{
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/