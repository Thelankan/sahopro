_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("QuickView.xls");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();

//Login to the application	
_click($HEADER_LOGIN_LINK);
//login to the application
login();

var $t = _testcase("357927", "Verify the functionality of  Store Types Form Field in store locator page both as an anonymous and a registered user.");
$t.start();
try
{

//click on store locator link
_click($STORELOCATOR_LINK);
//verify the heading 
_assertVisible($STOREFRONT_HEADING);
//bread crumb for store locator 
_assertEqual($storelocator[0][0], _getText($PAGE_BREADCRUMB));
	
for (var $i=0;$i<$countOfCheckboxes;$i++)
{
	//verify the presence of checkbox
	_assertVisible(_checkbox("dwfrm_storelocator_store"+"["+$i+"]"));
	//check whether it is checked by default or not
	_checkbox("dwfrm_storelocator_store"+"["+$i+"]").checked;
}

for (var $i=0;$i<$countOfCheckboxes;$i++)
{
	//check whether it is checked by default or not
	_uncheck(_checkbox("dwfrm_storelocator_store"+"["+$i+"]"));
	_assertEqual(false,_checkbox("dwfrm_storelocator_store"+"["+$i+"]").checked);
}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357929/357932","Verify the functionality of  Radius Form Field in store locator page both as an anonymous and a registered user.");
$t.start();
try
{

//set values in store locator
_setValue($STORELOCATOR_POSTAL_CODE_TEXTBOX,$storelocator[0][1]);
_setSelected($STORELOCATOR_RADIUS_DROPDOWN,$storelocator[1][1]);
//postal code should accept
_assertEqual($storelocator[0][1],_getText($STORELOCATOR_POSTAL_CODE_TEXTBOX));
//should not accept more than 5 chars
_setValue($STORELOCATOR_POSTAL_CODE_TEXTBOX,$storelocator[0][2]);
_assertEqual($storelocator[0][1],_getText($STORELOCATOR_POSTAL_CODE_TEXTBOX));
//invalid zip code
_setValue($STORELOCATOR_POSTAL_CODE_TEXTBOX,$storelocator[1][2]);
//click on submit button
_click($STORELOCATOR_SEARCHBUTTON_BELOW_RADIUSDROPDOWN);
//error message should display
_assertEqual($storelocator[0][3],_getText($STORELOCATOR_ZIPCODE_ERROR_MESSAGE));
//select state
_setSelected($STORELOCATOR_STATE_DROPDOWN,$storelocator[2][1]);
_click($STORELOCATOR_SEARCHBUTTON_BELOW_STATEDROPDOWN);
//verification is pending
_assertVisible($STORELOCATOR_RESULTS_PAGE);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357943/357949/357944","Verify the functionality of Store Map CTA in store locator results found page both as an anonymous and a registered user.");
$t.start();
try
{

//enter text and click on submit button 
_setValue($STORELOCATOR_POSTAL_CODE_TEXTBOX,$storelocator[0][1]);
_setSelected($STORELOCATOR_RADIUS_DROPDOWN,$storelocator[1][1]);
//click on submit button
_click($STORELOCATOR_SEARCHBUTTON_BELOW_RADIUSDROPDOWN);
//click on map link
_popup("10 Presidential Way - Google Maps")._assertVisible($STORELOCATOR_MAP_DIRECTIONS);
_popup("10 Presidential Way - Google Maps").close();
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase(" ", " ");
$t.start();
try
{
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();