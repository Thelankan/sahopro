_include("../../GenericLibrary/GlobalFunctions.js");
_resource("Cart.xls");

SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);

var $t = _testcase("352786/352765/352781","Verify the UI of 'Cart' / UI of 'Promo code' and total section/UI of Line Items displayed  in the application as an anonymous user.");
$t.start();
try
{
	
	//Adding Item to the cart
	addToCartSearch($Item[0][0],$Item[0][1]);
	_click($MINICART_LINK); 
	//click on slot1
	if (_isVisible(_div("top-main-banner")))
	{
		_click(_div("top-main-banner"));
	}
	if(_isVisible(_div("pdpMain")))
		{
		_assertVisible(_div("pdpMain"));
		}
	else
		{
		_assert(false,"Not navigating to PDP");
		}
	 //cart navigation verification
	 _assertVisible($CART_TABLE);
	 _assertVisible($CART_ACTIONBAR);
	//continue shopping at top section
	 _assertVisible($CART_CONTINUESHOPPING);
	 //Express paypal
	 _assertVisible($EXPRESS_PAYPAL_TOP);
	 //checkout button
	 _assertVisible($CART_CHECKOUT_BUTTON);	 
	//LineItems
	if(_isVisible($CART_ROW))
		{
			_assertVisible($CART_PRODUCTLIST_ITEM, _in($CART_ROW));
		}
	else
		{
			_log("Cart is Empty");			
		}
	
	var $TotCartItems=_count("_div","/product-list-item/",_in(_table("cart-table")));
	//Verifying all Details of line items in cart page
	if(!isMobile() || mobile.iPad())
		{
			//table Heading
			_assertVisible($CART_PRODUCT_HEADING);
			_assertVisible($CART_QUANTITY_HEADING);
			_assertVisible($CART_PRICE_HEADING);
			_assertVisible($CART_TOTAL_HEADING);	
		}
	_assertVisible($CART_IMAGE_CELL);
	_assertVisible($CART_QUANTITY_CELL);
	_assertVisible($CART_PRICE_CELL);
	_assertVisible($CART_ITEM_TOTAL_CELL);
	for(var $i=0; $i<$TotCartItems; $i++)
		{		
			_assertVisible($CART_PRODUCTLIST_ITEM,_in(_row("cart-row["+$i+"]")));
			//Name
			_assertVisible($CART_NAME,_in(_row("cart-row["+$i+"]")));
			//image
			_assertVisible($CART_IMAGE_CELL,_in(_row("cart-row["+$i+"]")));
			//SKU
			_assertVisible($CART_SKU, _in(_row("cart-row["+$i+"]")));
			//edit details
			_assertVisible($CART_EDIT_DETAILS, _in(_row("cart-row["+$i+"]")));
			//delivery option
			_assertVisible($CART_DELIVERYOPTION_VALUE, _in(_row("cart-row["+$i+"]")));
			//quantity
			_assertVisible($CART_QTY_DROPDOWN, _in(_row("cart-row["+$i+"]")));
			//availability
			_assertVisible($CART_AVAILABILITY_MESSEGE, _in(_row("cart-row["+$i+"]")));
			//remove
			_assertVisible($CART_REMOVE_LINK,_in(_row("cart-row["+$i+"]")));
			//add to wish list
			_assertVisible($CART_ADDTO_WISHLIST_LINK,_in((_row("cart-row["+$i+"]"))));
			//Unit price
			_assertVisible($CART_SALES_PRICE,_in(_row("cart-row["+$i+"]")));
			_assertVisible($CART_LINEITEM_PRICE,	_in(_row("cart-row["+$i+"]")));
		}
		
	//Coupon code section
	_assertVisible($CART_COUPON_SECTION);
	//Text box and Apply button
	_assertVisible($CART_COUPON_TEXTBOX);
	_assertVisible($CART_COUPON_APPLY);	
	//update button
	_assertVisible($CART_UPDATE_BUTTON);
	
	//Order total section
	_assertVisible($CART_TOTAL_SECTION);
	//subtotal
	_assertVisible($CART_SUBTOTAL);
	//shipping charge
	_assertVisible($CART_SHIPPINGPRICE);
	//sales tax
	_assertVisible($CART_SALESTAX);
	//estimated total
	_assertVisible($CART_ESTIMATED_TOTAL);

	//Continue shopping Button
	_assertVisible($CART_CONTINUESHOPPING_BOTTOM);
	//express paypal
	_assertVisible($EXPRESS_PAYPAL_BOTTOM);
	//Checkout Button
	_assertVisible($CART_CHECKOUT_BUTTON_BOTTOM);
	//featured products
	_assertVisible($CART_FEATURED);
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t = _testcase("352827/352667/352668","Verify the UI of empty cart page.");
$t.start();
try
{
	
//Removing Cart Items
ClearCartItems();
//continue shopping link
_assertVisible($CART_CONTINUESHOPPING);
//featured products
_assertVisible($CART_RECOMMENDATION_HEADING);
//Verifying "Your Shopping Cart is Empty" message
_assertVisible($CART_EMPTY_HEADING);
//Header UI
_assertVisible(_div("top-banner"));
//footer UI
_assertVisible(_div("footer-container"));
//click on continue shopping button
_click($CART_CONTINUESHOPPING);
//should navigate to home page
//Homeoage banner image
_assertVisible(_div("banner-image"));
//3rd slot
_assertVisible(_div("homepage-large home-category-1"));
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("352795/155590" ,"Verify the Edit Details link functionality for each line item in cart page/Minicart update as a guest user.");
$t.start();
try
{
	//Add item to the cart
	addToCartSearch($Item[0][0],$Item[0][1]);
	_click($MINICART_LINK);  
	//Capturing line item details
	var $SKUBefore=_getText($CART_SKUNUMBER);
	var $SubTotalBefore=_extract(_getText($CART_SUBTOTAL),"/[$](.*)/",true);
	if($colorBoolean==true)
		{
			var $ColorVarianceBefore=_getText($CART_COLOR_VARIANCE);
		}
	if($sizeBoolean== true)
		{
			var $SizeVarianceBefore=_getText($CART_SIZE_VARIANCE);	
		}
	if($widthBoolean==true)
		{		
			var $WidthVarianceBefore=_getText($CART_WIDTH_VARIANCE);	
		}
	//To click on the the edit details link seen in the cart page of the first line item 
    _click($CART_EDIT_DETAILS);
    //Quick view dialog should visible
    _assertVisible($QUICKVIEW_DIALOG);
    var $colorAttribute=false;
    var $sizeAttribute=false;
    var $widthAttribute=false;
    
	//select different swatches    
    if(_isVisible($QUICKVIEW_COLOR_LIST) && _count("_link","/swatchanchor/", _in($QUICKVIEW_COLOR_LIST))>1)
	    {
	       _click($QUICKVIEW_COLOR_SWATCH);
	       $colorAttribute=true;
	    }
	if(_isVisible($QUICKVIEW_SIZE_LIST) && _count("_link","/swatchanchor/", _in($QUICKVIEW_SIZE_LIST))>1)
	    {
	       _click($QUICKVIEW_SIZE_SWATCH);
	       $sizeAttribute=true;
	    }
	if(_isVisible($QUICKVIEW_WIDTH_LIST) && _count("_link","/swatchanchor/", _in($QUICKVIEW_WIDTH_LIST))>1)
	    {
	       _click($QUICKVIEW_WIDTH_SWATCH);
	       $widthAttribute=true;
	    }  
    //set the quantity
	_setValue($QUICKVIEW_QUANTITY_BOX,$Item[1][1]);
	//Update
	_click($QUICKVIEW_ADDTO_CART);
	//After updating Product Details
	//variances
	if($colorAttribute==true)
		{
			_assertNotEqual($ColorVarianceBefore, _getText($CART_COLOR_VARIANCE));
		}
	if($sizeAttribute==true)
		{
			_assertNotEqual($SizeVarianceBefore, _getText($CART_SIZE_VARIANCE));
		}
	if($widthAttribute==true)
		{
			_assertNotEqual($WidthVarianceBefore, _getText($CART_WIDTH_VARIANCE));
		}
	//SKU
	_assertNotEqual($SKUBefore ,_getText($CART_SKUNUMBER));	
	//PRICE
	_assertNotEqual($SubTotalBefore,_extract(_getText($CART_SUBTOTAL),"/[$](.*)/",true));	
	
	//verification in minicart
	_mouseOver($MINICART_LINK);
	//swatches
	if($colorAttribute==true)
		{
			var $ColorVarianceMiniCart=_getText(_div("/color:/",_in(_div("mini-cart-product")))).split(" ")[1].trim();
			_assertEqual($ColorVarianceMiniCart, _getText($CART_COLOR_VARIANCE));
		}
	if($sizeAttribute==true)
		{
			var $SizeVarianceMiniCart=_getText(_div("/Size:/",_in(_div("mini-cart-product")))).split(" ")[1].trim();
			_assertEqual($SizeVarianceMiniCart, _getText($CART_SIZE_VARIANCE));
		}
	if($widthAttribute==true)
		{
			var $WidthVarianceMiniCart=_getText(_div("/width/",_in(_div("mini-cart-product")))).split(" ")[1].trim();
			_assertEqual($WidthVarianceMiniCart, _getText($CART_WIDTH_VARIANCE));
		}
	//quantity
	var $QtyMiniCart=_getText(_span("value", _in(_div("mini-cart-pricing"))));
	_assertEqual($Item[1][1],$QtyMiniCart);
	//Price
	var $PriceMiniCart=_extract(_getText(_span("mini-cart-price", _in(_div("mini-cart-pricing")))),"/[$](.*)/",true);
	_assertEqual($PriceMiniCart,_extract(_getText($CART_SUBTOTAL),"/[$](.*)/",true));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}	               
$t.end();


//Removing Cart Items
ClearCartItems();
var $t = _testcase("352773"," Verify the functionality of continue shopping link when user navigates to cart page from various pages.");
$t.start();
try
{
	//Adding Item to the cart
	addToCartSearch($Item[0][0],$Item[0][1]);
	//navigate to cart
	_click($MINICART_LINK);  
	//Capturing Recently Visited product
	var $RecenProdName=_getText($CART_NAME, _in($CART_PRODUCTLIST_ITEM));
	//To Click on continue shopping link
	_click($CART_CONTINUESHOPPING);	
	//Verifying whether its navigated to Pd-Page or not
	_assertVisible($PDP_MAIN);
	_assertEqual($RecenProdName, _getText($PDP_PRODUCTNAME));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("125262","Verify the display of product name in the cart page in the application both as an anonymous user.");
$t.start();
try
{
	//Capture Product name at PD page
	var $ProdNameAtPDP=_getText($PDP_PRODUCTNAME);
	//navigate to cart
	_click($MINICART_LINK);  		
	//verifying Product name at Cart page
	_assertEqual($ProdNameAtPDP, _getText($CART_NAME));
	//click on product name
	_click($CART_NAME_LINK);
	//verifying the navigation
	_assertVisible($PDP_MAIN);
	_assertEqual($ProdNameAtPDP, _getText($PDP_PRODUCTNAME));	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("352760","Verify the display of the Line item total as a each line item in the application both as an anonymous and a registered user");  																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																											
$t.start();
try
{	
	//navigate to cart
	_click($MINICART_LINK); 
	//calculating the line item total price as: product price * Qty = Line item price  
	OrderTotalVerification(0);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

ClearCartItems();
var $t = _testcase("352751","Verify whether user can remove the line item from cart");
$t.start(); 
try
{
	//Adding Another Item to the cart
	addToCartSearch($Item[1][0],$Item[0][1]);
	//navigate to cart
	_click($MINICART_LINK);
	_wait(2000);
	var $TotCartItems=_count("_div","/product-list-item/",_in(_table("cart-table")));
	for(var $i=0; $i<$TotCartItems;$i++)
		{
			var $TotCartItemsBefore=_count("_div","/product-list-item/",_in(_table("cart-table")));
			$COrderTotal=_extract(_getText($CART_ESTIMATED_TOTAL),"/[$](.*)/",true);
		     var $ProdName=_getText($CART_NAME,_in($CART_ROW));  
		     //Click on Remove
			_click($CART_REMOVE_LINK, _in($CART_ROW));
			//After removing Respective product,Product name should not Visible
			_assertNotVisible($ProdName);
			if($i==$TotCartItems-1)
				{
					//Verifying "Your Shopping Cart is Empty" message
					_assertVisible($CART_EMPTY_HEADING);	
				}
			else
				{					
					//Estimated Total should Change
					_assertNotEqual($EstiTotalBefore,_extract(_getText($CART_ESTIMATED_TOTAL),"/[$](.*)/",true));
				}
			//number products verification
			var $TotCartItemsAfter=_count("_div","/product-list-item/",_in(_table("cart-table")));
			_assertEqual($TotCartItemsBefore-1,$TotCartItemsAfter);
		}		
}
catch($e)
{
	 _logExceptionAsFailure($e);
}		
$t.end();


ClearCartItems();

//Till here commented

var $t=_testcase("352752","Verify whether anonymous user can add item to Wish list from cart page in the application.");
$t.start();
try
{
	//Adding Another Item to the cart
	navigateToCartSearch($Item[0][0],$Item[0][1]);
	//Click on add to wish list in cart page
	_click($CART_ADDTO_WISHLIST_LINK);
	//verifying the navigation to wishlist Login page
	_assertVisible($WISHLIST_RETURNINGCUSTOMER_SECTION);
	_assertVisible($WISHLIST_ENTIRE_LOGIN_SECTION);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("287808","Verify the display of Featured Products in cart page in the application as a guest user.");
$t.start();
try
{
	//Navigating to the cart
	 _click($MINICART_LINK);  		
	//Heading
	_assertVisible($CART_FEATURED_HEADING);
	var $TotFeaturedProds=_count("_div","/product-image/", _in($CART_FEATURED));
	for(var $i=0; $i<$TotFeaturedProds; $i++)
		{
			//Product image
			_assertVisible($CART_FEATURED_IMAGE, _in(_div("product-tile["+$i+"]", _in($CART_FEATURED_SECTION))));
			//Product name
			_assertVisible($CART_FEATURED_ITEMLINK, _in(_div("product-tile["+$i+"]", _in($CART_FEATURED_SECTION))));
			//Product Price
			_assertVisible($CART_FEATURED_PRICE,_in(_div("product-tile["+$i+"]", _in($CART_FEATURED_SECTION))));
		}	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t = _testcase("352794","Verify the response when user click on either product image or product name in Featured Products section in cart page");
$t.start();
try
{
	//To fetch the product name from the featured product section
	var $FeaturedProd= _getText($CART_FEATURED_ITEMLINK);	
	//Click on Feature Product
	_click($CART_FEATURED_ITEMLINK);	
	//verifying the navigation
	_assertEqual($FeaturedProd,_getText($PDP_PRODUCTNAME));
	_assertVisible($PDP_MAIN);
}
catch($e)
{
 _logExceptionAsFailure($e);
}		
$t.end();

var $t=_testcase("125279/125280","Verify user can update the quantity/quantity text box validation of each line item in the application both as an anonymous user");
$t.start();
try
{
	//Navigating to the cart
	 _click($MINICART_LINK);  
		for(var $i=0;$i<$Quantity.length;$i++)
			{
				if($i==0)
					{
						_focus(_numberbox("input-text"));
						_focusWindow();
						_typeNative($Quantity[$i][1]);
					}
				else
					{
						_setValue($CART_QTY_DROPDOWN,$Quantity[$i][1]);
					}				
				if($i!=2)
					{
						//click on update button
						_click($CART_UPDATE_BUTTON);
					}				
				if($i==1)
					{
						_assertVisible($CART_NONAVAILABILITY);
						_assertEqual($Quantity[1][2], _getText($CART_NONAVAILABILITY));
						_assertVisible($CART_QUANTITY_ERROR);
						_assertEqual($Quantity[1][3], _getText($CART_QUANTITY_ERROR));
					}
				else if($i==2)
					{
						_assertEqual($Quantity[$i][1].length, _getText($CART_QTY_DROPDOWN).length);
					}
				else if($i==3)
					{
						//Verifying Qty
						_assertEqual($Quantity[$i][1],_getText($CART_QTY_DROPDOWN));
						//verifying price
						_log($CItemBasicPrice);
						_log($Quantity[$i][1]);
						var $ExpectedItemPrice=parseFloat($CItemBasicPrice) * parseFloat($Quantity[$i][1]);
						var $ActualItemPrice;
						if(_isVisible($CART_LINEITEM_PRICE))
							{
								$ActualItemPrice=_extract(_getText($CART_LINEITEM_PRICE),"/[$](.*)/",true).toString();
							}
						else if(_isVisible($CART_LINEITEM_DISCOUNT_PRICE))
							{
								$ActualItemPrice=_extract(_getText($CART_LINEITEM_DISCOUNT_PRICE),"/[$](.*)/",true).toString();
							}
						_assertEqual($ExpectedItemPrice,$ActualItemPrice);
					}
				else if($i==4)
					{
						//Verifying "Your Shopping Cart is Empty" message
						_assertVisible($CART_EMPTY_HEADING);
					}
				else
					{
						_assertVisible($CART_ERROR_MESSAGE);
						_assertEqual($Quantity[$i][2], _getText($CART_ERROR_MESSAGE));
					}
			}
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

ClearCartItems();
var $t=_testcase("287922","Verify if the user can update the Qty of the line item by click on checkout button as a guest user in cart page.");
$t.start();
try
{
	//Adding Another Item to the cart
	 navigateToCartSearch($Item[0][0],$Item[0][1]);
	//update cart Qty
	_setValue($CART_QTY_DROPDOWN,$Item[1][1]);
	//click on checkout button
	_click($CART_CHECKOUT_BUTTON);
	//Should Nav to ILP page
	 _assertVisible($CHECKOUT_LOGIN_PAGE);
	 _assertVisible($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
	//navigate back to cart
	 _click($MINICART_LINK); 
	//verify whether the quantity is updated or not
	_assertNotEqual($Item[1][1],_getText($CART_QTY_DROPDOWN));
	_assertEqual($Item[0][1],_getText($CART_QTY_DROPDOWN));	 
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end(); 


var $t=_testcase("352774","Verify the navigation on click of 'Checkout' button from the cart page in the application as a anonymous user.");
$t.start();
try
{
	//navigate back to cart
	 _click($MINICART_LINK);
	 //click on checkout button
	 _click($CART_CHECKOUT_BUTTON);
	 //Should Nav to ILP page
	 _assertVisible($CHECKOUT_LOGIN_PAGE);
	 _assertVisible($CHECKOUT_LOGINPAGE_CHECKOUTASGUEST_BUTTON);
	 //nav to Home page
	 _click($LOGO_LINK);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("289906","Verify the application behavior when user click on Checkout with Paypal button in the cart page as a guest user.");
$t.start();
try
{
	//Navigating to the cart
	 _click($MINICART_LINK);  	
	//click on checkout button
	_click($EXPRESS_PAYPAL_TOP);
	//verification
	_assertVisible($PAYPAL_LOGIN_SECTION);
	//cancel
	_click($EXPRESS_PAYPAL_TOP);
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

ClearCartItems();
var $t=_testcase("287806","Verify the display of success message at line item after adding item to wishlist, in the application as a Guest user");
$t.start();
try
{
	//Adding Another Item to the cart
	addToCartSearch($Item[0][0],$Item[0][1]);
	//Navigate to cart
	 _click($MINICART_LINK);  	
	//Click on add to wish list in cart page
	_click($CART_ADDTO_WISHLIST_LINK);
	//verifying the navigation to wishlist Login page
	_assertVisible($WISHLIST_RETURNINGCUSTOMER_SECTION);
	_assertVisible($WISHLIST_ENTIRE_LOGIN_SECTION);
	//login with valid credentials
	 login();
	//Verifying Success message
 	_assertVisible($CART_WISHLIST_SUCCESS_MESSAGE);
 	_assertEqual("This item has been added to your Wish List.", _getText($CART_WISHLIST_SUCCESS_MESSAGE));
 	_click(_link("user-account"));
 	_click(_link("user-wishlist"));
 	//Verifying respective product in wish list
 	_assertEqual($CProductName,$WISHLIST_PRODUCTNAME);
 	
 	//removing the wish list
 	clearWishList();
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

ClearCartItems();
var $t = _testcase("286269","Verify if more than one and less than four products in Mini Cart and Cart are same as a guest User.");
$t.start();
try
{
	 for(var $i=0;$i<$Item.length;$i++)
		 {
			//Adding Item to the cart
			 navigateToCartSearch($Item[$i][0],$Item[0][1]);	
			 var $Product= new Array();
			 //storing product details			
			 var $ProductName=_getText($CART_NAME,_in(_row("cart-row["+$i+"]")));
			 $Product.push($ProductName);
			 if($colorBoolean==true)
				{
					var $ColorVariance=_getText($CART_COLOR_VARIANCE,_in(_row("cart-row["+$i+"]")));
					$Product.push($ColorVariance);
				}
			if($sizeBoolean== true)
				{
					var $SizeVariance=_getText($CART_SIZE_VARIANCE,_in(_row("cart-row["+$i+"]")));
					$Product.push($SizeVariance);
				}
			if($widthBoolean==true)
				{		
					var $WidthVariance=_getText($CART_WIDTH_VARIANCE,_in(_row("cart-row["+$i+"]")));
					$Product.push($WidthVariance);
				}
			var $Quantity=_getText($CART_QTY_DROPDOWN,_in(_row("cart-row["+$i+"]")));
			$Product.push($Quantity);
			var $BasicPrice=_extract(_getText($CART_SALES_PRICE,_in(_row("cart-row["+$i+"]"))),"/[$](.*)/",true).toString();
			$Product.push($BasicPrice);
			
			if($i==0)
				{
					var $Product1= new Array();
					var $Product1=$Product;
				}
			if($i==1)
				{
					var $Product2= new Array();
					var $Product2=$Product;
				}
			if($i==2)
				{
					var $Product3= new Array();
					var $Product3=$Product;
				}
			if($i==3)
				{
					var $Product4= new Array();
					var $Product4=$Product;
				}
		 }
	 var $TotCartItems=_count("_div","/product-list-item/",_in(_table("cart-table")));
	 //fetching the product attributes in mini cart
	 _mouseOver(_link("mini-cart-link"));
	 _click(_span("mini-cart-toggle fa fa-angle-down"));
	 for(var $i=0;$i<$TotCartItems;$i++)
		 {
		 	var $MiniCartProduct= new Array();
		 	_mouseOver(_link("mini-cart-link"));
		 	//click on toggle
		 	_click(_span("mini-cart-toggle fa fa-angle-right["+$i+"]"));
		 	//storing product details			
			 var $MiniCartProductName=_getText($MINICART_PRODUCT_NAME,_in(_div("mini-cart-product collapsed["+$i+"]")));
			 $Product.push($MiniCartProductName);
			 if($colorBoolean==true)
				{
					var $ColorVarianceMiniCart=_getText($MINICART_COLOR_VARIANCE,_in(_div("mini-cart-product collapsed["+$i+"]")));
					$Product.push($ColorVarianceMiniCart);
				}
			if($sizeBoolean== true)
				{
					var $SizeVarianceMiniCart=_getText($MINICART_SIZE_VARIANCE,_in(_div("mini-cart-product collapsed["+$i+"]")));
					$Product.push($SizeVarianceMiniCart);
				}
			if($widthBoolean==true)
				{		
					var $WidthVarianceMiniCart=_getText($MINICART_WIDTH_VARIANCE,_in(_div("mini-cart-product collapsed["+$i+"]")));
					$Product.push($WidthVarianceMiniCart);
				}
			var $QuantityMiniCart=_getText($MINICART_PRODUCT_QUANTITY,_in(_div("mini-cart-product collapsed["+$i+"]")));
			$Product.push($QuantityMiniCart);
			var $BasicPriceMiniCart=_extract(_getText( $MINICART_PRODUCT_PRICE,_in(_div("mini-cart-product collapsed["+$i+"]"))),"/[$](.*)/",true).toString();
			$Product.push($BasicPriceMiniCart);
		 	
		 	if($i==0)
				{
					var $MiniCartProduct4= new Array();
					var $MiniCartProduct4=$MiniCartProduct;
				}
			if($i==1)
				{
					var $MiniCartProduct3= new Array();
					var $MiniCartProduct3=$MiniCartProduct;
				}
			if($i==2)
				{
					var $MiniCartProduct2= new Array();
					var $MiniCartProduct2=$MiniCartProduct;
				}
			if($i==3)
				{
					var $MiniCartProduct1= new Array();
					var $MiniCartProduct1=$MiniCartProduct;
				}
		 }
	 //Product details Verification
	 _assertEqualArrays($Product1,$MiniCartProduct4);
	 _assertEqualArrays($Product2,$MiniCartProduct3);
	 _assertEqualArrays($Product3,$MiniCartProduct2);
	 _assertEqualArrays($Product4,$MiniCartProduct1);
}
catch($e)
{
	  _logExceptionAsFailure($e);
}
	
$t.end();

ClearCartItems();

/*var $t = _testcase(" ", " ");
$t.start();
try
{
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/
cleanup();
