_include("../../GenericLibrary/GlobalFunctions.js");
_resource("Cart.xls");

SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);

var $t = _testcase("352680/352681/352682/352677/352674/352738/352736/352740/352733","Verify the functionality of gift wrap when user goes back to PDP from cart page and adds another item which already has gift wrap selected both as guest and registered user");
$t.start();
try
{
	
//Add a product which is Not applicable for gift wrapping 
for (var $i=0;$i<2;$i++)
{
	navigateToCartSearch($giftWrap[0][2],$giftWrap[0][1]);
}
//quantity textbox
_assertEqual("2",_getText($CART_QTY_DROPDOWN));
//Gift wrap un available function
_assertEqual("Gift wrap not available", _getText(_span("not-available")));
_assertNotVisible($GIFTWRAP_PRICE);
//clear cart
ClearCartItems();
//Add a product which is applicable for gift wrapping
navigateToCartSearch($giftWrap[0][0],$giftWrap[0][1]);
_assertEqual("1",_getText($CART_QTY_DROPDOWN));
//checking UI elements 
_assertVisible($GIFTWRAP_CHECKBOX);
_assertEqual(false,$GIFTWRAP_CHECKBOX.checked);
_assertNotVisible($GIFTWRAP_PRICE);
_assertVisible($GIFTWRAP_LABEL_NO);
_check($GIFTWRAP_CHECKBOX);
_assertVisible($GIFTWRAP_LABEL_YES);
_assertVisible($GIFTWRAP_PRICE);
//Click on gift wrap product name in cart
var $productNameinCart=_getText(_link("/(.*)/", _in(_div("name"))));
//click on product name in cart
_click(_link("/(.*)/", _in(_div("name"))));
//Should Navigate to PDP
_assertVisible(_heading1("product-name"));
var $productNameinPDP=_getText(_heading1("product-name"));
_assertEqual($productNameinCart,$productNameinPDP);
//click on add to cart 
_click(_submit("/add-to-cart/"));
//navigate to Cart page
_click($MINICART_VIEWCART_LINK);
//Click on gift wrap product name in cart
var $productNameinCart=_getText(_link("/(.*)/", _in(_div("name[1]"))));
//gift wrap item should display in seperate line item
_assertEqual($productNameinPDP,$productNameinCart);
_assertEqual("1",_getText(_numberbox("/input-text/", _in(_cell("item-quantity[2]")))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("352682/352678/352741","Verify the functionality of Bag Total  Gift Wrap in bag page both as an anonymous and a registered user.");
$t.start();
try
{
	
//Check all the gift wrap check boxes
var $checkBoxes=_count("_checkbox","/input-checkbox/",_in(_table("cart-table")));

var $Actprice=new Array();
var $giftprice=new Array();
var $TotalActualPrice=0;
var $TotalGiftPrice=0;
var $expectedOrderTotal;
var $price;
var $gprice;

for (var $i=0;$i<$checkBoxes;$i++)
{

//click on all the check boxs	
var $k=_collectAttributes("_checkbox","/Gift Wrap/","id",_in(_table("cart-table")));
_check(_checkbox($k[$i]));
_wait(5000);
//_click(_checkbox("Gift Wrap: No["+$i+"]"));
//Fetching prices
$price=parseFloat(_extract(_getText(_span("price-sales["+$i+"]")),"/[$](.*)/",true));
$Actprice.push($price);
//gift wrap price
$gprice=parseFloat(_extract(_getText(_span("value", _in(_div("price-option["+$i+"]")))),"/[$](.*)/",true));
$giftprice.push($gprice);
//Counting all the prices
$TotalActualPrice=parseFloat($TotalActualPrice+$Actprice[$i]);
$TotalGiftPrice=parseFloat($TotalGiftPrice+$giftprice[$i]);

}


$expectedOrderTotal=parseFloat($TotalActualPrice+$TotalGiftPrice);
_log("Expected order total"+$expectedOrderTotal);

//comparing prices
var $orderSubtotal=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
var $giftWrapprice=parseFloat(_extract(_getText(_row("order-giftwrap-total")),"/[$](.*)/",true));
var $OrdersubtotalPrice=parseFloat($orderSubtotal+$giftWrapprice);
_assertEqual($expectedOrderTotal,$OrdersubtotalPrice);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("352683","Verify the functionality of Bag - Total Price in bag page both as an anonymous and a registered user.");
$t.start();
try
{
	
//fetching the prices for shipping tax and sales tax
var $shippingPrice=parseFloat(_extract(_getText(_row("order-shipping")),"/[$](.*)/",true));
//sales tax
var $Salestax=0;
if (!_isVisible(_cell("TBD", _in(_row("order-sales-tax")))))
{
	$Salestax=parseFloat(_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true));
}
//adding total
var $ExpectedOrderTotal=parseFloat($orderSubtotal+$giftWrapprice+$shippingPrice+$Salestax);
var $ActualTotal=_extract(_getText(_cell("order-value")),"/[$](.*)/",true);
//comparing price
_assertEqual($ExpectedOrderTotal,$ActualTotal);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("352684/352685/352676/352743/352744","Verify the UI of  Gift Wrap Modal Window in bag page both as an anonymous and a registered user/Verify the functionality of Gift Wrap Modal Window in bag page both as an anonymous and a registered user." +
		"Verify the functionality of Gift Wrap Further Details in bag page both as an anonymous and a registered user.");
$t.start();
try
{
	
//click on gift wrap window
_click($GIFTWRAP_DIALOGUE_LINK);
//Dialogue should be visible
_assertVisible($GIFTWRAP_DIALOGUE);
_assertVisible($GIFTWRAP_HEADING_IN_DIALOGUE);
_assertVisible($GIFTWRAP_DIALOGUE_IMAGE);
_assertVisible($GIFTWRAP_DIALOGUE_CLOSE_BUTTON);
_assertVisible($GIFTWRAP_DIALOGUE_TEXT);
//close the popup
_click($GIFTWRAP_DIALOGUE_CLOSE_BUTTON);
_assertNotVisible($GIFTWRAP_DIALOGUE);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("352688/352689/352690/352737","Verify the functionality of Gift Wrap -Order Summary in shipping page both as an anonymous and a registered user." +
		"Verify the functionality of Checkout Total - Gift Wrap in shipping page both as an anonymous and a registered user.");
$t.start();
try
{
	
//click on checkout button
_click(_submit("dwfrm_cart_checkoutCart"));
//click on login as guest
_click(_submit("dwfrm_login_unregistered"));
//gift wrap amount on order summary page
var $giftwrap_in_orderSummary_product1=parseFloat(_extract(_getText(_div("/Gift Wrap/",_near(_span("mini-cart-toggle fa fa-angle-right[1]")))),"/[$](.*)/",true));
_log("Gift Wrap Price1="+$giftwrap_in_orderSummary_product1);
var $giftwrap_in_orderSummary_product2=parseFloat(_extract(_getText(_div("/Gift Wrap/",_near(_span("mini-cart-toggle fa fa-angle-right[2]")))),"/[$](.*)/",true));
_log("Gift Wrap Price2="+$giftwrap_in_orderSummary_product2);

//comparing indivudial prices
_assertEqual($giftprice[0],$giftwrap_in_orderSummary_product1);
_assertEqual($giftprice[1],$giftwrap_in_orderSummary_product2);

//product price in order summary page
var $ProductPrice1=parseFloat(_extract(_getText(_div("/mini-cart-pricing/",_near(_span("mini-cart-toggle fa fa-angle-right[1]")))),"/[$](.*)/",true));
_log("Price Price1="+$ProductPrice1);
var $ProductPrice2=parseFloat(_extract(_getText(_div("/mini-cart-pricing/",_near(_span("mini-cart-toggle fa fa-angle-right[2]")))),"/[$](.*)/",true));
_log("Price Price2="+$ProductPrice2);

//Verifying price in indivudial session
var $totalpriceInOcp=parseFloat($giftwrap_in_orderSummary_product1+$giftwrap_in_orderSummary_product2+$ProductPrice1+$ProductPrice2);
_log("$totalpriceInOcp ="+$totalpriceInOcp);
//Price in price session
var $subTotalInocp=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
var $giftWrapTotalInocp=parseFloat(_extract(_getText(_row("order-giftwrap-total")),"/[$](.*)/",true));
var $CombineProductandGiftPrice=parseFloat($subTotalInocp+$giftWrapTotalInocp);
//Comparing prices
_assertEqual($totalpriceInOcp,$CombineProductandGiftPrice);


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//clear cart items
ClearCartItems();

var $t = _testcase("352679/352739/352730","Verify the functionality of QTY Field in bag page both as an anonymous and a registered user./Verify the functionality of Gift Wrap in mini bag flyout as an guest and registered user.");
$t.start();
try
{
	
//Add a product which is applicable for gift wrapping
navigateToCartSearch($giftWrap[0][0],$giftWrap[0][1]);
//Gift wrap Should not display in mini cart
_mouseOver($MINICART_LINK);
_assertNotVisible(_div("mini-cart-gift options"));
//check Gift wrap checkbox
_check($GIFTWRAP_CHECKBOX);
//Gift wrap Should not display in mini cart
_mouseOver($MINICART_LINK);
_assertVisible(_div("mini-cart-gift options"));
//Gift wrap price
var $gprice=parseFloat(_extract(_getText(_span("value", _in(_div("price-option")))),"/[$](.*)/",true));
//update the quantity and check the price
_setValue($CART_QTY_DROPDOWN, "2");
//product quantity update button
_click($CART_UPDATE_BUTTON);
var $updatedgprice=parseFloat(_extract(_getText(_span("value", _in(_div("price-option")))),"/[$](.*)/",true));
_assertEqual(($gprice)*2,$updatedgprice);
//Normal price
var $updatedPrice=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true);
_assertEqual(($CItemBasicPrice)*2,$updatedPrice);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*var $t = _testcase(" ", " ");
$t.start();
try
{
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/