_include("../util.GenericLibrary/BM_Configuration.js");
_resource("ExcelData_Cart.xls");

var $ProdId=_readExcelFile("ExcelData_Cart.xls","InventoryProducts");
var $subcat=_readExcelFile("ExcelData_Cart.xls","Search");	
var $cart_data=_readExcelFile("ExcelData_Cart.xls","Quantity");
var $coupon_data=_readExcelFile("ExcelData_Cart.xls","Coupon");
var $color=_readExcelFile("ExcelData_Cart.xls","Style");
var $Generic=_readExcelFile("ExcelData_Cart.xls","Generic");

SiteURLs();
cleanup();
_setAccessorIgnoreCase(true); 


var $t = _testcase("125281","Verify the display of the Inventory status for line item.");
$t.start();
try
{
	
	for(var $i=0; $i<$ProdId.length; $i++)
	{
		search($ProdId[$i][1]);
		if($i==1)
			{
			//For out of stock status message
			_assertEqual($ProdId[$i][3], _getText(_div("availability-msg")));
			//Add to cart button should be disabled
			_assertEqual(true,_getAttribute(_button("add-to-cart"),"disabled"))
			}
		else
			{
			_click(_submit("add-to-cart"));
			_click(_link("mini-cart-link"));  
			//Verifying Availabity Status messages
			_assertEqual("/"+$ProdId[$i][3]+"/",_getText(_list("product-availability-list", _in(_row("cart-row")))));
			}
		ClearCartItems();
	}
	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

ClearCartItems();

var $t=_testcase("125286","Verify the details displayed in the Cart Summary section of cart page,  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	//Searching & adding respective product to the cart to apply the coupon
	search($coupon_data[5][0]);
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	 _click(_link("mini-cart-link"));  		
	//Applying Order_level Coupon code 
	_setValue(_textbox("dwfrm_cart_couponCode"),$coupon_data[5][1]);
	_click(_submit("add-coupon"));		
	//Order Subtotal
	_assertVisible(_row("order-subtotal"));
	//Order discount
	_assertVisible(_row("order-discount discount"));
	//Shipping
	_assertVisible(_row("order-shipping"));
	//Sales Tax
	_assertVisible(_row("order-sales-tax"));
	//Calculating Estimated Total = Subtotal + tax + shipping Discount or order Discount.
	CalculateEstiTotal(0,10);
	ClearCartItems();
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

ClearCartItems();


var $t = _testcase("125269","Verify whether sub total / Order Total/ Discount/ Shipping gets updated after applying the coupon code.");
$t.start();
try
{
	search($coupon_data[5][0]);
	//select swatches
	selectSwatch();
	//set the quantity
	_setValue(_textbox("Quantity"),1);
	//Click on add to cart button
	_click(_submit("add-to-cart"));
	_click(_link("mini-cart-link"));  
	//Applying Coupon
	_setValue(_textbox("dwfrm_cart_couponCode"),$coupon_data[5][1]);
	_click(_submit("add-coupon"));
	//Coupon should be applied and Order sub total and estimated sub total should be updated
	_assertVisible(_span("Applied", _in(_row("rowcoupons"))));
	//Function to calculate Estimated Total (10 is the Coupon discount)
	CalculateEstiTotal(0,10);//Referring function from "Total_Calculate.js" 
	//Removing coupon
	_click(_submit("Remove", _in(_row("rowcoupons"))));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}		
$t.end();     

ClearCartItems();

var $t = _testcase("125267/125268/125270/125274/125271/352797","Verify whether user can apply the valid coupon code in the cart page and Verify the display of elements in the line item after applying valid coupon code and Verify whether user can apply the invalid/Case sensitivity/Redeem/Expired coupon code in the cart page");
$t.start();
try
{
	for(var $i=0; $i<$coupon_data.length; $i++)
		{
		//Searching & adding respective product to the cart to apply the coupon
		search($coupon_data[$i][0]);
		_click(_submit("add-to-cart"));
		//Navigating to the cart
		_click(_link("mini-cart-link"));  		
		//Applying Coupon code
		_setValue(_textbox("dwfrm_cart_couponCode"),$coupon_data[$i][1]);
		_click(_submit("add-coupon"));
		//After applying coupon code its taking delay to display error message thats why adding delay
		_wait(3000);
		//Verifying When valid coupon applied to the cart		
		if($i==0 || $i==5)
			{
			//Verifying coupon row is visible or not
			_assertVisible(_row("rowcoupons"));
			//Verifying the coupon code is expected or not
			_assertEqual($coupon_data[$i][1],_getText(_span("value", _in(_div("cartcoupon clearfix")))));
			//Verifying "Applied" message
			_assertEqual($coupon_data[$i][2], _getText(_span("bonus-item", _near(_div("cartcoupon clearfix")))));
			//parameter 10 is Discount for product coupon 
			//Verifying order Total by calculating each line item total
			orderSummaryValidation(10);//Referring function from "Total_Calculate.js"
			//Calculating Estimated Total 10 is the order discount
			CalculateEstiTotal(0,10);//Referring function from "Total_Calculate.js"
			}		
		
		else
			{		
			//Verifying error messages
			_assertEqual($coupon_data[$i][2],_getText(_div("error", _in(_div("cart-coupon-code"))))); 
			//Verifying font color of error message
			_assertEqual($color[0][0],_style(_div("error", _in(_div("cart-coupon-code"))),"color"));  
			//Verifying order Total by calculating each line item total
			orderSummaryValidation(0);//Referring function from "Total_Calculate.js"
			//Calculating Estimated Total 
			CalculateEstiTotal(0,0);//Referring function from "Total_Calculate.js"	
			}
		
		
		ClearCartItems();
		}	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("125266/145063","Verify whether on click of  'Checkout' button, updates quantity and promotion and navigates to shipping page  in the application");
$t.start();
try
{
	//Searching & adding respective product to the cart to apply the coupon
	search($coupon_data[0][0]);
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	_click(_link("mini-cart-link"));  
	
	//Applying Coupon code
	_setValue(_textbox("dwfrm_cart_couponCode"),$coupon_data[0][1]);
	_click(_submit("add-coupon"));
	//Click on checkout button
	_click(_submit("dwfrm_cart_checkoutCart"));
	
	//Checking the presence of login button
	_click(_link("user-account"));
	if(_isVisible(_link("Login")))
		{
		_click(_submit("dwfrm_login_unregistered"));
		}
	
	//Should Navigated to the shipping page
	if($CheckoutType==$BMConfig[0][1])
		{
			//Should Nav to Shipping page
			_assertVisible(_heading2($Generic[3][1]));
			_assertVisible(_div("checkout-tabs spc-shipping"));
		}
	else if($CheckoutType==$BMConfig[1][1])
		{
			//verify the navigation
			_assertVisible(_fieldset("/"+$Generic[5][1]+" /"));
			_assertVisible(_submit("dwfrm_singleshipping_shippingAddress_save"));
		}	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

ClearCartItems();

var $t = _testcase("125273","Verify whether user can remove a applied coupon code in the cart page.");
$t.start();
try
{
	//Searching & adding respective product to the cart to apply the coupon
	search($coupon_data[0][0]);
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	_click(_link("mini-cart-link"));  	
	//Applying Coupon code
	_setValue(_textbox("dwfrm_cart_couponCode"),$coupon_data[0][1]);
	_click(_submit("add-coupon"));
	//Click on Remove in Coupon row
	_click(_submit("dwfrm_cart_coupons_i0_deleteCoupon"));
	//Coupon row Should not visible
	_assertNotVisible(_row("rowcoupons"));
	_assertNotVisible(_span("bonus-item"));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();
	
ClearCartItems();

var $t=_testcase("125275","Verify by providing valid coupon code and hit enter key.");
$t.start();
try{
	//Searching & adding respective product to the cart to apply the coupon

	search($coupon_data[0][0]);
	_click(_submit("add-to-cart"));
	//Navigating to the cart
	_click(_link("mini-cart-link"));  
	//Applying Coupon code
	_setValue(_textbox("dwfrm_cart_couponCode"),$coupon_data[0][1]);
	_focusWindow();
	//Statement to hit on enter key
	_focus(_textbox("dwfrm_cart_couponCode"));
	//_keyPress(document.body, [13,13]);
	 _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	//Verifying coupon row is visible or not
	_assertVisible(_row("rowcoupons"));
	//Verifying the coupon code is expected or not
	_assertEqual($coupon_data[0][1],_getText(_span("value", _in(_div("cartcoupon clearfix")))));
	//Verifying "Applied" message
	_assertEqual($coupon_data[0][2], _getText(_span("bonus-item", _near(_div("cartcoupon clearfix")))));

	ClearCartItems();
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148619/148621","Verify the UI of 'Promo code'  and total section in Cart in the application both as an anonymous and a registered user and Verify the display of promotions in cart page  in the application both as an anonymous and a registered user");
$t.start();
try
{	
	//Searching & adding respective product to the cart to apply the coupon
	search($coupon_data[0][0]);
	_click(_submit("add-to-cart"));	
	//Navigating to the cart
	_click(_link("mini-cart-link"));  	
	//UI Coupon Code Section
	_assertVisible(_div("cart-coupon-code"));
	//_assertVisible(_label("ENTER COUPON CODE"));  
	//Place holder
	_assertEqual("Enter Coupon Code",_getAttribute(_textbox("dwfrm_cart_couponCode"), "placeholder"));
	
	_assertVisible(_textbox("dwfrm_cart_couponCode"));
	_assertVisible(_submit("add-coupon"));
	//UI of Total  Section
	_assertVisible(_cell("Subtotal"));
	_assertVisible(_row("order-subtotal"));
	_assertVisible(_cell("/Shipping/"));
	_assertVisible(_row("order-shipping"));
	_assertVisible(_cell("Sales Tax"));
	_assertVisible(_row("order-sales-tax"));
	_assertVisible(_cell("Estimated Total"));
	_assertVisible(_row("order-total"));
	//Applying Coupon code
	_setValue(_textbox("dwfrm_cart_couponCode"),$coupon_data[0][1]);
	_click(_submit("add-coupon"));
	//Verifying in cart row
	//Promo name
	_assertVisible(_div("promo-adjustment"));
	//List price
	_assertVisible(_span("price-unadjusted"));
	//Sale Price
	_assertVisible(_span("price-adjusted-total"));
	//Verifying coupon row is visible or not
	_assertVisible(_row("rowcoupons"));
	//Verifying the coupon code is expected or not
	_assertEqual($coupon_data[0][1],_getText(_span("value", _in(_div("cartcoupon clearfix")))));
	//Verifying "Applied" message
	_assertEqual($coupon_data[0][2], _getText(_span("bonus-item", _near(_div("cartcoupon clearfix")))));
	//parameter 10 is Discount for product coupon 
	//Verifying order Total by calculating each line item total
	orderSummaryValidation(10);//Referring function from "Total_Calculate.js"
	//Calculating Estimated Total 10 is the order discount
	CalculateEstiTotal(0,0);//Referring function from "Total_Calculate.js"		
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

 	
var $t=_testcase("125279","Verify user can update the quantity of each line item in the application both as an anonymous and a registered user");
$t.start();
try
{
	var $ValidQty=4;
	_click(_link("mini-cart-link"));  
	//verifying by entering valid Quantity
	_setValue(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity"),$ValidQty);
	//Click on enter key from keyboard
	//Statements to hit on enter key
	_focusWindow();
	_focus(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity"));
	_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	//Verifying Qty
	_assertEqual($ValidQty,_getText(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity")));	
	//verifying by entering 0 Quantity
	var $prodID=_getText(_span("value", _in(_div("sku",_in(_row("cart-row"))))));
	_setValue(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity", _in(_row("cart-row"))),0);
	//Statements to hit on enter key
	_focusWindow();
	_focus(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity",_in(_row("cart-row"))));
	_typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	_wait(2000);
	//Product Should not present
	_assertNotVisible(_span($prodID));	
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();

cleanup();
