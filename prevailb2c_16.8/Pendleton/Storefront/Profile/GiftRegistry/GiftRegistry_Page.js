_include("../../../GenericLibrary/GlobalFunctions.js");

var $color=$Generic_GiftRegistry[0][6];
var $color1=$Generic_GiftRegistry[1][6];
//from util.GenericLibrary/BrowserSpecific.js
var $FirstName1=$FName;
var $LastName1=$LName;
var $UserEmail=$uId;


SiteURLs();
cleanup();
_setAccessorIgnoreCase(true); 
//Login
_click($HEADER_USERACCOUNT_LINK);
_click($HEADER_LOGIN_LINK);
login();
//Deleting addresses, which intern will delete wishlist and gift registry
deleteAddress();


var $t = _testcase("288188/288216", "Verify the navigation related to left nav links on Gift Registry page in application as a Registered user");
$t.start();
try
{
	//create a gift registry with no added items
	_click($FOOTER_GIFTREGISTRY_LINK);
	
	//left nav links	
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
	{
         //Navigate to Gift Registry page
		_click($FOOTER_GIFTREGISTRY_LINK);
		
		//click on left nav links 
		_click($LeftnavlinkNames[$i]);
		
		var $Expected_LeftnavHeading = _heading1("/"+$LeftNav[$i][4]+"/");
		
		//verify the navigation
		_assertVisible($Expected_LeftnavHeading);
		_assertContainsText($LeftNav[$i][3], $PAGE_BREADCRUMB);
	}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//create a gift registry with no added items
_click($HEADER_USERACCOUNT_LINK);
_click($FOOTER_GIFTREGISTRY_LINK);
//click on new registry
_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
//creating the registry
createRegistry($GiftRegistry, 0);
//Pre/Post Event shipping info
Pre_PostEventInfo($GiftRegistry, 4);
//Submit button
_click($GIFTREGISTRY_EVENTINFORMATION_CONTINUE_BUTTON);

//**
var $t = _testcase("124348", "Verify the UI of Gift registry page of the application as a registered user having saved registry with no added items");
$t.start();
try
{
	
	//Validating Navigation
	_assertVisible($CREATEGIFTREGISTRY_HEADING);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
	//Tabs
	_assertVisible($GIFTREGISTRY_CONTENT_TABS);
	//registry
	_assertVisible($GIFTREGISTRY_REGISTER_TAB);
	_assertEqual($Generic_GiftRegistry[1][10], _getText($GIFTREGISTRY_REGISTER_TAB));
	//event info
	_assertVisible($GIFTREGISTRY_EVENT_TAB);
	_assertEqual($Generic_GiftRegistry[2][10], _getText($GIFTREGISTRY_EVENT_TAB));
	//shipping info
	_assertVisible($GIFTREGISTRY_SHIPPINGINFO_TAB);
	_assertEqual($Generic_GiftRegistry[3][10], _getText($GIFTREGISTRY_SHIPPINGINFO_TAB));
	//purchases
	_assertVisible($GIFTREGISTRY_PURCHASES_TAB);
	_assertEqual($Generic_GiftRegistry[4][10], _getText($GIFTREGISTRY_PURCHASES_TAB));
	//gift registry details
	_assertVisible($GIFTREGISTRY_PAGE_CONTENT_TAB);
	_assertVisible(_heading2($GiftRegistry[0][1]+" - "+$GiftRegistry[0][16]));
	_assertVisible($GIFTREGISTRY_NOITEMSIN_GF_TEXT);
	//add items link
	_assertVisible($GIFTREGISTRY_ADDITEMS_LINK);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//**
var $t = _testcase("124347/288215", "Verify the application behavior on click of links in breadcrumb of create gift registry page in application as an registered user");
$t.start();
try
{
	
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));

	
	//verify the navigation to home page
	_click($HEADER_HOME_LINK);
	
	_assertVisible($HOMEPAGE_SLIDES);
	_assertVisible($HOMEPAGE_SLOTS);
	
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_GIFTREGISTRY_LINK);
	//click on new registry
	_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
	_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
	
	//Navigation related to My account
	_click($MY_ACCOUNT_LINK_BREADCRUMB);
	//verify the navigation
	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
	_assertVisible($MY_ACCOUNT_HEADING);
	_assertVisible($MY_ACCOUNT_OPTIONS);


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//**
var $t = _testcase("124323/289812/124356", "Verify the functionality related to 'View' link in giftregistery page  in application as a  Registered user./Verify the application behavior on click of view link for a gift registry in gift registry search results page as a guest or registered user/functionality related to 'Add Items' link on Gift registry details page of the application as a registered user");
$t.start();
try
{
	//verify the navigation to home page
	_click($HEADER_HOME_LINK);
	_click($FOOTER_GIFTREGISTRY_LINK);
	//Click on view 
	_click($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));
	//HEADING
	_assertVisible($CREATEGIFTREGISTRY_HEADING);
	_assertVisible($GIFTREGISTRY_ADDGIFTCERTIFICATE);
	//CONTENT
	_assertVisible($GIFTREGISTRY_PAGE_CONTENT_TAB);
	
	//adding items to gift registry
	addItems($Generic_GiftRegistry[0][11]);
	
	//verify
	_assertVisible($GIFTREGISTRY_ITEMLIST);
	_assertEqual($ProductName, _getText($GIFTREGISTRY_PRODUCT_NAME));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("128722", "Verify the application behavior on click of product name in gift registry details page of the application");
$t.start();
try
{
	
	var $productName=_getText($GIFTREGISTRY_PRODUCT_NAME);
	//click on name link
	_click($GIFTREGISTRY_PRODUCT_NAME);
	//verify the navigation
	_assertVisible(_span($productName, _in($PAGE_BREADCRUMB)));
	_assertEqual($productName, _getText($PDP_PRODUCTNAME));
	//navigating to wish list login page
	_click($HEADER_GIFTREGISTRY_LINK);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



//**
var $t = _testcase("124324", "Verify the UI of Gift Registry details page of the application as a registered user having a saved gift registry");
$t.start();
try
{
	//Click on view 
	_click($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
	
	//Validating Navigation
	_assertVisible($CREATEGIFTREGISTRY_HEADING);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));

	//Tabs
	_assertVisible($GIFTREGISTRY_CONTENT_TABS);
	if(_isVisible($GIFTREGISTRY_MAKETHISPUBLIC_BUTTON))
		{
			_click($GIFTREGISTRY_MAKETHISPUBLIC_BUTTON);	
		}
	//make this registry private
	_assertVisible($GIFTREGISTRY_MAKETHISPRIVATE_BUTTON);
	//verifying url
	if (!_isVisible($GIFTREGISTRY_MAKETHISPUBLIC_BUTTON))
		{
		_click($GIFTREGISTRY_SHARELINK);
		_assertVisible($GIFTREGISTRY_URL_LINK);
		_assertVisible($GIFTREGISTRY_URL_SECTION);
		}
	else
		{
			_click($GIFTREGISTRY_MAKETHISPUBLIC_BUTTON);
			_click($GIFTREGISTRY_SHARELINK);
			_assertVisible($GIFTREGISTRY_URL_LINK);
			_assertVisible($GIFTREGISTRY_URL_SECTION);
		}
	//heading
	_assertVisible(_heading2($GiftRegistry[0][1]+" - "+$GiftRegistry[0][16]));
	//Item image
	_assertVisible($GIFTREGISTRY_PRODUCT_IMAGE);
	//Item name
	_assertVisible($GIFTREGISTRY_PRODUCT_NAME);
	//edit details link
	_assertVisible($GIFTREGISTRY_PRODUCT_EDIT_DETAILS);
	//product details
	_assertVisible($GIFTREGISTRY_PRODUCT_ITEMDETAILS);
	//date added
	_assertVisible($GIFTREGISTRY_PRODUCT_DATEADDED_SECTION);
	_assertVisible($GIFTREGISTRY_DATEADDED_LABLE);
	//make this item public check box
	_assertVisible($GIFTREGISTRY_MAKETHISITEMPUBLIC_CHECKBOX);
	_assert($GIFTREGISTRY_MAKETHISITEMPUBLIC_CHECKBOX.checked);
	//update
	_assertVisible($GIFTREGISTRY_PRODUCTUPDATE_LINK);
	//remove
	_assertVisible($GIFTREGISTRY_PRODUCTREMOVE_LINK);
	//quantity text box
	_assertVisible($GIFTREGISTRY_PRODUCT_QUANTITYBOX);
	//add to cart
	_assertVisible($GIFTREGISTRY_ADDTOCART_BUTTON);	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//**
var $t = _testcase("124423/289656/148741/148773", "Verify the Navigation related to  'Edit details' link present  on Gift registery page, /application behavior when user edits product variances in gift registry details page/application behavior when user updates product quantity in gift registry details page of the application as a registered user");
$t.start();
try
{
	_assertVisible($GIFTREGISTRY_PRODUCT_EDIT_DETAILS);
	var $productName=_getText($GIFTREGISTRY_PRODUCT_NAME);
	var $productPrice=_getText($GIFTREGISTRY_PRODUCT_PRICE);
	var $productQty=_getText($GIFTREGISTRY_PRODUCT_QUANTITYBOX)
	
	_click($GIFTREGISTRY_PRODUCT_EDIT_DETAILS);
	//validating navigation
	_assertEqual($productName, _getText($GIFTREGISTRY_PRODUCTNAME_EDITDETAILS_OVERLAY));
	_assertEqual($productPrice, _getText($GIFTREGISTRY_PRODUCTPRICE_EDITDETAILS_OVERLAY));
	_assertEqual($productQty, _getText($GIFTREGISTRY_PRODUCTQTY_EDITDETAILS_OVERLAY));
	
	//updating the quality variance in quickview overlay
	_setValue($GIFTREGISTRY_PRODUCTQTY_EDITDETAILS_OVERLAY, $Generic_GiftRegistry[0][14]);
	_click($GIFTREGISTRY_EDITDETAILS_OVERLAY_SUBMITBUTTON);
	
	//verify
	_assertEqual($Generic_GiftRegistry[0][14], _getText($GIFTREGISTRY_PRODUCT_QUANTITYBOX));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//**
var $t = _testcase("124424", "Verify the functionality related to update link present on Gift registry details page as a  Registered user having saved gift registry");
$t.start();
try
{
	_click($FOOTER_GIFTREGISTRY_LINK);
	//Click on view 
	_click($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
	_setValue($GIFTREGISTRY_QUANTITYDESIRED_NUMBERBOX, $Generic_GiftRegistry[0][14]);
	//selectig value
	_setSelected($GIFTREGISTRY_PRIORITY_DROPDOWN,$Generic_GiftRegistry[1][14]);
	_uncheck($GIFTREGISTRY_MAKETHIS_ITEM_PUBLIC_CHECKBOX);
	_click($GIFTREGISTRY_PRODUCTUPDATE_LINK);
	//validating
	_assertEqual($Generic_GiftRegistry[0][14], _getValue($GIFTREGISTRY_QUANTITYDESIRED_NUMBERBOX));
	_assertEqual($Generic_GiftRegistry[0][14], _getValue($GIFTREGISTRY_PRODUCT_QUANTITYBOX));
	_assertEqual($Generic_GiftRegistry[1][14], _getSelectedText($GIFTREGISTRY_PRIORITY_DROPDOWN));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("288217", "Verify the application behavior on click of event info tab on gift registry details page as a registered user having saved registry");
$t.start();
try
{
    //info tab
	_click($GIFTREGISTRY_EVENT_TAB);
	//verify navigation
	_assertVisible($CREATEGIFTREGISTRY_EVENTINFORMATION_HEADING);
	
    //pre-filled
	GiftregistryEventInfo_Prepopulate($GiftRegistry,0);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("288219", "Verify the application behavior on click of shipping info tab on gift registry details page as a registered user having saved registry");
$t.start();
try
{
    //info tab
	_click($GIFTREGISTRY_SHIPPINGINFO_TAB);
	
	//verify navigation
	_assertVisible($GIFTREGISTRY_PREEVENTSHIPPING_HEADING);
	
	//pre-filled
	GiftregistryShippingInfo_Prepopulate($GiftRegistry,4);

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("288221", "Verify the validations related to quantity desired input field in gift registry details page as a registered user having saved registry");
$t.start();
try
{
	_assert(false,"application issue");
	
//	//gift registry footer
//	_click($FOOTER_GIFTREGISTRY_LINK);
//	
//	//view link
//	_click($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
//	
//	//validations for quantity field
//	for(var $i=0;$i<$Giftregistry_QuantityValidations.length;$i++)
//		{
//		_setValue($GIFTREGISTRY_QUANTITYDESIRED_NUMBERBOX,$Giftregistry_QuantityValidations[$i][1]);
//		_click($GIFTREGISTRY_ADDTOCART_BUTTON);
//		
//		if ($i==0 || $i==1 || $i==2)
//			{
////			_assertEqual($qunatityvalidations[0][2],_getText($WISHLIST_QUANTITY_ERRORMESSAGE));
////			_assertEqual($qunatityvalidations[0][3],_style($WISHLIST_QUANTITY_TEXTBOX,"background-color"));
////			_assertNotVisible($MINICART_QUANTITY);
////			_assertEqual("",_getText($WISHLIST_QUANTITY_TEXTBOX))
//			_assert(false,"application issue");
//			}
//		else
//			{
//				_assertEqual($Giftregistry_QuantityValidations[3][1],_getText($MINICART_QUANTITY));
//			}
//		}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("288222", "Verify the validations related to quantity text field in gift registry details page as a registered user having saved registry");
$t.start();
try
{
	_assert(false,"application issue");
	
//	//gift registry footer
//	_click($FOOTER_GIFTREGISTRY_LINK);
//	
//	//view link
//	_click($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
//	
//	//validations for quantity field
//	for(var $i=0;$i<$Giftregistry_QuantityValidations.length;$i++)
//		{
//		_setValue($GIFTREGISTRY_PRODUCT_QUANTITYBOX,$Giftregistry_QuantityValidations[$i][1]);
//		_click($GIFTREGISTRY_ADDTOCART_BUTTON);
//		
//		if ($i==0 || $i==1 || $i==2)
//			{
////			_assertEqual($qunatityvalidations[0][2],_getText($WISHLIST_QUANTITY_ERRORMESSAGE));
////			_assertEqual($qunatityvalidations[0][3],_style($WISHLIST_QUANTITY_TEXTBOX,"background-color"));
////			_assertNotVisible($MINICART_QUANTITY);
////			_assertEqual("",_getText($WISHLIST_QUANTITY_TEXTBOX))
//			
//			}
//		else
//			{
//				_assertEqual($Giftregistry_QuantityValidations[3][1],_getText($MINICART_QUANTITY));
//			}
//		}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("289658","Verify the functionality of make this item pulic checkbox displayed for a prodcut in gift registry details page of the application");
$t.start();
try
{
//click on wish list link from footer
_click($FOOTER_GIFTREGISTRY_LINK);
	
//click on view link in search result page 
_click($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
	
var $productName_before_public=_getText($GIFTREGISTRY_PRODUCT_NAME);	
//un check the check box 
_uncheck($GIFTREGISTRY_MAKETHISITEMPUBLIC_CHECKBOX);
//click on update button
_click($GIFTREGISTRY_PRODUCTUPDATE_LINK);
//logout from the application
logout();
//click on wish list link from footer
_click($FOOTER_GIFTREGISTRY_LINK);

//navigate to search result page
navigateToFindSomeOneGiftRegistry();
var $expText=$LName+" "+$FName;
_assertEqual(true,_isVisible(_row("/"+$expText+"/")));

//click on view link in search result page 
_click($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
//
if (_isVisible($GIFTREGISTRY_PRODUCTUPDATE_LINK))
	{
		var $productName_after_public=_getText($GIFTREGISTRY_PRODUCTUPDATE_LINK);
		//product name should not match
		_assertNotEquals($productName_before_public,$productName_after_public);
	}
else
	{
		_assertVisible($GIFTREGISTRY_ITEMFOUND_MESSAGE);
	}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


//login to application
_click($HEADER_USERACCOUNT_LINK);
_click($HEADER_LOGIN_LINK);
login();
ClearCartItems();


//**
var $t = _testcase("124425", "Verify the functionality related to remove link present on Gift registry details page as a Registered user having saved gift registry");
$t.start();
try
{
	//click on wish list link from footer
	_click($FOOTER_GIFTREGISTRY_LINK);
	_click($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
	
	//remove
	_assertVisible($GIFTREGISTRY_PRODUCTREMOVE_LINK);
	_click($GIFTREGISTRY_PRODUCTREMOVE_LINK);
	//Item name
	_assertNotVisible($GIFTREGISTRY_PRODUCT_NAME);
	//edit details link
	_assertNotVisible($GIFTREGISTRY_PRODUCT_EDIT_DETAILS);
	//product details
	_assertNotVisible($GIFTREGISTRY_PRODUCT_ITEMDETAILS);
	//gift registry details
	_assertVisible($GIFTREGISTRY_PAGE_CONTENT_TAB);
	_assertVisible(_heading2($GiftRegistry[0][1]+" - "+$GiftRegistry[0][16]));
	_assertVisible($GIFTREGISTRY_NOITEMSIN_GF_TEXT);
	//add items link
	_assertVisible($GIFTREGISTRY_ADDITEMS_LINK);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//adding items to gift registry
addItems($Generic_GiftRegistry[0][11]);

//**
var $t = _testcase("124422", "Verify the functionality related to add to cart button in gift registry details page of the application as a registered user having saved gift registry");
$t.start();
try
{
	//Click on add to cart
	_click($GIFTREGISTRY_ADDTOCART_BUTTON);
	//validating
	_assertEqual($ProductName, _getText($MINICART_PRODUCT_NAME));

	_assertEqual($QTY, _extract(_getText($MINICART_PRODUCT_QUANTITY), "/Qty: (.*) [$]/", true).toString());

	_assertEqual($ProductPrice, _extract(_getText($MINICART_SUBTOTALS_SECTION), "/Order Subtotal (.*)/", true).toString());
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



//removing items from cart
ClearCartItems();

_click($FOOTER_GIFTREGISTRY_LINK);
//Click on view 
_click($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
//**
var $t = _testcase("124350", "Verify the functionality related to the ' 'Make This Registry Private' button on Gift registery page  in application as a  Registered user.' ");
$t.start();
try
{
	//click on 'Make This Registry Private'
	_click($GIFTREGISTRY_MAKETHISPRIVATE_BUTTON);
	//Make this registry public button
	_assertVisible($GIFTREGISTRY_MAKETHISPUBLIC_BUTTON);
	_assertNotVisible($GIFTREGISTRY_MAKETHISPRIVATE_BUTTON);
	
	//header gift registry link
	_click($FOOTER_GIFTREGISTRY_LINK);
	
	//items should not be visible
	_setValue($GIFTREGISTRY_LASTNAME_TEXTBOX, $LName);
	_setValue($GIFTREGISTRY_FIRSTNAME_TEXTBOX, $FName);
	
	//click on find button
	_click($GIFTREGISTRY_FIND_BUTTON);
	
	//GIFT REGISTRY results table should not display for invalid credentials
	var $expText=$LName+" "+$FName;
	_assertEqual(false,_isVisible(_row("/"+$expText+"/"))); 
	_assertNotVisible($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
	_assertVisible($GIFTREGISTRY_NOGIFTREGISTRYFOUND_MESSAGE);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
//**
var $t = _testcase("124349", "Verify the functionality related to the ' 'Make This Registry Public' button on Gift registery page in application as a  Registered user.' ");
$t.start();
try
{
	//header gift registry link
	_click($FOOTER_GIFTREGISTRY_LINK);
	_click($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
	//Make this registry public button
	_assertVisible($GIFTREGISTRY_MAKETHISPUBLIC_BUTTON);
	//click on make this registry public button
	_click($GIFTREGISTRY_MAKETHISPUBLIC_BUTTON);
	//Validating navigation
	//make this registry private
	_assertVisible($GIFTREGISTRY_MAKETHISPRIVATE_BUTTON);
	//make this item public check box
	_assertVisible($GIFTREGISTRY_MAKETHISITEMPUBLIC_CHECKBOX);
	_assert($GIFTREGISTRY_MAKETHISITEMPUBLIC_CHECKBOX.checked);
	
	//header gift registry link
	_click($FOOTER_GIFTREGISTRY_LINK);
	
	//items should not be visible
	_setValue($GIFTREGISTRY_LASTNAME_TEXTBOX, $LName);
	_setValue($GIFTREGISTRY_FIRSTNAME_TEXTBOX, $FName);
	
	//click on find button
	_click($GIFTREGISTRY_FIND_BUTTON);
	
	//GIFT REGISTRY results table should not display for invalid credentials
	var $expText=$LName+" "+$FName;
	_assertVisible(_row("/"+$expText+"/"));
	_assertVisible($WISHLIST_VIEW_LINK);
	_assertNotVisible($WISHLIST_NOWISHLIST_PARARAPH);  
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//**
var $t = _testcase("289803/124322/288077/289804", "Verify UI of gift registry search results page/UI of Gift Registry page of the application as a registered user having saved gift registries/UI of left navigation pane of gift registry page of the application as a registered user/ UI of left navigation pane in gift registry search results page");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_GIFTREGISTRY_LINK);

	
	//navigate to search result page
	navigateToFindSomeOneGiftRegistry();
	
	var $expText=$LName+" "+$FName;
	_assertEqual(true,_isVisible(_row("/"+$expText+"/")));
	
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));

	//heading
	_assertVisible($GIFTREGISTRY_FINDSOMEONE_HEADING);
	
	//Left nav
	var $Leftnavlinks=HeadingCountInLeftNav();
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	//UI of  leftnavlinks
	for(var $i=0;$i<$Leftnavlinks.length;$i++)
		{
			var $Expected_Leftnavlink = _span($LeftNav[$i][1]);
			
			_assertVisible($Expected_Leftnavlink);			
		}
	
	//UI of leftnavlinks Names
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
		{
		    var $Expected_LeftnavlinkNames = _link($LeftNav[$i][2]);
		    
			_assertVisible($Expected_LeftnavlinkNames);			
		}
	//last name
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXTBOX);
	//first name
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXTBOX);
	//email 
	_assertVisible($GIFTREGISTRY_EVENTTYPE_TEXT);
	_assertVisible($GIFTREGISTRY_EVENTTYPE_DROPDOWN);
	//find button
	_assertVisible($GIFTREGISTRY_FIND_BUTTON);
	//advanced search
	_assertVisible($GIFTREGISTRY_ADVANCESEARCH_LINK);
	
	//UI of search result
	//heading
	_assertVisible($GIFTREGISTRY_SEARCHRESULT_HEADING);
	
	//table header
	_assertVisible($GIFTREGISTRY_YOURGIFTREGISTRY_ROW_ATTRIBUTES_GUEST);
	//registry contents
	_assertVisible(_row($LastName1+" "+$FirstName1+" "+$GiftRegistry[0][1]+" "+$GiftRegistry[0][16]+" "+$GiftRegistry[0][6]+", "+$GiftRegistry[1][5]+" View"));		

	//view link
	_assertVisible($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
	//delete link
	_assertNotVisible($GIFTREGISTRY_DELETE_REGISTRY_LINK);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("289811", "Verify the application behavior on click of components in breadcrumb of gift registry search results page as registered user");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_GIFTREGISTRY_LINK);

	
	//navigate to search result page
	navigateToFindSomeOneGiftRegistry();
	
	var $expText=$LName+" "+$FName;
	_assertEqual(true,_isVisible(_row("/"+$expText+"/")));
	
	//navigating to Gift Registry page
	_assertEqual($Giftregistry_guest[12][0], _getText($PAGE_BREADCRUMB));
	
	//click on my account link present in the bread crumb
	_click($MY_ACCOUNT_LINK_BREADCRUMB);
	//verify the navigation
	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
	_assertVisible($MY_ACCOUNT_HEADING);
	_assertVisible($MY_ACCOUNT_OPTIONS);
	}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("289810", "Verify the navigation related to links in left navigation pane of gift registry search result page as a registered user");
$t.start();
try
{

	_click($HEADER_GIFTREGISTRY_LINK);
	
	//navigate to search result page
	navigateToFindSomeOneGiftRegistry()

	//left nav links	
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
	{
          //Navigate to gift registry search
		_click($HEADER_GIFTREGISTRY_LINK);
		//navigate to search result page
		navigateToFindSomeOneGiftRegistry()
		
		//click on left nav links 
		_click($LeftnavlinkNames[$i]);
		
		var $Expected_LeftnavHeading = _heading1("/"+$LeftNav[$i][4]+"/");
		
		//verify the navigation
		_assertVisible($Expected_LeftnavHeading);
		_assertContainsText($LeftNav[$i][3], $PAGE_BREADCRUMB);
	}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//**
var $t = _testcase("124357", "Verify the UI elements after clicking 'Advanced Search' link  on Gift registery page, find someone's Gift registery list section in applicationas a  Registered user.");
$t.start();
try
{
	_click($HEADER_GIFTREGISTRY_LINK);
	_click($GIFTREGISTRY_ADVANCESEARCH_LINK);
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXTBOX);
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXTBOX);
	_assertVisible($GIFTREGISTRY_EVENTTYPE_DROPDOWN);
	_assertVisible($GIFTREGISTRY_EVENTMONTH_TEXTBOX);
	_assertVisible($GIFTREGISTRY_EVENTYEAR_TEXTBOX);
	_assertVisible($GIFTREGISTRY_EVENTCITY_TEXTBOX);
	_assertVisible($GIFTREGISTRY_STATE_DROPDOWN);
	_assertVisible($GIFTREGISTRY_COUNTRY_DROPDOWN);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("156106", "Verify application behavior of state drop down when country drop down value changes.");
$t.start();
try
{
	//set US as country
	_setValue($GIFTREGISTRY_COUNTRY_DROPDOWN, $Generic_GiftRegistry[7][10]);
	
	//verify states
	_assertEqual($Generic_GiftRegistry[6][10], _getText($GIFTREGISTRY_STATE_DROPDOWN));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//**
var $t = _testcase("124358/148787/148788/148789/148790/148791/288129/288228", "Verify validation related to Event name/Event City/Event Type Event Month text fields/Year drop down/State drop down/Country drop down  present  on Gift registery page, find someone's Gift registery list section in applicationas a  Registered user/functionality related to 'Find' button in find someone's Gift Registry section of gift registry page as a registered user");
$t.start();
try
{
	for(var $i=0;$i<$Advance.length;$i++)
	{
		
		_setValue($GIFTREGISTRY_LASTNAME_TEXTBOX,$Advance[$i][0]);
		_setValue($GIFTREGISTRY_FIRSTNAME_TEXTBOX,$Advance[$i][1]);
		_setValue($GIFTREGISTRY_EVENTTYPE_DROPDOWN,$Advance[$i][2]);
		_setValue($GIFTREGISTRY_EVENTNAME_TEXTBOX,$Advance[$i][3]);
		_setSelected($GIFTREGISTRY_EVENTMONTH_TEXTBOX,$Advance[$i][4]);
		_setSelected($GIFTREGISTRY_EVENTYEAR_TEXTBOX,$Advance[$i][5]);
		_setValue($GIFTREGISTRY_EVENTCITY_TEXTBOX,$Advance[$i][6]);	
		if($i==$Advance.length-1)
			{
			_setValue($GIFTREGISTRY_LASTNAME_TEXTBOX,$LastName1);
			_setValue($GIFTREGISTRY_FIRSTNAME_TEXTBOX,$FirstName1);
			}
		//_setSelected($GIFTREGISTRY_STATE_DROPDOWN,$Advance[$i][7]);
		//_setSelected($GIFTREGISTRY_COUNTRY_DROPDOWN,$Advance[$i][8]);
		_click($GIFTREGISTRY_FIND_BUTTON);
		_assertVisible($GIFTREGISTRY_SEARCHRESULT_HEADING);
		if($i==$Advance.length-1)
			{
				_assertVisible($GIFTREGISTRY_SEARCHRESULT_CONTENT);
				_assertVisible(_row($LastName1+" "+$FirstName1+" "+$GiftRegistry[0][1]+" "+$GiftRegistry[0][16]+" "+$GiftRegistry[0][6]+", "+$GiftRegistry[1][5]+" View"));		
			}
		else
			{
				_assertVisible($GIFTREGISTRY_NOGIFTREGISTRYFOUND_MESSAGE);
			}	
		_click($GIFTREGISTRY_ADVANCESEARCH_LINK);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//**
var $t=_testcase("288157/288158","Verify validation related to Last name/First Name text field present in find someone's gift registry section on gift registry page in application as a registered user");
$t.start();
try
{
//navigating to Gift Registry page
_click($FOOTER_GIFTREGISTRY_LINK);
//validating the fields last name,first name,email 
for(var $i=0;$i<$Validations_GiftRegistry.length;$i++)
  {
	_setValue($GIFTREGISTRY_LASTNAME_TEXTBOX, $Validations_GiftRegistry[$i][1]);
	_setValue($GIFTREGISTRY_FIRSTNAME_TEXTBOX, $Validations_GiftRegistry[$i][2]);
	
	//click on find button
	_click($GIFTREGISTRY_FIND_BUTTON);
	//Blank
	if($i==0)
		{
			//last name,first name fields should highlight
			_assertEqual($Validations_GiftRegistry[0][4],_style($GIFTREGISTRY_LASTNAME_TEXTBOX,"background-color"));
			_assertEqual($Validations_GiftRegistry[0][4],_style($GIFTREGISTRY_FIRSTNAME_TEXTBOX,"background-color"));
		}
	//invalid
	if($i==2 || $i==3 || $i==4 || $i==5 || $i==6)
		{
			_setSelected($GIFTREGISTRY_EVENTTYPE_DROPDOWN,$Validations_GiftRegistry[$i][3]);
			//user should be in same page
			_assertVisible(_heading1("/"+$Giftregistry_guest[8][1]+"/"));
			//verify the text message
			if(i==6)
				{
				_setValue($GIFTREGISTRY_EVENTTYPE_DROPDOWN, $Validations_GiftRegistry[$i][3]);
				_assertVisible($GIFTREGISTRY_PRODUCT_TABLES);
				_assertVisible($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
				}
			else
				{
				
				_assertVisible($GIFTREGISTRY_NOGIFTREGISTRYFOUND_MESSAGE);
				}
		}
	//max characters
	if($i==1)
		{
			_assertEqual($Validations_GiftRegistry[1][4],_getText($GIFTREGISTRY_LASTNAME_TEXTBOX).length);
			_assertEqual($Validations_GiftRegistry[1][4],_getText($GIFTREGISTRY_FIRSTNAME_TEXTBOX).length);		
		}
	}	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t = _testcase("124325", "Verify the functionality related to 'Delete' link in Gift Registry page of the application as a registered user having saved gift registry");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);
	_click($FOOTER_GIFTREGISTRY_LINK);
	if(_isVisible($GIFTREGISTRY_DELETE_REGISTRY_LINK))
	  {
	  
	      _click($GIFTREGISTRY_DELETE_REGISTRY_LINK);
	      _expectConfirm("Do you want to remove this gift registry?", true); 
	      _assertNotVisible($GIFTREGISTRY_YOURGIFTREGISTRY_HEADING);
	      _assertNotVisible(_table("item-list"));
	    _log("Gift Registry deleted successfully");
	  }
	  else
	  { 
	    _assert(false, "Delete Link is not present");
	  }
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

_click($HEADER_HOME_LINK);

cleanup();