_include("../../../GenericLibrary/GlobalFunctions.js");

_setAccessorIgnoreCase(true); 
var $FirstName1=$FName;
var $LastName1=$LName;
var $UserEmail=$uId;

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//login to the application and delete gift registry
_click($HEADER_LOGIN_LINK);
login();
Delete_GiftRegistry();
//logout from application
_click($HEADER_LOGOUT_LINK);

//**
var $t = _testcase("124308/288076","Verify the UI of 'Returning customers' section Gift Registry login page in application as an Anonymous user/ UI of left navigation pane of gift registry login page of the application as a guest user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click($FOOTER_GIFTREGISTRY_LINK);
	//verify bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertVisible($GIFTREGISTRY_LINK_BREADCRUMB);
	//Verifying the UI of returning customer section
	//MyAccount login as heading
	_assertVisible($GIFTREGISTRY_LOGIN_HEADING);
	//Returning customer heading
	_assertVisible($GIFTREGISTRY_LOGIN_RETURNINGCUSTOMERS_HEADING);
	//Paragraph with static text
	_assertVisible($GIFTREGISTRY_LOGIN_PARAGRAPH);
	//email Address
	_assertVisible($GIFTREGISTRY_LOGIN_EMAILTEXT);
	_assertVisible($GIFTREGISTRY_LOGIN_EMAIL_ADDRESS_TEXTBOX);
	_assertEqual("", _getValue($GIFTREGISTRY_LOGIN_EMAIL_ADDRESS_TEXTBOX));
	//password
	_assertVisible($GIFTREGISTRY_LOGIN_PASSWORDTEXT);
	_assertVisible($GIFTREGISTRY_LOGIN_PASSWORD_TEXTBOX);
	_assertEqual("", _getValue($GIFTREGISTRY_LOGIN_PASSWORD_TEXTBOX));
	//remember me
	_assertVisible($GIFTREGISTRY_LOGIN_REMEMBERMETEXT);
	_assertVisible($GIFTREGISTRY_LOGIN_REMEMBEMETEXTBOX);
	_assertNotTrue($GIFTREGISTRY_LOGIN_REMEMBEMETEXTBOX.checked);
	//login button
	_assertVisible($GIFTREGISTRY_LOGIN_SUBMIT_BUTTON);
	//forgot password
	_assertVisible($GIFTREGISTRY_FORGETPASSWORD_LINK);
	_assertEqual($Giftregistry_guest[8][0], _getText($GIFTREGISTRY_FORGETPASSWORD_LINK));
	//social links
	_assertVisible($GIFTREGISTRY_LOGIN_SOCIALLINKS);
	_assertVisible($GIFTREGISTRY_ENTIRE_LOGIN_SECTION);
	
	//left nav
	var $Leftnavlinks=HeadingCountInLeftNav();
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	//UI of  leftnavlinks
	for(var $i=0;$i<$Leftnavlinks.length;$i++)
		{
			var $Expected_Leftnavlink = _span($LeftNav[$i][5]);
			
			_assertVisible($Expected_Leftnavlink);			
		}
	
	//UI of leftnavlinks Names
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
		{
		    var $Expected_LeftnavlinkNames = _link($LeftNav[$i][6]);
		    
			_assertVisible($Expected_LeftnavlinkNames);			
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//**
var $t=_testcase("124309","Verify the UI of ' New Customer's' section Gift Registry login page in application as an  Anonymous user");
$t.start();
try
{
	//verify the UI of New Customer section
	_assertVisible($GIFTREGISTRY_LOGIN_NEW_CUSTOMER_HEADING);
	_assertVisible($CONTENT_ASSET);
	_assertVisible($GIFTREGISTRY_LOGIN_CREATEACCOUNTNOW_BUTTON);
	_assertVisible($GIFTREGISTRY_LOGIN_PARAGRAPH);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124310","Verify the UI of 'FIND SOMEONE'S GIFT REGISTRY section Gift Registry login page in application as an Anonymous user");
$t.start();
try
{
	//verify the UI of 'FIND SOMEONE'S GIFT REGISTRY
	_assertVisible($GIFTREGISTRY_LOGIN_FINDSOMEONES_REGISTRY_HEADING);
	_assertVisible($GIFTREGISTRY_FINDSOMEONES_REGISTRY_PARAGRAPH);
	//last name
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXTBOX);
	//first name
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXTBOX);
	//email 
	_assertVisible($GIFTREGISTRY_EVENTTYPE_TEXT);
	_assertVisible($GIFTREGISTRY_EVENTTYPE_DROPDOWN);
	//find button
	_assertVisible($GIFTREGISTRY_FIND_BUTTON);
	//advanced search
	_assertVisible($GIFTREGISTRY_ADVANCESEARCH_LINK);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("125408","Verify the UI of 'Gift Registry login page' in application as an Anonymous user");
$t.start();
try
{
	_assertVisible($GIFTREGISTRY_LINK_BREADCRUMB);		
	_assert(_isVisible($GIFTREGISTRY_LOGIN_HEADING));
	//left nav section
	_assertVisible($LEFTNAV_ACCOUNTSETTINGS);
	_assertVisible($LEFTNAV_CREATEACCOUNT_LINK);
	_assertVisible($LEFTNAV_SHOPCONFIDENTLY);
	_assertVisible($LEFTNAV_PRIVACYPOLICY_LINK);
	_assertVisible($LEFTNAV_SECURESHOPPING_LINK);
	//need help section
	_assertVisible($LEFTNAV_NEEDHELP_HEADING);
	_assertVisible($LEFTNAV_CONTENTASSET);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124304","Verify the navigation on click of links in breadcrumb of Gift Registry login page in application as an anonymous user");
$t.start();
try
{
	//verify the navigation to home page
	_click($HEADER_HOME_LINK);
	_assertVisible($HOMEPAGE_SLIDES);
	_assertVisible($HOMEPAGE_SLOTS);
	//navigating to Gift Registry page
	_click($FOOTER_GIFTREGISTRY_LINK);
	_assertEqual($Giftregistry_guest[12][0], _getText(_div("breadcrumb")));
	
	//click on my account link present in the bread crumb
	_click($MY_ACCOUNT_LINK_BREADCRUMB);
	//verify the navigation
	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
	_assertVisible($MY_ACCOUNT_HEADING);
	_assertVisible($LOGIN_SECTION);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124305/125423/148734/148735","Verify the navigation related to Create an Account/Privacy Policy/Secure Shopping/Contact Us link on Gift Registry login page left nav in application as an  Anonymous user");
$t.start();
try
{
	//left nav links	
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
	{
         //Navigate to Gift Registry login page
		_click($FOOTER_GIFTREGISTRY_LINK);
		
		//click on left nav links 
		_click($LeftnavlinkNames[$i]);
		
		var $Expected_LeftnavHeading = _heading1("/"+$LeftNav[$i][8]+"/");
		
		//verify the navigation
		_assertVisible($Expected_LeftnavHeading);
		_assertContainsText($LeftNav[$i][7], $PAGE_BREADCRUMB);
	}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


//**
var $t=_testcase("148736","Verify the UI of 'Forgot Password' overlay on Gift Registry login page in application as an Anonymous user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click($FOOTER_GIFTREGISTRY_LINK);
	//click on forgot password link
	_click($GIFTREGISTRY_FORGETPASSWORD_LINK);
	//verify the overlay appearance
	_assertVisible($GIFTREGISTRY_PASSWORDRESET_DIALOG);
	_assertVisible($GIFTREGISTRY_RESETPASSWORD_HEADING);
	_assertVisible($GIFTREGISTRY_LOGIN_FORGETPASSWORD_TEXTBOX);
	_assertVisible($GIFTREGISTRY_FORGETPASSWORD_SEND_BUTTON);
	_assertVisible($GIFTREGISTRY_FORGETPASSWORD_CANCEL_BUTTON);
	//close the overlay
	_click($GIFTREGISTRY_FORGETPASSWORD_CANCEL_BUTTON);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124313/288223","Verify the forgot password functionality on Gift Registry login page of the application as an anonymous user/application behavior on click of forgot password link in gift registry login page of the application as a guest user");
$t.start();
try
{
	_click($GIFTREGISTRY_FORGETPASSWORD_LINK);
	//entering valid email address
	_setValue($GIFTREGISTRY_LOGIN_FORGETPASSWORD_TEXTBOX,$Giftregistry_guest[0][2]);	
	//click on send button
	_click($GIFTREGISTRY_FORGETPASSWORD_SEND_BUTTON);
	//verify the navigation
	_assertVisible($GIFTREGISTRY_RESETPASSWORD_CONFIRMATION_HEADING);
	_assertVisible($HEADER_HOME_LINK);
	//close the dialog box
	_click($GIFTREGISTRY_FORGETPASSWORD_CANCEL_BUTTON);	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124314","Verify the navigation related to 'Create an account now' button Gift Registry login page create in application as an Anonymous user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click($FOOTER_GIFTREGISTRY_LINK);
	//click on link Create an account now button
	_click($GIFTREGISTRY_LOGIN_CREATEACCOUNTNOW_BUTTON);
	//verify the navigation
	_assertVisible($GIFTREGISTRY_CREATEACCOUNTNOW_HEADING);
	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124311/148738/124312","Verify validation related to the 'Email address'/'password' field's and login button on Gift Registry login page, Returning customers section in application as a Anonymous user");
$t.start();
try
{
	
	
	_click($HEADER_HOME_LINK);
	_click($FOOTER_GIFTREGISTRY_LINK);
	//validating the email address and password fields	
	for(var $i=0;$i<$GiftRegistyLogin_Validation.length;$i++)
		{
		if($i==5)
			{
				login();
			}
		else
			{
		_setValue($GIFTREGISTRY_LOGIN_EMAIL_ADDRESS_TEXTBOX,$GiftRegistyLogin_Validation[$i][1]);
		_setValue($GIFTREGISTRY_LOGIN_PASSWORD_TEXTBOX,$GiftRegistyLogin_Validation[$i][2]);
		_click($GIFTREGISTRY_LOGIN_SUBMIT_BUTTON);
			}
		//blank
		if($i==0)
			{
			//username
			_assertEqual($GiftRegistyLogin_Validation[$i][5],_style($GIFTREGISTRY_LOGIN_EMAIL_ADDRESS_TEXTBOX,"background-color"));
			_assertVisible(_span($GiftRegistyLogin_Validation[$i][3]));
			//password
			_assertVisible(_span($GiftRegistyLogin_Validation[$i][4]));
			_assertEqual($GiftRegistyLogin_Validation[$i][5],_style($GIFTREGISTRY_LOGIN_PASSWORD_TEXTBOX,"background-color"));
			}
		//valid
		else if($i==5)
			{
			_assertVisible($GIFTREGISTRY_FINDSOMEONE_HEADING);
			}
		//invalid email id
		else if($i==1||$i==2||$i==3)
		{
			//username
			_assertEqual($GiftRegistyLogin_Validation[0][5],_style($GIFTREGISTRY_LOGIN_EMAIL_ADDRESS_TEXTBOX,"background-color"));
			_assertVisible(_span($GiftRegistyLogin_Validation[2][3]));
		}
		//invalid 
		else
			{
			_assertVisible(_div($GiftRegistyLogin_Validation[1][3]));
			}
		}
	//logout from application
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_LOGOUT_LINK);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


//**
var $t=_testcase("148739/148740/124316","Verify validation related to the 'Last name'/'First name' field and find button functionality on Gift Registry login page, find someone's Gift Registry section in application as a Anonymous user");
$t.start();
try
{

//validating the fields last name,first name,email 
for(var $i=0;$i<$Validations_GiftRegistry.length;$i++)
  {
	//navigating to Gift Registry page
	_click($FOOTER_GIFTREGISTRY_LINK);

	//max length
	if($i==1)
	{
		_setValue($GIFTREGISTRY_LASTNAME_TEXTBOX, $Validations_GiftRegistry[$i][1]);
		_setValue($GIFTREGISTRY_FIRSTNAME_TEXTBOX, $Validations_GiftRegistry[$i][2]);
	}
	else
		{
			_setValue($GIFTREGISTRY_LASTNAME_TEXTBOX, $Validations_GiftRegistry[$i][1]);
			_setValue($GIFTREGISTRY_FIRSTNAME_TEXTBOX, $Validations_GiftRegistry[$i][2]);
			_setSelected($GIFTREGISTRY_EVENTTYPE_DROPDOWN,$Validations_GiftRegistry[$i][3]);
			//click on find button
			_click($GIFTREGISTRY_FIND_BUTTON);
		}
	//Blank
	if($i==0)
		{
			//last name,first name fields should highlight
			_assertEqual($Validations_GiftRegistry[0][4],_style($GIFTREGISTRY_LASTNAME_TEXTBOX,"background-color"));
			_assertEqual($Validations_GiftRegistry[0][4],_style($GIFTREGISTRY_FIRSTNAME_TEXTBOX,"background-color"));
		}
	//invalid
	if($i==2 || $i==3 || $i==4 || $i==5 || $i==6)
		{
			
			//user should be in same page
			_assertVisible(_heading1("/"+$Giftregistry_guest[8][1]+"/"));
			//verify the text message
			if(i==6)
				{
					//_setSelected($GIFTREGISTRY_EVENTTYPE_DROPDOWN, $Validations_GiftRegistry[$i][3]);
					_assertVisible($GIFTREGISTRY_PRODUCT_TABLES);
					_assertVisible($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
				}
			else
				{
				
					_assertVisible($GIFTREGISTRY_NOGIFTREGISTRYFOUND_MESSAGE);
				}
		}
	//max characters
	if($i==1)
		{
		_assertEqual($Validations_GiftRegistry[1][4],_getText($GIFTREGISTRY_LASTNAME_TEXTBOX).length);
		_assertEqual($Validations_GiftRegistry[1][4],_getText($GIFTREGISTRY_FIRSTNAME_TEXTBOX).length);		
		}
	}	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();




//**
var $t = _testcase("289800", "Verify the UI of empty gift registry search results page in application as a guest user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click($FOOTER_GIFTREGISTRY_LINK);
	_wait(3000);
	
	//navigate to search result page
	navigateToFindSomeOneGiftRegistry();
	
	//bread crumb
	_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));

	//heading
	_assertVisible($GIFTREGISTRY_FINDSOMEONE_HEADING);
	
	//left nav
	var $Leftnavlinks=HeadingCountInLeftNav();
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	//UI of  leftnavlinks
	for(var $i=0;$i<$Leftnavlinks.length;$i++)
		{
			var $Expected_Leftnavlink = _span($LeftNav[$i][5]);
			
			_assertVisible($Expected_Leftnavlink);			
		}
	
	//UI of leftnavlinks Names
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
		{
		    var $Expected_LeftnavlinkNames = _link($LeftNav[$i][6]);
		    
			_assertVisible($Expected_LeftnavlinkNames);			
		}
	
	//last name
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXTBOX);
	//first name
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXTBOX);
	//email 
	_assertVisible($GIFTREGISTRY_EVENTTYPE_TEXT);
	_assertVisible($GIFTREGISTRY_EVENTTYPE_DROPDOWN);
	//find button
	_assertVisible($GIFTREGISTRY_FIND_BUTTON);
	//advanced search
	_assertVisible($GIFTREGISTRY_ADVANCESEARCH_LINK);
	//No gift registry heading
	_assertVisible($GIFTREGISTRY_NOGIFTREGISTRYFOUND_MESSAGE);
	_assertVisible($GIFTREGISTRY_SEARCHRESULT_HEADING);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//**********************************************test case regarding Search result page***************************************

//Add item in gift registry

_click($HEADER_LOGIN_LINK);
login();

_click($MY_ACCOUNT_GIFTREGISTRY_OPTIONS);

//click on new registry
_click($GIFTREGISTRY_CREATENEW_REGISTRY_BUTTON);
//creating the registry
createRegistry($GiftRegistry, 0);

Pre_PostEventInfo($GiftRegistry, 4);
//Submit button
_click($GIFTREGISTRY_EVENTINFORMATION_CONTINUE_BUTTON);

//adding items to gift registry
addItems($Generic_GiftRegistry[0][11]);

//make it public
_click($GIFTREGISTRY_MAKETHISPUBLIC_BUTTON);

//logout
logout();

//**
var $t = _testcase("289813/289808", "Verify the application behavior on click of find button /application behavior on click of breadcrumb components in gift registry search results page of the application as a guest user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click($FOOTER_GIFTREGISTRY_LINK);
	_wait(3000);
	
	//navigate to search result page
	navigateToFindSomeOneGiftRegistry();
	var $expText=$LName+" "+$FName;
	_assertEqual(true,_isVisible(_row("/"+$expText+"/")));
	
	//verify breadcrumb
	_assertEqual($Giftregistry_guest[12][0], _getText($PAGE_BREADCRUMB));
	
	//click on my account link present in the bread crumb
	_click($MY_ACCOUNT_LINK_BREADCRUMB);
	//verify the navigation
	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
	_assertVisible($MY_ACCOUNT_HEADING);
	_assertVisible($LOGIN_SECTION);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();




var $t = _testcase("289802", "Verify the UI of gift registry search results page in application as a guest user");
$t.start();
try
{
	
	//navigating to Gift Registry page
	_click($FOOTER_GIFTREGISTRY_LINK);

	//navigate to search result page
	navigateToFindSomeOneGiftRegistry()

	var $expText=$LName+" "+$FName;
	_assertEqual(true,_isVisible(_row("/"+$expText+"/")));
	
	//bread crumb
	_assertEqual($Generic_GiftRegistry[0][0], _getText($PAGE_BREADCRUMB));

	//heading
	_assertVisible($GIFTREGISTRY_FINDSOMEONE_HEADING);
	
	//Left nav
	var $Leftnavlinks=HeadingCountInLeftNav();
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	//UI of  leftnavlinks
	for(var $i=0;$i<$Leftnavlinks.length;$i++)
		{
			var $Expected_Leftnavlink = _span($LeftNav[$i][5]);
			
			_assertVisible($Expected_Leftnavlink);			
		}
	
	//UI of leftnavlinks Names
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
		{
		    var $Expected_LeftnavlinkNames = _link($LeftNav[$i][6]);
		    
			_assertVisible($Expected_LeftnavlinkNames);			
		}
	
	//last name
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_LASTNAME_TEXTBOX);
	//first name
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXT);
	_assertVisible($GIFTREGISTRY_FIRSTNAME_TEXTBOX);
	//email 
	_assertVisible($GIFTREGISTRY_EVENTTYPE_TEXT);
	_assertVisible($GIFTREGISTRY_EVENTTYPE_DROPDOWN);
	//find button
	_assertVisible($GIFTREGISTRY_FIND_BUTTON);
	//advanced search
	_assertVisible($GIFTREGISTRY_ADVANCESEARCH_LINK);

	//UI of search result
	//heading
	_assertVisible($GIFTREGISTRY_SEARCHRESULT_HEADING);
	
	//table header
	_assertVisible($GIFTREGISTRY_YOURGIFTREGISTRY_ROW_ATTRIBUTES_GUEST);
	//registry contents
	_assertVisible(_row($LastName1+" "+$FirstName1+" "+$GiftRegistry[0][1]+" "+$GiftRegistry[0][16]+" "+$GiftRegistry[0][6]+", "+$GiftRegistry[1][5]+" View"));		

	//view link
	_assertVisible($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
	//delete link
	_assertNotVisible($GIFTREGISTRY_DELETE_REGISTRY_LINK);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//**
var $t=_testcase("289805/289806/289807","Verify the validations for first name/last name/event type field in gift registry search results page of the application");
$t.start();
try
{
	//validating the fields last name,first name,email 
	for(var $i=0;$i<$Validations_GiftRegistry.length;$i++)
	  {
		
		//max length
		if($i==1)
		{
			_setValue($GIFTREGISTRY_LASTNAME_TEXTBOX, $Validations_GiftRegistry[$i][1]);
			_setValue($GIFTREGISTRY_FIRSTNAME_TEXTBOX, $Validations_GiftRegistry[$i][2]);
		}
		else
			{
				_setValue($GIFTREGISTRY_LASTNAME_TEXTBOX, $Validations_GiftRegistry[$i][1]);
				_setValue($GIFTREGISTRY_FIRSTNAME_TEXTBOX, $Validations_GiftRegistry[$i][2]);
				_setSelected($GIFTREGISTRY_EVENTTYPE_DROPDOWN,$Validations_GiftRegistry[$i][3]);
				//click on find button
				_click($GIFTREGISTRY_FIND_BUTTON);
			}
		//Blank
		if($i==0)
			{
				//last name,first name fields should highlight
				_assertEqual($Validations_GiftRegistry[0][4],_style($GIFTREGISTRY_LASTNAME_TEXTBOX,"background-color"));
				_assertEqual($Validations_GiftRegistry[0][4],_style($GIFTREGISTRY_FIRSTNAME_TEXTBOX,"background-color"));
			}
		//invalid
		if($i==2 || $i==3 || $i==4 || $i==5 || $i==6)
			{
				
				//user should be in same page
				_assertVisible(_heading1("/"+$Giftregistry_guest[8][1]+"/"));
				//verify the text message
				if(i==6)
					{
						//_setSelected($GIFTREGISTRY_EVENTTYPE_DROPDOWN, $Validations_GiftRegistry[$i][3]);
						_assertVisible($GIFTREGISTRY_PRODUCT_TABLES);
						_assertVisible($GIFTREGISTRY_VIEWLINK_PRODUCTTABLE);
					}
				else
					{
					
						_assertVisible($GIFTREGISTRY_NOGIFTREGISTRYFOUND_MESSAGE);
					}
			}
		//max characters
		if($i==1)
			{
			_assertEqual($Validations_GiftRegistry[1][4],_getText($GIFTREGISTRY_LASTNAME_TEXTBOX).length);
			_assertEqual($Validations_GiftRegistry[1][4],_getText($GIFTREGISTRY_FIRSTNAME_TEXTBOX).length);		
			}
	  }
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("289809", "Verify the navigation related to links in left navigation pane of wish list search result page as a guest user");
$t.start();
try
{
	//left nav links	
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
	{
		//navigate to search result page
		_click($FOOTER_GIFTREGISTRY_LINK);
		navigateToFindSomeOneGiftRegistry();
		var $expText=$LName+" "+$FName;
		_assertEqual(true,_isVisible(_row("/"+$expText+"/")));
		
		//click on left nav links 
		_click($LeftnavlinkNames[$i]);
		
		var $Expected_LeftnavHeading = _heading1("/"+$LeftNav[$i][8]+"/");
		
		//verify the navigation
		_assertVisible($Expected_LeftnavHeading);
		_assertContainsText($LeftNav[$i][7], $PAGE_BREADCRUMB);
	}
		
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
