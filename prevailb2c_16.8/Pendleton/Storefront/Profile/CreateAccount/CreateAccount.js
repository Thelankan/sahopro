_include("../../../GenericLibrary/GlobalFunctions.js");

//variable declaration
var $color=$Account_Validation_createaccount[0][8];
var $color1=$Account_Validation_createaccount[1][8];

//deleting the user if exists
deleteUser();
SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);


var $t = _testcase("124585", "Verify the functionality of breadcrumb displayed in create account page of the application as an anonymous user");
$t.start();
try
{

	//Navigate to create account page
	_click($HEADER_LOGIN_LINK);
	_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
	
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Generic_Register[1][0], _getText($PAGE_BREADCRUMB));

	//Navigation related to my account
	_click($PAGE_BREADCRUMB_LINK);
	_assertVisible($MY_ACCOUNT_HEADING);
	_assertVisible($LOGIN_SECTION);

	_click($HEADER_LOGIN_LINK);
	_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//heading
	_assertVisible($REGISTER_HEADING);
	//breadcrumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Generic_Register[1][0], _getText($PAGE_BREADCRUMB));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148638", "Verify the UI  of 'Create Account' page in application as an  Anonymous user.");
$t.start();
try
{
	//click on user icon from header
	_click($HEADER_LOGIN_LINK);
	//click on register link
	_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//verify page
	_assertVisible($REGISTER_HEADING);
	//breadcrumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Generic_Register[1][0], _getText($PAGE_BREADCRUMB));
	//EMAIL / LOGIN INFORMATION
	_assertVisible($REGISTER_FIELDSET);
	//FName
	_assertVisible($REGISTER_FIRSTNAME);
	_assertVisible($REGISTER_FIRSTNAME_TEXTBOX);
	_assertEqual("", _getValue($REGISTER_FIRSTNAME_TEXTBOX));
	//Lname
	_assertVisible(_span($REGISTER_LASTNAME));
	_assertVisible($REGISTER_LASTNAME_TEXTBOX);
	_assertEqual("", _getValue($REGISTER_LASTNAME_TEXTBOX));

	//Email
	_assertVisible($REGISTER_EMAIL);
	_assertVisible($REGISTER_EMAIL_TEXTBOX);
	_assertEqual("", _getValue($REGISTER_EMAIL_TEXTBOX));
	//confirm email
	_assertVisible($REGISTER_CONFIRM_EMAIL);
	_assertVisible($REGISTER_CONFIRM_EMAIL_TEXTBOX);
	_assertEqual("", _getValue($REGISTER_CONFIRM_EMAIL_TEXTBOX));
	//Password
	_assertVisible($REGISTER_PASSWORD);
	_assertVisible($REGISTER_PASSWORD_TEXTBOX);
	_assertEqual("", _getValue($REGISTER_PASSWORD_TEXTBOX));
	//static text
	_assertVisible($REGISTER_PASSWORD_STATICTEXT);
	//confirm password
	_assertVisible($REGISTER_CONFIRM_PASSWORD);
	_assertVisible($REGISTER_CONFIRM_PASSWORD_TEXTBOX);
	_assertEqual("", _getValue($REGISTER_CONFIRM_PASSWORD_TEXTBOX));
	//checkbox
	_assertVisible($REGISTER_CHECKBOX);
	_assertNotTrue($REGISTER_CHECKBOX.checked);
	//Privacy policy link
	_assertVisible($REGISTER_SEEPRIVACYPOLICY_LINK);
	//apply button
	_assertVisible($REGISTER_SUBMIT_BUTTON);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124592", "Verify the application behavior on click of 'see privacy policy link' in create account page of the application.");
$t.start();
try
{
	
//click on user icon from header
_click($HEADER_LOGIN_LINK);
//click on register link
_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
//click on privacy policy from register page
_click($REGISTER_SEEPRIVACYPOLICY_LINK);
//verify the privacy policy overlay
_assertVisible(_heading1("Privacy Policy"));
_assertVisible(_div("dialog-container"));
_click(_button("Close"));
_assertNotVisible(_div("dialog-container"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124588/124589/124601/125240/124599/288242", "Verify the validation related to  'First name'/'Last name'/'Email'/'Confirm Email'/'Password'/'Confirm Password' field  and  functionality related to 'Apply' button on Create account page in application as an  Anonymous user.");
$t.start();
try
{
	//click on user icon from header
	_click($HEADER_LOGIN_LINK);
	//click on register link
	_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//verify page
	_assertVisible($REGISTER_HEADING);
	
	for(var $i=0;$i<$Account_Validation_createaccount.length;$i++)
		{
			if($i==10)
				{
					createAccount();
					//verify whether account got created or not
			 	   var $exp=$userData[$user][1]+" "+$userData[$user][2];
			 	   _assertEqual($exp,_extract(_getText(_div("pwheading-one")),"[|] (.*) [(]",true).toString());
			 	   _click($HEADER_USERACCOUNT_LINK);
			 	   _click($MY_ACCOUNT_LOGOUT_LINK);
			 	  _click($LOGIN_CREATEACCOUNTNOW_BUTTON);
				}
			else
				{
				 _setValue($REGISTER_FIRSTNAME_TEXTBOX, $Account_Validation_createaccount[$i][1]);
			 	 _setValue($REGISTER_LASTNAME_TEXTBOX, $Account_Validation_createaccount[$i][2]);
			 	 _setValue($REGISTER_EMAIL_TEXTBOX, $Account_Validation_createaccount[$i][3]);
			 	 _setValue($REGISTER_CONFIRM_EMAIL_TEXTBOX, $Account_Validation_createaccount[$i][4]);
			 	 _setValue($REGISTER_PASSWORD_TEXTBOX, $Account_Validation_createaccount[$i][5]);
			 	 _setValue($REGISTER_CONFIRM_PASSWORD_TEXTBOX, $Account_Validation_createaccount[$i][6]);
			 	 if($i!=1)
			 		 {
			 		 	_click($REGISTER_SUBMIT_BUTTON); 
			 		 }	
			 	 //blank field validation
			 	 if($i==0)
			 		 {
			 		  _assertEqual($Account_Validation_createaccount[0][8], _style($REGISTER_FIRSTNAME_TEXTBOX, "border-color"));
			 		  _assertEqual($Account_Validation_createaccount[0][8], _style($REGISTER_LASTNAME_TEXTBOX, "border-color"));
			 		  _assertEqual($Account_Validation_createaccount[0][8], _style($REGISTER_EMAIL_TEXTBOX, "border-color"));
			 		  _assertEqual($Account_Validation_createaccount[0][8], _style($REGISTER_CONFIRM_EMAIL_TEXTBOX, "border-color"));
			 		  _assertEqual($Account_Validation_createaccount[0][8], _style($REGISTER_PASSWORD_TEXTBOX, "border-color"));
			 	  	  _assertEqual($Account_Validation_createaccount[0][8], _style($REGISTER_CONFIRM_PASSWORD_TEXTBOX, "border-color"));
			 		 }
			 	 //Max characters
			 	 else if($i==1)
			 		 {
			 		_assertEqual($Account_Validation_createaccount[0][9],_getText($REGISTER_FIRSTNAME_TEXTBOX).length);
			 		_assertEqual($Account_Validation_createaccount[1][9],_getText($REGISTER_LASTNAME_TEXTBOX).length);
			 		_assertEqual($Account_Validation_createaccount[2][9],_getText($REGISTER_EMAIL_TEXTBOX).length);
			 		_assertEqual($Account_Validation_createaccount[3][9],_getText($REGISTER_CONFIRM_EMAIL_TEXTBOX).length);
			 		_assertEqual($Account_Validation_createaccount[4][9],_getText($REGISTER_PASSWORD_TEXTBOX).length);
			 		_assertEqual($Account_Validation_createaccount[5][9],_getText($REGISTER_CONFIRM_PASSWORD_TEXTBOX).length);
			 		 }
			 	 //invalid data
			 	 else if($i==2 || $i==3 ||$i==4 || $i==5)
			 		 {
			 		 _assertVisible(_span($Account_Validation_createaccount[$i][7], _in($REGISTER_EMAIL_ERROR)));
			 		 //_assertEqual($Account_Validation_createaccount[0][8], _style($REGISTER_EMAIL_TEXTBOX, "border-color"));	 		  
			 		 _assertVisible(_span($Account_Validation_createaccount[$i][7], _in($REGISTER_EMAIL_CONFIRM_ERROR)));
			 		 //_assertEqual($Account_Validation_createaccount[0][8], _style($REGISTER_CONFIRM_EMAIL_TEXTBOX, "border-color"));
			 		 }	 	 
			 	 //password less than 8 characters
			 	 else if($i==6)
			 		 {
			 		_assertVisible(_div($Account_Validation_createaccount[$i][7]));
			 		_assertEqual($Account_Validation_createaccount[0][8],_style($REGISTER_PASSWORD_TEXTBOX, "border-color"));			 		 
			 		_assertVisible(_div($Account_Validation_createaccount[$i][7], _in($REGISTER_PASSWORD_CONFIRM_ERROR)));
			 		_assertEqual($Account_Validation_createaccount[0][8],_style($REGISTER_CONFIRM_PASSWORD_TEXTBOX, "border-color"));
			 		 }
			 	 else if($i==7 || $i==8 || $i==9)
			 		 {			 		
			 		 	if($i==7)
			 		 		{
			 		 		 _assertEqual($Account_Validation_createaccount[1][8], _style(_span($Account_Validation_createaccount[0][10]), "color"));
			 		 		}
			 		 	if($i==8)
			 		 		{
			 		 		_assertEqual($Account_Validation_createaccount[1][8], _style(_span($Account_Validation_createaccount[2][10]), "color"));
			 		 		}
			 		_assertVisible(_div($Account_Validation_createaccount[$i][7]));
			 		 }
				}
		}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();

var $t = _testcase("124584/148639/148640/148641", "Verify the application behavior on click of create account/Privacy Policy/Secure Shopping/Contact Us link from left navigation pane of create account page");
$t.start();
try
{
	
	//click on register link
	_click(_link("Register"));

	//left nav links	
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
	{
         //Navigate to create account page
		_click($HEADER_LOGIN_LINK);
		_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
		
		//click on left nav links 
		_click($LeftnavlinkNames[$i]);
		
		var $Expected_LeftnavHeading = _div("/"+$LeftNav[$i][8]+"/");
		
		//verify the navigation
		_assertVisible($Expected_LeftnavHeading);
		_assertContainsText($LeftNav[$i][7], $PAGE_BREADCRUMB);
	}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();

