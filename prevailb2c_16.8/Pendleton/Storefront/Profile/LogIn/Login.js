_include("../../../GenericLibrary/GlobalFunctions.js");

var $UserEmail=$uId;
SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);


//######################## Script's Related to Pendleton ############################

var $t = _testcase("148631", "Verify the functionality related of 'Forgot Password' link on My Account login's page Returning customers section in   site as an  Anonymous user");
$t.start();
try
{	
	//navigate back login page
	_click($HEADER_LOGIN_LINK);
	//clicking on forgot password link
	_click($LOGIN_PASSWORD_RESET);
	//verify whether overlay got opened or not
	_assertVisible($LOGIN_DIALOG_CONTAINER);
	_assertVisible($LOGIN_DIALOG_TITLE);
	//_assertEqual($Login_Data[0][5], _getText(_span("ui-dialog-title")));
	_assertVisible($CLOSE_BUTTON);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("288481","Verify the UI of forgot password overlay of the application");
$t.start();
try
{
    //clicking on forgot password link
	_click($LOGIN_PASSWORD_RESET);
	_wait(2000);
	//verify the UI of forgot password overlay
	_assertVisible($LOGIN_RESETPASSWORD_HEADING);	
	_assertVisible($LOGIN_FORGETPASSWORD_OVERLAY_TEXT);
	//email field
	_assertVisible($LOGIN_FORGETPASSWORD_EMAIL_LABEL);
	_assertVisible($LOGIN_FORGETPASSWORD_TEXTBOX);
	_assertVisible($LOGIN_FORGETPASSWORD_SEND_BUTTON);


}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("357905","Verify the functionality of Request to Reset your Password Form Field as an guest user.");
$t.start();
try
{
	
	//validating the email id & password field's
	for(var $i=0;$i<$ForgotPassword_Validation.length;$i++)
		{

			if($i==5)
				{
					_setValue($CHECKOUT_LOGINPAGE_LOGIN_FORGETPASSWORD_TEXTBOX,$ForgotPassword_Validation[$i][1]);
				}
				else
					{
						_setValue($CHECKOUT_LOGINPAGE_LOGIN_FORGETPASSWORD_TEXTBOX,$ForgotPassword_Validation[$i][1]);
						_click($CHECKOUT_LOGINPAGE_FORGETPASSWORD_SEND_BUTTON);
						_wait(2000);
					}
		
		if (isMobile())
			{
			_wait(3000);
			}
		//blank
		if($i==0)
			{
			//username
			_assertEqual($ForgotPassword_Validation[$i][4],_style($CHECKOUT_LOGINPAGE_LOGIN_FORGETPASSWORD_TEXTBOX,"border-color"));
			_assertVisible(_span($ForgotPassword_Validation[$i][3]));
			}
		
		//in valid email
		else if($i==1 || $i==2 || $i==3 || $i==4 ||$i==6 ||$i==7 ||$i==8)
			{
			_assertEqual($ForgotPassword_Validation[3][3], _getText($CHECKOUT_LOGINPAGE_FORGETPASSWORD_EMAIL_ERRORMESSAGE));
			}
		
			
		//More Number of characters in email field
		else if ($i==5)
			{
			_assertEqual($ForgotPassword_Validation[5][2],_getText($CHECKOUT_LOGINPAGE_LOGIN_FORGETPASSWORD_TEXTBOX).length);
			}
		//valid
		else
			{
			if (isMobile())
				{
			_wait(3000,_isVisible($MY_ACCOUNT_OPTIONS));
				}
			//verify the navigation
			_assertVisible(_div("Thank You!"));
			_assertVisible(_paragraph("We've sent you an email with a link to reset your password. It might take a few minutes to reach your inbox. Please check your junk mail or spam if you don�t receive it."));
			_assertVisible(_link("Start Shopping"));
			//closing the dialog
			_click($CHECKOUT_LOGINPAGE_FORGETPASSWORD_CANCEL_BUTTON);
			_wait(2000);
			_assertNotVisible($CHECKOUT_LOGINPAGE_PASSWORDRESET_DIALOG);
			}
		}
	
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//######################## Script's Related to Prevail ############################

var $t = _testcase("124604", "Verify the navigation to My Account login page in application as an  Anonymous user");
$t.start();
try
{
	//click on login link
	_click($HEADER_LOGIN_LINK);
	//verify the navigation
	_assertVisible($LOGIN_HEADING);
	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124605/124606/148627", "Verify UI of 'Returning customers section'/'New customers section'/'My account login' on My account login page");
$t.start();
try
{
	//Verifying the UI of returning customer section
	//MyAccount login as heading
	_assertVisible($LOGIN_HEADING);
	//Returning customer heading
	_assertVisible($LOGIN_RETURNINGNCUSTOMER_HEADING);
	//Paragraph with static text
	_assertVisible($LOGIN_RETURNUS_TEXT);
	//email Address
	_assertVisible($LOGIN_EMAIL_ADDRESS_TEXT);
	_assertVisible($LOGIN_EMAIL_ADDRESS_TEXTBOX);
	_assertEqual("", _getValue($LOGIN_EMAIL_ADDRESS_TEXTBOX));
	//password
	_assertVisible($LOGIN_PASSWORD_TEXT);
	_assertVisible($LOGIN_PASSWORD_TEXTBOX);
	_assertEqual("", _getValue($LOGIN_PASSWORD_TEXTBOX));
	//remember me
	_assertVisible($LOGIN_REMEMBERMETEXT);
	_assertVisible($LOGIN_REMEMBEMETEXTBOX);
	_assertNotTrue($LOGIN_REMEMBEMETEXTBOX.checked);
	//login button
	_assertVisible($LOGIN_SUBMIT_BUTTON);
	//forgot password
	_assertVisible($LOGIN_PASSWORD_RESET);
	_assertEqual($Login_Data[4][0], _getText($LOGIN_PASSWORD_RESET));
	//social links
	_assertVisible($LOGIN_SOCIALLINKS);
	_assertVisible($LOGIN_ENTIRE_LOGIN_SECTION);
	
	//verify the UI of New Customer section
	_assertVisible(_heading2($Login_Data[0][1]));
	//paragraph
	_assertVisible($LOGIN_NEW_CUSTOMER_PARAGRAPHTEXT);
	//create an account button
	_assertVisible($LOGIN_CREATEACCOUNTNOW_BUTTON);
	//content assert
	_assertVisible($CONTENT_ASSET);
	_assertVisible($LOGIN_STATICTEXT1);
	_assertVisible($LOGIN_STATICTEXT2);
	_assertVisible($LOGIN_STATICTEXT3);
	_assertVisible($LOGIN_STATICTEXT4);
	_assertVisible($LOGIN_READMOREABOUTSECURITY_LINK, _in(_paragraph($Login_Data[3][8])));

	//read more about security link
	_assertVisible($LOGIN_READMOREABOUTSECURITY_LINK);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
 

var $t = _testcase("124608", "Verify the application behavior on click of contents displayde in breadcrumb of my account login page of the application");
$t.start();
try
{

	//Navigate to home page
	_click($HEADER_HOME_LINK);
	//verify the navigation to home page
	_assertVisible($HOMEPAGE_HERO_IMAGE);
	//click on link login
	_click($HEADER_LOGIN_LINK);
	//click on my account bread crumb link 
	_click($MY_ACCOUNT_LINK_BREADCRUMB);
	//verfiy the navigation
	_assertVisible($LOGIN_HEADING);
	_assertVisible($LOGIN_SECTION);
	_assertVisible($LOGIN_NEW_CUSTOMER_SECTION);

}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();	

var $t = _testcase("124624", "Verify the navigation related to 'Create an account now' button My Account login page, New customers  section  in application as an  Anonymous user");
$t.start();
try
{

//login link header	
_click($HEADER_LOGIN_LINK);	
//click on create an account button
_click($LOGIN_CREATEACCOUNTNOW_BUTTON);
//verify the navigation
_assertVisible($REGISTER_HEADING);
_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
_assertVisible($REGISTER_SUBMIT_BUTTON);
	
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124615","Verify the functionality related of 'LOGIN' button My Account login's page, Returning customers section in    site as an  Anonymous user");
$t.start();
try
{
	
//navigate back to my account page
_click($HEADER_LOGIN_LINK);	
//login to the application
login();
_assertVisible($MY_ACCOUNT_LOGOUT_LINK);
//click on link not
_click($MY_ACCOUNT_LOGOUT_LINK);
//verify the functionality
_click($HEADER_USERACCOUNT_LINK);
_assertVisible($HEADER_LOGIN_LINK);
_click($HEADER_LOGIN_LINK);
//verify my account breadcrumb
_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
//enter invalid data
_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$Login_Data[0][6]);
_setValue($LOGIN_PASSWORD_TEXTBOX,$Login_Data[1][6]);
_click($LOGIN_SUBMIT_BUTTON);
//verify the error message
_assertVisible($LOGIN_ERROR_MESSGAESECTION);
_assertEqual($Login_Data[2][6], _getText($LOGIN_ERROR_MESSGAESECTION));
	
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124610/148636","Verify the validation related to the 'Email address'/'password' field on My Account login page, Returning customers section in application as a Anonymous user");
$t.start();
try
{
	
//validating the email id & password field's
for(var $i=0;$i<$Login_Validation.length;$i++)
	{
	
	if($i==9)
		{
			login();
		}
		else if($i==5)
			{
				_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$Login_Validation[$i][1]);
				_setValue($LOGIN_PASSWORD_TEXTBOX,$Login_Validation[$i][2]);
			}
			else
				{
				_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$Login_Validation[$i][1]);
				_setValue($LOGIN_PASSWORD_TEXTBOX,$Login_Validation[$i][2]);
				_click($LOGIN_SUBMIT_BUTTON);
				}
	
	if (isMobile())
		{
		_wait(3000);
		}
	//blank
	if($i==0)
		{
		//username
		_assertEqual($Login_Validation[$i][5],_style($LOGIN_EMAIL_ADDRESS_TEXTBOX,"border-color"));
		_assertVisible(_span($Login_Validation[$i][3]));
		//password
		_assertVisible(_span($Login_Validation[$i][4]));
		_assertEqual($Login_Validation[$i][5],_style($LOGIN_PASSWORD_TEXTBOX,"border-color"));
		}
	
	//in valid email
	else if($i==1 || $i==2 || $i==3 ||$i==6 ||$i==7 ||$i==8)
		{
		_assertEqual($Login_Validation[3][4], _getText($LOGIN_ERROR_MESSGAESECTION));
		}
	
	//invalid 
	else if($i==4)
		{
		_assertVisible(_div($Login_Validation[1][3]));
		}
	
	//More Number of characters in email field
	else if ($i==5)
		{
		_assertEqual($Login_Validation[5][3],_getText($LOGIN_EMAIL_ADDRESS_TEXTBOX).length);
		}
	//valid
	else
		{
		if (isMobile())
			{
		_wait(3000,_isVisible($MY_ACCOUNT_OPTIONS));
			}
		_assertVisible($MY_ACCOUNT_LOGOUT_LINK);
		_assertVisible(_heading1("/"+$Login_Data[1][3]+"/"));
		}
	}
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


/*var $t = _testcase("148628/148632", "Verify the navigation related to links in left navigation pane on my account login page of the application");
$t.start();
try
{

	//click on link login
	_click($HEADER_LOGIN_LINK);
	
	//left nav links	
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
	{
		//navigate back login page
		_click($HEADER_LOGIN_LINK);
		
		//click on left nav links 
		_click($LeftnavlinkNames[$i]);
		
		var $Expected_LeftnavHeading = _div("/"+$LeftNav[$i][8]+"/");
		
		//verify the navigation
		_assertVisible($Expected_LeftnavHeading);
		_assertContainsText($LeftNav[$i][7], $PAGE_BREADCRUMB);
	}
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("148633","Verify the navigation related to  'read more about security' social link on My Account login page, New customers  section  in application as an  Anonymous user");
$t.start();
try
{
	//navigate back to my account page
	_click($HEADER_LOGIN_LINK);	
	//click on read more about security link
	_click($LOGIN_READMOREABOUTSECURITY_LINK);
	//verify the navigation
	_assertVisible($SECURITYPOLICY_PAGE_BREADCRUMBLINK);
	_assertContainsText($Login_Data[2][1], $PAGE_BREADCRUMB);
	_assertVisible(_heading1($SECURITYPOLICY_PAGE_HEADING));
	_assertVisible($CONTENT_ASSET);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();*/



cleanup();