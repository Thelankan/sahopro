_include("../../../GenericLibrary/GlobalFunctions.js");
_resource("MyAcc_Home.xls");
var $MyAcc_Home=_readExcelFile("MyAcc_Home.xls","MyAcc_Home");

SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);
//login to the application
_click($HEADER_LOGIN_LINK);
login();

//******
var $t = _testcase("367295/288531", "Verify the UI  of 'My Account home page/ UI of left navigation pane  in my account landing page of the application as a  Registered user");
$t.start();
try
{
	//verify the UI of my account home page
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
	//logged in or not
	_assertVisible($HEADER_USERACCOUNT_LINK);
	//account landing section
	for(var $i=0;$i<4;$i++)
		{
		//personal data
		_assertVisible(_div($MyAcc_Home[$i][1]));
		}
	//left nav section
	var $Leftnavlinks=HeadingCountInLeftNav();
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	//UI of  leftnavlinks
	for(var $i=0;$i<$Leftnavlinks.length;$i++)
		{
		
		if ($i==4)
		{
			var $Expected_Leftnavlink = _div($LeftNav[$i][1]);
			_assertVisible($Expected_Leftnavlink);	
		}
		else
			{
			var $Expected_Leftnavlink = _span($LeftNav[$i][1]);
			_assertVisible($Expected_Leftnavlink);	
			}
		
			
		}
	
	//UI of leftnavlinks Names
	for(var $i=0;$i<$LeftnavlinkNames.length-1;$i++)
		{
		    var $Expected_LeftnavlinkNames = _link($LeftNav[$i][2]);
			_assertVisible($Expected_LeftnavlinkNames);			
		}
	
	_assertVisible($LEFTNAV_NEEDHELP_HEADING);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//*****
var $t = _testcase("367301 ", "Verify the application behavior on click of links in breadcrumb of my account landing page of the application");
$t.start();
try
{
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($MyAcc_Home[0][0], _getText($PAGE_BREADCRUMB));

	//Navigation related to my account
	_click($PAGE_BREADCRUMB_LINK);
	_assertVisible($MY_ACCOUNT_OPTIONS);


}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//****
var $t = _testcase("367296 /367297/367298/148654/148655", "Verify the navigation related to  'Personal Data'/orders/address/wishlist/payment settings/gift registry menu on my account home page in application");
$t.start();
try
{
//navigate to my account home page
_click($HEADER_USERACCOUNT_LINK);	
_click($HEADER_MYACCOUNT_LINK);
//fetch account-options links
var $account_options=_collectAttributes("_div","/pwheading-two acc-land/","sahiText",_in(_list("account-options")));
for(var $i=0;$i<$account_options.length;$i++)
	{
	//navigate to my account home page
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_MYACCOUNT_LINK);
	//clicking on account-option links
	_click(_div($account_options[$i]));
	//verify the navigation
	_assertVisible(_div($MyAcc_Home[$i][8]));
	_assertVisible(_link($MyAcc_Home[$i][9], _in($PAGE_BREADCRUMB)));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("367300", "Verify the navigation related to links in left navigation pane on my account landing page of the application");
$t.start();
try
{
	
//navigate to my account home page
_click($HEADER_USERACCOUNT_LINK);	
_click($HEADER_MYACCOUNT_LINK);

//left nav links	
var $LeftnavlinkNames=LinkCountInLeftNav();

for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
{
	 //navigate to my account home page
_click($HEADER_USERACCOUNT_LINK);
_click($HEADER_MYACCOUNT_LINK);

//click on left nav links 
_click($LeftnavlinkNames[$i]);

var $Expected_LeftnavHeading = _div("/"+$LeftNav[$i][4]+"/");

//verify the navigation
	_assertVisible($Expected_LeftnavHeading);
	_assertContainsText($LeftNav[$i][3], $PAGE_BREADCRUMB);
}
		
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//*****
var $t = _testcase("367298", "Verify the functionality related to 'Logout' link on My Account Home page in application");
$t.start();
try
{
	//click on link log out
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_MYACCOUNT_LINK);
	
	_assertVisible($MY_ACCOUNT_OPTIONS);
	
	//click on logout link
	_click($MY_ACCOUNT_LOGOUT_LINK);
	//account options should not visible
	_assertNotVisible($MY_ACCOUNT_OPTIONS);
	
	//navigate to login page
	_assertVisible($LOGIN_SECTION);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



cleanup();