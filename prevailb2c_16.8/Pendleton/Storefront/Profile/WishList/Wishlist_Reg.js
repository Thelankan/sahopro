_include("../../../GenericLibrary/GlobalFunctions.js");;


SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//**
var $t=_testcase("367358","Verify Wishlist functionality for a Logged in user");
$t.start();
try
{

//login to the application
_click($HEADER_LOGIN_LINK);
login();

//navigate to wish list page
_click($HEADER_WISHLIST_LINK);
if (_isVisible($WISHLIST_SHIPPINGADDRESS_DROPDOWN))
	{
		_setSelected($WISHLIST_SHIPPINGADDRESS_DROPDOWN,0);
	}

//deleting if any addresses are present
deleteAddress();


//navigate to PDP page by searching product
search($Wishlist_Reg[0][0]);

$PDP_pName=_getText($PDP_PRODUCTNAME);
selectSwatch();
_click($WISHLIST_ADDTOWISHLIST_LINK);
_click($HEADER_WISHLIST_LINK);
if(_isVisible($WISHLIST_MAKETHISPUBLIC_BUTTON))
{
	_click($WISHLIST_MAKETHISPUBLIC_BUTTON);
}

//logout 
_click($HEADER_LOGOUT_LINK);
login();

}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("367363","Verify the display of wish list shipping address drop down in wish list page as a registered user having no saved address");
$t.start();
try
{

//navigate to wish list page
_click($HEADER_WISHLIST_LINK);
//Address Dropdown should not present if there are no addresses
_assertNotVisible($WISHLIST_SHIPPINGADDRESS_DROPDOWN);

}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


//create an address 
_click($LEFTNAV_ADDRESS_LINK);

var $address=new Array();
for(var $i=0;$i<$Wislist_ValidAddress.length;$i++)
	{
		_wait(2000);
		//create new address
		_click($ADDRESSES_CREATENEW_ADDRESS_LINK);
		_wait(3000);
		//adding address
		addAddress($Wislist_ValidAddress,$i);
		_click($ADDRESSES_OVERLAY_APPLYBUTTON);
		//$address[$i]="("+$Wislist_ValidAddress[$i][1]+")"+" "+$Wislist_ValidAddress[$i][4]+", "+$Wislist_ValidAddress[$i][8]+", "+$Wislist_ValidAddress[$i][11]+", "+$Wislist_ValidAddress[$i][9];
		$address[$i]="("+$Wislist_ValidAddress[$i][1]+")"+" "+$Wislist_ValidAddress[$i][4]+" "+$Wislist_ValidAddress[$i][8]+" "+$Wislist_ValidAddress[$i][11]+" "+$Wislist_ValidAddress[$i][9];
	}
	
//verifying the presence of addresses
var $boolean=_isVisible($ADDRESSES_ADDRESSES_LIST);

//**
var $t=_testcase("367386/289204/367371/367379","Verify the UI of empty Wish List search' page in application as an registered user/ UI of wish list page of the application as a registered user not having products in wish list");
$t.start();
try
{	
	//navigate to wish list page
	_click($HEADER_WISHLIST_LINK);
	//Clear wish list
	while(_isVisible($WISHLIST_DELETE_LINK))
		{
		_click($WISHLIST_DELETE_LINK);
		}
	//verify the UI of empty Wish List search
	emptyWishlistUI();
	_assertVisible($WISHLIST_EMPTYWISHLIST_HEADING);
	//start adding items
	_assertVisible($WISHLIST_ADDINGITEM_LINK);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124545/128724/367383","Verify the functionality of 'Click here to start adding items.' link when no items are added to the wish list/Verify Wishlist functionality for a Logged in user");
$t.start();
try
{
	
//add products to wish list by clicking on link Click here to start adding items.
_click($WISHLIST_ADDINGITEM_LINK);
//verify the navigation to home page
_assertVisible($HOMEPAGE_BANNER_IMAGE);
//add items
search($Wishlist_Reg[0][0]);
//select swatches
selectSwatch();
//prodname in PDP
var $productName=_getText(_heading1("product-name"));
//click on link add to wish list
_click($WISHLIST_ADDTOWISHLIST_LINK);
//navigate to wish list detail page
_assertVisible($WISHLIST_FINDSOMEONES_WISHLIST);
	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("289203/367381/289689/367364/367384/367353/367391/367357","verify UI of wish list page of the application as a registered user having products in wish list/Verify the UI of wish list details page of the application as a registered user/UI of wish list details section in wish list details page of the application as a registered user/ UI of wish list search results page of the application as a registered user");
$t.start();
try
{
	
	//navigate to search result page
	navigateToFindSomeOneWishlist();
	
	_assertVisible($WISHLIST_FINDSOMEONES_WISHLIST);
	_assertVisible($WISHLIST_LINK_BREADCRUMB);
	_assertVisible($WISHLIST_SEARCHRESULTPAGE_FINDSOMEONE_HEADING);
	var $expText=$LName+" "+$FName;
	_assertVisible(_row("/"+$expText+"/"));
	//last names
	_assertVisible($WISHLIST_SEARCH_LASTNAME_TEXT);
	_assertEqual($LName, _getText($WISHLIST_SEARCH_LASTNAME_TEXT));
	//first name
	_assertVisible($WISHLIST_SEARCH_FIRSTNAME_TEXT);
	_assertEqual($FName, _getText($WISHLIST_SEARCH_FIRSTNAME_TEXT));
	_assertVisible($WISHLIST_SEARCH_CITY_TEXT);
	_assertVisible($WISHLIST_SEARCH_VIEW_TEXT);
	
	//click on view link 
	while(_isVisible($WISHLIST_VIEW_LINK))
	{
	 _click($WISHLIST_VIEW_LINK);
	}

	//with products in wishlist
	_assertVisible($WISHLIST_LASTNAME_TEXTBOX);
	_assertEqual("", _getValue($WISHLIST_LASTNAME_TEXTBOX));
	_assertVisible($WISHLIST_FIRSTNAME_TEXTBOX);
	_assertEqual("", _getValue($WISHLIST_FIRSTNAME_TEXTBOX));
	_assertVisible($WISHLIST_EMAIL_TEXTBOX);
	_assertEqual("", _getValue($WISHLIST_EMAIL_TEXTBOX));
	//textbox labels
	_assertVisible($WISHLIST_LASTNAME_LABEL);
	_assertVisible($WISHLIST_FIRSTNAME_LABEL);
	_assertVisible($WISHLIST_EMAIL_LABEL);
	//product ID
	_assertVisible($WISHLIST_SKU_SECTION);
	_assertVisible($WISHLIST_PRODUCTID_SKU);
	//product Price
	_assertVisible($WISHLIST_PRICE);
	//image
	_assertVisible($WISHLIST_PRODUCTIMAGE);
	//name 
	_assertVisible($WISHLIST_PRODUCTNAME);
	_assertEqual($productName, _getText($WISHLIST_PRODUCTNAME));
	//product list item
	_assertVisible($WISHLIST_PRODUCT_LISTITEM);
	//edit details link
	_assertVisible($WISHLIST_EDIT_DETAILS);
	//availability
	_assertVisible($WISHLIST_INSTOCK);
	//date added
	_assertVisible($WISHLIST_DATEADDED_TEXT);
	//quantity
	_assertVisible($WISHLIST_QUNATITYDESIRED_TEXT);
	//_assertVisible(_div("/QTY:/"));
	//priority
	_log("Should check regarding priority drop down");
//	_assertVisible($WISHLIST_PRIORITY_TEXT);
//	_assertVisible($WISHLIST_PRIORITY_DROPDOWN);
	//update
	_assertVisible($WISHLIST_UPDATE_BUTTON);
	//remove
	_assertVisible($WISHLIST_REMOVE_BUTTON);
	//quantity
	_assertVisible($WISHLIST_QUANTITY_TEXT);
	_assertVisible($WISHLIST_QUANTITY_TEXTBOX);
	//add to cart
	_assertVisible($WISHLIST_ADDTOCART_BUTTON);
	
	//UI of left nav
	var $Leftnavlinks=HeadingCountInLeftNav();
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	//UI of  leftnavlinks
	for(var $i=0;$i<$Leftnavlinks.length;$i++)
		{
			var $Expected_Leftnavlink = _span($LeftNav[$i][1]);
			
			_assertVisible($Expected_Leftnavlink);			
		}
	
	//UI of leftnavlinks Names
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
		{
		    var $Expected_LeftnavlinkNames = _link($LeftNav[$i][2]);
		    
			_assertVisible($Expected_LeftnavlinkNames);			
		}
	
		
	}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


//**
var $t=_testcase("367365","Verify the functionality related to Wish List Shipping Address dropdown on wish list page of the application as a registered user");
$t.start();
try
{
//Your Wish List Shipping Address drop down
if($boolean)
	{
		//shipping address drop down 
		_assertVisible($WISHLIST_SHIPPINGADDRESS_DROPDOWN);
		//select any address from drop down
		var $length=_getText($WISHLIST_SHIPPINGADDRESS_DROPDOWN).length;
		var $j=0;
		_log($j);
		for(var $i=0;$i<$length;$i++)
			{
				_setSelected($WISHLIST_SHIPPINGADDRESS_DROPDOWN,$i);
				if($i==0)
					{
						_assertEqual($Wishlist_Login[11][1], _getSelectedText($WISHLIST_SHIPPINGADDRESS_DROPDOWN));
					}
				else
					{
						_log($j);
						//_assertEqual($address[$i], _getSelectedText($WISHLIST_SHIPPINGADDRESS_DROPDOWN));
						$address="("+$Wislist_ValidAddress[$j][1]+")"+" "+$Wislist_ValidAddress[$j][4]+" "+$Wislist_ValidAddress[$j][8]+" "+$Wislist_ValidAddress[$j][11]+" "+$Wislist_ValidAddress[$j][9];
						_assertEqual($address,_getSelectedText($WISHLIST_SHIPPINGADDRESS_DROPDOWN));
						$j++;
					}
			}
	}
else
	{
		_assert(false,"addresses are not present");
	}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("367369/367375/367360","Verify the application behavior on click of breadcrumb components in wish list search results page of the application as a registered user");
$t.start();
try
{
	
//Navigate to home page
_click($HEADER_HOME_LINK);

//verify the navigation to home page
_assertVisible($HOMEPAGE_BANNER_IMAGE);
//navigating to wish list login page
_click($HEADER_WISHLIST_LINK);
_assertEqual($Wishlist_Login[13][1], _getText($PAGE_BREADCRUMB));
//click on my account link present in the bread crumb
_click($MY_ACCOUNT_LINK_BREADCRUMB);
//verify the navigation
_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
_assertContainsText("My Account", _div("pwheading-one"));
	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


//**
var $t=_testcase("367366/367352","Verify the navigation of 'WISH LIST' link at the header as a logged-in user/application behavior on click of product name in wish list page of the applicaiton as a registered user");
$t.start();
try
{
	
	//navigating to wish list login page
	_click($HEADER_WISHLIST_LINK);
	//fetch the total no.of products
	var $count=WishlistProductCount();
	
	for(var $i=0;$i<$count;$i++)
		{
			var $productName=_getText(_link("/(.*)/",_in(_div("name ["+$i+"]"))));
			//click on name link
			_click(_link("/(.*)/",_in(_div("name ["+$i+"]"))));
			//verify the navigation
			_assertVisible(_span($productName, _in($PAGE_BREADCRUMB)));
			_assertEqual($productName, _getText($PDP_PRODUCTNAME));
			//navigating to wish list login page
			_click($HEADER_WISHLIST_LINK);
		}
}

catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124547/289649/367356/367380","Verify the application behavior on click of edit details link for a product in wish list page of the applicaiton as a registered user/edit details functionality for a product in wish list page of the application as a registered user");
$t.start();
try
{
	//navigating to wish list login page
	_click($HEADER_WISHLIST_LINK);
	var $productName=_getText($WISHLIST_PRODUCTNAME);
	//click on edit details link under wishlist product
	_click($WISHLIST_EDIT_DETAILS);
	//verify the navigation to respective PDP
	_assertVisible(_div("QuickViewDialog"));
	

}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("367382/367350/367362/367354/367348/367385/367384/367387/367355/367361/367346/367347","Verify the validation related to the 'Last name',First name,email id field/'Find' button functionality on wish list login page, find someone's wish list section in application as a registered user");
$t.start();
try
{
	//navigating to wish list login page
	_click($HEADER_WISHLIST_LINK);
	//validating the fields last name,first name,email 
	for(var $i=0;$i<$Validations.length;$i++)
	  {
		_log($i);
		if($i==6)
			{
				_setValue($WISHLIST_LASTNAME_TEXTBOX, $LName);
				_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $FName);
				_setValue($WISHLIST_EMAIL_TEXTBOX,$uId);
			}
		else
			{
				_setValue($WISHLIST_LASTNAME_TEXTBOX, $Validations[$i][1]);
				_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $Validations[$i][2]);
				_setValue($WISHLIST_EMAIL_TEXTBOX, $Validations[$i][3]);
			}
		//Blank
		if($i==0 || $i==2 || $i==3 || $i==4 || $i==5 || $i==6)
		{
			//click on find button
			_click($WISHLIST_FIND_BUTTON);
			//user should be in same page
			_assertVisible($WISHLIST_SEARCHRESULTPAGE_FINDSOMEONE_HEADING);
			//verify the text message
			if($i==6)
				{
				var $expText=$LName+" "+$FName;
				_assertVisible(_row("/"+$expText+"/"));
				_assertVisible($WISHLIST_VIEW_LINK);
				}
			else
				{
				_assertVisible($WISHLIST_NOWISHLIST_PARARAPH);
				}
		}
		//max characters
		else if($i==1)
			{
			_assertEqual($Validations[1][4],_getText($WISHLIST_LASTNAME_TEXTBOX).length);
			_assertEqual($Validations[1][4],_getText($WISHLIST_FIRSTNAME_TEXTBOX).length);
			_assertEqual($Validations[1][4],_getText($WISHLIST_EMAIL_TEXTBOX).length);
			}	
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("367349/289168/289205/289122/367359","Verify the UI of left navigation pane in wish list search results page of the application as a registered user");
$t.start();
try
{
	//left nav links	
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
	{
		//navigate to search result page
		navigateToFindSomeOneWishlist();
		
		//click on left nav links 
		_click($LeftnavlinkNames[$i]);
		
		var $Expected_LeftnavHeading = _div("/"+$LeftNav[$i][4]+"/");
		
		//verify the navigation
		_assertVisible($Expected_LeftnavHeading);
		_assertContainsText($LeftNav[$i][3], $PAGE_BREADCRUMB);
	}

}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("367387/367374/367376","Verify the functionality related to the 'Make This List Private' button on Wishlist page, find someone's wish list section in application as an registered user.");
$t.start();
try
{
	_click($HEADER_WISHLIST_LINK);
	
	//_click($WISHLIST_VIEW_LINK);
	
	//verify the 'Make This List Private' functionality
	if(_isVisible($WISHLIST_MAKETHISPRIVATE_BUTTON))
		{
		_click($WISHLIST_MAKETHISPRIVATE_BUTTON);
		//_click($WISHLIST_MAKETHISPRIVATE_BUTTON);
		}
	//'Make This List public' link should be there
	_assertVisible($WISHLIST_MAKETHISPUBLIC_BUTTON);
	_assertNotVisible($WISHLIST_MAKETHISPUBLIC_TEXT);
	
	//items should not be visible
	_setValue($WISHLIST_EMAIL_TEXTBOX,$Wishlist_Login[3][4]);
	_click($WISHLIST_FIND_BUTTON  );
	//wish list results table should not display for invalid credentials
	var $expText=$LName+" "+$FName;
	_assertEqual(false,_isVisible(_row("/"+$expText+"/"))); 
	_assertNotVisible($WISHLIST_VIEW_LINK);
	_assertVisible($WISHLIST_NOWISHLIST_PARARAPH);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("367368","Verify the functionality related to the 'Make This List public' button on Wishlist page, find someone's wish list section in application as an registered user.");
$t.start();
try
{
	_click($HEADER_WISHLIST_LINK);
	//_click($WISHLIST_VIEW_LINK);
	//verify the 'Make This List Public' functionality
	if (_isVisible($WISHLIST_MAKETHISPUBLIC_BUTTON))
		{
		_click($WISHLIST_MAKETHISPUBLIC_BUTTON);
		}
	//'Make This List private' link should be there
	_assertVisible($WISHLIST_MAKETHISPRIVATE_BUTTON);
	_assertVisible($WISHLIST_MAKETHISPUBLIC_TEXT);
	//items should be visible
	_setValue($WISHLIST_EMAIL_TEXTBOX,$uId);
	_click($WISHLIST_FIND_BUTTON  );
	//wish list results table should not display for invalid credentials
	var $expText=$LName+" "+$FName;
	_assertVisible(_row("/"+$expText+"/"));
	_assertVisible($WISHLIST_VIEW_LINK);
	_assertNotVisible($WISHLIST_NOWISHLIST_PARARAPH);  
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("367372","Verify the application behavior on click of update link in Wish list page of the application as a registered  user");
$t.start();
try
{
	//navigating to wish list login page
	_click($HEADER_WISHLIST_LINK);
	
	//_click($WISHLIST_VIEW_LINK);
	//making list as public
	if(_isVisible($WISHLIST_MAKETHISPUBLIC_BUTTON))
		{
		_click($WISHLIST_MAKETHISPUBLIC_BUTTON);
		}
	//incresing the quantity desired
	_setValue(_numberbox("dwfrm_wishlist_items_i"+$Wishlist_Reg[0][9]+"_quantity"),$Wishlist_Reg[0][3]);
	//setting priority to high
	_setSelected($WISHLIST_PRIORITY_DROPDOWN,$Wishlist_Reg[1][3]);
	//un check the public check box
	_uncheck($WISHLIST_MAKETHISPUBLIC_CHECKBOX);
	//click on update link
	_click($WISHLIST_UPDATE_BUTTON);
	//verify the functionality
	_assertEqual($Wishlist_Reg[0][3],_getText(_numberbox("dwfrm_wishlist_items_i"+$Wishlist_Reg[0][9]+"_quantity")));
	_assertEqual($Wishlist_Reg[1][3],_getSelectedText($WISHLIST_PRIORITY_DROPDOWN));
	_assertNotTrue($WISHLIST_MAKETHISPUBLIC_CHECKBOX.checked);	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("367367/367373","Verify the functionality related to the 'ADD TO CART' button wish list search result page, find someone's wish list section in application as a Registered user");
$t.start();
try
{
	//fetch the total no.of products
	var $count=WishlistProductCount();
	if($count!=0)
		{
		for(var $i=0;$i<$count;$i++)
			{
			var $productName=_getText(_link("/(.*)/",_in(_div("name ["+$i+"]"))));	
			//var $cartQuantity=_extract(_getText($MINICART_TOTAL),"[(](.*)[)]",true);
			var $cartQuantity=_getText($MINICART_TOTAL);
			var $quanitytInWishlist=parseInt(_getText($WISHLIST_QUANTITY_TEXTBOX));
			//verify the functionality of add to cart button
			//_click(_submit("dwfrm_wishlist_items_i"+$i+"_addItemToCart"));  //dwfrm_wishlist_items_i0_addToCart
			
			_click(_submit("dwfrm_wishlist_items_i"+$i+"_addToCart"));
			//navigate to cart
			_click($MINICART_VIEWCART_LINK);
			//Page should navigate to cart page
			_assertVisible($CART_TABLE);
			var $prodNameInCart=_getText($WISHLIST_PRODUCTNAMEIN_CART);
			$cartQuantity=parseInt($cartQuantity)+$quanitytInWishlist;
			//_assertEqual(parseInt($cartQuantity),parseInt(_extract(_getText($MINICART_TOTAL),"[(](.*)[)]",true)));
			_assertEqual(parseInt($cartQuantity),_getText($MINICART_TOTAL));
			
			}
		}
	else
		{
		_assert(false,"no items are there in wish list");
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

cleanup();
//**
var $t=_testcase("367377","Verify the validations for quantity field in wish list page of the application as a registered user");
$t.start();
try
{

//navigating to wish list login page
_click($HEADER_WISHLIST_LINK);	

//validations for quantity field
for(var $i=0;$i<$qunatityvalidations.length;$i++)
	{
	_setValue($WISHLIST_QUANTITY_TEXTBOX,$qunatityvalidations[$i][1]);
	_click($WISHLIST_ADDTOCART_BUTTON);
	
	if ($i==0 || $i==1 || $i==2)
		{
		_assert(false,"application issue");
//		_assertEqual($qunatityvalidations[0][2],_getText($WISHLIST_QUANTITY_ERRORMESSAGE));
//		_assertEqual($qunatityvalidations[0][3],_style($WISHLIST_QUANTITY_TEXTBOX,"background-color"));
//		_assertNotVisible($MINICART_QUANTITY);
//		_assertEqual("",_getText($WISHLIST_QUANTITY_TEXTBOX))
		}
	else
		{
			_assertEqual($qunatityvalidations[3][1],_getText($MINICART_QUANTITY));
		}
	}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


//**
var $t=_testcase("367351","Verify the functionality of make this item pulic checkbox displayed for a prodcut in wish list page of the application");
$t.start();
try
{
//navigating to wish list login page
_click($HEADER_WISHLIST_LINK);	
var $productName_before_public=_getText($WISHLIST_PRODUCTNAME);	
//un check the check box 
_uncheck($WISHLIST_MAKETHISPUBLIC_CHECKBOX);
//click on update button
_click($WISHLIST_UPDATE_BUTTON);
//logout from the application
logout();
//click on wish list link from footer
_click($HEADER_WISHLIST_LINK);
//enter valid values in text boxes
_setValue($WISHLIST_LASTNAME_TEXTBOX, $LName);
_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $FName);
_setValue($WISHLIST_EMAIL_TEXTBOX,$uId);
//click on submit button
_click($WISHLIST_FIND_BUTTON);
//click on view link in search result page 
_click($WISHLIST_VIEW_LINK);
//
if (_isVisible($WISHLIST_PRODUCTNAME))
	{
		var $productName_after_public=_getText($WISHLIST_PRODUCTNAME);
		//product name should not match
		_assertNotEquals($productName_before_public,$productName_after_public);
	}
else
	{
		_assertVisible($WISHLIST_NOWISHLIST_PARARAPH);
	}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124550","Verify the application behavior on click of remove link for a product in Wish list page of the application as a registered user");
$t.start();
try
{
	//Navigate to WL page
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_WISHLIST_LINK);
	
	//fetch the total no.of products	
	var $count=WishlistProductCount();
	if($count==1)
		{
		_click($WISHLIST_DELETE_LINK);
		//no products should be present 
		_assertVisible(_link($Wishlist_Reg[0][8]));
		_assertNotVisible($WISHLIST_REMOVE_BUTTON);
		}
	else
		{
		for(var $i=0;$i<$count;$i++)
			{	
			var $productName=_getText($WISHLIST_PRODUCTNAME);
			_click($WISHLIST_DELETE_LINK);
			//verify whether first item is removed or not
			_assertNotVisible(_link($productName,_in($WISHLIST_PRODUCTNAME_DIV)));
			}
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();



cleanup();