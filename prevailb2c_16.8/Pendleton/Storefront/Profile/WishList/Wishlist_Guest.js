_include("../../../GenericLibrary/GlobalFunctions.js");


SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//adding items to wish list for particular account
//additemsToWishListAccount($Wishlist_Reg[0][0]);

//**
var $t = _testcase("124818/124426", "Verify the UI of 'Returning customers' section wishlist login page in application as an Anonymous user" +
		  "Verify the navigation of 'WISH LIST' link at the header as a guest user.");
$t.start();
try
{
	
//navigating to wish list login  page
//_click($FOOTER_WISHLIST_LINK);
_navigateTo($URL[4][0]);
//verify bread crumb
_assertVisible($WISHLIST_LINK_BREADCRUMB);
//Verifying the UI of returning customer section
//MyAccount login as heading
_assertVisible($WISHLIST_LOGIN_HEADING);
//Returning customer heading
_assertVisible($WISHLIST_LOGIN_RETURNINGNCUSTOMER_HEADING);
//Paragraph with static text
_assertVisible($WISHLIST_LOGIN_RETURNUS_TEXT);
//email Address
_assertVisible($WISHLIST_LOGIN_EMAIL_ADDRESS_TEXT);
_assertVisible($WISHLIST_LOGIN_EMAIL_ADDRESS_TEXTBOX);
_assertEqual("", _getValue($WISHLIST_LOGIN_EMAIL_ADDRESS_TEXTBOX));
//password
_assertVisible($WISHLIST_LOGIN_PASSWORD_TEXT);
_assertVisible($WISHLIST_LOGIN_PASSWORD_TEXTBOX);
_assertEqual("", _getValue($WISHLIST_LOGIN_PASSWORD_TEXTBOX));
//remember me
_assertVisible($WISHLIST_LOGIN_REMEMBERMETEXT);
_assertVisible($WISHLIST_LOGIN_REMEMBEMETEXTBOX);
_assertNotTrue($WISHLIST_LOGIN_REMEMBEMETEXTBOX.checked);
//login button
_assertVisible($WISHLIST_LOGIN_SUBMIT_BUTTON);
//forgot password
_assertVisible($WISHLIST_FORGETPASSWORD_LINK);
_assertEqual($Wishlist_Login[8][0], _getText($WISHLIST_FORGETPASSWORD_LINK));
//social links
_assertVisible($WISHLIST_ENTIRE_LOGIN_SECTION);
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//**
var $t=_testcase("124806","Verify the UI of 'Create a wishlist' section on wishlist login page in application as an anonymous user");
$t.start();
try
{
	
//navigating to wish list login page
//_click($FOOTER_WISHLIST_LINK);
_navigateTo($URL[4][0]);
//verify the UI of create wish list section
_assertVisible($WISHLIST_CREATEWISHLIST_HEADING);
_assertVisible($CONTENT_ASSET);
_assertVisible($WISHLIST_CREATEACCOUNTNOW_BUTTON);
	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("148695","Verify the UI of  'Find Someone's Wish List'  section on wishlist login page in application as an anonymous user");
$t.start();
try
{
	
//verify the UI of 'Find Someone's Wish List'
_assertVisible($WISHLIST_LOGINPAGE_FINDSOMEONE_HEADING);
_assertVisible($WISHLIST_TEXT_PARAGRAPH);
//last name
_assertVisible($WISHLIST_LASTNAME_LABEL);
_assertVisible($WISHLIST_LASTNAME_TEXTBOX);
//first name
_assertVisible($WISHLIST_FIRSTNAME_LABEL);
_assertVisible($WISHLIST_FIRSTNAME_TEXTBOX);
//email 
_assertVisible($WISHLIST_EMAIL_LABEL);
_assertVisible($WISHLIST_EMAIL_TEXTBOX);
//find button
_assertVisible($WISHLIST_FIND_BUTTON);
	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124428","Verify the applicaiton behavior on click of components in breadcrumb of wish list login page of the application");
$t.start();
try
{
	
//Navigate to home page
_click($HEADER_HOME_LINK);
//verify the navigation to home page
_assertVisible($HOMEPAGE_BANNER_IMAGE);

//navigating to wish list login page
//_click($FOOTER_WISHLIST_LINK);
_navigateTo($URL[4][0]);
_assertEqual($Wishlist_Login[13][1], _getText($PAGE_BREADCRUMB));

//click on my account link present in the bread crumb
_click($MY_ACCOUNT_LINK_BREADCRUMB);
//verify the navigation
_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
_assertVisible(_div("My Account Sign In"));
	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("148696/148701","Verify the navigation related to 'Forgot Password' link on on Wishlist login page returning customer section in application as an anonymous user/Verify the forgot password functionality in wishlist login page of the application");
$t.start();
try
{
	
//navigate to wish list page
//_click($FOOTER_WISHLIST_LINK);
_navigateTo($URL[4][0]);
//Checking Bread crumb
_assertVisible($WISHLIST_LINK_BREADCRUMB);
//wish list page
_assertVisible($WISHLIST_LOGIN_HEADING);

//clicking on forgot password link
_click($WISHLIST_FORGETPASSWORD_LINK);
_wait(2000);

//verify the UI of forgot password overlay
_assertVisible($WISHLIST_LOGIN_RESETPASSWORD_HEADING);	
_assertVisible($WISHLIST_FORGETPASSWORD_OVERLAY_TEXT);
//email field
_assertVisible($WISHLIST_FORGETPASSWORD_TEXTBOX);
//enter valid id in text box
_setValue($WISHLIST_FORGETPASSWORD_TEXTBOX,$Wishlist_Login[2][4]);
_click($WISHLIST_FORGETPASSWORD_SEND_BUTTON);

//confirmation text
_assertVisible($WISHLIST_LOGIN_RESETYOURPASSWORD_TEXT);
_assertVisible($WISHLIST_LOGIN_FORGETPASSWORD_CLOSE_BUTTON);

//closing the dialog
_click($WISHLIST_LOGIN_FORGETPASSWORD_CLOSE_BUTTON);
	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124504","Verify the UI of 'Forgot Password' overlay  on wishlist login page in application as an  Anonymous user");
$t.start();
try
{
	
//navigate to wish list page
//_click($FOOTER_WISHLIST_LINK);
_navigateTo($URL[4][0]);
//click on forgot password link
_click($WISHLIST_FORGETPASSWORD_LINK);
//verify the overlay appearance
_assertVisible($WISHLIST_RESETPASSWORD_DIALOG_TITLE);
_assertVisible($WISHLIST_LOGIN_RESETPASSWORD_HEADING);
_assertVisible($WISHLIST_FORGETPASSWORD_TEXTBOX);
_assertVisible($WISHLIST_FORGETPASSWORD_SEND_BUTTON);
	

}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124505","Verify the navigation related to 'Create an account now' button on Wishlist login page create a wishlist section  in application as an anonymous user");
$t.start();
try
{
	
//_click($FOOTER_WISHLIST_LINK);
_navigateTo($URL[4][0]);
//click on link Create an account now button
_click($WISHLIST_CREATEACCOUNTNOW_BUTTON);
//verify the navigation
_assertVisible($REGISTER_HEADING);
_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("148703/148704/124506/148705","Verify the validation related to the 'Last name',First name,email id field/'Find' button functionality on wish list login page, find someone's wish list section in application as a anonymous user");
$t.start();
try
{
	
	//validating the fields last name,first name,email 
	for(var $i=0;$i<$Validations.length;$i++)
	  {
		//_click($FOOTER_WISHLIST_LINK);
		_navigateTo($URL[4][0]);
		_wait(3000);
		if($i==6)
		{
			_setValue($WISHLIST_LASTNAME_TEXTBOX, $LName);
			_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $FName);
			_setValue($WISHLIST_EMAIL_TEXTBOX,$uId);
		}
		else
		{
			_setValue($WISHLIST_LASTNAME_TEXTBOX, $Validations[$i][1]);
			_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $Validations[$i][2]);
			_setValue($WISHLIST_EMAIL_TEXTBOX, $Validations[$i][3]);
		}	
		//Blank
		if($i==0 || $i==2 || $i==3  || $i==6)
		{
			//click on find button
			_click($WISHLIST_FIND_BUTTON);
			//user should be in same page
			_assertVisible($WISHLIST_SEARCHRESULTPAGE_FINDSOMEONE_HEADING);
			//verify the text message
			
		if($i==6)
			{
			var $expText=$LName+" "+$FName+" - "+"View";
			_assertVisible(_row($expText));
			_assertVisible($WISHLIST_VIEW_LINK);
			}
		else
			{
			_assertVisible($WISHLIST_NOWISHLIST_PARARAPH);
			}
		}
		if($i==4 || $i==5 )
		{
			_assertVisible(_div($Validations[6][7]));
			_assertEqual($Validations[1][5], _style($WISHLIST_EMAIL_TEXTBOX,"border-color"));
		}
		
		//max characters
		if($i==1)
			{
			_assertEqual($Validations[1][4],_getText($WISHLIST_LASTNAME_TEXTBOX).length);
			_assertEqual($Validations[1][4],_getText($WISHLIST_FIRSTNAME_TEXTBOX).length);
			_assertEqual($Validations[1][4],_getText($WISHLIST_EMAIL_TEXTBOX).length);
			}
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124430/124431/148702","Verify the validation related to the 'Email address'/'password' field/'LOGIN' button functionality on Wish list login page, Returning customers section in application as a Anonymous user");
$t.start();
try
{
	_click($HEADER_HOME_LINK);
	//_click($FOOTER_WISHLIST_LINK);
	_navigateTo($URL[4][0]);
	//validating the email address and password fields	
	for(var $i=0;$i<$WishListLogin_Validation.length;$i++)
		{
		if($i==5)
			{
				login();
			}
		else
			{
		_setValue($WISHLIST_LOGIN_EMAIL_ADDRESS_TEXTBOX,$WishListLogin_Validation[$i][1]);
		_setValue($WISHLIST_LOGIN_PASSWORD_TEXTBOX,$WishListLogin_Validation[$i][2]);
		_click($WISHLIST_LOGIN_SUBMIT_BUTTON);
			}
		//blank
		if($i==0)
			{
			//username
			_assertEqual($WishListLogin_Validation[$i][5],_style($WISHLIST_LOGIN_EMAIL_ADDRESS_TEXTBOX,"border-color"));
			_assertVisible(_span($WishListLogin_Validation[$i][3]));
			//password
			_assertVisible(_span($WishListLogin_Validation[$i][4]));
			_assertEqual($WishListLogin_Validation[$i][5],_style($WISHLIST_LOGIN_PASSWORD_TEXTBOX,"border-color"));
			}
		//valid
		else if($i==5)
			{
			_assertVisible($WISHLIST_SEARCHRESULTPAGE_FINDSOMEONE_HEADING);
			}
		//invalid email id
		else if($i==1||$i==2||$i==3)
		{
			//username
			_assertEqual($WishListLogin_Validation[0][5],_style($WISHLIST_LOGIN_EMAIL_ADDRESS_TEXTBOX,"border-color"));
			_assertVisible(_span($WishListLogin_Validation[2][3]));
		}
		//invalid 
		else
			{
			_assertVisible(_div($WishListLogin_Validation[1][3]));
			}
		}
	//logout from application
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_LOGOUT_LINK);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


//**
var $t=_testcase("124508","Verify the UI of empty Wish List search' page in application as an anonymous user");
$t.start();
try
{
	//navigate to wish list page
	//_click($FOOTER_WISHLIST_LINK);
	_navigateTo($URL[4][0]);
	_setValue($WISHLIST_EMAIL_TEXTBOX, $Wishlist_Login[2][4]);
	//click on find button
	_click($WISHLIST_FIND_BUTTON);
	//verify the UI of empty Wish List search
	_assertVisible($WISHLIST_LINK_BREADCRUMB);
	_assertVisible($WISHLIST_SEARCHRESULTPAGE_FINDSOMEONE_HEADING);
	_assertVisible($WISHLIST_FINDSOMEONESWISLLIST_SECTION);
	_assertVisible($WISHLIST_NOWISHLIST_PARARAPH);

	//fields
	_assertVisible($WISHLIST_LASTNAME_LABEL);
	_assertVisible($WISHLIST_FIRSTNAME_LABEL);
	_assertVisible($WISHLIST_EMAIL_LABEL);
	_assertVisible($WISHLIST_LASTNAME_TEXTBOX);
	_assertEqual("", _getValue($WISHLIST_LASTNAME_TEXTBOX));
	_assertVisible($WISHLIST_FIRSTNAME_TEXTBOX);
	_assertEqual("", _getValue($WISHLIST_FIRSTNAME_TEXTBOX));
	_assertVisible($WISHLIST_EMAIL_TEXTBOX);
	_assertEqual("", _getValue($WISHLIST_EMAIL_TEXTBOX));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();	

//**
var $t=_testcase("124509/289164/289688","Verify the UI of wish list search results page of the application as a guest user/ UI of wish list details page of the application as a guest user");
$t.start();
try
{
	
//navigate to wish list page
//_click($FOOTER_WISHLIST_LINK);
_navigateTo($URL[4][0]);
_setValue($WISHLIST_LASTNAME_TEXTBOX, $LName);
_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $FName);
_setValue($WISHLIST_EMAIL_TEXTBOX,$uId);

//click on find button
_click($WISHLIST_FIND_BUTTON);
//verify the UI of non empty Wish List search
_assertVisible($WISHLIST_LINK_BREADCRUMB);
_assertVisible($WISHLIST_SEARCHRESULTPAGE_FINDSOMEONE_HEADING);
var $expText=$LName+" "+$FName+" - "+"View";
_assertVisible(_row($expText));
//last names
_assertVisible($WISHLIST_SEARCH_LASTNAME_TEXT);
_assertEqual($LName, _getText($WISHLIST_SEARCH_LASTNAME_TEXT));
//first name
_assertVisible($WISHLIST_SEARCH_FIRSTNAME_TEXT);
_assertEqual($FName, _getText($WISHLIST_SEARCH_FIRSTNAME_TEXT));
_assertVisible($WISHLIST_SEARCH_CITY_TEXT);
_assertVisible($WISHLIST_SEARCH_VIEW_TEXT);

//fields
_assertVisible($WISHLIST_LASTNAME_LABEL);
_assertVisible($WISHLIST_FIRSTNAME_LABEL);
_assertVisible($WISHLIST_EMAIL_LABEL);
_assertVisible($WISHLIST_LASTNAME_TEXTBOX);
_assertEqual("", _getValue($WISHLIST_LASTNAME_TEXTBOX));
_assertVisible($WISHLIST_FIRSTNAME_TEXTBOX);
_assertEqual("", _getValue($WISHLIST_FIRSTNAME_TEXTBOX));
_assertVisible($WISHLIST_EMAIL_TEXTBOX);
_assertEqual("", _getValue($WISHLIST_EMAIL_TEXTBOX));

//find
_assertVisible($WISHLIST_FIND_BUTTON);

//textbox labels
_assertVisible($WISHLIST_LASTNAME_LABEL);
_assertVisible($WISHLIST_FIRSTNAME_LABEL);
_assertVisible($WISHLIST_EMAIL_LABEL);
//product ID
_assertVisible($WISHLIST_SKU_SECTION);
_assertVisible($WISHLIST_PRODUCTID_SKU);
//product Price
_assertVisible($WISHLIST_PRICE);
//image
_assertVisible($WISHLIST_PRODUCTIMAGE);
//name 
_assertVisible($WISHLIST_PRODUCTNAME);

	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


////deleting items from wish list
//deleteAddeditemsFromWishlistAccount();


//**************************************************wishlist search result page************************************************
////adding items to wish list for particular account
//additemsToWishListAccount($Wishlist_Reg[0][0]);


//**
var $t=_testcase("148708","Verify the application behavior on click of breadcrumb components in wish list search results page of the application as a guest user");
$t.start();
try
{
	//navigating to find some one's wish list page	
	navigateToFindSomeOneWishlist();
		
	//Navigate to home page
	_click($HEADER_HOME_LINK);
	
	//verify the navigation to home page
	_assertVisible($HOMEPAGE_BANNER_IMAGE);
	//navigating to find some one's wish list page	
	navigateToFindSomeOneWishlist();
	_assertEqual($Wishlist_Login[13][1], _getText($PAGE_BREADCRUMB));
	//click on my account link present in the bread crumb
	_click($MY_ACCOUNT_LINK_BREADCRUMB);
	//verify the navigation
	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


//**
var $t=_testcase("124512/124514","Verify the functionality related to the 'View' link on wish list search result page, find someone's wish list section  in application as a Anonymous user or registered user/navigation related to  product name link  on wish list search result page in application as an Anonymous user");
$t.start();
try
{
	//navigating to find some one's wish list page	
	navigateToFindSomeOneWishlist();
	_wait(3000);
	if(_isVisible($WISHLIST_VIEW_LINK))
		{
		 _click($WISHLIST_VIEW_LINK);
		}
	else
		{
		_assert(false,"entered account is not having any products");
		}
		
	//verify the navigation
	_assertVisible($WISHLIST_ADDTOCART_BUTTON);
	_assertVisible($WISHLIST_ITEM_LIST);
	
	//fetch the total no.of products
	var $count=WishlistProductCount();
	for(var $i=0;$i<$count;$i++)
		{
			var $productName=_getText(_link("/(.*)/",_in(_div("name ["+$i+"]"))));
			//click on name link
			_click(_link("/(.*)/",_in(_div("name ["+$i+"]"))));
			//verify the navigation	
			_assertVisible(_span($productName, _in($PAGE_BREADCRUMB)));
			_assertVisible($PDP_PRODUCTNAME);
			_assertEqual($productName, _getText($PDP_PRODUCTNAME));

		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124522/148711/148712/124511","Verify the validation related to the 'Last name',First name,email id field/'Find' button functionality on wish list search results page of application as a anonymous user");
$t.start();
try
{	
	//validating the fields last name,first name,email 
	for(var $i=0;$i<$Validations.length;$i++)
	  {
		_click($HEADER_HOME_LINK);
		//navigating to wish list search results page
		navigateToFindSomeOneWishlist();
		_wait(3000);
		if($i==6)
			{
				_setValue($WISHLIST_LASTNAME_TEXTBOX, $LName);
				_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $FName);
				_setValue($WISHLIST_EMAIL_TEXTBOX,$uId);
			}
		else
			{
				_setValue($WISHLIST_LASTNAME_TEXTBOX, $Validations[$i][1]);
				_setValue($WISHLIST_FIRSTNAME_TEXTBOX, $Validations[$i][2]);
				_setValue($WISHLIST_EMAIL_TEXTBOX, $Validations[$i][3]);
			}		
		//Blank
		if($i==0 || $i==2 || $i==3 || $i==6)
			{
				//click on find button
				_click($WISHLIST_FIND_BUTTON);
				//user should be in same page
				_assertVisible($WISHLIST_SEARCHRESULTPAGE_FINDSOMEONE_HEADING);
				//verify the text message
				if($i==6)
					{
					var $expText=$LName+" "+$FName+" - "+"View";
					_assertVisible(_row($expText));
					_assertVisible($WISHLIST_VIEW_LINK);
					}
				else
					{
						_assertVisible(_paragraph($Validations[0][7]));
						
					}
			}
		//invalid
		if($i==4 || $i==5 )
		{
			_assertVisible(_div($Validations[6][7]));
			_assertEqual($Validations[1][5], _style($WISHLIST_EMAIL_TEXTBOX, "border-color"));

		}
		
		//max characters
		if($i==1)
			{
			_assertEqual($Validations[1][4],_getText($WISHLIST_LASTNAME_TEXTBOX).length);
			_assertEqual($Validations[1][4],_getText($WISHLIST_FIRSTNAME_TEXTBOX).length);
			_assertEqual($Validations[1][4],_getText($WISHLIST_EMAIL_TEXTBOX).length);
			}
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


//**
var $t=_testcase("124515","Verify the application behavior on click of add to cart button for a product in Wish list details page of the application");
$t.start();
try
{
	
	_wait(2000);
	if(_isVisible($WISHLIST_VIEW_LINK))
	{
	_click($WISHLIST_VIEW_LINK);
	_wait(3000);
	}
else
	{
	_assert(false,"entered account is not having any products");
	}
	//fetch the total no.of products
	var $count=WishlistProductCount();
	if($count!=0)
		{
		for(var $i=0;$i<$count;$i++)
			{
			var $productName=_getText(_link("/(.*)/",_in(_div("name ["+$i+"]"))));	
			//var $cartQuantity=_extract(_getText($MINICART_TOTAL),"[(](.*)[)]",true);
			var $cartQuantity=_getText($MINICART_TOTAL);
			var $quanitytInWishlist=parseInt(_getText($WISHLIST_QUANTITY_TEXTBOX));
			//verify the functionality of add to cart button
			//_click(_submit("dwfrm_wishlist_items_i"+$i+"_addItemToCart"));  //dwfrm_wishlist_items_i0_addToCart
			
			_click(_submit("dwfrm_wishlist_items_i"+$i+"_addToCart"));
			//navigate to cart
			_click($MINICART_VIEWCART_LINK);
			//Page should navigate to cart page
			_assertVisible($CART_TABLE);
			var $prodNameInCart=_getText($WISHLIST_PRODUCTNAMEIN_CART);
			$cartQuantity=parseInt($cartQuantity)+$quanitytInWishlist;
			//_assertEqual(parseInt($cartQuantity),parseInt(_extract(_getText($MINICART_TOTAL),"[(](.*)[)]",true)));
			_assertEqual(parseInt($cartQuantity),_getText($MINICART_TOTAL));
			
			}
		}
	else
		{
		_assert(false,"no items are there in wish list");
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


//deleting items from wishlist
deleteAddeditemsFromWishlistAccount();


