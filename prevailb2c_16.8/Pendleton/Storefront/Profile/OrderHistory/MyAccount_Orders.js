_include("../../../GenericLibrary/GlobalFunctions.js");

var $UserEmail=$uId;

SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);

_log("******************************************************ORDER HISTORY************************************************************");
//login with credentials having no saved orders
_click($HEADER_LOGIN_LINK);
_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$Account[0][0]);
_setValue($LOGIN_PASSWORD_TEXTBOX,$Account[0][1]);
_click($LOGIN_SUBMIT_BUTTON);

//124795
var $t = _testcase("367323", "Verify the UI of Order history page of the application as a registered user when there are no order records created");
$t.start();
try
{
	
	_click($MY_ACCOUNT_ORDERS_OPTIONS);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);

	_assertEqual($Generic_Myaccount_orders[0][0], _getText($PAGE_BREADCRUMB));

	//heading
	_assertVisible($ORDERS_HEADING);
	//static text
	_assertVisible($ORDERS_NOORDERS_HEADING);
	_assertEqual($Generic_Myaccount_orders[2][0], _getText($ORDERS_NOORDERS_HEADING));
	
	//UI of left nav
	var $Leftnavlinks=HeadingCountInLeftNav();
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	//UI of  leftnavlinks
	for(var $i=0;$i<$Leftnavlinks.length;$i++)
		{
			var $Expected_Leftnavlink = _span($LeftNav[$i][1]);
			
			_assertVisible($Expected_Leftnavlink);			
		}
	
	//UI of leftnavlinks Names
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
		{
		    var $Expected_LeftnavlinkNames = _link($LeftNav[$i][2]);
		    
			_assertVisible($Expected_LeftnavlinkNames);			
		}
	
	//logout from application
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_LOGOUT_LINK);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



//login with credentials having saved orders
_click($HEADER_LOGIN_LINK);
login();


//place order
navigateToShippingPage("45350","1");
//shipping details
shippingAddress($Data,3);
_click($SHIPPING_SUBMIT_BUTTON);
_wait(3000);
//verify address verification overlay

//address Verification Overlay
//if(_isVisible($SHIPPING_ADDRESS_VERIFICATION_OVERLAY))
//{
//_click($SHIPPING_ADDRESS_VERIFICATION_OVERLAY_CONTINUEBUTTON);
//}

//Billing details
BillingAddress($Data,3);
//Payment details
PaymentDetails($Valid_Data_orderreview,4);
_click($BILLING_CONTINUE_BUTTON);

//if(_isVisible(_div("address-validation-dialog[1]")))
//{
//
//_click(_submit("Continue[1]"))
//
//}
//Placing order
_wait(3000);
_click($ORDERSUMMARY_CONTINUE_BUTTON);

//fetch the order number from confirmation pae
//var $OrderNumber=_getText($ORDER_CONFIRMATION_ORDER_NUMBER);

//_click($HEADER_USERACCOUNT_LINK);	
//_click($HEADER_CHECKORDER_LINK);
//_click($ORDERS_ORDERDETAIL_BUTTON);

if(isIE11() || _isIE10())
	{
	  var $OrderTotal=_extract(_getText($ORDERS_ORDERDETAIL_ORDERTOTAL), "/Order Total:(.*)/",true).toString();
	}
else
	{
	var $OrderTotal=  _extract(_getText($ORDERS_ORDERDETAIL_ORDERTOTAL), "/Order Total: (.*)/",true).toString();
	}
var $ProductName=_getText($ORDERS_ORDERDETAIL_PRODUCTNAME);
//var $Zipcode=_getText($ADDRESS_MINIADDRESS_LOCATION).split(",")[1].split(" ")[2];
var $OrderStatus=$Generic_Myaccount_orders[0][5];
//var $OrderInfo=_getText($ORDERS_ORDERDETAIL_PAYMENTSECTION).toString();
var $prodInfo=_getText($ORDERS_ORDERDETAIL_LINEITEMDETAILS).toString();
var $ShipmentInfo=_getText($ORDERS_ORDERDETAIL_SHIPPINADDRESS).toString();
var $billingInfo=_getText($ORDERS_ORDERDETAIL_BILLINGSECTION).toString();

//navigate to my account page
_click($HEADER_USERACCOUNT_LINK);	
_click($HEADER_MYACCOUNT_LINK);
	
//***	
var $t = _testcase("367324/367321/367331 ","Verify the UI  of 'My Account Order history page' /UI of left nav when there are multiple order records in application as a  Registered user.");
$t.start();
try
{
	_click($LEFTNAV_ORDERHISTORY_LINK);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);

	_assertEqual($Generic_Myaccount_orders[0][0], _getText($PAGE_BREADCRUMB));

	//heading
	_assertVisible($ORDERS_HEADING);
	//Pagination header
	_assertVisible($ORDERS_PAGINATION);
	//Pagination footer
	_assertVisible($ORDERS_PAGINATION, _under($ORDERS_SEARCHRESULT_ITEMS));
	//Checking the details of the last placed order
	//Order date
	_assertVisible($ORDERS_ORDERDATE);
	//order status
	_assertVisible($ORDERS_ORDERSTATUS);

	_assertEqual("Order Status: "+$OrderStatus, _getText($ORDERS_ORDERSTATUS));

	//Order number
	//_assertVisible($ORDERS_ORDERNUMBER);
	//_assertEqual("Order Number: "+$OrderNumber, _getText($ORDERS_ORDERNUMBER));
	//Order details link
	_assertVisible($ORDERS_ORDERDETAIL_BUTTON);
	//Product name
	_assertEqual($ProductName,_getTableContents($ORDERS_ORDERTABLE,[1],[1]).toString());
	//order total
	_assertEqual($OrderTotal,_getTableContents($ORDERS_ORDERTABLE,[2],[1]).toString());	
	
	//UI of left nav
	var $Leftnavlinks=HeadingCountInLeftNav();
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	//UI of  leftnavlinks
	for(var $i=0;$i<$Leftnavlinks.length;$i++)
		{
			var $Expected_Leftnavlink = _span($LeftNav[$i][1]);
			
			_assertVisible($Expected_Leftnavlink);			
		}
	
	//UI of leftnavlinks Names
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
		{
		    var $Expected_LeftnavlinkNames = _link($LeftNav[$i][2]);
		    
			_assertVisible($Expected_LeftnavlinkNames);			
		}
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//124797
var $t = _testcase("124797","Verify the application behavior on click of contents displayed in the breadcrumb of order history page");
$t.start();
try
{
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Generic_Myaccount_orders[0][0], _getText($PAGE_BREADCRUMB));

	//click on my account breadcrumb
	_click($MY_ACCOUNT_LINK_BREADCRUMB);
	//verify navigation 
	_assertVisible($MY_ACCOUNT_HEADING);
	_assertVisible($MY_ACCOUNT_OPTIONS);
	_assertEqual($Generic_Myaccount_orders[6][0], _getText($PAGE_BREADCRUMB));

}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();




//***
var $t = _testcase("128536","Verify the application behavior on click of product name in order details page of the application as a registered user");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);	
	_click($HEADER_CHECKORDER_LINK);
	//Order details link
	_click($ORDERS_ORDERDETAIL_BUTTON);
	//checking Product name
	_assertVisible($ORDERS_ORDERDETAIL_PRODUCTNAME);
	var $PName=_getText($ORDERS_ORDERDETAIL_PRODUCTNAME);
	_click($ORDERS_ORDERDETAIL_PRODUCTNAME);
	//Validating navigation
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertContainsText($PName,$PAGE_BREADCRUMB);
	//heading
	_assertEqual($PName, _getText($PDP_PRODUCTNAME));	
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//logout from application
_click($HEADER_USERACCOUNT_LINK);
_click($HEADER_LOGOUT_LINK);

//Enter with login credentials having more orders saved
_click($HEADER_LOGIN_LINK);
_setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX,$Account[1][0]);
_setValue($LOGIN_PASSWORD_TEXTBOX,$Account[1][1]);
_click($LOGIN_SUBMIT_BUTTON);

//***
var $t = _testcase("367326","Verify the functionality related to pagination links on order history page of the application as a Registered user having multiple order records");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_CHECKORDER_LINK);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);

	_assertEqual($Generic_Myaccount_orders[0][0], _getText($PAGE_BREADCRUMB));

	//Pagination
	if(_isVisible($SHOPNAV_PAGINATION_LINK))
		{
			pagination();
		}
	else
		{
			_assert(false, "Pagination is not displaying");
		}
	
	//navigate to check order page
	_click($HEADER_USERACCOUNT_LINK);
	_click($HEADER_CHECKORDER_LINK);

}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();




//test case regarding left nav navigation for order history page
//var $t = _testcase("/367319 ","Verify the navigation related to links in left navigation pane on order history page of the application");
//$t.start();
//try
//{
//	
//	
//	//left nav links	
//	var $LeftnavlinkNames=LinkCountInLeftNav();
//	
//	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
//	{
//		//navigate to check order page
//		_click($HEADER_CHECKORDER_LINK);
//		
//		//click on left nav links 
//		_click($LeftnavlinkNames[$i]);
//		
//		var $Expected_LeftnavHeading = _heading1("/"+$LeftNav[$i][4]+"/");
//		
//		//verify the navigation
//		_assertVisible($Expected_LeftnavHeading);
//		_assertContainsText($LeftNav[$i][3], $PAGE_BREADCRUMB);
//	}
//}	
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
$t.end();
_log("******************************************************ORDER DETAIL SECTION************************************************************");
//***
var $t = _testcase("367319 /367332 /367325/288919","Verify the  application behavior on clcick on  'Order Details' button  on  'My Account order history'  page in application as a  Registered user/UI of order details page/UI of order details section/UI of left navigation pane in order details page of the application as a registered user");
$t.start();
try
{
	_click($HEADER_CHECKORDER_LINK);
	//Order details link
	_assertVisible($ORDERS_ORDERDETAIL_BUTTON);
	_click($ORDERS_ORDERDETAIL_BUTTON);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);

	//_assertEqual($Generic_Myaccount_orders[0][0]+" "+$OrderNumber, _getText($PAGE_BREADCRUMB));
	
	//heading
	//_assertVisible(_heading1($Generic_Myaccount_orders[3][0]+" "+$OrderNumber));
	//Validating UI
	_assertVisible($ORDERS_ORDERDETAIL_PAYMENTSECTION);
	//_assertEqual($OrderInfo, _getText($ORDERS_ORDERDETAIL_PAYMENTSECTION));
	_assertVisible($ORDERS_ORDERDETAIL_SHIPPINADDRESS);
	_assertEqual($ShipmentInfo, _getText($ORDERS_ORDERDETAIL_SHIPPINADDRESS));
	_assertVisible($ORDERS_ORDERDETAIL_SHIPPINGSTATUS);
	_assertVisible($ORDERS_ORDERDETAIL_SHIPMENTNO);
	//billing details
	_assertVisible($ORDERS_ORDERDETAIL_BILLINGSECTION);
	_assertEqual($billingInfo,_getText($ORDERS_ORDERDETAIL_BILLINGSECTION));
	//product details
	_assertVisible($ORDERS_ORDERDETAIL_LINEITEMDETAILS);
	_assertEqual($prodInfo, _getText($ORDERS_ORDERDETAIL_LINEITEMDETAILS));
	//links
	_assertVisible($ORDERS_RETURNTO_ORDERHISTORY_LINK);
	_assertVisible($ORDERS_RETURNTOSHOPPING_LINK);
	//left nav
	var $Leftnavlinks=HeadingCountInLeftNav();
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	//UI of  leftnavlinks
	for(var $i=0;$i<$Leftnavlinks.length;$i++)
		{
			var $Expected_Leftnavlink = _span($LeftNav[$i][1]);
			
			_assertVisible($Expected_Leftnavlink);			
		}
	
	//UI of leftnavlinks Names
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
		{
		    var $Expected_LeftnavlinkNames = _span($LeftNav[$i][2]);
		    
			_assertVisible($Expected_LeftnavlinkNames);			
		}
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



//***
var $t = _testcase("367330 ","Verify the navigation related to 'Return to order history'  link on order details page of the application as a registered user");
$t.start();
try
{
	_assertVisible($ORDERS_RETURNTO_ORDERHISTORY_LINK);
	_click($ORDERS_RETURNTO_ORDERHISTORY_LINK);
	//Validating the navigation
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);

	_assertEqual($Generic_Myaccount_orders[0][0], _getText($PAGE_BREADCRUMB));

	//heading
	_assertVisible($ORDERS_HEADING);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//***
var $t = _testcase("367329 ","Verify the navigation related to Return to shopping link on order details page in application as a registered user");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);	
	_click($HEADER_CHECKORDER_LINK);
	//Order details link
	_click($ORDERS_ORDERDETAIL_BUTTON);
	//return to shopping link
	_assertVisible($ORDERS_RETURNTOSHOPPING_LINK);
	_click($ORDERS_RETURNTOSHOPPING_LINK);
	//Validating the navigation
	_assertVisible($HOMEPAGE_SLIDES);
	_assertVisible($HOMEPAGE_SLOTS);
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//***
var $t = _testcase("367322/36732","Verify the application behavior on click of contents in the breadcrumb of order details page of the application");
$t.start();
try
{
	_click($HEADER_USERACCOUNT_LINK);	
	_click($HEADER_CHECKORDER_LINK);
	//Order details link
	_click($ORDERS_ORDERDETAIL_BUTTON);
	//bread crumb
	_assertVisible($PAGE_BREADCRUMB);

	//
	//_assertEqual($Generic_Myaccount_orders[0][0]+" "+$OrderNumber, _getText($PAGE_BREADCRUMB));

	
	//Validating navigation on click of order history link in breadcrumb
	_assertVisible($ORDERS_ORDERHISTORYLINK_BREADCRUMB);
	_click($ORDERS_ORDERHISTORYLINK_BREADCRUMB);
	_assertEqual($Generic_Myaccount_orders[0][0], _getText($PAGE_BREADCRUMB));
	_assertVisible($ORDERS_HEADING);	
	
	//navigate to order detail page
	_click($HEADER_USERACCOUNT_LINK);	
	_click($HEADER_CHECKORDER_LINK);
	_click($ORDERS_ORDERDETAIL_BUTTON);
	
	//Validating navigation on click of My account link in breadcrumb
	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
	_click($MY_ACCOUNT_LINK_BREADCRUMB);
	_assertVisible($MY_ACCOUNT_HEADING);
	_assertEqual($Generic_Myaccount_orders[6][0], _getText($PAGE_BREADCRUMB));

}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//logout from application
_click($HEADER_USERACCOUNT_LINK);
_click($HEADER_LOGOUT_LINK);

//
//
//var $t = _testcase("367326 /367332 ","Verify the UI of order details page/ UI of left navigation pane/ UI of shipment details section  in order details page of the application as a guest user");
//$t.start();
//try
//{
//	_click($HEADER_HOMEIMAGE);
//	_click($FOOTER_CHECKORDER_LINK);	
//	
//	//navigating to order summary
//	CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode);
//	
//	//bread crumb
//	_assertVisible($PAGE_BREADCRUMB);
//	_assertEqual($Generic_Myaccount_orders[0][0]+" "+$OrderNumber, _getText($PAGE_BREADCRUMB));
//	
//	//heading
//	_assertVisible(_heading1($Generic_Myaccount_orders[3][0]+" "+$OrderNumber));
//	//billing details
//	_assertVisible($ORDERS_ORDERDETAIL_BILLINGSECTION);
//	_assertEqual($billingInfo,_getText($ORDERS_ORDERDETAIL_BILLINGSECTION));
//	
//	//UI of shipment details section
//	_assertVisible($ORDERS_ORDERDETAIL_PAYMENTSECTION);
//	_assertEqual($OrderInfo, _getText($ORDERS_ORDERDETAIL_PAYMENTSECTION));
//	_assertVisible($ORDERS_ORDERDETAIL_SHIPPINADDRESS);
//	_assertEqual($ShipmentInfo, _getText($ORDERS_ORDERDETAIL_SHIPPINADDRESS));
//	_assertVisible($ORDERS_ORDERDETAIL_SHIPPINGSTATUS);
//	_assertVisible($ORDERS_ORDERDETAIL_SHIPMENTNO);
//
//	//product details
//	_assertVisible($ORDERS_ORDERDETAIL_LINEITEMDETAILS);
//	_assertEqual($prodInfo, _getText($ORDERS_ORDERDETAIL_LINEITEMDETAILS));
//	//links
//	_assertNotVisible($ORDERS_RETURNTO_ORDERHISTORY_LINK);
//	_assertVisible($ORDERS_RETURNTOSHOPPING_LINK);
//	
//	//left nav for guest user
//	var $Leftnavlinks=HeadingCountInLeftNav();
//	var $LeftnavlinkNames=LinkCountInLeftNav();
//	
//	//UI of  leftnavlinks
//	for(var $i=0;$i<$Leftnavlinks.length;$i++)
//		{
//			var $Expected_Leftnavlink = _span($LeftNav[$i][5]);
//			
//			_assertVisible($Expected_Leftnavlink);			
//		}
//	
//	//UI of leftnavlinks Names
//	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
//		{
//		    var $Expected_LeftnavlinkNames = _span($LeftNav[$i][6]);
//		    
//			_assertVisible($Expected_LeftnavlinkNames);			
//		}
//}	
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();
//
////***
//var $t = _testcase("367328","Verify the application behavior on click of product name in order details page of the application as a guest user");
//$t.start();
//try
//{
//	//homepage
//	_click($HEADER_HOMEIMAGE);
//	//navigate to Login page 
//	_click($HEADER_LOGIN_LINK);
//	
//	//navigating to order summary
//	CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode);
//
//	//checking Product name
//	_assertVisible($ORDERS_ORDERDETAIL_PRODUCTNAME);
//	var $PName=_getText($ORDERS_ORDERDETAIL_PRODUCTNAME);
//	_click($ORDERS_ORDERDETAIL_PRODUCTNAME);
//	//Validating navigation
//	//bread crumb
//	_assertVisible($PAGE_BREADCRUMB);
//	_assertContainsText($PName,$PAGE_BREADCRUMB);
//	//heading
//	_assertEqual($PName, _getText($PDP_PRODUCTNAME));	
//}	
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();



//***
//var $t = _testcase("367325","Verify the application behavior on click of contents in the breadcrumb of order details page of the application as guest user");
//$t.start();
//try
//{
//
//	//navigate to Login page 
//	_click($HEADER_LOGIN_LINK);
//	//navigating to order summary
//	CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode);
//	//bread crumb
//	_assertVisible($PAGE_BREADCRUMB);
//	_assertEqual($Generic_Myaccount_orders[0][0]+" "+$OrderNumber, _getText($PAGE_BREADCRUMB));
//
//	//heading
//	_assertVisible(_heading1($Generic_Myaccount_orders[3][0]+" "+$OrderNumber));
//	
//	//Validating navigation on click of order history link in breadcrumb
//	_assertVisible($ORDERS_ORDERHISTORYLINK_BREADCRUMB);
//	_click($ORDERS_ORDERHISTORYLINK_BREADCRUMB);
//	_assertEqual($Generic_Myaccount_orders[6][0], _getText($PAGE_BREADCRUMB));
//	_assertVisible($MY_ACCOUNT_HEADING);
//	_assertVisible($LOGIN_SECTION);
//
//	//Enter order summary
//	CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode);
//
//	//Validating navigation on click of My account link in breadcrumb
//	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
//	_click($MY_ACCOUNT_LINK_BREADCRUMB);
//	_assertVisible($MY_ACCOUNT_HEADING);
//	_assertEqual($Generic_Myaccount_orders[6][0], _getText($PAGE_BREADCRUMB));
//	_assertVisible($LOGIN_SECTION);
//
//}	
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();


//***
//var $t = _testcase("367327","Verify the navigation related to Return to shopping link on order details page in application as an anonymous user");
//$t.start();
//try
//{
//	//navigate to Login page 
//	_click($HEADER_LOGIN_LINK);
//	//navigating to order summary
//	CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode);
//	//return to shopping link
//	_assertVisible($ORDERS_RETURNTOSHOPPING_LINK);
//	_click($ORDERS_RETURNTOSHOPPING_LINK);
//	//Validating the navigation
//	_assertVisible($HOMEPAGE_SLIDES);
//	_assertVisible($HOMEPAGE_SLOTS);
//}	
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();
//
////navigating to order summary
//_click($HEADER_LOGIN_LINK);
//CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode);

//var $t = _testcase("288927", "Verify the navigation related to links in left navigation pane of order details page as a guest user");
//$t.start();
//try
//{
//
//	
//	//left nav links	
//	var $LeftnavlinkNames=LinkCountInLeftNav();
//	
//	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
//	{
//          //Navigate to login page
//		_click($HEADER_LOGIN_LINK);
//		
//		//click on left nav links 
//		_click($LeftnavlinkNames[$i]);
//		
//		var $Expected_LeftnavHeading = _heading1("/"+$LeftNav[$i][4]+"/");
//		
//		//verify the navigation
//		_assertVisible($Expected_LeftnavHeading);
//		_assertContainsText($LeftNav[$i][3], $PAGE_BREADCRUMB);
//	}
//		
//	//navigate to check order detail page
//	_click($HEADER_CHECKORDER_LINK);
//	_click($ORDERS_ORDERDETAIL_BUTTON);
//}	
//catch($e)
//{
//	_logExceptionAsFailure($e);
//}	
//$t.end();

//login with credentials having saved orders
_click($HEADER_LOGIN_LINK);
login();

//test case regarding left nav navigation order details section
var $t = _testcase("288929 ","Verify the navigation related to links in left navigation pane of order details page as registered user");
$t.start();
try
{
	
	//Order details link
	//left nav links	
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
	{
          //Navigate to check order detail page
		_click($HEADER_CHECKORDER_LINK);
		_click($ORDERS_ORDERDETAIL_BUTTON)
		
		//click on left nav links 
		_click($LeftnavlinkNames[$i]);
		
		var $Expected_LeftnavHeading = _heading1("/"+$LeftNav[$i][8]+"/");
		
		//verify the navigation
		_assertVisible($Expected_LeftnavHeading);
		_assertContainsText($LeftNav[$i][7], $PAGE_BREADCRUMB);
	}

}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();





