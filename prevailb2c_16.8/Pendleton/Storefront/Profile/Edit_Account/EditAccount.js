_include("../../../GenericLibrary/GlobalFunctions.js");

var $color=$NameEmail_Validation[0][7];
var $color1=$NameEmail_Validation[1][7];
//from util.GenericLibrary/BrowserSpecific.js
var $FirstName=$FName.toUpperCase();
var $LastName=$LName.toUpperCase();
_log($FirstName);
_log($LastName);


SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);
//login to the application
_click($HEADER_LOGIN_LINK);
login();

////navigate to my account home page
_click($HEADER_USERACCOUNT_LINK);
_click($HEADER_MYACCOUNT_LINK);
_click($LEFTNAV_EDITINFORMATION_LINK);

var $t = _testcase("124641/124639/264284/264685/124760/288371", "Verify the validation related to First name/Last name/Email/ Confirm Email/ Password fields and  functionality related to 'Apply' button of change name / email section in Edit Account page of the application as a Registered user");
$t.start();
try 
{
	for(var $i=0;$i<$NameEmail_Validation.length;$i++)
		{
		//edit account link
			_click($LEFTNAV_EDITINFORMATION_LINK);
			if($i==0)
				  {
				  	 _setValue($EDITACCOUNT_FIRSTNAME_TEXTBOX, "");
				 	 _setValue($EDITACCOUNT_LASTNAME_TEXTBOX, "");
				 	 _setValue($EDITACCOUNT_EMAIL_TEXTBOX, "");
				  }
			 _setValue($EDITACCOUNT_FIRSTNAME_TEXTBOX, $NameEmail_Validation[$i][1]);
		 	 _setValue($EDITACCOUNT_LASTNAME_TEXTBOX, $NameEmail_Validation[$i][2]);
		 	 _setValue($EDITACCOUNT_EMAIL_TEXTBOX, $NameEmail_Validation[$i][3]);
		 	 _setValue($EDITACCOUNT_CONFIRM_EMAIL_TEXTBOX, $NameEmail_Validation[$i][4]);
		 	
		 	 if($i==0 ||$i==1 || $i==6|| $i==7)
		 		 {
		 		 	_setValue($EDITACCOUNT_PASSWORD_TEXTBOX, $NameEmail_Validation[$i][5]);
		 		 }
		 	 else
		 		 {
		 		 	_setValue($EDITACCOUNT_PASSWORD_TEXTBOX, $pwd);
		 		 }
		 	 _wait(2000);
		 	if($i!=1)
		 		 {
		 		 	_click($EDITACCOUNT_SUBMIT_BUTTON); 
		 		 	_wait(2000);
		 		 }	
		 	//blank field validation
		 	 if($i==0)
			 	 {	
		 		 	 //first name
		 		 	  _assertVisible(_span($NameEmail_Validation[0][6],  _near($EDITACCOUNT_FIRSTNAME)));
			 		  _assertEqual($color, _style($EDITACCOUNT_FIRSTNAME_TEXTBOX, "border-color"));
		 		      _assertEqual($color1, _style(_span($NameEmail_Validation[0][6], _near($EDITACCOUNT_FIRSTNAME)), "color"));
		 		      //last name
		 		 	  _assertVisible(_span($NameEmail_Validation[0][6], _near($EDITACCOUNT_LASTNAME)));
			 		  _assertEqual($color, _style($EDITACCOUNT_LASTNAME_TEXTBOX, "border-color"));
			 		  _assertEqual($color1, _style(_span($NameEmail_Validation[0][6], _near($EDITACCOUNT_LASTNAME)), "color"));
			 		  //email
			 		  _assertVisible(_span($NameEmail_Validation[0][6], _near($EDITACCOUNT_EMAIL)));
			 		  _assertEqual($color, _style($EDITACCOUNT_EMAIL_TEXTBOX, "border-color"));
			 		  _assertEqual($color1, _style(_span($NameEmail_Validation[0][6], _near($EDITACCOUNT_EMAIL)), "color"));
			 		  //confirm email
			 		  _assertVisible(_span($NameEmail_Validation[0][6], _near($EDITACCOUNT_CONFIRM_EMAIL)));
			 		  _assertEqual($color, _style($EDITACCOUNT_CONFIRM_EMAIL_TEXTBOX, "border-color"));
			 		  _assertEqual($color1, _style(_span($NameEmail_Validation[0][6], _near($EDITACCOUNT_CONFIRM_EMAIL)), "color"));
			 		  //password
			 		  _assertVisible(_span($NameEmail_Validation[0][6], _near($EDITACCOUNT_PASSWORD)));
			 		  _assertEqual($color, _style($EDITACCOUNT_PASSWORD_TEXTBOX, "border-color"));
			 		  _assertEqual($color1, _style(_span($NameEmail_Validation[0][6], _near($EDITACCOUNT_PASSWORD)), "color"));			 		  
			 	 }
		 	 //Max characters
		 	 else if($i==1)
		 		 {
			 		_assertEqual($NameEmail_Validation[0][8],_getText($EDITACCOUNT_FIRSTNAME_TEXTBOX).length);
			 		_assertEqual($NameEmail_Validation[1][8],_getText($EDITACCOUNT_LASTNAME_TEXTBOX).length);
			 		_assertEqual($NameEmail_Validation[2][8],_getText($EDITACCOUNT_EMAIL_TEXTBOX).length);
			 		_assertEqual($NameEmail_Validation[3][8],_getText($EDITACCOUNT_CONFIRM_EMAIL_TEXTBOX).length);
			 		_assertEqual($NameEmail_Validation[4][8],_getText($EDITACCOUNT_PASSWORD_TEXTBOX).length);
		 		 }
		 	 //Invalid data
		 	 else if($i==2 || $i==3 ||$i==4 )
		 		 {
			 		 _assertVisible(_span($NameEmail_Validation[$i][6], _near($EDITACCOUNT_EMAIL)));
			 		 _assertEqual($color1, _style($EDITACCOUNT_EMAIL_TEXTBOX, "color")); 		  
			 		 _assertVisible(_span($NameEmail_Validation[$i][6], _near($EDITACCOUNT_CONFIRM_EMAIL)));
			 		 _assertEqual($color1, _style($EDITACCOUNT_CONFIRM_EMAIL_TEXTBOX, "color"));
		 		 }
		 	//Different email/confirm email
		 	 else if($i==5)
		 		 {
		 		 	_assertVisible(_div($NameEmail_Validation[$i][6], _near($EDITACCOUNT_CONFIRM_EMAIL)));
		 		 }		 	 
		 	//password less than 8 characters
		 	 else if($i==6)
		 		 {
		 		 	_assertVisible(_span($NameEmail_Validation[$i][6], _near($EDITACCOUNT_PASSWORD)));
		 		 	_assertEqual($color1, _style($EDITACCOUNT_PASSWORD_TEXTBOX, "color"));
		 		 }
		 	 //Invalid password
			 else if( $i==7)
		 		 {			 		
				 	_assertEqual($color1, _style($EDITACCOUNT_PASSWORD, "color"));
		 		 	_assertVisible(_div($NameEmail_Validation[$i][6], _near($EDITACCOUNT_PASSWORD)));
		 		 }
		 	 //Valid
			 else
				 {
					 _assertVisible($HEADER_USERACCOUNT_LINK);
					 _assertVisible(_div($Generic_Editaccount[5][0]+" "+$NameEmail_Validation[$i][1]+" "+$NameEmail_Validation[$i][2]+" "+$Generic_Editaccount[6][0]));
					 _assertVisible($MY_ACCOUNT_LOGOUT_LINK);
					 //verification in edit account page
					 _click($LEFTNAV_EDITINFORMATION_LINK);
					 //Fname
					 _assertEqual($NameEmail_Validation[$i][1], _getValue($EDITACCOUNT_FIRSTNAME_TEXTBOX));
					 //Lname
 					 _assertEqual($NameEmail_Validation[$i][2], _getValue($EDITACCOUNT_LASTNAME_TEXTBOX));
					 //email
					 _assertEqual($NameEmail_Validation[$i][3], _getValue($EDITACCOUNT_EMAIL_TEXTBOX));
					 //resetting the data					 
					 _setValue($EDITACCOUNT_FIRSTNAME_TEXTBOX, $FirstName);
				 	 _setValue($EDITACCOUNT_LASTNAME_TEXTBOX, $LastName);
				 	 _setValue($EDITACCOUNT_EMAIL_TEXTBOX, $uId);
				 	 _setValue($EDITACCOUNT_CONFIRM_EMAIL_TEXTBOX, $uId);
				 	 _setValue($EDITACCOUNT_PASSWORD_TEXTBOX, $pwd);
			 		 _click($EDITACCOUNT_SUBMIT_BUTTON); 
				 	_wait(3000);
				 	_assertVisible($HEADER_USERACCOUNT_LINK);
				 	_assertVisible(_div($Generic_Editaccount[5][0]+" "+$FirstName+" "+$LastName+" "+$Generic_Editaccount[6][0]));
					_assertVisible($MY_ACCOUNT_LOGOUT_LINK);
				 }
		}
}
catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end();
	
var $t = _testcase("124643", "Verify the UI  of edit account page of the application as a registered user");
$t.start();
try
{

	//click personal data link
	_click(_div("Personal Data"));
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual("My Account", _getText(_link("breadcrumb-element")));
	//heading
	_assertVisible($EDITACCOUNT_EDITACCOUNT_HEADING);
	//Name and email section
	_assertVisible(_div("Edit Account"));
	//Fname
	_assertVisible($EDITACCOUNT_FIRSTNAME);
	_assertVisible($EDITACCOUNT_FIRSTNAME_TEXTBOX);
	_assertEqual($FirstName, _getValue($EDITACCOUNT_FIRSTNAME_TEXTBOX));
	//Lname
	_assertVisible($EDITACCOUNT_LASTNAME);
	_assertVisible($EDITACCOUNT_LASTNAME_TEXTBOX);
	_assertEqual($LastName, _getValue($EDITACCOUNT_LASTNAME_TEXTBOX));
	//email
	_assertVisible($EDITACCOUNT_EMAIL);
	_assertVisible($EDITACCOUNT_EMAIL_TEXTBOX);
	_assertEqual($uId, _getValue($EDITACCOUNT_EMAIL_TEXTBOX));
	//Confirm email
	_assertVisible($EDITACCOUNT_CONFIRM_EMAIL);
	_assertVisible($EDITACCOUNT_CONFIRM_EMAIL_TEXTBOX);
	_assertEqual("", _getValue($EDITACCOUNT_CONFIRM_EMAIL_TEXTBOX));
	//Password
	_assertVisible($EDITACCOUNT_PASSWORD);
	_assertVisible($EDITACCOUNT_PASSWORD_TEXTBOX);
   _assertVisible(_div($Generic_Editaccount[3][0]));
	//Checkbox
	_assertVisible($EDITACCOUNT_CHECKBOX);
	//Privacy policy
	_assertVisible(_link("See Privacy Policy"));
	//Apply button
	_assertVisible($EDITACCOUNT_SUBMIT_BUTTON);
	//Current password
	_assertVisible($EDITACCOUNT_CURRENTPASSWORD);
	_assertVisible($EDITACCOUNT_CURRENTPASSWORD_TEXTBOX);
	_assertEqual("", _getValue($EDITACCOUNT_CURRENTPASSWORD_TEXTBOX));
	//new password
	_assertVisible($EDITACCOUNT_NEWPASSWORD);
	_assertVisible($EDITACCOUNT_NEWPASSWORD_TEXTBOX);
	_assertEqual("", _getValue($EDITACCOUNT_NEWPASSWORD_TEXTBOX));
	//8-255 characters
	_assertVisible(_div($Generic_Editaccount[3][0],  _near($EDITACCOUNT_PASSWORD)));
	//Confirm password
	_assertVisible($EDITACCOUNT_CONFIRMPASSWORD);
	_assertVisible($EDITACCOUNT_CONFIRMPASSWORD_TEXTBOX);
	_assertEqual("", _getValue($EDITACCOUNT_CONFIRMPASSWORD_TEXTBOX));
	//Apply button
	_assertVisible($EDITACCOUNT_CHANGEPASSWORD_SUBMITBUTTON);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124646", "Verify the application behavior on click of links in breadcrumb of edit account page of the application");
$t.start();
try 
{
	
	//breadcrumb
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Generic_Editaccount[0][0], _getText($PAGE_BREADCRUMB));

	//Bread crumb last
	_assertVisible($EDITACCOUNT_LINK_BREADCRUMB);
	//Navigation to My account
	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
	_click($MY_ACCOUNT_LINK_BREADCRUMB);
	_assertVisible($PAGE_BREADCRUMB);
	_assertEqual($Generic_Editaccount[9][0], _getText($PAGE_BREADCRUMB));
	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
	//resetting to edit account Page
	_click($MY_ACCOUNT_PERSONALDATA_OPTIONS);
	_assertEqual($Generic_Editaccount[0][0], _getText($PAGE_BREADCRUMB));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("264287/264288/264289/264290", "Verify the validation related to Current Password/New Password/Confirm New Password field and functionality of Apply button in Edit Account page of the application.");
$t.start();
try 
{


for(var $i=0;$i<$Password_Validation.length;$i++)
{
	_click($LEFTNAV_EDITINFORMATION_LINK);
	
	if($i==2 || $i==3  || $i==6 || $i==9)
		{
		   _setValue($EDITACCOUNT_CURRENTPASSWORD_TEXTBOX, $pwd);
		}
	else
		{
		 _setValue($EDITACCOUNT_CURRENTPASSWORD_TEXTBOX, $Password_Validation[$i][1]);
		}			
	_setValue($EDITACCOUNT_NEWPASSWORD_TEXTBOX, $Password_Validation[$i][2]);
	_setValue($EDITACCOUNT_CONFIRMPASSWORD_TEXTBOX, $Password_Validation[$i][3]);
	_wait(2000);
	if($i!=1)
		{
			_click($EDITACCOUNT_CHANGEPASSWORD_SUBMITBUTTON);
			_wait(2000);
		}
	//blank field validation
 	 if($i==0)
	 	 {	
	 		 //Current Password
		 	  _assertVisible(_span($Password_Validation[0][4], _in($EDITACCOUNT_CURRENTPASSWORD_SECTION)));
			  _assertEqual($color, _style($EDITACCOUNT_CURRENTPASSWORD_TEXTBOX, "border-color"));
		      _assertEqual($color1, _style(_span($Password_Validation[0][4], _in($EDITACCOUNT_CURRENTPASSWORD_SECTION)), "color"));
		      //New Password
		 	  _assertVisible(_span($Password_Validation[0][4], _in($EDITACCOUNT_NEWPASSWORD_SECTION)));
			  _assertEqual($color, _style($EDITACCOUNT_NEWPASSWORD_TEXTBOX, "border-color"))
			  _assertEqual($color1, _style(_span($Password_Validation[0][4], _in($EDITACCOUNT_NEWPASSWORD_SECTION)), "color"));
			  //Confirm New Password
			  _assertVisible(_span($Password_Validation[0][4], _in($EDITACCOUNT_CONFIRMNEWPASSWORD_SECTION)));
			  _assertEqual($color, _style($EDITACCOUNT_CONFIRMPASSWORD_TEXTBOX, "border-color"));
			  _assertEqual($color1, _style(_span($Password_Validation[0][4], _in($EDITACCOUNT_CONFIRMNEWPASSWORD_SECTION)), "color"));
	 	 }
 	 //Max characters
 	 else if($i==1)
 		 {
	 		_assertEqual($Password_Validation[0][5],_getText($EDITACCOUNT_CURRENTPASSWORD_TEXTBOX).length);
	 		_assertEqual($Password_Validation[0][5],_getText($EDITACCOUNT_NEWPASSWORD_TEXTBOX).length);
	 		_assertEqual($Password_Validation[0][5],_getText($EDITACCOUNT_CONFIRMPASSWORD_TEXTBOX).length);
 		 }
 	//Different pwd/confirm pwd
 	 else if($i==2)
 		 {
		 	_assertEqual($color1, _style($EDITACCOUNT_CONFIRMPASSWORD_TEXTBOX, "color"));
		 	_assertVisible(_div($Password_Validation[2][4], _in($EDITACCOUNT_CONFIRMNEWPASSWORD_SECTION)));
 		 }
 	 else if($i==3)
  		 {
			  _assertEqual($color1, _style(_div($Password_Validation[3][4], _in($EDITACCOUNT_CONFIRMNEWPASSWORD_SECTION)), "color"));
 		 }
 	else if($i==4)
 		 {
		 	_assertEqual($color1, _style($EDITACCOUNT_CURRENTPASSWORD_TEXTBOX, "color"));
		 	_assertVisible(_div($Password_Validation[4][4], _in($EDITACCOUNT_CURRENTPASSWORD_SECTION)));
 		 }
 	else if($i==5 || $i==6 || $i==7 || $i==8)
 		{
		 	_assertVisible(_div($Password_Validation[8][4], _in($EDITACCOUNT_CURRENTPASSWORD_SECTION)));
 		}
 	 //Valid
	 else
		 {
			 _assertVisible($HEADER_USERACCOUNT_LINK);
			 _assertVisible(_div($Generic_Editaccount[5][0]+" "+$FirstName+" "+$LastName+" "+$Generic_Editaccount[6][0]));
			 _assertVisible($MY_ACCOUNT_LOGOUT_LINK);
			 //logout
			 logout();
			//Login()
			 _click($HEADER_LOGIN_LINK);
			 //login with new pwd
			 _setValue($LOGIN_EMAIL_ADDRESS_TEXTBOX, $uId);
			 _setValue($LOGIN_PASSWORD_TEXTBOX, $Password_Validation[9][2]);
		     _click($LOGIN_SUBMIT_BUTTON);
		     //verification
		    _assertVisible($HEADER_USERACCOUNT_LINK);
			 _assertVisible(_div($Generic_Editaccount[5][0]+" "+$FirstName+" "+$LastName+" "+$Generic_Editaccount[6][0]));
			 _assertVisible($MY_ACCOUNT_LOGOUT_LINK);
			 //resetting the data
			 _click($LEFTNAV_EDITINFORMATION_LINK);
			 _setValue($EDITACCOUNT_CURRENTPASSWORD_TEXTBOX, $Password_Validation[9][2]);	
			 _setValue($EDITACCOUNT_NEWPASSWORD_TEXTBOX, $pwd);
			 _setValue($EDITACCOUNT_CONFIRMPASSWORD_TEXTBOX, $pwd);
			 _click($EDITACCOUNT_CHANGEPASSWORD_SUBMITBUTTON); 
		 	_wait(3000);
		 	_assertVisible($HEADER_USERACCOUNT_LINK);
		 	_assertVisible(_div($Generic_Editaccount[5][0]+" "+$FirstName+" "+$LastName+" "+$Generic_Editaccount[6][0]));
		 }
  }

}
catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end();


cleanup();
