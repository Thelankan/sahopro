_include("../../../GenericLibrary/GlobalFunctions.js");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


//login to the application
_click($HEADER_LOGIN_LINK);
login();
DeleteCreditCard();

//**
var $t = _testcase("125897/128534","Verify the UI of payment settings page of the application/UI of left navigation pane in payment settings page as a registered user not having saved credit card details");
$t.start();
try
{
	//navigate to payment page	
	_click($LEFTNAV_PAYMENTSETTINGS_LINK);
	_wait(3000);
	//verify the UI
	//Brad crumb
	_assertVisible(_link($Payment_Data_Profile[0][2], _in($PAGE_BREADCRUMB)));
	//heading
	_assertVisible($PAYMENT_CREDITCARD_INFORMATION_HEADING);
	_assertVisible($PAYMENT_ADDCREDITCARD);
	//left nav section

	var $Leftnavlinks=HeadingCountInLeftNav();
	var $LeftnavlinkNames=LinkCountInLeftNav();
	
	//UI of  leftnavlinks
	for(var $i=0;$i<$Leftnavlinks.length;$i++)
		{
		
		if ($i==4)
		{
			var $Expected_Leftnavlink = _div($LeftNav[$i][1]);
			_assertVisible($Expected_Leftnavlink);	
		}
		else
			{
			var $Expected_Leftnavlink = _span($LeftNav[$i][1]);
			_assertVisible($Expected_Leftnavlink);	
			}
		
			
		}
	
	//UI of leftnavlinks Names
	for(var $i=0;$i<$LeftnavlinkNames.length;$i++)
		{
		    var $Expected_LeftnavlinkNames = _link($LeftNav[$i][2]);
			_assertVisible($Expected_LeftnavlinkNames);			
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//**
var $t = _testcase("128723/124785/128535","Verify the UI of payment settings page of the application as a registered user having saved credit card details/application behavior on click of apply button in add credit card overlay/Verify if the user is able to add a create card details in Add payment details overlay as a registered user");
$t.start();
try
{
	//Pre conditions(save 1 credit card details)
	_click($PAYMENT_ADDCREDITCARD );
	_wait(3000)
	CreateCreditCard(0,$Createcard);

	//click on apply button
	_click($PAYMENT_APPLYBUTTON);
	//overlay got closed or not
	closedOverlayVerification();
	

	//verify the UI
	
	//Brad crumb
	_assertVisible(_link($Payment_Data_Profile[0][2], _in($PAGE_BREADCRUMB)));
	
	//heading
	_assertVisible($PAYMENT_CREDITCARD_INFORMATION_HEADING);
	_assertVisible($PAYMENT_ADDCREDITCARD );
	
	//verify whether credit card got saved or not and also UI
	_assertVisible($PAYMENT_PAYMENTLIST);
	var $saveInfo=_getText(_listItem("/first/")).split(" ");
	var $actualExpdate=$saveInfo[8]+""+$saveInfo[9];
	_assertEqual($Createcard[0][1],$saveInfo[0]);
	_assertEqual($Createcard[0][2],$saveInfo[1]);
	var $expDate=$Createcard[0][15]+"."+$Createcard[1][4]+"."+$Createcard[0][5];
	_assertEqual($expDate,$actualExpdate);
	_assertVisible($PAYMENT_DELETECARD_LINK);
		
	//left nav section
    var $Leftnavlinks=LeftNavHeadingCount();
	var $LeftnavlinkNames=LeftNavLinkCount();
	
	//UI of  leftnavlinks
	for(var $i=0;$i<$Leftnavlinks;$i++)
		{
			var $Expected_Leftnavlink = _span($LeftNav[$i][1]);
			
			_assertVisible($Expected_Leftnavlink);			
		}
	
	//UI of leftnavlinks Names
	for(var $i=0;$i<$LeftnavlinkNames;$i++)
		{
		    var $Expected_LeftnavlinkNames = _link($LeftNav[$i][2]);
		    
			_assertVisible($Expected_LeftnavlinkNames);			
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//navigate to check order page
_click($HEADER_USERACCOUNT_LINK);
_click($HEADER_MYACCOUNT_LINK);
_click($MY_ACCOUNT_PAYMENTSETTING_OPTIONS);

//**
var $t=_testcase("124787","Verify the application behavior on click of components displayed in breadcrumb of payment settings page of the application");
$t.start();
try
{


	//VERIFY BREADCRUMB
	_assertEqual($Payment_Data_Profile[5][0],_getText($PAGE_BREADCRUMB));

	//clicking on my account link
	_click($MY_ACCOUNT_LINK_BREADCRUMB);
	_assertVisible($MY_ACCOUNT_LINK_BREADCRUMB);
	_assertVisible($MY_ACCOUNT_OPTIONS);
	
	//navigating to payment settings page
	_click($MY_ACCOUNT_PAYMENTSETTING_OPTIONS);
	
	//VERIFY BREADCRUMB
	_assertEqual($Payment_Data_Profile[5][0],_getText($PAGE_BREADCRUMB));
	_assertVisible($PAYMENT_CREDITCARD_INFORMATION_HEADING);


}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


//**
var $t=_testcase("124781","Verify the application behavior on click of add credit card button in payment settings page of the application");
$t.start();
try
{
	//navigating to Payment setting page 	
	_click($HEADER_USERACCOUNT_LINK);
	_click($LEFTNAV_PAYMENTSETTINGS_LINK);
	_wait(3000)
	_click($PAYMENT_ADDCREDITCARD);
	_wait(3000)
	if (_isVisible($PAYMENT_ADDCARDOVERLAY))
		{		  
		  _assert(true, "Add a Credit Card overlay is displayed");		
		}
	else
		{
		_assert(false, "Add a Credit Card overlay is not displayed");
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124782","Verify the UI of add credit card overlay in payment settings page of the application");
$t.start();
try
{
	
//verify the UI of Add a credit card overlay
//Name on Card
_assertVisible($PAYMENT_NAMETEXT);
_assertVisible($PAYMENT_NAMETEXTBOX);
//Tyep
_assertVisible($PAYMENT_CARDTYPE_TEXT);
_assertVisible($PAYMENT_CARDTYPE_DROPDOWN);
//card number
_assertVisible($PAYMENT_NUMBER_TEXT);
_assertVisible($PAYMENT_NUMBER_TEXTBOX);
//month
_assertVisible($PAYMENT_MONTH_TEXT);
_assertVisible($PAYMENT_MONTH_DROPDOWN);
//Year
_assertVisible($PAYMENT_YEAR_TEXT);
_assertVisible($PAYMENT_YEAR_DROPDOWN);
//Apply button
_assertVisible($PAYMENT_APPLYBUTTON);
_assertVisible($PAYMENT_CANCELBUTTON);
	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124783","Verify the functionality related to close icon in add credit card overlay in payment settings page of the application");
$t.start();
try
{
	//click on close link
	if(_isVisible($PAYMENT_CLOSEBUTTON))
		{
		_click($PAYMENT_CLOSEBUTTON);
		}
	//overlay should not be displayed
	closedOverlayVerification();
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124784","Verify the application behavior on click of cancel button in add credit card overlay on payment settings page of the application");
$t.start();
try
{
	//open the credit card overlay
	_click($PAYMENT_ADDCREDITCARD );
	_wait(3000)
	//filling details in create card fields
	CreateCreditCard(0,$Createcard);		
	//click on cancel button
	_click($PAYMENT_CANCELBUTTON);
	//verify whether closed or not 
	closedOverlayVerification();
	//open the credit card overlay
	_click($PAYMENT_ADDCREDITCARD);
	//click on cancel button
	_click($PAYMENT_CANCELBUTTON);
	//verify whether closed or not 
	closedOverlayVerification();
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//**
var $t=_testcase("124790","Verify the functionality of delete link displayed for saved cards in payment settings page as a registered user having saved cards");
$t.start();
try
{
	var $Count = _count("_submit","Delete Card",_in(_list("payment-list")));
	_log($Count);
	//click on delete card link
	for(var $i=0;$i<$Count;$i++)
      {
     	   _byPassWaitMechanism(true);
		   _click($PAYMENT_DELETECARD_LINK); 
		   _expectConfirm("/Do you want to remove this credit card/",true);
		
			//click on ok in confirmation overlay
			
		   	_focusWindow();
		   // CTRL+R will reload this page using robot events
		    _wait(3000);   
			var $robot = new java.awt.Robot();
			$robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
			_wait(500);
			$robot.keyPress(java.awt.event.KeyEvent.VK_R);
			_wait(500);
			$robot.keyRelease(java.awt.event.KeyEvent.VK_R);
			_wait(500);
			$robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
			_wait(500);
      }
    	
	//verify whether deleted or not
	_assertNotVisible($PAYMENT_PAYMENTLIST);
	_assertNotVisible($PAYMENT_DELETECARD_LINK);
	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//124779
var $t = _testcase("124779","Verify the navigation related to links in left navigation pane on payment settings page of the application");
$t.start();
try
{
	
//	//navigate to check order page
//	_click($HEADER_USERACCOUNT_LINK);
//	_click($HEADER_MYACCOUNT_LINK);
//	_click($MY_ACCOUNT_PAYMENTSETTING_OPTIONS);
	
	var $Links=LeftNavLinkCount();
	 
	for(var $i=0;$i<$Links;$i++)
		{
			
			
			//click on left nav
			_click(_link($LeftNav[$i][2]));
			_wait(3000);
			_assertVisible(_link($LeftNav[$i][3], _in($PAGE_BREADCRUMB)));
		   	_assertVisible(_div($LeftNav[$i][4]));		    
			//navigate to check order page
			_click($HEADER_USERACCOUNT_LINK);
			_click($HEADER_MYACCOUNT_LINK);
			_click($MY_ACCOUNT_PAYMENTSETTING_OPTIONS);

		}	
	

	//click on contact us link
	_assertVisible($LEFTNAV_CONTACTUS_LINK);
	_click($LEFTNAV_CONTACTUS_LINK);
	//Verifying the navigation
	_assertVisible($PAGE_BREADCRUMB);

	_assertVisible($CONTACTUS_PAGE_HEADING);
	_assertVisible($CONTACT_CONTENT);
	

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



//**
/*var $t=_testcase("125899/148683/148684/148685/148686/148687/148688/148689/148690/148691/148692/148693/148694/288243/289004/289020","Verify the validation of 'Name on Card'/'type'/number/first name/last name/address1/city/state/zip code/phone field/email field/card type/ expiration fields  present in the Add a credit card overlay on my account payment methods page of the application as a Registered user");
$t.start();
try
{
	
	
	if (_isIE())
	{
		_wait(2000,_isVisible($PAYMENT_ADDCREDITCARD ));
	}

	//validation of credit card overlay
	for(var $i=0;$i<$Validations_Payment.length;$i++)
		{

		
		//for validating email fields 
		$uId=$Validations_Payment[$i][20];
		
		//add credit card link
		_click($PAYMENT_ADDCREDITCARD);
		_wait(3000);
		//credit card fields
		CreateCreditCard($i,$Validations_Payment);
		_click($PAYMENT_APPLYBUTTON);
		_log($i);
		//blank
		if($i==0)
			{
				_assertEqual($Validations_Payment[0][15],_style($PAYMENT_NAMETEXTBOX,"background-color"));
				_assertEqual($Validations_Payment[0][15],_style($PAYMENT_NUMBER_TEXTBOX,"background-color"));
//				if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
//				{
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_FIRSTNAME_TEXTBOX,"background-color"));
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_LASTNAME_TEXTBOX,"background-color"));
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_ADDRESS1_TEXTBOX,"background-color"));
					//_assertEqual($Validations_Payment[0][15],_style($PAYMENT_COUNTRY_TEXTBOX,"background-color"));
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_CITY_TEXTBOX,"background-color"));
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_STATE_TEXTBOX,"background-color"));
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_ZIPCODE_TEXTBOX,"background-color"));
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_PHONE_TEXTBOX,"background-color"));
					_assertEqual($Validations_Payment[0][15],_style($PAYMENT_EMAIL_FIELD,"background-color"));
					//error msg
					_assertVisible(_label($Validations_Payment[6][15], _near($PAYMENT_NAMETEXT)));
					_assertVisible(_label($Validations_Payment[6][15], _near($PAYMENT_NUMBER_TEXT)));
					_assertVisible(_label($Validations_Payment[6][15], _near($PAYMENT_FIRSTNAME_TEXT)));
					_assertVisible(_label($Validations_Payment[6][15], _near($PAYMENT_LASTNAME_TEXT)));
					_assertVisible(_label($Validations_Payment[6][15], _near($PAYMENT_ADDRESS1_TEXT)));
					_assertVisible(_label($Validations_Payment[6][15], _near($PAYMENT_CITY_TEXT)));
					_assertVisible(_label($Validations_Payment[6][15], _near($PAYMENT_ZIPCODE_TEXT)));
					_assertVisible(_label($Validations_Payment[6][15], _near($PAYMENT_PHONE_TEXT)));
					_assertVisible(_label($Validations_Payment[6][15], _near($PAYMENT_EMAIL_TEXT)));
					//close button
					_click($PAYMENT_CLOSEBUTTON);
					_wait(2000);
	
//				}
			}
		//max length
		if($i==1)
			{
			_assertEqual($Validations_Payment[1][15],_getText($PAYMENT_NAMETEXTBOX).length);
			_assertEqual($Validations_Payment[1][16],_getText($PAYMENT_NUMBER_TEXTBOX).length);
//			if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
//				{
					_assertEqual($Validations_Payment[1][17],_getText($PAYMENT_FIRSTNAME_TEXTBOX).length);
					_assertEqual($Validations_Payment[1][17],_getText($PAYMENT_LASTNAME_TEXTBOX).length);
					_assertEqual($Validations_Payment[1][17],_getText($PAYMENT_ADDRESS1_TEXTBOX).length);
					_assertEqual($Validations_Payment[1][17],_getText($PAYMENT_ADDRESS2_TEXTBOX).length);
					_assertEqual($Validations_Payment[1][17],_getText($PAYMENT_CITY_TEXTBOX).length);
					_assertEqual($Validations_Payment[1][18],_getText($PAYMENT_ZIPCODE_TEXTBOX).length);
					_assertEqual($Validations_Payment[1][19],_getText($PAYMENT_PHONE_TEXTBOX).length);
					_assertEqual($Validations_Payment[1][17],_getText($PAYMENT_EMAIL_FIELD).length);
					//close button
					_click($PAYMENT_CLOSEBUTTON);
					_wait(2000);
//				}			
			}
		//invalid card number & Exp Year
		if($i==2)
			{
			//card number 
			_assertEqual($Validations_Payment[0][16],_style($PAYMENT_NUMBER_TEXT,"color"));
			_assertVisible(_div($Validations_Payment[2][15]));
			_assertEqual($Validations_Payment[0][15],_style(_div($Validations_Payment[2][15]),"background-color"));
			//Exp Year
			_assertEqual($Validations_Payment[0][16],_style($PAYMENT_MONTH_TEXT,"color"));
			_assertEqual($Validations_Payment[0][16],_style($PAYMENT_YEAR_TEXT,"color"));
			_assertVisible(_div($Validations_Payment[2][16]));
			_assertEqual($Validations_Payment[0][15],_style(_div($Validations_Payment[2][16]),"background-color"));
			//close button
			_click($PAYMENT_CLOSEBUTTON);
			_wait(2000);
			}
		//invalid in zip code,phone number
		if($i==3 || $i==4 || $i==5 || $i==6)
			{
			//number
			_assertEqual($Validations_Payment[0][16],_style(_label($Validations_Payment[2][15]),"color"));
			_assertVisible(_div($Validations_Payment[2][15]));
			_assertEqual($Validations_Payment[0][15],_style(_div($Validations_Payment[2][15]),"background-color"));
			//ZIP CODE
			_assertEqual($Validations_Payment[0][16],_style(_label($Validations_Payment[3][15]),"color"));
			_assertVisible(_div($Validations_Payment[3][15]));
			_assertEqual($Validations_Payment[0][15],_style($PAYMENT_ZIPCODE_TEXTBOX,"background-color"));
			//PHONE
			_assertEqual($Validations_Payment[0][16],_style(_label($Validations_Payment[5][15]),"color"));
			_assertVisible(_div($Validations_Payment[5][15]));
			_assertEqual($Validations_Payment[0][15],_style($PAYMENT_PHONE_TEXTBOX,"background-color"));
			//EMAIL
			_assertEqual($Validations_Payment[0][16],_style(_label($Validations_Payment[4][15]),"color"));
			_assertVisible(_div($Validations_Payment[4][15]));
			_assertEqual($Validations_Payment[0][15],_style($PAYMENT_EMAIL_FIELD,"background-color"));
			//close button
			_click($PAYMENT_CLOSEBUTTON);
			_wait(2000);
			}
			
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();*/

//cleanup();