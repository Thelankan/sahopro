_include("../../util.GenericLibrary/BrowserSpecific.js");
_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Register_Data.xls");
var $Generic=_readExcelFile("Register_Data.xls","Generic");
var $Account_Validation=_readExcelFile("Register_Data.xls","Account_Validation");
var $color=$Account_Validation[0][8];
var $color1=$Account_Validation[1][8];
//deleting the user if exists
deleteUser();
SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);
var $t = _testcase("148638", "Verify the UI  of 'Create Account' page in application as an  Anonymous user.");
$t.start();
try
{
	_click(_link("Login"));
	_click(_submit("dwfrm_login_register"));
	//heading
	_assertVisible(_heading1($Generic[0][0]));
	//breadcrumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[1][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[1][0], _getText(_div("breadcrumb")));
		}
	//fields
	_assertVisible(_fieldset("/"+$Generic[2][0]+"/"));
	//FName
	_assertVisible(_span("First Name"));
	_assertVisible(_textbox("dwfrm_profile_customer_firstname"));
	_assertEqual("", _getValue(_textbox("dwfrm_profile_customer_firstname")));
	//Lname
	_assertVisible(_span("Last Name"));
	_assertVisible(_textbox("dwfrm_profile_customer_lastname"));
	_assertEqual("", _getValue(_textbox("dwfrm_profile_customer_lastname")));
	//EMAIL / LOGIN INFORMATION
	_assertVisible(_fieldset($Generic[3][0]));
	//Email
	_assertVisible(_span("Email"));
	_assertVisible(_textbox("dwfrm_profile_customer_email"));
	_assertEqual("", _getValue(_textbox("dwfrm_profile_customer_email")));
	//confirm email
	_assertVisible(_span("Confirm Email"));
	_assertVisible(_textbox("dwfrm_profile_customer_emailconfirm"));
	_assertEqual("", _getValue(_textbox("dwfrm_profile_customer_emailconfirm")));
	//Password
	_assertVisible(_span("Password"));
	_assertVisible(_password("/dwfrm_profile_login_password/"));
	_assertEqual("", _getValue(_password("/dwfrm_profile_login_password/")));
	//static text
	_assertVisible(_div($Generic[4][0]));
	//confirm password
	_assertVisible(_span("Confirm Password"));
	_assertVisible(_password("/dwfrm_profile_login_passwordconfirm/"));
	_assertEqual("", _getValue(_password("/dwfrm_profile_login_passwordconfirm/")));
	//checkbox
	_assertVisible(_checkbox("dwfrm_profile_customer_addtoemaillist"));
	_assertNotTrue(_checkbox("dwfrm_profile_customer_addtoemaillist").checked);
	_assertVisible(_span($Generic[5][0]));
	//Privacy policy link
	_assertVisible(_link("See Privacy Policy"));
	//apply button
	_assertVisible(_submit("dwfrm_profile_confirm"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124585", "Verify the navigation related to 'Home' and 'My Account' links  in breadcrumb on Create account page in application as an  Anonymous user.");
$t.start();
try
{
	_click(_link("Login"));
	_click(_submit("dwfrm_login_register"));
	//heading
	_assertVisible(_heading1($Generic[0][0]));
	//breadcrumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[1][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[1][0], _getText(_div("breadcrumb")));
		}
	//Navigation related to my account
	_click(_link("My Account", _in(_div("breadcrumb"))));
	_assertVisible(_heading1("My Account Login"));
	_assertVisible(_div("login-box login-account"));

	_click(_link("Login"));
	_click(_submit("dwfrm_login_register"));
	//heading
	_assertVisible(_heading1($Generic[0][0]));
	//breadcrumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[1][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[1][0], _getText(_div("breadcrumb")));
		}
	/*//Navigation related to home
	_click(_link("Home", _in(_div("breadcrumb"))));
	_assertVisible(_list("homepage-slides"));
	_assertVisible(_div("home-bottom-slots"));*/
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148639", "Verify the navigation related to ''Privacy Policy' link  on Create account page left nav in  application as an  Anonymous user.");
$t.start();
try
{
	_click(_link("Login"));
	_click(_submit("dwfrm_login_register"));
	//heading
	_assertVisible(_heading1($Generic[0][0]));
	//Privacy policy link
	_assertVisible(_link("Privacy Policy"));
	_click(_link("Privacy Policy"));
	//verify the navigation
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][1].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][1], _getText(_div("breadcrumb")));
		}
	_assertVisible(_heading1($Generic[1][1]));
	_assertVisible(_div("content-asset"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148640", "Verify the navigation related to 'Secure Shopping' link  on Create account page left nav in  application as an  Anonymous user.");
$t.start();
try
{
	_click(_link("Login"));
	_click(_submit("dwfrm_login_register"));
	//heading
	_assertVisible(_heading1($Generic[0][0]));
	//Secure Shopping Link
	_assertVisible(_link("Secure Shopping"));
	_click(_link("Secure Shopping"));
	//verify the navigation
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][2].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][2], _getText(_div("breadcrumb")));
		}
	_assertVisible(_heading1($Generic[1][2]));
	_assertVisible(_div("content-asset"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("148641", "Verify the navigation related to 'Contact Us' link  on Create account page left nav in  application as an  Anonymous user.");
$t.start();
try
{
	_click(_link("Login"));
	_click(_submit("dwfrm_login_register"));
	//heading
	_assertVisible(_heading1($Generic[0][0]));
	//Contact us
	_assertVisible(_link("Contact Us"));
	_click(_link("Contact Us"));
	//Verifying the navigation
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][3].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][3], _getText(_div("breadcrumb")));
		}
	_assertVisible(_heading1("/"+$Generic[1][3]+"/"));
	_assertVisible(_div("primary-content"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("148642", "Verify the functionality related to  'ACCOUNT SETTINGS' down arrow on Create account  page left Nav in application as an  Anonymous user.");
$t.start();
try
{
	_click(_link("Login"));
	_click(_submit("dwfrm_login_register"));
	//collect the list present under the account settings
	var $list=_collectAttributes("_list","/(.*)/","sahiText",_above(_span($Generic[1][4])));
	//click on account settings link
	_click(_span($Generic[0][4]));
	//after clicking links present under account settings shouldn't visible
	for(var $i=0;$i<$list.length;$i++)
		{
		_assertNotVisible(_listItem($list[$i]));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124592", "Verify the functionality related to  'SHOP CONFIDENTLY' down arrow on Create account page left Nav in application as an  Anonymous user.");
$t.start();
try
{
	_click(_link("Login"));
	_click(_submit("dwfrm_login_register"));
	//collect the list present under the SHOP CONFINDENTLY
	var $list=_collectAttributes("_list","/(.*)/","sahiText",_under(_span($Generic[1][4])));
	//click on account settings link
	_click(_span($Generic[1][4]));
	//after clicking links present under account settings shouldn't visible
	for(var $i=0;$i<$list.length;$i++)
		{
		_assertNotVisible(_listItem($list[$i]));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124588/124589/124597/124601/--/125240/124599", "Verify the validation related to  'First name'/'Last name'/'Email'/'Confirm Email'/'Password'/'Confirm Password' field  and  functionality related to 'Apply' button on Create account page in application as an  Anonymous user.");
$t.start();
try
{
	_click(_link("Login"));
	_click(_submit("dwfrm_login_register"));
	//heading
	_assertVisible(_heading1($Generic[0][0]));
	for(var $i=0;$i<$Account_Validation.length;$i++)
		{
			if($i==10)
				{
					createAccount();
					//verify whether account got created or not
			 	   var $exp=$userData[$user][1]+" "+$userData[$user][2];
			 	   _assertEqual($exp, _extract(_getText(_heading1("/My Account/")),"/[|] (.*) [(]/",true).toString());
			 	   _click(_link("user-account"));
			 	   _click(_link("Logout"));
			 	  _click(_submit("dwfrm_login_register"));
				}
			else
				{
				 _setValue(_textbox("dwfrm_profile_customer_firstname"), $Account_Validation[$i][1]);
			 	 _setValue(_textbox("dwfrm_profile_customer_lastname"), $Account_Validation[$i][2]);
			 	 _setValue(_textbox("dwfrm_profile_customer_email"), $Account_Validation[$i][3]);
			 	 _setValue(_textbox("dwfrm_profile_customer_emailconfirm"), $Account_Validation[$i][4]);
			 	 _setValue(_password("/dwfrm_profile_login_password/"), $Account_Validation[$i][5]);
			 	 _setValue(_password("/dwfrm_profile_login_passwordconfirm/"), $Account_Validation[$i][6]);
			 	 if($i!=1)
			 		 {
			 		 	_click(_submit("Apply")); 
			 		 }	
			 	 //blank field validation
			 	 if($i==0)
			 		 {
			 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_firstname"), "background-color"));
			 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_lastname"), "background-color"));
			 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_email"), "background-color"));
			 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_emailconfirm"), "background-color"));
			 		  _assertEqual($color, _style(_password("/dwfrm_profile_login_password/"), "background-color"));
			 	  	  _assertEqual($color, _style(_password("/dwfrm_profile_login_passwordconfirm/"), "background-color"));
			 		 }
			 	 //Max characters
			 	 else if($i==1)
			 		 {
			 		_assertEqual($Account_Validation[0][9],_getText(_textbox("dwfrm_profile_customer_firstname")).length);
			 		_assertEqual($Account_Validation[1][9],_getText(_textbox("dwfrm_profile_customer_lastname")).length);
			 		_assertEqual($Account_Validation[2][9],_getText(_textbox("dwfrm_profile_customer_email")).length);
			 		_assertEqual($Account_Validation[3][9],_getText(_textbox("dwfrm_profile_customer_emailconfirm")).length);
			 		_assertEqual($Account_Validation[4][9],_getText(_password("/dwfrm_profile_login_password/")).length);
			 		_assertEqual($Account_Validation[5][9],_getText(_password("/dwfrm_profile_login_passwordconfirm/")).length);
			 		 }
			 	 //invalid data
			 	 else if($i==2 || $i==3 ||$i==4 || $i==5)
			 		 {
			 		 _assertVisible(_span($Account_Validation[$i][7], _in(_div("/Email/"))));
			 		 //_assertEqual($color, _style(_textbox("dwfrm_profile_customer_email"), "background-color"));	 		  
			 		 _assertVisible(_span($Account_Validation[$i][7], _in(_div("/Confirm Email/"))));
			 		 //_assertEqual($color, _style(_textbox("dwfrm_profile_customer_emailconfirm"), "background-color"));
			 		 }	 	 
			 	 //password less than 8 characters
			 	 else if($i==6)
			 		 {
			 		_assertVisible(_div($Account_Validation[$i][7]));
			 		_assertEqual($color1,_style(_span($Account_Validation[1][10]), "color"));			 		 
			 		_assertVisible(_div($Account_Validation[$i][7], _in(_div("/ Confirm Password /"))));
			 		_assertEqual($color1,_style(_span($Account_Validation[2][10]), "color"));
			 		 }
			 	 else if($i==7 || $i==8 || $i==9)
			 		 {			 		
			 		 	if($i==7)
			 		 		{
			 		 		 _assertEqual($color1, _style(_span($Account_Validation[0][10]), "color"));
			 		 		}
			 		 	if($i==8)
			 		 		{
			 		 		_assertEqual($color1, _style(_span($Account_Validation[2][10]), "color"));
			 		 		}
			 		_assertVisible(_div($Account_Validation[$i][7]));
			 		 }
				}
		}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();
//deleting the user if exists
deleteUser();
