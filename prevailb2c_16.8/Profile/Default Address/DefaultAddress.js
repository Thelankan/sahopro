_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("DefaultAddress_Data.xls");

var $Valid_Address=_readExcelFile("DefaultAddress_Data.xls","Valid_Address");
var $data=_readExcelFile("DefaultAddress_Data.xls","Data");
var $PaymentData=_readExcelFile("DefaultAddress_Data.xls","Payment_data");

//Enabling default address
enableDefaultShippingAddress();

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//login
_click(_link("Login"));
login();

//de-selecting the addresses  from address
_click(_link("Wish List"));
if(_isVisible(_select("editAddress")))
	{
	_setSelected(_select("editAddress"), "No Shipping Address Assigned");
	}

//Precondition
_click(_link("Addresses"));
deleteAddress();


var $t = _testcase("155594", "Check application behavior when default shipping address check box is checked and address saved in profile section.");
$t.start();
try
{
	
 _click(_link("Create New Address"));
 
 if(_isVisible(_div("dialog-container")))
 	{
	 addAddress($Valid_Address,0);
	 //Checking default shipping address check box
	 //_check(_checkbox("dwfrm_profile_address_defaultshipping"));
	 _click(_submit("Apply"));
	 _wait(3000);
	 }
 else
	 {
		 _assert(false,"Add address Overlay is not displaying"); 
	 }
 
 navigateToShippingPage($data[0][0],1);
 
 //Verifying the default shipping is present in shipping drop down or not
 VerifyingShippingDropdown(0);
//verifying the selected default address is pre populating in shipping address or not
 PrepopulateShippingValues(0);

//clicking on submit button
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));

//Checking overlay
if (_isVisible(_div("address-validation-dialog")))
	{
	_click(_submit("ship-to-original-address"));
	}
else
	{
	_log("Overlay is not displayed");
	}

//Verifying address values pre populated in billing fields or not (Should not pre populate as we are selecting default shipping address)
ShouldNotPrepopulateBillingValues();  
//Verifying the default shipping is present in billing drop down or not
_setSelected(_select("dwfrm_billing_addressList"), 1);
VerifyingBillingDropdown(0);

	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
 $t.end();
 
//Deleting Address
_click(_link("user-account"));
_click(_link("My Account"));
_click(_link("Addresses"));
deleteAddress(); 
 
 var $t = _testcase("155595", "Check application behavior when default billing address check box is checked and address saved in profile section.");
 $t.start();
 try
 {
	 
//creating new address
_click(_link("Create New Address"));
 
if(_isVisible(_div("dialog-container")))
	{
 addAddress($Valid_Address,0);
 //Checking default shipping address check box
 //_check(_checkbox("dwfrm_profile_address_defaultbilling"));
 _click(_submit("Apply"));
 _wait(3000);
	 }
 	else
		 {
			 _assert(false,"Add address Overlay is not displaying"); 
		 }
//Navigating to shipping page
_mouseOver(_link("mini-cart-link"));
_click(_link("mini-cart-link-checkout"));

 _setSelected(_select("dwfrm_singleshipping_addressList"), 1);
 
//clicking on submit button
 _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
 
//Clicking on overlay
if (_isVisible(_div("address-validation-dialog")))
{
_click(_submit("ship-to-original-address"));
}
 else
 	{
 	_log("Overlay is not displayed");
 	}

//verifying presence of address in billing page drop down
VerifyingBillingDropdown(0);
//Verifying address values pre populated in billing fields or not (Should pre populate as we are selecting default shipping address)
PrepopulateBillingValues(0); 

 }
 catch($e)
 {
 	_logExceptionAsFailure($e);
 }	
  $t.end();

//Deleting Address
_click(_link("user-account"));
_click(_link("My Account"));
_click(_link("Addresses"));
deleteAddress(); 

  
  var $t = _testcase("155596/157210", "Check application behavior when both my default billing/my default shipping address check box is checked and address saved in profile section " +
  		             "Verify the application behvaior when the user changes the same default billing and shipping address to non default shipping address.");
  $t.start();
  try
  {
	  
  _click(_link("Create New Address"));
  if(_isVisible(_div("dialog-container")))
  	{
   addAddress($Valid_Address,0);
   //Checking default shipping address check box
   _check(_checkbox("dwfrm_profile_address_defaultshipping"));
   _check(_checkbox("dwfrm_profile_address_defaultbilling"));
   _click(_submit("Apply"));
  	 }
   	else
  		 {
  			 _assert(false,"Add address Overlay is not displaying"); 
  		 }
   navigateToShippingPage($data[0][0],1);
   
//Verification of presence of address in shipping page drop down
VerifyingShippingDropdown(0);
//verifying the selected default address is pre populating in shipping address or not
PrepopulateShippingValues(0);
 
 //clicking on submit button
   _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
   
   //Clicking on overlay
  if (_isVisible(_div("address-validation-dialog")))
  {
  //_click(_submit("Continue"));
  _click(_submit("ship-to-original-address"));
  }
   else
   	{
   	_log("Overlay is not displayed");
   	}
    
//Verification of presence of address in Billing page
VerifyingBillingDropdown(0);
//Verifying address values pre populated in billing fields or not (Should pre populate as we are selecting default shipping address)
PrepopulateBillingValues(0);

//Un checking the default billing and shipping address to non default shipping & billing address.
_click(_link("user-account"));
_click(_link("Addresses"));
_click(_link("address-edit"));

if(_isVisible(_div("dialog-container")))
{
//unChecking default shipping address check box
_uncheck(_checkbox("dwfrm_profile_address_defaultshipping"));
_uncheck(_checkbox("dwfrm_profile_address_defaultbilling"));
_click(_submit("Apply"));
 }
	else
	 {
		 _assert(false,"Add address Overlay is not displaying"); 
	 }

//Navigating to shipping page
_mouseOver(_link("mini-cart-link"));
_click(_link("mini-cart-link-checkout"));


//verifying the selected default address is pre populating in shipping address or not
ShouldNotPrepopulateShippingValues();
//Verification of presence of address in shipping page drop down
_setSelected(_select("dwfrm_singleshipping_addressList"), 1); 
VerifyingShippingDropdown(0);

//clicking on submit button
 _click(_submit("dwfrm_singleshipping_shippingAddress_save"));
 
 //Clicking on overlay
if (_isVisible(_div("address-validation-dialog")))
{
//_click(_submit("Continue"));
_click(_submit("ship-to-original-address"));
}
 else
 	{
 	_log("Overlay is not displayed");
 	}
  
//Verifying address values pre populated in billing fields or not (Should pre populate as we are selecting default shipping address)
ShouldNotPrepopulateBillingValues(); 
//Verification of presence of address in Billing page
_setSelected(_select("dwfrm_billing_addressList"), 1); 
VerifyingBillingDropdown(0);

  }
  catch($e)
  {
  	_logExceptionAsFailure($e);
  }	
   $t.end();
  
   
var $t = _testcase("157197", "Verify the display of address in shipping page when the default shipping address is changed to non default shipping address.");
$t.start();
try
 {
	
  _click(_link("user-account"));
  _click(_link("Addresses"));
  _click(_link("address-edit"));
   
if(_isVisible(_div("dialog-container")))
	{
	//unChecking default shipping address check box
	_uncheck(_checkbox("dwfrm_profile_address_defaultshipping"));
	_click(_submit("Apply"));
	 }
		else
		 {
			 _assert(false,"Add address Overlay is not displaying"); 
		 }
//Navigating to shipping page
_mouseOver(_link("mini-cart-link"));
_click(_link("mini-cart-link-checkout"));

//Values should not pre populate in shipping page fields
ShouldNotPrepopulateShippingValues();
//verifying the selected default address is pre populating in shipping address or not
_setSelected(_select("dwfrm_singleshipping_addressList"), 1); 
VerifyingShippingDropdown(0);
    	  
 }
 catch($e)
   {
   	_logExceptionAsFailure($e);
   }	
    $t.end();  
    
    
var $t = _testcase("157202", "Verify the display of address in shipping page when the default billing address is changed to non default billing address.");
$t.start();
try
{
 
 _click(_link("user-account"));
 _click(_link("Addresses"));
 _click(_link("address-edit"));
   
if(_isVisible(_div("dialog-container")))
	{
	//un Checking default billing address check box
	_uncheck(_checkbox("dwfrm_profile_address_defaultbilling"));
	_click(_submit("Apply"));
	 }
		else
	 	{
		 _assert(false,"Add address Overlay is not displaying"); 
   		 }

//Navigating to shipping page
_mouseOver(_link("mini-cart-link"));
_click(_link("mini-cart-link-checkout"));
//Selecting shipping values 
_setSelected(_select("dwfrm_singleshipping_addressList"), 1);

_click(_submit("dwfrm_singleshipping_shippingAddress_save"));	

if(_isVisible(_div("address-validation-dialog")))
{
	_click(_submit("ship-to-original-address"));
}

//Values should not pre populate in billing page fields
ShouldNotPrepopulateBillingValues();  
//Verification the presence of address in Billing page dropdown
_setSelected(_select("dwfrm_billing_addressList"), 1);
VerifyingBillingDropdown(0);
 
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
 $t.end();   
 
//Checking shipping default address check box
 _click(_link("user-account"));
 _click(_link("Addresses"));
 _click(_link("address-edit"));
 
 if(_isVisible(_div("dialog-container")))
	{
	//Checking default shipping address check box
	_check(_checkbox("dwfrm_profile_address_defaultshipping"));
	_click(_submit("Apply"));
	 }
		else
		 {
			 _assert(false,"Add address Overlay is not displaying"); 
		 }
 
 //Creating address and checking default billing check box
 _click(_link("Create New Address"));
 if(_isVisible(_div("dialog-container")))
 	{
  addAddress($Valid_Address,1);
//Checking billing default address check box
  _check(_checkbox("dwfrm_profile_address_defaultbilling"));
  _click(_submit("Apply"));
 	 }
  	else
 		 {
 			 _assert(false,"Add address Overlay is not displaying"); 
 		 }
 
 var $t = _testcase("157207", "Verify the application behvaior when a default shipping address is deleted.");
 $t.start();
 try
 {
	 
//Navigating to shipping page
_mouseOver(_link("mini-cart-link"));
_click(_link("mini-cart-link-checkout"));
//verifying the selected default address is pre populating in shipping address or not
VerifyingShippingDropdown(0);
//Navigating to address page
_click(_link("user-account"));
_click(_link("Addresses"));
//deleting default shipping address
_click(_link("Delete", _in(_listItem("/Default Shipping Address /"))));
_expectConfirm("/Do you want/",true);
//Navigating to shipping page
_mouseOver(_link("mini-cart-link"));
_click(_link("mini-cart-link-checkout"));

//Should not pre populate shipping values
ShouldNotPrepopulateShippingValues();

//Deleted default shipping Address should not present in shipping drop down
if (_isVisible(_select("dwfrm_singleshipping_addressList")))
	{
_setSelected(_select("dwfrm_singleshipping_addressList"), 1); 
var $dropdownvalueShipping=_getSelectedText(_select("dwfrm_singleshipping_addressList"));
var $expectedAddressShipping="("+$Valid_Address[0][1]+")"+" "+$Valid_Address[0][4]+","+" "+$Valid_Address[0][8]+","+" "+$Valid_Address[0][7]+","+" "+$Valid_Address[0][9];
_assertNotEqual($expectedAddressShipping,$dropdownvalueShipping);
	}
else
	{
	_log("Address drop down is not present in shipping page");
	} 
 }
 catch($e)
 {
 	_logExceptionAsFailure($e);
 }	
  $t.end();
 
  
var $t = _testcase("157209", "Verify the application behvaior when a default billing address is deleted");
$t.start();
try
  {
 	 
//Navigating to shipping page
_mouseOver(_link("mini-cart-link"));
_click(_link("mini-cart-link-checkout"));
//verifying the selected default address is pre populating in shipping address or not
VerifyingBillingDropdown(1);
//Navigating to address page
_click(_link("user-account"));
_click(_link("Addresses"));
//deleting default shipping address
_click(_link("Delete", _in(_listItem("/Default Billing Address Billing /"))));
_expectConfirm("/Do you want/",true);
//Navigating to shipping page
_mouseOver(_link("mini-cart-link"));
_click(_link("mini-cart-link-checkout"));
shippingAddress($Valid_Address,0);
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));	

if(_isVisible(_div("address-validation-dialog")))
{
	_click(_submit("ship-to-original-address"));
}

//Should not pre populate billing values
ShouldNotPrepopulateBillingValues();

if (_isVisible(_select("dwfrm_billing_addressList")))
	{
//verifying billing drop down
_setSelected(_select("dwfrm_billing_addressList"), 1); 
var $dropdownvalueBilling=_getSelectedText(_select("dwfrm_billing_addressList"));
var $expectedAddressBilling="("+$Valid_Address[1][1]+")"+" "+$Valid_Address[1][4]+","+" "+$Valid_Address[1][8]+","+" "+$Valid_Address[1][7]+","+" "+$Valid_Address[1][9];
_assertNotEqual($expectedAddressBilling,$dropdownvalueBilling);
	}
else
	{
	_log("Address drop down is not present in billing page");
	}

  }
catch($e)
  {
  	_logExceptionAsFailure($e);
  }	
   $t.end(); 
   
 var $t = _testcase("157211", "Verify the application behvaior when the address saved as default shipping and billing address is deleted.");
 $t.start();
 try
   {
	 
	//Deleting Address
	deleteAddress();
	//Navigating to shipping page
	_mouseOver(_link("mini-cart-link"));
	_click(_link("mini-cart-link-checkout"));
	
	//Should not pre populate shipping values
	ShouldNotPrepopulateShippingValues();
	_assertNotVisible(_select("dwfrm_singleshipping_addressList"));
	
	//Entering values in shipping page
	shippingAddress($Valid_Address,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));	

	if(_isVisible(_div("address-validation-dialog")))
	{
		_click(_submit("ship-to-original-address"));
	}

	//Should not pre populate billing values
	ShouldNotPrepopulateBillingValues();
	_assertNotVisible(_select("dwfrm_billing_addressList"));
	
   }
  catch($e)
   {
   	_logExceptionAsFailure($e);
   }	
    $t.end(); 

    
var $t = _testcase("155599", "Verify behavior when another address added in profile section by checked the checkbox 'Make my default shipping address'");
$t.start();
try
{
	
function VerifyMakeDefaultShippingAddress($j)	
	{
//Navigating to address page
_click(_link("user-account"));
_click(_link("Addresses"));
 //Creating address and checking default billing check box
 _click(_link("Create New Address"));
 if(_isVisible(_div("dialog-container")))
 	{
	 addAddress($Valid_Address,$j);
	//Checking billing default address check box
	_check(_checkbox("dwfrm_profile_address_defaultshipping"));
	//_check(_checkbox("dwfrm_profile_address_defaultbilling"));
	_click(_submit("Apply"));
	
	var $DefaultAddress=$Valid_Address[$j][2]+" "+$Valid_Address[$j][3];
	
	if ($j==0)
		{
		_assertEqual($DefaultAddress,_getText(_div("mini-address-name", _in(_listItem("/Default Shipping Address /")))));
		}
		else
			{
			_assertEqual($DefaultAddress,_getText(_div("mini-address-name", _in(_listItem("/Default Shipping Address /")))));
			}
 	 }
  	else
 		 {
 			 _assert(false,"Add address Overlay is not displaying"); 
 		 }
//Navigating to shipping page
_mouseOver(_link("mini-cart-link"));
_click(_link("mini-cart-link-checkout"));

//Verifying shipping drop down values
PrepopulateShippingValues($j);
if ($j==0)
	{
	//Shipping Drop down
	VerifyingShippingDropdown(0);
	}
	else
		{
		VerifyingShippingDropdown(1);
		}


_click(_submit("dwfrm_singleshipping_shippingAddress_save"));	

if(_isVisible(_div("address-validation-dialog")))
{
	_click(_submit("ship-to-original-address"));
}

//Should not pre populate billing values
ShouldNotPrepopulateBillingValues();
//Verifying billing drop down
if($j==0)
	{
	_setSelected(_select("dwfrm_billing_addressList"), 1);
	VerifyingBillingDropdown(0);	
	}
	else
		{
		_setSelected(_select("dwfrm_billing_addressList"), 2);
		VerifyingBillingDropdown(1);	
		}
	}

VerifyMakeDefaultShippingAddress(0);
VerifyMakeDefaultShippingAddress(1);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
 $t.end();
 
 
 
 //Deleting Address
 deleteAddress();
 
 var $t = _testcase("155603/157168", "Verify behavior when saved address edited in profile section and checkbox 'Maky this my default billing address' is checked.");
 $t.start();
 try
 {
	 
	 function VerifyMakeDefaultBillingAddress($j)	
		{
	//Navigating to address page
	_click(_link("user-account"));
	_click(_link("Addresses"));
	 //Creating address and checking default billing check box
	 _click(_link("Create New Address"));
	 if(_isVisible(_div("dialog-container")))
	 	{
		 addAddress($Valid_Address,$j);
		//Checking billing default address check box
		_check(_checkbox("dwfrm_profile_address_defaultbilling"));
		_click(_submit("Apply"));
		
		var $DefaultAddress=$Valid_Address[$j][2]+" "+$Valid_Address[$j][3];
		
		if ($j==0)
			{
			_assertEqual($DefaultAddress,_getText(_div("mini-address-name", _in(_listItem("/Default Billing Address /")))));
			}
			else
				{
				_assertEqual($DefaultAddress,_getText(_div("mini-address-name", _in(_listItem("/Default Billing Address /")))));
				}
	 	 }
	  	else
	 		 {
	 			 _assert(false,"Add address Overlay is not displaying"); 
	 		 }
	//Navigating to shipping page
	_mouseOver(_link("mini-cart-link"));
	_click(_link("mini-cart-link-checkout"));

	//Verifying shipping field values and drop down values
	ShouldNotPrepopulateShippingValues();
	//Verifying billing drop down
	if($j==0)
		{
		_setSelected(_select("dwfrm_singleshipping_addressList"),1);
		VerifyingShippingDropdown(0);
		}
		else
			{
			_setSelected(_select("dwfrm_singleshipping_addressList"),2);
			VerifyingShippingDropdown(1);	
			}
	

	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));	

	if(_isVisible(_div("address-validation-dialog")))
	{
		_click(_submit("ship-to-original-address"));
	}
	
	//Verifying billing drop down values
	PrepopulateBillingValues($j);
	if ($j==0)
		{
		//billing Drop down
		VerifyingBillingDropdown(0);
		}
		else
			{
			VerifyingBillingDropdown(1);
			}

		}
 
 VerifyMakeDefaultBillingAddress(0);
 VerifyMakeDefaultBillingAddress(1);
 
 }
 catch($e)
 {
 	_logExceptionAsFailure($e);
 }	
  $t.end();
  
 
  //Deleting Address
  deleteAddress();
  
  var $t = _testcase("157169", "Verify behavior when another address added in profile section by checked the checkbox for both default shiiping and billing.");
  $t.start();
  try
  {
	  function MakeDefaultShippingAndBillingAddress($j)	
		{
	//Navigating to address page
	_click(_link("user-account"));
	_click(_link("Addresses"));
	 //Creating address and checking default billing check box
	 _click(_link("Create New Address"));
	 if(_isVisible(_div("dialog-container")))
	 	{
		 addAddress($Valid_Address,$j);
		//Checking billing default address check box
		_check(_checkbox("dwfrm_profile_address_defaultshipping"));
		_check(_checkbox("dwfrm_profile_address_defaultbilling"));
		_click(_submit("Apply"));
		
		var $DefaultAddress=$Valid_Address[$j][2]+" "+$Valid_Address[$j][3];
		
		if ($j==0)
			{
			_assertEqual($DefaultAddress,_getText(_div("mini-address-name", _in(_listItem("/Default Shipping Address Default Billing Address /")))));
			}
			else
				{
				_assertEqual($DefaultAddress,_getText(_div("mini-address-name", _in(_listItem("/Default Shipping Address Default Billing Address /")))));
				}
	 	 }
	  	else
	 		 {
	 			 _assert(false,"Add address Overlay is not displaying"); 
	 		 }
	//Navigating to shipping page
	_mouseOver(_link("mini-cart-link"));
	_click(_link("mini-cart-link-checkout"));

	//Verifying shipping field values and drop down values
	PrepopulateShippingValues($j);
	if ($j==0)
		{
		//Shipping Drop down
		VerifyingShippingDropdown(0);
		}
		else
			{
			VerifyingShippingDropdown(1);
			}

	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));	

	if(_isVisible(_div("address-validation-dialog")))
	{
		_click(_submit("ship-to-original-address"));
	}
	
	//Verifying billing drop down values
	PrepopulateBillingValues($j);
	if ($j==0)
		{
		//billing Drop down
		VerifyingBillingDropdown(0);
		}
		else
			{
			VerifyingBillingDropdown(1);
			}
		}
	  
  MakeDefaultShippingAndBillingAddress(0);
  MakeDefaultShippingAndBillingAddress(1);	
	  
  }
  catch($e)
  {
  	_logExceptionAsFailure($e);
  }	
   $t.end();
  
   //Deleting Address
   deleteAddress(); 
   
   var $t = _testcase("157476", "Verify the multi shipping page when user save default shipping address in profile.");
   $t.start();
   try
   {

//Navigating to address page
_click(_link("user-account"));
_click(_link("Addresses"));
//Creating address and checking default billing check box
_click(_link("Create New Address"));

if(_isVisible(_div("dialog-container")))
	{
	 addAddress($Valid_Address,0);
	//Checking billing default address check box
	_check(_checkbox("dwfrm_profile_address_defaultshipping"));
	_click(_submit("Apply"));
	 }
	else
		 {
			 _assert(false,"Add address Overlay is not displaying"); 
		 }

//Navigating to shipping page
navigateToShippingPage($data[1][0],1);
//Verifying shipping field values and drop down values
PrepopulateShippingValues(0);
//click on ship to multiple address page
_click(_submit("dwfrm_singleshipping_shipToMultiple"));
//multi shipment page heading
_assertVisible(_div("ship-to-single"));	
var $count=_count("_row","/cart-row/",_in(_table("item-list")));

var $expectedMultiShippingAddress="("+$Valid_Address[0][1]+")"+" "+$Valid_Address[0][4]+","+" "+$Valid_Address[0][8]+","+" "+$Valid_Address[0][7]+","+" "+$Valid_Address[0][9];

for(var $i=0;$i<$count;$i++)
	{
	_assertVisible(_select("select-address required["+$i+"]"));
	_setSelected(_select("select-address required["+$i+"]"), 1);
	var $SelectedText=_getSelectedText(_select("select-address required["+$i+"]"));
	_assertEqual($expectedMultiShippingAddress,$SelectedText);
	}
_click(_submit("button-fancy-large"));
//Selecting address in billing page
_setSelected(_select("dwfrm_billing_addressList"),1);
//Entering payment details
PaymentDetails($PaymentData,4);
//click on save button
_click(_submit("dwfrm_billing_save"));
//Verifying order summary page
verifyOrderSummary();
//click on submit order
_click(_submit("submit"));
//Order Confirmation
if (_isVisible(_image("CloseIcon.png")))
	{
	_click(_image("CloseIcon.png"));
	}
//Order Confirmation page
_assertVisible(_heading1($data[0][1]));
_assertVisible(_heading2($data[1][1]));
_assertVisible(_table("order-shipment-table"));

}
catch($e)
   {
   	_logExceptionAsFailure($e);
   }	
    $t.end();
 

    
    
 //##################################### GLOBAL FUNCTIONS #########################################
    
 function PrepopulateShippingValues($i)
 {
	  
 _assertEqual($Valid_Address[$i][2],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName")));
 _assertEqual($Valid_Address[$i][3],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName")));
 _assertEqual($Valid_Address[$i][4],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1")));
 _assertEqual($Valid_Address[$i][5],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2")));
 _assertEqual($Valid_Address[$i][6],_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_country")));
 _assertEqual($Valid_Address[$i][7],_getValue(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
 _assertEqual($Valid_Address[$i][8],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")));
 _assertEqual($Valid_Address[$i][9],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")));
 _assertEqual($Valid_Address[$i][10],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"))); 
	 
 }
 
 function PrepopulateBillingValues($i)
 {
	 
  _assertEqual($Valid_Address[$i][2],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
  _assertEqual($Valid_Address[$i][3],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
  _assertEqual($Valid_Address[$i][4],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
  _assertEqual($Valid_Address[$i][5],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address2")));
  _assertEqual($Valid_Address[$i][6],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
  _assertEqual($Valid_Address[$i][7],_getValue(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
  _assertEqual($Valid_Address[$i][8],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
  _assertEqual($Valid_Address[$i][9],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_postal")));
  _assertEqual($Valid_Address[$i][10],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_phone"))); 
	 
 }
 
 
 function ShouldNotPrepopulateShippingValues()
 {
	  
 _assertEqual($Valid_Address[0][0],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName")));
 _assertEqual($Valid_Address[0][0],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName")));
 _assertEqual($Valid_Address[0][0],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1")));
 _assertEqual($Valid_Address[0][0],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2")));
 _assertEqual($Valid_Address[0][0],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")));
 _assertEqual($Valid_Address[0][0],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")));
 _assertEqual($Valid_Address[0][0],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"))); 
	 
 }
 
 
 function ShouldNotPrepopulateBillingValues()
 {
	 
 _assertEqual($Valid_Address[0][0],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
 _assertEqual($Valid_Address[0][0],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
 _assertEqual($Valid_Address[0][0],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
 _assertEqual($Valid_Address[0][0],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address2")));
 _assertEqual($Valid_Address[0][0],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
 _assertEqual($Valid_Address[0][0],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_postal")));
 _assertEqual($Valid_Address[0][0],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_phone"))); 
	 
 }
 
 function VerifyingShippingDropdown($i)
 {

//Select Saved Address from drop down
 _setSelected(_select("dwfrm_billing_addressList"), "1");
 
 var $dropdownvalueShipping=_getSelectedText(_select("dwfrm_singleshipping_addressList"));
 //var $expectedAddressShipping="("+$Valid_Address[$i][1]+")"+" "+$Valid_Address[$i][4]+","+" "+$Valid_Address[$i][8]+","+" "+$Valid_Address[$i][7]+","+" "+$Valid_Address[$i][9];
 var $expectedAddressShipping="("+$Valid_Address[$i][1]+")"+" "+$Valid_Address[$i][4]+" "+$Valid_Address[$i][8]+" "+$Valid_Address[$i][7]+" "+$Valid_Address[$i][9];
 _assertEqual($expectedAddressShipping,$dropdownvalueShipping); 
 }
 
 function VerifyingBillingDropdown($i)
 {	 
 var $dropdownvalueBilling=_getSelectedText(_select("dwfrm_billing_addressList"));
 //var $expectedAddressBilling="("+$Valid_Address[$i][1]+")"+" "+$Valid_Address[$i][4]+","+" "+$Valid_Address[$i][8]+","+" "+$Valid_Address[$i][7]+","+" "+$Valid_Address[$i][9];
 var $expectedAddressBilling="("+$Valid_Address[$i][1]+")"+" "+$Valid_Address[$i][4]+" "+$Valid_Address[$i][8]+" "+$Valid_Address[$i][7]+" "+$Valid_Address[$i][9];
  _assertEqual($expectedAddressBilling,$dropdownvalueBilling); 
 } 
