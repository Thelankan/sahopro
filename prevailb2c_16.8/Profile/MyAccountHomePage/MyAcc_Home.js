_include("../../util.GenericLibrary/BrowserSpecific.js");
_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("MyAcc_Home.xls");
var $MyAcc_Home=_readExcelFile("MyAcc_Home.xls","MyAcc_Home");

SiteURLs()
cleanup();
_setAccessorIgnoreCase(true);
//login to the application
_click(_link("Login"));
login();

var $t = _testcase("124634", "Verify the UI  of 'My Account home page in application as a  Registered user");
$t.start();
try
{
	//verify the UI of my account home page
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	_assertVisible(_link($MyAcc_Home[0][0], _in(_div("breadcrumb"))));
	_assertVisible(_heading1("/"+$MyAcc_Home[0][0]+"/"));
	//logged in or not
	_assertVisible(_link("user-account"));
	//account landing section
	for(var $i=0;$i<5;$i++)
		{
		//personal data
		_assertVisible(_heading2($MyAcc_Home[$i][1]));
		//_assertVisible(_paragraph("/(.*)/",_under(_heading2($MyAcc_Home[$i][1]))));
		//_assertVisible(_link($MyAcc_Home[0][7]), _under(_heading2("Personal Data")))
		}
	//left nav section
	var $Heading=_count("_span", "/(.*)/",_in(_div("secondary-navigation")));
	var $Links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
	       {
	              _assertVisible(_span($MyAcc_Home[$i][2]));                
	       }
	//checking links
	for(var $i=0;$i<$Links;$i++)
	       {
	              _assertVisible(_link($MyAcc_Home[$i][3]));                
	       }
	//need help section
	_assertVisible(_heading2("/"+$MyAcc_Home[1][0]+"/"));
	_assertVisible(_div("account-nav-asset"));
	//contact us link
	_assertVisible(_link($MyAcc_Home[2][0]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124630/148643/148644/148645/148646/148647/148648/148649", "Verify the navigation related to  'Personal Data'/Address/payment settings/order history/modify wish list/search Wish Lists/create registry/Search Registries/Modify Registries/privacy policy/secure shopping  link  present in left nav on My account home page in application");
$t.start();
try
{
//left nav links	
var $Links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
for(var $i=0;$i<$Links;$i++)
	{
		//navigate to my account home page
		_click(_link("user-account"));
		_click(_link("My Account"));
		_click(_link($MyAcc_Home[$i][3],_in(_listItem($MyAcc_Home[$i][3]))));
		//click on link personal data
		if($i==0)
			{
			//verify the navigation
			_assertVisible(_heading1($MyAcc_Home[$i][4]));
			_assertVisible(_div("breadcrumb"));
			_assertVisible(_link($MyAcc_Home[$i][4], _in(_div("breadcrumb"))));
			}
		//click on link Address
		if($i==1)
			{
			//verify the navigation
			_assertVisible(_heading1($MyAcc_Home[$i][4]));
			_assertVisible(_div("breadcrumb"));
			_assertVisible(_link($MyAcc_Home[$i][4], _in(_div("breadcrumb"))));
			}
		//click on link payment settings
		if($i==2)
			{
			//verify the navigation
			_assertVisible(_heading1($MyAcc_Home[$i][4]));
			_assertVisible(_div("breadcrumb"));
			_assertVisible(_link($MyAcc_Home[2][3], _in(_div("breadcrumb"))));
			}
		//click on link order history
		if($i==3)
			{
			//verify the navigation
			_assertVisible(_heading1($MyAcc_Home[$i][4]));
			_assertVisible(_div("breadcrumb"));
			_assertVisible(_link($MyAcc_Home[$i][4], _in(_div("breadcrumb"))));
			}
		//click on link modify wish list & Search Wish Lists
		if($i==4 || $i==5)
			{
			//verify the navigation
			_assertVisible(_heading1($MyAcc_Home[4][4]));
			_assertVisible(_div("breadcrumb"));
			_assertVisible(_link($MyAcc_Home[4][5], _in(_div("breadcrumb"))));
			}
		//click on link create registry & Search Registries & Modify Registries
		if($i==6 || $i==7 || $i==8)
			{
			//verify the navigation 
			_assertVisible(_heading1("/"+$MyAcc_Home[6][4]+"/"));
			_assertVisible(_div("breadcrumb"));
			_assertVisible(_link($MyAcc_Home[6][5], _in(_div("breadcrumb"))));
			}
		//click on link privacy policy
		if($i==9)
			{
			//verify the navigation 
			_assertVisible(_heading1($MyAcc_Home[$i][4]));
			_assertContainsText($MyAcc_Home[$i][4], _div("breadcrumb"));
			
			}
		//click on link Secure Shopping
		if($i==10)
			{
			//verify the navigation 
			_assertVisible(_heading1($MyAcc_Home[$i][4]));
			_assertContainsText($MyAcc_Home[$i][4], _div("breadcrumb"));
			}
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148650", "Verify the navigation related to  'Contact Us'  link  present in left nav on My account home page in application");
$t.start();
try
{
	//navigate to my account home page
	_click(_link("user-account"));
	_click(_link("My Account"));
	//click on contact us link
	_click(_link($MyAcc_Home[2][0]));
	//verify the navigation
	_assertVisible(_heading1("/"+$MyAcc_Home[2][0]+"/"));
	_assertVisible(_link($MyAcc_Home[3][0], _in(_div("breadcrumb"))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148651/148652/148653/148654/148655", "Verify the navigation related to  'Personal Data'/orders/address/wishlist/payment settings/gift registry menu on my account home page in application");
$t.start();
try
{
//navigate to my account home page
_click(_link("user-account"));	
_click(_link("My Account"));
//fetch account-options links
var $account_options=_collectAttributes("_link","/(.*)/","sahiText",_in(_list("account-options")));
for(var $i=0;$i<$account_options.length;$i++)
	{
	//navigate to my account home page
	_click(_link("user-account"));
	_click(_link("My Account"));
	//clicking on account-option links
	_click(_link($account_options[$i]));
	//personal data
	if($i==0)
		{
		//verify the navigation
		_assertVisible(_heading1($MyAcc_Home[$i][4]));
		_assertVisible(_link($MyAcc_Home[$i][4], _in(_div("breadcrumb"))));
		}
	//Orders
	if($i==1)
		{
		_assertVisible(_heading1($MyAcc_Home[$i][6]));
		_assertVisible(_link($MyAcc_Home[$i][6], _in(_div("breadcrumb"))));
		}
	//Addresses
	if($i==2)
		{
		_assertVisible(_heading1($MyAcc_Home[$i][6]));
		_assertVisible(_link($MyAcc_Home[$i][6], _in(_div("breadcrumb"))));
		}
	//wish list
	if($i==3)
		{
		//verify the navigation
		_assertVisible(_heading1($MyAcc_Home[4][4]));
		_assertVisible(_link($MyAcc_Home[4][5], _in(_div("breadcrumb"))));
		}
	//payment settings
	if($i==4)
		{
		_assertVisible(_heading1($MyAcc_Home[2][4]));
		_assertVisible(_link($MyAcc_Home[2][3], _in(_div("breadcrumb"))));
		}
	//gift registry
	if($i==5)
		{
		//verify the navigation 
		_assertVisible(_heading1("/"+$MyAcc_Home[6][4]+"/"));
		_assertVisible(_link($MyAcc_Home[6][5], _in(_div("breadcrumb"))));
		}
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

if(!isMobile() || mobile.iPad())
	{
var $t = _testcase("148656/148657/148658/148659", "Verify the functionality related to 'MY ACCOUNT'/order information/wishlist/gift registries/shop confidently down arrow present in left Nav on the My account home page in application");
$t.start();
try
{
var $j=1;
//fetching the no.of headings in content assert
var $headings=_collectAttributes("_span","/toggle/","sahiText",_in(_div("secondary-navigation")));
for(var $i=0;$i<$headings.length;$i++)
	{
		//fetch links present in respective toggle's
		if($i==$headings.length-1)
			{
				$g=$i-1;
				var $links=_collectAttributes("_listItem","/.*/","sahiText",_in(_list($i,_in(_div("content-asset")))));
				_click(_span($headings[$i]));
				for(var $k=0;$k<$links.length;$k++)
					{
						if($links[$k]!="")
							{
								_assertNotVisible(_listItem($links[$k]));
							}						
					}
			}
		else
			{
				var $links=_collectAttributes("_listItem","/.*/","sahiText",_in(_list($i,_in(_div("content-asset")))));
				_click(_span($headings[$i]));
				for(var $k=0;$k<$links.length;$k++)
					{
						if($links[$k]!="")
							{
								_assertNotVisible(_listItem($links[$k]));
							}
					}
				$j++;
			}	
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
	}

/*
var $t = _testcase("124648", "Verify the functionality related to 'Logout' link on My Account Home page in application");
$t.start();
try
{
	//click on link log out
	_click(_link("user-account"));
	_wait(4000,_isVisible(_link("logout")));
	while(_isVisible(_link("logout")))
		{
		_click(_link("logout"));
		}
	_click(_link("Login"));
	//verify the functionality
	_wait(4000,_isVisible(_heading1($MyAcc_Home[4][0])))
	_assertVisible(_heading1($MyAcc_Home[4][0]));
	_assertVisible(_link("Login"));
	_assertVisible(_div("breadcrumb"));
	_assertVisible(_link($MyAcc_Home[0][0], _in(_div("breadcrumb"))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/
cleanup();