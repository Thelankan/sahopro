_include("../../util.GenericLibrary/GlobalFunctions.js");
_include("../../util.GenericLibrary/BrowserSpecific.js");
_resource("WishList.xls");

SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();


var $Wishlist_Login=_readExcelFile("WishList.xls","Wishlist_Login");
var $Validations=_readExcelFile("WishList.xls","Validations");
var $Login_Validation=_readExcelFile("WishList.xls","Login_Validations");
var $Wishlist_Reg=_readExcelFile("WishList.xls","Wishlist_Reg");


//adding items to wish list for particular account
additemsToWishListAccount($Wishlist_Reg[0][0]);



var $t = _testcase("----/124426", "Verify the UI of 'Returning customers' section wishlist login page in application as an Anonymous user" +
		  "Verify the navigation of 'WISH LIST' link at the header as a guest user.");
$t.start();
try
{
	//navigating to wish list login  page
	_click(_link("Wish List"));
	//verify bread crumb
	_assertVisible(_link($Wishlist_Login[2][1], _in(_div("breadcrumb"))));
	//Verifying the UI of returning customer section
	//MyAccount login as heading
	_assertVisible(_heading1($Wishlist_Login[3][0]));
	//Returning customer heading
	_assertVisible(_heading2($Wishlist_Login[4][0]));
	//Paragraph with static text
	_assertVisible(_paragraph("/(.*)/",_in(_div("login-box-content returning-customers clearfix"))));
	//email Address
	_assertVisible(_label($Wishlist_Login[6][0]));
	_assertVisible(_textbox("/dwfrm_login_username/"));
	_assertEqual("", _getValue(_textbox("/dwfrm_login_username/")));
	//password
	_assertVisible(_label($Wishlist_Login[7][0]));
	_assertVisible(_password("/dwfrm_login_password/"));
	_assertEqual("", _getValue(_password("/dwfrm_login_password/")));
	//remember me
	_assertVisible(_label("Remember Me"));
	_assertVisible(_checkbox("dwfrm_login_rememberme"));
	_assertNotTrue(_checkbox("dwfrm_login_rememberme").checked);
	//login button
	_assertVisible(_submit("Login"));
	//forgot password
	_assertVisible(_link("password-reset"));
	_assertEqual($Wishlist_Login[8][0], _getText(_link("password-reset")));
	//social links
	_assertVisible(_div("login-box-content returning-customers clearfix"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("124806","Verify the UI of 'Create a wishlist' section on wishlist login page in application as an anonymous user");
$t.start();
try
{
	//verify the UI of create wish list section
	_assertVisible(_heading2($Wishlist_Login[0][6]));
	_assertVisible(_div("content-asset"));
	_assertVisible(_submit("dwfrm_login_register"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148695","Verify the UI of  'Find Someone's Wish List'  section on wishlist login page in application as an anonymous user");
$t.start();
try
{
	//verify the UI of 'Find Someone's Wish List'
	_assertVisible(_heading2($Wishlist_Login[1][6]));
	_assertVisible(_paragraph("/(.*)/",_in(_div("login-box-content"))));
	//last name
	_assertVisible(_span($Wishlist_Login[2][6]));
	_assertVisible(_textbox("dwfrm_wishlist_search_lastname"));
	//first name
	_assertVisible(_span($Wishlist_Login[3][6]));
	_assertVisible(_textbox("dwfrm_wishlist_search_firstname"));
	//email 
	_assertVisible(_label($Wishlist_Login[4][6]));
	_assertVisible(_textbox("dwfrm_wishlist_search_email"));
	//find button
	_assertVisible(_submit("dwfrm_wishlist_search_search"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124427","Verify the UI of  'Wishlist login page' in application as an anonymous user");
$t.start();
try
{
	//verify the UI
	_assertVisible(_link($Wishlist_Login[2][1], _in(_div("breadcrumb"))));
	_assert(_isVisible(_heading1($Wishlist_Login[3][0])));
	//left nav section
	_assertVisible(_span($Wishlist_Login[5][6]));
	_assertVisible(_link($Wishlist_Login[2][5]));
	_assertVisible(_span($Wishlist_Login[6][6]));
	_assertVisible(_link($Wishlist_Login[7][6]));
	_assertVisible(_link($Wishlist_Login[8][6]));
	//need help section
	_assertVisible(_heading2($Wishlist_Login[9][6]));
	_assertVisible(_div("content-asset",_in(_div("account-nav-asset"))));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124428","Verify the navigation related to 'Home' and 'My Account' links in Wish list login page bread crumb in application as an  Anonymous user");
$t.start();
try
{
	//click on home link present in the bread crumb
	//_click(_link("Home",_in(_div("breadcrumb"))));
	
	//Navigate to home page
	_click(_link("Demandware SiteGenesis"));
	
	//verify the navigation to home page
	_assertVisible(_list("homepage-slides"));
	_assertVisible(_div("home-bottom-slots"));
	//navigating to wish list login page
	_click(_link("Wish List"));
	//click on my account link present in the bread crumb
	_click(_link($Wishlist_Login[1][1],_in(_div("breadcrumb"))));
	//verify the navigation
	_assertVisible(_link($Wishlist_Login[1][1], _in(_div("breadcrumb"))));
	_assertVisible(_heading1($Wishlist_Login[3][0]));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148696","Verify the navigation related to 'Forgot Password' link on on Wishlist login page returning customer section in application as an anonymous user.");
$t.start();
try
{
	//navigate to wish list page
	_click(_link("Wish List"));
	//Checking Bread crumb
	_assertVisible(_link($Wishlist_Login[0][0], _in(_div("breadcrumb"))));
	//wish list page
	_assertVisible(_heading1($Wishlist_Login[3][0]));
	//clicking on forgot password link
	_click(_link("/Forgot Password/"));
	_assertVisible(_div("dialog-container"));
	_assertVisible(_div("dialog-content ui-dialog-content ui-widget-content"));
	_assertVisible(_heading1($Wishlist_Login[2][0]));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("----","Verify the functionality �Send� button related to Forgot password overlay on Wish list login page returning customer section in application as an Anonymous user");
$t.start();
try
{
	//entering valid email address
	_setValue(_textbox("dwfrm_requestpassword_email"),$Wishlist_Login[2][4]);	
	//click on send button
	_click(_submit("dwfrm_requestpassword_send"));
	//verify the navigation
	_assertVisible(_heading1($Wishlist_Login[12][1]));
	_assertVisible(_link("Go to the home page"));
	//close the dialog box
	_click(_button("Close"));	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("124429","Verify the navigation related to 'Create an account' link  on on Wishlist login page left nav in application as an anonymous user");
$t.start();
try
{
	_click(_link("Wish List"));
	//click on link create an account
	_click(_link($Wishlist_Login[2][5]));
	//verify the navigation
	_assertVisible(_heading1($Wishlist_Login[0][5]));
	_assertVisible(_link($Wishlist_Login[1][5], _in(_div("breadcrumb"))));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148697","Verify the navigation related to 'Privacy Policy'  link  on on Wishlist login page left nav in application as an anonymous user");
$t.start();
try
{
	//navigate to wish list page
	_click(_link("Wish List"));
	//click on privacy policy link
	_click(_link($Wishlist_Login[4][1]));
	//verify the navigation
	_assertVisible(_heading1($Wishlist_Login[4][1]));
	_assertVisible(_div("content-asset"));
	_assertVisible(_div("breadcrumb"));
	_assertContainsText($Wishlist_Login[4][1], _div("breadcrumb"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148698","Verify the navigation related to  'Secure Shopping'  link  on on Wishlist login page left nav in application as an anonymous user");
$t.start();
try
{
	//navigate to wish list page
	_click(_link("Wish List"));
	//click on Secure Shopping link	
	_click(_link($Wishlist_Login[8][6]));
	//verify the navigation
	_assertVisible(_div("breadcrumb"));
	_assertContainsText($Wishlist_Login[5][1], _div("breadcrumb"));
	_assertVisible(_heading1($Wishlist_Login[5][1]));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148699","Verify the navigation related to 'Contact Us'  link  on on Wishlist login page left nav in application as an anonymous user");
$t.start();
try
{
//navigate to wish list page
_click(_link("Wish List"));
//click on contact us link
_click(_link($Wishlist_Login[6][1]));
//verify the navigation
_assertVisible(_heading1("/"+$Wishlist_Login[6][1]+"/"));
_assertVisible(_div("breadcrumb"));
_assertContainsText($Wishlist_Login[9][1], _div("breadcrumb"));
_assertVisible(_submit("dwfrm_contactus_send"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124504","Verify the UI of 'Forgot Password' overlay  on wishlist login page in application as an  Anonymous user");
$t.start();
try
{
	//navigate to wish list page
	_click(_link("Wish List"));
	//click on forgot password link
	_click(_link("password-reset"));
	//verify the overlay appearance
	_assertVisible(_span("ui-dialog-title"));
	_assertVisible(_heading1($Wishlist_Login[7][1]));
	_assertVisible(_textbox("dwfrm_requestpassword_email"));
	_assertVisible(_submit("dwfrm_requestpassword_send"));
	_assertVisible(_span("CLOSE"));
	//close the overlay
	_click(_span("CLOSE"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124505","Verify the navigation related to 'Create an account now' button on Wishlist login page create a wishlist section  in application as an anonymous user");
$t.start();
try
{
	_click(_link("Wish List"));
	//click on link Create an account now button
	_click(_submit("dwfrm_login_register"));
	//verify the navigation
	_assertVisible(_heading1($Wishlist_Login[0][5]));
	_assertVisible(_link($Wishlist_Login[1][5], _in(_div("breadcrumb"))));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148703/148704/124506/148705","Verify the validation related to the 'Last name',First name,email id field/'Find' button functionality on wish list login page, find someone's wish list section in application as a anonymous user");
$t.start();
try
{
	_click(_link("Wish List"));
	//validating the fields last name,first name,email 
	for(var $i=0;$i<$Validations.length;$i++)
	  {
		if($i==6)
		{
			_setValue(_textbox("dwfrm_wishlist_search_lastname"), $LName);
			_setValue(_textbox("dwfrm_wishlist_search_firstname"), $FName);
			_setValue(_textbox("dwfrm_wishlist_search_email"),$uId);
		}
		else
		{
			_setValue(_textbox("dwfrm_wishlist_search_lastname"), $Validations[$i][1]);
			_setValue(_textbox("dwfrm_wishlist_search_firstname"), $Validations[$i][2]);
			_setValue(_textbox("dwfrm_wishlist_search_email"), $Validations[$i][3]);
		}	
		//Blank
		if($i==0 || $i==2 || $i==3 || $i==4 || $i==5 || $i==6)
		{
			//click on find button
			_click(_submit("dwfrm_wishlist_search_search"));
			//user should be in same page
			_assertVisible(_heading1($Wishlist_Login[1][0]));
			//verify the text message
		if($i==6)
			{
			var $expText=$LName+" "+$FName+" - "+"View";
			_assertVisible(_row($expText));
			_assertVisible(_link("View"));
			}
		else
			{
			_assertVisible(_paragraph("/No Wish List has been found for/"));
			}
		}
		//max characters
		if($i==1)
			{
			_assertEqual($Validations[1][4],_getText(_textbox("dwfrm_wishlist_search_lastname")).length);
			_assertEqual($Validations[1][4],_getText(_textbox("dwfrm_wishlist_search_firstname")).length);
			_assertEqual($Validations[1][4],_getText(_textbox("dwfrm_wishlist_search_email")).length);
			}
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124430/124431/148702","Verify the validation related to the 'Email address'/'password' field/'LOGIN' button functionality on Wish list login page, Returning customers section in application as a Anonymous user");
$t.start();
try
{
	_click(_image("Demandware SiteGenesis"));
	_click(_link("Wish List"));
	//validating the email address and password fields	
	for(var $i=0;$i<$Login_Validation.length;$i++)
		{
		if($i==5)
			{
			login();
			}
		else
			{
		_setValue(_textbox("/dwfrm_login_username/"),$Login_Validation[$i][1]);
		_setValue(_password("/dwfrm_login_password/"),$Login_Validation[$i][2]);
		_click(_submit("dwfrm_login_login"));
			}
		//blank
		if($i==0)
			{
			//username
			_assertEqual($Login_Validation[$i][5],_style(_textbox("/dwfrm_login_username/"),"background-color"));
			_assertVisible(_span($Login_Validation[$i][3]));
			//password
			_assertVisible(_span($Login_Validation[$i][4]));
			_assertEqual($Login_Validation[$i][5],_style(_password("/dwfrm_login_password/"),"background-color"));
			}
		//valid
		else if($i==5)
			{
			_assertVisible(_heading1($Wishlist_Login[1][0]));
			}
		//invalid email id
		else if($i==1||$i==2||$i==3)
		{
			//username
			_assertEqual($Login_Validation[0][5],_style(_textbox("/dwfrm_login_username/"),"background-color"));
			_assertVisible(_span($Login_Validation[2][3]));
		}
		//invalid 
		else
			{
			_assertVisible(_div($Login_Validation[1][3]));
			}
		}
	//logout from application
	_click(_link("user-account"));
	_click(_link("Logout"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148706/124508","Verify the UI of empty Wish List search' page in application as an anonymous user");
$t.start();
try
{
	//navigate to wish list page
	_click(_link("Wish List"));
	_setValue(_textbox("dwfrm_wishlist_search_email"), $Wishlist_Login[2][4]);
	//click on find button
	_click(_submit("dwfrm_wishlist_search_search"));
	//verify the UI of empty Wish List search
	_assertVisible(_div("breadcrumb"));
	_assertVisible(_link($Wishlist_Login[0][0], _in(_div("breadcrumb"))));
	_assertVisible(_heading1($Wishlist_Login[1][0]));
	_assertVisible(_div("form-inline"));
	//left nav section
	_assertVisible(_span($Wishlist_Login[5][6]));
	_assertVisible(_link($Wishlist_Login[2][5]));
	_assertVisible(_span($Wishlist_Login[6][6]));
	_assertVisible(_link($Wishlist_Login[7][6]));
	_assertVisible(_link($Wishlist_Login[8][6]));
	//need help section
	_assertVisible(_heading2($Wishlist_Login[9][6]));
	_assertVisible(_div("content-asset",_in(_div("account-nav-asset"))));
	_assertVisible(_link("Contact Us"));
	//fields
	_assertVisible(_label($Wishlist_Login[2][7]));
	_assertVisible(_label($Wishlist_Login[3][7]));
	_assertVisible(_label($Wishlist_Login[4][7]));
	_assertVisible(_textbox("dwfrm_wishlist_search_lastname"));
	_assertEqual("", _getValue(_textbox("dwfrm_wishlist_search_lastname")));
	_assertVisible(_textbox("dwfrm_wishlist_search_firstname"));
	_assertEqual("", _getValue(_textbox("dwfrm_wishlist_search_firstname")));
	_assertVisible(_textbox("dwfrm_wishlist_search_email"));
	_assertEqual("", _getValue(_textbox("dwfrm_wishlist_search_email")));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();	


var $t=_testcase("148707/124509","Verify the UI of  non empty Wish List search' page in application as an anonymous user");
$t.start();
try
{
	//navigate to wish list page
	_click(_link("Wish List"));
	_setValue(_textbox("dwfrm_wishlist_search_lastname"), $LName);
	_setValue(_textbox("dwfrm_wishlist_search_firstname"), $FName);
	_setValue(_textbox("dwfrm_wishlist_search_email"),$uId);
	
	//click on find button
	_click(_submit("dwfrm_wishlist_search_search"));
	//verify the UI of non empty Wish List search
	_assertVisible(_div("breadcrumb"));
	_assertVisible(_link($Wishlist_Login[0][0], _in(_div("breadcrumb"))));
	_assertVisible(_heading1($Wishlist_Login[1][0]));
	var $expText=$LName+" "+$FName+" - "+"View";
	_assertVisible(_row($expText));
	//last names
	_assertVisible(_cell("last-name"));
	_assertEqual($LName, _getText(_cell("last-name")));
	//first name
	_assertVisible(_cell("first-name"));
	_assertEqual($FName, _getText(_cell("first-name")));
	_assertVisible(_cell("city"));
	_assertVisible(_span("View"));
	//left nav section
	_assertVisible(_span($Wishlist_Login[5][6]));
	_assertVisible(_link($Wishlist_Login[2][5]));
	_assertVisible(_span($Wishlist_Login[6][6]));
	_assertVisible(_link($Wishlist_Login[7][6]));
	_assertVisible(_link($Wishlist_Login[8][6]));
	//need help section
	_assertVisible(_heading2($Wishlist_Login[9][6]));
	_assertVisible(_div("content-asset",_in(_div("account-nav-asset"))));
	_assertVisible(_link("Contact Us"));
	//fields
	_assertVisible(_label($Wishlist_Login[2][7]));
	_assertVisible(_label($Wishlist_Login[3][7]));
	_assertVisible(_label($Wishlist_Login[4][7]));
	_assertVisible(_textbox("dwfrm_wishlist_search_lastname"));
	_assertEqual("", _getValue(_textbox("dwfrm_wishlist_search_lastname")));
	_assertVisible(_textbox("dwfrm_wishlist_search_firstname"));
	_assertEqual("", _getValue(_textbox("dwfrm_wishlist_search_firstname")));
	_assertVisible(_textbox("dwfrm_wishlist_search_email"));
	_assertEqual("", _getValue(_textbox("dwfrm_wishlist_search_email")));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

//deleting items from wish list
deleteAddeditemsFromWishlistAccount();

cleanup();
