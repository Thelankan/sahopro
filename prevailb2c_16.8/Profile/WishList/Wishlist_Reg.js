_include("../../util.GenericLibrary/GlobalFunctions.js");
_include("../../util.GenericLibrary/BrowserSpecific.js");
_resource("WishList.xls");

var $Wishlist_Login=_readExcelFile("WishList.xls","Wishlist_Login");
var $Wishlist_Reg=_readExcelFile("WishList.xls","Wishlist_Reg");
var $Validations=_readExcelFile("WishList.xls","Validations");
var $addr_data=_readExcelFile("WishList.xls","Address");
var $sendtofriend=_readExcelFile("WishList.xls","Sendtofriend_Validation");

//function
function emptyWishlistUI()
{
	_assertVisible(_div("breadcrumb"));
	_assertVisible(_link($Wishlist_Login[0][0], _in(_div("breadcrumb"))));
	_assertVisible(_paragraph("/(.*)/", _in(_div("primary"))));
	//_assertVisible(_paragraph("/(.*)/",_under(_div("form-inline"))));
	//left nav section
	var $Heading=_count("_span", "/(.*)/",_in(_div("secondary-navigation")));
	var $Links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
	       {
	              _assertVisible(_span($Wishlist_Login[$i][9]));                
	       }
	//checking links
	for(var $i=0;$i<$Links;$i++)
	       {
	              _assertVisible(_link($Wishlist_Login[$i][10]));                
	       }
	//need help section
	_assertVisible(_heading2($Wishlist_Login[9][6]));
	_assertVisible(_div("content-asset",_in(_div("account-nav-asset"))));
	_assert(_isVisible(_link("Contact Us")));
	//fields
	_assertVisible(_label($Wishlist_Login[2][7]));
	_assertVisible(_label($Wishlist_Login[3][7]));
	_assertVisible(_textbox("dwfrm_wishlist_search_lastname"));
	_assertEqual("", _getValue(_textbox("dwfrm_wishlist_search_lastname")));
	_assertVisible(_textbox("dwfrm_wishlist_search_firstname"));
	_assertEqual("", _getValue(_textbox("dwfrm_wishlist_search_firstname")));
	_assertVisible(_textbox("dwfrm_wishlist_search_email"));
	_assertEqual("", _getValue(_textbox("dwfrm_wishlist_search_email")));
	//shipping address drop down
	_assertVisible(_label($Wishlist_Login[11][0]));
	_assertVisible(_select("editAddress"));
	_assertEqual($Wishlist_Login[11][1], _getSelectedText(_select("editAddress")));
	//gift certificate
	_assertVisible(_submit("dwfrm_wishlist_addGiftCertificate"));
	_assertVisible(_paragraph("/(.*)/",_near(_submit("dwfrm_wishlist_addGiftCertificate"))));
}


SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//adding items to wish list for particular account
additemsToWishListAccount($Wishlist_Reg[0][0]);

//login to the application
_click(_link("Login"));
login();

//navigate to wish list page
_click(_link("Wish List"));
if (_isVisible(_select("editAddress")))
	{
	_setSelected(_select("editAddress"),0);
	}
//deleting gift registry
Delete_GiftRegistry();

//deleting if any addresses are present
//navigate address page
deleteAddress();

var $address=new Array();
for(var $i=0;$i<$addr_data.length;$i++)
	{
		_wait(2000);
		//create new address
		_click(_link("Create New Address"));
		//adding address
		addAddress($addr_data,$i);
		_click(_submit("Apply"));
		//$address[$i]="("+$addr_data[$i][1]+")"+" "+$addr_data[$i][4]+", "+$addr_data[$i][8]+", "+$addr_data[$i][11]+", "+$addr_data[$i][9];
		$address[$i]="("+$addr_data[$i][1]+")"+" "+$addr_data[$i][4]+" "+$addr_data[$i][8]+" "+$addr_data[$i][11]+" "+$addr_data[$i][9];
	}
	
//verifying the presence of addresses
var $boolean=_isVisible(_list("address-list"));

var $t=_testcase("148713/148714","Verify the UI of empty Wish List search' page in application as an registered user");
$t.start();
try
{	
	//navigate to wish list page
	_click(_link("Wish List"));
	//Clear wish list
	while(_isVisible(_submit("/deleteItem/")))
		{
		_click(_submit("/deleteItem/"));
		}
	//verify the UI of empty Wish List search
	emptyWishlistUI();
	_assertVisible(_heading2($Wishlist_Login[12][0]));
	_assertVisible(_link($Wishlist_Login[13][0]));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148715/148716/124545","Verify the UI of non empty Wish List search' page in application as an registered user./Verify the functionality of 'Click here to start adding items.' link when no items are added to the wish list.");
$t.start();
try
{
	//add products to wish list by clicking on link Click here to start adding items.
	_click(_link($Wishlist_Login[13][0]));
	
	//verify the navigation to home page
	_assertVisible(_list("homepage-slides"));
	_assertVisible(_div("home-bottom-slots"));
	
	//navigate to sub category
	_click(_link($Wishlist_Reg[0][0]));
	var $pName=_getText(_link("name-link"));
	//click on name link
	_click(_link("name-link"));
	//select swatches
	selectSwatch();	
	//click on link add to wish list
	_click(_link("Add to Wishlist"));
	//verify the UI
	emptyWishlistUI();
	//when product is there
	//image
	_assertVisible(_image("/(.*)/",_in(_table("item-list"))));
	//name 
	_assertVisible(_link("/(.*)/",_in(_table("item-list"))));
	_assertEqual($pName, _getText(_link("/(.*)/",_in(_table("item-list")))));
	//product list item
	_assertVisible(_div("product-list-item"));
	//edit details link
	_assertVisible(_link("Edit Details"));
	//availability
	_assertVisible(_listItem("is-in-stock"));
	//date added
	_assertVisible(_div("form-row option-date-added"));
	//quantity
	_assertVisible(_div("/Quantity desired/"));
	//_assertVisible(_div("/QTY:/"));
	//priority
	_assertVisible(_span("Priority"));
	_assertVisible(_select("/dwfrm_wishlist_items/"));
	//update
	_assertVisible(_submit("button-text update-item"));
	//remove
	_assertVisible(_submit("button-text delete-item"));
	//quantity
	_assertVisible(_label("/QTY/"));
	_assertVisible(_numberbox("Quantity"));
	//add to cart
	_assertVisible(_submit("button-fancy-small add-to-cart"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124523","Verify the functionality related 'Your Wish List Shipping Address' dropdown on 'Wish List page in application as a Registered user");
$t.start();
try
{
//Your Wish List Shipping Address drop down
if($boolean)
	{
		//shipping address drop down 
		_assertVisible(_select("editAddress"));
		//select any address from drop down
		var $length=_getText(_select("editAddress")).length;
		var $j=0;
		_log($j);
		for(var $i=0;$i<$length;$i++)
			{
				_setSelected(_select("editAddress"),$i);
				if($i==0)
					{
						_assertEqual($Wishlist_Login[11][1], _getSelectedText(_select("editAddress")));
					}
				else
					{
						_log($j);
						//_assertEqual($address[$i], _getSelectedText(_select("editAddress")));
						$address="("+$addr_data[$j][1]+")"+" "+$addr_data[$j][4]+", "+$addr_data[$j][8]+", "+$addr_data[$j][11]+", "+$addr_data[$j][9];
						_assertEqual($address,_getSelectedText(_select("editAddress")));
						$j++;
					}
			}
	}
else
	{
		_assert(false,"addresses are not present");
	}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124525","Verify the functionality related 'Add Gift Certificate'button on'Wish List  page in application as a Registered user");
$t.start();
try
{
	//click on add gift certificate link
	_click(_submit("dwfrm_wishlist_addGiftCertificate"));
	//after clicking 'add gift certificate' button should not visible
	_assertNotVisible(_submit("dwfrm_wishlist_addGiftCertificate"));
	//verify the added gift certificate
	_assertVisible(_row("/Gift Certificate/"));
	//image
	_assertVisible(_image("/(.*)/",_in(_row("/Gift Certificate/"))));
	//link
	_assertVisible(_link("Gift Certificate"));
	_assertVisible(_div("/"+$Wishlist_Reg[0][1]+"/"));
	//date added
	_assertVisible(_div("/Date Added/",_in(_row("/Gift Certificate/"))));
	//remove link
	_assertVisible(_submit("/button-text delete-item/",_in(_row("/Gift Certificate/"))));
	//remove the added gift certificate
	_click(_submit("/button-text delete-item/",_in(_row("/Gift Certificate/"))));
	//after clicking on remove
	_assertVisible(_submit("dwfrm_wishlist_addGiftCertificate"));
	_assertNotVisible(_row("/Gift Certificate/"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148717/148718","Verify the navigation related to 'Home'/'My Account' link on 'Wish List page bread crumb in application as a Registered user");//Test Case Id Need Change(Vinay)
$t.start();
try
{
	//click on home link present in the bread crumb
	//_click(_link("Home",_in(_div("breadcrumb"))));
	
	//Navigate to home page
	_click(_link("Demandware SiteGenesis"));
	
	//verify the navigation to home page
	_assertVisible(_list("homepage-slides"));
	_assertVisible(_div("home-bottom-slots"));
	//navigating to wish list login page
	_click(_link("Wish List"));
	//click on my account link present in the bread crumb
	_click(_link($Wishlist_Login[1][1],_in(_div("breadcrumb"))));
	//verify the navigation
	_assertVisible(_link($Wishlist_Login[1][1], _in(_div("breadcrumb"))));
	_assertVisible(_heading1("/"+$Wishlist_Login[1][1]+"/"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124527/124507","Verify the navigation related to product name present below the 'Make This List Public' button wishlist page in application as a  Registered  user");
$t.start();
try
{
	//navigating to wish list login page
	_click(_link("Wish List"));
	//fetch the total no.of products
	var $count=_count("_div","/name/",_in(_table("item-list")));
	
	for(var $i=0;$i<$count;$i++)
		{
		var $productName=_getText(_link("/(.*)/",_in(_div("name ["+$i+"]"))));
		//click on name link
		_click(_link("/(.*)/",_in(_div("name ["+$i+"]"))));
		//verify the navigation
		_assertVisible(_span($productName, _in(_div("breadcrumb"))));
		_assertVisible(_heading1("product-name"));
		_assertEqual($productName, _getText(_heading1("product-name")));
		//navigating to wish list login page
		_click(_link("Wish List"));
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124547","Verify the navigation related to 'Edit details' link in on Wishlist page left nav in application as an registred user.");
$t.start();
try
{
	//navigating to wish list login page
	_click(_link("Wish List"));
	var $pName=_getText(_link("/(.*)/",_in(_table("item-list"))));
	//click on edit details link under wishlist product
	_click(_link("Edit Details"));
	//verify the navigation to respective PDP
	_assertVisible(_span($pName, _in(_div("breadcrumb"))));
	_assertVisible(_heading1("product-name"));
	_assertEqual($pName, _getText(_heading1("product-name")));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148722/148723/148724/148725","Verify the validation related to the 'Last name',First name,email id field/'Find' button functionality on wish list login page, find someone's wish list section in application as a registered user");
$t.start();
try
{
	//navigating to wish list login page
	_click(_link("Wish List"));
	//validating the fields last name,first name,email 
	for(var $i=0;$i<$Validations.length;$i++)
	  {
		_log($i);
		if($i==6)
			{
				_setValue(_textbox("dwfrm_wishlist_search_lastname"), $LName);
				_setValue(_textbox("dwfrm_wishlist_search_firstname"), $FName);
				_setValue(_textbox("dwfrm_wishlist_search_email"),$uId);
			}
		else
			{
				_setValue(_textbox("dwfrm_wishlist_search_lastname"), $Validations[$i][1]);
				_setValue(_textbox("dwfrm_wishlist_search_firstname"), $Validations[$i][2]);
				_setValue(_textbox("dwfrm_wishlist_search_email"), $Validations[$i][3]);
			}
		//Blank
		if($i==0 || $i==2 || $i==3 || $i==4 || $i==5 || $i==6)
		{
			//click on find button
			_click(_submit("dwfrm_wishlist_search_search"));
			//user should be in same page
			_assertVisible(_heading1($Wishlist_Login[1][0]));
			//verify the text message
			if($i==6)
				{
				var $expText=$LName+" "+$FName;
				_assertVisible(_row("/"+$expText+"/"));
				_assertVisible(_link("View"));
				}
			else
				{
				_assertVisible(_paragraph("/No Wish List has been found for/"));
				}
		}
		//max characters
		else if($i==1)
			{
			_assertEqual($Validations[1][4],_getText(_textbox("dwfrm_wishlist_search_lastname")).length);
			_assertEqual($Validations[1][4],_getText(_textbox("dwfrm_wishlist_search_firstname")).length);
			_assertEqual($Validations[1][4],_getText(_textbox("dwfrm_wishlist_search_email")).length);
			}	
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148719","Verify the functionality related to the 'Make This List Private' button on Wishlist page, find someone's wish list section in application as an registered user.");
$t.start();
try
{
	_click(_link("Wish List"));
	
	//_click(_link("View"));
	
	//verify the 'Make This List Private' functionality
	if(_isVisible(_submit("dwfrm_wishlist_setListPrivate")))
		{
		_click(_submit("dwfrm_wishlist_setListPrivate"));
		//_click(_submit("dwfrm_wishlist_setListPrivate"));
		}
	//'Make This List public' link should be there
	_assertVisible(_submit("dwfrm_wishlist_setListPublic"));
	_assertNotVisible(_div("Make this item Public"));
	
	//items should not be visible
	_setValue(_textbox("dwfrm_wishlist_search_email"),$Wishlist_Login[3][4]);
	_click(_submit("/dwfrm_wishlist_search/"));
	//wish list results table should not display for invalid credentials
	var $expText=$LName+" "+$FName;
	_assertEqual(false,_isVisible(_row("/"+$expText+"/"))); 
	_assertNotVisible(_link("View"));
	_assertVisible(_paragraph("/No Wish List has been found for/"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("140720","Verify the functionality related to the 'Make This List public' button on Wishlist page, find someone's wish list section in application as an registered user.");
$t.start();
try
{
	_click(_link("Wish List"));
	
	//_click(_link("View"));
	//verify the 'Make This List Public' functionality
	if (_isVisible(_submit("dwfrm_wishlist_setListPublic")))
		{
		_click(_submit("dwfrm_wishlist_setListPublic"));
		}
	//'Make This List private' link should be there
	_assertVisible(_submit("dwfrm_wishlist_setListPrivate"));
	_assertVisible(_div("Make this item Public"));
	//items should be visible
	_setValue(_textbox("dwfrm_wishlist_search_email"),$uId);
	_click(_submit("/dwfrm_wishlist_search/"));
	//wish list results table should not display for invalid credentials
	var $expText=$LName+" "+$FName;
	_assertVisible(_row("/"+$expText+"/"));
	_assertVisible(_link("View"));
	_assertNotVisible(_paragraph("/No Wish List has been found for/"));  
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("140722/124549","Verify the functionality related to the 'Update' link on Wishlist page, find someone's wish list section in application as a registered user.");
$t.start();
try
{
	//navigating to wish list login page
	_click(_link("Wish List"));
	
	//_click(_link("View"));
	//making list as public
	if(_isVisible(_submit("dwfrm_wishlist_setListPublic")))
		{
		_click(_submit("dwfrm_wishlist_setListPublic"));
		}
	//incresing the quantity desired
	_setValue(_numberbox("dwfrm_wishlist_items_i"+$Wishlist_Reg[0][9]+"_quantity"),$Wishlist_Reg[0][3]);
	//setting priority to high
	_setSelected(_select("/priority/"),$Wishlist_Reg[1][3]);
	//un check the public check box
	_uncheck(_checkbox("/public/"));
	//click on update link
	_click(_submit("button-text update-item"));
	//verify the functionality
	_assertEqual($Wishlist_Reg[0][3],_getText(_numberbox("dwfrm_wishlist_items_i"+$Wishlist_Reg[0][9]+"_quantity")));
	_assertEqual($Wishlist_Reg[1][3],_getSelectedText(_select("/priority/")));
	_assertNotTrue(_checkbox("/public/").checked);	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124551/148721","Verify the functionality related to the 'ADD TO CART' button wish list search result page, find someone's wish list section in application as a Registered user");
$t.start();
try
{
	//fetch the total no.of products
	var $count=_count("_div","/name/",_in(_table("item-list")));
	if($count!=0)
		{
		for(var $i=0;$i<$count;$i++)
			{
			var $productName=_getText(_link("/(.*)/",_in(_div("name ["+$i+"]"))));	
			//var $cartQuantity=_extract(_getText(_div("mini-cart-total")),"[(](.*)[)]",true);
			var $cartQuantity=_getText(_div("mini-cart-total"));
			var $quanitytInWishlist=parseInt(_getText(_numberbox("Quantity")));
			//verify the functionality of add to cart button
			//_click(_submit("dwfrm_wishlist_items_i"+$i+"_addItemToCart"));  //dwfrm_wishlist_items_i0_addToCart
			
			_click(_submit("dwfrm_wishlist_items_i"+$i+"_addToCart"));
			//navigate to cart
			_click(_link("View Cart"));
			//Page should navigate to cart page
			_assertVisible(_table("cart-table"));
			var $prodNameInCart=_getText(_link("/(.*)/", _in(_div("name"))));
			$cartQuantity=parseInt($cartQuantity)+$quanitytInWishlist;
			//_assertEqual(parseInt($cartQuantity),parseInt(_extract(_getText(_div("mini-cart-total")),"[(](.*)[)]",true)));
			_assertEqual(parseInt($cartQuantity),_getText(_div("mini-cart-total")));
			
			}
		}
	else
		{
		_assert(false,"no items are there in wish list");
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("124550","Verify the functionality related to the 'Remove' link on Wish list page, find someone's wish list section in application as a  Registered user");
$t.start();
try
{
	//Navigate to WL page
	_click(_link("user-account"));
	_click(_link("Wish List"));
	
	//fetch the total no.of products	
	var $count=_count("_div","/name/",_in(_table("item-list")));
	if($count==1)
		{
		_click(_submit("/deleteItem/"));
		//no products should be present 
		_assertVisible(_link($Wishlist_Reg[0][8]));
		_assertNotVisible(_submit("/deleteItem"));
		}
	else
		{
		for(var $i=0;$i<$count;$i++)
			{	
			var $productName=_getText(_link("/(.*)/",_in(_div("name "))));
			_click(_submit("/deleteItem/"));
			//verify whether first item is removed or not
			_assertNotVisible(_link($productName,_in(_div("name "))));
			}
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

cleanup();