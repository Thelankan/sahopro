_include("../util.GenericLibrary/GlobalFunctions.js");
_include("../util.GenericLibrary/BM_Configuration.js");
_resource("Register/Register_Data.xls");
_resource("LogIn/Login_Data.xls");
_resource("Addresses/Address_Data.xls");
_resource("Payment/Payment.xls");

var $Generic=_readExcelFile("Register/Register_Data.xls","Generic");
var $Login_Data=_readExcelFile("LogIn/Login_Data.xls","Login_Data");
var $Addr_Validation=_readExcelFile("Addresses/Address_Data.xls","Address_Validation");
var $Createcard=_readExcelFile("Payment/Payment.xls","CreateCard");

//deleting the user if exists
deleteUser();
SiteURLs();
cleanup();
_setAccessorIgnoreCase(true); 


var $t = _testcase("124599", "Verify the functionality related to 'Apply' button Create account page in application as an  Anonymous user.");
$t.start();
try
{
	_click(_link("Login"));
	_click(_submit("dwfrm_login_register"));
	//heading
	_assertVisible(_heading1($Generic[0][0]));	
	createAccount();
	//verify whether account got created or not
   var $exp=$userData[$user][1]+" "+$userData[$user][2];
   _assertEqual($exp, _extract(_getText(_heading1("/My Account/")),"/[|] (.*) [(]/",true).toString());
   _click(_link("user-account"));
   _click(_link("Logout"));
   _click(_submit("dwfrm_login_register"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("124615","Verify the functionality related of 'LOGIN' button My Account login's page, Returning customers section in    site as an  Anonymous user");
$t.start();
try
{
	//navigate back to my account page
	_click(_link("Login"));	
	//login to the application
	login();
	if (isMobile())
		{
			_wait(3000,_isVisible(_list("account-options")));
		}
	_assertVisible(_link("Logout"));
	_assertVisible(_heading1("/"+$Login_Data[1][3]+"/"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//Login and navigate to address page
deleteAddress();
var $t = _testcase("124770", "Verify the functionality related to  'Apply' button  'Add Address overlay Addresses page' in application as a  Registered user.");
$t.start();
try
{	
		_click(_link("Create New Address"));
		 if(_isVisible(_div("dialog-container")))
			 {
				 addAddress($Addr_Validation,7);
				 _click(_submit("Apply"));
			 	_assertEqual($Addr_Validation[7][1],_getText(_div("mini-address-title")));
			 	_assertEqual($Addr_Validation[7][2]+" "+$Addr_Validation[7][3],_getText(_div("mini-address-name")));
			 	_assertEqual($Addr_Validation[7][11], _getText(_div("mini-address-location")));				 
			}
		 else
			 {
				 _assert(false,"Add address Overlay is not displaying"); 
			 }		
}
catch($e)
{
	_logExceptionAsFailure($e);
}
$t.end();


//deleting cards
DeleteCreditCard();

var $t=_testcase("124785","Verify the functionality related to 'Apply' button the Add a credit card overlay on my account payment methods page of the application as a Registered user");
$t.start();
try
{
	_click(_link("user-account"));
	_click(_link("Payment Settings"));
	//open the credit card overlay
	_click(_link("Add a Credit Card"));
	//filling details in create card fields
	if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
		{		
			CreateCreditCard(0,$Createcard, true);
		}
	else
		{
			CreateCreditCard(0,$Createcard);
		}
	//click on apply button
	_click(_submit("applyBtn"));
	//verify whether credit card got saved or not and also UI
	_assertVisible(_list("payment-list"));
	var $saveInfo=_getText(_listItem(" first ")).split(" ");
	_assertEqual($Createcard[0][1],$saveInfo[0]);
	_assertEqual($Createcard[0][2],$saveInfo[1]);
	var $expDate=$Createcard[0][15]+"."+$Createcard[1][4]+"."+$Createcard[0][5];
	_assertEqual($expDate,$saveInfo[3]);
	_assertVisible(_submit("Delete Card"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

cleanup();