_include("../../util.GenericLibrary/BM_Configuration.js");
_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Login_Data.xls");

var $Login_Data=_readExcelFile("Login_Data.xls","Login_Data");
var $Login_Validation=_readExcelFile("Login_Data.xls","Login_Validation");
var $Order_Validation=_readExcelFile("Login_Data.xls","Order_Validation");
var $Data=_readExcelFile("Login_Data.xls","Data");
var $UserEmail=$uId;
SiteURLs()
cleanup();
_setAccessorIgnoreCase(true);

var $t = _testcase("124604", "Verify the navigation to My Account login page in application as an  Anonymous user");
$t.start();
try
{
	//click on login link
	_click(_link("Login"));
	//verify the navigation
	_assertVisible(_heading1($Login_Data[0][0]));
	_assertVisible(_link($Login_Data[1][3], _in(_div("breadcrumb"))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124605/148627", "Verify Returning customers section on My account login page");
$t.start();
try
{
	//Verifying the UI of returning customer section
	//MyAccount login as heading
	_assertVisible(_heading1($Login_Data[0][0]));
	//Returning customer heading
	_assertVisible(_heading2("/"+$Login_Data[1][0]+"/"));
	//Paragraph with static text
	_assertVisible(_paragraph("/(.*)/",_in(_div("login-box-content returning-customers clearfix"))));
	//email Address
	_assertVisible(_label($Login_Data[2][0]));
	_assertVisible(_textbox("/dwfrm_login_username/"));
	_assertEqual("", _getValue(_textbox("/dwfrm_login_username/")));
	//password
	_assertVisible(_label($Login_Data[3][0]));
	_assertVisible(_password("/dwfrm_login_password/"));
	_assertEqual("", _getValue(_password("/dwfrm_login_password/")));
	//remember me
	_assertVisible(_label("Remember Me"));
	_assertVisible(_checkbox("dwfrm_login_rememberme"));
	_assertNotTrue(_checkbox("dwfrm_login_rememberme").checked);
	//login button
	_assertVisible(_submit("Login"));
	//forgot password
	_assertVisible(_link("password-reset"));
	_assertEqual($Login_Data[4][0], _getText(_link("password-reset")));
	//social links
	_assertVisible(_heading1($Login_Data[7][0]));
	_assertVisible(_div("login-box-content returning-customers clearfix"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
 

var $t = _testcase("124606/148627", "Verify New customers section on My account login page");
$t.start();
try
{
	//verify the UI of New Customer section
	_assertVisible(_heading2($Login_Data[0][1]));
	//paragraph
	_assertVisible(_paragraph("/(.*)/",_in(_div("login-box login-create-account clearfix"))));
	//create an account button
	_assertVisible(_submit("dwfrm_login_register"));
	//content assert
	_assertVisible(_div("content-asset",_in(_div("login-box login-create-account clearfix"))));
	//read more about security link
	_assertVisible(_link("read more about security"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
 $t.end();
 

var $t = _testcase("124607/148627", "Verify check an Order section on My account login page");
$t.start();
try
{
	//verify the UI of check an order section
	_assertVisible(_heading2("/"+$Login_Data[0][2]+"/"));
	_assertVisible(_paragraph("/(.*)/",_in(_div("login-box login-order-track"))));
	//order number
	_assertVisible(_label($Login_Data[1][2]));
	_assertVisible(_textbox("dwfrm_ordertrack_orderNumber"));
	_assertEqual("", _getValue(_textbox("dwfrm_ordertrack_orderNumber")));
	//order email
	_assertVisible(_label($Login_Data[2][2]));
	_assertVisible(_textbox("dwfrm_ordertrack_orderEmail"));
	_assertEqual("", _getValue(_textbox("dwfrm_ordertrack_orderEmail")));
	//postal code
	_assertVisible(_label($Login_Data[3][2]));
	_assertVisible(_textbox("dwfrm_ordertrack_postalCode"));
	_assertEqual("", _getValue(_textbox("dwfrm_ordertrack_postalCode")));
	//submit button
	_assertVisible(_submit("dwfrm_ordertrack_findorder"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124608", "Verify the navigation related to 'Home' and 'My account' links  in breadcrumb on my account login page in application as an Anonymous user.");
$t.start();
try
{
	//verifying the bread crumb related links
	//clicking on home link
	//_click(_link($Login_Data[0][3],_in(_div("breadcrumb"))));
	//Navigate to home page
	_click(_link("Demandware SiteGenesis"));
	//verify the navigation to home page
	_assertVisible(_list("homepage-slides"));
	_assertVisible(_div("home-bottom-slots"));
	//click on link login
	_click(_link("Login"));
	//click on my account bread crumb link 
	_click(_link($Login_Data[1][3],_in(_div("breadcrumb"))));
	//verfiy the navigation
	_assertVisible(_heading1($Login_Data[0][0]));
	_assertVisible(_div("login-box login-account"));
	_assertVisible(_div("login-box login-create-account clearfix"));
	_assertVisible(_div("login-box login-order-track"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();	


var $t = _testcase("148628", "Verify the navigation related to ''Privacy Policy' link  on my account login page left nav in  application as an Anonymous user");
$t.start();
try
{
	//click on privacy policy link
	_click(_link($Login_Data[0][4]));
	//verify the navigation
	_assertVisible(_link($Login_Data[0][4], _in(_div("breadcrumb"))));
	_assertContainsText($Login_Data[0][4], _div("breadcrumb"));
	_assertVisible(_heading1($Login_Data[0][4]));
	_assertVisible(_div("content-asset"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();	


var $t = _testcase("148629", "Verify the navigation related to 'Secure Shopping' link  on my account login page left nav in  application as an  Anonymous user");
$t.start();
try
{
	//navigate back to my account page
	_click(_link("Login"));
	//click on secure shopping link
	_click(_link($Login_Data[4][4]));
	//verify the navigation
	_assertVisible(_link($Login_Data[1][4], _in(_div("breadcrumb"))));
	_assertVisible(_heading1($Login_Data[1][4]));
	_assertVisible(_div("content-asset"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("148630", "Verify the navigation related to 'Contact Us' link  on my account login page left nav in  application as an  Anonymous user");
$t.start();
try
{
	//navigate back to my account page
	_click(_link("Login"));
	//click on contact us link
	_click(_link($Login_Data[2][4],_in(_div("account-nav-asset"))));
	//verify the navigation
	_assertVisible(_heading1("/"+$Login_Data[2][4]+"/"));
	_assertVisible(_submit("dwfrm_contactus_send"));
	_assertVisible(_link($Login_Data[3][4], _in(_div("breadcrumb"))));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148631", "Verify the functionality related of 'Forgot Password' link on My Account login's page Returning customers section in   site as an  Anonymous user");
$t.start();
try
{	
	//navigate back to my account page
	_click(_link("Login"));
	//clicking on forgot password link
	_click(_link("password-reset"));
	//verify whether overlay got opened or not
	_assertVisible(_div("dialog-container"));
	_assertVisible(_span("ui-dialog-title"));
	//_assertEqual($Login_Data[0][5], _getText(_span("ui-dialog-title")));
	_assertVisible(_button("Close"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("148632", "Verify the UI of 'Forgot Password' overlay on My Account login page in application as an  Anonymous user");
$t.start();
try
{
	//verify the UI of forgot password overlay
	_assertVisible(_heading1($Login_Data[1][5]));	
	_assertVisible(_paragraph("/(.*)/",_in(_div("dialog-container"))));
	//email field
	_assertVisible(_label($Login_Data[2][5]));
	_assertVisible(_textbox("dwfrm_requestpassword_email"));
	_assertVisible(_submit("dwfrm_requestpassword_send"));
	//closing the dialog
	_click(_button("Close"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124619","Verify the functionality �Send� button related to Forgot password overlay on My Account Login page returning customer section  in application as an  Anonymous user");
$t.start();
try
{
	//clicking on forgot password link
	_click(_link("password-reset"));
	//enter valid email id
	_setValue(_textbox("dwfrm_requestpassword_email"),$Login_Data[3][5]);
	//click on send button
	_click(_submit("dwfrm_requestpassword_send"));
	//verify the navigation
	_assertVisible(_heading1($Login_Data[4][5]));
	_assertVisible(_button("Close"));
	_assertVisible(_paragraph("/(.*)/",_in(_div("dialog-container"))));
	_assertVisible(_link("Go to the Home page"));
	//closing the dialog
	_click(_button("Close"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124624", "Verify the navigation related to 'Create an account now' button My Account login page, New customers  section  in application as an  Anonymous user");
$t.start();
try
{
	//click on create an account button
	_click(_submit("dwfrm_login_register"));
	//verify the navigation
	_assertVisible(_heading1($Login_Data[1][1]));
	_assertVisible(_div("primary-content"));
	_assertVisible(_submit("dwfrm_profile_confirm"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148633","Verify the navigation related to  'read more about security' social link on My Account login page, New customers  section  in application as an  Anonymous user");
$t.start();
try
{
	//navigate back to my account page
	_click(_link("Login"));	
	//click on read more about security link
	_click(_link("read more about security"));
	//verify the navigation
	_assertVisible(_link($Login_Data[2][1], _in(_div("breadcrumb"))));
	_assertContainsText($Login_Data[2][1], _div("breadcrumb"));
	_assertVisible(_heading1($Login_Data[2][1]));
	_assertVisible(_div("content-asset"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124615","Verify the functionality related of 'LOGIN' button My Account login's page, Returning customers section in    site as an  Anonymous user");
$t.start();
try
{
	//navigate back to my account page
	_click(_link("Login"));	
	//login to the application
	login();
	_assertVisible(_link("Logout", _in(_span("account-logout"))));
	//click on link not
	_click(_link("Logout", _in(_span("account-logout"))));
	//verify the functionality
	_click(_link("user-account"));
	_assertVisible(_link("Login"));
	_click(_link("Login"));
	
	if (isMobile() || isIE11())
		{
		_assertVisible(_link($Login_Data[1][3], _in(_div("breadcrumb"))));
		}
	else
		{
		_assertVisible(_link($Login_Data[1][3], _in(_div("breadcrumb"))));
		}
	//enter invalid data
	_setValue(_textbox("/dwfrm_login_username/"),$Login_Data[0][6]);
	_setValue(_password("/dwfrm_login_password/"),$Login_Data[1][6]);
	_click(_submit("dwfrm_login_login"));
	//verify the error message
	_assertVisible(_span("error"));
	_assertEqual($Login_Data[2][6], _getText(_span("error")));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148634","Verify the functionality related to  'ACCOUNT SETTINGS' down arrow on My Account login's page left Nav in   site as an  Anonymous user");
$t.start();
try
{
	//collect the list present under the account settings
	var $list=_collectAttributes("_list","/(.*)/","sahiText",_above(_span($Login_Data[1][7])));
	//click on account settings link
	_click(_span($Login_Data[0][7]));
	//after clicking links present under account settings shouldn't visible
	for(var $i=0;$i<$list.length;$i++)
		{
		_assertNotVisible(_listItem($list[$i]));
		}
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148635","Verify the functionality related to 'SHOP CONFINDENTLY' down arrow on My Account login's page left Nav in   site as an  Anonymous user");
$t.start();
try
{
	//collect the list present under the SHOP CONFINDENTLY
	var $list=_collectAttributes("_list","/(.*)/","sahiText",_under(_span($Login_Data[1][7])));
	//click on account settings link
	_click(_span($Login_Data[1][7]));
	//after clicking links present under account settings shouldn't visible
	for(var $i=0;$i<$list.length;$i++)
		{
		_assertNotVisible(_listItem($list[$i]));
		}	
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124610/148636","Verify the validation related to the 'Email address'/'password' field on My Account login page, Returning customers section in application as a Anonymous user");
$t.start();
try
{
//validating the email id & password field's
for(var $i=0;$i<$Login_Validation.length;$i++)
	{
	if($i==5)
		{
			login();
		}
	else
		{
		_setValue(_textbox("/dwfrm_login_username/"),$Login_Validation[$i][1]);
		_setValue(_password("/dwfrm_login_password/"),$Login_Validation[$i][2]);
		_click(_submit("dwfrm_login_login"));
		}	
	if (isMobile())
		{
		_wait(3000);
		}
	//blank
	if($i==0)
		{
		//username
		_assertEqual($Login_Validation[$i][5],_style(_textbox("/dwfrm_login_username/"),"background-color"));
		_assertVisible(_span($Login_Validation[$i][3]));
		//password
		_assertVisible(_span($Login_Validation[$i][4]));
		_assertEqual($Login_Validation[$i][5],_style(_password("/dwfrm_login_password/"),"background-color"));
		}
	
	//in valid email
	else if($i==1 || $i==2 || $i==3)
		{
		_assertEqual($Login_Validation[2][3], _getText(_span("error")));
		}
	
	//valid
	else if($i==5)
		{
		if (isMobile())
			{
		_wait(3000,_isVisible(_list("account-options")));
			}
		_assertVisible(_link("Logout", _in(_span("account-logout"))));
		_assertVisible(_heading1("/"+$Login_Data[1][3]+"/"));
		}
	//invalid 
	else
		{
		_assertVisible(_div($Login_Validation[1][3]));
		}
	}
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//place order
navigateToShippingPage($Data[0][0],$Data[0][1]);
//shipping details
shippingAddress($Data,3);
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//verify address verification overlay
addressVerificationOverlay();
//Billing details
BillingAddress($Data,3);
//Payment details
PaymentDetails($Data,6);
_click(_submit("dwfrm_billing_save"));
//Placing order
_click(_submit("button-fancy-large"));
if(_isVisible(_image("CloseIcon.png")))
	{
	_click(_image("CloseIcon.png"));
	}
//fetch order num
var $OrderNum=_getText(_span("value", _in(_heading1("order-number"))));
_log($OrderNum);

_click(_link("user-account"));
_click(_link("My Account"));
_click(_link("/Orders/"));
_click(_submit("Order Details"));
_wait(2000);
//var $OrderNum= _extract(_getText(_div("order-number ")),"/Order Number (.*)/", true).toString();
var $PostalCode=_getText(_div("mini-address-location")).split(",")[1].split(" ")[2];
//logout from application
_click(_link("My Account"));
_click(_link("Logout", _in(_span("account-logout"))));


var $t = _testcase("124627/148637/124812","Verify the validation related to the 'Order number' field on My Account login page, Check an order section in application as a Anonymous user");
$t.start();
try
{
for(var $i=0;$i<$Order_Validation.length;$i++)
	{
	if($i==0)
		{
		_setValue(_textbox("dwfrm_ordertrack_orderNumber"), "");
		_setValue(_textbox("dwfrm_ordertrack_orderEmail"), "");
		_setValue(_textbox("dwfrm_ordertrack_postalCode"), "");
		}
	else if($i==7)
		{
		_setValue(_textbox("dwfrm_ordertrack_orderNumber"), $OrderNum);
		_setValue(_textbox("dwfrm_ordertrack_orderEmail"), $UserEmail);
		_setValue(_textbox("dwfrm_ordertrack_postalCode"), $PostalCode);
		}
	else 
		{
		_setValue(_textbox("dwfrm_ordertrack_orderNumber"), $Order_Validation[$i][1]);
		_setValue(_textbox("dwfrm_ordertrack_orderEmail"), $Order_Validation[$i][2]);
		_setValue(_textbox("dwfrm_ordertrack_postalCode"), $Order_Validation[$i][3]);
		}
	_click(_submit("dwfrm_ordertrack_findorder"));
	//blank
	if($i==0)
		{
	_assertEqual($Order_Validation[0][4],_style(_textbox("dwfrm_ordertrack_orderNumber"),"background-color"));
	_assertEqual($Order_Validation[0][4],_style(_textbox("dwfrm_ordertrack_orderEmail"),"background-color"));
	_assertEqual($Order_Validation[0][4],_style(_textbox("dwfrm_ordertrack_postalCode"),"background-color"));
		}		
	//valid
	else if($i==7)
		{
		_wait(4000);
		_assertVisible(_link($OrderNum, _in(_div("breadcrumb"))));
		_assertVisible(_heading1($Order_Validation[2][4]+" "+$OrderNum));
		}
	//invalid zipcode
	else if($i==3 || $i==4)
		{
		_assertVisible(_div($Order_Validation[1][4]));
		_assertVisible(_div($Order_Validation[1][5]));
		}
	//invalid email
	else
		{
		_assertVisible(_div($Order_Validation[1][4]));
		}	
	}
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();