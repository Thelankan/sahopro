_include("../../util.GenericLibrary/GlobalFunctions.js");
_include("../../util.GenericLibrary/BM_Configuration.js");
_resource("Order_Data.xls");

var $Generic=_readExcelFile("Order_Data.xls","Generic");
var $Account=_readExcelFile("Order_Data.xls","Account");
var $Valid_Data=_readExcelFile("Order_Data.xls","Valid_Data");
var $creditcard_data=_readExcelFile("Order_Data.xls","Payment_Data");
var $paypal=_readExcelFile("Order_Data.xls","Paypal");
var $UserEmail=$uId;


SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);


_click(_link("Login"));
_setValue(_textbox("/dwfrm_login_username/"),$Account[0][0]);
_setValue(_password("/dwfrm_login_password/"),$Account[0][1]);
_click(_submit("dwfrm_login_login"));


var $t = _testcase("124792/124795", "Verify the navigation to 'Orders' link in the My Account - Home page and Verify the UI  of 'My Account Order history page'  when there are no order records created in application as a  Registered user.");
$t.start();
try
{
	
	_click(_heading2("Orders"));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'')); 
		}
	else
		{
		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
		}
	//heading
	_assertVisible(_heading1($Generic[1][0]));
	//static text
	_assertVisible(_div("no_orders"));
	_assertEqual($Generic[2][0], _getText(_div("no_orders")));
	//left nav
	_assertVisible(_div("secondary-navigation"));
	var $Heading=_count("_span", "/(.*)/",_in(_div("secondary-navigation")));
	var $Links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
		{
			_assertVisible(_span($Generic[$i][1]));			
		}
	//checking links
	for(var $i=0;$i<$Links;$i++)
		{
			_assertVisible(_link($Generic[$i][2]));			
		}
	//left nav content asset(contact us)
	_assertVisible(_div("account-nav-asset"));
	
	//logout from application
	_click(_link("My Account"));
	_click(_link("Logout"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//Login()
_click(_link("Login"));
login();

//---------
//place order
navigateToShippingPage($Generic[0][3],0);
//fill shipping details
shippingAddress($Valid_Data,0);
//check use this for billing check box
_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//UPS verification
if($AddrVerification==$BMConfig[0][2])
{
	_click(_submit("ship-to-original-address"));
}

//entering Credit card Information
if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
    {
		   //PaymentDetails($Valid_Data,4);
		   PaymentDetails($creditcard_data,0); 
           $paymentType="Credit Card";
           _wait(3000,_isVisible(_submit("dwfrm_billing_save")));
           _click(_submit("dwfrm_billing_save"));
           _wait(10000,_isVisible(_submit("submit")));
    }
else if($CreditCard=="No" && $paypalNormal=="Yes")
    {
           _click(_radio("is-PayPal"));
           $paymentType="Pay Pal";
           _wait(3000,_isVisible(_submit("dwfrm_billing_save")));
           _click(_submit("dwfrm_billing_save"));
           if(isMobile())
           {
                 _setValue(_emailbox("login_email"), $paypal[0][0]);
           }
           else
           {
           _setValue(_textbox("login_email"), $paypal[0][0]);
           }
    _setValue(_password("login_password"), $paypal[0][1]);
    _click(_submit("Log In"));
    _click(_submit("Continue"));
    _wait(10000,_isVisible(_submit("submit")));
    }

//Placing order
_click(_submit("button-fancy-large"));
//fetch the order number from confirmation pae
var $OrderNumber=_getText(_span("value",_in(_heading1("order-number"))));

_click(_link("My Account"));	
_click(_heading2("Orders"));
_click(_submit("Order Details"));

if(isIE11() || _isIE10() || _isIE9())
	{
	var $OrderTotal=_extract(_getText(_row("order-total")), "/Order Total:(.*)/",true).toString();
	}
else
	{
	var $OrderTotal=  _extract(_getText(_row("order-total")), "/Order Total: (.*)/",true).toString();
	}
var $ProductName=_getText(_div("name", _in(_div("product-list-item"))));
var $Zipcode=_getText(_div("mini-address-location")).split(",")[1].split(" ")[2];
var $OrderStatus=$Generic[0][5];
var $OrderInfo=_getText(_div("order-payment-instruments")).toString();
var $prodInfo=_getText(_div("line-item-details")).toString();
var $ShipmentInfo=_getText(_div("order-shipment-address")).toString();
var $billingInfo=_getText(_div("order-billing")).toString();


var $t = _testcase("124794","Verify the UI  of 'My Account Order history page'  when there are multiple order records in application as a  Registered user.");
$t.start();
try
{
	_click(_link("Order History"));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
		}
	//heading
	_assertVisible(_heading1($Generic[1][0]));
	//Pagination header
	_assertVisible(_div("pagination"));
	//Pagination footer
	_assertVisible(_div("pagination", _under(_list("search-result-items"))));
	//Checking the details of the last placed order
	//Order date
	_assertVisible(_div("order-date"));
	//order status
	_assertVisible(_div("order-status"));
	if(_isIE9())
		{
			var $temp="Order Status: "+$OrderStatus;
		_assertEqual($temp.replace(/\s/g,''), _getText(_div("order-status")).replace(/\s/g,'')); 
		}
	else
		{
		_assertEqual("Order Status: "+$OrderStatus, _getText(_div("order-status")));
		}	
	//Order number
	_assertVisible(_div("order-number"));
	_assertEqual("Order Number: "+$OrderNumber, _getText(_div("order-number")));
	//Order details link
	_assertVisible(_submit("Order Details"));
	//Product name
	_assertEqual($ProductName,_getTableContents(_table("order-history-table"),[1],[1]).toString());
	//order total
	_assertEqual($OrderTotal,_getTableContents(_table("order-history-table"),[2],[1]).toString());	
	//left nav
	_assertVisible(_div("secondary-navigation"));
	var $Heading=_count("_span", "/(.*)/",_in(_div("secondary-navigation")));
	var $Links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
		{
			_assertVisible(_span($Generic[$i][1]));			
		}
	//checking links
	for(var $i=0;$i<$Links;$i++)
		{
			_assertVisible(_link($Generic[$i][2]));			
		}
	//left nav content asset(contact us)
	_assertVisible(_div("account-nav-asset"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124797/124799","Verify the navigation related to 'Home' and 'My Account' links on my account order history page breadcrumb  in application as a Registered user.");
$t.start();
try
{
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
		}
	
	_click(_link("My Account"));	
	_click(_heading2("Orders"));
	//Navigation related to My account
	_assertVisible(_link("My Account", _in(_div("breadcrumb"))));
	_click(_link("My Account", _in(_div("breadcrumb"))));
	_assertVisible(_heading1("/My Account /"));
	if(_isIE9())
		{
		_assertEqual($Generic[6][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[6][0], _getText(_div("breadcrumb")));
		}
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("128536","Verify the functionality of Product link in the Order Summary page");
$t.start();
try
{
	_click(_link("My Account"));	
	_click(_heading2("Orders"));
	//Order details link
	_assertVisible(_submit("Order Details"));
	_click(_submit("Order Details"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("124800/124814","Verify the navigation related to 'Order Details' button  on  'My Account order history'  page in application as a  Registered user./Verify the UI  of 'My Account Order summery page' in application as a  Registered user.");
$t.start();
try
{
	_click(_link("My Account"));	
	_click(_heading2("Orders"));
	//Order details link
	_assertVisible(_submit("Order Details"));
	_click(_submit("Order Details"));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
			var $temp=$Generic[0][0]+" "+$OrderNumber;
		_assertEqual($temp.replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'')); 
		}
	else
		{
		_assertEqual($Generic[0][0]+" "+$OrderNumber, _getText(_div("breadcrumb")));
		}
	
	//heading
	_assertVisible(_heading1($Generic[3][0]+" "+$OrderNumber));
	//Validating UI
	_assertVisible(_div("order-payment-instruments"));
	_assertEqual($OrderInfo, _getText(_div("order-payment-instruments")));
	_assertVisible(_div("order-shipment-address"));
	_assertEqual($ShipmentInfo, _getText(_div("order-shipment-address")));
	//billing details
	_assertVisible(_div("order-billing"));
	_assertEqual($billingInfo,_getText(_div("order-billing")));
	//product details
	_assertVisible(_div("line-item-details"));
	_assertEqual($prodInfo, _getText(_div("line-item-details")));
	//links
	_assertVisible(_link($Generic[4][0]));
	_assertVisible(_link($Generic[5][0]));
	//left nav
	_assertVisible(_div("secondary-navigation"));
	var $Heading1=_count("_span", "/(.*)/",_in(_div("secondary-navigation")));
	var $Links1=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
	//checking headings
	for(var $i=0;$i<$Heading1;$i++)
		{
			_assertVisible(_span($Generic[$i][1]));			
		}
	//checking links
	for(var $i=0;$i<$Links1;$i++)
		{
			_assertVisible(_link($Generic[$i][2]));			
		}
	//left nav content asset(contact us)
	_assertVisible(_div("account-nav-asset"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124808","Verify the navigation related to 'Return to order history'  link on  'My Account order summery' page in application as a  Registered  user.");
$t.start();
try
{
	_assertVisible(_link($Generic[4][0]));
	_click(_link($Generic[4][0]));
	//Validating the navigation
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
		}
	//heading
	_assertVisible(_heading1($Generic[1][0]));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124810","Verify the navigation related to 'Return to shopping'  link on  'My Account order summery' page in application as a  Registered  user.");
$t.start();
try
{
	_click(_link("My Account"));	
	_click(_heading2("Orders"));
	//Order details link
	_click(_submit("Order Details"));
	//return to shopping link
	_assertVisible(_link($Generic[5][0]));
	_click(_link($Generic[5][0]));
	//Validating the navigation
	_assertVisible(_list("homepage-slides"));
	_assertVisible(_div("home-bottom-slots"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124805","Verify the navigation related to 'Home'  and 'My Account links   on  'My Account order summery' page bread crumb in application both a Registered user.");
$t.start();
try
{
	_click(_link("My Account"));	
	_click(_heading2("Orders"));
	//Order details link
	_click(_submit("Order Details"));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
			var $temp=$Generic[0][0]+" "+$OrderNumber;
		_assertEqual($temp.replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'')); 
		}
	else
		{
		_assertEqual($Generic[0][0]+" "+$OrderNumber, _getText(_div("breadcrumb")));
		}
	
	//Validating navigation on click of order history
	_assertVisible(_link("Order History", _in(_div("breadcrumb"))));
	_click(_link("Order History", _in(_div("breadcrumb"))));
	//bread crumb
	if(_isIE9())
		{
		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
		}
	//heading
	_assertVisible(_heading1($Generic[1][0]));	
	
	_click(_link("My Account"));	
	_click(_heading2("Orders"));
	//Order details link
	_click(_submit("Order Details"));
	//Validating navigation on click of My account
	_assertVisible(_link("My Account", _in(_div("breadcrumb"))));
	_click(_link("My Account", _in(_div("breadcrumb"))));
	_assertVisible(_heading1("/My Account /"));
	if(_isIE9())
		{
		_assertEqual($Generic[6][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[6][0], _getText(_div("breadcrumb")));
		}
	
	_click(_link("My Account"));	
	_click(_heading2("Orders"));
	//Order details link
	_click(_submit("Order Details"));
	//click of Home page link
	_click(_link("/(.*)/", _in(_heading1("primary-logo"))));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125909","Verify the navigation related to product name on  'My Account order summery' page in application as a  Registered  user.");
$t.start();
try
{
	_click(_link("My Account"));	
	_click(_heading2("Orders"));
	//Order details link
	_click(_submit("Order Details"));
	//checking Product name
	_assertVisible(_link("/(.*)/", _in(_div("name", _in(_div("product-list-item"))))));
	var $PName=_getText(_link("/(.*)/", _in(_div("name", _in(_div("product-list-item"))))));
	_click(_link("/(.*)/", _in(_div("name", _in(_div("product-list-item"))))));
	//Validating navigation
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	_assertContainsText($PName,_div("breadcrumb"));
	//heading
	_assertEqual($PName, _getText(_heading1("product-name")));	
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
//logout from application
_click(_link("My Account"));
_click(_link("Logout"));



//defect in application need to remove comments once fixed
///user login
_click(_link("Login"));
_setValue(_textbox("/dwfrm_login_username/"),$Account[1][0]);
_setValue(_password("/dwfrm_login_password/"),$Account[1][1]);
_click(_submit("dwfrm_login_login"));


//Comment below code if you are already executed this script once

for(var $i=0;$i<30;$i++)
{
		//Placing an order
		//Navigate to cart
		navigateToShippingPage($Generic[0][3],$Generic[0][4]);
		//Shipping
		shippingAddress($Valid_Data,0);
		//Billing
		_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
		if(_isVisible(_div("address-validation-dialog")))
		{
		_click(_submit("ship-to-original-address"));
		}
		//enter valid billing address
		BillingAddress($Valid_Data,0);
		//enter credit card details
		if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
		{
		      PaymentDetails($Valid_Data,4);
		      //click on continue
		      _click(_submit("dwfrm_billing_save"));   
		}
		else if($CreditCard=="No" && $paypalNormal=="Yes")
		{
		      _assert(_radio("PayPal").checked);
		//click on continue
		_click(_submit("dwfrm_billing_save"));   
		//paypal
		if(isMobile())
		    {
		          _setValue(_emailbox("login_email"), $paypal[0][0]);
		    }
		else
		    {
		    _setValue(_textbox("login_email"), $paypal[0][0]);
		    }
		_setValue(_password("login_password"), $paypal[0][1]);
		_click(_submit("Log In"));
		_click(_submit("Continue"));
		_wait(10000,_isVisible(_submit("submit")));
		}
		//Click on submit order
		_click(_submit("Place Order"));
		if(_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
}




var $t = _testcase("125908","Verify the functionality related to pagination  on  'My Account order history'  page in application as a  Registered user.");
$t.start();
try
{
	_click(_link("My Account"));
	_click(_heading2("Orders"));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
			_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,''));
		}
	else
		{
			_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
		}	
	//Pagination
	if(_isVisible(_div("/pagination/")))
		{
			pagination();
		}
	else
		{
			_assert(false, "Pagination is not displaying");
		}
	//logout from application
	_click(_link("My Account"));
	_click(_link("Logout"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124820","Verify the navigation related to 'Home'  and 'My Account links   on  'My Account order summery' page bread crumb in application both as an  Anonymous user.");
$t.start();
try
{
	//navigate to Login page 
	_click(_link("Login"));
	//navigating to order summary
	CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode);
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
			var $temp=$Generic[0][0]+" "+$OrderNumber;
		_assertEqual($temp.replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'')); 
		}
	else
		{
		_assertEqual($Generic[0][0]+" "+$OrderNumber, _getText(_div("breadcrumb")));
		}
	//heading
	_assertVisible(_heading1($Generic[3][0]+" "+$OrderNumber));
	
	//Validating navigation on click of order history
	_assertVisible(_link("Order History", _in(_div("breadcrumb"))));
	_click(_link("Order History", _in(_div("breadcrumb"))));
	//bread crumb
	if(_isIE9())
		{
		_assertEqual($Generic[6][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[6][0], _getText(_div("breadcrumb")));
		}
	//heading
	_assertVisible(_heading1($Generic[7][0]));
	_assertVisible(_div("login-box login-account"));
	
	//navigate to Login page 
	_click(_link("Login"));
	//navigating to order summary
	CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode);
	//Validating navigation on click of My account
	_assertVisible(_link("My Account", _in(_div("breadcrumb"))));
	_click(_link("My Account", _in(_div("breadcrumb"))));
	//bread crumb
	if(_isIE9())
		{
		_assertEqual($Generic[6][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[6][0], _getText(_div("breadcrumb")));
		}
	//heading
	_assertVisible(_heading1($Generic[7][0]));
	_assertVisible(_div("login-box login-account"));
	
	//navigate to Login page 
	_click(_link("Login"));
	//navigating to order summary
	CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode);
	//Validating navigation on click of Home
	_click(_link("/(.*)/", _in(_heading1("primary-logo"))));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124815","Verify the navigation related to 'Return to shopping'  link on  'My Account order summery' page in application as a Anonymous user.");
$t.start();
try
{
	//navigate to Login page 
	_click(_link("Login"));
	//navigating to order summary
	CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode);
	//return to shopping link
	_assertVisible(_link($Generic[5][0]));
	_click(_link($Generic[5][0]));
	//Validating the navigation
	_assertVisible(_list("homepage-slides"));
	_assertVisible(_div("home-bottom-slots"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();
