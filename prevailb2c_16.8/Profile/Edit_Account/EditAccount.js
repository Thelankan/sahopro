_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("EditAccount_Data.xls");

var $Generic=_readExcelFile("EditAccount_Data.xls","Generic");
var $NameEmail_Validation=_readExcelFile("EditAccount_Data.xls","Name_Email_Validation");
var $Password_Validation=_readExcelFile("EditAccount_Data.xls","Password_Validation");
var $color=$NameEmail_Validation[0][7];
var $color1=$NameEmail_Validation[1][7];
//from util.GenericLibrary/BrowserSpecific.js
var $FirstName=$FName.toUpperCase();
var $LastName=$LName.toUpperCase();
_log($FirstName);
_log($LastName);


SiteURLs();
cleanup();
_setAccessorIgnoreCase(true);
//Login()
_click(_link("Login"));
login();/*
_click(_link("Personal Data"));

var $t = _testcase("124643/124646", "Verify the UI  of 'Personal data (Edit Account)' page in application as a  Registered user.");
$t.start();
try
{
	//bread crumb
	if(_isIE9())
	{
	_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
	}
else
	{
	_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
	}		
	//heading
	_assertVisible(_heading1("Edit Account"));
	_assertVisible(_div($Generic[8][0]));
	_assertVisible(_div("gigya-login-providers-list"));
	//Name and email section
	_assertVisible(_fieldset("/"+$Generic[1][0]+" /"));
	//Fname
	_assertVisible(_span("First Name"));
	_assertVisible(_textbox("dwfrm_profile_customer_firstname"));
	_assertEqual($FirstName, _getValue(_textbox("dwfrm_profile_customer_firstname")));
	//Lname
	_assertVisible(_span("Last Name"));
	_assertVisible(_textbox("dwfrm_profile_customer_lastname"));
	_assertEqual($LastName, _getValue(_textbox("dwfrm_profile_customer_lastname")));
	//email
	_assertVisible(_span("Email"));
	_assertVisible(_textbox("dwfrm_profile_customer_email"));
	_assertEqual($uId, _getValue(_textbox("dwfrm_profile_customer_email")));
	//Confirm email
	_assertVisible(_span("Confirm Email"));
	_assertVisible(_textbox("dwfrm_profile_customer_emailconfirm"));
	_assertEqual("", _getValue(_textbox("dwfrm_profile_customer_emailconfirm")));
	//Password
	_assertVisible(_span("Password"));
	_assertVisible(_password("/dwfrm_profile_login_password/"));
	_assertEqual("", _getValue(_password("/dwfrm_profile_login_password/")));
	_assertVisible(_div($Generic[3][0]));
	//Checkbox
	_assertVisible(_checkbox("dwfrm_profile_customer_addtoemaillist"));
	_assertVisible(_span($Generic[4][0]));
	//Privacy policy
	_assertVisible(_span($Generic[7][0]));
	_assertVisible(_link("privacy-policy"));
	//Apply button
	_assertVisible(_submit("dwfrm_profile_confirm"));
	
	//Password section
	_assertVisible(_fieldset("/"+$Generic[2][0]+" /"));
	//Current password
	_assertVisible(_span("Current Password"));
	_assertVisible(_password("/dwfrm_profile_login_currentpassword/"));
	_assertEqual("", _getValue(_password("/dwfrm_profile_login_currentpassword/")));
	//new password
	_assertVisible(_span("New Password"));
	_assertVisible(_password("/dwfrm_profile_login_newpassword/"));
	_assertEqual("", _getValue(_password("/dwfrm_profile_login_newpassword/")));
	//8-255 characters
	_assertVisible(_div($Generic[3][0], _in(_div("/ New Password /"))));
	//Confirm password
	_assertVisible(_span("Confirm New Password"));
	_assertVisible(_password("/dwfrm_profile_login_newpasswordconfirm/"));
	_assertEqual("", _getValue(_password("/dwfrm_profile_login_newpasswordconfirm/")));
	//Apply button
	_assertVisible(_submit("dwfrm_profile_changepassword"));

	//left nav
	_assertVisible(_div("secondary-navigation"));
	var $Heading=_count("_span", "/(.*)/",_in(_div("secondary-navigation")));
	var $Links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
		{
			_assertVisible(_span($Generic[$i][1]));			
		}
	//checking links
	for(var $i=0;$i<$Links;$i++)
		{
			_assertVisible(_link($Generic[$i][2]));			
		}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124641/124639/124670/264284/264685", "Verify the validation related to First name/Last name/Email/ Confirm Email/ Password fields and  functionality related to 'Apply' button of change name / email section in Edit Account page of the application as a Registered user");
$t.start();
try 
{
	for(var $i=0;$i<$NameEmail_Validation.length;$i++)
		{
			_click(_link("Personal Data"));
			if($i==0)
				  {
				  	 _setValue(_textbox("dwfrm_profile_customer_firstname"), "");
				 	 _setValue(_textbox("dwfrm_profile_customer_lastname"), "");
				 	 _setValue(_textbox("dwfrm_profile_customer_email"), "");
				  }
			 _setValue(_textbox("dwfrm_profile_customer_firstname"), $NameEmail_Validation[$i][1]);
		 	 _setValue(_textbox("dwfrm_profile_customer_lastname"), $NameEmail_Validation[$i][2]);
		 	 _setValue(_textbox("dwfrm_profile_customer_email"), $NameEmail_Validation[$i][3]);
		 	 _setValue(_textbox("dwfrm_profile_customer_emailconfirm"), $NameEmail_Validation[$i][4]);
		 	 if($i==0 ||$i==1 || $i==6|| $i==7)
		 		 {
		 		 	_setValue(_password("/dwfrm_profile_login_password/"), $NameEmail_Validation[$i][5]);
		 		 }
		 	 else
		 		 {
		 		 	_setValue(_password("/dwfrm_profile_login_password/"), $pwd);
		 		 }
		 	 
		 	if($i!=1)
		 		 {
		 		 	_click(_submit("dwfrm_profile_confirm")); 
		 		 }	
		 	//blank field validation
		 	 if($i==0)
			 	 {	
		 		 	 //first name
		 		 	  _assertVisible(_span($NameEmail_Validation[0][6], _in(_div("/ First Name/"))));
			 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_firstname"), "background-color"));
		 		      _assertEqual($color1, _style(_span($NameEmail_Validation[0][6], _in(_div("/ First Name/"))), "color"));
		 		      //last name
		 		 	  _assertVisible(_span($NameEmail_Validation[0][6], _in(_div("/ Last Name/"))));
			 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_lastname"), "background-color"))
			 		  _assertEqual($color1, _style(_span($NameEmail_Validation[0][6], _in(_div("/ Last Name/"))), "color"));
			 		  //email
			 		  _assertVisible(_span($NameEmail_Validation[0][6], _in(_div("/ Email/"))));
			 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_email"), "background-color"));
			 		  _assertEqual($color1, _style(_span($NameEmail_Validation[0][6], _in(_div("/ Email/"))), "color"));
			 		  //confirm email
			 		  _assertVisible(_span($NameEmail_Validation[0][6], _in(_div("/ Confirm Email/"))));
			 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_emailconfirm"), "background-color"));
			 		  _assertEqual($color1, _style(_span($NameEmail_Validation[0][6], _in(_div("/ Confirm Email/"))), "color"));
			 		  //password
			 		  _assertVisible(_span($NameEmail_Validation[0][6], _in(_div("/ Password/"))));
			 		  _assertEqual($color, _style(_password("/dwfrm_profile_login_password/"), "background-color"));
			 		  _assertEqual($color1, _style(_span($NameEmail_Validation[0][6], _in(_div("/ Password/"))), "color"));			 		  
			 	 }
		 	 //Max characters
		 	 else if($i==1)
		 		 {
			 		_assertEqual($NameEmail_Validation[0][8],_getText(_textbox("dwfrm_profile_customer_firstname")).length);
			 		_assertEqual($NameEmail_Validation[1][8],_getText(_textbox("dwfrm_profile_customer_lastname")).length);
			 		_assertEqual($NameEmail_Validation[2][8],_getText(_textbox("dwfrm_profile_customer_email")).length);
			 		_assertEqual($NameEmail_Validation[3][8],_getText(_textbox("dwfrm_profile_customer_emailconfirm")).length);
			 		_assertEqual($NameEmail_Validation[4][8],_getText(_password("/dwfrm_profile_login_password/")).length);
		 		 }
		 	 //Invalid data
		 	 else if($i==2 || $i==3 ||$i==4 )
		 		 {
			 		 _assertVisible(_span($NameEmail_Validation[$i][6], _in(_div("/ Email/"))));
			 		 _assertEqual($color1, _style(_textbox("dwfrm_profile_customer_email"), "color")); 		  
			 		 _assertVisible(_span($NameEmail_Validation[$i][6], _in(_div("/ Confirm Email/"))));
			 		 _assertEqual($color1, _style(_textbox("dwfrm_profile_customer_emailconfirm"), "color"));
		 		 }
		 	//Different email/confirm email
		 	 else if($i==5)
		 		 {
		 		 	_assertEqual($color1, _style(_span("Confirm Email"), "color"));
		 		 	_assertEqual($color1, _style(_textbox("dwfrm_profile_customer_emailconfirm"), "color"));
		 		 	_assertVisible(_div($NameEmail_Validation[$i][6], _in(_div("/ Confirm Email/"))));
		 		 }		 	 
		 	//password less than 8 characters
		 	 else if($i==6)
		 		 {
		 		 	_assertVisible(_span($NameEmail_Validation[$i][6], _in(_div("/ Password/"))));
		 		 	_assertEqual($color1, _style(_password("/dwfrm_profile_login_password/"), "color"));
		 		 }
		 	 //Invalid password
			 else if( $i==7)
		 		 {			 		
				 	_assertEqual($color1, _style(_span("Password"), "color"));
		 		 	_assertEqual($color1, _style(_password("/dwfrm_profile_login_password/"), "color"));
		 		 	_assertVisible(_div($NameEmail_Validation[$i][6], _in(_div("/ Password /"))));
		 		 }
		 	 //Valid
			 else
				 {
					 _assertVisible(_link("user-account"));
					 _assertVisible(_heading1($Generic[5][0]+" "+$NameEmail_Validation[$i][1]+" "+$NameEmail_Validation[$i][2]+" "+$Generic[6][0]));
					 _assertVisible(_link("Logout", _in(_span("account-logout"))));
					 //verification in edit account page
					 _click(_link("Personal Data"));
					 //Fname
					 _assertEqual($NameEmail_Validation[$i][1], _getValue(_textbox("dwfrm_profile_customer_firstname")));
					 //Lname
 					 _assertEqual($NameEmail_Validation[$i][2], _getValue(_textbox("dwfrm_profile_customer_lastname")));
					 //email
					 _assertEqual($NameEmail_Validation[$i][3], _getValue(_textbox("dwfrm_profile_customer_email")));
					 //resetting the data					 
					 _setValue(_textbox("dwfrm_profile_customer_firstname"), $FirstName);
				 	 _setValue(_textbox("dwfrm_profile_customer_lastname"), $LastName);
				 	 _setValue(_textbox("dwfrm_profile_customer_email"), $uId);
				 	 _setValue(_textbox("dwfrm_profile_customer_emailconfirm"), $uId);
				 	 _setValue(_password("/dwfrm_profile_login_password/"), $pwd);
			 		 _click(_submit("dwfrm_profile_confirm")); 
				 	_wait(5000);
				 	_assertVisible(_link("user-account"));
				 	_assertVisible(_heading1($Generic[5][0]+" "+$FirstName+" "+$LastName+" "+$Generic[6][0]));
					_assertVisible(_link("Logout", _in(_span("account-logout"))));
				 }
		}
}
catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end();


var $t = _testcase("264287/264288/264289/264290", "Verify the validation related to Current Password/New Password/Confirm New Password field and functionality of Apply button in Edit Account page of the application.");
$t.start();
try 
{
	for(var $i=0;$i<$Password_Validation.length;$i++)
		{
			_click(_link("Personal Data"));
			if($i==2 || $i==3 || $i==5)
				{
					_setValue(_password("/dwfrm_profile_login_currentpassword/"), $pwd);
				}
			else
				{
					_setValue(_password("/dwfrm_profile_login_currentpassword/"), $Password_Validation[$i][1]);
				}			
			_setValue(_password("/dwfrm_profile_login_newpassword/"), $Password_Validation[$i][2]);
			_setValue(_password("/dwfrm_profile_login_newpasswordconfirm/"), $Password_Validation[$i][3]);
			if($i!=1)
				{
					_click(_submit("dwfrm_profile_changepassword"));
				}
			//blank field validation
		 	 if($i==0)
			 	 {	
		 		 	 //Current Password
		 		 	  _assertVisible(_span($Password_Validation[0][4], _in(_div("/ Current Password/"))));
			 		  _assertEqual($color, _style(_password("/dwfrm_profile_login_currentpassword/"), "background-color"));
		 		      _assertEqual($color1, _style(_span($Password_Validation[0][4], _in(_div("/ Current Password/"))), "color"));
		 		      //New Password
		 		 	  _assertVisible(_span($Password_Validation[0][4], _in(_div("/ New Password/"))));
			 		  _assertEqual($color, _style(_password("/dwfrm_profile_login_newpassword/"), "background-color"))
			 		  _assertEqual($color1, _style(_span($Password_Validation[0][4], _in(_div("/ New Password/"))), "color"));
			 		  //Confirm New Password
			 		  _assertVisible(_span($Password_Validation[0][4], _in(_div("/ Confirm New Password/"))));
			 		  _assertEqual($color, _style(_password("/dwfrm_profile_login_newpasswordconfirm/"), "background-color"));
			 		  _assertEqual($color1, _style(_span($Password_Validation[0][4], _in(_div("/ Confirm New Password/"))), "color"));  
			 	 }
		 	 //Max characters
		 	 else if($i==1)
		 		 {
			 		_assertEqual($Password_Validation[0][5],_getText(_password("/dwfrm_profile_login_currentpassword/")).length);
			 		_assertEqual($Password_Validation[0][5],_getText(_password("/dwfrm_profile_login_newpassword/")).length);
			 		_assertEqual($Password_Validation[0][5],_getText(_password("/dwfrm_profile_login_newpasswordconfirm/")).length);
		 		 }
		 	//Different pwd/confirm pwd
		 	 else if($i==2)
		 		 {
		 		 	_assertEqual($color1, _style(_span("Confirm New Password"), "color"));
		 		 	_assertEqual($color1, _style(_password("/dwfrm_profile_login_newpasswordconfirm/"), "color"));
		 		 	_assertVisible(_div($Password_Validation[$i][4], _in(_div("/ Confirm New Password/"))));
		 		 }
		 	 else if($i==3)
		 		 {
		 		      //new pwd
		 		 	  _assertVisible(_span($Password_Validation[$i][4], _in(_div("/ New Password/"))));
			 		  _assertEqual($color, _style(_password("/dwfrm_profile_login_newpassword/"), "background-color"))
			 		  _assertEqual($color1, _style(_span($Password_Validation[$i][4], _in(_div("/ New Password/"))), "color"));
			 		  //confirm pwd
			 		  _assertVisible(_span($Password_Validation[$i][4], _in(_div("/ Confirm New Password/"))));
			 		  _assertEqual($color, _style(_password("/dwfrm_profile_login_newpasswordconfirm/"), "background-color"));
			 		  _assertEqual($color1, _style(_span($Password_Validation[$i][4], _in(_div("/ Confirm New Password/"))), "color"));
		 		 }
		 	else if($i==4)
		 		 {
		 		 	_assertEqual($color1, _style(_span("Current Password"), "color"));
		 		 	_assertEqual($color1, _style(_password("/dwfrm_profile_login_currentpassword/"), "color"));
		 		 	_assertVisible(_div($Password_Validation[$i][4], _in(_div("/ Current Password /"))));
		 		 }
		 	 //Valid
			 else
				 {
					 _assertVisible(_link("user-account"));
					 _assertVisible(_heading1($Generic[5][0]+" "+$FirstName+" "+$LastName+" "+$Generic[6][0]));
					 _assertVisible(_link("Logout", _in(_span("account-logout"))));
					 //logout
					 logout();
					//Login()
					 _click(_link("Login"));
					 //login with new pwd
					 _setValue(_textbox("/dwfrm_login_username/"), $uId);
					 _setValue(_password("/dwfrm_login_password/"), $Password_Validation[5][2]);
	     		     _click(_submit("Login"));
	     		     //verification
	     		    _assertVisible(_link("user-account"));
					 _assertVisible(_heading1($Generic[5][0]+" "+$FirstName+" "+$LastName+" "+$Generic[6][0]));
					 _assertVisible(_link("Logout", _in(_span("account-logout"))));
					 //resetting the data
					 _click(_link("Personal Data"));
					 _setValue(_password("/dwfrm_profile_login_currentpassword/"), $Password_Validation[5][2]);	
					 _setValue(_password("/dwfrm_profile_login_newpassword/"), $pwd);
					 _setValue(_password("/dwfrm_profile_login_newpasswordconfirm/"), $pwd);
					 _click(_submit("dwfrm_profile_changepassword")); 
				 	_wait(5000);
				 	_assertVisible(_link("user-account"));
				 }
		}
}
catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end();
*/
var $t = _testcase("264291", "Verify the functionality related to see privacy policy link in Edit Account page of the application as a Registered user.");
$t.start();
try 
{
	 //resetting the data
	 _click(_link("Personal Data"));
	 //click on privacy policy
	 _click(_link("See Privacy Policy"));
	 //verification
	 _assertVisible(_div("dialog-container"));
	 _assertVisible(_heading1("Privacy Policy"));
	 //click on close
	 _click(_button("Close"));
	 _assertNotVisible(_div("dialog-container"));
}
catch ($e)
{
 _logExceptionAsFailure($e);
}
$t.end();

var $t = _testcase("124630/148643/148644/148645/148646/148647/148648/148649", "Verify the navigation related to  'Personal Data'/Address/payment settings/order history/modify wish list/search Wish Lists/create registry/Search Registries/Modify Registries/privacy policy/secure shopping  link  present in left nav on My account home page in application");
$t.start();
try
{
//left nav links	
var $Links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
for(var $i=0;$i<$Links;$i++)
	{
		//navigate to my account home page
		_click(_link("user-account"));
		_click(_link("My Account"));
		_click(_link("Personal Data"));
		_click(_link($Generic[$i][2],_in(_listItem($Generic[$i][2]))));
		//click on link personal data
		if($i==0)
			{
			//verify the navigation
			_assertVisible(_heading1($Generic[$i][3]));
			_assertVisible(_div("breadcrumb"));
			_assertVisible(_link($Generic[$i][3], _in(_div("breadcrumb"))));
			}
		//click on link Address
		if($i==1)
			{
			//verify the navigation
			_assertVisible(_heading1($Generic[$i][3]));
			_assertVisible(_div("breadcrumb"));
			_assertVisible(_link($Generic[$i][3], _in(_div("breadcrumb"))));
			}
		//click on link payment settings
		if($i==2)
			{
			//verify the navigation
			_assertVisible(_heading1($Generic[$i][3]));
			_assertVisible(_div("breadcrumb"));
			_assertVisible(_link($Generic[2][2], _in(_div("breadcrumb"))));
			}
		//click on link order history
		if($i==3)
			{
			//verify the navigation
			_assertVisible(_heading1($Generic[$i][4]));
			_assertVisible(_div("breadcrumb"));
			_assertVisible(_link($Generic[$i][4], _in(_div("breadcrumb"))));
			}
		//click on link modify wish list & Search Wish Lists
		if($i==4 || $i==5)
			{
			//verify the navigation
			_assertVisible(_heading1($Generic[4][3]));
			_assertVisible(_div("breadcrumb"));
			_assertVisible(_link($Generic[4][4], _in(_div("breadcrumb"))));
			}
		//click on link create registry & Search Registries & Modify Registries
		if($i==6 || $i==7 || $i==8)
			{
			//verify the navigation 
			_assertVisible(_heading1("/"+$Generic[6][3]+"/"));
			_assertVisible(_div("breadcrumb"));
			_assertVisible(_link($Generic[6][4], _in(_div("breadcrumb"))));
			}
		//click on link privacy policy
		if($i==9)
			{
			//verify the navigation 
			_assertVisible(_heading1($Generic[$i][3]));
			_assertContainsText($Generic[$i][3], _div("breadcrumb"));
			
			}
		//click on link Secure Shopping
		if($i==10)
			{
			//verify the navigation 
			_assertVisible(_heading1($Generic[$i][3]));
			_assertContainsText($Generic[$i][3], _div("breadcrumb"));
			}
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();
