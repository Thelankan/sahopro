_include("../../util.GenericLibrary/GlobalFunctions.js");
_include("../../util.GenericLibrary/BrowserSpecific.js");
_include("../../util.GenericLibrary/BM_Configuration.js");
_resource("Payment.xls");

SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();

var $Payment_Data=_readExcelFile("Payment.xls","Payment_Data");
var $Createcard=_readExcelFile("Payment.xls","CreateCard");
var $Validations=_readExcelFile("Payment.xls","Creditcard_validations");

//login to the application
_click(_link("Login"));
login();
DeleteCreditCard();


var $t = _testcase("125897","Verify the UI of 'My Account Payment page' when there are no payment methods added in application as a Registered user");
$t.start();
try
{
	//navigate to payment page	
	_click(_link("Payment Settings"));
	//verify the UI
	//Brad crumb
	_assertVisible(_link($Payment_Data[0][2], _in(_div("breadcrumb"))));
	//heading
	_assertVisible(_heading1($Payment_Data[0][3]));
	_assertVisible(_div("/"+$Payment_Data[1][3]+"/"));
	_assertVisible(_link("Add Credit Card") );
	//left nav section
	var $Heading=_count("_span", "/(.*)/",_in(_div("secondary-navigation")));
	var $Links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
	       {
	              _assertVisible(_span($Payment_Data[$i][0]));                
	       }
	//checking links
	for(var $i=0;$i<$Links;$i++)
	       {
	              _assertVisible(_link($Payment_Data[$i][1]));                
	       }
	//need help section
	_assertVisible(_heading2("/"+$Payment_Data[2][3]+"/"));
	_assertVisible(_div("account-nav-asset"));
	//contact us link
	_assertVisible(_link($Payment_Data[3][3]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();




var $t = _testcase("124779/128723","Verify the navigation related to 'Payment Settings' menu on my Account Home  page and Verify the UI of 'My Account Payment page' when there are Multiple payment methods added in application as a Registered user");
$t.start();
try
{
	//navigate to payment page	
	_click(_link("Payment Settings"));
	//verify the UI
	//Brad crumb
	_assertVisible(_link($Payment_Data[0][2], _in(_div("breadcrumb"))));
	//heading
	_assertVisible(_heading1($Payment_Data[0][3]));
	_assertVisible(_div("/"+$Payment_Data[1][3]+"/"));
	_assertVisible(_link("Add Credit Card") );
	//left nav section
	var $Heading=_count("_span", "/(.*)/",_in(_div("secondary-navigation")));
	var $Links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
	       {
	              _assertVisible(_span($Payment_Data[$i][0]));                
	       }
	//checking links
	for(var $i=0;$i<$Links;$i++)
	       {
	              _assertVisible(_link($Payment_Data[$i][1]));                
	       }
	//need help section
	_assertVisible(_heading2("/"+$Payment_Data[2][3]+"/"));
	_assertVisible(_div("account-nav-asset"));
	//contact us link
	_assertVisible(_link($Payment_Data[3][3]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("124787","Verify the navigation related to 'Home'  and  'My Account' links  on   'My Account Payment page' bread crumb in application as a  Registered user");
$t.start();
try
{
	//click on home link present in the bread crumb
	//_click(_link("Home",_in(_div("breadcrumb"))));
	
	//Navigate to home page
	_click(_link("Demandware SiteGenesis"));
	//verify the navigation to home page
	_assertVisible(_list("homepage-slides"));
	_assertVisible(_div("home-bottom-slots"));
	//navigating to wish list login page
	_click(_link("Wish List"));
	//click on my account link present in the bread crumb
	_click(_link($Payment_Data[1][2],_in(_div("breadcrumb"))));
	//verify the navigation
	_assertVisible(_link($Payment_Data[1][2], _in(_div("breadcrumb"))));
	_assertVisible(_heading1("/"+$Payment_Data[1][2]+"/"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124781","Verify the functionality related tothe 'ADD A CREDIT CARD' link present in the Payment section header on 'My Account Payment page' of the application as a Registered user");
$t.start();
try
{
	//navigating to Payment setting page 	
	_click(_link("user-account"));
	_click(_link("Payment Settings"));
	_click(_link("Add Credit Card") );
	if (_isVisible(_div("dialog-container")))
		{		  
		  _assert(true, "Add a Credit Card overlay is displayed");		
		}
	else
		{
		_assert(false, "Add a Credit Card overlay is not displayed");
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124782","Verify the UI of Add a credit card overlay on payment methods page in application as a Registered user");
$t.start();
try
{
	//verify the UI of Add a credit card overlay
	//Name on Card
	_assertVisible(_span("Name on Card"));
	_assertVisible(_textbox("dwfrm_paymentinstruments_creditcards_newcreditcard_owner"));
	//Tyep
	_assertVisible(_span("Type"));
	_assertVisible(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_type"));
	//card number
	_assertVisible(_span("Number"));
	_assertVisible(_textbox("/dwfrm_paymentinstruments_creditcards_newcreditcard_number/"));
	//month
	_assertVisible(_span("Month"));
	_assertVisible(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_month"));
	//Year
	_assertVisible(_span("Year"));
	_assertVisible(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_year"));
	//checking which credit card type is enabled
	if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
		{
		//first name
		_assertVisible(_span("First Name"));
		_assertVisible(_textbox("dwfrm_paymentinstruments_creditcards_address_firstname"));
		//last name
		_assertVisible(_span("Last Name"));
		_assertVisible(_textbox("dwfrm_paymentinstruments_creditcards_address_lastname"));
		//address1
		_assertVisible(_span("Address 1"));
		_assertVisible(_textbox("dwfrm_paymentinstruments_creditcards_address_address1"));
		//address2
		_assertVisible(_span("Address 2"));
		_assertVisible(_textbox("dwfrm_paymentinstruments_creditcards_address_address2"));
		//country
		_assertVisible(_span("Country"));
		_assertVisible(_select("dwfrm_paymentinstruments_creditcards_address_country"));
		//city
		_assertVisible(_span("City"));
		_assertVisible(_textbox("dwfrm_paymentinstruments_creditcards_address_city"));
		//state
		_assertVisible(_span("State"));
		_assertVisible(_select("dwfrm_paymentinstruments_creditcards_address_states_state"));
		//zip code
		_assertVisible(_span("Zip Code"));
		_assertVisible(_textbox("dwfrm_paymentinstruments_creditcards_address_postal"));
		//phone number
		_assertVisible(_span("Phone"));
		_assertVisible(_textbox("dwfrm_paymentinstruments_creditcards_address_phone"));
		}
	_assertVisible(_submit("applyBtn"));
	_assertVisible(_submit("Cancel"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124783","Verify the functionality related to 'X' icon 'Add a credit card overlay payment methods page' in application as a Registered user");
$t.start();
try
{
	//click on close link
	if(_isVisible(_button("Close")))
		{
		_click(_button("Close"));
		}
	//overlay should not be displayed
	closedOverlayVerification();
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124784","Verify the functionality related to 'Cancel' button the Add a credit card overlay on my account payment methods page of the application as a Registered user");
$t.start();
try
{
	//open the credit card overlay
	_click(_link("Add Credit Card") );
	//filling details in create card fields
	if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
		{		
			CreateCreditCard(0,$Createcard, true);
		}
	else
		{
			CreateCreditCard(0,$Createcard);
		}		
	//click on cancel button
	_click(_submit("Cancel"));
	//verify whether closed or not 
	closedOverlayVerification();
	//open the credit card overlay
	_click(_link("Add Credit Card") );
	//click on cancel button
	_click(_submit("Cancel"));
	//verify whether closed or not 
	closedOverlayVerification();
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124785","Verify the functionality related to 'Apply' button the Add a credit card overlay on my account payment methods page of the application as a Registered user");
$t.start();
try
{
	//open the credit card overlay
	_click(_link("Add Credit Card") );
	//filling details in create card fields
	if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
		{		
			CreateCreditCard(0,$Createcard, true);
		}
	else
		{
			CreateCreditCard(0,$Createcard);
		}	
	//click on apply button
	_click(_submit("applyBtn"));
	//overlay got closed or not
	closedOverlayVerification();
	//verify whether credit card got saved or not and also UI
	_assertVisible(_list("payment-list"));
	var $saveInfo=_getText(_listItem("/first/")).split(" ");
	var $actualExpdate=$saveInfo[3]+""+$saveInfo[4];
	_assertEqual($Createcard[0][1],$saveInfo[0]);
	_assertEqual($Createcard[0][2],$saveInfo[1]);
	var $expDate=$Createcard[0][15]+"."+$Createcard[1][4]+"."+$Createcard[0][5];
	_assertEqual($expDate,$actualExpdate);
	_assertVisible(_submit("Delete Card"));
	//left nav section
	var $Heading=_count("_span", "/(.*)/",_in(_div("secondary-navigation")));
	var $Links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
	       {
	              _assertVisible(_span($Payment_Data[$i][0]));                
	       }
	//checking links
	for(var $i=0;$i<$Links;$i++)
	       {
	              _assertVisible(_link($Payment_Data[$i][1]));                
	       }
	//need help section
	_assertVisible(_heading2("/"+$Payment_Data[2][3]+"/"));
	_assertVisible(_div("account-nav-asset"));
	//contact us link
	_assertVisible(_link($Payment_Data[3][3]));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124790","Verify the functionality related tothe 'Delete card' link present in the Payment section header on 'My Account Payment page' of the application as a Registered user");
$t.start();
try
{
	//click on delete card link
	while(_isVisible(_submit("Delete Card")))
		{
		_click(_submit("Delete Card"));
		}
	//click on ok in confirmation overlay
	_expectConfirm("/Do you want/",true);
	//verify whether deleted or not
	_assertNotVisible(_list("payment-list"));
	_assertNotVisible(_submit("Delete Card"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("125899/148683/148684/148685/148686/148687/148688/148689/148690/148691/148692/148693/148694","Verify the validation of 'Name on Card'/'type'/number/first name/last name/address1/city/state/zip code/phone field present in the Add a credit card overlay on my account payment methods page of the application as a Registered user");
$t.start();
try
{
	if (_isIE())
	{
	_wait(2000,_isVisible(_link("Add Credit Card") ));
	}
	_click(_link("Add Credit Card") );
	//validation of credit card overlay
	for(var $i=0;$i<$Validations.length;$i++)
		{
		if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
			{		
				CreateCreditCard($i,$Validations, true);
			}
		else
			{
				CreateCreditCard($i,$Validations);
			}
		_click(_submit("applyBtn"));
		_log($i);
		//blank
		if($i==0)
			{
				_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_newcreditcard_owner"),"background-color"));
				_assertEqual($Validations[0][15],_style(_textbox("/dwfrm_paymentinstruments_creditcards_newcreditcard_number/"),"background-color"));
				if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
				{
					_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_firstname"),"background-color"));
					_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_lastname"),"background-color"));
					_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_address1"),"background-color"));
					_assertEqual($Validations[0][15],_style(_select("dwfrm_paymentinstruments_creditcards_address_country"),"background-color"));
					_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_city"),"background-color"));
					_assertEqual($Validations[0][15],_style(_select("dwfrm_paymentinstruments_creditcards_address_states_state"),"background-color"));
					_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_postal"),"background-color"));
					_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_phone"),"background-color"));
				}
			}
		//max length
		if($i==1)
			{
			//_assertEqual($Validations[1][15],_getText(_textbox("dwfrm_paymentinstruments_creditcards_newcreditcard_owner")).length);
			//_assertEqual($Validations[1][16],_getText(_textbox("/dwfrm_paymentinstruments_creditcards_newcreditcard_number/")).length);
			if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
				{
					_assertEqual($Validations[1][17],_getText(_textbox("dwfrm_paymentinstruments_creditcards_address_firstname")).length);
					_assertEqual($Validations[1][17],_getText(_textbox("dwfrm_paymentinstruments_creditcards_address_lastname")).length);
					_assertEqual($Validations[1][17],_getText(_textbox("dwfrm_paymentinstruments_creditcards_address_address1")).length);
					_assertEqual($Validations[1][17],_getText(_textbox("dwfrm_paymentinstruments_creditcards_address_address2")).length);
					_assertEqual($Validations[1][17],_getText(_textbox("dwfrm_paymentinstruments_creditcards_address_city")).length);
					_assertEqual($Validations[1][18],_getText(_textbox("dwfrm_paymentinstruments_creditcards_address_postal")).length);
					_assertEqual($Validations[1][19],_getText(_textbox("dwfrm_paymentinstruments_creditcards_address_phone")).length);
				}			
			}
		//invalid card number & Exp Year
		if($i==2)
			{
			//card number 
			_assertEqual($Validations[0][16],_style(_span("Number"),"color"));
			_assertVisible(_div($Validations[2][15]));
			_assertEqual($Validations[0][15],_style(_div($Validations[2][15]),"background-color"));
			//Exp Year
			_assertEqual($Validations[0][16],_style(_span("Month"),"color"));
			_assertEqual($Validations[0][16],_style(_span("Year"),"color"));
			_assertVisible(_div($Validations[2][16]));
			_assertEqual($Validations[0][15],_style(_div($Validations[2][16]),"background-color"));
			}
		//invalid in zip code,phone number & number
		if($i==3)
			{
			//number
			_assertEqual($Validations[0][16],_style(_span("Number"),"color"));
			_assertVisible(_div($Validations[2][15]));
			_assertEqual($Validations[0][15],_style(_div($Validations[2][15]),"background-color"));		
			}
		if($i==5)
			{
			if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
			{
				//zip code
				_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_postal"),"background-color"));
				_assertVisible(_span($Validations[3][15]));
				_assertEqual($Validations[0][16],_style(_span($Validations[3][15]),"color"));
			}
			}
		if($i==4)
			{	
			if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
				{
					_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_phone"),"background-color"));
					_assertVisible(_span($Validations[3][16]));
					_assertEqual($Validations[0][16],_style(_span($Validations[3][16]),"color"));	
				}				
			}
		
		if($i==6)
			{
			_assertEqual($Validations[0][16],_style(_div($Validations[2][15]),"color"));
			if($CreditCardPayment==$BMConfig[2][4] || $CreditCardPayment==$BMConfig[1][4])
				{
					//zip code
					_assertEqual($Validations[0][15],_style(_textbox("dwfrm_paymentinstruments_creditcards_address_postal"),"background-color"));
					_assertVisible(_span($Validations[3][15]));
					_assertEqual($Validations[0][16],_style(_span($Validations[3][15]),"color"));
				}
			
			}
		}
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

cleanup();