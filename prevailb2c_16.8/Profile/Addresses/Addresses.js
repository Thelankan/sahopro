_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Address_Data.xls");

var $Generic=_readExcelFile("Address_Data.xls","Generic");
var $Addr_Validation=_readExcelFile("Address_Data.xls","Address_Validation");
var $Background=_readExcelFile("Address_Data.xls","Background");
var $State_Country=_readExcelFile("Address_Data.xls","State_Country_Validation");
var $Edit_Addr_Validation=_readExcelFile("Address_Data.xls","Edit_Addr_Validation");
var $Valid_Address=_readExcelFile("Address_Data.xls","Valid_Address");
var $color=$Background[0][0];

//enable make default
//enableMakeDefault();

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//login
_click(_link("Login"));
login();


//Precondition
deleteAddress();


var $t = _testcase("124756", "Verify the navigation of 'Addresses' link in the My Account home page");
$t.start();
try
{
	//Click on home page link
	_click(_image("logo.png"));
	//click on user account link
	_click(_link("user-account"));
	_click(_link("My Account"));
	//Click on link addresses
	_click(_link("Addresses")); 
	//Address page
	_assertVisible(_heading1($Generic[0][0]));
	//breadcrumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[1][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[1][0], _getText(_div("breadcrumb")));
		}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
 $t.end();
 

//UI without addresses
var $t = _testcase("124757", "Verify the UI  of 'My Account Addresses page' in application as a  Registered user.");
$t.start();
try
{
	//heading
	_assertVisible(_heading1($Generic[0][0]));
	//breadcrumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[1][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[1][0], _getText(_div("breadcrumb")));
		}
	//addresses text and create link
	//_assertVisible(_div("section-header"));
	_assertVisible(_link("Create New Address"));	
	//left nav
	_assertVisible(_div("secondary-navigation"));
	var $Heading=_count("_span", "/(.*)/",_in(_div("secondary-navigation")));
	var $Links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
		{
			_assertVisible(_span($Generic[$i][1]));			
		}
	//checking links
	for(var $i=0;$i<$Links;$i++)
		{
			_assertVisible(_link($Generic[$i][2]));			
		}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
 $t.end();
 
 
 var $t = _testcase("124761", "Verify the navigation related to 'Home' and 'My Account' links on My  account Addresses page bread crumb in application as a  Registered user.");
 $t.start();
 try
 {
 	//breadcrumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[1][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[1][0], _getText(_div("breadcrumb")));
		}
	//Bread crumb last
	_assertVisible(_link($Generic[0][0], _in(_div("breadcrumb"))));
	//Navigation to My account
	_assertVisible(_link("My Account", _in(_div("breadcrumb"))));
	_click(_link("My Account", _in(_div("breadcrumb"))));
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[3][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[3][0], _getText(_div("breadcrumb")));
		}
	_assertVisible(_link("My Account", _in(_div("breadcrumb"))));
	//resetting to addresses Page
	_click(_link("Addresses"));
	if(_isIE9())
		{
		_assertEqual($Generic[1][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[1][0], _getText(_div("breadcrumb")));
		}
	//Navigation to Home
	//_assertVisible(_link("Home", _in(_div("breadcrumb"))));
	//_click(_link("Home", _in(_div("breadcrumb"))));
	//verifying Home page
	//_assertVisible(_div("homepage-slider"));
	//_assertVisible(_div("home-bottom-slots"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}
 $t.end();
 
 var $t = _testcase("124763/124764", "Verify functionality of 'Create New Address' link and the UI elements in the 'Add Address' overlay in the My Account - Addresses page");
 $t.start();
 try
 {
	_click(_link("user-account")); 
	_click(_link("My Account"));
	_click(_link("Addresses"));  	
	_click(_link("Create New Address"));
	_wait(5000, _isVisible(_div("dialog-container")));
	 if(_isVisible(_div("dialog-container")))
	 {
		 //heading
		 _assertVisible(_heading1($Generic[0][3]));
		// _assertVisible(_fieldset("/"+$Generic[1][3]+"/"));  //(commented by rahul because its not visible on webpage)
		 //Close icon
		 _assertVisible(_button("Close"));
		 //Checking the display of fields
		 _assertVisible(_span("Address Name"));
		 _assertVisible(_textbox("dwfrm_profile_address_addressid"));
		 _assertVisible(_span("First Name"));
		 _assertVisible(_textbox("dwfrm_profile_address_firstname"));
		 _assertVisible(_span("Last Name"));
		 _assertVisible(_textbox("dwfrm_profile_address_lastname"));
		 _assertVisible(_span("Address 1"));
		 _assertVisible(_textbox("dwfrm_profile_address_address1"));
		 _assertVisible(_div($Generic[2][3], _in(_div("dialog-container"))));
		 _assertVisible(_span("Address 2"));
		 _assertVisible(_textbox("dwfrm_profile_address_address2"));
		 _assertVisible(_div($Generic[3][3], _in(_div("dialog-container"))));
		 _assertVisible(_span("Country"));
		 _assertVisible(_select("dwfrm_profile_address_country"));
		 _assertVisible(_span("State"));
		 _assertVisible(_select("dwfrm_profile_address_states_state"));
		 _assertVisible(_span("City"));
		 _assertVisible(_textbox("dwfrm_profile_address_city"));
		 _assertVisible(_span("Zip Code"));
		 _assertVisible(_textbox("dwfrm_profile_address_postal"));
		 _assertVisible(_span("Phone"));
		 _assertVisible(_textbox("dwfrm_profile_address_phone"));
		 _assertVisible(_div($Generic[4][3], _in(_div("dialog-container"))));
		 if (!isMobile())
			 {
			 
			 var $count=_count("_link","/tooltip/",_in(_div("dialog-container")));
			 
			 for(var $i=0;$i<$count;$i++)
				 {
				 
				 	_assertVisible(_link("tooltip["+$i+"]"));
				 	_mouseOver(_link("tooltip["+$i+"]"));
				 
			        if(_getAttribute(_link("/.*/",_in(_div("form-field-tooltip["+$i+"]"))),"aria-describedby")!=null)
		              {
		                     _assert(true);
		              }
		        else
		              {
		                     _assert(false);
		              }
				 
				 }
			 
			 
//			 //tool tip
//			 _assertVisible(_link("tooltip"));
//			 //Due to some conflicts in Chrome
//			 //var $TooltipMsg=_extract(_getText(_link("tooltip")),"/(.*)[?]/",true).toString()+"?";
//			 
//			 //Verifying why is this required tool tip
//			 _mouseOver(_link("/tooltip/",_near(_textbox("dwfrm_profile_address_phone"))));
//			 
//			 var $TooltipMsg=_getText(_link("tooltip", _in(_div("Why is this required?"))));
//			 _assertEqual($Generic[5][3],$TooltipMsg);
			 
			 }
		 //Apply and cancel button
		 _assertVisible(_submit("Apply"));
		 _assertVisible(_submit("Cancel"));
		 _click(_button("Close"));
	 }
	 else
	 {
		 _assert(false,"Add address Overlay is not displaying"); 
	 }
 }
 catch($e)
 {
 	_logExceptionAsFailure($e);
 }
 $t.end();
 
 var $t = _testcase("124767", "Verify the functionality related to 'X' icon  'Add Address overlay Addresses page' in application as a  Registered user.");
 $t.start();
 try
 {
	//Click on create new address
	 _click(_link("Create New Address"));
	 //Checking close functionality
	 _assertVisible(_button("Close"));
	 _click(_button("Close"));
	 _assertNotVisible(_div("dialog-container"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}
 $t.end();
 
/* if (!isMobile())
	 {
 var $t = _testcase("124769", "Verify the navigation related to 'Why is this required '  tool tip  on   'Add Address overlay Addresses page' in application as a  Registered  user.");
 $t.start();
 try
 {
	 //Click on create new address
	 _click(_link("Create New Address"));
	 _assertVisible(_link("/tooltip/",_near(_textbox("dwfrm_profile_address_phone"))));
	 //Click on tool tip
	 _click(_link("/tooltip/",_near(_textbox("dwfrm_profile_address_phone"))));
	 //Validating navigation
	 _assertVisible(_div("breadcrumb"));
	 if(_isIE9())
		{
		_assertEqual($Generic[4][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[4][0], _getText(_div("breadcrumb")));
		}
	 _assertVisible(_div("content-asset"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}
 $t.end();
	 }
 
 _click(_link("user-account"));
 _click(_link("My Account"));
 _click(_link("Addresses"));*/
 var $t = _testcase("--/148666", "Verify the validation related to 'Country', 'State'  drop down  on  'Add Address overlay Addresses page' in application as a  Registered user.");
 $t.start();
 try
 {
	 //Balnk field validation And valid data  of this is covered in next test script
	 //Click on create new address
	 if (isMobile())
		 {
		 _wait(2000,_isVisible(_link("Create New Address")));
		 _click(_link("Create New Address"));
		 }
	 else
		 {
	 _click(_link("Create New Address"));
		 }
	 _assertVisible(_select("dwfrm_profile_address_states_state"));
	 _assertEqual($State_Country[0][11], _getSelectedText(_select("dwfrm_profile_address_states_state")));
	 _assertVisible(_select("dwfrm_profile_address_country"));
	 _assertEqual($State_Country[0][12], _getSelectedText(_select("dwfrm_profile_address_country")));
	 for(var $i=0;$i<$State_Country.length-3;$i++)
		{
		 	_log($i);
			_setValue(_textbox("dwfrm_profile_address_addressid"), $State_Country[$i][1]);  
			_setValue(_textbox("dwfrm_profile_address_firstname"), $State_Country[$i][2]);  
			_setValue(_textbox("dwfrm_profile_address_lastname"), $State_Country[$i][3]);  
			_setValue(_textbox("dwfrm_profile_address_address1"), $State_Country[$i][4]);  
			_setValue(_textbox("dwfrm_profile_address_address2"), $State_Country[$i][5]);  
			_setSelected(_select("dwfrm_profile_address_country"),$State_Country[$i][6]);  
			//Blank field validation
			if($i==0)
				{
					_setSelected(_select("dwfrm_profile_address_states_state"), $State_Country[$i][7]);  
					_setValue(_textbox("dwfrm_profile_address_city"), $State_Country[$i][8]);  
					_setValue(_textbox("dwfrm_profile_address_postal"), $State_Country[$i][9]);  
					_setValue(_textbox("dwfrm_profile_address_phone"),$State_Country[$i][10]);  	
				 	_click(_submit("Apply"));
					//Checking background color
				 	_assertEqual($color, _style(_select("dwfrm_profile_address_states_state"), "background-color"));    
				}		
			//when different Countries are selected
			else 
				{
				  _assertEqual($State_Country[$i][11], _getText(_select("dwfrm_profile_address_states_state")).toString());
				}
		}
	 _click(_button("Close"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}
 $t.end(); 
 
 
 var $t = _testcase("128532/148661/148662/148663/148664/148667/148669/124770/124766/148681", "Verify the validation related to 'Address Name','First Name','Last  Name','Address 1','Address 2','City','Phone' fields and functionality related to 'Apply' button & 'cancel' button on 'Add Address overlay Addresses page'in application as a  Registered  user.");
 $t.start();
 try
 {
	for(var $i=0;$i<$Addr_Validation.length;$i++)
		{
			_log($i);
			_click(_link("Create New Address"));
			_wait(5000, _isVisible(_div("dialog-container")));
			 if(_isVisible(_div("dialog-container")))
				 {
					 addAddress($Addr_Validation,$i);
					
					 //blank field validation
					 if($i==0)
						 {
						 	_click(_submit("Apply"));
							_assertEqual($color, _style(_textbox("dwfrm_profile_address_addressid"), "background-color"));
							_assertEqual($color, _style(_textbox("dwfrm_profile_address_firstname"), "background-color"));
							_assertEqual($color, _style(_textbox("dwfrm_profile_address_lastname"), "background-color"));
							_assertEqual($color, _style(_textbox("dwfrm_profile_address_address1"), "background-color"));
							//_assertEqual($color, _style(_select("dwfrm_profile_address_country"), "background-color"));
							_assertEqual($color, _style(_select("dwfrm_profile_address_states_state"), "background-color"));
							_assertEqual($color, _style(_textbox("dwfrm_profile_address_city"), "background-color"));
							_assertEqual($color, _style(_textbox("dwfrm_profile_address_postal"), "background-color"));
					        _assertEqual($color, _style(_textbox("dwfrm_profile_address_phone"), "background-color")); 
					        _click(_button("Close"));
						 }
					 //cancel functionality without entering values
					 else if($i==1)
						 {
						 	
						 	_click(_submit("Cancel"));
						 	_assertNotVisible(_div("dialog-container"));
						 	_assertNotVisible(_list("address-list"));
						 }
					 //Special characters, Phone number inavlid format and alphanumeric value
					 else if($i==2 || $i==3 ||$i==5)
						 {
						
						 	_click(_submit("Apply"));
						 	_assertVisible(_span($Addr_Validation[$i][14]));
						 	 if(_isVisible(_span("error", _in(_div("/Phone/")))))
								{
									_assertEqual($Addr_Validation[$i][12],_getText(_span("error", _in(_div("/Phone/")))));
								}
						 	 else if(_isVisible(_span($Addr_Validation[$i][12])))
						 		 {
						 		 _assertVisible(_span($Addr_Validation[$i][12]));
						 		 }
			                else
			     
								{
									_assert(false, "Error message for Phone field is not displayed as expected");
								}
						 	
						 	
						 	 _click(_button("Close"));
						 }
					 //Max characters
					 else if($i==4)
						 {
						 		
						 		 _assertEqual($Addr_Validation[0][13],_getText(_textbox("dwfrm_profile_address_addressid")).length);  
								 _assertEqual($Addr_Validation[1][13],_getText(_textbox("dwfrm_profile_address_firstname")).length);
								 _assertEqual($Addr_Validation[2][13],_getText(_textbox("dwfrm_profile_address_lastname")).length);
								 _assertEqual($Addr_Validation[3][13],_getText(_textbox("dwfrm_profile_address_address1")).length);
								 _assertEqual($Addr_Validation[4][13],_getText(_textbox("dwfrm_profile_address_address2")).length);
								 _assertEqual($Addr_Validation[8][13],_getText(_textbox("dwfrm_profile_address_city")).length);  
								 _assertEqual($Addr_Validation[6][13],_getText(_textbox("dwfrm_profile_address_phone")).length); 
								 _click(_button("Close"));
						 }
					//cancel functionality after entering values
					 else if($i==6)
						 {
						 	_click(_submit("Cancel"));
						 	_assertNotVisible(_div("dialog-container"));
						 	_assertNotVisible(_list("address-list"));						 		
						 }
					 else if($i==8)
						 {
						 
							 _click(_submit("Apply"));
								 	 
						 	 if(_isVisible(_span("form-caption  error-message")))
								{
									_assertEqual($Addr_Validation[$i][11],_getText(_span("form-caption  error-message")));
								}
			                else
								{
									_assert(false, "Error message for Address name field is not displayed as expected");
								}
							 
						 	 _click(_button("Close"));
						 }
					 else
						 {
						
						 	_click(_submit("Apply"));
						 	_assertEqual($Addr_Validation[$i][1],_getText(_div("mini-address-title")));
						 	_assertEqual($Addr_Validation[$i][2]+" "+$Addr_Validation[$i][3],_getText(_div("mini-address-name")));
						 	_assertEqual($Addr_Validation[$i][11], _getText(_div("mini-address-location")));						 
						 }					 
				 }
			 else
				 {
					 _assert(false,"Add address Overlay is not displaying"); 
				 }			
		} 
}
catch($e)
{
	_logExceptionAsFailure($e);
}
 $t.end();
 

 var $t=_testcase("124775/148670","Verify the functionality related tothe 'Edit'  link present below each of the addresses on my account Addresses page and Verify the UI of the Edit  Address  overlay  Address page of the application as a Registered user.");
 $t.start();
 try
 {
	 _assertVisible(_link("address-edit"));
	 if(_isVisible(_link("Edit")))
		 {
		 	_click(_link("Edit"));
		 	_wait(5000, _isVisible(_div("dialog-container")));
		 	 if(_isVisible(_div("dialog-container")))
			 {
		 		 //heading
				 _assertVisible(_heading1($Generic[0][4]));
				// _assertVisible(_fieldset("/"+$Generic[1][4]+"/"));  //(commented by rahul because its not present on webpage)
				 //Close icon
				 _assertVisible(_button("Close"));
				 //Checking the display of fields
				 _assertVisible(_span("Address Name"));
				 _assertVisible(_textbox("dwfrm_profile_address_addressid"));
				 _assertEqual($Addr_Validation[7][1], _getValue(_textbox("dwfrm_profile_address_addressid")));
				 _assertVisible(_span("First Name"));
				 _assertVisible(_textbox("dwfrm_profile_address_firstname"));
				 _assertEqual($Addr_Validation[7][2], _getValue(_textbox("dwfrm_profile_address_firstname")));
				 _assertVisible(_span("Last Name"));
				 _assertVisible(_textbox("dwfrm_profile_address_lastname"));
				 _assertEqual($Addr_Validation[7][3], _getValue(_textbox("dwfrm_profile_address_lastname")));
				 _assertVisible(_span("Address 1"));
				 _assertVisible(_textbox("dwfrm_profile_address_address1"));
				 _assertEqual($Addr_Validation[7][4], _getValue(_textbox("dwfrm_profile_address_address1")));
				 _assertVisible(_div($Generic[2][3], _in(_div("dialog-container"))));
				 _assertVisible(_span("Address 2"));
				 _assertVisible(_textbox("dwfrm_profile_address_address2"));
				 _assertEqual($Addr_Validation[7][5], _getValue(_textbox("dwfrm_profile_address_address2")));
				 _assertVisible(_div($Generic[3][3], _in(_div("dialog-container"))));
				 _assertVisible(_span("Country"));
				 _assertVisible(_select("dwfrm_profile_address_country"));
				 _assertEqual($Addr_Validation[7][6], _getSelectedText(_select("dwfrm_profile_address_country")));
				 _assertVisible(_span("State"));
				 _assertVisible(_select("dwfrm_profile_address_states_state"));
				 _assertEqual($Addr_Validation[7][7], _getSelectedText(_select("dwfrm_profile_address_states_state")));
				 _assertVisible(_span("City"));
				 _assertVisible(_textbox("dwfrm_profile_address_city"));
				 _assertEqual($Addr_Validation[7][8], _getValue(_textbox("dwfrm_profile_address_city")));
				 _assertVisible(_span("Zip Code"));
				 _assertVisible(_textbox("dwfrm_profile_address_postal"));
				 _assertEqual($Addr_Validation[7][9], _getValue(_textbox("dwfrm_profile_address_postal")));
				 _assertVisible(_span("Phone"));
				 _assertVisible(_textbox("dwfrm_profile_address_phone"));
				 _assertEqual($Addr_Validation[7][10], _getValue(_textbox("dwfrm_profile_address_phone")));
				 _assertVisible(_div($Generic[4][4], _in(_div("dialog-container"))));
				 if(!isMobile() || mobile.iPad())
					 {
					 //tool tip
					 _assertVisible(_link("tooltip",_near(_textbox("dwfrm_profile_address_phone"))));
					 var $TooltipMsg=_extract(_getText(_link("tooltip",_near(_textbox("dwfrm_profile_address_phone")))),"/(.*)[?]/",true).toString()+"?";
					 _assertEqual($Generic[5][4],$TooltipMsg);
					 }
				 //Apply and cancel button
				 _assertVisible(_submit("Apply"));
				 _assertVisible(_submit("Cancel"));
				 _assertVisible(_submit("Delete"));
				 _click(_button("Close"));		 		 
			 }
		 	else
			 {
				 _assert(false,"Edit address Overlay is not displaying"); 
			 }		 	
		 }
	 else
		 {
		 	_assert(false, "Edit link does not exits");
		 }
 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end();
 

 var $t=_testcase("148672","Verify the functionality related to 'X' icon  'Edit Address overlay Addresses page' in application as a  Registered user.");
 $t.start();
 try
 {
	 	_click(_link("Edit"));
	 	_wait(5000, _isVisible(_div("dialog-container")));
	 	 if(_isVisible(_div("dialog-container")))
		 {
	 		 //Checking close functionality
	 		 _assertVisible(_button("Close"));
	 		 _click(_button("Close"));
	 		 _assertNotVisible(_div("dialog-container"));
	 		_assertVisible(_list("address-list"));
		 }
	 	else
		 {
			 _assert(false,"Edit address Overlay is not displaying"); 
		 }	

 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end();
 
 
if (!isMobile())
	{
 var $t=_testcase("148671","Verify the navigation related to 'Why is this required '  link  on   'Edit Address overlay Addresses page' in application as a  Registered  user.");
 $t.start();
 try
 {
	 	_click(_link("Edit"));
	 	_wait(5000, _isVisible(_div("dialog-container")));
	 	 if(_isVisible(_div("dialog-container")))
		 {
			 //tool tip
			 _assertVisible(_link("tooltip",_near(_textbox("dwfrm_profile_address_phone"))));
			 var $TooltipMsg=_extract(_getText(_link("tooltip",_near(_textbox("dwfrm_profile_address_phone")))),"/(.*)[?]/",true).toString()+"?";
			 _assertEqual($Generic[5][4],$TooltipMsg);
			 
//On click of tool tip application is navigating to no where thays y commenting below mentioned code			 
//	 		 //Click on tool tip
//	 		 _click(_link("tooltip"));
//	 		 //Validating navigation
//	 		 _assertVisible(_div("breadcrumb"));
//	 		if(_isIE9())
//				{
//				_assertEqual($Generic[4][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
//				}
//			else
//				{
//				_assertEqual($Generic[4][0], _getText(_div("breadcrumb")));
//				}
//	 		 _assertVisible(_div("content-asset"));
		 }
	 	else
		 {
			 _assert(false,"Edit address Overlay is not displaying"); 
		 }	

 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end();
	}
 
 _click(_link("user-account"));
 _click(_link("My Account"));
 _click(_link("Addresses")); 
 var $t=_testcase("124776","Verify the functionality related to 'Delete' button 'Edit Address overlay Addresses page' in application as a Registered user.");
 $t.start();
 try
 {
	 _click(_link("Edit"));
	 _wait(5000, _isVisible(_div("dialog-container")));
 	 if(_isVisible(_div("dialog-container")))
	 {
 		 //Checking close functionality
 		 _assertVisible(_submit("Delete"));
 		 _click(_submit("Delete"));
 	      //_wait(1000);
 	 	 _expectConfirm("/Do you want/",true);
 		 _assertNotVisible(_div("dialog-container"));
 		 _assertNotVisible(_list("address-list"));
	 }
 	else
	 {
		 _assert(false,"Edit address Overlay is not displaying"); 
	 }

 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end();

 ///setup
 _click(_link("user-account"));
 _click(_link("My Account"));
 _click(_link("Addresses")); 
 for(var $i=0;$i<$Valid_Address.length;$i++)  
	{
	 _click(_link("Create New Address"));
	 _wait(5000, _isVisible(_div("dialog-container")));
	 if(_isVisible(_div("dialog-container")))
		 {
			 addAddress($Valid_Address,$i);	
			 _click(_submit("Apply"));
			 _wait(3000);
		 }
	 else
		 {
			 _assert(false,"Add address Overlay is not displaying"); 
		 }
	}
 

 var $t=_testcase("124774","Verify the functionality related tothe 'Make Default'  link present below each of the addresses on my account Addresses page of the application as a Registered user.");
 $t.start();
 try
 {
	//click on make default link
	 _click(_link("Make Default"));
	 //comparing address after making it as default
	 var $expaddress=$Valid_Address[1][4]+" "+$Valid_Address[1][5]+" "+$Valid_Address[1][8]+", "+$Valid_Address[1][7]+" "+$Valid_Address[1][9]+" "+$Valid_Address[1][6]+" Phone: "+$Valid_Address[1][10];
	 _assertEqual($expaddress,_getText(_div("mini-address-location"))).toString();
	 
	//reset back
	//click on make default link
	 _click(_link("Make Default"));
	 //comparing address after making it as default
	 var $expaddress=$Valid_Address[0][4]+" "+$Valid_Address[0][5]+" "+$Valid_Address[0][8]+", "+$Valid_Address[0][7]+" "+$Valid_Address[0][9]+" "+$Valid_Address[0][6]+" Phone: "+$Valid_Address[0][10];
	 _assertEqual($expaddress,_getText(_div("mini-address-location"))).toString();
	 
 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end();
 

 var $t = _testcase("148678/148679", "Verify the validation related to 'Country', 'State'  drop down  on  'Edit Address overlay Addresses page' in application as a  Registered user.");
 $t.start();
 try
 {
	 //Balnk field validation And valid data  of this is covered in next test script
	 //Click on edit
	 _click(_link("user-account"));  
	 _click(_link("My Account"));
	_click(_link("Addresses"));  	
	 _click(_link("Edit"));
	 for(var $i=0;$i<$State_Country.length-3;$i++)
		{
			_setValue(_textbox("dwfrm_profile_address_addressid"), $State_Country[$i][1]);  
			_setValue(_textbox("dwfrm_profile_address_firstname"), $State_Country[$i][2]);  
			_setValue(_textbox("dwfrm_profile_address_lastname"), $State_Country[$i][3]);  
			_setValue(_textbox("dwfrm_profile_address_address1"), $State_Country[$i][4]);  
			_setValue(_textbox("dwfrm_profile_address_address2"), $State_Country[$i][5]);  
			_setSelected(_select("dwfrm_profile_address_country"),$State_Country[$i][6]);  		
			//Blank field validation
			if($i==0)
				{
					_setSelected(_select("dwfrm_profile_address_states_state"),"");  	
					_setValue(_textbox("dwfrm_profile_address_city"), $State_Country[$i][8]);  	
					_setValue(_textbox("dwfrm_profile_address_postal"), $State_Country[$i][9]);  	
					_setValue(_textbox("dwfrm_profile_address_phone"),$State_Country[$i][10]);  		
				 	_click(_submit("Apply"));
					//Checking background color
				 	_assertEqual($color, _style(_select("dwfrm_profile_address_states_state"), "background-color"));    
				}		
			//when different Countries are selected
			else 
				{
				  _assertEqual($State_Country[$i][11], _getText(_select("dwfrm_profile_address_states_state")).toString());
				}
		}
	 _click(_button("Close"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}
 $t.end(); 
 
 
 var $t = _testcase("148673/148674/148675/148676/148677/148680/148682/124777/124778/148668", "Verify the validation related to 'Address Name','First Name','Last  Name','Address 1','Address 2','City','Phone','Zip Code' fields and functionality related to 'Apply' button & 'cancel' button on 'Edit Address overlay Addresses page'in application as a  Registered  user.");
 $t.start();
 try
 {
	for(var $i=0;$i<$Edit_Addr_Validation.length;$i++)
		{
			_click(_link("Edit"));
			_wait(5000, _isVisible(_div("dialog-container")));
			 if(_isVisible(_div("dialog-container")))
				 {
					 addAddress($Edit_Addr_Validation,$i);	
					
					 //blank field validation
					 if($i==0)
						 {
						 	_click(_submit("Apply"));
							_assertEqual($color, _style(_textbox("dwfrm_profile_address_addressid"), "background-color"));
							_assertEqual($color, _style(_textbox("dwfrm_profile_address_firstname"), "background-color"));
							_assertEqual($color, _style(_textbox("dwfrm_profile_address_lastname"), "background-color"));
							_assertEqual($color, _style(_textbox("dwfrm_profile_address_address1"), "background-color"));
							//only one country which is pre selected so this error message will not display
							//_assertEqual($color, _style(_select("dwfrm_profile_address_country"), "background-color"));
							_assertEqual($color, _style(_select("dwfrm_profile_address_states_state"), "background-color"));
							_assertEqual($color, _style(_textbox("dwfrm_profile_address_city"), "background-color"));
							_assertEqual($color, _style(_textbox("dwfrm_profile_address_postal"), "background-color"));
					        _assertEqual($color, _style(_textbox("dwfrm_profile_address_phone"), "background-color")); 
					        _click(_button("Close"));
						 }
					 //cancel functionality without entering values
					 else if($i==1)
						 {
						 	_click(_submit("Cancel"));
						 	_assertNotVisible(_div("dialog-container"));
						 	_assertVisible(_list("address-list"));
						 	_assertEqual($Valid_Address[0][1],_getText(_div("mini-address-title")));
						 	_assertEqual($Valid_Address[0][2]+" "+$Valid_Address[0][3], _getText(_div("mini-address-name")));
						 	_assertEqual($Valid_Address[0][11], _getText(_div("mini-address-location")));
						 }
					 //Special characters, Phone number inavlid format and alphanumeric value
					 else if($i==2 || $i==3 ||$i==5)
						 {
						 	_click(_submit("Apply"));		 	
						 	 if(_isVisible(_span("error", _in(_div("/Phone/")))))
								{
									_assertEqual($Addr_Validation[$i][12],_getText(_span("error", _in(_div("/Phone/")))));
								}
						 	 else if(_isVisible(_span($Addr_Validation[$i][12])))
						 		 {
						 		 _assertVisible(_span($Addr_Validation[$i][12]));
						 		 }
			                else
			     
								{
									_assert(false, "Error message for Phone field is not displayed as expected");
								}
						 	
						 	 _click(_button("Close"));
						 }
					 
					 //Max characters
					 else if($i==4)
						 {
						 		 _assertEqual($Edit_Addr_Validation[0][13],_getText(_textbox("dwfrm_profile_address_addressid")).length);
								 _assertEqual($Edit_Addr_Validation[1][13],_getText(_textbox("dwfrm_profile_address_firstname")).length);
								 _assertEqual($Edit_Addr_Validation[2][13],_getText(_textbox("dwfrm_profile_address_lastname")).length);
								 _assertEqual($Edit_Addr_Validation[3][13],_getText(_textbox("dwfrm_profile_address_address1")).length);
								 _assertEqual($Edit_Addr_Validation[4][13],_getText(_textbox("dwfrm_profile_address_address2")).length);
								 _assertEqual($Edit_Addr_Validation[5][13],_getText(_textbox("dwfrm_profile_address_city")).length);
								 _assertEqual($Edit_Addr_Validation[6][13],_getText(_textbox("dwfrm_profile_address_phone")).length); 
								 _click(_button("Close"));
						 }
					//cancel functionality after entering values
					 else if($i==6)
						 {
						 	_click(_submit("Cancel"));
						 	_assertNotVisible(_div("dialog-container"));
						 	_assertEqual($Valid_Address[0][1],_getText(_div("mini-address-title")));
							_assertEqual($Valid_Address[0][2]+" "+$Valid_Address[0][3], _getText(_div("mini-address-name")));
						 	_assertEqual($Valid_Address[0][11], _getText(_div("mini-address-location")));
						 }
					 else if($i==8)
						 {
							 _click(_submit("Apply"));
						 	 if(_isVisible(_span("form-caption  error-message")))
								{
									_assertEqual($Edit_Addr_Validation[$i][11],_getText(_span("form-caption  error-message")));
								}
			                else
								{
									_assert(false, "Error message for Address name field is not displayed as expected");
								}
						 	 _click(_button("Close"));
						 }
					 else
						 {
						 	_click(_submit("Apply"));
						 	_assertEqual($Edit_Addr_Validation[$i][1],_getText(_div("mini-address-title")));
							_assertEqual($Edit_Addr_Validation[$i][2]+" "+$Edit_Addr_Validation[$i][3], _getText(_div("mini-address-name")));  
						 	_assertEqual($Edit_Addr_Validation[$i][11], _getText(_div("mini-address-location")));						 
						 }					 
				 }
			 else
				 {
					 _assert(false,"Add address Overlay is not displaying"); 
				 }			
		} 
}
catch($e)
{
	_logExceptionAsFailure($e);
}
 $t.end(); 
 
 cleanup();