_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("GiftRegistry_Data.xls");

var $Generic=_readExcelFile("GiftRegistry_Data.xls","Generic");
var $GiftRegistry=_readExcelFile("GiftRegistry_Data.xls","GiftRegistry");
var $EventShippingInfo=_readExcelFile("GiftRegistry_Data.xls","EventShippingInfo");
var $Address=_readExcelFile("GiftRegistry_Data.xls","Address");
var $color=$Generic[0][6];
var $color1=$Generic[1][6];
//from util.GenericLibrary/BrowserSpecific.js
var $FirstName1=$FName;
var $LastName1=$LName;
var $UserEmail=$uId;

SiteURLs()
cleanup();
_setAccessorIgnoreCase(true); 
//Login
_click(_link("Login"));
login();

//Deleting addresses, which intern will delete wishlist and gift registry
deleteAddress();
_wait(4000,_isVisible(_link("Create New Address")));
//adding addresses
for(var $i=0;$i<$Address.length;$i++)
{
	_click(_link("Create New Address"));
	_wait(2000);
	if(_isVisible(_div("dialog-container")))
		 {
			 addAddress($Address,$i);	
			 _click(_submit("Apply"));
			 _wait(4000,_isVisible(_link("Create New Address")));
		 }
	else
		 {
			 _assert(false,"Add address Overlay is not displaying"); 
		 }
}

var $t = _testcase("124335", "Verify the UI  of Pre/Post Event Shipping page  in application as a  Registered user.");
$t.start();
try
{
	_click(_link("My Account"));
	_click(_heading2("Gift Registries"));
	//click on new registry
	_click(_submit("dwfrm_giftregistry_create"));	
	//creating the registry
	createRegistry($GiftRegistry, 0);
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
		}
	//heading
	_assertVisible(_heading1($Generic[0][8]));
	//pre event shipping
	_assertVisible(_heading2($Generic[1][8]));
	_assertVisible(_paragraph($Generic[2][8]));
	//
	_assertVisible(_fieldset($Generic[3][8]));
	//select address drop down
	_assertVisible(_label("Select from Saved Addresses"));
	_assertVisible(_select("dwfrm_giftregistry_eventaddress_addressBeforeList"));
	//first name
	_assertVisible(_span("First Name"));
	_assertVisible(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_firstname"));
	//last name
	_assertVisible(_span("Last Name"));
	_assertVisible(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_lastname"));
	//address 1
	_assertVisible(_span("Address 1"));
	_assertVisible(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address1"));
	_assertVisible(_div($Generic[4][8], _in(_fieldset("address-after"))));
	//tooltip
	_assertContainsText("APO/FPO", _link("tooltip"));
	//address 2
	_assertVisible(_span("Address 2"));
	_assertVisible(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address2"));
	_assertVisible(_div($Generic[5][8], _in(_fieldset("address-after"))));
	//country
	_assertVisible(_span("Country"));
	_assertVisible(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_country"));
	//state
	_assertVisible(_span("State"));
	_assertVisible(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_states_state"));
	//city
	_assertVisible(_span("City"));
	_assertVisible(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_city"));
	//zip code
	_assertVisible(_span("Zip Code"));
	_assertVisible(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_postal"));
	//phone
	_assertVisible(_span("Phone"));
	_assertVisible(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_phone"));
	_assertVisible(_div($Generic[6][8]));
	//tooltip
	_assertContainsText("Why is this required?",_link("tooltip[1]"));

	//
	//Post event shipping
	//heading
	_assertVisible(_heading2($Generic[8][8]));
	_assertVisible(_paragraph($Generic[9][8]));
	//use preevent shipping address button
	_assertVisible(_button("usepreevent"));
	//
	_assertVisible(_fieldset("address-after"));
	//address drop down
	_assertVisible(_label("Select from Saved Addresses", _in(_fieldset("address-after"))));
	_assertVisible(_select("dwfrm_giftregistry_eventaddress_addressAfterList"));
	//fname
	_assertVisible(_span("First Name", _in(_fieldset("address-after"))));
	_assertVisible(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_firstname"));
	//lname
	_assertVisible(_span("Last Name", _in(_fieldset("address-after"))));
	_assertVisible(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_lastname"));
	//address 1
	_assertVisible(_span("Address 1", _in(_fieldset("address-after"))));
	_assertVisible(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_address1"));
	_assertVisible(_div($Generic[4][8], _in(_fieldset("address-after"))));
	//tooltip
	_assertVisible(_link("tooltip", _in(_fieldset("address-after"))));
	//address 2
	_assertVisible(_span("Address 2", _in(_fieldset("address-after"))));
	_assertVisible(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_address2"));
	_assertVisible(_div($Generic[5][8], _in(_fieldset("address-after"))));
	//Country
	_assertVisible(_span("Country", _in(_fieldset("address-after"))));
	_assertVisible(_select("dwfrm_giftregistry_eventaddress_addressAfterEvent_country"));
	//state
	_assertVisible(_span("State", _in(_fieldset("address-after"))));
	_assertVisible(_select("dwfrm_giftregistry_eventaddress_addressAfterEvent_states_state"));
	//city
	_assertVisible(_span("City", _in(_fieldset("address-after"))));
	_assertVisible(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_city"));
	//zipcode
	_assertVisible(_span("Zip Code", _in(_fieldset("address-after"))));
	_assertVisible(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_postal"));
	//phone
	_assertVisible(_span("Phone", _in(_fieldset("address-after"))));
	_assertVisible(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_phone"));
	_assertVisible(_div($Generic[6][8], _in(_fieldset("address-after"))));
	//tool tip
	_assertVisible(_link("tooltip", _in(_fieldset("address-after"))));
	//previous
	_assertVisible(_submit("dwfrm_giftregistry_eventaddress_back"));
	//continue
	_assertVisible(_submit("Continue"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124336", "Verify the breadcrumb functionality of Pre/Post Event Shipping page  in application as a  Registered user.");
$t.start();
try
{
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
		}
	//Validating navigation on click of Home
	_click(_link("/(.*)/", _in(_heading1("primary-logo"))));
	
	_click(_link("My Account"));
	_click(_heading2("Gift Registries"));
	//click on new registry
	_click(_submit("dwfrm_giftregistry_create"));
	
	//creating the registry
	createRegistry($GiftRegistry, 0);
	
	//Navigation related to My account
	_assertVisible(_link("My Account", _in(_div("breadcrumb"))));
	_click(_link("My Account", _in(_div("breadcrumb"))));
	_assertVisible(_heading1("/My Account /"));
	if(_isIE9())
		{
		_assertEqual($Generic[7][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[7][0], _getText(_div("breadcrumb")));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124338", "Verify the navigation related to left nav links on  Pre/Post Event Shipping page in application as a Registered user. ");
$t.start();
try
{
	_click(_link("user-account"));
	_click(_link("Gift Registry"));
	//click on new registry
	_click(_submit("dwfrm_giftregistry_create"));
	//creating the registry
	createRegistry($GiftRegistry, 0);
	//left nav links	
	var $links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
	for(var $i=0;$i<$links;$i++)
	{
		//navigate to create gift registry page
		_click(_link("user-account"));
		_click(_link("Gift Registry"));
		//click on new registry
		_click(_submit("dwfrm_giftregistry_create"));
		//click on each links 
		_click(_link($Generic[$i][2]));
		//verify the navigation
		_assertVisible(_heading1("/"+$Generic[$i][4]+"/"));
		if($i==9 || $i==10)
			{
			_assertContainsText($Generic[$i][5], _div("breadcrumb"));
			}
		else
			{
			_assertContainsText($Generic[$i][5], _div("breadcrumb"));
			_assertVisible(_div("breadcrumb"));
			_assertVisible(_link($Generic[$i][5], _in(_div("breadcrumb"))));
			}
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148755", "Verify the functionality related to  down arrow present in left Nav on  Pre/Post Event Shipping page in application a Registered user. ");
$t.start();
try
{
	_click(_link("user-account"));
	_click(_link("Gift Registry"));
	//click on new registry
	_click(_submit("dwfrm_giftregistry_create"));
	//creating the registry
	createRegistry($GiftRegistry, 0);
	
/*	var $m=1;
	//fetching the no.of headings in content assert
	var $heading=_collectAttributes("_span","/toggle/","sahiText",_in(_div("secondary-navigation")));
	for(var $i=0;$i<$heading.length;$i++)
		{
		//fetch links present in respective toggle's
		if($i==$heading.length-1)
			{
				$g=$m-1;
				var $links=_collectAttributes("_listItem","/(.*)/","sahiText",_under(_span("toggle["+$g+"]")));
				_click(_span($heading[$i]));
				for(var $k=0;$k<$links.length;$k++)
					{
					if($links[$k]!="")
					{
						_assertNotVisible(_listItem($links[$k]));
					}
					}
			}
		else
			{
				var $links=_collectAttributes("_listItem","/(.*)/","sahiText",_above(_span("toggle["+$m+"]")));
			}
		_click(_span($heading[$i]));
		for(var $k=2;$k<$links.length;$k++)
			{
			if($links[$k]!="")
			{
				_assertNotVisible(_listItem($links[$k]));
			}
			}
		$m++;
		 }*/
	
	
	
	var $heading=_collectAttributes("_span","/toggle/","sahiText",_in(_div("secondary-navigation")));
	var $links=_collectAttributes("_listItem","/(.*)/","sahiText", _in(_div("secondary-navigation")));
	
	for(var $i=0;$i<$links.length;$i++)
	{
		_assertVisible(_listItem($links[$i]));
	}
		
	for(var $i=0;$i<$heading.length;$i++)
	{
	_assertVisible(_span($heading[$i], _in(_div("content-asset"))));
	_click(_span($heading[$i]));
	}
	
	for(var $i=0;$i<$links.length;$i++)
	{
		_assertNotVisible(_listItem($links[$i]));
	}
	
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124340", "Verify the navigation related to the 'Previous' link on  Pre/Post Event Shipping page in application as a  Registered user.");
$t.start();
try
{
	_assertVisible(_submit("dwfrm_giftregistry_eventaddress_back"));
	_click(_submit("dwfrm_giftregistry_eventaddress_back"));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
		}	
	//Navigate to event shipping info page
	_click(_submit("Continue"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124337/124341/124342/148756/148757/148757/148757/148758/148759/148760/148761/148762/148763/148765/148766/148767/148768/148769/148770/148771", "Verify validation related to Firstname/Lastname/Address1/Address2/Country/States/City/Phone fields  present in Pre/Post Event Shipping  section on  Pre/Post Event Shipping page in application as a  Registered user.");
$t.start();
try
{
	
	_click(_link("user-account"));
	_click(_link("Gift Registry"));
	//click on new registry
	_click(_submit("dwfrm_giftregistry_create"));
	//creating the registry
	createRegistry($GiftRegistry, 0);
	//country validations
	var $Length=_getText(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_country")).length;
	for(var $j=0;$j<$Length-1;$j++)
		{
		_log($j);
		_setSelected(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_country"),$EventShippingInfo[$j][11]);
		_setSelected(_select("dwfrm_giftregistry_eventaddress_addressAfterEvent_country"),$EventShippingInfo[$j][11]);
		_assertEqual($EventShippingInfo[$j][12], _getText(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_states_state")).toString());
		_assertEqual($EventShippingInfo[$j][12], _getText(_select("dwfrm_giftregistry_eventaddress_addressAfterEvent_states_state")).toString());
		}
	
	for(var $i=0;$i<$EventShippingInfo.length;$i++)
		{
		if($i!=3 || $i!=4)
			{
		_log($i);
			_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_firstname"),$EventShippingInfo[$i][1]);
			_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_lastname"),$EventShippingInfo[$i][2]);
			_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address1"),$EventShippingInfo[$i][3]);
			_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address2"),$EventShippingInfo[$i][4]);
			_setSelected(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_country"),$EventShippingInfo[$i][5]);
			_setSelected(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_states_state"),$EventShippingInfo[$i][6]);
			_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_city"),$EventShippingInfo[$i][7]);
			_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_postal"),$EventShippingInfo[$i][8]);
			_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_phone"),$EventShippingInfo[$i][9]);
			_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_firstname"),$EventShippingInfo[$i][1]);
			_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_lastname"),$EventShippingInfo[$i][2]);
			_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_address1"),$EventShippingInfo[$i][3]);
			_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_address2"),$EventShippingInfo[$i][4]);
			_setSelected(_select("dwfrm_giftregistry_eventaddress_addressAfterEvent_country"),$EventShippingInfo[$i][5]);
			_setSelected(_select("dwfrm_giftregistry_eventaddress_addressAfterEvent_states_state"),$EventShippingInfo[$i][6]);
			_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_city"),$EventShippingInfo[$i][7]);
			_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_postal"),$EventShippingInfo[$i][8]);
			_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_phone"),$EventShippingInfo[$i][9]);
			if($i!=1)
				{
				_click(_submit("Continue"));
				}
			//blank field validation
			if($i==0)
				{
					_assertEqual($color, _style(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_firstname"), "background-color"));
					_assertEqual($color, _style(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_lastname"), "background-color"));
					_assertEqual($color, _style(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address1"), "background-color"));
					_assertEqual($color, _style(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_states_state"), "background-color"));
					_assertEqual($color, _style(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_city"), "background-color"));
					_assertEqual($color, _style(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_postal"), "background-color"));
					_assertEqual($color, _style(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_phone"), "background-color"));
					_assertEqual($color, _style(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_firstname"), "background-color"));
					_assertEqual($color, _style(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_lastname"), "background-color"));
					_assertEqual($color, _style(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_address1"), "background-color"));
					_assertEqual($color, _style(_select("dwfrm_giftregistry_eventaddress_addressAfterEvent_states_state"), "background-color"));
					_assertEqual($color, _style(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_city"), "background-color"));
					_assertEqual($color, _style(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_postal"), "background-color"));
					_assertEqual($color, _style(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_phone"), "background-color"));	
				}
			//Max characters
			else if($i==1)
				{
					_assertEqual($EventShippingInfo[0][13],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_firstname")).length);
					_assertEqual($EventShippingInfo[0][13],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_lastname")).length);
					_assertEqual($EventShippingInfo[0][13],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address1")).length);
					_assertEqual($EventShippingInfo[0][13],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address2")).length);
					_assertEqual($EventShippingInfo[1][13],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_city")).length);
					_assertEqual($EventShippingInfo[3][13],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_postal")).length);
					_assertEqual($EventShippingInfo[2][13],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_phone")).length);
					_assertEqual($EventShippingInfo[0][13],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_firstname")).length);
					_assertEqual($EventShippingInfo[0][13],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_lastname")).length);
					_assertEqual($EventShippingInfo[0][13],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_address1")).length);
					_assertEqual($EventShippingInfo[0][13],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_address2")).length);
					_assertEqual($EventShippingInfo[1][13],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_city")).length);
					_assertEqual($EventShippingInfo[3][13],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_postal")).length);
					_assertEqual($EventShippingInfo[2][13],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_phone")).length);				
				}
			//spl characters in zip code
			else if($i==4)
				{
				_assertEqual($EventShippingInfo[5][10],_style(_span("Zip Code"),"color"));
				}
			//valid
			else if($i==5)
				{
				_assertVisible(_heading2("Event Information"));
				_assertVisible(_heading3("Shipping Address(es)"));
				}
			//spl characters and invalid phone number
			else 
				{
				_assertVisible(_span($EventShippingInfo[$i][10]));
				_assertEqual($color, _style(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_phone"), "background-color"));
				_assertVisible(_span($EventShippingInfo[$i][10], _in(_fieldset("address-after"))));
				_assertEqual($color, _style(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_phone"), "background-color"));
				}
			}
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124344", "Verify the UI  of Event information page  in application as a  Registered user.");
$t.start();
try
{	
	/*if (_isVisible(_submit("dwfrm_giftregistry_eventaddress_back")))
		{
	_click(_submit("dwfrm_giftregistry_eventaddress_back"));
		}*/
	Pre_PostEventInfo($GiftRegistry, 4);	
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
		}
	//heading
	_assertVisible(_heading2($Generic[0][9]));
	//Static text
	_assertVisible(_paragraph($Generic[1][9]));
	//section header
	_assertVisible(_heading3($Generic[2][9]));
	//Event info
	_assertVisible(_dList("Registrant(s):"+" "+$FirstName1+" "+$LastName1+" , "+$GiftRegistry[0][12]+" "+$GiftRegistry[0][13]+" "+"Event Type:"+" "+$GiftRegistry[0][1]+" "+"Event Name:"+" "+$GiftRegistry[0][2]+" "+"Event Date:"+" "+$GiftRegistry[0][3]+" "+"Event Location:"+" "+$GiftRegistry[0][6]+", "+$GiftRegistry[1][5]+" "+$GiftRegistry[1][4]));	
	//section header
	_assertVisible(_heading3($Generic[3][9]));
	//Participants info
	_assertVisible(_dList("Registrant: "+$FirstName1+" "+$LastName1+" "+$UserEmail+" Co-Registrant: "+$GiftRegistry[0][12]+" "+$GiftRegistry[0][13]+" "+$GiftRegistry[0][14]));
	//Shipping info
	//section header
	_assertVisible(_heading3($Generic[4][9]));
	//Pre/Post Event shipping info
	_assertVisible(_dList("Pre-event Shipping: "+$GiftRegistry[4][1]+" "+$GiftRegistry[4][2]+" "+$GiftRegistry[4][3]+" "+$GiftRegistry[4][4]+" "+$GiftRegistry[4][7]+", "+$GiftRegistry[5][6]+" "+$GiftRegistry[4][8]+" Post-event Shipping: "+$GiftRegistry[4][1]+" "+$GiftRegistry[4][2]+" "+$GiftRegistry[4][3]+" "+$GiftRegistry[4][4]+" "+$GiftRegistry[4][7]+", "+$GiftRegistry[5][6]+" "+$GiftRegistry[4][8]));
	//Parevious button
	_assertVisible(_submit("dwfrm_giftregistry_eventaddress_back"));
	//Submit button
	_assertVisible(_submit("Submit"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124343", "Verify the breadcrumb Navigation of Event information page  in application as a  Registered user.");
$t.start();
try
{
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
		}
	_click(_link("user-account"));
	_click(_link("Gift Registry"));
	//click on new registry
	_click(_submit("dwfrm_giftregistry_create"));
	//creating the registry
	createRegistry($GiftRegistry, 0);
	//Pre/Post Event shipping info
	Pre_PostEventInfo($GiftRegistry, 4);
	//Navigation related to My account
	_assertVisible(_link("My Account", _in(_div("breadcrumb"))));
	_click(_link("My Account", _in(_div("breadcrumb"))));
	_assertVisible(_heading1("/My Account /"));
	if(_isIE9())
		{
		_assertEqual($Generic[7][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[7][0], _getText(_div("breadcrumb")));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124345", "Verify the navigation related to the 'Previous' link on Event information page  in application as a  Registered user.");
$t.start();
try
{
	_click(_link("user-account"));
	_click(_link("Gift Registry"));
	//click on new registry
	_click(_submit("dwfrm_giftregistry_create"));
	//creating the registry
	createRegistry($GiftRegistry, 0);
	//Pre/Post Event shipping info
	Pre_PostEventInfo($GiftRegistry, 4);
	//Parevious button
	_assertVisible(_submit("dwfrm_giftregistry_eventaddress_back"));
	_click(_submit("dwfrm_giftregistry_eventaddress_back"));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
		}
	//heading
	_assertVisible(_heading1($Generic[0][8]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//Navigate to Event shipping info Page
_click(_link("user-account"));
_click(_link("Gift Registry"));
//click on new registry
_click(_submit("dwfrm_giftregistry_create"));
//creating the registry
createRegistry($GiftRegistry, 0);
//bread crumb
_assertVisible(_div("breadcrumb"));
if(_isIE9())
{
_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
}
else
{
_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
}

var $t = _testcase("148754/148764", "Verify functionality related to Select from Saved Addresses drop down present in Pre-Event Shipping and post-Event Shipping section on  Pre/Post Event Shipping page in application as a  Registered user.");
$t.start();
try
{
	_setSelected(_select("dwfrm_giftregistry_eventaddress_addressBeforeList"),$Address[0][11]);
	//Validating Prepopulated data
	_assertEqual($Address[0][2],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_firstname")));
	_assertEqual($Address[0][3],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_lastname")));
	_assertEqual($Address[0][4],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address1")));
	_assertEqual($Address[0][5],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address2")));
	_assertEqual($Address[0][6],_getSelectedText(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_country")));
	_assertEqual($Address[0][7],_getSelectedText(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_states_state")));
	_assertEqual($Address[0][8],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_city")));
	_assertEqual($Address[0][9],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_postal")));
	_assertEqual($Address[0][10],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_phone")));
	
	_setSelected(_select("dwfrm_giftregistry_eventaddress_addressAfterList"),$Address[1][11]);
	//Validating Prepopulated data
	_assertEqual($Address[1][2],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_firstname")));
	_assertEqual($Address[1][3],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_lastname")));
	_assertEqual($Address[1][4],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_address1")));
	_assertEqual($Address[1][5],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_address2")));
	_assertEqual($Address[1][6],_getSelectedText(_select("dwfrm_giftregistry_eventaddress_addressAfterEvent_country")));
	_assertEqual($Address[1][7],_getSelectedText(_select("dwfrm_giftregistry_eventaddress_addressAfterEvent_states_state")));
	_assertEqual($Address[1][8],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_city")));
	_assertEqual($Address[1][9],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_postal")));
	_assertEqual($Address[1][10],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_phone")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//Navigate to Event shipping info Page
_click(_link("user-account"));
_click(_link("Gift Registry"));
//click on new registry
_click(_submit("dwfrm_giftregistry_create"));
//creating the registry
createRegistry($GiftRegistry, 0);
//bread crumb
_assertVisible(_div("breadcrumb"));
if(_isIE9())
{
_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
}
else
{
_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
}

var $t = _testcase("124339", "Verify the  functionality related to 'Use this address for Post Event Shipping' check box   Pre-Event Shipping  section on  Pre/Post Event Shipping page in application as a  Registered user.");
$t.start();
try
{
	_setSelected(_select("dwfrm_giftregistry_eventaddress_addressBeforeList"),$Address[0][11]);
	//Validating Prepopulated data
	_assertEqual($Address[0][2],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_firstname")));
	_assertEqual($Address[0][3],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_lastname")));
	_assertEqual($Address[0][4],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address1")));
	_assertEqual($Address[0][5],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address2")));
	_assertEqual($Address[0][6],_getSelectedText(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_country")));
	_assertEqual($Address[0][7],_getSelectedText(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_states_state")));
	_assertEqual($Address[0][8],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_city")));
	_assertEqual($Address[0][9],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_postal")));
	_assertEqual($Address[0][10],_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_phone")));
	//selecting use this address checkbox
	_click(_button("usepreevent"));
	//Validating Prepopulated data
	_assertEqual(_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_firstname")),_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_firstname")));
	_assertEqual(_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_lastname")),_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_lastname")));
	_assertEqual(_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address1")),_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_address1")));
	_assertEqual(_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address2")),_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_address2")));
	_assertEqual(_getSelectedText(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_country")),_getSelectedText(_select("dwfrm_giftregistry_eventaddress_addressAfterEvent_country")));
	_assertEqual(_getSelectedText(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_states_state")),_getSelectedText(_select("dwfrm_giftregistry_eventaddress_addressAfterEvent_states_state")));
	_assertEqual(_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_city")),_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_city")));
	_assertEqual(_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_postal")),_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_postal")));
	_assertEqual(_getText(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_phone")),_getText(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_phone")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("148773", "Verify the functionality related to  down arrow present in left Nav on Event information page  in application as a  Registered user.");
$t.start();
try
{
	
/*	var $n=1;
	//fetching the no.of headings in content assert
	var $heading1=_collectAttributes("_span","/toggle/","sahiText",_in(_div("secondary-navigation")));
	for(var $i=0;$i<$heading1.length;$i++)
	{
	//fetch links present in respective toggle's
	if($i==$heading1.length-1)
		{
		$g=$n-1;
		var $links=_collectAttributes("_listItem","/(.*)/","sahiText",_under(_span("toggle["+$g+"]")));
		_click(_span($heading1[$i]));
		for(var $k=0;$k<$links.length;$k++)
			{
				if($links[$k]!="")
				{
					_assertNotVisible(_listItem($links[$k]));
				}
			}
		}
	else
		{
		var $links=_collectAttributes("_listItem","/(.*)/","sahiText",_above(_span("toggle["+$n+"]")));
		}
		_click(_span($heading1[$i]));
		for(var $k=2;$k<$links.length;$k++)
			{
				if($links[$k]!="")
				{
					_assertNotVisible(_listItem($links[$k]));
				}
			}
		$n++;
	}*/
	
	
	
	var $heading=_collectAttributes("_span","/toggle/","sahiText",_in(_div("secondary-navigation")));
	var $links=_collectAttributes("_listItem","/(.*)/","sahiText", _in(_div("secondary-navigation")));
	
	for(var $i=0;$i<$links.length;$i++)
	{
		_assertVisible(_listItem($links[$i]));
	}
		
	for(var $i=0;$i<$heading.length;$i++)
	{
	_assertVisible(_span($heading[$i], _in(_div("content-asset"))));
	_click(_span($heading[$i]));
	}
	
	for(var $i=0;$i<$links.length;$i++)
	{
		_assertNotVisible(_listItem($links[$i]));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("148772", "Verify the navigation related to left nav links on Event information page  in application as a  Registered user.");
$t.start();
try
{
	//left nav links	
	var $links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
	for(var $i=0;$i<$links;$i++)
	{
		//navigate to create gift registry page
		_click(_link("My Account"));
		_click(_heading2("Gift Registries"));
		//click on new registry
		_click(_submit("dwfrm_giftregistry_create"));
		//creating the registry
		createRegistry($GiftRegistry, 0);
		//Pre/Post Event shipping info
		Pre_PostEventInfo($GiftRegistry, 4);
		//click on each links in event info page
		_click(_link($Generic[$i][2]))
		//verify the navigation
		_assertVisible(_heading1("/"+$Generic[$i][4]+"/"));
		_assertContainsText($Generic[$i][5], _div("breadcrumb"));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124346", "Verify the navigation related to the 'Submit' link on Event information page  in application as a  Registered user.");
$t.start();
try
{
	//navigate to create gift registry page
	_click(_link("user-account"));
	_click(_link("Gift Registry"));
	//click on new registry
	_click(_submit("dwfrm_giftregistry_create"));
	//creating the registry
	createRegistry($GiftRegistry, 0);
	//Pre/Post Event shipping info
	Pre_PostEventInfo($GiftRegistry, 4);
	//Click on submit
	//Submit button
	_click(_submit("dwfrm_giftregistry_event_confirm"));
	//Validating Navigation
	_assertVisible(_heading1($Generic[0][10]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


cleanup();