_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("GiftRegistry_Data.xls");

var $Giftregistry_guest=_readExcelFile("GiftRegistry_Data.xls","Giftregistry_guest");
var $Validations=_readExcelFile("GiftRegistry_Data.xls","Validations");
var $Login_Validation=_readExcelFile("GiftRegistry_Data.xls","Login_Validations");

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("124303/124308", "Verify 'GIFT REGISTRY' link at the Header for Guest User, Verify the UI of 'Returning customers' section Gift Registry login page in application as an Anonymous user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click(_link("Gift Registry"));
	//verify bread crumb
	_assertVisible(_div("breadcrumb"));
	_assertVisible(_link($Giftregistry_guest[2][1], _in(_div("breadcrumb"))));
	//Verifying the UI of returning customer section
	//MyAccount login as heading
	_assertVisible(_heading1($Giftregistry_guest[3][0]));
	//Returning customer heading
	_assertVisible(_heading2($Giftregistry_guest[4][0]));
	//Paragraph with static text
	_assertVisible(_paragraph("/(.*)/",_in(_div("login-box-content returning-customers clearfix"))));
	//email Address
	_assertVisible(_label($Giftregistry_guest[6][0]));
	_assertVisible(_textbox("/dwfrm_login_username/"));
	_assertEqual("", _getValue(_textbox("/dwfrm_login_username/")));
	//password
	_assertVisible(_label($Giftregistry_guest[7][0]));
	_assertVisible(_password("/dwfrm_login_password/"));
	_assertEqual("", _getValue(_password("/dwfrm_login_password/")));
	//remember me
	_assertVisible(_label("Remember Me"));
	_assertVisible(_checkbox("dwfrm_login_rememberme"));
	_assertNotTrue(_checkbox("dwfrm_login_rememberme").checked);
	//login button
	_assertVisible(_submit("Login"));
	//forgot password
	_assertVisible(_link("password-reset"));
	_assertEqual($Giftregistry_guest[8][0], _getText(_link("password-reset")));
	//social links
	//_assertVisible(_paragraph($Giftregistry_guest[10][0]));
	_assertVisible(_div("login-box-content returning-customers clearfix"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("124309","Verify the UI of ' New Customer's' section Gift Registry login page in application as an  Anonymous user");
$t.start();
try
{
	//verify the UI of New Customer section
	_assertVisible(_heading2($Giftregistry_guest[0][4]));
	_assertVisible(_div("content-asset"));
	_assertVisible(_submit("dwfrm_login_register"));
	_assertVisible(_paragraph("/(.*)/",_in(_div("login-box login-create-account clearfix"))));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124310","Verify the UI of 'FIND SOMEONE'S GIFT REGISTRY section Gift Registry login page in application as an Anonymous user");
$t.start();
try
{
	//verify the UI of 'FIND SOMEONE'S GIFT REGISTRY
	_assertVisible(_heading2($Giftregistry_guest[1][4]));
	_assertVisible(_paragraph("/(.*)/",_in(_div("login-box-content"))));
	//last name
	_assertVisible(_span($Giftregistry_guest[2][4]));
	_assertVisible(_textbox("dwfrm_giftregistry_search_simple_registrantLastName"));
	//first name
	_assertVisible(_span($Giftregistry_guest[3][4]));
	_assertVisible(_textbox("dwfrm_giftregistry_search_simple_registrantFirstName"));
	//email 
	_assertVisible(_label($Giftregistry_guest[4][4]));
	_assertVisible(_textbox("dwfrm_giftregistry_search_simple_eventType"));
	//find button
	_assertVisible(_submit("dwfrm_giftregistry_search_search"));
	//advanced search
	_assertVisible(_link($Giftregistry_guest[1][5]));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("----","Verify the UI of 'Gift Registry login page' in application as an Anonymous user");
$t.start();
try
{
	_assertVisible(_div("breadcrumb"));	
	_assertVisible(_link($Giftregistry_guest[2][1], _in(_div("breadcrumb"))));		
	_assert(_isVisible(_heading1($Giftregistry_guest[3][0])));
	//left nav section
	_assertVisible(_span($Giftregistry_guest[5][4]));
	_assertVisible(_link($Giftregistry_guest[2][3]));
	_assertVisible(_span($Giftregistry_guest[6][4]));
	_assertVisible(_link($Giftregistry_guest[7][4]));
	_assertVisible(_link($Giftregistry_guest[8][4]));
	//need help section
	_assertVisible(_heading2($Giftregistry_guest[9][4]));
	_assertVisible(_div("content-asset",_in(_div("account-nav-asset"))));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124304","Verify the navigation related to 'Home' and 'My account' links in Gift Registry login page bread crumb in application as an Anonymous user");
$t.start();
try
{
	//click on home link present in the bread crumb
	//_click(_link("Home",_in(_div("breadcrumb"))));
	
	//verify the navigation to home page
	_click(_link("Demandware SiteGenesis"));
	
	_assertVisible(_list("homepage-slides"));
	_assertVisible(_div("home-bottom-slots"));
	//navigating to Gift Registry page
	_click(_link("Gift Registry"));
	//click on my account link present in the bread crumb
	_click(_link($Giftregistry_guest[1][1],_in(_div("breadcrumb"))));
	//verify the navigation
	_assertVisible(_div("breadcrumb"));	
	_assertVisible(_link($Giftregistry_guest[1][1], _in(_div("breadcrumb"))));
	_assertVisible(_heading1($Giftregistry_guest[11][0]));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124305","Verify the navigation related to 'Create an Account' link on Gift Registry login page left nav in application as an  Anonymous user");
$t.start();
try
{
	//click on link create an account
	_click(_submit("dwfrm_login_register"));
	//verify the navigation
	_assertVisible(_heading1($Giftregistry_guest[0][3]));
	_assertVisible(_div("breadcrumb"));	
	_assertVisible(_link($Giftregistry_guest[1][3], _in(_div("breadcrumb"))));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("125423","Verify the navigation related to 'Privacy Policy' link  on Gift Registry login page left nav in  application as an Anonymous user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click(_link("Gift Registry"));
	//click on privacy policy link
	_click(_link($Giftregistry_guest[4][1]));
	//verify the navigation
	_assertVisible(_heading1($Giftregistry_guest[4][1]));
	_assertVisible(_div("content-asset"));
	_assertContainsText("/"+$Giftregistry_guest[4][1]+"/", _div("breadcrumb"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148734","Verify the navigation related to 'Secure Shopping' link on Gift Registry login page left nav in  application as an  Anonymous user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click(_link("Gift Registry"));
	//click on Secure Shopping link	
	_click(_link($Giftregistry_guest[8][4]));
	//verify the navigation
	_assertContainsText("/"+$Giftregistry_guest[5][1]+"/", _div("breadcrumb"));
	_assertVisible(_heading1($Giftregistry_guest[5][1]));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148735","Verify the navigation related to 'Contact Us' link  on Gift Registry login page left nav in  application as an  Anonymous user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click(_link("Gift Registry"));
	//click on contact us link
	_click(_link($Giftregistry_guest[6][1]));
	//verify the navigation
	_assertVisible(_heading1("/"+$Giftregistry_guest[6][1]+"/"));
	_assertVisible(_div("breadcrumb"));	
	_assertVisible(_link($Giftregistry_guest[9][1], _in(_div("breadcrumb"))));
	_assertVisible(_submit("dwfrm_contactus_send"));
		
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148736","Verify the UI of 'Forgot Password' overlay on Gift Registry login page in application as an Anonymous user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click(_link("Gift Registry"));
	//click on forgot password link
	_click(_link("password-reset"));
	//verify the overlay appearance
	_assertVisible(_span("ui-dialog-title"));
	_assertVisible(_heading1($Giftregistry_guest[7][1]));
	_assertVisible(_textbox("dwfrm_requestpassword_email"));
	_assertVisible(_submit("dwfrm_requestpassword_send"));
	_assertVisible(_span("CLOSE"));
	//close the overlay
	_click(_span("CLOSE"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124313","Verify the functionality �Send� button related to Forgot password overlay on Gift Registry login page returning customer section in application as an Anonymous user");
$t.start();
try
{
	_click(_link("password-reset"));
	//entering valid email address
	_setValue(_textbox("dwfrm_requestpassword_email"),$Giftregistry_guest[0][2]);	
	//click on send button
	_click(_submit("dwfrm_requestpassword_send"));
	//verify the navigation
	_assertVisible(_heading1($Giftregistry_guest[10][1]));
	_assertVisible(_link("Go to the home page"));
	//close the dialog box
	_click(_button("Close"));	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124314","Verify the navigation related to 'Create an account now' button Gift Registry login page create a wishlist section in application as an Anonymous user");
$t.start();
try
{
	//navigating to Gift Registry page
	_click(_link("Gift Registry"));
	//click on link Create an account now button
	_click(_submit("dwfrm_login_register"));
	//verify the navigation
	_assertVisible(_heading1($Giftregistry_guest[0][3]));
	_assertVisible(_div("breadcrumb"));	
	_assertVisible(_link($Giftregistry_guest[1][3], _in(_div("breadcrumb"))));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("124311/148738/124312","Verify validation related to the 'Email address'/'password' field's and login button on Gift Registry login page, Returning customers section in application as a Anonymous user");
$t.start();
try
{
	_click(_link("Gift Registry"));
	//validating the email address and password fields	
	for(var $i=0;$i<$Login_Validation.length;$i++)
		{
			_log($i);
			//valid
			if($i==5)
				{
				login();
				}	
			else
				{
				_setValue(_textbox("/dwfrm_login_username/"),$Login_Validation[$i][1]);
				_setValue(_password("/dwfrm_login_password/"),$Login_Validation[$i][2]);
				_click(_submit("dwfrm_login_login"));
				}
			
			if (isMobile())
				{
				_wait(2000);
				}
			//blank
			if($i==0)
				{
				//username
				_assertEqual($Login_Validation[$i][5],_style(_textbox("/dwfrm_login_username/"),"background-color"));
				_assertVisible(_span($Login_Validation[$i][3]));
				//password
				_assertVisible(_span($Login_Validation[$i][4]));
				_assertEqual($Login_Validation[$i][5],_style(_password("/dwfrm_login_password/"),"background-color"));
				}
			//valid
			else if($i==5)
				{
				_assertVisible(_heading1("/"+$Giftregistry_guest[8][1]+"/"));
				}
			//invalid 
			else
				{
				_assertVisible(_div($Login_Validation[1][3]));
				}
		}
	//logout from application
	_click(_link("My Account"));
	_click(_link("Logout"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148739/148740/124316","Verify validation related to the 'Last name'/'First name' field and find button functionality on Gift Registry login page, find someone's Gift Registry section in application as a Anonymous user");
$t.start();
try
{
//navigating to Gift Registry page
_click(_link("Gift Registry"));
//validating the fields last name,first name,email 
for(var $i=0;$i<$Validations.length;$i++)
  {
	_setValue(_textbox("dwfrm_giftregistry_search_simple_registrantLastName"), $Validations[$i][1]);
	_setValue(_textbox("dwfrm_giftregistry_search_simple_registrantFirstName"), $Validations[$i][2]);
	
	//click on find button
	_click(_submit("dwfrm_giftregistry_search_search"));
	//Blank
	if($i==0)
		{
			//_setSelected(_select("dwfrm_productlists_search_eventType"), $Validations[$i][3]);
			//last name,first name fields should highlight
			_assertEqual($Validations[0][4],_style(_textbox("dwfrm_giftregistry_search_simple_registrantLastName"),"background-color"));
			_assertEqual($Validations[0][4],_style(_textbox("dwfrm_giftregistry_search_simple_registrantFirstName"),"background-color"));
		}
	//invalid
	if($i==2 || $i==3 || $i==4 || $i==5 || $i==6)
		{
			_setValue(_textbox("dwfrm_giftregistry_search_simple_eventType"), $Validations[$i][3]);
			//user should be in same page
			_assertVisible(_heading1("/"+$Giftregistry_guest[8][1]+"/"));
			//verify the text message
			if(i==6)
				{
				_setValue(_textbox("dwfrm_giftregistry_search_simple_eventType"), $Validations[$i][3]);
				_assertVisible(_table("registry-results-table item-list"));
				_assertVisible(_link("View"));
				}
			else
				{
				
				_assertVisible(_paragraph("/No Gift Registry has been found for/"));
				}
		}
	//max characters
	if($i==1)
		{
		_assertEqual($Validations[1][4],_getText(_textbox("dwfrm_giftregistry_search_simple_registrantLastName")).length);
		_assertEqual($Validations[1][4],_getText(_textbox("dwfrm_giftregistry_search_simple_registrantFirstName")).length);		
		}
	}	
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("--","Verify advanced search functionality on Gift Registry login page, find someone's Gift Registry section in application as a Anonymous user");
$t.start();
try
{
 	//navigating to Gift Registry page
	_click(_link("Gift Registry"));	
	//click on advanced search button
	_click(_link("Advanced Search"));
	//verify the UI
	//event name
	_assertVisible(_span($Giftregistry_guest[0][9]));
	_assertVisible(_textbox("dwfrm_giftregistry_search_advanced_eventName"));
	//event month
	_assertVisible(_span($Giftregistry_guest[1][9]));
	_assertVisible(_select("dwfrm_giftregistry_search_advanced_eventMonth"));
	//event year
	_assertVisible(_span($Giftregistry_guest[2][9]));
	_assertVisible(_select("dwfrm_giftregistry_search_advanced_eventYear"));
	//event city
	_assertVisible(_span($Giftregistry_guest[3][9]));
	_assertVisible(_textbox("dwfrm_giftregistry_search_advanced_eventCity"));
	//event state
	_assertVisible(_span($Giftregistry_guest[4][9]));
	_assertVisible(_select("dwfrm_giftregistry_search_advanced_eventAddress_states_state"));
	//event country
	_assertVisible(_span($Giftregistry_guest[5][9]));
	_assertVisible(_select("dwfrm_giftregistry_search_advanced_eventAddress_country"));
}
catch($e)
{      
    _logExceptionAsFailure($e);
}
$t.end();

cleanup();