_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("GiftRegistry_Data.xls");
var $Generic=_readExcelFile("GiftRegistry_Data.xls","Generic");
var $GiftRegistry=_readExcelFile("GiftRegistry_Data.xls","GiftRegistry");
var $sendtofriend=_readExcelFile("GiftRegistry_Data.xls","Sendtofriend_Validation");
var $Advance=_readExcelFile("GiftRegistry_Data.xls","Advance_Search");
var $color=$Generic[0][6];
var $color1=$Generic[1][6];
//from util.GenericLibrary/BrowserSpecific.js
var $FirstName1=$FName;
var $LastName1=$LName;
var $UserEmail=$uId;


SiteURLs()
cleanup();
_setAccessorIgnoreCase(true); 
//Login
_click(_link("user-account"));
_click(_link("Login"));
login();
//Deleting addresses, which intern will delete wishlist and gift registry
deleteAddress();

var $t = _testcase("124348", "Verify the UI  on Gift registery page in application as a  Registered user.");
$t.start();
try
{
	_click(_link("My Account"));
	_click(_link("Gift Registry"));
	//click on new registry
	_click(_submit("dwfrm_giftregistry_create"));
	//creating the registry
	createRegistry($GiftRegistry, 0);
	//Pre/Post Event shipping info
	Pre_PostEventInfo($GiftRegistry, 4);
	//Submit button
	_click(_submit("Submit"));
	//Validating Navigation
	_assertVisible(_heading1($Generic[0][10]));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	
//	if(_isIE9())
//		{
//		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
//		}
//	else
//		{
//		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
//		}
	
	//Tabs
	_assertVisible(_div("page-content-tab-navigaton"));
	//registry
	_assertVisible(_submit("dwfrm_giftregistry_navigation_navRegistry"));
	_assertEqual($Generic[1][10], _getText(_submit("dwfrm_giftregistry_navigation_navRegistry")));
	//event info
	_assertVisible(_submit("dwfrm_giftregistry_navigation_navEvent"));
	_assertEqual($Generic[2][10], _getText(_submit("dwfrm_giftregistry_navigation_navEvent")));
	//shipping info
	_assertVisible(_submit("dwfrm_giftregistry_navigation_navShipping"));
	_assertEqual($Generic[3][10], _getText(_submit("dwfrm_giftregistry_navigation_navShipping")));
	//purchases
	_assertVisible(_submit("dwfrm_giftregistry_navigation_navPurchases"));
	_assertEqual($Generic[4][10], _getText(_submit("dwfrm_giftregistry_navigation_navPurchases")));
	//gift registry details
	_assertVisible(_div("page-content-tab-wrapper"));
	_assertVisible(_heading2($GiftRegistry[0][1]+" - "+$GiftRegistry[0][15]));
	_assertVisible(_paragraph($Generic[5][10]));
	//add items link
	_assertVisible(_link("Add Items"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124347", "Verify the navigation related to the 'Home' and  'My account' bread crumb on Gift registery page   in application as a  Registered user.");
$t.start();
try
{
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	
//	if(_isIE9())
//		{
//		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
//		}
//	else
//		{
//		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
//		}
	
	//Navigation related to home
	//_assertVisible(_link("Home", _in(_div("breadcrumb"))));
	//_click(_link("Home", _in(_div("breadcrumb"))));
	
	//verify the navigation to home page
	_click(_link("Demandware SiteGenesis"));
	
	_assertVisible(_list("homepage-slides"));
	_assertVisible(_div("home-bottom-slots"));
	
	_click(_link("user-account"));
	_click(_link("Gift Registry"));
	//click on new registry
	_click(_submit("dwfrm_giftregistry_create"));
	//creating the registry
	createRegistry($GiftRegistry, 0);
	//Pre/Post Event shipping info
	Pre_PostEventInfo($GiftRegistry, 4);
	//Submit button
	_click(_submit("Submit"));
	//Validating Navigation
	_assertVisible(_heading1($Generic[0][10]));
	//Navigation related to My account
	_assertVisible(_link("My Account", _in(_div("breadcrumb"))));
	_click(_link("My Account", _in(_div("breadcrumb"))));
	_assertVisible(_heading1("/My Account /"));
	if(_isIE9())
		{
		_assertEqual($Generic[7][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[7][0], _getText(_div("breadcrumb")));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("128722", "Verify creation of Gift Registry for a logged in User");
$t.start();
try
{
	//Deleting
	Delete_GiftRegistry();
	//creating the registry
	_click(_submit("dwfrm_giftregistry_create"));
	createRegistry($GiftRegistry, 0);
	//Pre/Post Event shipping info
	Pre_PostEventInfo($GiftRegistry, 4);
	_click(_submit("Submit"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124356", "Verify the functionality related to the 'Add Items' on Gift registery page, find someone's Gift registery list section in applicationas a  Registered user.");
$t.start();
try
{
	//verify the navigation to home page
	_click(_link("Demandware SiteGenesis"));
	
	_click(_link("Gift Registry"));
	//Click on view 
	_click(_link("View"));
	//adding items to gift registry
	addItems($Generic[0][11]);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124323", "Verify the functionality related to 'View' link in giftregistery page  in application as a  Registered user.");
$t.start();
try
{
	//verify the navigation to home page
	_click(_link("Demandware SiteGenesis"));

	_click(_link("Gift Registry"));
	//Click on view 
	_click(_link("View"));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	
//	if(_isIE9())
//		{
//		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
//		}
//	else
//		{
//		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
//		}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("124324", "Verify the UI  on Gift Registry page after clicking the �View� button in application as a  Registered user.");
$t.start();
try
{
	//Validating Navigation
	_assertVisible(_heading1($Generic[0][10]));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	
//	if(_isIE9())
//		{
//		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'')); 
//		}
//	else
//		{
//		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
//		}
//	
	//Tabs
	_assertVisible(_div("page-content-tab-navigaton"));
	_assertVisible(_submit("dwfrm_giftregistry_setPublic"));
	if(_isVisible(_submit("dwfrm_giftregistry_setPublic")))
		{
			_click(_submit("dwfrm_giftregistry_setPublic"));	
		}
	//make this registry private
	_assertVisible(_submit("dwfrm_giftregistry_setPrivate"));
	//verifying url
	if (!_isVisible(_submit("dwfrm_giftregistry_setPublic")))
		{
		_click(_link("Share Link"));
		_assertVisible(_link($Generic[1][16]));
		_assertVisible(_div($Generic[0][16]));
		}
	else
		{
		_click(_submit("dwfrm_giftregistry_setPublic"));
		_click(_link("Share Link"));
		_assertVisible(_link($Generic[1][16]));
		_assertVisible(_div($Generic[0][16]));
		}
	//heading
	_assertVisible(_heading2($GiftRegistry[0][1]+" - "+$GiftRegistry[0][15]));
	//Item image
	_assertVisible(_cell("item-image"));
	//Item name
	_assertVisible(_div("name "));
	//edit details link
	_assertVisible(_link("Edit Details"));
	//product details
	_assertVisible(_div("product-list-item"));
	//date added
	_assertVisible(_div("form-row option-date-added"));
	_assertVisible(_label("Date Added"));
	//make this item public check box
	_assertVisible(_checkbox("Make this item Public"));
	_assert(_checkbox("Make this item Public").checked);
	//update
	_assertVisible(_submit("Update"));
	//remove
	_assertVisible(_submit("Remove"));
	//quantity text box
	_assertVisible(_numberbox("Quantity"));
	//add to cart
	_assertVisible(_submit("button-fancy-small add-to-cart"));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124423", "Verify the Naviagetion related to  'Edit details' link present  on Gift registery page, find someone's Gift registery list section in applicationas a  Registered user.");
$t.start();
try
{
	_assertVisible(_link("Edit Details"));
	var $productName=_getText(_div("name "));
		var $productPrice=_getText(_span("price-sales"));
	_click(_link("Edit Details"));
	//validating navigation
		_assertEqual($productName, _getText(_heading1("product-name")));
		_assertEqual($productPrice, _getText(_span("price-sales", _in(_div("product-price")))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124424", "Verify the functionality related to  'Update' link present  on Gift registery page, find someone's Gift registery list section in applicationas a  Registered user.");
$t.start();
try
{
	_click(_link("Gift Registry"));
	//Click on view 
	_click(_link("View"));
	_setValue(_numberbox("/dwfrm_giftregistry/"), $Generic[0][14]);
	//selectig value
	_setSelected(_select("/dwfrm_giftregistry_items/"),$Generic[1][14]);
	_uncheck(_checkbox("/dwfrm_giftregistry_items/"));
	_click(_submit("Update"));
	//validating
	_assertEqual($Generic[0][14], _getValue(_numberbox("/dwfrm_giftregistry/")));
	_assertEqual($Generic[0][14], _getValue(_numberbox("Quantity")));
	_assertEqual($Generic[1][14], _getSelectedText(_select("/dwfrm_giftregistry_items/")));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124425", "Verify the functionality related to  'Remove' link present  on Gift registery page, find someone's Gift registery list section in applicationas a  Registered user.");
$t.start();
try
{
	_assertVisible(_submit("Remove"));
	_click(_submit("Remove"));
	//Item name
	_assertNotVisible(_div("name "));
	//edit details link
	_assertNotVisible(_link("Edit Details"));
	//product details
	_assertNotVisible(_div("product-list-item"));
	//gift registry details
	_assertVisible(_div("page-content-tab-wrapper"));
	_assertVisible(_heading2($GiftRegistry[0][1]+" - "+$GiftRegistry[0][15]));
	_assertVisible(_paragraph($Generic[5][10]));
	//add items link
	_assertVisible(_link("Add Items"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//adding items to gift registry
addItems($Generic[0][11]);
var $t = _testcase("124422", "Verify the functionality related to  'ADD TO CART' button present  on Gift registery page, find someone's Gift registery list section in applicationas a  Registered user.");
$t.start();
try
{
	//Click on add to cart
	_click(_submit("button-fancy-small add-to-cart"));
	//validating
	_assertEqual($ProductName, _getText(_div("mini-cart-name")));
	if(_isIE9())
		{
		_assertEqual($QTY, _extract(_getText(_div("mini-cart-pricing")), "/Qty: (.*)[$]/", true).toString()) 
		}
	else
		{
		_assertEqual($QTY, _extract(_getText(_div("mini-cart-pricing")), "/Qty: (.*) [$]/", true).toString());
		}
	_assertEqual($ProductPrice, _extract(_getText(_div("mini-cart-subtotals")), "/Order Subtotal (.*)/", true).toString());
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//removing items from cart
ClearCartItems()

_click(_link("Gift Registry"));
//Click on view 
_click(_link("View"));
var $t = _testcase("124350", "Verify the functionality related to the ' 'Make This Registry Private' button on Gift registery page  in application as a  Registered user.' ");
$t.start();
try
{
	//click on 'Make This Registry Private'
	_click(_submit("dwfrm_giftregistry_setPrivate"));
	//Make this registry public button
	_assertVisible(_submit("dwfrm_giftregistry_setPublic"));
	//checking URL
	_assertNotVisible(_paragraph("/(.*)/", _in(_div("list-share"))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124349", "Verify the functionality related to the ' 'Make This Registry Public' button on Gift registery page in application as a  Registered user.' ");
$t.start();
try
{
	//Make this registry public button
	_assertVisible(_submit("dwfrm_giftregistry_setPublic"));
	//click on make this registry public button
	_click(_submit("dwfrm_giftregistry_setPublic"));
	//Validating navigation
	//make this registry private
	_assertVisible(_submit("dwfrm_giftregistry_setPrivate"));
	//make this item public check box
	_assertVisible(_checkbox("Make this item Public"));
	_assert(_checkbox("Make this item Public").checked);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124322", "Verify the UI  on Gift Registry page Non empty list of Gift Registry in application as a  Registered user.");
$t.start();
try
{
	_click(_link("user-account"));
	
	_click(_link("Gift Registry"));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
		}
	//heading
	_assertVisible(_heading1("/"+$Generic[1][0]+"/"));
	//left nav
	_assertVisible(_div("secondary-navigation"));
	var $Heading=_count("_span", "/(.*)/",_in(_div("secondary-navigation")));
	var $Links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
		{
			_assertVisible(_span($Generic[$i][1]));			
		}
	//checking links
	for(var $i=0;$i<$Links;$i++)
		{
			_assertVisible(_link($Generic[$i][2]));			
		}
	//left nav content asset(contact us)
	_assertVisible(_div("account-nav-asset"));
	
	//last name
	_assertVisible(_span($Generic[0][15]));
	_assertVisible(_textbox("dwfrm_giftregistry_search_simple_registrantLastName"));
	//first name
	_assertVisible(_span($Generic[3][0]));
	_assertVisible(_textbox("dwfrm_giftregistry_search_simple_registrantFirstName"));
	//email 
	_assertVisible(_label($Generic[4][0]));
	_assertVisible(_textbox("dwfrm_giftregistry_search_simple_eventType"));
	//find button
	_assertVisible(_submit("dwfrm_giftregistry_search_search"));
	//advanced search
	_assertVisible(_link($Generic[5][0]));
	//new registry button
	_assertVisible(_submit("dwfrm_giftregistry_create"));
	//text
	_assertVisible(_paragraph($Generic[6][0]));
	//UI with gift registry's
	//heading
	_assertVisible(_heading1($Generic[13][0]));
	//table header
	_assertVisible(_row($Generic[9][0]));
	//registry contents
	_assertVisible(_row($GiftRegistry[0][2]+" "+$GiftRegistry[0][1]+" "+$GiftRegistry[0][3]+" "+$GiftRegistry[0][6]+", "+$GiftRegistry[1][5]+" View Delete"));
	//view link
	_assertVisible(_link("View"));
	//delete link
	_assertVisible(_link("Delete"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124357", "Verify the UI elements after clicking 'Advanced Search' link  on Gift registery page, find someone's Gift registery list section in applicationas a  Registered user.");
$t.start();
try
{
	_click(_link("Advanced Search"));
	_assertVisible(_textbox("dwfrm_giftregistry_search_simple_registrantLastName"));
	_assertVisible(_textbox("dwfrm_giftregistry_search_simple_registrantFirstName"));
	_assertVisible(_textbox("dwfrm_giftregistry_search_simple_eventType"));
	_assertVisible(_textbox("dwfrm_giftregistry_search_advanced_eventName"));
	_assertVisible(_select("dwfrm_giftregistry_search_advanced_eventMonth"));
	_assertVisible(_select("dwfrm_giftregistry_search_advanced_eventYear"));
	_assertVisible(_textbox("dwfrm_giftregistry_search_advanced_eventCity"));
	//_assertVisible(_select("dwfrm_giftregistry_search_advanced_eventAddress_states_state"));
	//_assertVisible(_select("dwfrm_giftregistry_search_advanced_eventAddress_country"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124358/148787/148788/148789/148787148790/148791", "Verify validation related to Event name/Event City/ Event Month text fields/Year drop down/State drop down/Country drop down  present  on Gift registery page, find someone's Gift registery list section in applicationas a  Registered user.");
$t.start();
try
{
	for(var $i=0;$i<$Advance.length;$i++)
	{
		_setValue(_textbox("dwfrm_giftregistry_search_simple_registrantLastName"),$LastName1);
		_setValue(_textbox("dwfrm_giftregistry_search_simple_registrantFirstName"),$FirstName1);
		_setValue(_textbox("dwfrm_giftregistry_search_simple_eventType"),$Advance[$i][2]);
		_setValue(_textbox("dwfrm_giftregistry_search_advanced_eventName"),$Advance[$i][3]);
		_setSelected(_select("dwfrm_giftregistry_search_advanced_eventMonth"),$Advance[$i][4]);
		_setSelected(_select("dwfrm_giftregistry_search_advanced_eventYear"),$Advance[$i][5]);
		_setValue(_textbox("dwfrm_giftregistry_search_advanced_eventCity"),$Advance[$i][6]);		
		//_setSelected(_select("dwfrm_giftregistry_search_advanced_eventAddress_states_state"),$Advance[$i][7]);
		//_setSelected(_select("dwfrm_giftregistry_search_advanced_eventAddress_country"),$Advance[$i][8]);
		_click(_submit("Find"));
		_assertVisible(_heading2($Generic[12][0]));
		if($i==$Advance.length-1)
			{
				_assertVisible(_table("registry-results-table item-list"));
				_assertVisible(_row($LastName1+" "+$FirstName1+" "+$Advance[$i][2]+" "+$Advance[0][10]+" "+$Advance[$i][6]+", "+$Advance[$i][7]+" View"));
			}
		else
			{
				_assertVisible(_paragraph($Generic[10][0]+" "+$FirstName1+" "+$LastName1+" , "+$Generic[11][0]));
			}	
		_click(_link("Advanced Search"));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124325", "Verify the functionality related to 'Delete' link  in Gift Registry page  in application as a  Registered user.");
$t.start();
try
{
	_click(_link("My Account"));
	_click(_link("Gift Registry"));
	if(_isVisible(_link("Delete")))
	  {
	  
	      _click(_link("Delete"));
	      _expectConfirm("Do you want to remove this gift registry?", true); 
	      _assertNotVisible(_heading1($Generic[13][0]));
	      _assertNotVisible(_table("item-list"));
	    _log("Gift Registry deleted successfully");
	  }
	  else
	  { 
	    _assert(false, "Delete Link is not present");
	  }
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


_click(_image("Demandware SiteGenesis"));

cleanup();