_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("GiftRegistry_Data.xls");

var $Generic=_readExcelFile("GiftRegistry_Data.xls","Generic");
var $CR_Validation=_readExcelFile("GiftRegistry_Data.xls","CreateRegistry_Validation");
var $GiftRegistry=_readExcelFile("GiftRegistry_Data.xls","GiftRegistry");



var $sendtofriend=_readExcelFile("GiftRegistry_Data.xls","Sendtofriend_Validation");
var $Advance=_readExcelFile("GiftRegistry_Data.xls","Advance_Search");
var $color=$Generic[0][6];
var $color1=$Generic[1][6];
//from util.GenericLibrary/BrowserSpecific.js
var $FirstName1=$FName;
var $LastName1=$LName;
var $UserEmail=$uId;


SiteURLs()
cleanup();
_setAccessorIgnoreCase(true); 
//Login
_click(_link("Login"));
login();

//Deleting addresses, which intern will delete wishlist and gift registry
deleteAddress();
/*
var $t = _testcase("124318/124320", "Verify 'GIFT REGISTRY' link at the Header for Logged-in User and Verify the UI of  'Gift Registry page' in application as a  Registered user.");
$t.start();
try
{

	_click(_link("Gift Registry"));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	
//	if(_isIE9())
//		{
//		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
//		}
//	else
//		{
//		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
//		}
	
	//heading
	_assertVisible(_heading1("/"+$Generic[1][0]+"/"));
	//left nav
	_assertVisible(_div("secondary-navigation"));
	var $Heading=_count("_span", "/(.*)/",_in(_div("secondary-navigation")));
	var $Links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
	//checking headings
	for(var $i=0;$i<$Heading;$i++)
		{
			_assertVisible(_span($Generic[$i][1]));			
		}
	//checking links
	for(var $i=0;$i<$Links;$i++)
		{
			_assertVisible(_link($Generic[$i][2]));			
		}
	//left nav content asset(contact us)
	_assertVisible(_div("account-nav-asset"));
	
	//last name
	_assertVisible(_span($Generic[2][0]));
	_assertVisible(_textbox("dwfrm_giftregistry_search_simple_registrantLastName"));
	//first name
	_assertVisible(_span($Generic[3][0]));
	_assertVisible(_textbox("dwfrm_giftregistry_search_simple_registrantFirstName"));
	//Event Type
	_assertVisible(_label($Generic[4][0]));
	_assertVisible(_select("dwfrm_giftregistry_search_simple_eventType"));
	//find button
	_assertVisible(_submit("dwfrm_giftregistry_search_search"));
	//advanced search
	_assertVisible(_link($Generic[5][0]));
	//new registry button
	_assertVisible(_submit("dwfrm_giftregistry_create"));
	//text
	_assertVisible(_paragraph($Generic[6][0]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124319", "Verify the navigation related to 'Home' and 'My Account' links on  'Gift Registry  page bread crumb in application as a  Registered user.");
$t.start();
try
{
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	
//	if(_isIE9())
//		{
//		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
//		}
//	else
//		{
//		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
//		}
	
	//Navigation related to home
	//_assertVisible(_link("Home", _in(_div("breadcrumb"))));
	//_click(_link("Home", _in(_div("breadcrumb"))));
	
	//verify the navigation to home page
	_click(_link("Demandware SiteGenesis"));

	_assertVisible(_list("homepage-slides"));
	_assertVisible(_div("home-bottom-slots"));
	
	_click(_link("user-account"));
	_click(_link("Gift Registry"));
	//Navigation related to My account
	_assertVisible(_link("My Account", _in(_div("breadcrumb"))));
	_click(_link("My Account", _in(_div("breadcrumb"))));
	_assertVisible(_heading1("/My Account /"));
	if(_isIE9())
		{
		_assertEqual($Generic[7][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[7][0], _getText(_div("breadcrumb")));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124334", "Verify the navigation related to left nav links on Create Gift Registry page in application as a Registered user. ");
$t.start();
try
{
	//left nav links	
	var $links=_count("_listItem", "/(.*)/",_in(_div("secondary-navigation")));
	for(var $i=0;$i<$links;$i++)
	{
		//navigate to create gift registry page
		_click(_link("user-account"));
		_click(_link("Gift Registry"));
		//click on each links 
		_setAccessorIgnoreCase(true); 
		_click(_link($Generic[$i][2], _in(_div("secondary-navigation"))));
		//verify the navigation
		_assertVisible(_heading1("/"+$Generic[$i][4]+"/"));
		//For 2 links bread crumb-last element does'nt exist
		if($i==9 || $i==10)
			{
			_assertContainsText($Generic[$i][5], _div("breadcrumb"));			
			}
		else
			{
			_assertContainsText($Generic[$i][5], _div("breadcrumb"));
			_assertVisible(_div("breadcrumb"));	
			_assertVisible(_link($Generic[$i][5], _in(_div("breadcrumb"))));			
			}		
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148741", "Verify the functionality related to  down arrow present in left Nav on Create Gift Registry page in application a Registered user. ");
$t.start();
try
{
	//navigate to create gift registry page
	_click(_link("user-account"));
	_click(_link("Gift Registry"));
	//click on new registry
	_click(_submit("dwfrm_giftregistry_create"));
	var $j=1;
	//fetching the no.of headings in content assert
	var $headings=_collectAttributes("_span","/toggle/","sahiText",_in(_div("secondary-navigation")));
	for(var $i=0;$i<$headings.length;$i++)
		{
			//fetch links present in respective toggle's
			if($i==$headings.length-1)
				{
					$g=$i-1;
					var $links=_collectAttributes("_listItem","/(.*)/","sahiText",_in(_list($i,_in(_div("content-asset")))));
					_click(_span($headings[$i]));
					for(var $k=0;$k<$links.length;$k++)
						{
							if($links[$k]!="")
								{
									_assertNotVisible(_listItem($links[$k]));
								}						
						}
				}
			else
				{
					var $links=_collectAttributes("_listItem","/(.*)/","sahiText",_in(_list($i,_in(_div("content-asset")))));
					_click(_span($headings[$i]));
					for(var $k=0;$k<$links.length;$k++)
						{
							if($links[$k]!="")
								{
									_assertNotVisible(_listItem($links[$k]));
								}
						}
					$j++;
				}	
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/

var $t = _testcase("124328", "Verify the navigation related to 'New Registry' button on Gift Registry page in application as a  Registered user.");
$t.start();
try
{
	_click(_link("user-account"));
	_click(_link("Gift Registry"));
	//click on new registry
	_click(_submit("dwfrm_giftregistry_create"));
	//bread crumb
	_assertVisible(_div("breadcrumb"));	
	if(_isIE9())
		{
		_assertEqual($Generic[0][0].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][0], _getText(_div("breadcrumb")));
		}	
	//heading
	_assertVisible(_heading1($Generic[0][3]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124329", "Verify the UI elements of event information section on  Create Gift Registry page in application as a  Registered user.");
$t.start();
try
{
	//section heading
	_assertVisible(_fieldset($Generic[1][3]));
	//event type
	_assertVisible(_span("Event Type"));
	_assertVisible(_textbox("dwfrm_giftregistry_event_type"));
	//event name
	_assertVisible(_span("Event Name"));
	_assertVisible(_textbox("dwfrm_giftregistry_event_name"));
	//event date
	_assertVisible(_span("Event Date"));
	_assertVisible(_datebox("dwfrm_giftregistry_event_date"));
	_assertVisible(_div($Generic[3][3]));
//	//Country
//	_assertVisible(_span("Country"));
//	_assertVisible(_select("dwfrm_giftregistry_event_eventaddress_country"));
//	_assertEqual($Generic[2][3], _getSelectedText(_select("dwfrm_giftregistry_event_eventaddress_country")));
//	//state
//	_assertVisible(_span("State"));
//	_assertVisible(_select("dwfrm_giftregistry_event_eventaddress_states_state"));
//	_assertEqual($Generic[2][3], _getSelectedText(_select("dwfrm_giftregistry_event_eventaddress_states_state")));
	//city
	_assertVisible(_span("Event City"));
	_assertVisible(_textbox("dwfrm_giftregistry_event_town"));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124330", "Verify the UI elements of  Participant Information section on  Create Gift Registry page   in application as a  Registered user.");
$t.start();
try
{
	_assertVisible(_fieldset($Generic[4][3]));
	//participant 1
	_assertVisible(_heading2($Generic[5][3]));
	//role
	_assertVisible(_span("Role"));
	_assertVisible(_select("dwfrm_giftregistry_event_participant_role"));
	_assertEqual($Generic[2][3], _getSelectedText(_select("dwfrm_giftregistry_event_participant_role")));
	//first name
	_assertVisible(_span("First Name"));
	_assertVisible(_textbox("dwfrm_giftregistry_event_participant_firstName"));
	_assertEqual($FirstName1, _getValue(_textbox("dwfrm_giftregistry_event_participant_firstName")));
	//last name
	_assertVisible(_span("Last Name"));
	_assertVisible(_textbox("dwfrm_giftregistry_event_participant_lastName"));
	_assertEqual($LastName1, _getValue(_textbox("dwfrm_giftregistry_event_participant_lastName")));
	//email
	_assertVisible(_span("Email"));
	_assertVisible(_textbox("dwfrm_giftregistry_event_participant_email"));
	_assertEqual($UserEmail, _getValue(_textbox("dwfrm_giftregistry_event_participant_email")));
	//participant 2
	_assertVisible(_heading2($Generic[6][3]));
	//role
	_assertVisible(_label("Role"));
	_assertVisible(_select("dwfrm_giftregistry_event_coParticipant_role"));
	_assertEqual($Generic[2][3], _getSelectedText(_select("dwfrm_giftregistry_event_coParticipant_role")));
	//Fname
	_assertVisible(_span("First Name", _in(_div("First Name"))));
	_assertVisible(_textbox("dwfrm_giftregistry_event_coParticipant_firstName"));
	//Lname
	_assertVisible(_span("Last Name", _in(_div("Last Name"))));
	_assertVisible(_textbox("dwfrm_giftregistry_event_coParticipant_lastName"));
	//email
	_assertVisible(_span("Email", _in(_div("Email"))));
	_assertVisible(_textbox("dwfrm_giftregistry_event_coParticipant_email"));
	//Continue button
	_assertVisible(_submit("Continue"));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124331/124332/124333/148742/148743/148744/148745/148746/148747/148748/148749/148750/148751/148752/148753", "Verify validations related to Event Type drop down/Event name/ Event date/country/state/city/First participents role/fname/lname/email/second participants role/fname/lname/email on  Create Gift Registry page in application as a  Registered user.");
$t.start();
try
{
	for(var $i=0;$i<$CR_Validation.length;$i++)
		{
			if($i==0)
				{	
				_setValue(_textbox("dwfrm_giftregistry_event_type"),"");
				_setValue(_textbox("dwfrm_giftregistry_event_name"),"");
				_setValue(_datebox("dwfrm_giftregistry_event_date"),"");
				//_setSelected(_select("dwfrm_giftregistry_event_eventaddress_country"),"");
				//_setSelected(_select("dwfrm_giftregistry_event_eventaddress_states_state"),"");
				_setValue(_textbox("dwfrm_giftregistry_event_town"),"");
				_setSelected(_select("dwfrm_giftregistry_event_participant_role"),"");
				_setValue(_textbox("dwfrm_giftregistry_event_participant_firstName"),"");
				_setValue(_textbox("dwfrm_giftregistry_event_participant_lastName"),"");
				_setValue(_textbox("dwfrm_giftregistry_event_participant_email"),"");
				_setSelected(_select("dwfrm_giftregistry_event_coParticipant_role"),"");
				_setValue(_textbox("dwfrm_giftregistry_event_coParticipant_firstName"),"");
				_setValue(_textbox("dwfrm_giftregistry_event_coParticipant_lastName"),"");
				_setValue(_textbox("dwfrm_giftregistry_event_coParticipant_email"),"");
				}
			else
				{
				_setValue(_textbox("dwfrm_giftregistry_event_type"),$CR_Validation[$i][1]);
				_setValue(_textbox("dwfrm_giftregistry_event_name"),$CR_Validation[$i][2]);
				_setValue(_datebox("dwfrm_giftregistry_event_date"),$CR_Validation[$i][3]);
				//_setSelected(_select("dwfrm_giftregistry_event_eventaddress_country"),$CR_Validation[$i][4]);
				//_setSelected(_select("dwfrm_giftregistry_event_eventaddress_states_state"),$CR_Validation[$i][5]);
				_setValue(_textbox("dwfrm_giftregistry_event_town"),$CR_Validation[$i][6]);
				_setSelected(_select("dwfrm_giftregistry_event_participant_role"),$CR_Validation[$i][7]);
				_setValue(_textbox("dwfrm_giftregistry_event_participant_firstName"),$CR_Validation[$i][8]);
				_setValue(_textbox("dwfrm_giftregistry_event_participant_lastName"),$CR_Validation[$i][9]);
				_setValue(_textbox("dwfrm_giftregistry_event_participant_email"),$CR_Validation[$i][10]);
				_setSelected(_select("dwfrm_giftregistry_event_coParticipant_role"),$CR_Validation[$i][11]);
				_setValue(_textbox("dwfrm_giftregistry_event_coParticipant_firstName"),$CR_Validation[$i][12]);
				_setValue(_textbox("dwfrm_giftregistry_event_coParticipant_lastName"),$CR_Validation[$i][13]);
				_setValue(_textbox("dwfrm_giftregistry_event_coParticipant_email"),$CR_Validation[$i][14]);
				}
			if($i!=1)
			{
				_click(_submit("Continue"));
			}
			
				if($i==6)
					{
						_setValue(_textbox("dwfrm_giftregistry_event_participant_firstName"),$FirstName1);
						_setValue(_textbox("dwfrm_giftregistry_event_participant_lastName"),$LastName1);
						_setValue(_textbox("dwfrm_giftregistry_event_participant_email"),$UserEmail);
					}	
				//blank field
				if($i==0)
					{
						_assertEqual($color, _style(_textbox("dwfrm_giftregistry_event_type"), "background-color"));
						_assertEqual($color, _style(_textbox("dwfrm_giftregistry_event_name"), "background-color"));
						_assertEqual($color, _style(_datebox("dwfrm_giftregistry_event_date"), "background-color"));
						//_assertEqual($color, _style(_select("dwfrm_giftregistry_event_eventaddress_country"), "background-color"));
						//_assertEqual($color, _style(_select("dwfrm_giftregistry_event_eventaddress_states_state"), "background-color"));
						_assertEqual($color, _style(_textbox("dwfrm_giftregistry_event_town"), "background-color"));
						_assertEqual($color, _style(_select("dwfrm_giftregistry_event_participant_role"), "background-color"));
						_assertEqual($color, _style(_textbox("dwfrm_giftregistry_event_participant_firstName"), "background-color"));
						_assertEqual($color, _style(_textbox("dwfrm_giftregistry_event_participant_lastName"), "background-color"));
						_assertEqual($color, _style(_textbox("dwfrm_giftregistry_event_participant_email"), "background-color"));
						//_assertEqual($color, _style(_select("dwfrm_giftregistry_event_coParticipant_role"), "background-color"));
						//_assertEqual($color, _style(_textbox("dwfrm_giftregistry_event_coParticipant_firstName"), "background-color"));
						//_assertEqual($color, _style(_textbox("dwfrm_giftregistry_event_coParticipant_lastName"), "background-color"));
						//_assertEqual($color, _style(_textbox("dwfrm_giftregistry_event_coParticipant_email"), "background-color"));					
					}
				//max
				else if($i==1)
					{
						_assertEqual($Generic[0][7],_getText(_textbox("dwfrm_giftregistry_event_type")).length);
						_assertEqual($Generic[0][7],_getText(_textbox("dwfrm_giftregistry_event_name")).length);
						_assertEqual($Generic[1][7],_getText(_datebox("dwfrm_giftregistry_event_date")).length);
						_assertEqual($Generic[0][7],_getText(_textbox("dwfrm_giftregistry_event_town")).length);
						_assertEqual($Generic[0][7],_getText(_textbox("dwfrm_giftregistry_event_participant_firstName")).length);
						_assertEqual($Generic[0][7],_getText(_textbox("dwfrm_giftregistry_event_participant_lastName")).length);
						_assertEqual($Generic[0][7],_getText(_textbox("dwfrm_giftregistry_event_participant_email")).length);
						_assertEqual($Generic[0][7],_getText(_textbox("dwfrm_giftregistry_event_coParticipant_firstName")).length);
						_assertEqual($Generic[0][7],_getText(_textbox("dwfrm_giftregistry_event_coParticipant_lastName")).length);
						_assertEqual($Generic[0][7],_getText(_textbox("dwfrm_giftregistry_event_coParticipant_email")).length);
					}
				//spl character
				else if($i==2)
					{
					//date
					_assertVisible(_span($CR_Validation[0][16],_in(_div("/Event Date/"))));
					_assertEqual($color, _style(_datebox("dwfrm_giftregistry_event_date"), "background-color"));
					//email
					_assertVisible(_span($CR_Validation[0][17]));
					_assertEqual($color1,_style(_span("Email"),"color"));
					}
				//invalid date
				else if($i==3)
					{
					_assertVisible(_span($CR_Validation[0][16],_in(_div("/Event Date/"))));
					_assertEqual($color, _style(_datebox("dwfrm_giftregistry_event_date"), "background-color"));
					}
				//invalid email format
				else if($i==4 ||$i==5)
					{
					_assertVisible(_span($CR_Validation[0][17]));
					_assertVisible(_span($CR_Validation[0][17], _in(_div("Email Please enter a valid email address."))));
					_assertEqual($color1,_style(_span("Email"),"color"));
					_assertEqual($color1,_style(_span("Email", _in(_label("Email"))),"color"));
					}					
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


cleanup();
