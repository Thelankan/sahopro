_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Search_Data.xls");
var $Generic=_readExcelFile("Search_Data.xls", "Generic");
var $Suggestion=_readExcelFile("Search_Data.xls", "Suggestion");

SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();



var $t = _testcase("124686/125409", "Verify the navigation to search result page from home page and UI of search result page  in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic[0][0]);
	//bread crumb
	_assertVisible(_span("breadcrumb-element breadcrumb-result-text"));
	_assertEqual($Generic[0][2]+" \""+$Generic[0][0]+"\"", _getText(_span("breadcrumb-element breadcrumb-result-text")));
	//sort by
	_assertVisible(_select("grid-sort-header"));
	_assertVisible(_select("grid-sort-footer"));
	if(!isMobile())
		{
		//refinement header
		_assertVisible(_span("refinement-header"));
		//left nav
		_assertVisible(_div("refinements"));
		//items per page
//		_assertVisible(_select("grid-paging-header"));
//		_assertVisible(_select("grid-paging-footer"));
		}
	else
		{
		//left nav
		_assertVisible(_div("navigation"));		
		}
	//Pagination
	_assertVisible(_div("pagination"));
	_assertVisible(_div("pagination", _under(_list("search-result-items"))));
	//Items displayed
	_assertExists(_list("search-result-items"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125413", "Verify the navigation related to 'Home' link on search result page breadcrumb  in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic[0][0]);
	//bread crumb
	_assertVisible(_span("breadcrumb-element breadcrumb-result-text"));
	_assertEqual($Generic[0][2]+" \""+$Generic[0][0]+"\"", _getText(_span("breadcrumb-element breadcrumb-result-text")));
	//navigation
	_assertNotVisible(_link("Home",_in(_div("breadcrumb"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125410", "Verify the navigation related to 'Keyword' link on search result page breadcrumb  in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic[0][0]);
	//bread crumb
	_assertVisible(_span("breadcrumb-element breadcrumb-result-text"));
	_assertEqual($Generic[0][2]+" \""+$Generic[0][0]+"\"", _getText(_span("breadcrumb-element breadcrumb-result-text")));
	//Click on search key word
	_click(_link($Generic[0][0], _in(_span("breadcrumb-element breadcrumb-result-text"))));
	//bread crumb
	_assertVisible(_span("breadcrumb-element breadcrumb-result-text"));
	_assertEqual($Generic[0][2]+" \""+$Generic[0][0]+"\"", _getText(_span("breadcrumb-element breadcrumb-result-text")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125415", "Verify the UI/functionality of 'REFINE SEARCH' on search result page  left nav in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic[0][0]);
	var $Filters=_collectAttributes("_heading3","/toggle/","sahiText",_in(_div("refinements")));
	for(var $i=0;$i<$Filters.length;$i++)
		{	
			//click on filter
			_click(_heading3($Filters[$i]));
			
			if ($Filters[$i]=="Category")
				{
				_assertNotVisible(_list("/(.*)/", _in(_div("/refinement  category-refinement/"))));
				//click on filter
				_click(_heading3($Filters[$i]));
				//Filter Should Collapse
				_assertVisible(_list("/(.*)/", _in(_div("/refinement  category-refinement/"))));
				
				}
			else if ($Filters[$i]=="Color")
				{
				_assertNotVisible(_list("/(.*)/", _in(_div("/refinement refinementColor/"))));
				//click on filter
				_click(_heading3($Filters[$i]));
				//Filter Should Collapse
				_assertVisible(_list("/(.*)/", _in(_div("/refinement refinementColor/"))));
				}
			else if ($Filters[$i]=="Price")
				{
				 _assertNotVisible(_list("/(.*)/", _in(_div("refinement "))));
				//click on filter
				_click(_heading3($Filters[$i]));
				//Filter Should Collapse
				 _assertVisible(_list("/(.*)/", _in(_div("refinement "))));
				}
			else
				{
				 _assertNotVisible((_list("/(.*)/", _in(_div("/refinement isNew/")))));
				//click on filter
				_click(_heading3($Filters[$i]));
				_assertVisible(_list("/(.*)/", _in(_div("/refinement isNew/"))));
				}
			
		
		}

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125416", "Verify the functionality related to  'Category' section on search result page left nav in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic[0][0]);
	_setAccessorIgnoreCase(true);
	//navigation of sub-category link.
	var $categories=_collectAttributes("_listItem","/(.*)/","sahiText",_in(_div("refinement  category-refinement")));
	for($i=1;$i<$categories.length;$i++)
	{
	_click(_link($categories[$i], _in(_div("refinement  category-refinement"))));
	_assertContainsText($categories[$i], _div("breadcrumb"));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125412", "Verify the functionality related to  'Display icon' on search result page in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic[0][0]);
	_assertVisible(_div("search-result-content"));
	_click(_span("toggle-grid"));
	_assertVisible(_byClassName("search-result-content wide-tiles", "DIV"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125417", "Verify the system's response on selection on color filter from left nav in the application both as an anonymous and a registered user");
$t.start();
try
{
	search($Generic[0][0]);
	var $colors = _collectAttributes("_link","/(.*)/","sahiText",_in(_list("clearfix swatches refinementColor")));
	var $unSelectable=_collectAttributes("_listItem","/unselectable swatch/","sahiText",_in(_list("clearfix swatches refinementColor")));
	for(var $k=0; $k<$colors.length; $k++)
	{
		if($unSelectable.indexOf($colors[$k])>=0)
			{
			__assert(true, "Swatch is unselectable");
			}
		else
			{
			 _click(_link($colors[$k]));
			  //clear link
			  _assertVisible(_link("Clear", _in(_div("refinement refinementColor"))));
			  _assertEqual($colors[$k]+" x",_getText(_span("breadcrumb-refinement-value")));
			 _click(_link("breadcrumb-relax"));
			 _wait(4000,!_isVisible(_link("breadcrumb-relax")));
			}  
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}		
$t.end();

var $t = _testcase("125418", "Verify the functionality related to  multiple 'Colour' section on search result page left nav in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic[0][0]);
	var $colors1 = _collectAttributes("_link","/(.*)/","sahiText",_in(_list("clearfix swatches refinementColor")));
	var $unSelectable1=_collectAttributes("_listItem","/unselectable swatch/","sahiText",_in(_list("clearfix swatches refinementColor")));
	var $colorCount=0;
	var $ExpColor= new Array();
	for(var $k=0; $k<$Generic[0][3]; $k++)
	{
		if($unSelectable1.indexOf($colors1[$k])>=0)
			{
			__assert(true, "Swatch is unselectable");
			}
		else
			{
			 _click(_link($colors1[$k]));
			  //clear link
			  _assertVisible(_link("Clear", _in(_div("refinement refinementColor"))));
			  $colorCount++;
			$ExpColor.push($colors1[$k]);
			_wait(2000);
			} 		
	}
	var $ActualColor=_collectAttributes("_listItem","/selected swatch/", "sahiText",_in(_list("clearfix swatches refinementColor")));
	_assertEqualArrays($ExpColor,$ActualColor);
}
catch($e)
{
	_logExceptionAsFailure($e);
}		
$t.end();

var $t = _testcase("125419", "Verify the functionality related to  'Price' section on search result page left nav in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic[0][0]);
	var $PriceRange = _collectAttributes("_link","/(.*)/","sahiText",_in(_div("refinement ")));
	for ($k=0; $k< $PriceRange.length; $k++)
	{
	_click(_link($PriceRange[$k]));
	_assertEqual($PriceRange[$k]+" x", _getText(_span("breadcrumb-refinement-value"))); 	
	priceCheck($PriceRange[$k]);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125420", "Verify the functionality related to  'New Arrival section on search result page left nav in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic[0][0]);
	if(_isVisible(_heading3("NEW ARRIVAL")))
	  {
		_click(_link("Refine by:true"));
		_assertVisible(_link("Clear"));
		_assertVisible(_span("breadcrumb-refinement-value"));
	    _click(_link("Clear"));
	    _assertNotVisible(_link("Clear"));
	    _assertNotVisible(_span("breadcrumb-refinement-value"));
	  }
	  else
	  {
	    _log("No New Arrival Refinement available");
	  }
	_click(_image("logo.png"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125422", "Verify the navigation related to product image on search result page in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic[0][0]);
	var $productName=_getText(_link("name-link"));
	var $productPrice = _getText(_span("product-sales-price"));
	//click on name link
	_click(_link("name-link"));
	_assertEqual($productName,_getText(_heading1("product-name")));
	_assertEqual($productPrice,_getText(_span("price-sales")));
	
	search($Generic[0][0]);
	var $productName=_getText(_link("name-link"));
	var $productPrice = _getText(_span("product-sales-price"));
	//click on image
	_click(_link("thumb-link"));
	_assertEqual($productName,_getText(_heading1("product-name")));
	_assertEqual($productPrice,_getText(_span("price-sales")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125411", "Verify the functionality related to 'Please Select One' drop down on search result page in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic[0][0]);
	if(_isVisible(_select("grid-sort-header")) && _isVisible(_select("grid-sort-footer")))
	{
		var $sort=_getText(_select("grid-sort-header"));
		var $j=0;
		for(var $i=1;$i<$sort.length;$i++)
		{
			_setSelected(_select("grid-sort-header"), $sort[$i]);
			//Validating the selection
			_assertEqual($sort[$i],_getSelectedText(_select("grid-sort-header")),"Selected option is displaying properly");
			_assertEqual($sort[$i],_getSelectedText(_select("grid-sort-footer")),"Selected option is displaying properly");
			if($sort.indexOf($Generic[$j][1])>-1)
			{
				_setSelected(_select("grid-sort-header"), $Generic[$j][1]);
				sortBy($Generic[$j][1]);
				$j++;
			}
		}
	}
	else
	{
		_assert(false, "sort by drop down is not present");
	}
	_click(_image("logo.png"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
/*
var $t = _testcase("125414", "Verify the pagination in category landing page in the application both as an anonymous and a registered user");
$t.start();
try
{
	search($Generic[0][0]);
	///pagination
	pagination();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/
if(!isMobile())
{
var $t=_testcase("--","Verify the navigation related to Quickview overlay In the application both as an anonymous and a registered user");
$t.start();
try
{
	search($Generic[0][0]);
	//fetching the product name
	var $PName=_getText(_link("name-link"));
	//mouse over on image
	_mouseOver(_link("thumb-link"));
	//Quick view button
	//_assertVisible(_link("quickviewbutton"));
	_click(_link("quickviewbutton"));  
	//quick view overlay
	_assertVisible(_div("ui-dialog-content ui-widget-content"));
	_assertEqual($PName, _getText(_heading1("product-name")));
	//closing overlay
	_click(_button("CLOSE"));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()
}

 var $t = _testcase("124687/125424/125429", "Verify the navigation to search no result page from home page, the UI of search no result page and the UI of 'REFINE SEARCH' section on search no result page  left nav in application both as an  Anonymous and  Registered  user.");
 $t.start();
 try
 {
	 search($Generic[0][4]);
	 //_assertEqual($Generic[0][2]+" \""+$Generic[0][4]+"\"", _getText(_span("breadcrumb-element breadcrumb-result-text")));
	 //heading
	 _assertVisible(_heading1($Generic[0][5]));
	 //text
	 _assertVisible(_paragraph($Generic[1][5]+" "+$Generic[0][4]));
	 _assertVisible(_div("content-asset"));
	 _assertVisible(_label($Generic[2][5]));
	 //try new search 
	 _assertVisible(_textbox("q", _in(_div("form-row label-above"))));
	 _assertVisible(_submit("Go"));
	 _assertVisible(_div("no-hits-footer"));
	 //refine search
	 _assertVisible(_heading2($Generic[3][5]));
	 _setAccessorIgnoreCase(true); 
	 var $Expected=_collectAttributes("_link","/has-sub-menu/", "sahiText");	 
	 var $Actual= _collectAttributes("_listItem","/expandable/", "sahiText", _in(_div("refinement Category")));
	 _assertEqualArrays($Expected.sort(),$Actual.sort());
}
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end()
 /*
 var $t = _testcase("125425", "Verify the navigation related to 'Home' link on search no result page breadcrumb  in application both as an  Anonymous and  Registered  user.");
 $t.start();
 try
 {
	 search($Generic[0][4]);
	 _assertEqual($Generic[0][2]+" \""+$Generic[0][4]+"\"", _getText(_span("breadcrumb-element breadcrumb-result-text")));
	//navigation
	 _assertNotVisible(_link("Home",_in(_div("breadcrumb"))));
 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end()
 */

 var $t = _testcase("125426", "Verify the functionality related to  Try new search 'GO' button for valid keyword on search no result page in application both as an  Anonymous and  Registered  user.");
 $t.start();
 try
 {
	 //Nosearch results navigation
	 search($Generic[0][4]);
	 //try new search 
	 _setValue(_textbox("q", _in(_div("form-row label-above"))), $Generic[0][0]);
	 _click(_submit("simplesearch"));
	//bread crumb
		_assertVisible(_span("breadcrumb-element breadcrumb-result-text"));
		_assertEqual($Generic[0][2]+" \""+$Generic[0][0]+"\"", _getText(_span("breadcrumb-element breadcrumb-result-text")));
 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end()
 
 var $t = _testcase("125427", "Verify the functionality related to  Try new search 'GO' button for invalid keyword on search no result page in application both as an  Anonymous and  Registered  user.");
 $t.start();
 try
 {
	 //Nosearch results navigation
	 search($Generic[0][4]);
	 //try new search 
	 _setValue(_textbox("q", _in(_div("form-row label-above"))),$Generic[1][4]);
	 _click(_submit("simplesearch"));
	 //_assertEqual($Generic[0][2]+" \""+$Generic[1][4]+"\"", _getText(_span("breadcrumb-element breadcrumb-result-text")));
	 //heading
	 _assertVisible(_heading1($Generic[0][5]));
	 //text
	 _assertVisible(_paragraph($Generic[1][5]+" "+$Generic[1][4]));
 }
 catch($e)
 {
 	 _logExceptionAsFailure($e);
 }
 $t.end()

 cleanup();