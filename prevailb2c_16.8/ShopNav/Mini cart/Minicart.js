_include("../../util.GenericLibrary/GlobalFunctions.js");
_include("../../util.GenericLibrary/BM_Configuration.js");
_resource("Minicart.xls");

var $Minicart=_readExcelFile("Minicart.xls","Minicart");


SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("124691", "Verify the UI of mini Cart  when single product is added to bag  in the application both as an anonymous and a registered user");
$t.start();
try
{
//add item to cart
addItemToCart($Minicart[0][0],$Minicart[0][4]);
var $pName=_getText(_heading1("product-name"));
var $price=_getText(_span("price-sales")).replace("$","");
//verify the UI of Mini cart 
//mini cart overlay
_mouseOver(_link("mini-cart-link"));
_assertVisible(_div("mini-cart-content"));
_assertVisible(_span("mini-cart-toggle fa fa-caret-down"));
//name link
_assertEqual($pName,_getText(_link("/(.*)/",_in(_div("mini-cart-name")))));
_mouseOver(_link("mini-cart-link"));
//image
_assertVisible(_image("/(.*)/",_in(_div("mini-cart-image"))));
_assertVisible(_div("mini-cart-image"));
//attributes
if($colorBoolean)
	{
		_mouseOver(_link("mini-cart-link"));
		_assertVisible(_span("Color"));
		_assertVisible(_span("value",_near(_span("Color"))));
	}
if($sizeBoolean)
	{
		_mouseOver(_link("mini-cart-link"));
		_assertVisible(_span("Size"));
		_assertVisible(_span("value",_near(_span("Size"))));
	}
if($widthBoolean)
	{
		_mouseOver(_link("mini-cart-link"));
		_assertVisible(_span("Width"));
		_assertVisible(_span("value",_near(_span("Width"))));
	}
_mouseOver(_link("mini-cart-link"));
//quantity
_assertVisible(_span("Qty:"));
_assertVisible(_div("mini-cart-pricing"));

_mouseOver(_link("mini-cart-link"));
//sub total
_assertVisible(_span("Order Subtotal"));
_assertVisible(_span("value",_rightOf(_span("Order Subtotal"))));
_assertVisible(_div("mini-cart-subtotals"));
//slot
_assertVisible(_div("mini-cart-slot"));
//view cart
_assertVisible(_link("VIEW CART"));
_assertVisible(_link("mini-cart-link-checkout"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125605/125606/125607", "Verify the calculation of 'Order Subtotal'  & Product total/Quantity/Total In the header in the mini cart in the application both as an anonymous and a registered user");
$t.start();
try
{
//add item to cart
addItemToCart($Minicart[1][0],$Minicart[1][4]);	
//no.of items present in mini cart
var $totalSectionsInMiniCart=_count("_div","/mini-cart-product/",_in(_div("mini-cart-products")));
var $minicartQuantity=new Array();
var $minicartPrice=new Array();
var $minicartName=new Array();

//featching data from minicart
for(var $i=0;$i<$totalSectionsInMiniCart;$i++)
	{
		//_mouseOver(_link("View Cart"));
		_mouseOver(_link("mini-cart-link"));
		$minicartQuantity[$i]=parseInt(_extract(_getText(_div("mini-cart-pricing["+$i+"]")),"/[:](.*)/",true));
		$minicartPrice[$i]=parseFloat(_extract(_getText(_div("mini-cart-pricing["+$i+"]")),"/[$](.*)/",true));
		//verifying the quantity
		//var $qty1=parseInt(_extract(_getText(_span("mini-cart-price",_near(_link($Minicart[$i][0])))),"/[:](.*)/",true));
		var $qty1=parseInt(_getText(_span("/value/",_in(_div("mini-cart-pricing["+$i+"]")))))
		_assertEqual($qty1,$Minicart[$i][5]);
	}

var $expQuantity=$minicartQuantity[0]+$minicartQuantity[1];
var $expSubTotal=$minicartPrice[0]+$minicartPrice[1];
$expSubTotal=round($expSubTotal,2);

var $actSubTotal=parseFloat(_extract(_getText(_div("mini-cart-subtotals")),"/[$](.*)/",true));

_assertEqual($expSubTotal,$actSubTotal);

//verify the quantity in the header
var $QtyInHeader=_getText(_span("minicart-quantity"));
_assertEqual($expQuantity,$QtyInHeader);

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148613", "Verify the UI of mini Cart on home page when multiple products are added to bag in the application both as an anonymous and a registered user");
$t.start();
try
{
//add item to cart
addItemToCart($Minicart[2][0],$Minicart[0][4]);	
addItemToCart($Minicart[3][0],$Minicart[0][4]);	
//verify the UI when multiple products are there
//no.of items present in mini cart
var $totalSectionsInMiniCart=_count("_div","/mini-cart-product/",_in(_div("mini-cart-products")));
//fetching data from mini cart
for(var $i=0;$i<$totalSectionsInMiniCart;$i++)
	{
	//mousehover
	_mouseOver(_link("mini-cart-link"));
	//image
	if($i==0)
		{
	_assertVisible(_image("/(.*)/",_in(_div("mini-cart-image["+$i+"]"))));
		}
	else
		{
		_assertNotVisible(_image("/(.*)/",_in(_div("mini-cart-image["+$i+"]"))));
		}
	//name link
	_assertVisible(_link("/(.*)/",_in(_div("mini-cart-name["+$i+"]"))));
	//attributes
	_assertVisible(_div("mini-cart-attributes["+$i+"]"));
	//quantity and price
	_assertVisible(_div("mini-cart-pricing["+$i+"]"));
	}
_mouseOver(_link("mini-cart-link"));
//slot
_assertVisible(_div("mini-cart-slot"));
//view cart
_assertVisible(_link("VIEW CART"));
_assertVisible(_link("mini-cart-link-checkout"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148614", "Verify the UI of mini Cart on home page when Product description is  expanded  in the application both as an anonymous and a registered user");
$t.start();
try
{
//when product description is in expanded mode
//image should be visible
_assertVisible(_image("/(.*)/",_in(_div("mini-cart-image"))));
//name link
_assertVisible(_link("/(.*)/",_in(_div("mini-cart-name"))));
//attributes
_assertVisible(_div("mini-cart-attributes"));
//quantity and price
_assertVisible(_div("mini-cart-pricing"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148615", "Verify the UI of mini Cart on home page when Product description is  collapsed in the application both as an anonymous and a registered user");
$t.start();
try
{
//when product description is in collapsed mode
//click on toggle
_mouseOver(_link("mini-cart-link"));
_click(_span("/mini-cart-toggle/"));
//image should not be visible
_assertNotVisible(_image("/(.*)/",_in(_div("mini-cart-image"))));
//name link
_assertVisible(_link("/(.*)/",_in(_div("mini-cart-name"))));
//attributes
_assertVisible(_div("mini-cart-attributes"));
//quantity and price
_assertVisible(_div("mini-cart-pricing"));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126806", "Verify the navigation on click of 'product name' on mini cart in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to home page
_click(_image("logo.png"));	
//mousehover
_mouseOver(_link("mini-cart-link"));
//get product name
var $pName=_getText(_link("/(.*)/",_in(_div("mini-cart-name"))));
//click on name link
_click(_link("/(.*)/",_in(_div("mini-cart-name"))));
//verify the navigation

//_assertVisible(_span("last"));
//_assertEqual($pName,_getText(_span("last")));

_assertVisible(_span($pName, _in(_div("breadcrumb"))));

_assertVisible(_heading1("product-name"));
_assertEqual($pName,_getText(_heading1("product-name")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125592", "Verify the navigation on click of 'VIEW Cart' button in mini cart   in the application both as an anonymous and a registered user");
$t.start();
try
{
//mousehover
_mouseOver(_link("mini-cart-link"));
//click on view cart
_click(_link("VIEW CART"));
//cart navigation verification
_assertVisible(_table("cart-table"));
_assertVisible(_div("cart-actions cart-actions-top"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124593", "Verify the functionality of 'Go straight to checkout' link in Mini cart in the application as an anonymous user");
$t.start();
try
{
//mousehover
_mouseOver(_link("mini-cart-link"));	
//click on go strait to checkout
_click(_link("mini-cart-link-checkout"));
//verify the navigation
_assertVisible(_heading2($Minicart[0][2]));
_assertVisible(_submit("dwfrm_login_unregistered")); 
_assertVisible(_heading2("/"+$Minicart[1][2]+"/"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125594", "Verify the functionality of 'Go straight to checkout' link in Mini cart in the application as an registered user");
$t.start();
try
{
//login to the application
_click(_link("Login"));
login();
//mousehover
_mouseOver(_link("mini-cart-link"));	
//click on go strait to checkout
_click(_link("mini-cart-link-checkout"));
//verify the navigation
//_assertVisible(_fieldset("/"+$Minicart[0][3]+"/"));
//_assertVisible(_div("shippingForm"));
if($CheckoutType==$BMConfig[0][1])
	{
	//Should Nav to Shipping page
	_assertVisible(_heading2("Shipping"));
	_assertVisible(_div("checkout-tabs spc-shipping"));
	}
	if($CheckoutType==$BMConfig[1][1])
	{
	//verify the navigation
	_assertVisible(_fieldset("/SELECT OR ENTER SHIPPING ADDRESS /"));
	_assertVisible(_submit("dwfrm_singleshipping_shippingAddress_save"));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();
