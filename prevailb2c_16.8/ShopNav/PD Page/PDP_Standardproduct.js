_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("PDPage.xls");

function verifyNavigation($PName)
{
	_assertVisible(_div("breadcrumb"));
	_assertVisible(_span($PName, _in(_div("breadcrumb"))));
	_assertVisible(_heading1("product-name"));
	_assertEqual($PName, _getText(_heading1("product-name")));	
}


var $PDP_Standard=_readExcelFile("PDPage.xls","PDP_Standard");
var $PDP_Productset=_readExcelFile("PDPage.xls","PDP_Productset");
var $Qty_Validations=_readExcelFile("PDPage.xls","Qty_Validations");
var $RootCatName=$PDP_Standard[0][8];
var $CatName=$PDP_Standard[1][8];

//delete the existing user
//deleteUser();


SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


var $t = _testcase("124731","Verify the UI of Product detail page for variation product in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to variation product
_click(_link($PDP_Standard[0][0]));
var $PName=_getText(_link("name-link"));
//click on name link
_click(_link("name-link"));
//verify the UI of variation product
_assertVisible(_div("breadcrumb"));
_assertVisible(_span($PName, _in(_div("breadcrumb"))));
_assertVisible(_div("product-primary-image"));
_assertVisible(_div("product-next"));
_assertVisible(_heading1("product-name"));
_assertVisible(_div("product-number"));
_assertVisible(_div("product-price"));
//review ratings
_assertVisible(_div("rating"));
_assertVisible(_div("product-review"));
//swatches
selectSwatch();

//if(_isVisible(_span("SELECT COLOR")))
//	{
//	_assertVisible(_listItem("/SELECT COLOR/"));
//	}
//if(_isVisible(_span("SELECT SIZE")))
//	{
//	_assertVisible(_listItem("/SELECT SIZE/"));
//	}
//if(_isVisible(_span("SELECT WIDTH")))
//	{
//	_assertVisible(_listItem("/SELECT WIDTH/"));
//	}
//quantity
_assertVisible(_label("Qty"));
_assertVisible(_textbox("Quantity"));

//price beside add to cart button [Removed a/c to functionality]
//_assertVisible(_div("product-price",_in(_fieldset("ADD TO CART OPTIONS"))));

//alternate images
if(_isVisible(_div("product-thumbnails")))
	{
	//_assertVisible(_heading2($PDP_Standard[0][1]));
	_assertVisible(_div("thumbnails"));
	}
else
	{
	_assert("alternate images are not present");
	}
//recommendations section
if(_isVisible(_div("recommendations cross-sell")))
	{
	_assertVisible(_label($PDP_Standard[0][2]));
	_assertVisible(_div("carousel-recommendations"));
	}

//selecting swatches
selectSwatch();
//verify the presence of add to cart
_assertVisible(_submit("add-to-cart"));
//add to wish list
_assertVisible(_link("Add to Wishlist"));
//add to gift registry
_assertVisible(_link("Add to Gift Registry"));
//description,reviews table
_assertVisible(_div("product-info"));
//_assertVisible(_div("product-tabs ui-tabs ui-widget ui-widget-content ui-corner-all"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148598","Verify the UI of Product detail page for Standard product in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to standard product
_click(_link($CatName));
var $PName=_getText(_link("name-link"));
_click(_link("name-link"));
//verify the UI
_assertVisible(_div("breadcrumb"));
_assertVisible(_span($PName, _in(_div("breadcrumb"))));
_assertVisible(_div("product-primary-image"));
_assertVisible(_heading1("product-name"));
_assertVisible(_div("product-number"));
_assertVisible(_div("product-price"));
//next and previous links
_assertVisible(_div("product-next"));
//review ratings
_assertVisible(_div("rating"));
_assertVisible(_div("product-review"));
//availability
_assertVisible(_label("Availability"));
_assertVisible(_div("availability-web"));
//swatches shouldn't be visible
_assertNotVisible(_listItem("/SELECT COLOR/"));
_assertNotVisible(_listItem("/SELECT SIZE/"));
_assertNotVisible(_listItem("/SELECT WIDTH/"));
//quantity
_assertVisible(_label("Qty"))
_assertVisible(_textbox("Quantity"));

//price beside add to cart button [Removed]
//_assertVisible(_div("product-price",_in(_fieldset("ADD TO CART OPTIONS"))));

//verify the presence of add to cart
_assertVisible(_submit("add-to-cart"));
//add to wish list
_assertVisible(_link("Add to Wishlist"));
//add to gift registry
_assertVisible(_link("Add to Gift Registry"));
//description,reviews table
_assertVisible(_div("product-info"));
//_assertVisible(_div("product-tabs ui-tabs ui-widget ui-widget-content ui-corner-all"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124733","Verify the functionality of breadcrumbs on product detail page in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to root category	
_click(_link($PDP_Standard[0][4]));
//navigate to sub category
_click(_link($PDP_Standard[0][0]));
//fetch the bread crumb
var $subCatBreadcrumb=_getText(_div("breadcrumb"));
var $PName=_getText(_link("name-link"));
if(_isIE9())
{
var $breadCrumb=$subCatBreadcrumb+$PName;
}
else
{
var $breadCrumb=$subCatBreadcrumb+" "+$PName;
}
//click on name link
_click(_link("name-link"));
//verify the presented breadcrumb
_assertEqual($breadCrumb,_getText(_div("breadcrumb")));
_assertVisible(_span($PName, _in(_div("breadcrumb"))));
//click on sub category link present in bread crumb
_click(_link($PDP_Standard[0][0],_in(_div("breadcrumb"))));
//verify the navigation
_assertEqual($subCatBreadcrumb,_getText(_div("breadcrumb")));
//navigate to PDP
_click(_link("name-link"));
//click on root category link present in the bread crumb
_click(_link($PDP_Standard[1][4],_in(_div("breadcrumb"))));
//Bread crumb should not visible for root category
_assertNotVisible(_div("breadcrumb"));

////verify the navigation
//_assertEqual($rootCatBreadcrumb,_getText(_div("breadcrumb")));
////navigate to PDP
//_click(_link($PDP_Standard[0][0]));
//_click(_link("name-link"));
////click on home link
//_click(_link("Home",_in(_div("breadcrumb"))));
////verify the navigation
//_assertVisible(_list("homepage-slides"));
//_assertVisible(_div("home-bottom-slots"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124734","Verify the navigation links previous and next on product detail page  in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to sub category
_click(_link($PDP_Standard[0][0]));	
var $prodNames = new Array();
var $numberOfProducts = _count("_div", "/product-name/", "true");
$prodNames = _collectAttributes("_div", "/product-name/", "sahiText");
$name=_getText(_link("name-link"))
//navigate to PD page on click of name link
_click(_link("name-link"));

  for ($i=0; $i<$numberOfProducts-1; $i++)
  {
    var $name = _getText(_heading1("product-name"));
    _assertEqual($prodNames[$i],$name);
    _click(_link("/(.*)/", _in(_div("product-next"))));
    //verify the presence of previous link
    if($i!=$numberOfProducts-2)
    	{
    		_assertVisible(_link("/(.*)/", _in(_div("product-previous divided"))));
    	}
  }
  for($i=$numberOfProducts-2; $i>0; $i--)
  {
	  if($i==$numberOfProducts-2)
		{
		_click(_link("/(.*)/", _in(_div("product-previous "))));
	  	}
	else
		{
		_click(_link("/(.*)/", _in(_div("product-previous divided"))));
		}
	//verify the presence of next link
	    if($i!=$numberOfProducts-2)
    	{
	    	_assertVisible(_link("/(.*)/", _in(_div("product-next"))));
    	}
    var $name = _getText(_heading1("product-name"));
    _assertEqual($prodNames[$i],$name);
  }	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//navigate to variation product
_click(_link($PDP_Standard[0][0]));
var $PName=_getText(_link("name-link"));
//click on name link
_click(_link("name-link"));

var $t = _testcase("124736","Verify the functionality of attribute 'select color' on product detail page in the application both as an anonymous and a registered user");
$t.start();
try
{
//verify the select colour functionality 	
_assertVisible(_list("swatches Color"));
var $colors = _count("_link","/swatchanchor/",_in(_list("swatches Color")));
var $k=1;
//only one swatchanchor is there
if($colors==1)
{
	if(_isVisible(_listItem("selectable selected", _in(_list("swatches Color")))))
		  {
		  var $title=_getAttribute(_link("swatchanchor", _in(_list("swatches Color"))),"title").split(" ")[2];
		  //verify whether selected value is displaying or not
		  _assertVisible(_listItem("selected-value", _in(_list("swatches Color"))));
		  _assertEqual($title, _getText(_listItem("selected-value", _in(_list("swatches Color")))));
		  }
	else
		  {
		  //click on swatchanchor
		  _click(_link("swatchanchor", _in(_list("swatches Color"))));
		  var $title=_getAttribute(_link("swatchanchor", _in(_list("swatches Color"))),"title");
		  //verify whether selected value is displaying or not
		  _assertVisible(_listItem("selected-value", _in(_list("swatches Color"))));
		  _assertEqual($title, _getText(_listItem("selected-value", _in(_list("swatches Color")))));
		  }
}
//more than one swatchanchor is there
else
{
 for(var $i=0;$i<$colors;$i++)
	{
		if($i==0 || $i==1)
			{
			var $a=_getText(_listItem("available", _in(_list("swatches Color"))));
			_mouseOver(_link($a, _in(_list("swatches Color"))));
			var $b=_getAttribute(_link($a), "title");
			_assertEqual($a,$b);
			_click(_link($a, _in(_list("swatches Color"))));
			var $act=_getText(_listItem("selected-value", _in(_list("swatches Color"))));
			_assertEqual($a,$act);
			}
		else
			{
			var $a=_getText(_listItem("available["+$k+"]", _in(_list("swatches Color"))));
			_mouseOver(_link($a, _in(_list("swatches Color"))));
			var $b=_getAttribute(_link($a), "title");
			_assertEqual($a,$b);
			_click(_link($a, _in(_list("swatches Color"))));
			var $act=_getText(_listItem("selected-value", _in(_list("swatches Color"))));
			_assertEqual($a,$act);
			$k++;
			}
		  }
	_assert(true, "Selected color is appropriate");
	}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124737","Verify the functionality of attribute 'Select size' on product detail page in the application both as an anonymous and a registered user");
$t.start();
try
{
_assertVisible(_list("swatches size"));
var $sizes = _count("_link","/swatchanchor/",_in(_list("swatches size")));
var $j=1;
//only one swatchanchor is there
if($sizes==1)
{
	if(_isVisible(_listItem("selectable selected",_in(_list("swatches size")))))
		  {
		  var $title=_getAttribute(_link("swatchanchor",_in(_list("swatches size"))),"title").split(" ")[2];
		  //verify whether selected value is displaying or not
		  _assertVisible(_listItem("selected-value",_in(_list("swatches size"))));
		  _assertEqual($title, _getText(_listItem("selected-value",_in(_list("swatches size")))));
		  }
	else
		  {
		  //click on swatch anchor
		  _click(_link("swatchanchor",_in(_list("swatches size"))));
		  var $title=_getAttribute(_link("swatchanchor"),"title");
		  //verify whether selected value is displaying or not
		  _assertVisible(_listItem("selected-value"));
		  _assertEqual($title, _getText(_listItem("selected-value")));
		  }
}
//more than one swatchanchor is there
else
  {
for (var $i=0; $i<$sizes; $i++)
    {
	  if($i==0 || $i==1)
		{
		var $a=_getText(_listItem("selectable", _in(_list("swatches size"))));
		_mouseOver(_link($a, _in(_list("swatches size"))));
		var $b=_getAttribute(_link($a), "title").split(" ")[2];
		_assertEqual($a,$b);
		_click(_link($a, _in(_list("swatches size"))));
		var $act=_getText(_listItem("selected-value", _in(_list("swatches size"))));
		_assertEqual($a,$act);
		}
	else
		{
		var $a=_getText(_listItem("selectable["+$j+"]", _in(_list("swatches size"))));
		_mouseOver(_link($a, _in(_list("swatches size"))));
		var $b=_getAttribute(_link($a), "title").split(" ")[2];
		_assertEqual($a,$b);
		_click(_link($a, _in(_list("swatches size"))));
		var $act=_getText(_listItem("selected-value", _in(_list("swatches size"))));
		_assertEqual($a,$act);
		$j++;
		}	
	_assert(true, "Selected size is appropriate");	
	}
  }     	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124743/124749","Verify the functionality of 'quantity box'/Add to cart on product detail page for in the application both as an anonymous and a registered user");
$t.start();
try
{
//select swatches
selectSwatch();
//enter quantity 5 in quantity box
_setValue(_textbox("Quantity"),$PDP_Standard[0][5]);
//verify whether entered value is present in quantity box or not
_assertEqual($PDP_Standard[0][5], _getValue(_textbox("Quantity")));
//Add to cart
_click(_submit("Add to Cart"));
//fetch quntity from mini cart
var $cartQuantity=_getText(_span("minicart-quantity"));
//verify the quantity in mini cart
_assertEqual($PDP_Standard[0][5],$cartQuantity);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

ClearCartItems();
var $t = _testcase("124753/124746","Verify the validation for 'Add to cart' button on product detail page  in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to sub category
_click(_link($PDP_Standard[0][0]));
//click on name link
_click(_link("name-link"));
//when no variants are selected verify the add to cart button
_assertNotVisible(_submit("add-to-cart"));
//select the variants
selectSwatch();
//now verify the add to cart dispaly
_assertVisible(_submit("add-to-cart"));
//quantity box validation
var $cartQuantity=0;
for(var $j=0;$j<$Qty_Validations.length;$j++)
	{
		if($j==0)
			{
				_setValue(_textbox("Quantity"),"");
			}
		else
			{
				_setValue(_textbox("Quantity"),$Qty_Validations[$j][1]);
			}
		if($j==0)
			{
				_click(_submit("Add to Cart"));
				_wait(2000);
				var $actQuantity=_getText(_span("minicart-quantity"));
				//verifying the quantity
				_assertEqual("1",$actQuantity);
			}
		//out of stock
		if($j==3)
			{
				//not available message
				_assertVisible(_paragraph("not-available-msg"));
				_assertEqual($Qty_Validations[3][2], _getText(_paragraph("not-available-msg")));
				//add to cart should disable
				_assertNotVisible(_submit("button-fancy-medium sub-product-item add-to-cart"));
			}
		else
			{						
				var $cartQuantity=_getText(_span("minicart-quantity"));
				_log($cartQuantity);
				//click on add to cart button
				_click(_submit("Add to Cart"));
				//default quantity should add to cart
				var $expQuantity=parseFloat($cartQuantity)+parseInt($Qty_Validations[0][2]);
				var $actQuantity=_getText(_span("minicart-quantity"));
				//verifying the quantity
				_assertEqual($expQuantity,$actQuantity);
			}		
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124789","Verify the functionality of You may also like(Alternate products) on Product detail page for standard product in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to PDP
search($PDP_Standard[1][2]);
var $Product=_getText(_link("name-link"));
if(_isVisible(_link("name-link", _in(_div("product-name", _in(_div("product-tile")))))))
	{
	_click(_link($Product, _in(_div("product-name", _in(_div("product-tile"))))));
	}
//verify the presence of You may also like Section
if(_isVisible(_div("recommendations cross-sell")))
	{ 
	var $count=_count("_div","/product-tile tooltip/",_in(_div("recommendations cross-sell")));
	for(var $i=0;$i<$count;$i++)
		{
		//get the product name
		var $PName=_getText(_div("product-name", _in(_div("product-tile tooltip["+$i+"]"))));
		//click on product image
		_click(_image("/(.*)/",_in(_div("product-tile tooltip["+$i+"]"))));
		//verify the navigation
		verifyNavigation($PName);
		//navigate to PDP
		search($PDP_Standard[1][2]);
		//click on product name link in search page
		if(_isVisible(_link("name-link", _in(_div("product-name", _in(_div("product-tile")))))))
			{
			_click(_link($Product, _in(_div("product-name", _in(_div("product-tile"))))));
			}
		//click on product name link in PDP
		_click(_link("/(.*)/",_in(_div("product-name", _in(_div("product-tile tooltip["+$i+"]"))))));
		//verify the navigation
		verifyNavigation($PName);
		//navigate to PDP
		search($PDP_Standard[1][2]);
		if(_isVisible(_link("name-link", _in(_div("product-name", _in(_div("product-tile")))))))
		{
		_click(_link($Product, _in(_div("product-name", _in(_div("product-tile"))))));
		}
		}
	//verify the carosal
	if($count>3)
		{
		_assertVisible(_div("jcarousel-next jcarousel-next-horizontal"));
		//click on next link
		_click(_div("jcarousel-next jcarousel-next-horizontal"));
		//previous should display
		_assertVisible(_div("jcarousel-prev jcarousel-prev-horizontal"));
		_click(_div("jcarousel-prev jcarousel-prev-horizontal"));
		//next link should display
		_assertVisible(_div("jcarousel-next jcarousel-next-horizontal"));
		}
	}
else
	{
	_assert(false,"recomendation section is not present for this product");
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124791", "Verify the UI of product tabs on product detail page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to PDP
	search($PDP_Standard[1][2]);	
	_click(_link("name-link"));		
	//verify the UI of product tab
	_assertVisible(_label("Description"));
	_assertVisible(_label("Product Details"));
	_assertVisible(_label("Reviews"));
	//Print button Has been Removed
	//_assertVisible(_submit("Print"));
	_assertVisible(_div("product-info"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124793", "Verify the Functionaity of product tabs on product detail page in the application both as an anonymous and a registered user");
$t.start();
try
{
	
	//navigate to PDP
	search($PDP_Standard[1][2]);	
	_click(_link("name-link"));	
	
	var $BottomSection=_collectAttributes("_label","tab-label","sahiText",_in(_div("tabs")));
	
	for(var $i=0;$i<$BottomSection.length;$i++)
		{
		_click(_label($BottomSection[$i]));
		_assertVisible(_div("/tab-content/", _in(_div("tabs"))));
		_assertEqual($BottomSection[$i],_getText(_label("tab-label["+$i+"]", _in(_div("tabs")))))
		}
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124739", "Verify the functionality of 'Size chart' link on product detail page for in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to womens category tops subcategory
	_click(_link($PDP_Standard[2][0]));
	//click on name link
	_click(_link("name-link"));
	//verify the presence of size chart
	_assertVisible(_link("Size Chart"));
	_click(_link("Size Chart"));
	_wait(2000, _isVisible(_div("ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable")));
	if (_isVisible(_div("sizinginformation ui-tabs ui-widget ui-widget-content ui-corner-all")))       
	     {
	       _assert(true, "Size chart dialogue is opened successfully");
	     }
	   else  
		 {
		   _assert(false, "Size chart window is not opened");
		 }
	_assertVisible(_button("Close"));
	_click(_button("Close"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


cleanup();
//delete the existing user
deleteUser();
