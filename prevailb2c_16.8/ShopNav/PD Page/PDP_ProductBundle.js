_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("PDPage.xls");

var $PDP_ProductBundle=_readExcelFile("PDPage.xls","PDP_ProductBundle");
var $PDP_Productset=_readExcelFile("PDPage.xls","PDP_Productset");
var $PDP_Standard=_readExcelFile("PDPage.xls","PDP_Standard");
var $Qty_Validations=_readExcelFile("PDPage.xls","Qty_Validations");

//delete the existing user
deleteUser();

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


var $t = _testcase("124841","Verify the UI of Product detail page for bundled products in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to bundle product page
//category
_click(_link($PDP_ProductBundle[0][3]));
//sub category
_click(_link($PDP_ProductBundle[0][4]));
//product name
_click(_link($PDP_ProductBundle[0][0]));
//verify the UI of bundle product
//image
_assertVisible(_div("product-primary-image"));
//name
_assertVisible(_heading1("product-name"));
_assertEqual($PDP_ProductBundle[0][0], _getText(_heading1("product-name")));
//number
_assertVisible(_div("product-number"));
//next and previous
_assertVisible(_div("product-nav-container"));
//price
_assertVisible(_span("price-sales"));
//fetch the total count present in the bundle
var $totalProductsInSet=_count("_div","/product-set-item product-bundle-item/",_in(_div("product-set-list")));
for(var $i=0;$i<$totalProductsInSet;$i++)
	{
	//image
	_assertVisible(_div("product-set-image", _in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//name
	_assertVisible(_link("item-name",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//number
	_assertVisible(_div("product-number",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//availability
	_assertVisible(_div("availability-msg",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//quantity
	_assertVisible(_span($PDP_ProductBundle[0][1],_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	_assertVisible(_span("value",_near(_span("QUANTITY:",_in(_div("product-set-item product-bundle-item["+$i+"]"))))));
	//product options
	if(_isVisible(_div("product-options["+$i+"]")))
		{
		_assertVisible(_div("product-options["+$i+"]"));
		}
	}
//alternate images
if(_isVisible(_div("product-thumbnails")))
	{
	//_assertVisible(_heading2($PDP_Standard[0][1]));
	_assertVisible(_div("thumbnails"));
	}
else
	{
	_assert("alternate images are not present");
	}

//add to wish list
_assertVisible(_link("Add to Wishlist"));
//add to gift registry
_assertVisible(_link("Add to Gift Registry"));
//price at bottom
_assertVisible(_span("price-sales",_near(_submit("button-fancy-large add-to-cart bundle"))));
//add all to cart
_assertVisible(_submit("button-fancy-large add-to-cart bundle"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124846","Verify the functionality of breadcrumbs on product detail page for bundled products in the application both as an anonymous and a registered user");
$t.start();
try
{
//click on sub sub category present in breadcrumb
_click(_link($PDP_ProductBundle[0][4],_in(_div("breadcrumb"))));
//verify the navigation
_assertVisible(_link($PDP_ProductBundle[0][4],_in(_div("breadcrumb"))));
//navigate to bundle product page
search($PDP_ProductBundle[0][0]);
//click on sub category present in breadcrumb
_click(_link($PDP_ProductBundle[0][3],_in(_div("breadcrumb"))));
//verify the navigation
_assertVisible(_link($PDP_ProductBundle[0][3],_in(_div("breadcrumb"))));
//navigate to bundle product page
search($PDP_ProductBundle[0][0]);
//click on category present in breadcrumb
_click($PDP_ProductBundle[0][2],_in(_div("breadcrumb")));
//verify the navigation
_assertVisible(_link($PDP_ProductBundle[0][2],_in(_div("breadcrumb"))));
//navigate to bundle product page
search($PDP_ProductBundle[0][0]);
//click on home link
_assertNotVisible(_link("Home",_in(_div("breadcrumb"))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124848","Verify the links previous and next on product detail page For bundled products in the application both as an anonymous and a registered user");
$t.start();
try
{
//category
_click(_link($PDP_ProductBundle[0][3]));
//sub category
_click(_link($PDP_ProductBundle[0][4]));
//collect all the product names
var $prodNames = new Array();
var $numberOfProducts = _count("_div", "/product-name/", "true");
$prodNames = _collectAttributes("_div", "/product-name/", "sahiText");
//product name
_click(_link("name-link"));
for ($i=0; $i<$numberOfProducts-1; $i++)
{
  var $name = _getText(_heading1("product-name"));
  _assertEqual($prodNames[$i],$name);
  _click(_link("/(.*)/", _in(_div("product-next"))));
  //verify the presence of previous link
  if($i!=$numberOfProducts-2)
		{
		  	_assertVisible(_link("/(.*)/", _in(_div("product-previous divided"))));
		}
}
for($i=$numberOfProducts-2; $i>0; $i--)
{
	if($i==$numberOfProducts-2)
		{
		_click(_link("/(.*)/", _in(_div("product-previous "))));
	  	}
	else
		{
		_click(_link("/(.*)/", _in(_div("product-previous divided"))));
		}
	
	//verify the presence of next link
	if($i!=$numberOfProducts-2)
	  	{
			_assertVisible(_link("/(.*)/", _in(_div("product-next"))));
	  	}
  var $name = _getText(_heading1("product-name"));
  _assertEqual($prodNames[$i],$name);
}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124851","Verify the functionality of product names  on  Product detail page for all products in the bundle in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to bundle product page
search($PDP_ProductBundle[0][0]);	
var $totalProductsInSet=_count("_div","/product-set-item product-bundle-item/",_in(_div("product-set-list")));	
for(var $i=0;$i<$totalProductsInSet;$i++)
	{	
	//fetch the product name
	var $pName=_getText(_link("item-name["+$i+"]"));
	//click on name link
	_click(_link("item-name["+$i+"]"));
	//verify the navigation
	_assertVisible(_span($pName,_in(_div("breadcrumb"))));
	_assertVisible(_heading1("product-name"));
	_assertEqual($pName, _getText(_heading1("product-name")));
	//navigate to bundle product page
	search($PDP_ProductBundle[0][0]);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124853","Verify the functionality of 'Extended warranty' drop dowm on product Detail page for all products in the bundle in the application both as an anonymous and a registered user");
$t.start();
try
{
//verify the presence of warranty drop down	
_assertVisible(_select("dwopt_sony-ps3-bundle_consoleWarranty"));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

ClearCartItems();
var $t = _testcase("124856","Verify the functionality of 'Add to cart' button on product detail  Page for all products in the set in the application both as an anonymous and a registered user");
$t.start();
try
{
	
//navigate to bundle product page
search($PDP_ProductBundle[0][0]);
//fetch product name 
var $PName=_getText(_heading1("product-name"));
var $productsInBundle=new Array();
var $totalProductsInSet=_count("_div","/product-set-item/",_in(_div("product-set-list")));	
for(var $i=0;$i<$totalProductsInSet;$i++)
	{	
	$productsInBundle[$i]=_getText(_link("item-name["+$i+"]"));
	}
//var $cartQuantity=_getText(_span("minicart-quantity"));
//click on add to cart
_click(_submit("button-fancy-large add-to-cart bundle"));
//verify the cart quantity
var $expQuantity=$Qty_Validations[0][2];
var $actQuantity=parseInt(_getText(_span("minicart-quantity")));
_assertEqual($expQuantity,$actQuantity);
//verify weather same products are added or not
_click(_link("View Cart"));
_assertEqual($PName,_getText(_link("/.*/",_in(_div("name")))));
var $actBundleproducts=_count("_row","/rowbundle/",_in(_table("cart-table")));
var $j=0;
for(var $i=1;$i<=$actBundleproducts;$i++)
	{
	_assertEqual($productsInBundle[$j],_getText(_link("/.*/",_in(_div("name["+$i+"]")))));
	$j++;
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124858","Verify the link 'Add to wishlist' on product detail page for bundled products in the application both as an anonymous and a registered user");
$t.start();
try
{
//click on add to wishlist link
_click(_link("Add to Wishlist"));
//verify the navigation
_assertVisible(_link($PDP_ProductBundle[0][5],_in(_div("breadcrumb"))));
_assertVisible(_heading1($PDP_ProductBundle[1][5]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124859","Verify the link 'Add to gift registry' on product detail page for bundled products in the application both as an anonymous and a registered user");
$t.start();
try
{
//navigate to bundle product page
search($PDP_ProductBundle[0][0]);	
//click on link gift registry
_click(_link("Add to Gift Registry"));
//verify the navigation
_assertVisible(_link($PDP_ProductBundle[0][6],_in(_div("breadcrumb"))));
_assertVisible(_heading1($PDP_ProductBundle[1][6]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();