_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Category_Data.xls");
var $Generic=_readExcelFile("Category_Data.xls", "Generic");
var $Quantity=_readExcelFile("Category_Data.xls", "Quantity");
var $SendToFriend=_readExcelFile("Category_Data.xls", "SendToFriend");
var $RootCat=$Generic[2][0];
//from util.GenericLibrary/BrowserSpecific.js
var $FirstName=$FName;
var $LastName=$LName;
var $UserEmail=$uId;
SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();

var $RootCatName=$Generic[0][0];

var $t = _testcase("124286", "Verify the UI of the root category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateRootCatPage();
	//category refinement header
	_assertVisible(_span("refinement-header"));
	_assertEqual("Shop "+$RootCatName, _getText(_span("refinement-header")));
	//Category refinement section
	_assertVisible(_list("category-level-1"));
	//Category banner
	_assertVisible(_heading1($RootCatName));
	_assertVisible(_div("catlanding-banner"));
	//SubCategory content slots
	_assertVisible(_div("secondary-content"));
	_assertVisible(_div("category-tile"));
	_assertVisible(_div("category-tile[1]"));
	_assertVisible(_div("category-tile[2]"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124288/148574", "Verify the display and functionality of category links under heading 'Shop <root category name>' in left nav  in the application both as an anonymous and a registered user");
$t.start();
try
{	
	_setAccessorIgnoreCase(true);
	//navigation of sub-category link.
	var $categories=_collectAttributes("_listItem","/(.*)/","sahiText",_in(_div("refinement  category-refinement")));
	for($i=0;$i<$categories.length;$i++)
	{
	_click(_link($categories[$i], _in(_div("refinement  category-refinement"))));
	_assertContainsText($categories[$i], _div("breadcrumb"));
	_assertEqual($categories[$i], _getText(_link("refinement-link  active", _in(_div("refinement  category-refinement")))));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


cleanup();

function navigateRootCatPage()
{
_click(_link($RootCat));
_log("We are now on " + $RootCatName + " landing page");
_wait(2000);
}
