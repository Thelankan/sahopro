_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Category_Data.xls");
var $Generic=_readExcelFile("Category_Data.xls", "Generic");
var $Quantity=_readExcelFile("Category_Data.xls", "Quantity");
var $SendToFriend=_readExcelFile("Category_Data.xls", "SendToFriend");
//from util.GenericLibrary/BrowserSpecific.js
var $FirstName=$FName;
var $LastName=$LName;
var $UserEmail=$uId;
SiteURLs();
var $RootCatName=$Generic[0][10];
var $CatName=$Generic[1][10];
var $SubCatName=$Generic[2][10];
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("125646", "Verify the UI of third category grid page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	_assertContainsText($SubCatName, _div("breadcrumb"));
	//hero slot
	_assertVisible(_div("content-slot slot-grid-header"));
	//sort by
	_assertVisible(_select("grid-sort-header"));
	_assertVisible(_select("grid-sort-footer"));
	if(!isMobile())
		{
		//refinement header
		_assertVisible(_span("refinement-header"));
		//left nav
		_assertVisible(_div("refinements"));
//		//items per page
//		_assertVisible(_select("grid-paging-header"));
//		_assertVisible(_select("grid-paging-footer"));
		}
	else
		{
		//left nav
		_assertVisible(_div("navigation"));		
		}
	//Pagination
//	_assertVisible(_div("pagination"));
//	_assertVisible(_div("pagination", _under(_list("search-result-items"))));
	//result hits
	_assertVisible(_div("results-hits"));
	_assertVisible(_div("results-hits", _under(_list("search-result-items"))));
	//Items displayed
	_assertVisible(_list("search-result-items"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("125634", "Verify the display of breadcrumb and its functionality in the third category grid page  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	_assertContainsText($SubCatName, _div("breadcrumb"));
	var $expBreadCrumb=$RootCatName+" "+$CatName+" "+$SubCatName;
	/*if(_isIE())
		{
		_assertEqual($expBreadCrumb.replace(/\s/g,""), _getText(_div("breadcrumb")).replace(/\s/g,""));
		}
	else
		{
		_assertEqual($expBreadCrumb, _getText(_div("breadcrumb")));
		}	*/
	//navigate to root category page
	_click(_link($RootCatName,_in(_div("breadcrumb"))));
	var $expBreadCrumb1=$RootCatName;
	if(_isIE())
	{
	_assertEqual($expBreadCrumb1.replace(/\s/g,""), _getText(_div("breadcrumb")).replace(/\s/g,""));
	}
	else
	{
	_assertNotVisible(_div("breadcrumb"));
	//Bread crumb is not visible in category landing page 
	//_assertEqual($expBreadCrumb1, _getText(_div("breadcrumb")));
	}
	
	navigateSubCatPage();
	//navigate to category page
	_click(_link($CatName,_in(_div("breadcrumb"))));
	var $expBreadCrumb2=$RootCatName+" "+$CatName;
	if(_isIE())
	{
	_assertEqual($expBreadCrumb2.replace(/\s/g,""), _getText(_div("breadcrumb")).replace(/\s/g,""));
	}
	else
	{
	_assertEqual($expBreadCrumb2, _getText(_div("breadcrumb")));
	}
	//Validating navigation from category page to home page
	navigateSubCatPage();
	_assertNotVisible(_link("Home",_in(_div("breadcrumb"))));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125637", "Verify the display of category links under heading Shop <cat name> in left nav in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	_setAccessorIgnoreCase(true);
	//the display of category names
	var $ExpectedCategory=$CatName;
	_assertVisible(_listItem("expandable fa fa-angle-right active fa-angle-down"));
	if(_isIE())
		{
			_assertEqual($ExpectedCategory.replace(/\s/g,""), _getText(_link("refinement-link  active")).replace(/\s/g,""));
		}
		else
		{
			_assertEqual($ExpectedCategory, _getText(_link("refinement-link  active")));
		}	
	//navigation of sub-category link.
	var $subcategories=_collectAttributes("_link","/refinement-link/","sahiText",_in(_list("category-level-2")));
	for($i=1;$i<$subcategories.length;$i++)
	{
		_click(_link($subcategories[$i], _in(_list("category-level-2"))));
		_assertContainsText($subcategories[$i], _div("breadcrumb"));
		_assertEqual($subcategories[$i], _getText(_link("refinement-link  active", _in(_list("category-level-2")))));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125638", "Verify the functionality after selecting different types of filters in third category grid page  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	navigateSubCatPage();
	var $Filters=_collectAttributes("_heading3","/toggle/","sahiText",_in(_div("refinements")));
	var $ExpectedApplied= new Array();
	for(var $i=0;$i<$Filters.length;$i++)
	{
		if(_isVisible(_div("refinement "+$Filters[$i])))
			{
			
			if ($Filters[$i]=="Category")
				{
				var $FilterOptions=_collectAttributes("_link","/(.*)/","sahiText",_in(_list("/(.*)/", _in(_div("/refinement  category-refinement/")))));
				_click(_link($FilterOptions[0]));	
				$ExpectedApplied.push($FilterOptions[0]+" x");
				}
			else if ($Filters[$i]=="Color")
				{
				var $FilterOptions=_collectAttributes("_link","/(.*)/","sahiText",_in(_list("/(.*)/", _in(_div("/refinement refinementColor/")))));
				_click(_link($FilterOptions[0]));	
				$ExpectedApplied.push($FilterOptions[0]+" x");
				}
			else if ($Filters[$i]=="Price")
				{
				var $FilterOptions=_collectAttributes("_link","/(.*)/","sahiText",_in(_list("/(.*)/", _in(_div("refinement ")))));
				_click(_link($FilterOptions[0]));	
				$ExpectedApplied.push($FilterOptions[0]+" x");
				}
			else
				{
				var $FilterOptions=_collectAttributes("_link","/(.*)/","sahiText",_in(_list("/(.*)/", _in(_div("/refinement isNew/")))));
				_click(_link($FilterOptions[0]));	
				$ExpectedApplied.push($FilterOptions[0]+" x");
				}
			}		
	}
	var $ActualApplied=_collectAttributes("_span","/breadcrumb-refinement-value/","sahiText",_in(_div("breadcrumb")));
	_assertEqual($ExpectedApplied.sort(),$ActualApplied.sort());
	_click(_image("logo.png"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125639", "Verify the functionality of NEW ARRIVAL filter in the left nav of third category grid page  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	navigateSubCatPage();
	if(_isVisible(_heading3("NEW ARRIVAL")))
	  {
		_click(_link("Refine by:true"));
		_assertVisible(_link("Clear"));
		_assertVisible(_span("breadcrumb-refinement-value"));
	    _click(_link("Clear"));
	    _assertNotVisible(_link("Clear"));
	    _assertNotVisible(_span("breadcrumb-refinement-value"));
	  }
	  else
	  {
	    _log("No New Arrival Refinement available");
	  }
	_click(_image("logo.png"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125640", "Verify the functionality of COLOR filter in the left nav of third category grid page  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	navigateSubCatPage();
	var $colors = _collectAttributes("_link","/(.*)/","sahiText",_in(_list("clearfix swatches refinementColor")));
	var $unSelectable=_collectAttributes("_listItem","/unselectable swatch/","sahiText",_in(_list("clearfix swatches refinementColor")));
	for(var $k=0; $k<$colors.length; $k++)
	{
		if($unSelectable.indexOf($colors[$k])>=0)
			{
			_assert(true, "Swatch is unselectable");
			}
		else
			{
			 _click(_link($colors[$k]));
			  //clear link
			  _assertVisible(_link("Clear", _in(_div("refinement refinementColor"))));
			  _assertEqual($colors[$k]+" x",_getText(_span("breadcrumb-refinement-value")));
			 _click(_link("breadcrumb-relax"));
			 _wait(4000,!_isVisible(_link("breadcrumb-relax")));
			}  
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}		
$t.end();

var $t = _testcase("125641", "Verify the functionality of SIZE filter in the left nav of third category grid page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	var $Size = _collectAttributes("_link","/(.*)/","sahiText",_in(_div("refinement Size")));
	var $SizeunSelectable=_collectAttributes("_listItem","/unselectable swatch/","sahiText",_in(_div("refinement Size")));
	for ($k=0; $k< $Size.length; $k++)
	{
			if($unSelectable.indexOf($colors[$k])>=0)
				{
				_assert(true, "Swatch is unselectable");
				}
			else
				{
				_click(_link("swatch-"+$Size[$k]));
				_assertEqual($Size[$k]+" x", _getText(_span("breadcrumb-refinement-value"))); 	
				_click(_link("breadcrumb-relax"));
				 _wait(4000,!_isVisible(_link("breadcrumb-relax")));
				}  
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125643", "Verify the system's response on selection on Price filter from left nav  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	var $PriceRange = _collectAttributes("_link","/(.*)/","sahiText",_in(_div("refinement ")));
	for ($k=0; $k< $PriceRange.length; $k++)
	{
	_click(_link($PriceRange[$k]));
	_assertEqual($PriceRange[$k]+" x", _getText(_span("breadcrumb-refinement-value"))); 	
	priceCheck($PriceRange[$k]);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
/*
var $t = _testcase("125644", "Verify the pagination in category landing page in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	pagination();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/
var $t = _testcase("125647/125648", "Verify the display of products in third category grid page and details displayed for each item in category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	var $productCount = _collect("_div", "/product-tile/", _in(_list("search-result-items")));
	for ($i=0 ; $i<$productCount.length; $i++)
	 {
		 var $prod = _getText(_div("product-tile["+$i+"]"));
		 _assertVisible(_link("thumb-link"));
		 _assertVisible(_div("product-name"));
		 if(_isVisible(_div("Test_revert-calloutmessage")))
		   {
		      _assert(_isVisible(_div("Test_revert-calloutmessage")));
		   }
		  else
		  {
		    _log("No promo message to display");
		  }
		if(_isVisible(_list("swatch-list")))
			{
			_assert(_isVisible(_list("swatch-list")));
			}
		else
			{
			  _log("No alternate swatches available to display");
			}
	_log($prod);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125649", "Verify the system's response on click of 'list view icon' (or) grid view icon beside pagination  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	_assertVisible(_div("search-result-content"));
	_click(_span("toggle-grid"));
	_assertVisible(_byClassName("search-result-content wide-tiles", "DIV"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125650", "Verify the display and functionality of Sort dropdown  in third category grid page in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	if(_isVisible(_select("grid-sort-header")) && _isVisible(_select("grid-sort-footer")))
	{
		var $sort=_getText(_select("grid-sort-header"));
		var $j=0;
		for(var $i=1;$i<$sort.length;$i++)
		{
			_setSelected(_select("grid-sort-header"), $sort[$i]);
			//Validating the selection
			_assertEqual($sort[$i],_getSelectedText(_select("grid-sort-header")),"Selected option is displaying properly");
			_assertEqual($sort[$i],_getSelectedText(_select("grid-sort-footer")),"Selected option is displaying properly");
			if($sort.indexOf($Generic[$j][1])>-1)
			{
				_setSelected(_select("grid-sort-header"), $Generic[$j][1]);
				sortBy($Generic[$j][1]);
				$j++;
			}
		}
	}
	else
	{
		_assert(false, "sort by drop down is not present");
	}
	_click(_image("logo.png"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


if(!isMobile())
{
var $t=_testcase("125859/125860","Verify the functionality of 'Compare Items ' button and display of UI elements in Compare page in Compare section  in the application both as an anonymous and a registered user");
$t.start();
try		
{
		//navigate to subcat page	
		navigateSubCatPage();
		//add 6 items for compare
		for(var $i=0;$i<$Generic[0][11];$i++)
			{
			//check the compare check box
			_check(_checkbox("compare-check["+$i+"]"));
			}		
		//verify whether compare section appeared or not
		_assertVisible(_div("compare-items"));
		//fetch total no.of added compare items
		var $totalCompareItems=_count("_div","/compare-item active/",_in(_div("compare-items-panel")));
		_assertEqual($Generic[0][11],$totalCompareItems);
		//click on compare items button
		_click(_submit("compare-items-button"));
		//verify the navigation and UI
		_assertVisible(_heading1("Compare"));
		//back to results links
		var $totalBackToResultLinks=_count("_link","/back to results/");
		_assertEqual($Generic[1][11],$totalBackToResultLinks);
		for(var $i=0;$i<$totalBackToResultLinks;$i++)
			{
			_assertVisible(_link("<< back to results["+$i+"]"));
			}
		
		//print link
		//_assertVisible(_submit("print-page"));
		//_assertVisible(_link("compare-print print-page button"));
		
		//remove,name,images links
		for(var $i=0;$i<$totalCompareItems;$i++)
			{
			_assertVisible(_link("remove-link["+$i+"]"));
			_assertVisible(_link("name-link["+$i+"]"));
			_assertVisible(_image("/(.*)/",_near(_link("name-link["+$i+"]"))));
			//add to cart
			_assertVisible(_submit("Add to Cart["+$i+"]"));
			//add to wish list
			_assertVisible(_link("Add to Wishlist["+$i+"]"));
			//add to gift registry
			_assertVisible(_link("Add to Gift Registry["+$i+"]"));
			//price label
			_assertVisible(_div("product-pricing["+$i+"]"));
			}
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()	
	
var $t = _testcase("125862","Verify the navigation of 'Back to Results' link in Compare page  in the application both as an anonymous and a registered user");
$t.start();
try
	{
		//fetch product name
		var $pName=_getText(_link("name-link"));
		//click on back to results link
		_click(_link("/back to results/"));
		//verify the navigation to search results
		//bread crumb
		_assertVisible(_div("breadcrumb"));
		_assertContainsText($Generic[2][10], _div("breadcrumb"));
	}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
	

//click on compare items button
_click(_submit("compare-items-button"));
var $t = _testcase("125861","Verify the functionality of 'Remove ' link in Compare page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	var $removeLinks=_count("_link","/action remove-link/");
	for(var $i=0;$i<$removeLinks;$i++)
		{
			var $removeLinksBeforeClick=_count("_link","/action remove-link/");
			if($i==$removeLinks-1)
				{
					//click on remove link
					_click(_link("action remove-link"));
					//should navigate back to sub category
					_assertVisible(_div("breadcrumb"));
				}
			else
				{
					//click on remove link
					_click(_link("action remove-link"));
					$removeLinksBeforeClick--;
					_wait(5000);
					$removeLinks=_count("_link","/action remove-link/");
					_assertEqual($removeLinksBeforeClick,$removeLinks);
				}
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();	


var $t=_testcase("125867","Verify the Quick view UI  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	//fetching the product name
	var $PName=_getText(_link("name-link"));
	//mouse over on image
	_mouseOver(_link("thumb-link"));
	//Quick view button
	_click(_link("quickviewbutton")); 
	//title
	_assertVisible(_span("ui-dialog-title"));
	_assertEqual($Generic[0][2], _getText(_span("ui-dialog-title")));
	//close
	_assertVisible(_button("Close"));
	//Product name
	_assertVisible(_heading1("product-name"));
	//Main image
	_assertVisible(_link("product-image main-image"));
	//Product number
	_assertVisible(_div("product-number"));
	//view details link
	_assertVisible(_link("View Full Details"));
	//Product price
	_assertVisible(_div("product-price"));
	//Previous & next links
	_assertVisible(_div("quickview-nav"));
	//stock status
	_assertVisible(_label("availability"));
	//Promo messages if any Promotions are applied
	if(_isVisible(_div("promotion-callout")))
	{
	  _assertVisible(_div("promotion-callout"));
	  _log(_getText(_div("promotion-callout")));
	}
	else
	{
	  _log("No promotional message to display");
	}
	//variations
	_assertVisible(_div("product-variations"));
	//quantity
	_assertVisible(_textbox("Quantity"));
	_assertEqual("1", _getValue(_textbox("Quantity")));
	
	//#### Price will not display beside Add tocart button according to new functionality ###########
	//_assertVisible(_span("price-sales", _in(_fieldset("ADD TO CART OPTIONS"))));
	
	//Add to cart
	_assertVisible(_div("product-add-to-cart"));
	//Alternate images
	_assertVisible(_div("product-thumbnails"));
	
	//########## Alternative images Heading is not displaying ###########
	//_assertVisible(_heading2($Generic[1][2]));
	
	//selecting variations
	selectSwatch();
	//add to wish list
	_assertVisible(_link("Add to Wishlist"));
	//add to gift registry
	_assertVisible(_link("Add to Gift Registry"));
	
	//closing overlay
	_click(_button("Close"));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


var $t=_testcase("126825","Verify 'Next' and 'Previous' links functionality in Quick view in sub category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	var $numberOfProducts = _count("_div", "/product-name/", "true");
	var $prodNames= _collectAttributes("_div", "/product-name/", "sahiText");
	_mouseOver(_link("thumb-link"));
	_click(_link("quickviewbutton"));
	for ($i=0; $i<$numberOfProducts-1; $i++)
	{
	  var $name = _getText(_heading1("product-name"));
	  _assertEqual($prodNames[$i],$name);
	  _click(_submit("quickview-next"));
	}
	for($i=$numberOfProducts-2; $i>0; $i--)
	{
	  _click(_submit("quickview-prev"));
	  var $name = _getText(_heading1("product-name"));
	  _assertEqual($prodNames[$i],$name);	  
	 }
	//closing overlay
	_click(_button("Close"));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


var $t=_testcase("128530","Verify the 'Add to cart' functionality in Quick view of sub category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	_mouseOver(_link("thumb-link"));
	_click(_link("quickviewbutton"));
	var $Product=_getText(_heading1("product-name"));
	//var $ProductPrice=_getText(_div("product-price"));
	var $ProductPrice=_getText(_span("price-sales"));
	//selecting variations
	selectSwatch();
	//click in add to cart
	_click(_submit("Add to Cart"));
	//navigate to cart
	_click(_link("mini-cart-link"));
	//Checking Product name
	_assertEqual($Product, _getText(_div("name")));
	_assertEqual($ProductPrice,_getText(_span("price-sales")));
	//removing item from cart
	while(_isVisible(_submit("Remove")))
	{
	_click(_submit("Remove"));
	}
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

//'Add to wish list' as guest
var $t=_testcase("125866","Verify the functionality of  'Add to wish list' link in Quick view  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	_mouseOver(_link("thumb-link"));
	_click(_link("quickviewbutton"));
	//selecting swatches
	selectSwatch();
	//add to wish list
	_assertVisible(_link("Add to Wishlist"));
	_click(_link("Add to Wishlist"));
	//validating the navigation
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][4].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][4], _getText(_div("breadcrumb")));
		}	
	//heading
	_assertVisible(_heading1($Generic[1][4]));
	_assertVisible(_heading2($Generic[2][4]));
	//wish list section
	_assertVisible(_div("login-box login-general clearfix"));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

//'Add to Gift Registry' as guest
var $t=_testcase("125865","Verify the functionality of  'Add to Gift Registry' link in Quick view  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	_mouseOver(_link("thumb-link"));
	_click(_link("quickviewbutton"));
	//selecting swatches
	selectSwatch();
	//add to gift registry
	_assertVisible(_link("Add to Gift Registry"));
	_click(_link("Add to Gift Registry"));
	//validating the navigation
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][5].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][5], _getText(_div("breadcrumb")));
		}	
	//heading
	_assertVisible(_heading1($Generic[1][5]));
	_assertVisible(_heading2("/"+$Generic[2][5]+"/"));
	//gift registry section
	_assertVisible(_div("login-box login-search-gift clearfix"));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


//Login()
_click(_link("Login"));
login();

var $t=_testcase("125866","Verify the functionality of  'Add to wish list' link in Quick view  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	_mouseOver(_link("thumb-link"));
	_click(_link("quickviewbutton"));
	var $Pname=_getText(_heading1("product-name"));
	//selecting swatches
	selectSwatch();
	//add to wish list
	_assertVisible(_link("Add to Wishlist"));
	_click(_link("Add to Wishlist"));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][9].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][9], _getText(_div("breadcrumb")));
		}
	//heading
	_assertVisible(_heading1($Generic[1][9]));
	//Product
	_assertVisible(_cell("/"+$Pname+"/"));
	//removing item
	_click(_submit("Remove", _in(_row("/"+$Pname+"/"))));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

var $t=_testcase("125865","Verify the functionality of  'Add to Gift Registry' link in Quick view  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	_mouseOver(_link("thumb-link"));
	_click(_link("quickviewbutton"));
	var $Pname=_getText(_heading1("product-name"));
	//selecting swatches
	selectSwatch();
	//add to gift registry
	_assertVisible(_link("Add to Gift Registry"));
	_click(_link("Add to Gift Registry"));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][8].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][8], _getText(_div("breadcrumb")));
		}
	//heading
	_assertVisible(_heading1($Generic[1][8])); 
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()
}
cleanup();


function navigateSubCatPage()
{
_click(_link($RootCatName));
_log("We are now on " + $RootCatName + " landing page");
_wait(2000);
_click(_link($CatName));
_log("We are now on " + $CatName + " landing page");
_wait(2000);
_click(_link($SubCatName));
_log("We are now on " + $SubCatName + " landing page");
_wait(2000);
}

