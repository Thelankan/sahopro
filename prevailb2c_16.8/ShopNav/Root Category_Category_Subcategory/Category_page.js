_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Category_Data.xls");

var $Generic=_readExcelFile("Category_Data.xls", "Generic");
var $Quantity=_readExcelFile("Category_Data.xls", "Quantity");
var $SendToFriend=_readExcelFile("Category_Data.xls", "SendToFriend");
var $RootCatName=$Generic[0][0];
var $CatName=$Generic[1][0];
//from util.GenericLibrary/BrowserSpecific.js
var $FirstName=$FName;
var $LastName=$LName;
var $UserEmail=$uId;
SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("124406", "Verify the system response on click of category name in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_assertVisible(_div("breadcrumb"));
	_assertContainsText($Generic[1][0], _div("breadcrumb"));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124407", "Verify the UI of the category landing page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	_assertContainsText($Generic[1][0], _div("breadcrumb"));
	//hero slot
	_assertVisible(_div("content-slot slot-grid-header"));
	//sort by
	_assertVisible(_select("grid-sort-header"));
	_assertVisible(_select("grid-sort-footer"));
	if(!isMobile())
		{
		//refinement header
		_assertVisible(_span("refinement-header"));
		//left nav
		_assertVisible(_div("refinements"));
//		//items per page
//		_assertVisible(_select("grid-paging-header"));
//		_assertVisible(_select("grid-paging-footer"));
		}
	else
		{
		//left nav
		_assertVisible(_div("navigation"));		
		}
//	//Pagination
//	_assertVisible(_div("pagination"));
//	_assertVisible(_div("pagination", _under(_list("search-result-items"))));
	//result hits
	_assertVisible(_div("results-hits"));
	_assertVisible(_div("results-hits", _under(_list("search-result-items"))));
	//Items displayed
	_assertVisible(_list("search-result-items"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124408", "Verify the breadcrumb in category landing page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	_assertContainsText($Generic[1][0], _div("breadcrumb"));
	//var $expBreadCrumb="Home "+$RootCatName+" "+$CatName;
	var $expBreadCrumb=$RootCatName+" "+$CatName;
	if(_isIE())
		{
		_assertEqual($expBreadCrumb.replace(/\s/g,""), _getText(_div("breadcrumb")).replace(/\s/g,""));
		}
	else
		{
		_assertEqual($expBreadCrumb, _getText(_div("breadcrumb")));
		}
	//navigate to root category page
	_click(_link($RootCatName,_in(_div("breadcrumb"))));
	//var $expBreadCrumb1="Home "+$RootCatName;
	
	//Bread crumb should not visible in Root category landing page
	_assertNotVisible(_div("breadcrumb"));
	
//	var $expBreadCrumb1=$RootCatName;
//	if(_isIE())
//	{
//	_assertEqual($expBreadCrumb1.replace(/\s/g,""), _getText(_div("breadcrumb")).replace(/\s/g,""));
//	}
//	else
//	{
//	//_assertEqual($expBreadCrumb1, _getText(_div("breadcrumb")));
//	_assertNotVisible(_div("breadcrumb"));
//	}
	
	//############################### Commenting below snippet of code as per functionality change ##################
	//Validating navigation from category page to home page
//	navigateCatPage();
//	_click(_link("Home"));
//	_assertVisible(_list("homepage-slides"));
//	_assertVisible(_div("home-bottom-slots"));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124409", "Verify the display of category links under heading Shop <cat name> in left nav in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_setAccessorIgnoreCase(true);
	//the display of category names
	var $ExpectedCategory=$CatName;
	_assertVisible(_listItem("expandable fa fa-angle-right active fa-angle-down"));
	_assertEqual($ExpectedCategory, _getText(_link("refinement-link  active")));
	//navigation of sub-category link.
	var $subcategories=_collectAttributes("_link","/refinement-link/","sahiText",_in(_list("category-level-2")));
	for($i=1;$i<$subcategories.length;$i++)
	{
	_click(_link($subcategories[$i], _in(_list("category-level-2"))));
	_assertContainsText($subcategories[$i], _div("breadcrumb"));
	_assertEqual($subcategories[$i], _getText(_link("refinement-link  active", _in(_list("category-level-2")))));
	}
	//Click on any category name and verifying display of subcategory names
	navigateCatPage();
	var $ExpectedCategory1=_getText(_link("refinement-link  active"));
	_click(_link("refinement-link  active"));
	_assertVisible(_listItem("expandable fa fa-angle-right active fa-angle-down"));
	_assertContainsText($ExpectedCategory1, _listItem("expandable fa fa-angle-right active fa-angle-down"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124414", "Verify the system's response on click of 'list view icon' (or) grid view icon beside pagination after applying the filter.");
$t.start();
try
{
	navigateCatPage();
	_assertVisible(_div("search-result-content"));
	_click(_span("toggle-grid"));
	_assertVisible(_byClassName("search-result-content wide-tiles", "DIV"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124410", "Verify the system's response on selection on color filter from left nav in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	var $colors = _collectAttributes("_link","/(.*)/","sahiText",_in(_list("clearfix swatches refinementColor")));
	var $unSelectable=_collectAttributes("_listItem","/unselectable swatch/","sahiText",_in(_list("clearfix swatches refinementColor")));
	for(var $k=0; $k<$colors.length; $k++)
	{
		if($unSelectable.indexOf($colors[$k])>=0)
			{
			_assert(true, "Swatch is unselectable");
			}
		else
			{
			 _click(_link($colors[$k]));
			  //clear link
			 _assertVisible(_link("Clear", _in(_div("refinement refinementColor"))));
			  _assertEqual($colors[$k]+" x",_getText(_span("breadcrumb-refinement-value")));
			 _click(_link("breadcrumb-relax"));
			 _wait(4000,!_isVisible(_link("breadcrumb-relax")));
			}  
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}		
$t.end();

var $t = _testcase("124411", "Verify the system's response on selection on Price filter from left nav  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	var $PriceRange =_collectAttributes("_link","/(.*)/","sahiText",_in(_div("refinement ")));
	for ($k=0; $k< $PriceRange.length; $k++)
	{
	_click(_link($PriceRange[$k]));
	_assertEqual($PriceRange[$k]+" x", _getText(_span("breadcrumb-refinement-value"))); 	
	priceCheck($PriceRange[$k]);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124420", "Verify the details displayed for each item in category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	var $productCount = _collect("_div", "/product-tile/", _in(_list("search-result-items")));
	for ($i=0 ; $i<$productCount.length; $i++)
	 {
		 var $prod = _getText(_div("product-tile["+$i+"]"));
		 _assertVisible(_link("thumb-link"));
		 _assertVisible(_div("product-name"));
		 if(_isVisible(_div("Test_revert-calloutmessage")))
		   {
		      _assert(_isVisible(_div("Test_revert-calloutmessage")));
		   }
		  else
		  {
		    _log("No promo message to display");
		  }
		if(_isVisible(_list("swatch-list")))
			{
			_assert(_isVisible(_list("swatch-list")));
			}
		else
			{
			  _log("No alternate swatches available to display");
			}
	_log($prod);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124413", "Verify the system's response on click of filter names displayed in left nav");
$t.start();
try
{
	navigateCatPage();
	var $Filters=_collectAttributes("_heading3","/toggle/","sahiText",_in(_div("refinements")));
	for(var $i=0;$i<$Filters.length;$i++)
		{	
			//click on filter
			_click(_heading3($Filters[$i]));
			//filter will collapse
			//_assertNotVisible(_list("/(.*)/", _in(_div("refinement "+$Filters[$i]))));
			_assertNotVisible(_list("/(.*)/",_near(_heading3($Filters[$i]))));
			//click on filter
			_click(_heading3($Filters[$i]));
			//filter will expand
			//_assertVisible(_list("/(.*)/", _in(_div("refinement "+$Filters[$i]))));
			_assertVisible(_list("/(.*)/",_near(_heading3($Filters[$i]))));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124416", "Verify whether filter get clear when user click on selected filter value");
$t.start();
try
{
	navigateCatPage();
	var $Filters=_collectAttributes("_heading3","/toggle/","sahiText",_in(_div("refinements")));
	var $FilterOptions=_collectAttributes("_link","/(.*)/","sahiText",_in(_list("/(.*)/", _under(_heading3($Filters[0])))));
	var $unSelectableOpt=_collectAttributes("_listItem","/unselectable swatch/","sahiText",_under(_heading3($Filters[0])));
	//var $FilterOptions=_collectAttributes("_link","/(.*)/","sahiText",_in(_list("/(.*)/", _in(_div("refinement "+$Filters[0])))));
	//var $unSelectableOpt=_collectAttributes("_listItem","/unselectable swatch/","sahiText",_in(_div("refinement "+$Filters[0])));
		for(var $j=0;$j<$FilterOptions.length;$j++)
			{
				if($unSelectableOpt.indexOf($FilterOptions[$j])>=0)
				{
					_assert(true, "Swatch is unselectable");
				}
			else
				{
					_click(_link($FilterOptions[$j]));
					_assertVisible(_link("Clear"));
					_assertVisible(_span("breadcrumb-refinement-value"));
					_assertEqual($FilterOptions[$j]+" x", _getText(_span("breadcrumb-refinement-value")));
					_click(_link($FilterOptions[$j]));
					_assertNotVisible(_link("Clear"));
					_assertNotVisible(_span("breadcrumb-refinement-value"));
				}
			}
	_click(_image("logo.png"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124419", "Verify the system's reponse when user apply multiple filters  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	var $Filter=_collectAttributes("_heading3","/toggle/","sahiText",_in(_div("refinements")));
	var $ExpectedApplied= new Array();
	for(var $i=0;$i<$Filter.length;$i++)
	{
		if(_isVisible(_div("refinement "+$Filter[$i])))
			{
			var $FilterOptions=_collectAttributes("_link","/(.*)/","sahiText",_in(_list("/(.*)/", _in(_div("refinement "+$Filter[$i])))));
			_click(_link($FilterOptions[0]));	
			$ExpectedApplied.push($FilterOptions[0]+" x");
			}		
	}
	var $ActualApplied=_collectAttributes("_span","/breadcrumb-refinement-value/","sahiText",_in(_div("breadcrumb")));
	_assertEqual($ExpectedApplied.sort(),$ActualApplied.sort());
	_click(_image("logo.png"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124412", "Verify the system's response on click of 'true' link under the 'New Arrival' section in left nav  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	if(_isVisible(_heading3("NEW ARRIVAL")))
	  {
		_click(_link("/true/",_near(_heading3("NEW ARRIVAL"))));
		_assertVisible(_link("Clear"));
		_assertVisible(_span("breadcrumb-refinement-value"));
	    _click(_link("Clear"));
	    _assertNotVisible(_link("Clear"));
	    _assertNotVisible(_span("breadcrumb-refinement-value"));
	  }
	  else
	  {
	    _log("No New Arrival Refinement available");
	  }
	_click(_image("logo.png"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124421", "Verify the system's response on click of Products links in category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	var $productName=_getText(_link("name-link"));
	var $productPrice = _getText(_span("product-sales-price"));
	//click on name link
	_click(_link("name-link"));
	_assertEqual($productName,_getText(_heading1("product-name")));
	_assertEqual($productPrice,_getText(_span("price-sales")));
	
	navigateCatPage();
	var $productName=_getText(_link("name-link"));
	var $productPrice = _getText(_span("product-sales-price"));
	//click on image
	_click(_link("thumb-link"));
	_assertEqual($productName,_getText(_heading1("product-name")));
	_assertEqual($productPrice,_getText(_span("price-sales")));
	
	if(!isMobile())
	{
		var $Checkcount=0;
		//compare checkbox
		navigateCatPage();
		_check(_checkbox("compare-check", _in(_div("product-tile"))));
		$Checkcount++;
		//validating compare 
		_assertVisible(_div("compare-items"));
		_assertEqual(1, _count("_div", "/compare-item active/"));
		_uncheck(_checkbox("compare-check", _in(_div("product-tile"))));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124415", "Verify the pagination in category landing page in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	pagination();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

if(!isMobile())
{
	var $t = _testcase("124418", "Verify the sustem's reponse on selecting value from 'Products per page' drop down  in the application both as an anonymous and a registered user");
	$t.start();
	try
	{
		navigateCatPage();
		if(_isVisible(_select("grid-paging-header")) && _isVisible(_select("grid-paging-footer")))
		{	
			var $itemsDisp=_getText(_select("grid-paging-header"));
			for(var $i=0;$i<$itemsDisp.length;$i++)
			{
				_setSelected(_select("grid-paging-header"), $itemsDisp[$i]);
				//Validating the selection
				_assertEqual($itemsDisp[$i],_getSelectedText(_select("grid-paging-header")),"Selected option is displaying properly");
				_assertEqual($itemsDisp[$i],_getSelectedText(_select("grid-paging-footer")),"Selected option is displaying properly at footer");
				ItemsperPage($itemsDisp[$i]);
			}		
		}
		else
		{
			_assert(true, "View drop down is not present");
		}
		_click(_image("logo.png"));
	}
	catch($e)
	{
		_logExceptionAsFailure($e);
	}	
	$t.end();
}

var $t = _testcase("124417", "Verify the system's reponse on selection of any sort by value");
$t.start();
try
{
	navigateCatPage();
	if(_isVisible(_select("grid-sort-header")) && _isVisible(_select("grid-sort-footer")))
	{
		var $sort=_getText(_select("grid-sort-header"));
		var $j=0;
		for(var $i=1;$i<$sort.length;$i++)
		{
			_setSelected(_select("grid-sort-header"), $sort[$i]);
			//Validating the selection
			_assertEqual($sort[$i],_getSelectedText(_select("grid-sort-header")),"Selected option is displaying properly");
			_assertEqual($sort[$i],_getSelectedText(_select("grid-sort-footer")),"Selected option is displaying properly");
			if($sort.indexOf($Generic[$j][1])>-1)
			{
				_setSelected(_select("grid-sort-header"), $Generic[$j][1]);
				sortBy($Generic[$j][1]);
				$j++;
			}
		}
	}
	else
	{
		_assert(false, "sort by drop down is not present");
	}
	_click(_image("logo.png"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

if(!isMobile())
{
var $t=_testcase("148584","Verify the navigation related to Quickview overlay In the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	//fetching the product name
	var $PName=_getText(_link("name-link"));
	//mouse over on image
	_mouseOver(_link("thumb-link"));
	//Quick view button
	//_assertVisible(_link("quickviewbutton"));
	_click(_link("quickviewbutton"));  
	//quick view overlay
	_assertVisible(_div("ui-dialog-content ui-widget-content"));
	_assertEqual($PName, _getText(_heading1("product-name")));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


var $t=_testcase("125620","Verify the UI of 'Quick view' In the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	//fetching the product name
	var $PName=_getText(_link("name-link"));
	//mouse over on image
	_mouseOver(_link("thumb-link"));
	//Quick view button
	_click(_link("quickviewbutton")); 
	//title
	_assertVisible(_span("ui-dialog-title"));
	_assertEqual($Generic[0][2], _getText(_span("ui-dialog-title")));
	//close
	_assertVisible(_button("Close"));
	//Product name
	_assertVisible(_heading1("product-name"));
	//Main image
	_assertVisible(_link("product-image main-image"));
	//Product number
	_assertVisible(_div("product-number"));
	//view details link
	_assertVisible(_link("View Full Details"));
	//Product price
	_assertVisible(_div("product-price"));
	//Previous & next links
	_assertVisible(_div("quickview-nav"));
	//stock status
	_assertVisible(_label("availability"));
	//Promo messages if any Promotions are applied
	if(_isVisible(_div("promotion-callout")))
	{
	  _assertVisible(_div("promotion-callout"));
	  _log(_getText(_div("promotion-callout")));
	}
	else
	{
	  _log("No promotional message to display");
	}
	//variations
	_assertVisible(_div("product-variations"));
	//quantity
	_assertVisible(_textbox("Quantity"));
	_assertEqual("1", _getValue(_textbox("Quantity")));
	
	//Price displayed beside Add tocart button [Price should not display now functionality removed]
	//_assertVisible(_span("price-sales", _in(_fieldset("ADD TO CART OPTIONS"))));
	
	//Add to cart
	_assertVisible(_div("product-add-to-cart"));
	//Alternate images
	_assertVisible(_div("product-thumbnails"));
	//_assertVisible(_heading2($Generic[1][2]));  (commented by Rahul because alternate view option is not there)
	//selecting variations
	selectSwatch();
	
	//add to wish list
	_assertVisible(_link("Add to Wishlist"));
	//add to gift registry
	_assertVisible(_link("Add to Gift Registry"));
	
	//closing overlay
	_click(_button("Close"));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


var $t=_testcase("126824","Verify 'Next' and 'Previous' links functionality in Quick view in category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	var $numberOfProducts = _count("_div", "/product-name/", "true");
	var $prodNames= _collectAttributes("_div", "/product-name/", "sahiText");
	_mouseOver(_link("thumb-link"));
	_click(_link("quickviewbutton"));
	for ($i=0; $i<$numberOfProducts-1; $i++)
	{
	  var $name = _getText(_heading1("product-name"));
	  _assertEqual($prodNames[$i],$name);
	  _click(_submit("quickview-next"));
	}
	for($i=$numberOfProducts-2; $i>0; $i--)
	{
	  _click(_submit("quickview-prev"));
	  var $name = _getText(_heading1("product-name"));
	  _assertEqual($prodNames[$i],$name);	  
	 }
	//closing overlay
	_click(_button("Close"));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


var $t=_testcase("128529","Verify the 'Add to cart' functionality in Quick view of category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver(_link("thumb-link"));
	_click(_link("quickviewbutton"));
	var $Product=_getText(_heading1("product-name"));
	//var $ProductPrice=_getText(_div("product-price"));
	var $ProductPrice=_getText(_span("price-sales")); 
	//selecting variations
	selectSwatch();
	//click in add to cart
	_click(_submit("Add to Cart"));
	//navigate to cart
	_click(_link("mini-cart-link"));
	//Checking Product name
	_assertEqual($Product, _getText(_div("name")));
	_assertEqual($ProductPrice,_getText(_span("price-sales")));
	//removing item from cart
	while(_isVisible(_submit("Remove")))
	{
	_click(_submit("Remove"));
	}
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

var $t=_testcase("148587","Verify the system response on click of color swatch displayed on 'Quick view' overlay in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver(_link("thumb-link"));
	_click(_link("quickviewbutton"));
	var $Product=_getText(_heading1("product-name"));	
	var $colors = _count("_link","/swatchanchor/",_in(_list("swatches color")));
	for(var $i=0;$i<$colors;$i++)
		{			
			var $a=_getAttribute(_image("/(.*)/", _in(_link("swatchanchor["+$i+"]", _in(_list("swatches color"))))),"title");
			_click(_link($a, _in(_list("swatches color"))));
			_assertEqual($a,_getText(_listItem("selected-value", _in(_list("swatches color")))));
			_assert(true, "Selected color is appropriate");
		}	
	//closing overlay
	_click(_button("Close"));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


var $t=_testcase("148585","Verify 'view full details' link in 'Quick view' overlay in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver(_link("thumb-link"));
	_click(_link("quickviewbutton"));
	var $Product=_getText(_heading1("product-name"));
	//var $ProductPrice=_getText(_div("product-price"));
	var $ProductPrice=_getText(_span("price-sales"));
	//click on view details link
	_click(_link("view-full-details"));
	//validating the navigation
	_assertEqual($Product,_getText(_heading1("product-name")));
	_assertEqual($ProductPrice,_getText(_span("price-sales")));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	_assertContainsText($Product, _div("breadcrumb"));
	//_assertEqual($Product, _getText(_span("last", _in(_list("breadcrumb")))));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


var $t=_testcase("--","Verify the functionality of 'QTY' drop down on 'Quick view' overlay in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver(_link("thumb-link"));
	_click(_link("quickviewbutton"));
	//quantity
	_assertVisible(_textbox("Quantity"));
	_assertEqual("1", _getValue(_textbox("Quantity")));
	for(var $i=0;$i<$Quantity.length;$i++)
		{
			_setValue(_textbox("Quantity"), $Quantity[$i][1]);
			//selecting variations
			selectSwatch();
			//click in add to cart
			_click(_submit("Add to Cart"));
			//navigate to cart
			_click(_link("mini-cart-link"));
			if($i==2)
				{
				_assertEqual($Quantity[$i][1],_getText(_numberbox("input-text", _in(_cell("item-quantity")))));
				}
			else
				{
				_assertEqual($Quantity[0][2],_getText(_numberbox("input-text", _in(_cell("item-quantity")))));
				}
			while(_isVisible(_submit("Remove")))
			{
			_click(_submit("Remove"));
			}
			navigateCatPage();
			_mouseOver(_link("thumb-link"));
			_click(_link("quickviewbutton"));
		}
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

//'Add to wish list' as guest
var $t=_testcase("125621","Verify the system's response on click on 'Add to wish list' link in Quick view in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver(_link("thumb-link"));
	_click(_link("quickviewbutton"));
	//selecting swatches
	selectSwatch();
	//add to wish list
	_assertVisible(_link("Add to Wishlist"));
	_click(_link("Add to Wishlist"));
	//validating the navigation
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][4].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][4], _getText(_div("breadcrumb")));
		}	
	//heading
	_assertVisible(_heading1($Generic[1][4]));
	_assertVisible(_heading2($Generic[2][4]));
	//wish list section
	_assertVisible(_div("login-box login-general clearfix"));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

//'Add to Gift Registry' as guest
var $t=_testcase("125622","Verify the system's response on click on 'Add to Gift Registry' link in Quick view  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver(_link("thumb-link"));
	_click(_link("quickviewbutton"));
	//selecting swatches
	selectSwatch();
	//add to gift registry
	_assertVisible(_link("Add to Gift Registry"));
	_click(_link("Add to Gift Registry"));
	//validating the navigation
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][5].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][5], _getText(_div("breadcrumb")));
		}	
	//heading
	_assertVisible(_heading1($Generic[1][5]));
	_assertVisible(_heading2("/"+$Generic[2][5]+"/"));
	//gift registry section
	_assertVisible(_div("login-box login-search-gift clearfix"));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()


//Login()
_click(_link("Login"));
login();

var $t=_testcase("125621","Verify the system's response on click on 'Add to wish list' link in Quick view in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver(_link("thumb-link"));
	_click(_link("quickviewbutton"));
	var $Pname=_getText(_heading1("product-name"));
	//selecting swatches
	selectSwatch();
	//add to wish list
	_assertVisible(_link("Add to Wishlist"));
	_click(_link("Add to Wishlist"));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][9].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][9], _getText(_div("breadcrumb")));
		}
	//heading
	_assertVisible(_heading1($Generic[1][9]));
	//Product
	_assertVisible(_cell("/"+$Pname+"/"));
	//removing item
	_click(_submit("Remove", _in(_row("/"+$Pname+"/"))));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

var $t=_testcase("125622","Verify the system's response on click on 'Add to Gift Registry' link in Quick view  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver(_link("thumb-link"));
	_click(_link("quickviewbutton"));
	var $Pname=_getText(_heading1("product-name"));
	//selecting swatches
	selectSwatch();
	//add to gift registry
	_assertVisible(_link("Add to Gift Registry"));
	_click(_link("Add to Gift Registry"));
	//bread crumb
	_assertVisible(_div("breadcrumb"));
	if(_isIE9())
		{
		_assertEqual($Generic[0][8].replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,'') ); 
		}
	else
		{
		_assertEqual($Generic[0][8], _getText(_div("breadcrumb")));
		}
	//heading
	_assertVisible(_heading1($Generic[1][8])); 
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()
}
cleanup();


function navigateCatPage()
{
_click(_link($RootCatName));
_log("We are now on " + $RootCatName + " landing page");
_wait(2000);
_click(_link($CatName));
_log("We are now on " + $CatName + " landing page");
_wait(2000);
}



