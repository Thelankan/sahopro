_include("../util.GenericLibrary/GlobalFunctions.js");
_resource("Root Category_Category_Subcategory/Category_Data.xls");
_resource("PD Page/PDPage.xls");
_resource("Search/Search_Data.xls");
_resource("Home Page_Header/Homepage.xls");
_resource("Mini cart/Minicart.xls");
_resource("Compare Page/ComparePage.xls");


var $Generic=_readExcelFile("Root Category_Category_Subcategory/Category_Data.xls", "Generic");
var $PDP_ProductBundle=_readExcelFile("PD Page/PDPage.xls","PDP_ProductBundle");
var $Qty_Validations=_readExcelFile("PD Page/PDPage.xls","Qty_Validations");
var $PDP_Standard=_readExcelFile("PD Page/PDPage.xls","PDP_Standard");
var $PDP_Productset=_readExcelFile("PD Page/PDPage.xls","PDP_Productset");
var $Generic1=_readExcelFile("Search/Search_Data.xls", "Generic");
var $data=_readExcelFile("Home Page_Header/Homepage.xls","Homepage");
var $Minicart=_readExcelFile("Mini cart/Minicart.xls","Minicart");
var $Compare=_readExcelFile("Compare Page/ComparePage.xls","Compare");
var $RootCatName=$Generic[0][0];
var $CatName=$Generic[1][0];
var $SubCatName=$Generic[2][10];
//from util.GenericLibrary/BrowserSpecific.js
var $FirstName=$FName;
var $LastName=$LName;
var $UserEmail=$uId;


SiteURLs();
cleanup();
_setAccessorIgnoreCase(true); 


function verifyNavigation($pName)
{
	_assertVisible(_span("last"));
	_assertEqual($pName, _getText(_span("last")));
	_assertVisible(_heading1("product-name"));
	_assertEqual($pName, _getText(_heading1("product-name")));	
}


function navigateSubCatPage()
{
_click(_link($RootCatName));
_log("We are now on " + $RootCatName + " landing page");
_wait(2000);
_click(_link($CatName));
_log("We are now on " + $CatName + " landing page");
_wait(2000);
_click(_link($SubCatName));
_log("We are now on " + $SubCatName + " landing page");
_wait(2000);
}


function navigateCatPage()
{
_click(_link($RootCatName));
_log("We are now on " + $RootCatName + " landing page");
_wait(2000);
_click(_link($CatName));
_log("We are now on " + $CatName + " landing page");
_wait(2000);
}



//Category.js
var $t = _testcase("124411", "Verify the system's response on selection on Price filter from left nav  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	var $PriceRange = _collectAttributes("_link","/(.*)/","sahiText",_in(_div("refinement Price")));
	for ($k=0; $k< $PriceRange.length; $k++)
	{
	_click(_link($PriceRange[$k]));
	_assertEqual($PriceRange[$k]+" x", _getText(_span("breadcrumb-refinement-value"))); 	
	priceCheck($PriceRange[$k]);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*
var $t = _testcase("124415", "Verify the pagination in category landing page in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	pagination();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/


var $t = _testcase("124417", "Verify the system's reponse on selection of any sort by value");
$t.start();
try
{
	navigateCatPage();
	if(_isVisible(_select("grid-sort-header")) && _isVisible(_select("grid-sort-footer")))
	{
		var $sort=_getText(_select("grid-sort-header"));
		var $j=0;
		for(var $i=1;$i<$sort.length;$i++)
		{
			_setSelected(_select("grid-sort-header"), $sort[$i]);
			//Validating the selection
			_assertEqual($sort[$i],_getSelectedText(_select("grid-sort-header")),"Selected option is displaying properly");
			_assertEqual($sort[$i],_getSelectedText(_select("grid-sort-footer")),"Selected option is displaying properly");
			if($sort.indexOf($Generic[$j][1])>-1)
			{
				_setSelected(_select("grid-sort-header"), $Generic[$j][1]);
				sortBy($Generic[$j][1]);
				$j++;
			}
		}
	}
	else
	{
		_assert(false, "sort by drop down is not present");
	}
	_click(_image("logo.png"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


_click(_image("Demandware SiteGenesis"));
var $t=_testcase("128529","Verify the 'Add to cart' functionality in Quick view of category landing page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	_mouseOver(_link("thumb-link"));
	_click(_link("quickviewbutton"));
	var $Product=_getText(_heading1("product-name"));
	//var $ProductPrice=_getText(_div("product-price"));
	var $ProductPrice=_getText(_span("price-sales")); 
	//selecting variations
	selectSwatch();
	//click in add to cart
	_click(_submit("Add to Cart"));
	//navigate to cart
	_click(_link("mini-cart-link"));
	//Checking Product name
	_assertEqual($Product, _getText(_div("name")));
	_assertEqual($ProductPrice,_getText(_span("price-sales")));
	//removing item from cart
	while(_isVisible(_submit("Remove")))
	{
	_click(_submit("Remove"));
	}
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()



if(!isMobile())
{
var $t=_testcase("148584","Verify the navigation related to Quickview overlay In the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateCatPage();
	//fetching the product name
	var $PName=_getText(_link("name-link"));
	//mouse over on image
	_mouseOver(_link("thumb-link"));
	//Quick view button
	//_assertVisible(_link("quickviewbutton"));
	_click(_link("quickviewbutton"));  
	//quick view overlay
	_assertVisible(_div("ui-dialog-content ui-widget-content"));
	_assertEqual($PName, _getText(_heading1("product-name")));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()
}


//Subcat.js
_click(_image("Demandware SiteGenesis"));
var $t = _testcase("125640", "Verify the functionality of COLOR filter in the left nav of third category grid page  in the application both as an anonymous and a registered user.");
$t.start();
try
{
	navigateSubCatPage();
	var $colors = _collectAttributes("_link","/(.*)/","sahiText",_in(_list("clearfix swatches Color")));
	var $unSelectable=_collectAttributes("_listItem","/unselectable swatch/","sahiText",_in(_list("clearfix swatches Color")));
	for(var $k=0; $k<$colors.length; $k++)
	{
		if($unSelectable.indexOf($colors[$k])>=0)
			{
			_assert(true, "Swatch is unselectable");
			}
		else
			{
			 _click(_link($colors[$k]));
			  //clear link
			  _assertVisible(_link("Clear", _in(_div("refinement Color"))));
			  _assertEqual($colors[$k]+" x",_getText(_span("breadcrumb-refinement-value")));
			 _click(_link("breadcrumb-relax"));
			 _wait(4000,!_isVisible(_link("breadcrumb-relax")));
			}  
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}		
$t.end();

var $t = _testcase("125641", "Verify the functionality of SIZE filter in the left nav of third category grid page  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	var $Size = _collectAttributes("_link","/(.*)/","sahiText",_in(_div("refinement Size")));
	var $SizeunSelectable=_collectAttributes("_listItem","/unselectable swatch/","sahiText",_in(_div("refinement Size")));
	for ($k=0; $k< $Size.length; $k++)
	{
			if($unSelectable.indexOf($colors[$k])>=0)
				{
				_assert(true, "Swatch is unselectable");
				}
			else
				{
				_click(_link("swatch-"+$Size[$k]));
				_assertEqual($Size[$k]+" x", _getText(_span("breadcrumb-refinement-value"))); 	
				_click(_link("breadcrumb-relax"));
				 _wait(4000,!_isVisible(_link("breadcrumb-relax")));
				}  
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125643", "Verify the system's response on selection on Price filter from left nav  in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	var $PriceRange = _collectAttributes("_link","/(.*)/","sahiText",_in(_div("refinement Price")));
	for ($k=0; $k< $PriceRange.length; $k++)
	{
	_click(_link($PriceRange[$k]));
	_assertEqual($PriceRange[$k]+" x", _getText(_span("breadcrumb-refinement-value"))); 	
	priceCheck($PriceRange[$k]);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125650", "Verify the display and functionality of Sort dropdown  in third category grid page in the application both as an anonymous and a registered user");
$t.start();
try
{
	navigateSubCatPage();
	if(_isVisible(_select("grid-sort-header")) && _isVisible(_select("grid-sort-footer")))
	{
		var $sort=_getText(_select("grid-sort-header"));
		var $j=0;
		for(var $i=1;$i<$sort.length;$i++)
		{
			_setSelected(_select("grid-sort-header"), $sort[$i]);
			//Validating the selection
			_assertEqual($sort[$i],_getSelectedText(_select("grid-sort-header")),"Selected option is displaying properly");
			_assertEqual($sort[$i],_getSelectedText(_select("grid-sort-footer")),"Selected option is displaying properly");
			if($sort.indexOf($Generic[$j][1])>-1)
			{
				_setSelected(_select("grid-sort-header"), $Generic[$j][1]);
				sortBy($Generic[$j][1]);
				$j++;
			}
		}
	}
	else
	{
		_assert(false, "sort by drop down is not present");
	}
	_click(_image("logo.png"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//PDP
_click(_image("Demandware SiteGenesis"));
var $t = _testcase("124749","Verify the functionality of 'quantity box'/Add to cart on product detail page for in the application both as an anonymous and a registered user");
$t.start();
try
{	
	//navigate to sub category
	_click(_link($PDP_Standard[0][0]));	
	$name=_getText(_link("name-link"))
	//navigate to PD page on click of name link
	_click(_link("name-link"));
	//select swatches
	selectSwatch();
	//enter quantity 5 in quantity box
	_setValue(_textbox("Quantity"),$PDP_Standard[0][5]);
	//verify whether entered value is present in quantity box or not
	_assertEqual($PDP_Standard[0][5], _getValue(_textbox("Quantity")));
	//Add to cart
	_click(_submit("Add to Cart"));
	//fetch quntity from mini cart
	var $cartQuantity=_getText(_span("minicart-quantity"));
	//verify the quantity in mini cart
	_assertEqual($PDP_Standard[0][5],$cartQuantity);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

ClearCartItems();
var $t = _testcase("124813","Verify the functionality of add to cart on product detail page  for all products in the set in the application both as an anonymous and a registered user");
$t.start();
try
{
	
//Navigate to product set
search($PDP_Productset[0][0]);
_click(_link($PDP_Productset[0][4]));	
var $totalProductsInSet=_count("_div","/product-set-item product-bundle-item/",_in(_div("product-set-list")));
var $cartQuantity=0;
for(var $i=0;$i<$totalProductsInSet;$i++)
  {	
	//enter value in quantity box
	_setValue(_textbox("Quantity",_in(_div("product-set-item product-bundle-item["+$i+"]"))),$PDP_Productset[0][3]);
	//verify the entered value
	_assertEqual($PDP_Productset[0][3], _getValue(_textbox("Quantity",_in(_div("product-set-item product-bundle-item["+$i+"]")))));
	//click on add to cart
	_click(_submit("button-fancy-medium sub-product-item add-to-cart",_in(_div("product-set-item product-bundle-item["+$i+"]"))));
	//verify the mini cart fly out
	_assertVisible(_div("mini-cart-content"));
	$cartQuantity=$cartQuantity+parseInt($PDP_Productset[0][3])
  }
//verify the total added quantity
var $expQuantity=$cartQuantity;
_assertEqual($cartQuantity,$expQuantity);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

ClearCartItems();

var $t = _testcase("124817","Verify the functionality of 'Add all to cart' button on product detail page With set of products in the application both as an anonymous and a registered user");
$t.start();
try
{	
	//Navigate to product set
	search($PDP_Productset[0][0]);
	_click(_link($PDP_Productset[0][4]));	
	var $totalProductsInSet=_count("_div","/product-set-item product-bundle-item/",_in(_div("product-set-list")));
	var $totalQty=0;	
	//fetch the quantity present in the all boxes in set 
	for(var $i=0;$i<$totalProductsInSet;$i++)
	  {		
		$totalQty=parseFloat($totalQty)+parseFloat(_getText(_textbox("Quantity["+$i+"]")));
	  }
	//fetch quantity from mini cart before clicking
	var $expCartQuantity=$totalQty;
	//click on add all to cart
	_click(_submit("add-all-to-cart"));
	var $actCartQuantity=_getText(_span("minicart-quantity"));
	//verify the cart quantity
	_assertEqual($expCartQuantity,$actCartQuantity);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

ClearCartItems();

var $t = _testcase("124856","Verify the functionality of 'Add to cart' button on product detail  Page for all products in the set in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to bundle product page
	search($PDP_ProductBundle[0][0]);
	//fetch product name 
	var $PName=_getText(_heading1("product-name"));
	var $productsInBundle=new Array();
	var $totalProductsInSet=_count("_div","/product-set-item product-bundle-item/",_in(_div("product-set-list")));	
	for(var $i=0;$i<$totalProductsInSet;$i++)
		{	
		$productsInBundle[$i]=_getText(_link("item-name["+$i+"]"));
		}
	//var $cartQuantity=_getText(_span("minicart-quantity"));
	//click on add to cart
	_click(_submit("button-fancy-large add-to-cart bundle"));
	//verify the cart quantity
	var $expQuantity=$Qty_Validations[0][2];
	var $actQuantity=parseInt(_getText(_span("minicart-quantity")));
	_assertEqual($expQuantity,$actQuantity);
	//verify weather same products are added or not
	_click(_link("View Cart"));
	_assertEqual($PName,_getText(_link("/.*/",_in(_div("name")))));
	var $actBundleproducts=_count("_row","/rowbundle/",_in(_table("cart-table")));
	var $j=0;
	for(var $i=1;$i<=$actBundleproducts;$i++)
		{
		_assertEqual($productsInBundle[$j],_getText(_link("/.*/",_in(_div("name["+$i+"]")))));
		$j++;
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//Search
_click(_image("Demandware SiteGenesis"));
var $t = _testcase("124686/125409", "Verify the navigation to search result page from home page and UI of search result page  in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic1[0][0]);
	//bread crumb
	_assertVisible(_span("breadcrumb-element breadcrumb-result-text"));
	_assertEqual($Generic1[0][2]+" \""+$Generic1[0][0]+"\"", _getText(_span("breadcrumb-element breadcrumb-result-text")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("124687", "Verify the navigation to search no result page from home page");
$t.start();
try
{
	 search($Generic1[0][4]);
	 _assertEqual($Generic1[0][2]+" \""+$Generic1[0][4]+"\"", _getText(_span("breadcrumb-element breadcrumb-result-text")));
	 //heading
	 _assertVisible(_heading1($Generic1[0][5]));
	 //text
	 _assertVisible(_paragraph($Generic1[1][5]+" "+$Generic1[0][4]));
	 _assertVisible(_div("content-asset"));
	 _assertVisible(_label($Generic1[2][5]));
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end()

/*
var $t = _testcase("125414", "Verify the pagination in category landing page in the application both as an anonymous and a registered user");
$t.start();
try
{
	search($$Generic1[0][0]);
	///pagination
	pagination();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/

var $t = _testcase("125417", "Verify the system's response on selection on color filter from left nav in the application both as an anonymous and a registered user");
$t.start();
try
{
	search($Generic1[0][0]);
	var $colors = _collectAttributes("_link","/(.*)/","sahiText",_in(_list("clearfix swatches Color")));
	var $unSelectable=_collectAttributes("_listItem","/unselectable swatch/","sahiText",_in(_list("clearfix swatches Color")));
	for(var $k=0; $k<$colors.length; $k++)
	{
		if($unSelectable.indexOf($colors[$k])>=0)
			{
			__assert(true, "Swatch is unselectable");
			}
		else
			{
			 _click(_link($colors[$k]));
			  //clear link
			  _assertVisible(_link("Clear", _in(_div("refinement Color"))));
			  _assertEqual($colors[$k]+" x",_getText(_span("breadcrumb-refinement-value")));
			 _click(_link("breadcrumb-relax"));
			 _wait(4000,!_isVisible(_link("breadcrumb-relax")));
			}  
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}		
$t.end();

var $t = _testcase("125419", "Verify the functionality related to  'Price' section on search result page left nav in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic1[0][0]);
	var $PriceRange = _collectAttributes("_link","/(.*)/","sahiText",_in(_div("refinement Price")));
	for ($k=0; $k< $PriceRange.length; $k++)
	{
	_click(_link($PriceRange[$k]));
	_assertEqual($PriceRange[$k]+" x", _getText(_span("breadcrumb-refinement-value"))); 	
	priceCheck($PriceRange[$k]);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t = _testcase("125422", "Verify the navigation related to product image on search result page in application both as an  Anonymous and  Registered  user.");
$t.start();
try
{
	search($Generic1[0][0]);
	var $productName=_getText(_link("name-link"));
	var $productPrice = _getText(_span("product-sales-price"));
	//click on name link
	_click(_link("name-link"));
	_assertEqual($productName,_getText(_heading1("product-name")));
	_assertEqual($productPrice,_getText(_span("price-sales")));
	
	search($Generic1[0][0]);
	var $productName=_getText(_link("name-link"));
	var $productPrice = _getText(_span("product-sales-price"));
	//click on image
	_click(_link("thumb-link"));
	_assertEqual($productName,_getText(_heading1("product-name")));
	_assertEqual($productPrice,_getText(_span("price-sales")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//home page
var $t=_testcase("124682","Verify the navigation of links in the category menu");
$t.start();
try
{
	//Nav to Home page
	_click(_image("Demandware SiteGenesis"));
	var $RootCategory=_collectAttributes("_link","/level-1/","sahiText",_in(_list("menu-category level-1")));
	for(var $i=0;$i<$RootCategory.length;$i++)
		{
			_click(_link($RootCategory[$i]), _select("menu-category level-1"));
			_log("We are in "+$RootCategory[$i]+ " landing page");
			if($RootCategory[$i]==$data[0][4])
				{		
					_assertVisible(_heading1($RootCategory[$i]));					
				}
			else if($RootCategory[$i]==$data[1][4])
				{
					_assertVisible(_div("breadcrumb"));
					var $exp="Home Sorted by "+$RootCategory[$i]
					_assertEqual($exp.replace(/\s/g,''), _getText(_div("breadcrumb")).replace(/\s/g,''));
				}
			else
				{								
					_assertContainsText($RootCategory[$i], _div("breadcrumb"));
				}			  
		}   
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("124683","Verify system's response when user clicks on any of the sub category links.");
$t.start();
try
{
	//Nav to Home page
	_click(_image("Demandware SiteGenesis"));
	var $SubCategory=_collectAttributes("_link","/level-2/","sahiText",_in(_list("menu-category level-1")));
	for(var $i=0;$i<$SubCategory.length;$i++)
		{
			_click(_link($SubCategory[$i]), _select("menu-category level-1"));
			_log("We are in "+$SubCategory[$i]+ " landing page");
			_assertContainsText($SubCategory[$i], _div("breadcrumb"));  
		}   
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//Mini cart
_click(_image("Demandware SiteGenesis"));
ClearCartItems();
var $t = _testcase("125605/125607", "Verify the calculation of 'Order Subtotal'  & Product total/Quantity/Total In the header in the mini cart in the application both as an anonymous and a registered user");
$t.start();
try
{
	//add item to cart
	addItemToCart($Minicart[1][0],$Minicart[1][4]);	
	//no.of items present in mini cart
	var $totalSectionsInMiniCart=_count("_div","/mini-cart-product/",_in(_div("mini-cart-products")));
	var $minicartQuantity=new Array();
	var $minicartPrice=new Array();
	var $minicartName=new Array();
	//featching data from minicart
	for(var $i=0;$i<$totalSectionsInMiniCart;$i++)
		{
			//_mouseOver(_link("View Cart"));
			_mouseOver(_link("mini-cart-link"));
			$minicartQuantity[$i]=parseInt(_extract(_getText(_div("mini-cart-pricing["+$i+"]")),"/[:](.*)/",true));
			$minicartPrice[$i]=parseFloat(_extract(_getText(_div("mini-cart-pricing["+$i+"]")),"/[$](.*)/",true));
			//verifying the quantity
			//var $qty1=parseInt(_extract(_getText(_span("mini-cart-price",_near(_link($Minicart[$i][0])))),"/[:](.*)/",true));
			var $qty1=parseInt(_getText(_span("/value/",_in(_div("mini-cart-pricing["+$i+"]")))))
			_assertEqual($qty1,$Minicart[$i][5]);
		}
	var $expQuantity=$minicartQuantity[0]+$minicartQuantity[1];
	var $expSubTotal=$minicartPrice[0]+$minicartPrice[1];
	$expSubTotal=round($expSubTotal,2);
	var $actSubTotal=parseFloat(_extract(_getText(_div("mini-cart-subtotals")),"/[$](.*)/",true));
	_assertEqual($expSubTotal,$actSubTotal);
	//verify the quantity in the header
	var $QtyInHeader=_getText(_span("minicart-quantity"));
	_assertEqual($expQuantity,$QtyInHeader);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



//campare page
_click(_image("Demandware SiteGenesis"));
var $t = _testcase("125461/125464/125465/148597","Verify the functionality of 'Compare Items' button/verify the Navvigation to/UI in Compare bucket section in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to subcat page	
	_click(_link($Compare[0][0]));
	//add at least two items to items to compare
	_check(_checkbox("compare-check"));
	_check(_checkbox("compare-check[1]"));
	//verify whether compare section appeared or not
	_assertVisible(_div("compare-items"));
	//fetch total no.of added compare items
	var $totalCompareItems=_count("_div","/compare-item active/",_in(_div("compare-items-panel")));
	//click on compare items button
	_click(_submit("compare-items-button"));
	//verify the navigation and UI
	_assertVisible(_heading1("Compare"));
	//back to results links
	var $totalBackToResultLinks=_count("_link","/back to results/");
	_assertEqual($Compare[3][1],$totalBackToResultLinks);
	//click on back results
	_click(_link("back"));
	//click on clear all button
	_click(_submit("clear-compared-items"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125467","Verify the functionality of 'Remove(X)' image in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to sub category	
	_click(_link($Compare[0][0]));
	//add 6 items for compare
	for(var $i=0;$i<$Compare[1][1];$i++)
		{
		//check the compare check box
		_check(_checkbox("compare-check["+$i+"]"));
		}
	//click on compare items button
	_click(_submit("Compare Items"));	
	//fetch total no.of remove links
	var $RemoveLinks=_count("_link","/remove-link/");_log($RemoveLinks); 
	for(var $i=0;$i<$RemoveLinks;$i++)
		{
			_log($i);
			var $removeLinksBeforeClick=_count("_link","/remove-link/");
			if($i==$RemoveLinks-1)
				{
					//click on remove link
					_click(_link("remove-link"));
					//should navigate back to sub category
					_assertVisible(_div("breadcrumb"));
				}
			else
				{
					//click on remove link
					_click(_link("remove-link"));
					$removeLinksBeforeClick--;
					_wait(5000);
					var $removeLinks=_count("_link","/remove-link/");
					_assertEqual($removeLinksBeforeClick,$removeLinks);
				}
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148593","Verify the functionality if user click on 'ADD TO CART' button in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to sub category	
	_click(_link($Compare[0][0]));
	//add at least two items to items to compare
	_check(_checkbox("compare-check"));
	_check(_checkbox("compare-check[1]"));
	var $totalCompareItem=_count("_div","/compare-item active/",_in(_div("compare-items-panel")));
	//click on compare items button
	_click(_submit("compare-items-button"));
	//click on add to cart button
	//verify the add to cart functionality
	for(var $i=0;$i<$totalCompareItems;$i++)
		{
			_mouseOver(_div("product-image["+$i+"]"));
			var $pName=_getText(_link("name-link["+$i+"]"));
			_wait(2000);
			//click on quick view overlay
			_click(_link("quickviewbutton",_div("product-image["+$i+"]")));
			_wait(2000);
			//verify whether overlay opened or not 
			_assertVisible(_heading1("product-name"));
			_assertEqual($pName, _getText(_heading1("product-name")));
			_assertVisible(_div("/QuickViewDialog/"));
			_assertVisible(_span("/ui-icon ui-icon-closethick/"));
			//close the overlay
			_click(_span("/ui-icon ui-icon-closethick/"));
		} 
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
cleanup();