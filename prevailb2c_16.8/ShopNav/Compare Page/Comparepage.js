_include("../../util.GenericLibrary/BrowserSpecific.js");
_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("ComparePage.xls");
var $Compare=_readExcelFile("ComparePage.xls","Compare");


SiteURLs()
cleanup();
_setAccessorIgnoreCase(true); 

var $t = _testcase("125458/125459","Verify the display of UI elements in Compare Bucket/navigation to Compare Bucket section  in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to subcat page	
	_click(_link($Compare[0][0]));
	//add at least two items to items to compare
	_check(_checkbox("compare-check"));
	_check(_checkbox("compare-check[1]"));
	//verify whether compare section appeared or not
	_assertVisible(_div("compare-items"));
	//verify the UI
	_assertVisible(_heading2($Compare[0][1]));
	_assertEqual($Compare[1][1], _count("_div","/compare-item-number/",_in(_div("compare-items"))));
	//compare items button
	_assertVisible(_submit("compare-items-button"));
	//clear all
	_assertVisible(_submit("clear-compared-items"));
	var $totalCompareItems=_count("_div","/compare-item active/",_in(_div("compare-items-panel")));
	//remove link
	for(var $i=0;$i<$totalCompareItems;$i++)
		{
		_assertVisible(_link("compare-item-remove["+$i+"]"));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125460","Verify the functionality of 'Remove(X)' image in Compare image widget box  in the application both as an anonymous and a registered user");
$t.start();
try
{
	//fetch total no.of added compare items
	var $totalCompareItems=_count("_div","/compare-item active/",_in(_div("compare-items-panel")));
	_log("$totalCompareItems"+$totalCompareItems);
	//Total check boxes
	var $totalCheckboxes=_count("_checkbox","/on/");
	var $CompareItemsBeforeRemoval=$totalCompareItems;
	var $CompareItemsAfterRemoval=0;
	for(var $i=0;$i<$totalCompareItems;$i++)
		{
			if($i==$totalCompareItems-1)
			{
			//compare items button shouldn't clickable
			_assertEqual($Compare[2][1],_style(_submit("compare-items-button"),"color"));
			//click on compare button 
			_click(_submit("compare-items-button"));
			//shouldn't navigate
			_assertNotVisible(_link("back"));
			}
			
			//click on remove link
			_click(_link("compare-item-remove"));
			_wait(2000);
			$CompareItemsAfterRemoval=_count("_div","/compare-item active/",_in(_div("compare-items-panel")));
			//verify after click on remove link
			if($i==$totalCompareItems-1)
				{
					//should navigate back to sub category
					_assertNotVisible(_div("compare-items"));
					_assertContainsText($Compare[0][0], _div("breadcrumb"));
				}
			else
				{
					//Verifying compare items in compare section
					_assertEqual($CompareItemsBeforeRemoval-1,$CompareItemsAfterRemoval);
					//verifying check boxes checked in Products display section
					_assertNotTrue(_checkbox("compare-check["+$i+"]").checked);
				}
		
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125461/125464/125465/148597","Verify the functionality of 'Compare Items' button/verify the Navvigation to/UI in Compare bucket section in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to subcat page	
	_click(_link($Compare[0][0]));
	//add at least two items to items to compare
	_check(_checkbox("compare-check"));
	_check(_checkbox("compare-check[1]"));
	//verify whether compare section appeared or not
	_assertVisible(_div("compare-items"));
	//fetch total no.of added compare items
	var $totalCompareItems=_count("_div","/compare-item active/",_in(_div("compare-items-panel")));
	//click on compare items button
	_click(_submit("compare-items-button"));
	//verify the navigation and UI
	_assertVisible(_heading1("Compare"));
	//back to results links
	var $totalBackToResultLinks=_count("_link","/back to results/");
	_assertEqual($Compare[3][1],$totalBackToResultLinks);
	for(var $i=0;$i<$totalBackToResultLinks;$i++)
		{
		_assertVisible(_link("<< back to results["+$i+"]"));
		}
	//print link
	//_assertVisible(_submit("print-page"));
	//remove,name,images links
	for(var $i=0;$i<$totalCompareItems;$i++)
		{
			_assertVisible(_link("remove-link["+$i+"]"));
			_assertVisible(_link("name-link["+$i+"]"));
			//_assertVisible(_image("/(.*)/",_near(_link("name-link["+$i+"]"))));
			_assertVisible(_div("product-image", _in(_div("product-tile["+$i+"]"))));
			//add to cart
			_assertVisible(_submit("Add to Cart["+$i+"]"));
			//add to wish list
			_assertVisible(_link("Add to Wishlist["+$i+"]"));
			//add to gift registry
			_assertVisible(_link("Add to Gift Registry["+$i+"]"));
			
			//price label [Is removed in the application so commenting below snippet of code]
			//_assertVisible(_span("product-sales-price["+$i+"]"));
			//colour Swatch
			//_assertVisible(_image("swatch-image["+$i+"]"));	
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("125462","Verify the functionality of 'Clear All' button in Compare bucket section  in the application both as an anonymous and a registered user");
$t.start();
try
{
	//click on back results
	_click(_link("back"));
	//click on clear all button
	_click(_submit("clear-compared-items"));
	//compare section shouln't visible
	_assertNotVisible(_div("compare-items"));
	_assertNotVisible(_heading2($Compare[0][1]));
	_assertNotVisible(_submit("compare-items-button"));
	_assertNotVisible(_submit("clear-compared-items"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125463","Verify the functionality of 'Comparison Grid' by adding more then 6 products to the Compare image widget box in the application both as an anonymous and a registered user");
$t.start();
try
{	
	//add 6 items for compare
	for(var $i=0;$i<$Compare[1][1];$i++)
		{
		//check the compare check box
		_check(_checkbox("compare-check["+$i+"]"));
		}
	//try add one more product for comparision
	_check(_checkbox("compare-check[6]"));
	//pop up should come click on cancel
	_expectConfirm("/This will remove/",false);
	//should remain in same page
	_assertVisible(_div("compare-items"));
	//add one more product for comparision
	_check(_checkbox("compare-check[6]"));
	//click on ok
	_expectConfirm("/This will remove/",true);
	//first item should replace 
	 _assertFalse(_checkbox("compare-check").checked)
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125466","Verify the navigation of 'Back to Results' link in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//click on compare items button
	_click(_submit("Compare Items"));	
	//fetch product name
	var $pName=_getText(_link("name-link"));
	//click on back to results link
	_click(_link("back"));
	//verify the navigation back to sub category page
	_assertVisible(_link($Compare[0][0], _in(_div("breadcrumb"))));
	_assertVisible(_submit("Compare Items"));
	_assertVisible(_submit("clear-compared-items"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("125467","Verify the functionality of 'Remove(X)' image in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to sub category	
	_click(_link($Compare[0][0]));	
	//click on compare items button
	_click(_submit("compare-items-button"));
	//fetch total no.of remove links
	var $RemoveLinks=_count("_link","/remove-link/");_log($RemoveLinks); 
	for(var $i=0;$i<$RemoveLinks;$i++)
		{
			_log($i);
			var $removeLinksBeforeClick=_count("_link","/remove-link/");
			if($i==$RemoveLinks-1)
				{
					//click on remove link
					_click(_link("remove-link"));
					//should navigate back to sub category
					_assertVisible(_div("breadcrumb"));
				}
			else
				{
					//click on remove link
					_click(_link("remove-link"));
					$removeLinksBeforeClick--;
					_wait(5000);
					var $removeLinks=_count("_link","/remove-link/");
					_assertEqual($removeLinksBeforeClick,$removeLinks);
				}
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("148590","Verify the mouse hover functionality if user place the mouse over on any Product image in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to sub category	
	_click(_link($Compare[0][0]));
	//add at least two items to items to compare
	_check(_checkbox("compare-check"));
	_check(_checkbox("compare-check[1]"));
	//fetch total no.of added compare items
	var $totalCompareItems=_count("_div","/compare-item active/",_in(_div("compare-items-panel")));
	//click on compare items button
	_click(_submit("compare-items-button"));
	//verify the quick view functionality
	for(var $i=0;$i<$totalCompareItems;$i++)
		{
			_mouseOver(_div("product-image["+$i+"]"));
			var $pName=_getText(_link("name-link["+$i+"]"));
			_wait(2000);
			//click on quick view overlay
			_click(_link("quickviewbutton",_div("product-image["+$i+"]")));
			_wait(2000);
			//verify whether overlay opened or not 
			_assertVisible(_heading1("product-name"));
			_assertEqual($pName, _getText(_heading1("product-name")));
			_assertVisible(_div("/QuickViewDialog/"));
			_assertVisible(_span("/ui-icon ui-icon-closethick/"));
			//close the overlay
			_click(_span("/ui-icon ui-icon-closethick/"));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148591","Verify the navigation if user click on any product image or product name link in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
for(var $i=0;$i<$totalCompareItems;$i++)
	{
		var $pName=_getText(_link("name-link["+$i+"]"));
		//click on name link
		_click(_link("name-link["+$i+"]"));
		//verify the navigation
		_assertVisible(_div("breadcrumb"));
		_assertContainsText($pName, _div("breadcrumb"));
		_assertVisible(_heading1("product-name"));
		_assertEqual($pName, _getText(_heading1("product-name")));
		//navigate to sub category	
		_click(_link($Compare[0][0]));
		//click on compare items button
		_click(_submit("compare-items-button"));
		//click on product image link
		_click(_image("/(.*)/",_in(_div("product-image["+$i+"]"))));
		//verify the navigation
		_assertVisible(_div("breadcrumb"));
		_assertContainsText($pName, _div("breadcrumb"));
		_assertVisible(_heading1("product-name"));
		_assertEqual($pName, _getText(_heading1("product-name")));
		//navigate to sub category	
		_click(_link($Compare[0][0]));
		//click on compare items button
		_click(_submit("compare-items-button"));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148593","Verify the functionality if user click on 'ADD TO CART' button in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to sub category	
	_click(_link($Compare[0][0]));
	//click on compare items button
	_click(_submit("compare-items-button"));
	//click on add to cart button
	//verify the add to cart functionality
	for(var $i=0;$i<$totalCompareItems;$i++)
		{
		_mouseOver(_div("product-image["+$i+"]"));
		var $pName=_getText(_link("name-link["+$i+"]"));
		_wait(2000);
		//click on quick view overlay
		_click(_link("quickviewbutton",_div("product-image["+$i+"]")));
		_wait(2000);
		//verify whether overlay opened or not 
		_assertVisible(_heading1("product-name"));
		_assertEqual($pName, _getText(_heading1("product-name")));
		_assertVisible(_div("/QuickViewDialog/"));
		_assertVisible(_span("/ui-icon ui-icon-closethick/"));
		//close the overlay
		_click(_span("/ui-icon ui-icon-closethick/"));
		} 
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148595","Verify the navigation if user click on 'Add to Wish list' button in Compare Products page as guest user in the application both as an anonymous user");
$t.start();
try
{
	for(var $i=0;$i<$totalCompareItems;$i++)
		{	
		//click on add to wish list link
		_click(_link("Add to Wishlist["+$i+"]"));
		//verify the navigation
		_assertVisible(_link($Compare[0][2], _in(_div("breadcrumb"))));
		_assertVisible(_heading1($Compare[1][2]));
		//navigate to sub category	
		_click(_link($Compare[0][0]));
		//click on compare items button
		_click(_submit("compare-items-button"));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148596","Verify the functionality if user select any value from 'Other Compare Items' drop down in Compare Products page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//navigate to other category and add some products for comparision
	_click(_link($Compare[1][0]));
	//add at least two items to items to compare
	_check(_checkbox("compare-check"));
	_check(_checkbox("compare-check[1]"));
	_check(_checkbox("compare-check[2]"));
	//click on compare items button
	_click(_submit("compare-items-button"));
	//'Other Compare Items'
	_assertVisible(_label($Compare[0][3]));
	_assertVisible(_select("compare-category-list"));
	//select the suits category from drop down
	_setSelected(_select("compare-category-list"),$Compare[0][0]);
	_assertEqual($Compare[3][1],_count("_link","/Remove/"));
	//select the suits category from drop down
	_setSelected(_select("compare-category-list"),$Compare[1][0]);
	_assertEqual($Compare[4][1],_count("_link","/Remove/"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//login to the application
_click(_link("Login"));
login();
//navigate to wish list and delete all added items 
_click(_link("Wish List"));
while(_isVisible(_submit("Remove")))
	{
	_click(_submit("Remove"));
	}

var $t = _testcase("148594","Verify the navigation if user click on 'Add to Wish list' button in Compare Products page as guest user in the application both as an anonymous user");
$t.start();
try
{
	//navigate to sub category	
	_click(_link($Compare[0][0]));	
	//click on compare items button
	_click(_submit("compare-items-button"));
	for(var $i=0;$i<$totalCompareItems;$i++)
		{	
		var $pName=_getText(_link("name-link["+$i+"]"));
		//click on add to wish list link
		_click(_link("Add to Wishlist["+$i+"]"));
		//verify the navigation
		_assertVisible(_link($Compare[0][2], _in(_div("breadcrumb"))));
		_assertVisible(_heading1($Compare[2][2]));
		_assertVisible(_link($pName));
		//navigate to sub category	
		_click(_link($Compare[0][0]));
		//click on compare items button
		_click(_submit("compare-items-button"));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


cleanup();
