_include("../../util.GenericLibrary/GlobalFunctions.js");
_resource("Homepage.xls");

var $data=_readExcelFile("Homepage.xls","Homepage");
var $footer_data=_readExcelFile("Homepage.xls","FooterLinks");
var $emailsignup=_readExcelFile("Homepage.xls","EmailSubsciption");

SiteURLs();
_setAccessorIgnoreCase(true);
cleanup();


var $t = _testcase("124676","Verify the UI of global header in the application as an anonymous user");
$t.start();
try
{
//UI of global header(demand ware image)
_assertVisible(_image("logo.png"));
//login
_click(_link("user-account"));
_assertVisible(_link("Login"));
//currency
//_assertVisible(_select("currencyMnemonic"));
//cart
_assertVisible(_div("mini-cart"));
_assertVisible(_link("Stores"));
_assertVisible(_link("Help"));
//Promo message
//FREE 2-Day SHIPPING FOR ORDERS OVER $300 promo message at home page
_assertVisible(_div($data[1][0]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148553","Verify the UI of global footer in the application both as an anonymous and a registered user");
$t.start();
try
{
	//Email textbox & Submit button
	_assertVisible(_textbox("email-alert-address"));
	_assertVisible(_submit("home-email"));
	//verify the UI of global footer
	_assertVisible(_link("MY ACCOUNT"));
	_assertVisible(_link("CHECK ORDER"));
	_assertVisible(_link("Help"));
	_assertVisible(_link("CONTACT US"));
	_assertVisible(_link("ABOUT US"));
	_assertVisible(_link("TERMS"));
	_assertVisible(_link("PRIVACY"));
	_assertVisible(_link("SITE MAP"));
	_assertVisible(_link("JOBS"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("148554","Verify the UI of body of the home page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//'Categories'
	_assertVisible(_list("menu-category level-1"));
	//Search
	_assertVisible(_textbox("/Search/"));
	_assertVisible(_label("/Search/"));
	//Home slider
	_assertVisible(_list("homepage-slides"));
	_assertVisible(_div("home-bottom-slots"));
	// '3 promotional slots' 
	_assertVisible(_div("home-bottom-left"));
	_assertVisible(_div("home-bottom-center"));
	_assertVisible(_div("home-bottom-right"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("124285/124679","Verify the system response on click of root category in the application both as an anonymous and a registered user. and Verify the Navigation of Sub Category' links at home page in  the application both as an anonymous and a registered user");
$t.start();
try
{
	//Mouse Over on  root category
	_mouseOver(_link($data[0][1]));
	
	//Root Category Bread crumb has removed
	
//	_click(_link($data[0][1]));
//	_assertVisible(_link($data[0][1], _in(_div("breadcrumb"))));
	
	//Nav to Home page
	_click(_image("Salesforce Commerce Cloud SiteGenesis"));
	_mouseOver(_link($data[0][1]));
	//Click on Subcat link
	_click(_link($data[1][1]));
	//Should Navigate to respective sub-category landing page
	_assertVisible(_link($data[1][1], _in(_div("breadcrumb"))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("124682/124685","Verify the navigation of links in the category menu and Verify the navigation of Gift Certificate and Top Seller link.");
$t.start();
try
{
	//Nav to Home page
	_click(_image("Salesforce Commerce Cloud SiteGenesis"));
	var $RootCategory= _collectAttributes("_link", "/has-sub-menu/", "sahiText", _in(_list("menu-category level-1")));
	for(var $i=0;$i<$RootCategory.length;$i++)
		{
			_click(_link("has-sub-menu["+$i+"]", _in(_list("menu-category level-1"))));
			_assertVisible(_span("Shop "+$RootCategory[$i]));			  
		}   
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("124683","Verify system's response when user clicks on any of the sub category links.");
$t.start();
try
{
	//Nav to Home page
	_click(_image("Salesforce Commerce Cloud SiteGenesis"));
	for(var $i=0;$i<$data.length;$i++)
		{
			_click(_link($data[$i][5], _in(_list("menu-category level-1"))));
			_assertContainsText($data[$i][5], _div("breadcrumb"));  
		}   
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("124684","Verify the navigation of links in the child categories(sub �sub categories) .");
$t.start();
try
{
	//Nav to Home page
	_click(_image("Salesforce Commerce Cloud SiteGenesis"));	
	for(var $i=0;$i<$data.length;$i++)
		{
			_click(_link($data[$i][3], _in(_list("menu-category level-1"))));
			_assertContainsText($data[$i][3], _div("breadcrumb"));  
		}   
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("148555/148562","Verify the functionality of 'LOGIN'/'Store locator' link at global header in the application as an anonymous user");
$t.start();
try
{
	//Nav to Home page
	_click(_image("Salesforce Commerce Cloud SiteGenesis"));
	//Click on login link from header
	_click(_link("Login"));
	//Should Navigate to My Account Login Page
	_assertVisible(_div("login-box login-account"));
	_assertVisible(_link("My Account", _in(_div("breadcrumb"))));
	//Nav to Home page
	_click(_image("Salesforce Commerce Cloud SiteGenesis"));
	//Click on Store Locator link
	_click(_link("Stores"));
	//Should navigate to Store locator page
	_assertVisible(_heading1($data[0][2]));
	_assertVisible(_link("Stores", _in(_div("breadcrumb"))));
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

var $t=_testcase("124694/148563/148564/148565/148566/148567/148568/148569/148570/148571","Verify the system response on click of  'Home'/'My Account'/'Check Order'/'Help'/'Contact Us'/'About Us'/'Terms'/'Privacy'/'sitemp'/'Jobs'  links at global footer in the application both as an anonymous and a registered user");
$t.start();
try
{
	for(var $i=0; $i<$footer_data.length; $i++)
		{
			//Nav to Home page
			_click(_image("Salesforce Commerce Cloud SiteGenesis"));
			
			//Clicking on Respective links from footer
			_click(_link($footer_data[$i][0],_in(_div("footer-container"))));
			//Verifying Breadcrumb and Heading to check whether its Navigated to respective pages or not.
			_assertVisible(_heading1("/"+$footer_data[$i][1]+"/"));
			_assertContainsText("/"+$footer_data[$i][2]+"/", _div("breadcrumb"));
		}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



_log("The Defect Need to be fixed for Email Sign up functionality in Home page");
var $t=_testcase("148572/148573","Verify the validation of email field on home page at the appliction both as an anonymous and a registered user and Verify the functionality of suscribe button at home page in the application both as an anonymous and a registered user");
$t.start();
try
{
	//Nav to Home page
	_click(_image("Salesforce Commerce Cloud SiteGenesis"));
	for(var $i=0; $i<$emailsignup.length; $i++)
		{
			_setValue(_textbox("dwfrm_emailsubscribe_email"),$emailsignup[$i][1]);
			_click(_submit("home-email"));
			// The Defect Need to be fixed for Email Sign up functionality in Home page		
			if($i==0)
				{
					//Text box should be highlighted with red color
					_assertEqual($emailsignup[0][3],_style(_textbox("dwfrm_emailsubscribe_email"),"background-color"))
				}
			else if($i==1)
				{
					//Verifying Error message and Font Color of Error message
					_assertEqual($emailsignup[$i][2],_getText(_span("error", _in(_div("home-bottom")))));
					_assertEqual($emailsignup[1][3],_style(_span("error", _in(_div("home-bottom"))),"color"));			
				}
			else
				{
					//Valid
					_assertNotVisible(_span("error", _in(_div("home-bottom"))));
				}
		}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("124681","Verify the functionality of 'CART' link at global header in the application both as an anonymous and a registered user.");
$t.start();
try
{	
	//Adding item to the cart
	addItemToCart($data[1][1],0);
	_click(_submit("add-to-cart"));
	_mouseOver(_link("mini-cart-link"));
	//Minicart Overlay Should be displayed
	_assertVisible(_div("mini-cart-content"));	
	//Clear the cart for verifying Empty cart
	ClearCartItems();
	//Mouse hover and click functionality should not happen. And products present should be 0
	_assertNotVisible(_div("mini-cart-content"));

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/* -----Below TCs for Logged in User-------*/
_click(_link("Login"));
login();


var $t=_testcase("148552","Verify the UI of global header in the application as a registered user");
$t.start();
try
{
	//Nav to Home page
	_click(_image("Salesforce Commerce Cloud SiteGenesis"));	
	//login
	_click(_link("user-account"));
	_assertVisible(_link("My Account"));
	//currency
	//_assertVisible(_select("currencyMnemonic"));
	//Minicart Label
	_assertVisible(_div("mini-cart"));
	_assertVisible(_link("Stores"));
	_assertVisible(_link("Help"));
	//Promo message
	//FREE 2-Day SHIPPING FOR ORDERS OVER $300 promo message at home page
	_assertVisible(_div($data[1][0]));	
	//Nav to Home page
	_click(_image("Salesforce Commerce Cloud SiteGenesis"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
cleanup();