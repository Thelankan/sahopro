_include("../util.GenericLibrary/BrowserSpecific.js");
_include("../util.GenericLibrary/GlobalFunctions.js");
_resource("StaticPages.xls");

var $Links=_readExcelFile("StaticPages.xls","Links");
var $SiteMap=_readExcelFile("StaticPages.xls","SiteMap");
var $ConactUs_Validations=_readExcelFile("StaticPages.xls","ConactUs_Validations");


SiteURLs()
cleanup();
_setAccessorIgnoreCase(true);


//Related to Help link

var $t = _testcase("124359/124361/125868/148792", "Verify the navigation related to 'Help' link  present at  global footer in application ' both as an  Anonymous and Registered user & " +
					"Verify the navigation related to 'My Account' and 'Customer Service' 'links  in breadcrumb on help page  in application both as an  Anonymous and Registered user. ");
$t.start();
try
{
	
//Navigate to Home page
_click(_link("Demandware SiteGenesis"));

//Verifying Navigation for Help link in global footer
if (_isVisible(_link($Links[0][0], _in(_div("footer-container")))))
	{
		//click on help link in global footer
		_click(_link($Links[0][0], _in(_div("footer-container"))));
		//Verifying bread crumb and heading
		//_assertEqual($Links[0][1],_getText(_link("breadcrumb-last")));
		_assertVisible(_link($Links[0][1], _in(_div("breadcrumb"))));
		_assertVisible(_heading1($Links[0][2]));
		
		//################## Home Link is not there in Breadcrumb ##########
		
//		//Click on home link in bread crumb
//		_click(_link("Home", _in(_div("breadcrumb"))));
//		//Verifying UI of home page 
//		_assertVisible(_image("Salesforce Commerce Cloud SiteGenesis"));
//		_assertEqual(false,_isVisible(_div("breadcrumb")));
//		_assertVisible(_textbox("Search"));
//		_assertVisible(_list("homepage-slides"));
		
		
		//again click on help link in global footer
		_click(_link($Links[0][0], _in(_div("footer-container"))));
		//Verifying bread crumb and heading
		//_assertEqual($Links[0][1],_getText(_link("breadcrumb-last")));
		_assertVisible(_link($Links[0][1], _in(_div("breadcrumb"))));
		_assertVisible(_heading1($Links[0][2]));
	}
	else
		{
		_assert(false,"Help link in not present in global footer");
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
 $t.end();
 
  var $t = _testcase("124360/124838", "Verify the UI  of 'Customer Service' page in application both as an  Anonymous and Registered user &" +
 			"Verify the functionality related to  down arrow present in left Nav on help page in application as an Anonymous and Registered user. ");
 $t.start();
 try
 {

//Checking bread crumb and heading	 
//_assertEqual($Links[0][1],_getText(_link("breadcrumb-last")));
_assertVisible(_link($Links[0][1], _in(_div("breadcrumb"))));
_assertVisible(_heading1($Links[0][2]));
//Checking UI of left Nav and down arrow functionality
if (_isVisible(_div("secondary-navigation")))
	{
	LeftNav_UI();
	}
	else
		{
		_log("Left Nav section is not present");
		}
	
 }
 catch($e)
 {
 	_logExceptionAsFailure($e);
 }	
  $t.end();
 
  
  var $t = _testcase("124933,124935,124938,124941,124943,124945,124948,124953", "Verify the navigation related to left nav links on help page in application both as an  Anonymous and Registered user.");
  $t.start();
  try
  {
	  
//Navigate to Home page
_click(_link("Demandware SiteGenesis"));
_click(_link($Links[0][0], _in(_div("footer-container"))));

 //Checking Navigation of left nav links present in Costumer service page
  for(var $j=0;$j<$Links.length;$j++)
	{
	  
  	_click(_link($Links[$j][3], _in(_div("secondary-navigation"))));
  	//Verifying Heading
  	_assertVisible(_heading1($Links[$j][5]));
 	//Verifying Bread crumb
  	//_assertVisible(_link($Links[$j][6], _in(_list("breadcrumb"))));
	_assertVisible(_link($Links[$j][6], _in(_div("breadcrumb"))));
  	//Navigating back to help link 
  	_click(_link("Help"));

	}
  }
 catch($e)
  {
  	_logExceptionAsFailure($e);
  }	
   $t.end(); 
   
   
   
var $t = _testcase("124842/124857", "Verify the navigation related to 'FAQ's' link present at on Contact Us page left nav in application both as an Anonymous and Registered user." +
   		"Verify the UI of 'FAQ's' page in application both as an Anonymous and Registered user."); 
   $t.start();
   try
   {

//Navigate to Home page
_click(_link("Demandware SiteGenesis"));
_click(_link($Links[0][0], _in(_div("footer-container"))));
//click on FAQ link in left nav
_click(_link($Links[0][3], _in(_div("secondary-navigation"))));
//click on faq link in bread crumb
//_click(_link($Links[0][3], _in(_list("breadcrumb"))));
_assertVisible(_link($Links[0][3], _in(_div("breadcrumb"))));
//Verifying Heading
_assertVisible(_heading1($Links[0][5]));
//Verifying Bread crumb
//_assertVisible(_link($Links[0][6], _in(_list("breadcrumb"))));
_assertVisible(_link($Links[0][6], _in(_div("breadcrumb"))));


//################## Home Link is not there in Breadcrumb ##########

////Click on home link in bread crumb
//_click(_link("Home", _in(_div("breadcrumb"))));
////Verifying UI of home page 
//_assertVisible(_image("Salesforce Commerce Cloud SiteGenesis"));
//_assertEqual(false,_isVisible(_div("breadcrumb")));
//_assertVisible(_textbox("Search"));
//_assertVisible(_list("homepage-slides"));
  
  
   }
   catch($e)
   {
   _logExceptionAsFailure($e);
   }	
   $t.end();
   
   
   
 var $t = _testcase("124922/124924", "Verify the navigation related to 'Checkout' link present at on Contact Us page left nav in application both as an Anonymous and Registered user" +
   			"Verify the navigation related to the click here link in Checkout page in application as an Anonymous and Registered user."); 
   $t.start();
   try
   {

	   
//Click on help link in footer
_click(_link($Links[0][0], _in(_div("footer-container")))); 
_click(_link($Links[3][3], _in(_div("secondary-navigation"))));
//Verifying Heading
_assertVisible(_heading1($Links[3][5]));
//Verifying Bread crumb
//_assertVisible(_link($Links[3][6], _in(_list("breadcrumb"))));
_assertVisible(_link($Links[3][6], _in(_div("breadcrumb"))));
//Click on (Click Here) link in checkout page
_click(_link("/click here/", _in(_div("content-asset"))));
_assertVisible(_div("breadcrumb"));
_assertVisible(_link("Shipping Methods", _in(_div("breadcrumb"))));
_assertVisible(_heading1("Shipping Methods"));
  
   }
   catch($e)
   {
   _logExceptionAsFailure($e);
   }	
   $t.end();
   

var $t = _testcase("124928", "Verify the navigation related to the Return' or 'Return Policy link in Changing/Canceling Orders page in application as an Anonymous and Registered user. "); 
   $t.start();
   try
   {
   	   
//Click on help link in footer
_click(_link("HELP", _in(_div("footer-container"))));
//click on Changing/Canceling Orders
_click(_link($Links[4][3], _in(_div("secondary-navigation"))));
//click on  Return Policy link in Changing/Canceling Orders page
_click(_link($Links[0][7]));
_assertVisible(_div("breadcrumb"));
//_assertVisible(_link($Links[1][7], _in(_list("breadcrumb"))));
_assertVisible(_link($Links[1][7], _in(_div("breadcrumb"))));
_assertEqual($Links[2][7], _getText(_heading1("content-header")));
	 
   }
   catch($e)
   {
   _logExceptionAsFailure($e);
   }	
   $t.end();
   
   
   var $t = _testcase("124957", "Verify the navigation related to 'links in breadcrumb on Opening An Account page in application both as an Anonymous and Registered user."); 
   $t.start();
   try
   {
   
	   	//click on help link in global footer
		_click(_link($Links[0][0], _in(_div("footer-container"))));
		//Verifying bread crumb and heading
		//_assertEqual($Links[0][1],_getText(_link("breadcrumb-last")));
		_assertVisible(_link($Links[0][1], _in(_div("breadcrumb"))));
		//click on Opening An Account link i left nav
		_click(_link($Links[11][3], _in(_div("secondary-navigation"))));
		//click on opening an account link in bread crumb 
		_click(_link($Links[11][6], _in(_div("breadcrumb"))));
	  	//Verifying Heading
	  	_assertVisible(_heading1($Links[11][5]));
	 	//Verifying Bread crumb
	  	//_assertVisible(_link($Links[11][6], _in(_list("breadcrumb"))));
	  	_assertVisible(_link($Links[11][6], _in(_div("breadcrumb"))));
	  	
	  //################## Home Link is not there in Breadcrumb ##########
	  	
//		//Click on home link in bread crumb
//		_click(_link("Home", _in(_div("breadcrumb"))));
//		//Verifying UI of home page 
//		_assertVisible(_image("Salesforce Commerce Cloud SiteGenesis"));
//		_assertEqual(false,_isVisible(_div("breadcrumb")));
//		_assertVisible(_textbox("Search"));
//		_assertVisible(_list("homepage-slides"));

   }
   catch($e)
   {
   _logExceptionAsFailure($e);
   }	
   $t.end();
 
   
 //Test Cases related to Contact Us Page 
   
 var $t = _testcase("124362/124387/124385/124831", "Verify the navigation related to 'Contact Us' link  present at  global footer in application both as an  Anonymous and Registered user." +
 		"Verify the navigation related to 'links in breadcrumb on contact us page in application both as an Anonymous and Registered user.");
 $t.start();
 try
   { 	
//Navigate to Home page
_click(_link("Demandware SiteGenesis"));

//Verifying Navigation for Contact us link in global footer
if (_isVisible(_link($Links[1][0], _in(_div("footer-container")))))
{
	_click(_link($Links[1][0], _in(_div("footer-container"))));
	_assertVisible(_div("breadcrumb"));
	//_assertEqual($Links[1][1],_getText(_link("breadcrumb-last")));
	_assertVisible(_link($Links[1][1], _in(_div("breadcrumb"))));
	_assertVisible(_heading1($Links[1][2]));
	_assertNotVisible(_link("Home", _in(_div("breadcrumb"))));
	
	//################## Home Link is not there in Breadcrumb ##########
	
//	_click(_link("Home", _in(_div("breadcrumb"))));
//	//Verifying UI of home page 
//	_assertVisible(_image("Salesforce Commerce Cloud SiteGenesis"));
//	_assertEqual(false,_isVisible(_div("breadcrumb")));
//	_assertVisible(_textbox("Search"));
//	_assertVisible(_list("homepage-slides"));
	
	
	//again click on help link in global footer
	_click(_link($Links[1][0], _in(_div("footer-container"))));
	//Verifying bread crumb and heading
	//_assertEqual($Links[1][1],_getText(_link("breadcrumb-last")));
	_assertVisible(_link($Links[1][1], _in(_div("breadcrumb"))));
	_assertVisible(_heading1($Links[1][2]));
}
else
	{
	_assert(false,"Contact us link in not present in global footer");
	}
   	
   }
   catch($e)
   {
   	_logExceptionAsFailure($e);
   }	
    $t.end();
    
    
    
  var $t = _testcase("124363", "Verify the UI  of 'Contact us' page in application both as an  Anonymous and Registered user."); 
    $t.start();
    try
    {
    	//Verifying bread crumb and heading
    	//_assertEqual($Links[1][1],_getText(_link("breadcrumb-last")));
    	_assertVisible(_link($Links[1][1], _in(_div("breadcrumb"))));
    	_assertVisible(_heading1($Links[1][2]));
    	//Checking UI contact us section
    	_assertVisible(_span("First Name"));
    	_assertVisible(_textbox("dwfrm_contactus_firstname"));
    	_assertVisible(_span("Last Name"));
    	_assertVisible(_textbox("dwfrm_contactus_lastname"));
    	_assertVisible(_span("E-Mail"));
    	_assertVisible(_textbox("dwfrm_contactus_email"));
    	_assertVisible(_span("Phone"));
    	_assertVisible(_textbox("dwfrm_contactus_phone"));
    	_assertVisible(_span("Order Number"));
    	_assertVisible(_textbox("dwfrm_contactus_ordernumber"));
    	_assertVisible(_span("My Question"));
    	_assertVisible(_select("dwfrm_contactus_myquestion"));
    	_assertVisible(_span("Comment"));
    	_assertVisible(_textarea("dwfrm_contactus_comment"));
    	_assertVisible(_submit("dwfrm_contactus_send"));

    }
    catch($e)
    {
    _logExceptionAsFailure($e);
    }	
    $t.end();
       
var $t = _testcase("124384", "Verify the navigation related to left nav links on contact us page in application both as an  Anonymous and Registered user.");
$t.start();
 try
 {
	 
  //Navigate to Home page
  _click(_link("Demandware SiteGenesis"));
  _click(_link($Links[1][0], _in(_div("footer-container"))));
  
//Checking Navigation of left nav links present in Costumer service page
   for(var $j=0;$j<$Links.length;$j++)
  	{
  	  
    	_click(_link($Links[$j][3], _in(_div("secondary-navigation"))));
    	//Verifying Heading
    	_assertVisible(_heading1($Links[$j][5]));
    	//Verifying Bread crumb
    	_assertVisible(_link($Links[$j][6], _in(_div("breadcrumb"))));
    	//Navigating back to contact us page 
    	_click(_link($Links[1][0], _in(_listItem($Links[1][0]))));
  	}
  }
   catch($e)
    {
    	_logExceptionAsFailure($e);
    }	
     $t.end();
     
            
var $t = _testcase("124836", "Verify the links displayed in Left Nav of the Static pages");
$t.start();
try
{
 //Navigate to Home page
 _click(_link("Demandware SiteGenesis"));
 _click(_link($Links[1][0], _in(_div("footer-container"))));	
_assertVisible(_link($Links[1][1], _in(_div("breadcrumb"))));
_assertVisible(_heading1($Links[1][2]));

_click(_link("Customer Service", _in(_div("breadcrumb"))));
	
//Checking UI of left Nav and down arrow functionality
if (_isVisible(_div("secondary-navigation")))
	{
	LeftNav_UI();
	}
	else
		{
		_log("Left Nav section is not present");
		}
}
catch($e)
{
_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124364/124365/148793/148794/148795/148796/148797/148798", "Verify the validation related to First name/Last Name/Email/Phone/order number/my question/comment/Submit  field   on contact us page in application both as an Anonymous and Registered user."); 
$t.start();
try
{
	
	_click(_link($Links[1][0], _in(_div("footer-container"))));
	
	var $length=$ConactUs_Validations.length;
	
	for(var $i=0;$i<$length;$i++)
	{
		_log($i);
		_setValue(_textbox("dwfrm_contactus_firstname"),$ConactUs_Validations[$i][1]);
		_setValue(_textbox("dwfrm_contactus_lastname"),$ConactUs_Validations[$i][2]);
		_setValue(_textbox("dwfrm_contactus_email"),$ConactUs_Validations[$i][3]);
		_setValue(_textbox("dwfrm_contactus_phone"),$ConactUs_Validations[$i][4]);
		_setValue(_textbox("dwfrm_contactus_ordernumber"),$ConactUs_Validations[$i][5]);
		_setSelected(_select("dwfrm_contactus_myquestion"),$ConactUs_Validations[$i][6]);
		_setValue(_textarea("dwfrm_contactus_comment"),$ConactUs_Validations[$i][7]);
		_click(_submit("dwfrm_contactus_send"));
		
		if ($i==0)
			{
			//_assertEqual($ConactUs_Validations[0][8],_style(_span("First Name"),"color"));
			//_assertEqual($ConactUs_Validations[0][8],_style(_span("Last Name"),"color"));
			//_assertEqual($ConactUs_Validations[0][8],_style(_span("E-Mail"),"color"));
			_assertEqual($ConactUs_Validations[0][8],_style(_textbox("dwfrm_contactus_firstname"),"background-color"));
			_assertEqual($ConactUs_Validations[0][8],_style(_textbox("dwfrm_contactus_lastname"),"background-color"));
			_assertEqual($ConactUs_Validations[0][8],_style(_textbox("dwfrm_contactus_email"),"background-color"));
			_assertEqual($ConactUs_Validations[0][9],_getText(_span("error",_near(_textbox("dwfrm_contactus_firstname")))));
			_assertEqual($ConactUs_Validations[0][9],_getText(_span("error",_near(_textbox("dwfrm_contactus_lastname")))));
			_assertEqual($ConactUs_Validations[0][9],_getText(_span("error",_near(_textbox("dwfrm_contactus_email")))));
			
			}
			 else if($i==1)
	 		 {
		 		_assertEqual($ConactUs_Validations[1][8],_getText(_textbox("dwfrm_contactus_firstname")).length);
		 		_assertEqual($ConactUs_Validations[1][8],_getText(_textbox("dwfrm_contactus_lastname")).length);
		 		_assertEqual($ConactUs_Validations[1][8],_getText(_textbox("dwfrm_contactus_email")).length);
		 		_assertEqual($ConactUs_Validations[1][8],_getText(_textbox("dwfrm_contactus_phone")).length);
		 		_assertEqual($ConactUs_Validations[1][8],_getText(_textbox("dwfrm_contactus_ordernumber")).length);
	 		 }
			 else if($i==2 || $i==3)
				 {
				 //_assertEqual($ConactUs_Validations[3][8], _getText(_div("form-caption error-message")));
				 _assertVisible(_div($ConactUs_Validations[3][8]));
				 _assertEqual($ConactUs_Validations[0][8],_style(_textbox("dwfrm_contactus_email"),"background-color"));
				 _assertEqual($ConactUs_Validations[1][9], _style(_span("dwfrm_contactus_email-error"), "color")
				 

				 }
			 else
				 {
				 _assertVisible(_paragraph($ConactUs_Validations[4][8]));
				 }
	}	
	
}
catch($e)
{
_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("124925", "Verify the navigation related to 'Changing/Canceling Orders' link present at on Contact Us page left nav in application both as an Anonymous and Registered user. "); 
$t.start();
try
{
    _click(_link("Demandware SiteGenesis"));
	_click(_link($Links[1][0], _in(_div("footer-container"))));
	//click on Changing/Canceling Orders
	_click(_link($Links[4][3], _in(_div("secondary-navigation"))));
	//Verifying Heading
	_assertVisible(_heading1($Links[4][5]));
	//Verifying Bread crumb
	_assertVisible(_link($Links[4][6], _in(_div("breadcrumb"))));
	
}
catch($e)
{
_logExceptionAsFailure($e);
}	
$t.end();



//Related to About us link

var $t = _testcase("124366/124368/124367", "Verify the navigation related to 'About us' link  present at  global footer on homepage in application ' both as an  Anonymous and Registered user.& " +
		  "Verify the navigation related to 'My Account', 'Customer Service' and 'About us' 'links  in breadcrumb on About us page in application both as an  Anonymous and Registered user. ");
$t.start();
try
  { 	
  //Navigate to Home page
  _click(_link("Demandware SiteGenesis"));
 //Verifying Navigation for About us link in global footer
  if (_isVisible(_link($Links[2][0], _in(_div("footer-container")))))
  	{
  		_click(_link($Links[2][0], _in(_div("footer-container"))));
  		_assertVisible(_link($Links[2][1], _in(_div("breadcrumb"))));
  		_assertVisible(_heading1($Links[2][2]));
  		
  	//################## Home Link is not there in Breadcrumb ##########
  		
//  		_click(_link("Home", _in(_div("breadcrumb"))));
//		//Verifying UI of home page 
//		_assertVisible(_image("Salesforce Commerce Cloud SiteGenesis"));
//		_assertEqual(false,_isVisible(_div("breadcrumb")));
//		_assertVisible(_textbox("Search"));
//		_assertVisible(_list("homepage-slides"));
  		
		//again click on About us link in global footer
		_click(_link($Links[2][0], _in(_div("footer-container"))));
		//Verifying bread crumb and heading
		_assertVisible(_link($Links[2][1], _in(_div("breadcrumb"))));
		_assertVisible(_heading1($Links[2][2]));
  	}
  	else
  		{
  		_assert(false,"About us link in not present in global footer");
  		}
  	
  }
  catch($e)
  {
  	_logExceptionAsFailure($e);
  }	
   $t.end();
   
   
   
//Related to terms link
   
var $t = _testcase("124369/124371/124370", "Verify the navigation related to 'Terms' link  present at  global footer in application ' both as an  Anonymous and Registered user & " +
		 "Verify the navigation related to 'My Account' and 'Customer Service' 'links  in breadcrumb on terms and service page  in application both as an  Anonymous and Registered user.");   
$t.start();
try
{ 	
//Navigate to Home page
_click(_link("Demandware SiteGenesis"));
//Verifying Navigation for terms link link in global footer
if (_isVisible(_link($Links[3][0], _in(_div("footer-container")))))
{
	_click(_link($Links[3][0], _in(_div("footer-container"))));
	_assertVisible(_link($Links[3][1], _in(_div("breadcrumb"))));
	_assertVisible(_heading1($Links[3][2]));
	
	//################## Home Link is not there in Breadcrumb ##########
	
//	_click(_link("Home", _in(_div("breadcrumb"))));
//	//Verifying UI of home page 
//	_assertVisible(_image("Salesforce Commerce Cloud SiteGenesis"));
//	_assertEqual(false,_isVisible(_div("breadcrumb")));
//	_assertVisible(_textbox("Search"));
//	_assertVisible(_list("homepage-slides"));
	
	
	//again click on terms link in global footer
	_click(_link($Links[3][0], _in(_div("footer-container"))));
	//Verifying bread crumb and heading
	_assertVisible(_link($Links[3][1], _in(_div("breadcrumb"))));
	_assertVisible(_heading1($Links[3][2]));
}
else
	{
	_assert(false,"Terms link in not present in global footer");
	}
}
catch($e)
{
_logExceptionAsFailure($e);
}	
$t.end(); 


//Related to privacy link in global footer
var $t = _testcase("124373/124375/124823", "Verify the navigation related to 'PRIVACY' link  present at  global footer in application ' both as an  Anonymous and Registered user &" +
		"Verify the navigation related to 'My Account' and 'Customer Service' 'links  in breadcrumb on  'Privacy Policy' page  in application both as an  Anonymous and Registered user"); 
$t.start();
try
{

//Navigate to Home page
_click(_link("Demandware SiteGenesis"));
	//Verifying Navigation for privacy link link in global footer
	if (_isVisible(_link($Links[4][0], _in(_div("footer-container")))))
	{
		_click(_link($Links[4][0], _in(_div("footer-container"))));
		_assertVisible(_link($Links[4][1], _in(_div("breadcrumb"))));
		_assertVisible(_heading1($Links[4][2]));
		
		//################## Home Link is not there in Breadcrumb ##########
		
//		_click(_link("Home", _in(_div("breadcrumb"))));
//		//Verifying UI of home page 
//		_assertVisible(_image("Salesforce Commerce Cloud SiteGenesis"));
//		_assertEqual(false,_isVisible(_div("breadcrumb")));
//		_assertVisible(_textbox("Search"));
//		_assertVisible(_list("homepage-slides"));
		
		//again click on privacy link in global footer
		_click(_link($Links[4][0], _in(_div("footer-container"))));
		//Verifying bread crumb and heading
		_assertVisible(_link($Links[4][1], _in(_div("breadcrumb"))));
		_assertVisible(_heading1($Links[4][2]));
	}
	else
		{
		_assert(false,"privacy link in not present in global footer");
		}	

}
catch($e)
{
_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("124825", "Verify the UI elements in the Privacy policy page."); 
$t.start();
try
{

	//UI elements in the Privacy policy page.
	_click(_link($Links[4][0], _in(_div("footer-container"))));
	_assertVisible(_div("breadcrumb"));
	_assertVisible(_link($Links[4][1], _in(_div("breadcrumb"))));
	_assertVisible(_heading1($Links[4][2]));
}
catch($e)
{
_logExceptionAsFailure($e);
}	
$t.end();



//Related to jobs link

var $t = _testcase("124376/124378/124377","Verify the navigation related to 'JOBS' link  present at  global footer in application ' both as an  Anonymous and Registered user.&" +
		"Verify the navigation related to 'My Account' and 'Customer Service' 'links  in breadcrumb on 'JOBS' page  in application both as an  Anonymous and Registered user."); 
$t.start();
try
{

//Navigate to Home page
_click(_link("Demandware SiteGenesis"));
//Verifying Navigation for Jobs link link in global footer
if (_isVisible(_link($Links[5][0], _in(_div("footer-container")))))
{
_click(_link($Links[5][0], _in(_div("footer-container"))));
_assertVisible(_link($Links[5][1], _in(_div("breadcrumb"))));
_assertVisible(_heading1($Links[5][2]));

//################## Home Link is not there in Breadcrumb ##########

//_click(_link("Home", _in(_div("breadcrumb"))));
////Verifying UI of home page 
//_assertVisible(_image("Salesforce Commerce Cloud SiteGenesis"));
//_assertEqual(false,_isVisible(_div("breadcrumb")));
//_assertVisible(_textbox("Search"));
//_assertVisible(_list("homepage-slides"));


//again click on Jobs link in global footer
_click(_link($Links[5][0], _in(_div("footer-container"))));
//Verifying bread crumb and heading
_assertVisible(_link($Links[5][1], _in(_div("breadcrumb"))));
_assertVisible(_heading1($Links[5][2]));
}
else
{
_assert(false,"Jobs link in not present in global footer");
}	

}
catch($e)
{
_logExceptionAsFailure($e);
}	
$t.end();


//Related to Site Map link

var $t = _testcase("124379/124380", "Verify the navigation related to'SITE MAP' link  present at  global footer in application ' both as an  Anonymous and Registered user &" +
		"Verify the navigation related to 'My Account' and 'Customer Service' 'links  in breadcrumb on'SITE MAP' page  in application both as an  Anonymous and Registered user."); 
$t.start();
try
{

//Navigate to Home page
_click(_link("Demandware SiteGenesis"));
//Verifying Navigation for Site map link link in global footer
if (_isVisible(_link($Links[6][0], _in(_div("footer-container")))))
{
	_click(_link($Links[6][0], _in(_div("footer-container"))));
	//_assertEqual($Links[6][1],_getText(_link("breadcrumb-last")));
	_assertVisible(_link($Links[6][1], _in(_div("breadcrumb"))));
	_assertVisible(_heading1($Links[6][2], _in(_div("primary"))));
	
	//################## Home Link is not there in Breadcrumb ##########
	
//	_click(_link("Home", _in(_div("breadcrumb"))));
//	//Verifying UI of home page 
//	_assertVisible(_image("Salesforce Commerce Cloud SiteGenesis"));
//	_assertEqual(false,_isVisible(_div("breadcrumb")));
//	_assertVisible(_textbox("Search"));
//	_assertVisible(_list("homepage-slides"));
	
	//again click on Site map link in global footer
	_click(_link($Links[6][0], _in(_div("footer-container"))));
	//Verifying bread crumb and heading
	//_assertEqual($Links[6][1],_getText(_link("breadcrumb-last")));
	_assertVisible(_link($Links[6][1], _in(_div("breadcrumb"))));
	_assertVisible(_heading1($Links[6][2], _in(_div("primary"))));
}
else
	{
	_assert(false,"Site map link in not present in global footer");
	}	

}
catch($e)
{
_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("124381/124386", "Verify the UI  of 'Site Map landing page' page in application both as an  Anonymous and Registered user. &" +
		 "Verify the functionality related to  down arrow present in left Nav on'SITE MAP' page in application as an Anonymous and Registered user.");
$t.start();
try
{
	
//_assertEqual($Links[6][1],_getText(_link("breadcrumb-last")));
_assertVisible(_link($Links[6][1], _in(_div("breadcrumb"))));
_assertVisible(_heading1($Links[6][2]));
	
//Checking UI of left Nav and down arrow functionality
if (_isVisible(_div("secondary-navigation")))
	{
	LeftNav_UI();
	}
	else
		{
		_log("Left Nav section is not present");
		}
//CH

}
catch($e)
{
_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148808", "Verify the navigation related to left nav links on'SITE MAP' page in application both as an  Anonymous and Registered user.");
$t.start();
 try
 {
	 
  //Navigate to Home page
  _click(_link("Demandware SiteGenesis"));
  _click(_link($Links[6][0], _in(_div("footer-container"))));
  
//Checking Navigation of left nav links present in Costumer service page
   for(var $j=0;$j<$Links.length;$j++)
  	{
  	  
    	_click(_link($Links[$j][3], _in(_div("secondary-navigation"))));
    	//Verifying Heading
    	_assertVisible(_heading1($Links[$j][5]));
    	//Verifying Bread crumb
    	_assertVisible(_link($Links[$j][6], _in(_div("breadcrumb"))));
    	//Navigating back to contact us page 
    	_click(_link($Links[6][0], _in(_listItem($Links[6][0]))));
  	}
  }
   catch($e)
    {
    	_logExceptionAsFailure($e);
    }	
     $t.end();
     
     
     
     
var $t = _testcase("124388", "Verify the navigation of 'Gift Certificates' link on Demandware SiteGenesis page");
$t.start();
try
{
	//Click on site map
	_click(_link($Links[6][0], _in(_div("footer-container"))));
	_click(_link($Links[0][11], _in(_listItem($Links[0][11]))));
	_assertVisible(_heading1($Links[1][11]));
	_assertVisible(_link($Links[1][11], _in(_div("breadcrumb"))));

}
catch($e)
{
_logExceptionAsFailure($e);
}	
$t.end();


 //Related to security policy link 
 
 var $t = _testcase("124827/124828", "Verify the breadcrumb in the Security policy page & Verify the UI elements in the Security policy page"); 
 $t.start();
 try
 {
	 _click(_link($Links[1][0], _in(_div("footer-container"))));
	 _click(_link($Links[2][3], _in(_listItem($Links[2][3]))));
	 _assertVisible(_div("breadcrumb"));
 	 //Verifying Heading
 	 _assertVisible(_heading1($Links[2][5]));
 	 //Verifying Bread crumb
 	 _assertVisible(_link($Links[2][6], _in(_div("breadcrumb"))));
 	 
 	//################## Home Link is not there in Breadcrumb ##########
 	 
// 	_click(_link("Home", _in(_div("breadcrumb"))));
//	//Verifying UI of home page 
//	_assertVisible(_image("Salesforce Commerce Cloud SiteGenesis"));
//	_assertEqual(false,_isVisible(_div("breadcrumb")));
//	_assertVisible(_textbox("Search"));
//	_assertVisible(_list("homepage-slides"));
	 
 }
 catch($e)
 {
 _logExceptionAsFailure($e);
 }	
 $t.end();

 
 var $t = _testcase("125869", "Verify the navigation of 'My Account' link on footer"); 
 $t.start();
 try
 {
	 //Navigate to Home page
	 _click(_link("Demandware SiteGenesis"));
	 //click on home link in global footer
	 _click(_link($Links[1][8], _in(_div("footer-container"))));
	 //UI of My account page
	//Verifying the UI of My Account page
	//MyAccount login as heading
	_assertVisible(_heading1($Links[0][9]));
	//Returning customer heading
	_assertVisible(_heading2("/"+$Links[1][9]+"/"));
	//Paragraph with static text
	_assertVisible(_paragraph("/(.*)/",_in(_div("login-box-content returning-customers clearfix"))));
	//email Address
	_assertVisible(_label($Links[2][9]));
	_assertVisible(_textbox("/dwfrm_login_username/"));
	_assertEqual("", _getValue(_textbox("/dwfrm_login_username/")));
	//password
	_assertVisible(_label($Links[3][9]));
	_assertVisible(_password("/dwfrm_login_password/"));
	_assertEqual("", _getValue(_password("/dwfrm_login_password/")));
	//remember me
	_assertVisible(_label("Remember Me"));
	_assertVisible(_checkbox("dwfrm_login_rememberme"));
	_assertNotTrue(_checkbox("dwfrm_login_rememberme").checked);
	//login button
	_assertVisible(_submit("Login"));
	//forgot password
	_assertVisible(_link("password-reset"));
	_assertEqual($Links[4][9], _getText(_link("password-reset")));
	//social links
	_assertVisible(_div("login-box-content returning-customers clearfix"));
	 

 }
 catch($e)
 {
 _logExceptionAsFailure($e);
 }	
 $t.end();
 
 
 var $t = _testcase("125870", "Verify the navigation of 'Check Ordert' link on footer"); 
 $t.start();
 try
 {
	 //Navigate to Home page
	 _click(_link("Demandware SiteGenesis"));
	 //click on home link in global footer
	 _click(_link($Links[2][8], _in(_div("footer-container"))));
	//verify the UI of check an order section
	 _assertVisible(_heading2("/"+$Links[0][10]+"/"));
	 _assertVisible(_paragraph("/(.*)/",_in(_div("login-box login-order-track"))));
	 //order number
	 _assertVisible(_label($Links[1][10]));
	 _assertVisible(_textbox("dwfrm_ordertrack_orderNumber"));
	 _assertEqual("", _getValue(_textbox("dwfrm_ordertrack_orderNumber")));
	 //order email
	 _assertVisible(_label($Links[2][10]));
	 _assertVisible(_textbox("dwfrm_ordertrack_orderEmail"));
	 _assertEqual("", _getValue(_textbox("dwfrm_ordertrack_orderEmail")));
	 //postal code
	 _assertVisible(_label($Links[3][10]));
	 _assertVisible(_textbox("dwfrm_ordertrack_postalCode"));
	 _assertEqual("", _getValue(_textbox("dwfrm_ordertrack_postalCode")));
	 //submit button
	 _assertVisible(_submit("dwfrm_ordertrack_findorder"));
	 

 }
 catch($e)
 {
 _logExceptionAsFailure($e);
 }	
 $t.end();
 

 
 
 var $t = _testcase("124923/124926/124934/124936/124940/124942/124944/124946/124951/124955", "Verify the UI of 'Checkout'/'Changing/Canceling Orders/'Valid Payment Methods'/Information About Shipping/" +
 			"Shipping Methods/Super Saver Shipping/Information About Returns/Managing An Account/Opening An Account/Terms & Conditions of Sale/ page in application both as an Anonymous and Registered user."); 
 $t.start();
 try
 {
 	
 //click on help link in global footer
 _click(_link($Links[0][0], _in(_div("footer-container"))));

  for(var $j=3;$j<$Links.length;$j++)
 	{
 	  
 	//Click on checkout link in left nav
  	_click(_link($Links[$j][3], _in(_div("secondary-navigation"))));
  	//Verifying Heading
  	_assertVisible(_heading1($Links[$j][5]));
  	//Verifying Bread crumb
  	_assertVisible(_link($Links[$j][6], _in(_div("breadcrumb"))));
  	
  //################## Home Link is not there in Breadcrumb ##########
  	
//	_assertVisible(_link("Home", _in(_div("breadcrumb"))));
// 	//Verifying UI of home page 
// 	_assertVisible(_div("content-asset"));
// 	_assertVisible(_div("footer"));
  	
 	_click(_link($Links[0][0], _in(_div("footer-container"))));
 	
 	}

 }
 catch($e)
 {
 _logExceptionAsFailure($e);
 }	
 $t.end();
 
 
 
var $t = _testcase("148811/148814/148818/148821/148824/148827/148830/148833", "Verify the Navigation of 'Checkout'/'Changing/Canceling Orders/'Valid Payment Methods'/Information About Shipping/" +
 			"Shipping Methods/Super Saver Shipping/Information About Returns/Managing An Account/Opening An Account/Terms & Conditions of Sale/ page in application both as an Anonymous and Registered user."); 
$t.start();
try
{
	
//click on help link in global footer
_click(_link($Links[0][0], _in(_div("footer-container"))));

 for(var $j=3;$j<$Links.length;$j++)
	{
	  
	//Click on checkout link in left nav
 	_click(_link($Links[$j][3], _in(_div("secondary-navigation"))));
 	//Verifying Heading
 	_assertVisible(_heading1($Links[$j][5]));
 	//Verifying Bread crumb
 	_assertVisible(_link($Links[$j][6], _in(_div("breadcrumb"))));
 	
 	//################## Home Link is not there in Breadcrumb ##########
 	
//	//Click on home link in bread crumb
//	_click(_link("Home", _in(_div("breadcrumb"))));
//	//Verifying UI of home page 
//	_assertVisible(_image("Salesforce Commerce Cloud SiteGenesis"));
//	_assertEqual(false,_isVisible(_div("breadcrumb")));
//	_assertVisible(_textbox("Search"));
//	_assertVisible(_list("homepage-slides"));
 	
 	//click on help link in global footer
	_click(_link($Links[0][0], _in(_div("footer-container"))));
	
	}

}
catch($e)
{
_logExceptionAsFailure($e);
}	
$t.end();




var $t = _testcase("124382","Verify the navigation related to categoty/subcategy links in the content slot on'SITE MAP' page in application both as an  Anonymous and Registered user."); 
 $t.start();
try
{
		
//Navigate to Home page
_click(_link("Demandware SiteGenesis"));
_click(_link($Links[6][0], _in(_div("footer-container"))));
 
var $Sitemap_links=_collectAttributes("_heading2","/(.*)/","sahiText",_in(_div("primary")));
for(var $i=0;$i<$Sitemap_links.length;$i++)
{
	
_click(_link($Sitemap_links[$i], _in(_div("primary"))));
_setAccessorIgnoreCase(true); 
_assertVisible(_div("breadcrumb"));
_assertEqual("Home"+" "+$Sitemap_links[$i], _getText(_div("breadcrumb")));
_click(_link($Links[6][0], _in(_div("footer-container"))));	
	
}


//var $Sitemap_Sublinks=_collectAttributes("_listItem","/(.*)/","sahiText",_in(_div("primary")));
//
//
//
//for(var $i=0;$i<$Sitemap_Sublinks.length;$i++)
//{
//	
//_click(_link("/(.*)/",_in(_listItem($Sitemap_Sublinks[$i],_in(_list("/(.*)/",_in(_div("primary"))))))));
//_setAccessorIgnoreCase(true); 
//_assertVisible(_div("breadcrumb"));
//_assertEqual("Home"+" "++""+, _getText(_div("breadcrumb")));
//_click(_link($Links[6][0], _in(_div("footer"))));	
//	
//}
 
}
catch($e)
{
_logExceptionAsFailure($e);
}	
$t.end();  
   
function LeftNav_UI()
{
		
//Verifying the UI of Contact us section in left nav	
if (_isVisible(_span($Links[0][4], _in(_div("secondary-navigation")))))
	{
	_assertVisible(_link($Links[0][3], _in(_div("secondary-navigation"))));
	}
	else
		{
		_assert(false,"Contact us link is not present in left nav");
		}

//Verifying the UI of Privacy and security section in left nav
if (_isVisible(_span($Links[1][4], _in(_div("secondary-navigation")))))
	{
	_assertVisible(_link($Links[1][3], _in(_div("secondary-navigation"))));
	_assertVisible(_link($Links[2][3], _in(_div("secondary-navigation"))));
	}
	else
		{
		_assert(false,"Contact us link is not present in left nav");
		}

//Verifying the UI of Ordering section in left nav
if (_isVisible(_span($Links[2][4], _in(_div("secondary-navigation")))))
	{
	_assertVisible(_link($Links[3][3], _in(_div("secondary-navigation"))));
	_assertVisible(_link($Links[4][3], _in(_div("secondary-navigation"))));
	_assertVisible(_link($Links[5][3], _in(_div("secondary-navigation"))));
	}
	else
		{
		_assert(false,"Ordering link is not present in left nav");
		}

//Verifying the UI of Shipping section in left nav
if (_isVisible(_span($Links[3][4], _in(_div("secondary-navigation")))))
	{
	_assertVisible(_link($Links[6][3], _in(_div("secondary-navigation"))));
	_assertVisible(_link($Links[7][3], _in(_div("secondary-navigation"))));
	_assertVisible(_link($Links[8][3], _in(_div("secondary-navigation"))));
	}
	else
		{
		_assert(false,"Shipping link is not present in left nav");
		}

//Verifying the UI of returns section in left nav
if (_isVisible(_span($Links[4][4], _in(_div("secondary-navigation")))))
	{
	_assertVisible(_link($Links[9][3], _in(_div("secondary-navigation"))));
	}
	else
		{
		_assert(false,"Returns link is not present in left nav");
		}

//Verifying the UI of Myaccount section in left nav
if (_isVisible(_span($Links[5][4], _in(_div("secondary-navigation")))))
	{
	_assertVisible(_link($Links[10][3], _in(_div("secondary-navigation"))));
	_assertVisible(_link($Links[11][3], _in(_div("secondary-navigation"))));
	}
	else
		{
		_assert(false,"Returns link is not present in left nav");
		}

//Verifying the UI of TERMS & CONDITIONS section in left nav
if (_isVisible(_span($Links[6][4], _in(_div("secondary-navigation")))))
	{
	_assertVisible(_link($Links[12][3], _in(_div("secondary-navigation"))));
	}
	else
		{
		_assert(false,"TERMS & CONDITIONS link is not present in left nav");
		}

//Need help section
_assertVisible(_div("/Need Help?/"));

}

function LeftNav_Links()
{
	
	LeftNav_UI();
	//Left nav main links count
	var $leftnav_main=_count("_span","toggle",_in(_div("secondary-navigation")));
	//verifying  the presence of Left nav main links 
	for(var $k=0;$k<$leftnav_main;$k++)
	{
		_assertVisible(_span($Links[$k][4]));	
	}
	
	
	//Left nav sub links count
	var $Count_leftnav=_count("_span","toggle",_in(_div("secondary-navigation")));

	//Click on the up arrow ('V') down of main links arrow and verify
	for(var $i=0;$i<$Count_leftnav;$i++)
	{
		_click(_span("toggle["+$i+"]"));
		
		if($i==0)
			{
			_assertEqual(false,_isVisible(_link($Links[0][3], _in(_div("secondary-navigation")))));
			}
		else if($i==1)
			{
			_assertEqual(false,_isVisible(_link($Links[1][3], _in(_div("secondary-navigation")))));
			_assertEqual(false,_isVisible(_link($Links[2][3], _in(_div("secondary-navigation")))));
			}
		else if($i==2)
			{
			_assertEqual(false,_isVisible(_link($Links[3][3], _in(_div("secondary-navigation")))));
			_assertEqual(false,_isVisible(_link($Links[4][3], _in(_div("secondary-navigation")))));
			_assertEqual(false,_isVisible(_link($Links[5][3], _in(_div("secondary-navigation")))));
			}
		else if($i==3) 
			{
			_assertEqual(false,_isVisible(_link($Links[6][3], _in(_div("secondary-navigation")))));
			_assertEqual(false,_isVisible(_link($Links[7][3], _in(_div("secondary-navigation")))));
			_assertEqual(false,_isVisible(_link($Links[8][3], _in(_div("secondary-navigation")))));
			}
		else if($i==4) 
			{
			_assertEqual(false,_isVisible(_link($Links[9][3], _in(_div("secondary-navigation")))));
			}
		else if($i==5) 
			{
			_assertEqual(false,_isVisible(_link($Links[10][3], _in(_div("secondary-navigation")))));
			_assertEqual(false,_isVisible(_link($Links[11][3], _in(_div("secondary-navigation")))));
			}
			else
			{
			_assertEqual(false,_isVisible(_link($Links[12][3], _in(_div("secondary-navigation")))));
			}
	}

	//again Click on the down arrow ('V') of main links down arrow and verify	
	for(var $j=0;$j<$Count_leftnav;$j++)
	{
		_click(_span("toggle["+$j+"]"));
		
		if($j==0)
			{
			_assertEqual(true,_isVisible(_link($Links[0][3], _in(_div("secondary-navigation")))));
			}
		else if($j==1)
			{
			_assertEqual(true,_isVisible(_link($Links[1][3], _in(_div("secondary-navigation")))));
			_assertEqual(true,_isVisible(_link($Links[2][3], _in(_div("secondary-navigation")))));
			}
		else if($j==2)
			{
			_assertEqual(true,_isVisible(_link($Links[3][3], _in(_div("secondary-navigation")))));
			_assertEqual(true,_isVisible(_link($Links[4][3], _in(_div("secondary-navigation")))));
			_assertEqual(true,_isVisible(_link($Links[5][3], _in(_div("secondary-navigation")))));
			}
		else if($j==3) 
			{
			_assertEqual(true,_isVisible(_link($Links[6][3], _in(_div("secondary-navigation")))));
			_assertEqual(true,_isVisible(_link($Links[7][3], _in(_div("secondary-navigation")))));
			_assertEqual(true,_isVisible(_link($Links[8][3], _in(_div("secondary-navigation")))));
			}
		else if($j==4) 
			{
			_assertEqual(true,_isVisible(_link($Links[9][3], _in(_div("secondary-navigation")))));
			}
		else if($j==5) 
			{
			_assertEqual(true,_isVisible(_link($Links[10][3], _in(_div("secondary-navigation")))));
			_assertEqual(true,_isVisible(_link($Links[11][3], _in(_div("secondary-navigation")))));
			}
			else
			{
			_assertEqual(true,_isVisible(_link($Links[12][3], _in(_div("secondary-navigation")))));
			}
	}


}
