_include("BrowserSpecific.js");
_include("BM_Functions.js");
_resource("User_Credentials.xls");


var $pName;
var $quantity;
var $price;
var $totalPrice;
var $shippingTax;
var $tax;
var $subTotal;
var $orderTotal;
var $actOrderTotal;
var $URL=_readExcelFile("User_Credentials.xls","URL");
var $userLog=_readExcelFile("User_Credentials.xls","BM_Login");
var $Run=_readExcelFile("User_Credentials.xls","Run");
var $widthBoolean=false;
var $colorBoolean=false;
var $sizeBoolean=false;
var $ProductName;
var $ProductPrice;
var $ProductNumber;
var $QTY;

//Function take a screen shot when a their is a script failure

function onScriptError()
{
  _log("Error log"); 
  _lockWindow(5000);
  _focusWindow();
  _takePageScreenShot();
  _unlockWindow();
}

function onScriptFailure()
{
  _log("Failure log");
  _lockWindow(5000);
  _focusWindow();
  _takePageScreenShot();
  _unlockWindow();
} 

function SiteURLs()
{
	_navigateTo($URL[0][0]);
}

function cleanup()
 {
	//Clearing the items from cart
	ClearCartItems();
	if(isMobile())
		{
		_click(_span("Menu"));
		}
	//logout from the application
	logout();
	_click(_image("Salesforce Commerce Cloud SiteGenesis"));
}


function logout()
{
	//logout from the application
	if(_isVisible(_link("user-account")))
	{
		_click(_link("user-account"));
		if(_isVisible(_link("Logout")))
		{		
			_click(_link("Logout"));
	
		}
	}	
}
//############################################################################
//Cart
 function ClearCartItems()
 {	 
 	if(_isVisible(_link("mini-cart-link")))
	{ 		
 		_click(_link("mini-cart-link"));
 		if(_isVisible(_row("rowcoupons")))
	 		 {
	 			_click(_submit("dwfrm_cart_coupons_i0_deleteCoupon"));
	 		 }	
 		if(isMobile())
 			{
	 			while(!_isVisible(_div("cart-empty")))
					{
		 				_click(_submit("Remove"));
					}
 			}
 		else
 			{
	 			while(_isVisible(_submit("Remove")))
					{
		 				_click(_submit("Remove"));
					}
 			}	
	}
 }
 
 function CalculateEstiTotal($shipDiscount,$orderDiscount)
 {
 	var $ExpOrderSubTotal=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
 	var $ExpShippingCost;
 	var $ExpTax;
 	var $ExpEstiTotal;
 	var $ActEstiTotal=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
 	
 		if(_extract(_getText(_row("order-shipping")),"/[ ](.*)/",true)=="N/A" || _extract(_getText(_row("order-shipping")),"/[ ](.*)/",true)=="-")
 		{
 		$ExpShippingCost=0;
 		}
 	else{
 		$ExpShippingCost=parseFloat(_extract(_getText(_row("order-shipping")),"/[$](.*)/",true));
 		}

 		var $ActTaxRes;
 		if(isIE11() || _isIE10())
 			{
 			$ActTaxRes=_extract(_getText(_row("order-sales-tax")),"/Tax (.*)/",true);
 			}
 		else
 			{
 			$ActTaxRes=_extract(_getText(_row("order-sales-tax")),"/Tax (.*)/",true);
 			}
 	
 	if($ActTaxRes=="N/A" || $ActTaxRes=="-")
 		{
 		$ExpTax=0;
 		}
 	else{
 		$ExpTax=parseFloat(_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true));
 	}
 	if(_isVisible(_row("order-shipping-discount discount")) || _isVisible(_row("order-discount discount")))
 	{
 		if(_isVisible(_row("order-discount discount")))
 			{
 			//calculating Order discount
 			var $OrderDisc=($orderDiscount/100)*$ExpOrderSubTotal;
 			$OrderDisc=round($OrderDisc,2);
 			//$ & - should prefixed with discount
 			_assertEqual("/- [$](.*)/",_extract(_getText(_row("order-discount discount")),"/Order Discount(.*)/",true).toString());
 			_assertEqual($OrderDisc,parseFloat(_extract(_getText(_row("order-discount discount")),"/[$](.*)/",true)));
 			$ExpEstiTotal=$ExpOrderSubTotal-$OrderDisc+$ExpShippingCost+$ExpTax;
 			$ExpEstiTotal=round($ExpEstiTotal, 2);
 			_assertEqual($ExpEstiTotal,$ActEstiTotal);
 		
 			}
 		else 
 			{
 			//calculating shipping discount
 			var $ShippingDisc=($shipDiscount/100)*$ExpShippingCost;
 			$ShippingDisc=round($ShippingDisc, 2);
 			//$ & - should prefixed with discount
 			_assertEqual("/- [$](.*)/",_extract(_getText(_row("order-shipping-discount discount")),"/Shipping Discount(.*)/",true).toString());
 			_assertEqual($ShippingDisc,parseFloat(_extract(_getText(_row("order-shipping-discount discount")),"/[$](.*)/",true)));
 			$ExpEstiTotal=$ExpOrderSubTotal+$ExpShippingCost-$ShippingDisc+$ExpTax;
 			$ExpEstiTotal=round($ExpEstiTotal, 2);
 			_assertEqual($ExpEstiTotal,$ActEstiTotal);
 			}
 	}
 	else{
 		$ExpEstiTotal=$ExpOrderSubTotal+$ExpShippingCost+$ExpTax;
 		$ExpEstiTotal=round($ExpEstiTotal, 2);
 		_assertEqual($ExpEstiTotal,$ActEstiTotal);
 	}
 	
 }

 
 
 
 
 
 
 
 
 
//############################################################################
//Profile
 
function login()
 {	 
	 _setValue(_textbox("/dwfrm_login_username/"), $uId);
	 _setValue(_password("/dwfrm_login_password/"), $pwd);
	    if(_isVisible(_submit("Login")))
	    {
	      _click(_submit("Login"));
	    }
	   else
	    {
	        _typeKeyCodeNative(java.awt.event.KeyEvent.VK_ENTER);
	    }
 }
 
function createAccount()
 {
 	 _setValue(_textbox("dwfrm_profile_customer_firstname"), $userData[$user][1]);
 	 _setValue(_textbox("dwfrm_profile_customer_lastname"), $userData[$user][2]);
 	 _setValue(_textbox("dwfrm_profile_customer_email"), $userData[$user][3]);
 	 _setValue(_textbox("dwfrm_profile_customer_emailconfirm"), $userData[$user][4]);
 	 _setValue(_password("/dwfrm_profile_login_password/"), $userData[$user][5]);
 	 _setValue(_password("/dwfrm_profile_login_passwordconfirm/"), $userData[$user][6]); 	 
 	 if(_isVisible(_submit("Apply")))
 		 {
 		 	_click(_submit("Apply")); 
 		 } 	 
 	 else
 		 {
 		   _click(_span("Create an Account"));
 		 }
 } 
 
 
function deleteAddress()
 {
	 _click(_link("user-account"));
	 clearWishList();
	 Delete_GiftRegistry();
	 _click(_link("Addresses"));
	 //deleting the existing addresses
	 while(_isVisible(_link("Delete")))
	   	{
	   	  _click(_link("Delete"));
	   	  _expectConfirm("/Do you want/",true);
	   	}
 }
 
function addAddress($validations,$r)
 {
	  	_setValue(_textbox("dwfrm_profile_address_addressid"), $validations[$r][1]);
		_setValue(_textbox("dwfrm_profile_address_firstname"), $validations[$r][2]);
		_setValue(_textbox("dwfrm_profile_address_lastname"), $validations[$r][3]);
		_setValue(_textbox("dwfrm_profile_address_address1"), $validations[$r][4]);
		_setValue(_textbox("dwfrm_profile_address_address2"), $validations[$r][5]);
		_setSelected(_select("dwfrm_profile_address_country"),$validations[$r][6]);
		_setSelected(_select("dwfrm_profile_address_states_state"), $validations[$r][7]);
		_setValue(_textbox("dwfrm_profile_address_city"), $validations[$r][8]);
		_setValue(_textbox("dwfrm_profile_address_postal"), $validations[$r][9]);
		_setValue(_textbox("dwfrm_profile_address_phone"),$validations[$r][10]);	
 }
 
function clearWishList()
 {
	 _click(_link("user-account"));
	 _click(_link("Wish List"));
	 if (_isVisible(_select("editAddress")))
		 {
			 _setSelected(_select("editAddress"),0);	 
		 }
	 while(_isVisible(_submit("Remove")))
		 {
		  	  _click(_submit("Remove"));
		 } 
 }
 
function deleteAddeditemsFromWishlistAccount()
{
	//login to the application
	 _click(_link("Login"));
	 //login to application for which account we have added item
	 login();
	 //clearing items from wishlist
	 clearWishList();
	 //logout from application
	 _click(_link("My Account"));
	 _click(_link("Logout"));
}

function additemsToWishListAccount($cat)
{
	//login to the application
	 _click(_link("Login"));
	 //login to application for which account we want to add item
	 login();
	 //clear items from wish list
	 clearWishList();
     //add items
     _click(_link($cat));
     _click(_link("name-link"));
     //selecting swatches
     selectSwatch();
     //click on wish list
     _click(_link("Add to Wishlist"));
     //navigate to wish list
     _click(_link("Wish List"));
     //click on make this as public if it is not having public visibility
     if(_isVisible(_submit("dwfrm_wishlist_setListPublic")))
    	 {
    	 _click(_submit("dwfrm_wishlist_setListPublic"));
    	 }
     //logout from application
     _click(_link("My Account"));
     _click(_link("Logout"));
}

function Delete_GiftRegistry()
 {
	 _click(_link("Gift Registry"));
	 while(_isVisible(_link("Delete")))
	   {  
	       _click(_link("Delete"));
	       _expectConfirm("/Do you want/",true);
	      // _expectConfirm("Do you want to remove this gift registry?", true); 
	     _log("Gift Registry deleted successfully");
	   }
 }

function addItems($Item)
{
	_click(_link("Add Items"));
	//Verifying navigation
	_assertVisible(_list("homepage-slides"));
	_assertVisible(_div("home-bottom-slots"));
	//Adding the items to gift registry
	_click(_link($Item));
	//click on name link
	$ProductName=_getText(_link("name-link"));
	_click(_link("name-link"));
	//select swatches
	selectSwatch();
	//fetching product name
	$ProductPrice=_getText(_span("price-sales", _in(_div("product-price"))));	
	$QTY=_getText(_textbox("Quantity"));	
	$ProductNumber=_extract(_getText(_div("product-number")), "/Item# (.*)/", true).toString();
	_click(_link("Add to Gift Registry"));	
}


function createRegistry($Sheet, $i)
{
	_setValue(_textbox("dwfrm_giftregistry_event_type"),$Sheet[$i][1]);
	_setValue(_textbox("dwfrm_giftregistry_event_name"),$Sheet[$i][2]);
	_setValue(_datebox("dwfrm_giftregistry_event_date"),$Sheet[$i][3]);
	//_setSelected(_select("dwfrm_giftregistry_event_eventaddress_country"),$Sheet[$i][4]);
	//_setSelected(_select("dwfrm_giftregistry_event_eventaddress_states_state"),$Sheet[$i][5]);
	_setValue(_textbox("dwfrm_giftregistry_event_town"),$Sheet[$i][6]);
	_setSelected(_select("dwfrm_giftregistry_event_participant_role"),$Sheet[$i][7]);
	_setValue(_textbox("dwfrm_giftregistry_event_participant_firstName"),$FirstName1);
	_setValue(_textbox("dwfrm_giftregistry_event_participant_lastName"),$LastName1);
	_setValue(_textbox("dwfrm_giftregistry_event_participant_email"),$UserEmail);
	_setSelected(_select("dwfrm_giftregistry_event_coParticipant_role"),$Sheet[$i][11]);
	_setValue(_textbox("dwfrm_giftregistry_event_coParticipant_firstName"),$Sheet[$i][12]);
	_setValue(_textbox("dwfrm_giftregistry_event_coParticipant_lastName"),$Sheet[$i][13]);
	_setValue(_textbox("dwfrm_giftregistry_event_coParticipant_email"),$Sheet[$i][14]);
	_click(_submit("Continue"));	
}

function Pre_PostEventInfo($Sheet, $i)
{
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_firstname"),$Sheet[$i][1]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_lastname"),$Sheet[$i][2]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address1"),$Sheet[$i][3]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_address2"),$Sheet[$i][4]);
	_setSelected(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_country"),$Sheet[$i][5]);
	_setSelected(_select("dwfrm_giftregistry_eventaddress_addressBeforeEvent_states_state"),$Sheet[$i][6]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_city"),$Sheet[$i][7]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_postal"),$Sheet[$i][8]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_phone"),$Sheet[$i][9]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressBeforeEvent_addressid"),$Sheet[$i][10]);
	
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_firstname"),$Sheet[$i][1]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_lastname"),$Sheet[$i][2]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_address1"),$Sheet[$i][3]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_address2"),$Sheet[$i][4]);
	_setSelected(_select("dwfrm_giftregistry_eventaddress_addressAfterEvent_country"),$Sheet[$i][5]);
	_setSelected(_select("dwfrm_giftregistry_eventaddress_addressAfterEvent_states_state"),$Sheet[$i][6]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_city"),$Sheet[$i][7]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_postal"),$Sheet[$i][8]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_phone"),$Sheet[$i][9]);
	_setValue(_textbox("dwfrm_giftregistry_eventaddress_addressAfterEvent_addressid"),$Sheet[$i][10]);
	
	_click(_submit("Continue"));
}

//credit card info
function CreateCreditCard($i,$Createcard, $boolean)
{
               
       _setValue(_textbox("dwfrm_paymentinstruments_creditcards_newcreditcard_owner"),$Createcard[$i][1]);
       _setSelected(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_type"),$Createcard[$i][2]);
       _setValue(_textbox("/dwfrm_paymentinstruments_creditcards_newcreditcard_number/"),$Createcard[$i][3]);
       _setSelected(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_month"),$Createcard[$i][4]);
       _setSelected(_select("dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_year"),$Createcard[$i][5]);
       if($boolean==true)
    	   {
               _setValue(_textbox("dwfrm_paymentinstruments_creditcards_address_firstname"),$Createcard[$i][6]);
               _setValue(_textbox("dwfrm_paymentinstruments_creditcards_address_lastname"),$Createcard[$i][7]);
               _setValue(_textbox("dwfrm_paymentinstruments_creditcards_address_address1"),$Createcard[$i][8]);
               _setValue(_textbox("dwfrm_paymentinstruments_creditcards_address_address2"),$Createcard[$i][9]);
               _setSelected(_select("dwfrm_paymentinstruments_creditcards_address_country"),$Createcard[$i][10]);
               _setValue(_textbox("dwfrm_paymentinstruments_creditcards_address_city"),$Createcard[$i][11]);
               _setSelected(_select("dwfrm_paymentinstruments_creditcards_address_states_state"),$Createcard[$i][12]);
               _setValue(_textbox("dwfrm_paymentinstruments_creditcards_address_postal"),$Createcard[$i][13]);
               _setValue(_textbox("dwfrm_paymentinstruments_creditcards_address_phone"),$Createcard[$i][14]);
               _setValue(_textbox("dwfrm_paymentinstruments_creditcards_address_email_emailAddress"), $uId);
    	   }
}


function closedOverlayVerification()
{
   _assertNotVisible(_div("ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable"));
   //heading
   _assertVisible(_heading1($Payment_Data[0][3]));
   _assertVisible(_div("/"+$Payment_Data[1][3]+"/"));
   _assertVisible(_link("Add Credit Card") );           
}


function DeleteCreditCard()
{               
	//verifying the address in account
	_click(_link("user-account"));
	_click(_link("My Account"));
	_click(_link("Payment Settings"));
	//click on delete card link
	while(_isVisible(_submit("Delete Card")))
       {
           _click(_submit("Delete Card"));
       }

	//click on ok in confirmation overlay
	_expectConfirm("/Do you want/",true);
	//verify whether deleted or not
	_assertNotVisible(_list("payment-list"));
	_assertNotVisible(_submit("Delete Card"));
}

 



 function SendtoFriend($sheet,$i)
 {

 _setValue(_textbox("dwfrm_sendtofriend_friendsname"),$sheet[$i][1]);
 _setValue(_textbox("dwfrm_sendtofriend_friendsemail"),$sheet[$i][2]);
 _setValue(_textbox("dwfrm_sendtofriend_confirmfriendsemail"),$sheet[$i][3]);
 _setValue(_textarea("dwfrm_sendtofriend_message"),$sheet[$i][4]);
 	
 }


 
 function addressVerificationOverlay()
 { 
	 _log("$AddrVerification"+$AddrVerification);
	//UPS verification
		if($AddrVerification==$BMConfig[0][2])
		{
			if(_isVisible(_submit("ship-to-original-address")))
			{
			_click(_submit("ship-to-original-address"));
			}
		}
		//group1 verification
		else if($AddrVerification==$BMConfig[1][2])
		{
			if(_isVisible(_submit("ship-to-original-address")))
				{
				_click(_submit("ship-to-original-address"));
				}
			if(_isVisible(_submit("ship-to-original-address[1]")))
				{
					_click(_submit("ship-to-original-address[1]"));
				}
		}
 }

 






 var $UserID=$userLog[0][0];
 var $Password=$userLog[0][1];


 
 function selectSwatch()
 {
		//select swatches
		if(_isVisible(_div("SELECT COLOR")))
		    {
		    $colorBoolean=true;
		    if(!_isVisible(_listItem("selectable selected",_in(_list("swatches Color")))))
		           {
		           _click(_link("/swatchanchor/",_in(_listItem("selectable",_in(_list("swatches Color"))))));
		           }
		    }
		if(isMobile())
		    {
		    _wait(4000);
		    }
		if(_isVisible(_div("SELECT SIZE")))
		    {
		    $sizeBoolean=true;
		    if(!_isVisible(_listItem("selectable selected",_in(_list("swatches size")))))
		           {
		           _click(_link("/swatchanchor/",_in(_listItem("selectable",_in(_list("swatches size"))))));
		           }
		    }
		if(isMobile())
		{
		_wait(4000);
		}
		if(_isVisible(_div("SELECT WIDTH")))
		    {
		    $widthBoolean=true;
		           if(!_isVisible(_listItem("selectable selected", _in(_list("swatches width")))))
		           {
		           _click(_link("/swatchanchor/",_in(_listItem("selectable",_in(_list("swatches width"))))));
		           }
		    }
 }
 
 //search related function
function search($product)
{
	if(isMobile())
		{
			_navigateTo(document.getElementsByName("simpleSearch")[0].getAttribute("action") + "?q=" + $product);
		}
	else
		{
			_setValue(_textbox("q"), $product);	
			_click(_submit("Search"));
		}
	
}

 //cart related functions

function addItemToCart($subCat,$i)
{
	//navigating to sub category
	_click(_link($subCat));
	//navigate to PDP
	_click(_link("name-link"));
	//selecting swatches
	selectSwatch();
	//set the quantity
	_setValue(_textbox("Quantity"),$i);
	//click on add to cart
	_click(_submit("add-to-cart"));
}

function navigateTOIL($excel)
{
	//navigate to cart page
	navigateToCart($excel[0][0],$excel[0][1]);
	//click on go straight to checkout to navigate to IL Page
	_click(_link("mini-cart-link-checkout"));
}

function navigateToCart($subCat,$i)
{
	//add item to cart
	addItemToCart($subCat,$i);
	//navigate to Cart page
	_click(_link("View Cart"));
	$pName=_getText(_link("/(.*)/",_in(_div("name"))));
	$quantity=_getText(_numberbox("input-text", _in(_cell("item-quantity"))));
	$price=_extract(_getText(_span("price-sales")),"/[$](.*)/",true).toString();
	if(_isVisible(_span("price-total")))
		{
	$totalPrice=_extract(_getText(_span("price-total")),"/[$](.*)/",true).toString();
		}
	else if(_isVisible(_span("price-unadjusted")))
		{
		$totalPrice=_extract(_getText(_span("price-adjusted-total")),"/[$](.*)/",true).toString();
		}
	$shippingTax=_extract(_getText(_row("order-shipping")),"/[$](.*)/",true).toString();
	$orderTotal=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
	_log($shippingTax);
}

//Checkout related functions

function navigateToShippingPage($data,$quantity)
{
	//navigate to cart page
	navigateToCart($data,$quantity);	
	//navigating to shipping page
	_click(_submit("dwfrm_cart_checkoutCart"));
	_wait(4000);
	if(isMobile())
		{
			_wait(3000);
		}
	if(_isVisible(_div("login-box login-account")))
		{
		_click(_submit("dwfrm_login_unregistered"));
		}
	_wait(4000);
	$subTotal=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
	$tax=parseFloat(_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString());
	_log($tax);
	$shippingTax=parseFloat(_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString());
	_log($shippingTax);
}

function navigateToBillingPage($data,$quantity)
{
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[1][0]);
	//shipping details
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//verifying address verification overlay
	addressVerificationOverlay();	
	$orderTotal=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());
	_log($orderTotal);
}

function shippingAddress($sheet,$i)
{
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), $sheet[$i][1]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), $sheet[$i][2]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"), $sheet[$i][3]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"),$sheet[$i][4]);
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$sheet[$i][5]);
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), $sheet[$i][6]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), $sheet[$i][7]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $sheet[$i][8]);
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),$sheet[$i][9]);
}
function BillingAddress($sheet,$i)
{
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"), $sheet[$i][1]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"), $sheet[$i][2]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1"), $sheet[$i][3]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address2"),$sheet[$i][4]);
	_setSelected(_select("dwfrm_billing_billingAddress_addressFields_country"),$sheet[$i][5]);
	_setSelected(_select("dwfrm_billing_billingAddress_addressFields_states_state"), $sheet[$i][6]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_city"), $sheet[$i][7]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_postal"), $sheet[$i][8]);
	_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_phone"),$sheet[$i][9]);
	
	_click(_link("user-account"));
	_wait(4000);
	
	if(!_isVisible(_link("Logout")))
		{
		_setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),$sheet[$i][10]);
		}
	
}

function verifyOrderSummary($shipmentType,$taxService,$numOfItems)
{
	var $actCount=0;
	var $actPrice=0; 
	//product name
	_assertEqual($pName, _getText(_link("/(.*)/",_in(_div("checkout-mini-cart")))));
	var $numOfShipments=_count("_div","/mini-cart-product/",_in(_div("checkout-mini-cart")));
	for(var $i=0;$i<$numOfShipments;$i++)
		{
		var $count=parseInt(_getText(_span("value",_in(_div("mini-cart-pricing["+$i+"]",_in(_div("checkout-mini-cart")))))));
		var $price=_extract(_getText(_span("mini-cart-price",_in(_div("checkout-mini-cart")))),"/[$](.*)/",true).toString();
		$actCount=parseFloat($actCount)+$count;
		$actPrice=parseFloat($actPrice)+parseFloat($price);		
		}
	//quantity
	_assertEqual($quantity,$actCount);
	//price
	_assertEqual($totalPrice,$actPrice);
	//sub total
	_assertVisible(_row("order-subtotal"));
	_assertEqual($totalPrice,_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString());
	//Shipping tax
	if($shipmentType=="Multishipment")
		{
		//var $multiShipmentTax=parseFloat(_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString())*numOfItems;
		for(var $i=0;$i<$numOfItems;$i++)
			{
			_assertEqual($shippingTax,parseFloat(_extract(_collectAttributes("_row","/order-shipping/","sahiText")[$i].toString(),"/[$](.*)/",true)));
			}
		//_assertEqual($shippingTax,$multiShipmentTax);
		}
	else
		{
	_assertVisible(_row("/order-shipping/"));
	_assertEqual($shippingTax,_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString());
		}
	//sales tax
	_assertVisible(_row("order-sales-tax"));
	
	if($taxService=="DEMANDWARE")
		{
			_assertEqual($tax,_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString());
			//order total
			//$actOrderTotal=$subTotal+$shippingTax+$salesTax;
			$actOrderTotal=parseFloat($totalPrice)+parseFloat($shippingTax)+parseFloat($tax);
			_log($actOrderTotal);
			$actOrderTotal=round($actOrderTotal,2);
			_assertEqual($actOrderTotal,_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());	
		}
	else
		{
		var $boolean=_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString();
		if($boolean=="false")
			{
			_assert(false,"sales tax is not added");
			}
		else
			{
			_assert(true,"sales tax got added");
			var $salesTax=parseFloat(_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString());
			//order total
			_assertVisible(_row("order-total"));
			
				if(_isVisible(_row("/Shipping Discount/")))
				{
					var $shippingDiscount=parseFloat(_extract(_getText(_row("/Shipping Discount/")),"/[$](.*)/",true));
					$actOrderTotal=$subTotal+$shippingTax*$numOfItems+$salesTax-$shippingDiscount;
					$actOrderTotal=round($actOrderTotal,2);
					_assertEqual($actOrderTotal,_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());
				}
				else
				{
					_log($subTotal);
					_log($shippingTax);
					_log($salesTax);
					$actOrderTotal=$subTotal+$shippingTax*$numOfItems+$salesTax;
					$actOrderTotal=round($actOrderTotal,2);
					_assertEqual($actOrderTotal,_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());
				}
			}
		}
}


function billingPageUI($excel)
{
	//verify the UI of Billing page 
	//fields 
	_assertVisible(_span("/First Name/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_firstname")); 
	_assertVisible(_span("/Last Name/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_lastname"));
	_assertVisible(_span("/Address 1/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_address1"));
	_assertVisible(_span("/Address 2/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_address2"));
	_assertVisible(_span("/State/"));
	_assertVisible(_select("dwfrm_billing_billingAddress_addressFields_states_state"));
	_assertVisible(_span("/City/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_city"));
	_assertVisible(_span("/Zip Code/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_postal"));
	_assertVisible(_span("/Phone/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_addressFields_phone"));
	_assertVisible(_span("/Email/"));
	_assertVisible(_textbox("dwfrm_billing_billingAddress_email_emailAddress"));
	//subscribe to pandora check box
	_assertVisible(_checkbox("dwfrm_billing_billingAddress_addToEmailList"));
	
	//checkbox
	_assertVisible(_checkbox("dwfrm_billing_billingAddress_addToEmailList"));
	_assertNotTrue(_checkbox("dwfrm_billing_billingAddress_addToEmailList").checked);
	//Privacy policy
	_assertVisible(_link("See Privacy Policy"));
	//ENTER GIFT CERTIFICATE OR COUPON/DISCOUNT CODES section
	_assertVisible(_span($excel[1][0]));
	_assertVisible(_textbox("dwfrm_billing_couponCode"));
	_assertVisible(_submit("APPLY"));
	
	if($CreditCard=="Yes" && $paypalNormal=="Yes")
		{
		//Payment section
		_assertVisible(_label("Credit Card"));
		_assertVisible(_radio("Credit Card"));
		_assert(_radio("Credit Card").checked);
		_assertVisible(_label("Pay Pal"));
		_assertVisible(_radio("PayPal"));
		_assertNotTrue(_radio("PayPal").checked);
		//Payment fields
		_assertVisible(_fieldset("/"+$excel[0][1]+"/"));
		_assertVisible(_span("Name on Card"));
		_assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"));
		_assertVisible(_span("Type"));
		_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_type"));
		_assertVisible(_span("Number"));
		_assertVisible(_div($excel[1][1], _in(_div("payment-method payment-method-expanded"))));
		//_assertEqual($excel[1][1], _getText(_span("form-caption", _in(_div("PaymentMethod_CREDIT_CARD")))));
		_assertVisible(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"));
		_assertVisible(_span("/Expiration Date/"));
		_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_expiration_month"));
		_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_expiration_year"));
		_assertVisible(_span("Security Code"));
		_assertVisible(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"));
		}
	else if($CreditCard=="Yes" && $paypalNormal=="No")
		{
		//Payment section
		_assertVisible(_label("Credit Card:"));
		_assertVisible(_radio("dwfrm_billing_paymentMethods_selectedPaymentMethodID"));
		_assert(_radio("dwfrm_billing_paymentMethods_selectedPaymentMethodID").checked);
		//Payment fields
		_assertVisible(_fieldset("/"+$excel[0][1]+"/"));
		_assertVisible(_span("Name on Card"));
		_assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"));
		_assertVisible(_span("Type"));
		_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_type"));
		_assertVisible(_span("Number"));
		_assertVisible(_span($excel[1][1], _in(_div("PaymentMethod_CREDIT_CARD"))));
		//_assertEqual($excel[1][1], _getText(_span("form-caption", _in(_div("PaymentMethod_CREDIT_CARD")))));
		_assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_number"));
		_assertVisible(_span("/Expiration Date/"));
		_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_month"));
		_assertVisible(_select("dwfrm_billing_paymentMethods_creditCard_year"));
		_assertVisible(_span("Security Code"));
		_assertVisible(_textbox("dwfrm_billing_paymentMethods_creditCard_cvn"));
		}
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	{
		_assertVisible(_label("Pay Pal:"));
		_assertVisible(_radio("PayPal"));
		_assert(_radio("PayPal").checked);
	}	
	_assertVisible(_submit("dwfrm_billing_save"));
	
	//Order summary section
	_assertVisible(_heading3("/ORDER SUMMARY/"));
	_assertVisible(_link("Edit"));
	
	//Click on expand link 
	if (_isVisible(_div("mini-cart-product collapsed")))
		{
		_click(_span("mini-cart-toggle fa fa-caret-right"));
		}
	_assertVisible(_div("mini-cart-image", _in(_div("checkout-mini-cart"))));
	_assertVisible(_div("mini-cart-name", _in(_div("checkout-mini-cart"))));
	_assertVisible(_row("order-subtotal"));
	_assertVisible(_row("/order-shipping/"));
	_assertVisible(_row("order-sales-tax"));
	_assertVisible(_row("order-total"));
}

function paypal($paypalUN,$paypaalPwd)
{
	if(_isVisible(_div("passwordSection")))
		{
			_setValue(_emailbox("login_email"), $paypalUN);
			_setValue(_password("login_password"), $paypaalPwd);
			_click(_submit("btnLogin"));
		}
	else
		{
			_assert(true,"Already logged in")
		}

}

function PaymentDetails($sheet,$i)
{
	  _setValue(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"), $sheet[$i][1]);
		 _setSelected(_select("dwfrm_billing_paymentMethods_creditCard_type"), $sheet[$i][2]);
		 _setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"), $sheet[$i][3]);
		 _setSelected(_select("dwfrm_billing_paymentMethods_creditCard_expiration_month"), $sheet[$i][4]);
	    _setSelected(_select("dwfrm_billing_paymentMethods_creditCard_expiration_year"), $sheet[$i][5]);
	    _setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"), $sheet[$i][6]);
}





function addAddressMultiShip($sheet,$i)
{
	  if(_isVisible(_textbox("dwfrm_profile_address_addressid")))
	  {
	    _setValue(_textbox("dwfrm_profile_address_addressid"), $sheet[$i][1]);
	  }
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_firstName")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_firstName"), $sheet[$i][2]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_lastName")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_lastName"),$sheet[$i][3]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_address1")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_address1"), $sheet[$i][4]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_address2")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_address2"), $sheet[$i][5]);
	_assert(_isVisible(_select("dwfrm_multishipping_editAddress_addressFields_country")));
	_setSelected(_select("dwfrm_multishipping_editAddress_addressFields_country"), $sheet[$i][6]);
	_assert(_isVisible(_select("dwfrm_multishipping_editAddress_addressFields_states_state")));
	_setSelected(_select("dwfrm_multishipping_editAddress_addressFields_states_state"), $sheet[$i][7]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_city")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_city"), $sheet[$i][8]);
	//_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_zip")));
	//_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_zip"), $sheet[$i][9]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_postal")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_postal"), $sheet[$i][9]);
	_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_phone")));
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_phone"), $sheet[$i][10]);
	if(_isVisible(_checkbox("Add to Address Book")))
	{
	_assertNotTrue(_checkbox("Add to Address Book").checked);
	_click(_checkbox("dwfrm_multishipping_editAddress_addToAddressBook"));
	}
	_click(_submit("Save"));
}





//--------Pagination function
function pagination()
{  
	if(_getText(_div("results-hits"))==_getText(_div("/pagination/")))
		{
			_assert(true,"Single Page is displayed");		
		}
	else
		{
			if((_isVisible(_link("page-next")))&&(_isVisible(_link("page-last"))))
				{	
					_assert(_isVisible(_link("page-next")));
					_assert(_isVisible(_link("page-last")));
					_assertEqual(false,_isVisible(_link("page-previous")));
					_assertEqual(false,_isVisible(_link("page-first")));
					if(_isVisible(_link("page-last")))
						{
							_click(_link("page-last"));
						}
					_assert(_isVisible(_link("page-previous")));
					_assert(_isVisible(_link("page-first")));
					_assertEqual(false,_isVisible(_link("page-next")));
					_assertEqual(false,_isVisible(_link("page-last")));
					var $pages=_getText(_listItem("current-page"));
					if(_isVisible(_link("page-first")))
						{
							_click(_link("page-first"));
						}				
					for(var $j=1;$j<=$pages;$j++)
						{
							if($j==1)
								{
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);
								}
							else
								{
									_click(_link("page-next"));
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom)					
								}				
						}
					for(var $j=$pages;$j>=1;$j--)
						{
							if($j==$pages)
								{
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);
								}
							else
								{
									_click(_link("page-previous"));
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom)						
								}				
						}
					for(var $j=1;$j<=$pages;$j++)
						{
							if($j==1)
								{
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);
								}
							else
								{
									_click(_link("page-"+$j));
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom)							
								}				
						}
				 }
			else
				 {
					var $pageDisp=_count("_listItem","/(.*)/",_in(_div("pagination")));
					for(var $j=1;$j<=$pageDisp;$j++)
						{
							if($j==1)
								{
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);
								}
							else
								{
									_click(_link("page-"+$j));
									var $curTop=_getText(_listItem("current-page"));
						  	    	_assertEqual($j,$curTop);
						  	    	var $curBottom=  _getText(_listItem("current-page", _in(_div("pagination", _under(_list("search-result-items"))))));
						  	    	_assertEqual($j,$curBottom);					
								}				
						}				
				 }
		}
}

//--------Items per page function---------------
function ItemsperPage($itemsExpected)
{
	var $pagination=_getText(_div("pagination"));
	var $ActTotalItems=_extract($pagination, /of (.*) Results/, true).toString();
	var $ExpTotalItems=0;
	var $itemsDisplayed=0;
	if(_getText(_div("results-hits"))==_getText(_div("/pagination/")))
		{
		
			$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));//_count("_div", "/product-tile/",_in(_list("search-result-items")));
			_assertEqual($itemsDisplayed,$ActTotalItems, "Single Page is displayed");		
		}
	else
		{
			if((_isVisible(_link("page-next")))&&(_isVisible(_link("page-last"))))
			{	
				
				while(_isVisible(_link("page-last")))
					{
						_click(_link("page-last"));
					}
				var $pages=_getText(_listItem("current-page"));
				while(_isVisible(_link("page-first")))
					{
						_click(_link("page-first"));
					}
				
				for(var $j=1;$j<=$pages;$j++)
					{
						if($j==1)
							{
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								_assertEqual($itemsExpected,$itemsDisplayed);
							}
						else if($j==$pages)
							{
								_click(_link("page-"+$j));
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								var $LastPageItems=$ActTotalItems%$itemsExpected;						
								_assertEqual($LastPageItems,$itemsDisplayed);
							}
						else 
							{
								_click(_link("page-"+$j));
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								_assertEqual($itemsExpected,$itemsDisplayed);
							}
						$ExpTotalItems=$ExpTotalItems+$itemsDisplayed;
					 }				
				}
			else
				{
					var $pageDisp=_count("_listItem","/(.*)/",_in(_div("/pagination/")));
					for(var $j=1;$j<=$pageDisp;$j++)
					{
						if($j==1)
							{
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								_assertEqual($itemsExpected,$itemsDisplayed);
							}
						else if($j==$pageDisp)
							{
								_click(_link("page-"+$j));
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								var $LastPageItems=$ActTotalItems%$itemsExpected;						
								_assertEqual($LastPageItems,$itemsDisplayed);
							}
						else 
							{
								_click(_link("page-"+$j));
								$itemsDisplayed=_count("_listItem", "/grid-tile/",_in(_list("search-result-items")));
								_log("Products displayed per page"+$itemsDisplayed);
								_assertEqual($itemsExpected,$itemsDisplayed);
							}
						$ExpTotalItems=$ExpTotalItems+$itemsDisplayed;
					 }				
				}
			_assertEqual($ExpTotalItems,$ActTotalItems);
		}
}


//Price filter
function priceCheck($Range)
{
	var $priceBounds = _extract(_getText(_link($Range)), "/[$](.*) - [$](.*)/",true).toString().split(",");
	var $minPrice = $priceBounds[0].toString();
	var $maxPrice = $priceBounds[1].toString();
	_log("$minPrice = " + $minPrice);
	_log("$maxPrice = " + $maxPrice);
	if(_getText(_div("results-hits"))==_getText(_div("/pagination/")))
	{
		var $prices = _collectAttributes("_span", "/product-sales-price/","sahiText");
		for (var $k = 0; $k < $prices.length; $k++) 
			{
				var $price = parseFloat(_extract($prices[$k],"/[$](.*)/",true)[0].replace(",",""));				
				_log("$price = " + $price);				
				if ($price < $minPrice) 
				{
					_assert(false, "Price " + $price + " is lesser than min price " + $minPrice);
				} 
				else if ($price > $maxPrice) 
				{
					_assert(false, "Price " + $price + " is greater than max price " + $maxPrice);
				}					
			}		
	}
	else
	{
		if((_isVisible(_link("page-next")))&&(_isVisible(_link("page-last"))))
			{
				if(_isVisible(_link("page-last")))
					{
						_click(_link("page-last"));
					}
				var $pages=_getText(_listItem("current-page"));
				if(_isVisible(_link("page-first")))
					{
						_click(_link("page-first"));
					}
			}
		else
			 {
				var $pages=_count("_listItem","/(.*)/",_in(_div("pagination")));
			 }						
		for(var $j=1;$j<=$pages;$j++)
			{
				if(!$j==1)
					{
						_click(_link("page-next"));							
					}
				var $prices = _collectAttributes("_span", "/product-sales-price/","sahiText");
				for (var $k = 0; $k < $prices.length; $k++) 
					{
						var $price = parseFloat(_extract($prices[$k],"/[$](.*)/",true)[0].replace(",",""));				
						_log("$price = " + $price);				
						if ($price < $minPrice) 
						{
							_assert(false, "Price " + $price + " is lesser than min price " + $minPrice);
						} 
						else if ($price > $maxPrice) 
						{
							_assert(false, "Price " + $price + " is greater than max price " + $maxPrice);
						}					
					}
			}
	}
}

//--------Sort By function--------------
function sortBy($opt)
{ 
	if($opt=="Price Low To High")
	{	
		sortAllPage($opt,"_span", "/product-sales-price/");
	}     
else if($opt=="Price High To Low")
	{
		sortAllPage($opt,"_span","/product-sales-price/");
	}
else if($opt=="Product Name A - Z")
	{
		sortAllPage($opt,"_link","/name-link/");            
	}
else if($opt=="Product Name Z - A")
	{
		sortAllPage($opt,"_link","/name-link/");        
	}	    
}

function sortAllPage($opt,$tag,$field)
{
	var $arr = new Array();
	var $temp =new Array();
	if(_getText(_div("results-hits"))==_getText(_div("/pagination/")))
		{
			$arr = _collectAttributes($tag,$field,"sahiText");
			assertValues($opt,$arr);		
		}
	else
		{
			if((_isVisible(_link("page-next")))&&(_isVisible(_link("page-last"))))
			{	
				
				while(_isVisible(_link("page-last")))
					{
						_click(_link("page-last"));
					}
				var $pages=_getText(_listItem("current-page"));
				while(_isVisible(_link("page-first")))
					{
						_click(_link("page-first"));
					}					
					for(var $i=1;$i<=$pages;$i++)
						{
							if($i==1)
								{
									$temp = _collectAttributes($tag,$field,"sahiText");
									$arr.push($temp);
								}
							else
								{
									_click(_link("page-"+$i));
									$temp = _collectAttributes($tag,$field,"sahiText");
									$arr.push($temp);									
								}
						}
					assertValues($opt,$arr);
				}
			else
				{
					var $pageDisp=_count("_listItem","/(.*)/",_in(_div("/pagination/")));
					for(var $i=1;$i<=$pageDisp;$i++)
						{
							if($i==1)
								{
									$temp = _collectAttributes($tag,$field,"sahiText");
									$arr.push($temp);								
								}
							else
								{
									_click(_link("page-"+$i));
									$temp = _collectAttributes($tag,$field,"sahiText");
									$arr.push($temp);																	
								}				
						}
					assertValues($opt,$arr);
				}
		}  	   
}

function assertValues($opt,$arr)
{
  	_log("Collected array-----:"+$arr);
     var $arrExp=$arr.sort();
	 if($opt=="Product Name Ascending" ||$opt=="Price Low To High")
		{
	 		$arrExp=$arr.sort();
		}
	 else if($t=="Product Name Descending" ||$opt=="Price High to Low")
		{
	  		$arrExp=$arr.sort().reverse();
		} 
	 _log("Sorted array-----:"+$arrExp);
     _assertEqualArrays($arrExp,$arr);
}

function round(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0)
      return Math.round(value);

    value = +value;
    exp  = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
      return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
  }




function orderSummaryValidation($discount)
{
	var $TotCartItems=_count("_row","/cart-row/",_in(_div("primary")));
	var $LineItemQty;
	var $Price;
	var $DiscountPrice;
	var $ActItemTotal;
	var $ExpItemTotal=0;
	var $ActOrderSubTotal;
	var $ExpOrderSubTotal=0;

	$ActOrderSubTotal=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
	for(var $i=0; $i<$TotCartItems; $i++)
		{
		//line item Quantity
		$LineItemQty=_getText(_numberbox("dwfrm_cart_shipments_i0_items_i0_quantity", _in(_row("cart-row"))));
		
		
		//individual item price
		
		$Price=parseFloat(_extract(_getText(_span("price-sales", _in(_row("cart-row["+$i+"]")))),"/[$](.*)/",true));
			if(_isVisible(_span("price-total",_in(_row("cart-row["+$i+"]")))))
			{
			$ExpItemTotal=$LineItemQty*$Price;
			$ExpItemTotal=round($ExpItemTotal, 2);
			$ActItemTotal=parseFloat(_extract(_getText(_span("price-total",_in(_row("cart-row["+$i+"]")))),"/[$](.*)/",true));
			_assertEqual($ExpItemTotal,$ActItemTotal);
			}
		
		else{
			
			
				$ExpItemTotal=$LineItemQty*$Price;
				$DiscountPrice=($discount/100)*$ExpItemTotal;
				$ExpItemTotal=$ExpItemTotal-$DiscountPrice;
				$ExpItemTotal=round($ExpItemTotal, 2);
				$ActItemTotal=parseFloat(_extract(_getText(_span("price-adjusted-total",_in(_row("cart-row["+$i+"]")))),"/[$](.*)/",true));
				_assertEqual($ExpItemTotal,$ActItemTotal);
			
			
			}
		//Item Total Price
		$ExpOrderSubTotal=$ExpOrderSubTotal+$ExpItemTotal;
		
		}	
	_assertEqual($ExpOrderSubTotal,$ActOrderSubTotal);
//Estimated Total
	
	
}



function multiShipAddressUI()
{
	//verify whether do want to ship to multiple addresses
	if(_isVisible(_div("ship-to-multiple")))
	{
	_assertVisible(_div("ship-to-multiple"));
	_assertVisible(_submit("dwfrm_singleshipping_shipToMultiple"));
	}
	else
	{
	_assert(false,"ship to multiple is missing");
	}
}

function ogonePayment($sheet,$row)
{
	_setValue(_textbox("Ecom_Payment_Card_Number"), $sheet[$row][1]);
	_setSelected(_select("Ecom_Payment_Card_ExpDate_Month"), $sheet[$row][2]);
	_setSelected(_select("Ecom_Payment_Card_ExpDate_Year"), $sheet[$row][3]);
	_setValue(_textbox("Ecom_Payment_Card_Verification"), $sheet[$row][4]);
}

function verifyNavigationToBillingPageFromOgone($sheet)
{
	_assertVisible(_fieldset("/"+$sheet[0][3]+"/"));
	_assertVisible(_submit("dwfrm_billing_save"));
	_assert(_radio("OGONE").checked);
	_assertEqual($sheet[0][1],_getSelectedText(_select("dwfrm_billing_paymentMethods_ogoneCard_type")));	
}

function verifyOCPThroughOgone($sheet,$expAmt)
{
//	_assertVisible(_heading3($sheet[0][7]));
//	if(_isVisible(_image("CloseIcon.png")))
//	{
//		_click(_image("CloseIcon.png"));
//	}
	_assertVisible(_heading1($sheet[0][2]));
	//payment type
	_assertVisible(_div("payment-type"));
	_assertEqual($sheet[0][4], _getText(_div("payment-type")));
	//verify the amount
	_assertEqual($expAmt,_extract(_getText(_span("value",_in(_div("payment-amount")))),"/[$](.*)/",true));
	_assertVisible(_div("order-confirmation-details"));
	_assertVisible(_span("value", _in(_heading1("order-number"))));
	_assertVisible(_link("Return to Shopping"));
}

function orderPlacement($dataSheet,$paypalSheet,$creditCardSheet,$tax)
{
	//navigate to billing page
	navigateToBillingPage($dataSheet[0][0],$dataSheet[1][0]);
	if($tax=="true")
		{
		//verify the tax should not ve NA or 0.00
		var $salesTax=_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true);
			if($salesTax!="N/A" && $salesTax!="0.00" && $salesTax!="false")
				{
				_assert(true);
				}
			else
				{
				_assert(false);
				}
		}
	//enter billing address
	//enter valid billing address
	BillingAddress($Valid_Data,0);
	//enter credit card details
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
		{
			  if($CreditCardPayment==$BMConfig[0][4])
			  {
			  	PaymentDetails($paypalSheet,2);
			  }
		  	  else
			  {
			  	PaymentDetails($creditCardSheet,3);
			  }
			//click on continue
			_click(_submit("dwfrm_billing_save"));	
		}
   else if($CreditCard=="No" && $paypalNormal=="Yes")
		{
	   		_assert(_radio("PayPal").checked);
			//click on continue
			_click(_submit("dwfrm_billing_save"));	
			//paypal
   			if(isMobile())
             {
             _setValue(_emailbox("login_email"), $paypalSheet[0][0]);
             }
   			else
             {
             _setValue(_textbox("login_email"), $paypalSheet[0][0]);
             }
          _setValue(_password("login_password"), $paypalSheet[0][1]);
          _click(_submit("Log In"));
          _click(_submit("Continue"));
          _wait(10000,_isVisible(_submit("submit")));
		}
	//click on submit order
	_click(_submit("submit"));
	//verify order placement
	_assertVisible(_heading1($dataSheet[0][2]));
	_assertVisible(_span("value", _near(_span("Order Placed"))));
	_assertVisible(_span("/value/",_near(_span("Order Number"))));  
	_assertVisible(_cell("order-billing"));
	_assertVisible(_cell("order-payment-instruments"));
	_assertVisible(_table("order-totals-table"));
}


function makeServiceDown($serviceName)
{
	//verify log on service down
	BM_Login();
	_click(_link("Administration"));
	_click(_link("Operations"));
	_click(_link("Services"));
	_click(_link($serviceName));
	_uncheck(_checkbox("Enabled"));
	_click(_submit("Apply"));
	_click(_link("Log off."));	
}

function makeServiceUpAndRunning($serviceName)
{
	//verify log on service down
	BM_Login();
	_click(_link("Administration"));
	_click(_link("Operations"));
	_click(_link("Services"));
	_click(_link($serviceName));
	_check(_checkbox("Enabled"));
	_click(_submit("Apply"));
	_click(_link("Log off."));	
}

function enableEndToEnd()
{
	//verify log on service down
	BM_Login();
	_click(_link("Site Preferences"));
	_click(_link("Custom Preferences"));
	//Click on PREVAIL Integrations link
	_click(_link("PREVAIL Integrations"));
	_check(_checkbox("/(.*)/",_rightOf(_row("/Is End To End Merchant/"))));
	_click(_submit("update"));
	_click(_link("Log off."));	
}

function disableEndToEnd()
{
	//verify log on service down
	BM_Login();
	_click(_link("Site Preferences"));
	_click(_link("Custom Preferences"));
	//Click on PREVAIL Integrations link
	_click(_link("PREVAIL Integrations"));
	_uncheck(_checkbox("/(.*)/",_rightOf(_row("/Is End To End Merchant/"))));
	_click(_submit("update"));
	_click(_link("Log off."));	
}

function CheckOrderStatus($OrderNumber,$UserEmail,$Zipcode)
{
	_setValue(_textbox("dwfrm_ordertrack_orderNumber"),$OrderNumber);
	_setValue(_textbox("dwfrm_ordertrack_orderEmail"),$UserEmail);
	_setValue(_textbox("dwfrm_ordertrack_postalCode"),$Zipcode);
	_click(_submit("dwfrm_ordertrack_findorder"));
}

function verifyNavigationToShippingPage()
{
	_assertVisible(_fieldset("/SELECT OR ENTER SHIPPING ADDRESS/"));
	_assertVisible(_submit("dwfrm_singleshipping_shippingAddress_save"));	
}