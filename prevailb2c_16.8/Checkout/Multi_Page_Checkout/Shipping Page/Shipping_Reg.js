_include("../../../util.GenericLibrary/GlobalFunctions.js");
_include("../../../util.GenericLibrary/BM_Configuration.js");
_resource("Shipping Page.xls");
_resource("../BillingPage/BillingPage.xls");

var $Shipping_Page=_readExcelFile("Shipping Page.xls","Shipping_Page");
var $addr_data=_readExcelFile("Shipping Page.xls","Address");
var $Shipping_addr=_readExcelFile("Shipping Page.xls","Address2");
var $Validation=_readExcelFile("Shipping Page.xls","Validations");
var $Payment_Data=_readExcelFile("Shipping Page.xls","Payment_Data");
var $Address=_readExcelFile("Shipping Page.xls","Address1");
var $paypal=_readExcelFile("../BillingPage/BillingPage.xls","Paypal");


//verifies whether five page or single page checkout
if($CheckoutType==$BMConfig[1][1])
{
SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();
//login to the application
_click(_link("Login"));
login();


var $savedAddress=$Shipping_addr[0][7]+" "+$Shipping_addr[0][1]+" "+$Shipping_addr[0][2]+" "+$Shipping_addr[0][3]+" "+$Shipping_addr[0][4]+" "+$Shipping_addr[0][7]+" "+$Shipping_addr[1][6]+" "+$Shipping_addr[0][8]+" "+$Shipping_addr[0][5]+" "+"Phone: "+$Shipping_addr[0][9];


var $t = _testcase("126653", "Verify the navigation to  'SHIPPING' page in Application as a Registered  user");
$t.start();
try
{
//add items to cart
navigateToCart($Shipping_Page[0][1],1);
//click on go strait to checkout link
_click(_link("mini-cart-link-checkout"));
//verify the navigation
verifyNavigationToShippingPage();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
	

var $t = _testcase("126654", "Verify the UI of checkout 'SHIPPING' page in Application as a Registered  user.");
$t.start();
try
{
//verify the UI of shipping page
//first name
_assertVisible(_label("/First Name/"));
_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"));
//last name
_assertVisible(_label("/Last Name/"));
_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"));
//address1
_assertVisible(_label("/Address 1/"));
_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"));
//address2
_assertVisible(_label("Address 2"));
_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"));
//country
_assertVisible(_label("/Country/"));
_assertVisible(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"));
//state
_assertVisible(_label("/State/"));
_assertVisible(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"));
//city
_assertVisible(_label("/City/"));
_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"));
//zip
_assertVisible(_label("/Zip Code/"));
_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")); 
//phone number
_assertVisible(_label("/Phone/"));
_assertVisible(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"));
_assertVisible(_div($Shipping_Page[10][0]));
//add to address book
_assertVisible(_label($Shipping_Page[0][0]));
_assertVisible(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook"));
_assertNotTrue(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook").checked);
//use this billing address for registered user
_assertVisible(_label($Shipping_Page[1][0]));
_assertVisible(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
_assertNotTrue(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress").checked);
if(!isMobile() || mobile.iPad())
	{
	//tooltip links
	_assertVisible(_link("tooltip"));
	_assertVisible(_link("tooltip[1]"));
	}

//Is this a gift section
_assertVisible(_label("/"+$Shipping_Page[2][0]+"/"));
_assertVisible(_radio("true"));
_assertNotTrue(_radio("true").checked);
_assertVisible(_radio("false"));
_assert(_radio("false").checked);

////Is this a gift section
//_assertVisible(_label("/"+$Shipping_Page[2][0]+"/"));
//_assertVisible(_radio("is-gift-yes"));
//_assertNotTrue(_radio("is-gift-yes").checked);
//_assertVisible(_radio("is-gift-no"));
//_assert(_radio("is-gift-no").checked);

//select shipping method section
_assertVisible(_fieldset($Shipping_Page[3][0]));
var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_fieldset($Shipping_Page[3][0])));
	for(var $i=0;$i<$totalShippingMethods;$i++)
	{
	//shipping methods
	_assertVisible(_div("form-row form-indent label-inline["+$i+"]"));
	}
//continue button
_assertVisible(_submit("dwfrm_singleshipping_shippingAddress_save"));
//summary section
_assertVisible(_heading3($Shipping_Page[6][0]));
_assertVisible(_div("secondary"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126656","Verify the functionality of 'Select an Address:' drop down from Checkout Shipping page in Application as a Registered  user");
$t.start();
try
{
//navigate address page
_click(_link("MY ACCOUNT"));
_click(_link("Addresses"));
//deleting the existing addresses
deleteAddress();
//create new address
_click(_link("Create New Address"));
//adding address
addAddress($addr_data,0);
_click(_submit("dwfrm_profile_address_create"));
_wait(3000);
//navigate to shipping page
_click(_link("mini-cart-link-checkout"));
//verify the drop down
_assertVisible(_fieldset($Shipping_Page[11][0]));
_assertVisible(_label($Shipping_Page[7][0]));
//_assertVisible(_select($Shipping_Page[7][0]));
_assertVisible(_select("dwfrm_singleshipping_addressList"));
//selecting the address from drop down
_setSelected(_select("dwfrm_singleshipping_addressList"), "1");
//verify the pre population
var $defaultAddress=new Array();
$defaultAddress=$addr_data[0][2]+$addr_data[0][3]+$addr_data[0][4]+$addr_data[0][5]+$addr_data[0][6]+$addr_data[0][7]+$addr_data[0][8]+$addr_data[0][9]+$addr_data[0][10];
var $address=new Array();
$address=[_getText(_textbox("/firstName/")),_getText(_textbox("/lastName/")),_getText(_textbox("/address1/")),_getText(_textbox("/Address 2/")),_getText(_textbox("/city/")),_getText(_textbox("/postal/")),_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_country")),_getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")),_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"))];
for(var $i=0;$i<$address.length;$i++)
	{
	if($defaultAddress.indexOf($address[$i])>=0)
		{
			_assert(true, $address[$i]+" data is Prepopulated");
		}
	else
		{
		_assert(false, $address[$i]+" data is not Prepopulated");
		}
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


ClearCartItems();
var $t = _testcase("126657","Verify the display of 'Do you want to ship to multiple addresses?' when user has added single item to the cart from Checkout Shipping page in Application as a Registered  user");
$t.start();
try
{
//adding two item's to cart
navigateToCart($Shipping_Page[0][1],1);
var $cartItems=_getText(_span("minicart-quantity"));
//verify whether do want to ship to multiple addresses is there or not
if($cartItems==1)
	{
	_assertNotVisible(_div("ship-to-multiple"));
	_assertNotVisible(_submit("dwfrm_singleshipping_shipToMultiple"));
	}
else
	{
	_assert(false,"more than one item is there in cart");
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


ClearCartItems();
var $t = _testcase("126658","Verify the display of 'Do you want to ship to multiple addresses?' when user has added morethan one item to the cart from Checkout Shipping page in Application as a Registered  user");
$t.start();
try
{
//adding two item's to cart
navigateToCart($Shipping_Page[0][1],2);
//navigate to shipping page
_click(_link("mini-cart-link-checkout"));
//verify whether do want to ship to multiple addresses
multiShipAddressUI();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

ClearCartItems();
var $t = _testcase("126659","Verify the display of  'Do you want to ship to multiple addresses?' when user has added more than 1 item to the cart  from Checkout Shipping page in Application as a Registered user");
$t.start();
try
{
//adding item to cart related to suits category
navigateToCart($Shipping_Page[0][1],1);	
//adding item to cart related to another category
navigateToCart($Shipping_Page[1][1],1);	
//navigate to shipping page
_click(_link("mini-cart-link-checkout"));
//verify whether do want to ship to multiple addresses
multiShipAddressUI();
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();




var $t = _testcase("126660","Verify the navigation of 'Yes' button on Do you want to ship to multiple addresses option from Checkout Shipping page in Application as a Registered user");
$t.start();
try
{
//click on YES button on Do you want to ship to multiple addresses
_click(_submit("dwfrm_singleshipping_shipToMultiple"));
//verify the functionality
_assertVisible(_submit("dwfrm_multishipping_shipToSingle"));
_assertEqual("/"+$Shipping_Page[0][2]+"/", _getText(_div("ship-to-single")));
//verify the line items
_assertVisible(_table("item-list"));
_assertVisible(_row("cart-row  first "));
_assertVisible(_row("cart-row "));
var $addrCount=0;
//adding addresses
for(var $i=0;$i<$Address.length;$i++)
{
	_click(_span("Add/Edit Address["+$i+"]"));
	addAddressMultiShip($Address,$i);
	$addrCount++;
}
//select addresses from drop down 
var $dropDowns=_count("_select","/dwfrm_multishipping_addressSelection_quantityLineItems/",_in(_table("item-list")));
var $c1=1;
var $j=1;
for(var $i=0;$i<$dropDowns;$i++)
	{
	   if($c1>$addrCount)
	    {
	      $c1=1;
	    }
	_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"),$j);
	$j++;
	}
_click(_submit("dwfrm_multishipping_addressSelection_save"));
var $count=_count("_table","/item-list/",_in(_div("checkoutmultishipping")));

for(var $i=0;$i<$count;$i++)
	{
	//verify the 'is this a gift' section 
	_assertVisible(_label($Shipping_Page[0][8]+"["+$i+"]"));
	//by default no should select
	_assertVisible(_radio("false["+$i+"]"));
	_assert(_radio("false["+$i+"]").checked);
	//selecting yes button
	_click(_radio("true["+$i+"]"));
	//verifying the message text field
	_assertVisible(_label($Shipping_Page[1][8]+"["+$i+"]"));
	_assertVisible(_textarea("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMessage"));
	//selecting no radio button
	_click(_radio("false["+$i+"]"));
	//verify the display of message box
	_assertEqual(false,_isVisible(_div("form-row gift-message-text["+$i+"]")));
	//message field validation
	_click(_radio("true["+$i+"]"));
	//verifying the message text field
	_assertVisible(_label($Shipping_Page[1][8]));
	_assertVisible(_textarea("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMessage"));
	//enter more than 150 characters in textbox and verify the max length
	_setValue(_textarea("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMessage"),$Shipping_Page[2][8]);
	_assertEqual($Shipping_Page[3][8],_getText(_textarea("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMessage")).length);
	_assertVisible(_div($Shipping_Page[4][8]));
	_assertVisible(_div("char-count"));
	_click(_radio("false["+$i+"]"));
	}
//click on yes button to reset back
//_click(_submit("dwfrm_multishipping_shipToSingle"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


ClearCartItems();
//add items to cart
navigateToCart($Shipping_Page[0][1],1);
_click(_submit("dwfrm_cart_checkoutCart"));	
//verify the navigation
verifyNavigationToShippingPage();

var $t = _testcase("126663/126668/148984/148985/148986/148988/148989/148991/148990","Verify the field validations for 'First Name'/last name/address1/address2/city/zip code/phone number text box on shipping page in Application as a Registered user");
$t.start();
try
{
//validating the fields
for(var $i=0;$i<$Validation.length;$i++)
	{
	if($i==0)
	{
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"),"");
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"),"");
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"),"");
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"),"");
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),"");
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), "");
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"),"");
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), "");
	_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),"");
	}
else
	{
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), $Validation[$i][1]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), $Validation[$i][2]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"), $Validation[$i][3]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"),$Validation[$i][4]);
_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$Validation[$i][5]);
_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), $Validation[$i][6]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), $Validation[$i][7]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), $Validation[$i][8]);
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),$Validation[$i][9]);
	}
	//click on continue button
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//with blank data
if($i==0)
	{	
	_assert(_submit("dwfrm_singleshipping_shippingAddress_save").disabled);
	}
//checking the max length
if($i==1)
	{
	//Fname,Lname Should not highlight
	_assertNotEqual($Validation[0][10],_style(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"),"background-color"));
	_assertNotEqual($Validation[0][10],_style(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"),"background-color"));
	//for address label verification
	_assertVisible(_label("/"+$Shipping_Page[8][0]+"/"));
	_assertVisible(_label("/"+$Shipping_Page[9][0]+"/"));
	//verifying the length of field's first name,last name,add1,addr2,phone number,City 
	_assertEqual($Validation[1][10],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName")).length);
	_assertEqual($Validation[1][10],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName")).length);
	_assertEqual($Validation[1][10],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1")).length);
	_assertEqual($Validation[1][10],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2")).length);
	_assertEqual($Validation[1][10],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city")).length);
	_assertEqual($Validation[0][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone")).length);
	//Zip Code max length
	_assertEqual($Validation[1][11],_getText(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal")).length); 
	}
//numeric,special chars,combination of both,alphabets data in city,phone number,Zip code field's
if($i==2 || $i==3 || $i==4 || $i==5)
	{
	//Fname,Lname Should not highlight
	_assertNotEqual($Validation[0][10],_style(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"),"background-color"));
	_assertNotEqual($Validation[0][10],_style(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"),"background-color"));
	//Zip Code
	_assertEqual($Validation[6][10],_style(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"),"background-color")); 
	_assertEqual($Validation[4][10],_style(_span($Validation[2][11]),"color"));
	_assertVisible(_span($Validation[2][11])); 
	//Phone Number
	_assertEqual($Validation[6][10],_style(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),"background-color"));
	_assertVisible(_span($Validation[2][10])); 
	_assertEqual($Validation[4][10],_style(_span($Validation[2][10]),"color")); 
	//City
	//In Progress
	
	}
//state field validation
if($i==6)
	{
	_assert(_submit("dwfrm_singleshipping_shippingAddress_save").disabled);
	}
//with valid data
if($i==7)
	{
	//UPS verification
	if($AddrVerification==$BMConfig[0][2])
	{
		if(_isVisible(_submit("ship-to-original-address")))
			{
				_click(_submit("ship-to-original-address"));
			}
	}
	//group1 verification
	else if($AddrVerification==$BMConfig[1][2])
	{
	if(_isVisible(_submit("ship-to-original-address[1]")))
		{
		_click(_submit("ship-to-original-address[1]"));
		}
	}
	//Should navigate to billing page
	_assertVisible(_fieldset("/"+$Shipping_Page[0][3]+"/"));
	_assertVisible(_submit("dwfrm_billing_save"));
	}

//In Progress

}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

/*
var $t = _testcase("148987","Verify the validation of 'Country' field on shipping page in Prevail application as a Registered user");
$t.start();
try
{
//navigating back to shipping page
_click(_link("Shipping"));	
//selecting US from drop down
var $countries=_getText(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"));
for(var $i=0;$i<$countries.length;$i++)
  {
	_assertEqual($countries[$i],$Shipping_Page[$i][4]);
	//selecting US from drop down
	if($i==1)
		{
		_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$Shipping_Page[$i][4]);
		//verify the states present in states drop down
			for(var $j=0;$j<$Shipping_Page[5][5];$j++)
			{
			_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"),$Shipping_Page[$j][5]);
			//verifying the selection
			_assertEqual($Shipping_Page[$j][5], _getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
			}
		}
	//select the Germany from drop down
	if($i==2)
		{
		_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$Shipping_Page[$i][4]);
		//verify the label changed to other or not
		_assertVisible(_label("/"+$Shipping_Page[0][7]+"/"));
			for(var $j=0;$j<$Shipping_Page[2][7];$j++)
			{
			_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"),$Shipping_Page[1][7]);
			//verifying the selection
			_assertEqual($Shipping_Page[1][7], _getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
			}
		}
	//select the canada from drop down
	if($i==3)
	{
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$Shipping_Page[$i][4]);
	//verify the label changed to province or not
	_assertVisible(_label("/"+$Shipping_Page[5][6]+"/"));
		for(var $j=0;$j<$Shipping_Page[5][5];$j++)
		{
		_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"),$Shipping_Page[$j][6]);
		//verifying the selection
		_assertEqual($Shipping_Page[$j][6], _getSelectedText(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state")));
		}
	}
  }

}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
*/

var $t = _testcase("126661","Verify the functionality of the Checkbox 'Use this address for Billing' on shipping page in Prevail application as a Registered user");
$t.start();
try
{
//navigating back to shipping page
_click(_link("Shipping"));	
//entering the shipping address
shippingAddress($Shipping_addr,0);
//check the use this address for billing checkbox
_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//UPS verification
if($AddrVerification==$BMConfig[0][2])
{
	_click(_submit("ship-to-original-address"));
}
//group1 verification
else if($AddrVerification==$BMConfig[1][2])
{
if(_isVisible(_submit("ship-to-original-address[1]")))
	{
	_click(_submit("ship-to-original-address[1]"));
	}
}
//verify the address prepopulation in billing page
_assertEqual($Shipping_addr[0][1],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
_assertEqual($Shipping_addr[0][2],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
_assertEqual($Shipping_addr[0][3],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
_assertEqual($Shipping_addr[0][5],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
_assertEqual($Shipping_addr[0][6],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
_assertEqual($Shipping_addr[0][7],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
_assertEqual($Shipping_addr[0][8],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_postal")));
_assertEqual($Shipping_addr[0][9],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_phone")));
//navigating back to shipping page
_click(_link("Shipping"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126664","Verify the functionality of 'Is this a gift?:' radio buttons on shipping page in Prevail application as a Registered user");
$t.start();
try
{
//verifying the is this gift order
_assertVisible(_label("/"+$Shipping_Page[0][8]+"/"));

//by default no should be selected
_assert(_radio("false").checked);
//selecting yes button
_click(_radio("true"));
//verifying the message text field
_assertVisible(_label($Shipping_Page[1][8]));
_assertVisible(_textarea("dwfrm_singleshipping_shippingAddress_giftMessage"));
//selecting no radio button
_click(_radio("false"));
//verify the display of message box
_assertEqual(false,_isVisible(_div("form-row gift-message-text")));		
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126665","Verify the validation of 'Message' field on  shipping page in Prevail application as a Registered user");
$t.start();
try
{
//selecting is this gift yes button
//selecting yes button
_click(_radio("true"));
//verifying the message text field
_assertVisible(_label($Shipping_Page[1][8]));
_assertVisible(_textarea("dwfrm_singleshipping_shippingAddress_giftMessage"));
//enter more than 150 characters in textbox and verify the max length
_setValue(_textarea("dwfrm_singleshipping_shippingAddress_giftMessage"),$Shipping_Page[2][8]);
_assertEqual($Shipping_Page[3][8],_getText(_textarea("dwfrm_singleshipping_shippingAddress_giftMessage")).length);
_assertVisible(_div($Shipping_Page[4][8]));
_assertVisible(_div("char-count"));		
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126666","Verify the UI of 'SELECT SHIPPING METHOD' Section on shipping page in Prevail application as a Registered user");
$t.start();
try
{
//verifying the UI of Shipping section
_assertVisible(_fieldset($Shipping_Page[3][0]));
var $totalToolTips=_count("_link","tooltip",_in(_fieldset($Shipping_Page[3][0])));
var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_fieldset($Shipping_Page[3][0])));
_assertEqual($totalShippingMethods,$totalToolTips);
for(var $i=0;$i<$totalShippingMethods;$i++)
{
//shipping methods
_assertVisible(_div("form-row form-indent label-inline["+$i+"]"));
_assertVisible(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
/*
//mousehovering on the tooltips
_mouseOver(_link("tooltip",_in(_div("form-row form-indent label-inline["+$i+"]"))));
//verifying the display of tool tip
_assertVisible(_div("small tooltip-shipping"));
*/	
}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126667","Verify the functionality of radio buttons under 'SELECT SHIPPING METHOD' Section on shipping page in Prevail application as a Registered user");
$t.start();
try
{
//verify the functionality of radio buttons
var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_fieldset($Shipping_Page[3][0])));
var $count=0;
for(var $i=0;$i<$totalShippingMethods;$i++)
	{
	//selecting radio buttons
	_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
	//verify the selection
	_assert(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked);
	//uncheck the checkbox
	_uncheck(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
	//verify the deselection
	_assert(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked);
	for(var $j=0;$j<$totalShippingMethods;$j++)
		{
		if(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked)
			{
			$count++;
			}
		}
	}		
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126670","Verify the functionality of 'EDIT' link in order summary Section in right nav on shipping page in Prevail application as a Registered user");
$t.start();
try
{
//navigating to shipping page
if(!isMobile() || mobile.iPad())
{
var $productName=_getText(_link("/(.*)/",_in(_div("mini-cart-name",_in(_div("checkout-mini-cart"))))));
//clicking on edit link present in the order Summuary section
_click(_link("EDIT",_in(_div("secondary"))));
}
else
{
var $productName=_getText(_link("/(.*)/",_in(_div("mini-cart-name",_in(_div("checkout-mini-cart"))))));
//clicking on edit link present in the order Summuary section
_click(_link("Edit",_in(_heading3("ORDER SUMMARY Edit"))));
}
//verify the navigation to Cart page

_assertVisible(_div("cart-actions cart-actions-top"));
_assertVisible(_table("cart-table"));
_assertVisible(_link("/(.*)/",_in(_div("name"))));
_assertEqual($productName,_getText(_link("/(.*)/",_in(_div("name")))));
//fetch the total items in cart
var $total=_count("_row","/cart-row/",_in(_table("cart-table")));
for(var $i=0;$i<$total;$i++)
	{	
//updating the quantity
	_setValue(_numberbox("input-text", _in(_cell("item-quantity"))),$Shipping_Page[5][5]);
	}
//click on update cart
_click(_submit("update-cart"));
_wait(5000,_isVisible(_link("mini-cart-link-checkout")));
//navigate to shipping page
_click(_link("mini-cart-link-checkout"));


//verify the cart updation
for(var $i=0;$i<$total;$i++)
{
_assertEqual($Shipping_Page[5][5], _getText(_span("value",_in(_div("mini-cart-pricing["+$i+"]",_in(_div("checkout-mini-cart")))))));
}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126671","Verify the navigation of 'product name' in order summary Section in right nav on shipping page in Prevail application as a Registered user");
$t.start();
try
{
//clicking on product name in shipping page
if(!isMobile() || mobile.iPad())
	{
var $productName=_getText(_link("/(.*)/",_in(_div("mini-cart-name",_in(_div("secondary"))))));
_click(_link("/(.*)/",_in(_div("mini-cart-name",_in(_div("secondary"))))));
	}
else
	{
	var $productName=_getText(_link("/(.*)/",_in(_div("mini-cart-name",_in(_div("checkout-mini-cart"))))));
	_click(_link("/(.*)/",_in(_div("mini-cart-name",_in(_div("checkout-mini-cart"))))));
	}
//verify the navigation to respective PD Page
_assertVisible(_div("breadcrumb"));
if(!isMobile())
	{
_assertVisible(_span("breadcrumb-element"));
	}
_assertEqual($productName, _getText(_span("breadcrumb-element")));
_assertVisible(_heading1("product-name"));
_assertEqual($productName, _getText(_heading1("product-name")));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


//navigate to shipping page
navigateToShippingPage($Shipping_Page[0][1],1);

if(!isMobile())
{
var $t = _testcase("126672","Verify the mouse hover link functionality for 'APO/FPO and Why is this required?' on shipping page in Prevail application as a Registered user");
$t.start();
try
{
//mouse hover on apo/Fpo tool tip
	 _mouseOver(_link("tooltip",_near(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"))));
                     //verifying the display of tool tip
                     if(_getAttribute(_link("tooltip"),"aria-describedby")!=null)
                           {
                                  _assert(true);
                           }
                     else
                           {
                                  _assert(false);
                           }


//mouse hovering on why is this required tool tip
_mouseOver(_link("tooltip",_in(_div("/Why is this required/"))));

                     //verifying the display of tool tip
                     if(_getAttribute(_link("tooltip[1]"),"aria-describedby")!=null)
                           {
                                  _assert(true);
                           }
                     else
                           {
                                  _assert(false);
                           }
                     
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
}


var $t = _testcase("126674","Verify the functionality of shipping charge and country selected on shipping page in Prevail application as a Registered user");
$t.start();
try
{
	//changing the shipping method 
	var $totalradio=_count("_radio","/dwfrm_singleshipping_shippingAddress_shippingMethodID/");
	if($totalradio!=0)
		{
			for(var $i=0;$i<$totalradio;$i++)
				{
				_click(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
				//verify wehter added in order summuary section or not
				_assertVisible(_label("/"+$Shipping_Page[$i][10]+"/"));
				}
		}
	else
		{
			_log(false,"Need to configure Shipping Methods");
		}/*
	//selecting the Country as Canada
	_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),$Shipping_Page[3][4]);
	//verifying the state field
	if(isMobile())
		{
		_assertVisible(_label("/"+$Shipping_Page[5][6]+"/"));
		}
	else
		{
	_assertVisible(_label("/"+$Shipping_Page[5][6]+"/",_leftOf(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"))));
		}*/
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126673","Verify the display of 'Details' tool tip in select shipping option Section on shipping page in Prevail application as a Registered user");
$t.start();
try
{
//verifying the tooltips
var $totalToolTips=_count("_link","tooltip",_in(_fieldset($Shipping_Page[3][0])));
var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_fieldset($Shipping_Page[3][0])));
_assertEqual($totalShippingMethods,$totalToolTips);
for(var $i=0;$i<$totalShippingMethods;$i++)
	{
	//mousehovering on the tooltips
    _mouseOver(_link("tooltip",_in(_div("form-row form-indent label-inline["+$i+"]"))));
    //verifying the display of tool tip
    if(_getAttribute(_link("tooltip",_in(_div("form-row form-indent label-inline["+$i+"]"))),"aria-describedby")!=null)
          {
                 _assert(true);
          }
    else
          {
                 _assert(false);
          }
	}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148992/126662","Verify the functionality of 'CONTINUE >' button/Add To Address Book on shipping page in Prevail application as a Registered user");
$t.start();
try
{
//delete if any existing addresses are there
_click(_link("My Account"));
_click(_link("Addresses"));
while(_isVisible(_link("Delete")))
	{
	_click(_link("Delete"));
	_wait(3000);
	}
//navigate to shipping page
_click(_link("/mini-cart-link-checkout/"));
//enter data in all optinal fields
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_firstName"), "");
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_lastName"), "");
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address1"),"");
_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_country"),"");
_setSelected(_select("dwfrm_singleshipping_shippingAddress_addressFields_states_state"), "");
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_city"), "");
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_postal"), "");
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_phone"),"");
_setValue(_textbox("dwfrm_singleshipping_shippingAddress_addressFields_address2"),$Shipping_addr[0][4]);
//click on continue button
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//verify the display
_assert(_submit("dwfrm_singleshipping_shippingAddress_save").disabled);
//enter all required data except optional data
shippingAddress($Shipping_addr,0);
//check add to address check box
_check(_checkbox("dwfrm_singleshipping_shippingAddress_addToAddressBook"));
//click on continue button
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//UPS verification
if($AddrVerification==$BMConfig[0][2])
{
	_click(_submit("ship-to-original-address"));
}
//group1 verification
else if($AddrVerification==$BMConfig[1][2])
{
if(_isVisible(_submit("ship-to-original-address[1]")))
	{
	_click(_submit("ship-to-original-address[1]"));
	}
}
//verify the navigation to billing page
_assertVisible(_fieldset("/"+$Shipping_Page[0][3]+"/"));
_assertVisible(_submit("dwfrm_billing_save"));
//navigate to my account page
_assertVisible(_link("user-account"));
_click(_link("My Account"));
_click(_link("Addresses"));
//verify the presence of saved address
_assertVisible(_listItem("address-tile  default"));
_assertVisible(_div("mini-address-location"));
var $adress=$Shipping_addr[0][7]+" "+$Shipping_addr[0][1]+" "+$Shipping_addr[0][2]+" "+_getText(_div("mini-address-location")).replace(",","");
_assertEqual($savedAddress,$adress);
//deleting the address
_click(_link("Delete"));	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


ClearCartItems(); 
var $t = _testcase("148993","Verify the calculation functionality in Order Summary pane on shipping page in Prevail application as a Registered user");
$t.start();
try
{
//navigating to Shipping page
navigateToShippingPage($Shipping_Page[0][1],1);
//verifying the order summury pane
if(!isMobile() || mobile.iPad())
{
_assertVisible(_div($pName,_in(_div("secondary"))));
_assertVisible(_span("/"+$Shipping_Page[1][3]+"/",_in(_div("secondary"))));
_assertEqual($quantity, _getText(_span("value",_rightOf(_span("/Qty:/",_in(_div("secondary")))))));
_assertEqual($price,_extract(_getText(_span("mini-cart-price",_in(_div("secondary")))),"/[$](.*)/",true).toString());
}
else
{
_assertVisible(_div($pName,_in(_div("checkout-mini-cart"))));
_assertVisible(_span("/"+$Shipping_Page[1][3]+"/",_in(_div("checkout-mini-cart"))));
_assertEqual($quantity, _getText(_span("value",_rightOf(_span("/Qty:/",_in(_div("checkout-mini-cart")))))));
_assertEqual($price,_extract(_getText(_span("mini-cart-price",_in(_div("checkout-mini-cart")))),"/[$](.*)/",true).toString());
}
_assertEqual($totalPrice,_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString());
if(isMobile() && !mobile.iPad())
{
_assertEqual($shippingTax,_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString());
}
else
{
_assertEqual($shippingTax,_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString());
}
_assertEqual($tax,_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString());
_assertEqual($orderTotal,_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("148994/148995","Verify the functionality of 'CONTINUE>' button/verify the contents of verify your shipping address page on shipping page in Prevail application as a Registered user");
$t.start();
try
{
//enter the shipping address
shippingAddress($Shipping_addr,0);	
//click on continue button
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//UPS verification
if($AddrVerification==$BMConfig[0][2])
{
	//verify the UI contents of dialog box
	_assertVisible(_heading1($Shipping_Page[1][2]));
	_assertVisible(_div("address-validation-dialog"));
	_assertVisible(_paragraph("/(.*)/",_in(_div("address-validation-dialog"))));
	//original address
	_assertVisible(_heading2($Shipping_Page[2][2]));
	_assertVisible(_div("original-address left-pane"));
	_assertVisible(_submit("original-address-edit"));
	_assertVisible(_submit("ship-to-original-address"));
	//suggested address
	_assertVisible(_heading2($Shipping_Page[3][2]));
	_assertVisible(_div("suggested-addresses origin"));
	_assertVisible(_submit("Edit",_in(_div("suggested-addresses origin"))));
	_assertVisible(_submit("ship-to-address-selected-1"));
	
	//click on link edit address
	_click(_submit("Edit",_in(_div("suggested-addresses origin"))));
	//again click on continue
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	if(_isVisible(_div("address-validation-dialog")))
	{
		_click(_submit("ship-to-original-address"));
	}
	//directly should navigate to billing page
	_assertVisible(_fieldset("/"+$Shipping_Page[0][3]+"/"));
	_assertVisible(_submit("dwfrm_billing_save"));
}
else
{
	_log("Enable the UPS in BM");
}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();
}