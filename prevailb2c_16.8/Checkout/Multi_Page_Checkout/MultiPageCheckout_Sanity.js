_include("../../util.GenericLibrary/GlobalFunctions.js");
_include("../../util.GenericLibrary/BM_Configuration.js");
_resource("IntermediateLoginPage/IntermediateLogin_Data.xls");
_resource("EndToEndScenarios/Paypal/BillingPage.xls");
_resource("BillingPage/BillingPage.xls");
_resource("OrderSummaryPage/OrderSummuary.xls");
_resource("OrderConfirmationPage/OrderConfirmation.xls");


//Intermediate
var $Item=_readExcelFile("IntermediateLoginPage/IntermediateLogin_Data.xls","Item");
var $IMLogin_Data=_readExcelFile("IntermediateLoginPage/IntermediateLogin_Data.xls","Login_Data");
//End to end and billing
var $item = _readExcelFile("BillingPage/BillingPage.xls","Item");
var $Valid_Data=_readExcelFile("BillingPage/BillingPage.xls","ValidData");
var $Generic=_readExcelFile("BillingPage/BillingPage.xls","Generic");
var $paypal=_readExcelFile("EndToEndScenarios/Paypal/BillingPage.xls","Paypal");
var $CouponValidations=_readExcelFile("BillingPage/BillingPage.xls","CouponValidations");
//Order summary
var $OrderSummuary=_readExcelFile("OrderSummaryPage/OrderSummuary.xls","OrderSummuary");
var $ValidData=_readExcelFile("OrderSummaryPage/OrderSummuary.xls","Valid_Data");
//OCP
var $Validation=_readExcelFile("OrderConfirmationPage/OrderConfirmation.xls","Validation");



if($CheckoutType==$BMConfig[1][1])
{
if($GiftCardPayment==$BMConfig[0][3]  && $giftCertificate=="Yes")
{
//creating giftcard
createGC($GC);
//_log("created");
}
//for deleting user
deleteUser();

SiteURLs();
cleanup();
_setAccessorIgnoreCase(true); 

var $t = _testcase("126708", "Verify the functionality of 'Forgot password' link on intermediate login page in Prevail application as an Anonymous user.");
$t.start();
try
{
	//navigate to cart page
	navigateToCart($Item[0][0],$Item[0][1]);
	//click on go straight to checkout
	_click(_link("mini-cart-link-checkout"));
	//validating IM login page
	_assertVisible(_heading2("/"+$IMLogin_Data[0][0]+"/"));
	//clicking on forgot password link
	_click(_link("password-reset"));
	//verify whether overlay got opened or not
	_assertVisible(_div("dialog-container"));
	_assertVisible(_span("ui-dialog-title"));
	_assertEqual($IMLogin_Data[0][2], _getText(_span("ui-dialog-title")));
	_assertVisible(_button("Close"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
 $t.end();


 
var $t = _testcase("126710","To verify functionality when clicked on 'Login' button in intermediate login page");
$t.start();
try
{
	//navigate to Cart page
	_click(_link("View Cart"));
	_click(_submit("dwfrm_cart_checkoutCart"));
	login();
	//verify navigation to checkout shipping page
	_assertVisible(_link("user-account"));
	_assertEqual("STEP 1: Shipping", _getText(_div("step-1 active")));
	_assertVisible(_fieldset("/SELECT OR ENTER A SHIPPING ADDRESS /"));
	//removing item from cart
	ClearCartItems();
	//logout from application
	_click(_link("Account"));
	_click(_link("Logout"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

//removing item from cart
ClearCartItems()
if($GiftCardPayment==$BMConfig[0][3] && $giftCertificate=="Yes")
{
var $t=_testcase("149064","Verify whether user is able to place the order using Gift Card alone on billing page in Application as a Anonymous user.");
$t.start();
try
{
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping details
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//verify address verification overlay
	addressVerificationOverlay();
	//applying GC
	_setValue(_textbox("dwfrm_billing_giftCertCode"),$GC[2][0]);
	_setValue(_textbox("dwfrm_billing_giftCertPin"),$GC[2][1]);
	_click(_submit("dwfrm_billing_redeemGiftCert"));
	//Billing details
	BillingAddress($Valid_Data,0);
	_click(_submit("dwfrm_billing_save"));
	//Placing order
	_click(_submit("submit"));
	if(_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
	//Checking order is placed or not
	_assertVisible(_heading1($Generic[0][2]));
	_assertVisible(_div("order-confirmation-details"));
	
	//checking GC
	 _assert(_isVisible(_cell("order-payment-instruments")));
	 var $exp=$GC[2][0].replace("GC","**");
	 var $act=_extract(_getText(_cell("order-payment-instruments")),"/Gift Certificate (.*) Amount/",true).toString();
	 _assertEqual($exp,$act);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();
}

//removing item from cart
ClearCartItems();
if($paypalNormal=="Yes")
{
var $t=_testcase("126697","Verify whether user is able to place the order using Paypal alone  on billing page in Application as a Anonymous user.");
$t.start();
try
{
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping details
	shippingAddress($Valid_Data,0);
	_wait(3000,_isVisible(_submit("dwfrm_singleshipping_shippingAddress_save")));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	if($AddrVerification==$BMConfig[0][2])
	{
		if(_isVisible(_div("address-validation-dialog")))
			{
				_click(_submit("ship-to-original-address"));
			}
	}
	//Billing details
	BillingAddress($Valid_Data,0);
	//Payment section through paypal
	if(($CreditCard=="Yes" && $paypalNormal=="Yes"))
	{
		_click(_radio("PayPal"));	
	}
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	{
		_assert(_radio("PayPal").checked);
	}
	_wait(3000,_isVisible(_submit("dwfrm_billing_save")));
	_click(_submit("dwfrm_billing_save"));
	_wait(10000);
	if(isMobile())
		 {
		 	_setValue(_emailbox("login_email"), $paypal[0][0]);
		 }
	 else
		 {
		 _setValue(_textbox("login_email"), $paypal[0][0]);
		 }
	 _setValue(_password("login_password"), $paypal[0][1]);
	 _click(_submit("Log In"));
	 _click(_submit("Continue"));
	 _wait(10000,_isVisible(_submit("submit")));
	//Placing order
	_click(_submit("submit"));
	if(_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
	//Checking order is placed or not
	_assertVisible(_heading1($Generic[0][2]));
	_assertVisible(_div("order-confirmation-details"));
	
	//checking Paypal
	 var $paymentType=_getText(_div("payment-type"));
	 _assertEqual($paypal[0][2],$paymentType);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();
}



var $t=_testcase("126626","Verify the functionality of 'Place Order' button on Summary Page in Application as a Anonymous user.");
$t.start();
try
{
	//Navigation till shipping page
	navigateToShippingPage($OrderSummuary[0][7],$OrderSummuary[0][8]);
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//Address Verification overlay
	addressVerificationOverlay();
	BillingAddress($ValidData,0); 
	//entering Credit card Information
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
	    {
			if($CreditCardPayment==$BMConfig[0][4])
			  {
			  	PaymentDetails($paypal,2);
			  }
		  	  else
			  {
			  	PaymentDetails($ValidData,4);
			  }
	           _click(_submit("dwfrm_billing_save"));
	    }
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	    {
	          _click(_radio("PayPal"));
	           _click(_submit("dwfrm_billing_save"));
	           if(isMobile())
	           {
	                 _setValue(_emailbox("login_email"), $paypal[0][0]);
	           }
	           else
	           {
	           _setValue(_textbox("login_email"), $paypal[0][0]);
	           }
	    _setValue(_password("login_password"), $paypal[0][1]);
	    _click(_submit("Log In"));
	    _click(_submit("Continue"));
	    _wait(10000,_isVisible(_submit("submit")));
	    }
	//Click on Place Order
	_click(_submit("Place Order"));
	if(_isVisible(_image("CloseIcon.png")))
	{
	_click(_image("CloseIcon.png"));
	}
	//verify the navigation to thank you page
	_assertVisible(_heading1($OrderSummuary[0][5]));
	_assertVisible(_heading2($OrderSummuary[1][5]));
	_assertVisible(_table("item-list"));
	_assertVisible(_table("order-shipment-table"));	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("126736","Verify the functionality of  'CREATE AN ACCOUNT' button in right nav on Thank you for your order page in Application as a Anonymous user.");
$t.start();
try
{
	_setValue(_textbox("dwfrm_profile_customer_email"),$email);
	_setValue(_textbox("dwfrm_profile_customer_emailconfirm"),$email);
	_setValue(_password("dwfrm_profile_login_password"),$Validation[9][5]);
	_setValue(_password("dwfrm_profile_login_passwordconfirm"),$Validation[9][6]);          
	_click(_submit("dwfrm_profile_confirm"));
	//verify the navigation to Profile page
	_assertVisible(_div("breadcrumb"));
	_assertVisible(_link("user-account"));
	//verifying whether user is logged in or not
	if(isMobile() && !mobile.iPad())
		{
			_assertVisible(_link("user-account"));
			_assertVisible(_link("Logout"));
		}
	else
		{
			_assertVisible(_link("user-account"));
		}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

cleanup();


//Registered----------------------------

if($GiftCardPayment==$BMConfig[0][3]  && $giftCertificate=="Yes")
{
//creating giftcard
createGC($GC);
//_log("created");
}
SiteURLs();
cleanup();
//login to the application
_click(_link("Login"));
login();

if($CreditCard=="Yes")
{
	if($CreditCardPayment==$BMConfig[1][4] || $CreditCardPayment==$BMConfig[2][4])
	{  
		var $t=_testcase("149059/149060/149061/149062/149065","Verify whether user is able to place order using VISA, MASTERCARD, AMERICAN EXPRESS, DISCOVER as credit card type  and Credit card alone on billing page in Application as a Registered user.");
		$t.start();
		try
		{
			for(var $i=4;$i<$Valid_Data.length;$i++)
				{
					_click(_link("user-account"));
					_click(_link("Payment Settings"));
					while(_isVisible(_submit("Delete Card")))
					{
					  _click(_submit("Delete Card"));
					  _expectConfirm("/Do you want/",true);
					}	
					//Navigation till shipping page
					navigateToShippingPage($item[0][0],$item[0][1]);
					//shipping details
					shippingAddress($Valid_Data,0);
					_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
					//address verification over lay
					addressVerificationOverlay();
					//Billing details
					BillingAddress($Valid_Data,0);
					//Payment details
					PaymentDetails($Valid_Data,$i);
					_click(_submit("dwfrm_billing_save"));
					//Placing order
					_click(_submit("submit"));
					if(_isVisible(_image("CloseIcon.png")))
						{
						_click(_image("CloseIcon.png"));
						}
					//Checking order is placed or not
					_assertVisible(_heading1($Generic[0][2]));
					_assertVisible(_div("order-confirmation-details"));
				}
		}
		catch($e)
		{      
		       _logExceptionAsFailure($e);
		}
		$t.end();
	}
}

//removing item from cart
ClearCartItems()
if($GiftCardPayment==$BMConfig[0][3] && $giftCertificate=="Yes")
{
	var $t=_testcase("149063","Verify whether user is able to place the order using Gift Card alone on billing page in Application as a Anonymous user.");
	$t.start();
	try
	{
		//Navigation till shipping page
		navigateToShippingPage($item[0][0],$item[0][1]);
		//shipping details
		shippingAddress($Valid_Data,0);
		_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
		//address verification over lay
		addressVerificationOverlay();
		//applying GC
		_setValue(_textbox("dwfrm_billing_giftCertCode"),$GC[2][0]);
		_setValue(_textbox("dwfrm_billing_giftCertPin"),$GC[2][1]);
		_click(_submit("dwfrm_billing_redeemGiftCert"));
		//Billing details
		BillingAddress($Valid_Data,0);
		_click(_submit("dwfrm_billing_save"));
		//Placing order
		_click(_submit("submit"));
		if(_isVisible(_image("CloseIcon.png")))
			{
			_click(_image("CloseIcon.png"));
			}
		//Checking order is placed or not
		_assertVisible(_heading1($Generic[0][2]));
		_assertVisible(_div("order-confirmation-details"));
		
		//checking GC
		 _assert(_isVisible(_cell("order-payment-instruments")));
		 var $exp=$GC[2][0].replace("GC","**");
		 var $act=_extract(_getText(_cell("order-payment-instruments")),"/Gift Certificate (.*) Amount/",true).toString();
		 _assertEqual($exp,$act);
	}
	catch($e)
	{      
	       _logExceptionAsFailure($e);
	}
	$t.end();
}

if($paypalNormal=="Yes")
{
var $t=_testcase("128376"," Verify whether user is able to place the order using Paypal alone  on billing page in Application as a Anonymous user.");
$t.start();
try
{
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping details
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	if($AddrVerification==$BMConfig[0][2])
	{
		if(_isVisible(_div("address-validation-dialog")))
			{
				_click(_submit("ship-to-original-address"));
			}
	}
	//Billing details
	BillingAddress($Valid_Data,0);
	//Payment section through paypal
	if(($CreditCard=="Yes" && $paypalNormal=="Yes"))
	{
		_click(_radio("PayPal"));	
	}
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	{
		_assert(_radio("PayPal").checked);
	}
	_click(_submit("dwfrm_billing_save"));
	if(isMobile())
		 {
		 	_setValue(_emailbox("login_email"), $paypal[0][0]);
		 }
	 else
		 {
		 _setValue(_textbox("login_email"), $paypal[0][0]);
		 }
	 _setValue(_password("login_password"), $paypal[0][1]);
	 _click(_submit("Log In"));
	 _click(_submit("Continue"));
	 _wait(10000,_isVisible(_submit("submit")));
	//Placing order
	_click(_submit("submit"));
	if(_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
	//Checking order is placed or not
	_assertVisible(_heading1($Generic[0][2]));
	_assertVisible(_div("order-confirmation-details"));
	
	//checking Paypal
	 var $paymentType=_getText(_div("payment-type"));
	 _assertEqual($paypal[0][2],$paymentType);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();
}


var $t=_testcase("126625","Verify the functionality of 'Place Order' button on Summary Page in Application as a Registered user.");
$t.start();
try
{
	navigateToShippingPage($OrderSummuary[0][7],$OrderSummuary[0][8]);
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//Address Verification overlay
	addressVerificationOverlay();
	BillingAddress($Valid_Data,0); 
	//entering Credit card Information
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
	    {
			if($CreditCardPayment==$BMConfig[0][4])
			  {
			  	PaymentDetails($paypal,2);
			  }
		  	  else
			  {
			  	PaymentDetails($Valid_Data,4);
			  }
	           _click(_submit("dwfrm_billing_save"));
	    }
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	    {
	          _click(_radio("PayPal"));
	           _click(_submit("dwfrm_billing_save"));
	           if(isMobile())
	           {
	                 _setValue(_emailbox("login_email"), $paypal[0][0]);
	           }
	           else
	           {
	           _setValue(_textbox("login_email"), $paypal[0][0]);
	           }
	    _setValue(_password("login_password"), $paypal[0][1]);
	    _click(_submit("Log In"));
	    _click(_submit("Continue"));
	    _wait(10000,_isVisible(_submit("submit")));
	    }

	//Click on Place Order
	_click(_submit("Place Order"));
	if(_isVisible(_image("CloseIcon.png")))
	{
	_click(_image("CloseIcon.png"));
	}
	//verify the navigation to thank you page
	_assertVisible(_heading1($OrderSummuary[0][5]));
	_assertVisible(_heading2($OrderSummuary[1][5]));
	_assertVisible(_table("item-list"));
	_assertVisible(_table("order-shipment-table"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("126689","Verify whether sub total / Order Total/  Discount/ Shipping gets updated after applying the valid coupon code in Billing page in Application as a Registered user.  Note: Verify the same for Guest user also.");
$t.start();
try
{
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//click on continue
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));	
	//Address Verification overlay
	addressVerificationOverlay();
	//Billing details
	BillingAddress($Valid_Data,0);
	//Fetching values
	var $subtotalBeforeApplyingCoupon = parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
	var $ordertotalBeforeApplyingCoupon = parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
	_setValue(_textbox("dwfrm_billing_couponCode"),$CouponValidations[2][1]);
	_click(_span("APPLY"));
	_assert(_isVisible(_div("/redemption coupon/")));
	var $sucessMsg="Promo Code "+$CouponValidations[2][1]+" has been added to your order and was applied.";
	//enter credit card details
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
		{
		  if($CreditCardPayment==$BMConfig[0][4])
		  {
		  	PaymentDetails($paypal,2);
		  }
	  	  else
		  {
		  	PaymentDetails($Valid_Data,4);
		  }
		//click on continue
		_click(_submit("dwfrm_billing_save"));	
		}
   else if($CreditCard=="No" && $paypalNormal=="Yes")
		{
   		_assert(_radio("PayPal").checked);
		//click on continue
		_click(_submit("dwfrm_billing_save"));	
		//paypal
   		if(isMobile())
             {
             _setValue(_emailbox("login_email"), $paypal[0][0]);
             }
          else
             {
             _setValue(_textbox("login_email"), $paypal[0][0]);
             }
          _setValue(_password("login_password"), $paypal[0][1]);
          _click(_submit("Log In"));
          _click(_submit("Continue"));
          _wait(10000,_isVisible(_submit("submit")));
		}
	//verify wehter valid Coupon got applied or not
	_assertVisible(_span("Applied"));
	_assertVisible(_div("promo  first "));
	_assertVisible(_span("Coupon Number:"));
	if(isMobile())
		{
		_assertVisible(_span("Coupon_Product",_near(_span("Coupon Number:"))));
		}
	else
		{
		_assertVisible(_span("Coupon_Product",_rightOf(_span("Coupon Number:"))));
		}
	_assertVisible(_span("value",_in(_div("discount clearfix  first "))));
	var $discountPrice=parseFloat(_extract(_getText(_span("value",_in(_div("discount clearfix  first ")))),"/[$](.*)/",true).toString());
	var $expDiscountPrice=$subtotalBeforeApplyingCoupon-$discountPrice;
	var $actDiscountPrice=parseFloat(_getText(_cell("item-total")).replace("$",""));
	//verifying the price
	_assertEqual($expDiscountPrice,$actDiscountPrice);
	//verify whether subtotal section got applied or not
	var $subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true);
	_assertEqual($actDiscountPrice,$subTotal);		
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
cleanup();
}


//128527
_include("EndToEndScenarios/Paypal/Multishipment1GCandPaypal.js");

//removing item from cart
ClearCartItems()
//128526
_include("EndToEndScenarios/CreditCard/MultishipPaymentWith1GCandCreditcard.js");










