_include("../../../../util.GenericLibrary/GlobalFunctions.js");
_include("../../../../util.GenericLibrary/BM_Configuration.js");
_resource("BillingPage.xls");

var $item = _readExcelFile("BillingPage.xls","Item");
var $Valid_Data=_readExcelFile("BillingPage.xls","ValidData");
var $Address=_readExcelFile("BillingPage.xls","Address");
var $Coupon=_readExcelFile("BillingPage.xls","CouponValidations");
var $paypal=_readExcelFile("BillingPage.xls","Paypal");

if($CheckoutType==$BMConfig[1][1] && $paypalNormal=="Yes")
{

SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();

_click(_link("Login"));
login();


 var $t=_testcase("128386","Verify order placement with  promotion and Paypal  on billing page in Application as a Registered  user..");
 $t.start();
 try
 {
 var $total;
 _click(_link("Addresses"));
 var $c=_count("_div", "/mini-address-location/");
  for(var $i=0;$i<$c;$i++)
 {
   _click(_link("Delete"));
 } 
 _log($Address.length);
 for(var $i=0;$i<$Address.length;$i++)
 {	
	 _log($i);
 	_wait(2000,_isVisible(_link("Create New Address"))); 		
 	_click(_link("Create New Address")); 	
 	addAddress($Address,$i);
 	_click(_submit("Apply"));
 	
 } 
	_wait(3000);
	//Navigation till shipping page
 addItemToCart($item[0][0],$item[0][1]);
 _click(_link("mini-cart-link"));
 
//Applying promotion code
 _setValue(_textbox("dwfrm_cart_couponCode"),$Coupon[2][1]);
 _click(_submit("dwfrm_cart_addCoupon"));
 if(_isVisible(_span("bonus-item"))){
	 var $actualCost=_extract(_getText(_span("price-sales")),"/[$](.*)/",true);
	 _log("Promotion Applied");
	 _assert(_isVisible(_cell("item-details[1]")));
	 _assert(_isVisible(_submit("textbutton")));
	 var $promotionAmt=parseFloat(_extract(_getText(_div("discount")),"/[$](.*)/",true));
	 $actualCost=$actualCost-$promotionAmt;
	 var $priceAfterPromotion=_extract(_getText(_row("order-total")),"/[$](.*)/",true);
	 _assertEqual($actualCost,$priceAfterPromotion);
 }
 
//navigating to shipping page
	_click(_submit("dwfrm_cart_checkoutCart"));
	if(!_isVisible(_link("user-account")))
		{
		_click(_submit("Checkout"));
		}

	//shipping details
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	if(_isVisible(_div("address-validation-dialog")))
		{
			_assertEqual($Valid_Data[0][3]+" "+$Valid_Data[0][4]+" "+$Valid_Data[0][7]+" , "+$Valid_Data[1][6]+" "+$Valid_Data[0][5]+" "+$Valid_Data[0][8], _getText(_paragraph("/(.*)/", _in(_div("original-address left-pane")))));
		}
	addressVerificationOverlay();
	var $OrderTotal=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
	 
	//Payment section through paypal
		_click(_radio("PayPal"));
		_click(_submit("dwfrm_billing_save"));
		if(isMobile())
			 {
			 	_setValue(_emailbox("login_email"), $paypal[0][0]);
			 }
		 else
			 {
			 _setValue(_textbox("login_email"), $paypal[0][0]);
			 }
		 _setValue(_password("login_password"), $paypal[0][1]);
		 _click(_submit("Log In"));
		 _click(_submit("Continue"));
		 _wait(10000,_isVisible(_submit("submit")));
		//Placing order
		 
		 _wait(10000,_isVisible(_submit("submit")));
		 if(_isVisible(_submit("submit")))
		 {
		   _assert(true, "User navigated to order review page using Paypal as a payment method");
		   _click(_submit("submit"));
		 }
		 else
		 {
		   _assert(false, "User navigated to order review page using Paypal as a payment method");
		 }
		//checking whether navigated to Order conformation Page
		 if(_isVisible(_image("CloseIcon.png"))){
		 _click(_image("CloseIcon.png"));
		 }
		 _assertVisible(_heading1("Thank you for your order."));
		 
	//checking Paypal
		 var $paymentType=_getText(_div("payment-type"));
		 _assertEqual($paypal[0][2],$paymentType);
		 var $paypalAmount=parseFloat(_extract(_getText(_div("payment-amount")),"/[$](.*)/",true));
		 
		//checking Order total
		 if(_getText(_row("order-total")).indexOf("Order Total: $")>-1){
			 var $orderTotalAct=_getText(_row("order-total")).replace("Order Total: $","");
		 }else{
		 var $orderTotalAct=_getText(_row("order-total")).replace("Order Total:$","");
		 }
		 _assertEqual($OrderTotal,$orderTotalAct);
		 var $total=$paypalAmount;
		 _assertEqual($OrderTotal,$total);
 }
 catch($e)
 {      
        _logExceptionAsFailure($e);
 }
$t.end();
cleanup();
} 