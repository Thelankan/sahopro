_include("../../../../util.GenericLibrary/GlobalFunctions.js");
_include("../../../../util.GenericLibrary/BM_Configuration.js");
_resource("BillingPage.xls");

var $item = _readExcelFile("BillingPage.xls","Item");
var $Valid_Data=_readExcelFile("BillingPage.xls","ValidData");
var $Address=_readExcelFile("BillingPage.xls","Address");
var $paypal=_readExcelFile("BillingPage.xls","Paypal");


if($CheckoutType==$BMConfig[1][1] && $GiftCardPayment==$BMConfig[0][3] && $giftCertificate=="Yes" && $paypalNormal=="Yes")
{
//creating giftcard
createGC($GC);
SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();
_click(_link("Login"));
login();
var $t=_testcase("128525","Verify order placement  with 2 GC and Paypal  on billing page in Application as a Registered  user..");
$t.start();
try
{
	 var $total;
	 deleteAddress();
	 _log($Address.length);
	 for(var $i=0;$i<$Address.length;$i++)
		 {	
			 _log($i);
		 	_wait(2000,_isVisible(_link("Create New Address"))); 		
		 	_click(_link("Create New Address")); 	
		 	addAddress($Address,$i);
		 	_click(_submit("Apply")); 	
		 }
	 _wait(3000);
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][2]);
	//shipping details
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	if(_isVisible(_div("address-validation-dialog")))
		{
			_assertEqual($Valid_Data[0][3]+" "+$Valid_Data[0][4]+" "+$Valid_Data[0][7]+" , "+$Valid_Data[1][6]+" "+$Valid_Data[0][5]+" "+$Valid_Data[0][8], _getText(_paragraph("/(.*)/", _in(_div("original-address left-pane")))));
		}
	addressVerificationOverlay();
	var $OrderTotal=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
	_setValue(_textbox("dwfrm_billing_giftCertCode"),$GC[0][0]);
	_setValue(_textbox("dwfrm_billing_giftCertPin"), $GC[0][1]);
	_click(_submit("dwfrm_billing_redeemGiftCert"));	 
	 if(_assertExists(_div("success giftcert-pi")))
	 {
		 var $didamt=parseFloat(_extract(_getText(_div("success giftcert-pi")),"/USD (.*) has/",true));
		 _log("Diducted amount is"+$didamt);
		  var $remamt=$OrderTotal-$didamt;
		 _log($remamt); 	 
		 _setValue(_textbox("dwfrm_billing_giftCertCode"),$GC[1][0]);
		 _setValue(_textbox("dwfrm_billing_giftCertPin"), $GC[1][1]);
		 _click(_submit("dwfrm_billing_redeemGiftCert"));
		 if(_assertExists(_div("success giftcert-pi[1]")))
			 {
				 var $didamt1=parseFloat(_extract(_getText(_div("success giftcert-pi[1]")),"/USD (.*) has/",true));
				 _log("Diducted amount is"+$didamt1);
				  var $remamt=$remamt-$didamt1;
				 _log($remamt);
			 }
		 if($remamt!=0)
		 {
			 _assert(true);
		 }
		 else
		 {
			 _assert(false);
		 }
	 }
	 else
		 {
		 _assert(false,"successful message is not displayed");
		 }	 
	//Payment section through paypal
		_click(_radio("PayPal"));
		_click(_submit("dwfrm_billing_save"));
		if(isMobile())
			 {
			 	_setValue(_emailbox("login_email"), $paypal[0][0]);
			 }
		 else
			 {
			 _setValue(_textbox("login_email"), $paypal[0][0]);
			 }
		 _setValue(_password("login_password"), $paypal[0][1]);
		 _click(_submit("Log In"));
		 _click(_submit("Continue"));
		 _wait(10000,_isVisible(_submit("submit")));
		//Placing order
		 
		 _wait(10000,_isVisible(_submit("submit")));
		 if(_isVisible(_submit("submit")))
		 {
		   _assert(true, "User navigated to order review page using Paypal as a payment method");
		   _click(_submit("submit"));
		 }
		 else
		 {
		   _assert(false, "User navigated to order review page using Paypal as a payment method");
		 }
		//checking whether navigated to Order conformation Page
		 if(_isVisible(_image("CloseIcon.png"))){
		 _click(_image("CloseIcon.png"));
		 }
		 _assertVisible(_heading1("Thank you for your order."));	 
	//checking 2GC
		var $count=_count("_span","/Gift Certificate/");
		 _assert(_isVisible(_div("order-payment-instruments")));
		 _assertEqual(2,$count);
		 var $exp=$GC[0][0].replace("GC","");
		 _log($exp);
		 _assertContainsText($exp,_div("orderpaymentinstrumentsgc"));
		 var $exp1=$GC[1][0].replace("GC","");
		 _log($exp1);
		 _assertContainsText($exp1,_div("orderpaymentinstrumentsgc[1]"));
	//checking Paypal
		 var $paymentType=_getText(_div("payment-type"));
		 _assertEqual($paypal[0][2],$paymentType);
		 var $paypalAmount=parseFloat(_extract(_getText(_div("payment-amount")),"/[$](.*)/",true));
		 
		//checking Order total
		 if(_getText(_row("order-total")).indexOf("Order Total: $")>-1){
			 var $orderTotalAct=_getText(_row("order-total")).replace("Order Total: $","");
		 }else{
		 var $orderTotalAct=_getText(_row("order-total")).replace("Order Total:$","");
		 }
		 _assertEqual($OrderTotal,$orderTotalAct);

		 _log($didamt);
		 _log($didamt1);
		 _log($paypalAmount);
		 var $total=$didamt+$didamt1+$paypalAmount;
		 _assertEqual($OrderTotal,$total);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();
cleanup();
deleteGC($GC);
}		 
		 
		 