_include("../../../../util.GenericLibrary/GlobalFunctions.js");
_include("../../../../util.GenericLibrary/BM_Configuration.js");
_resource("BillingPage.xls");

var $item = _readExcelFile("BillingPage.xls","Item");
var $Valid_Data=_readExcelFile("BillingPage.xls","ValidData");
var $Address=_readExcelFile("BillingPage.xls","Address");
var $Generic=_readExcelFile("BillingPage.xls","Generic");

if($CheckoutType==$BMConfig[1][1] && $GiftCardPayment==$BMConfig[0][3] && $giftCertificate=="Yes" && $CreditCard=="Yes")
{
	//creating giftcard
	createGC($GC);
	SiteURLs()
	_setAccessorIgnoreCase(true); 
	cleanup();
	_click(_link("Login"));
	login();
	//removing item from cart
	ClearCartItems();
	
	var $t=_testcase("128526","Multishipment with 1GC and Creditcard");
	$t.start();
	try
	{
	 var $total;
	 deleteAddress();
	 _log($Address.length);
	 for(var $i=0;$i<$Address.length;$i++)
	 {	
		 _log($i);
	 	_wait(2000,_isVisible(_link("Create New Address"))); 		
	 	_click(_link("Create New Address")); 	
	 	addAddress($Address,$i);
	 	_click(_submit("Apply")); 	
	 } 	
	 _wait(3000);
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][2]); 
	 //Multishipment 
	 _click(_submit("dwfrm_singleshipping_shipToMultiple"));
	 var $ItemCount=_count("_select", "/select-address required/",_in(_table("item-list")));
	 var $k=1;
	 for(var $i=0; $i<$ItemCount;$i++)
		 {
		 _setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"), $k);
		 $k++;
		 }
	 //address save 
	 _click(_submit("dwfrm_multishipping_addressSelection_save"));
	 //shipping options
	 _click(_submit("dwfrm_multishipping_shippingOptions_save"));
	//address verification over lay
	addressVerificationOverlay();
	 var $OrderTotal=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
	 _setValue(_textbox("dwfrm_billing_giftCertCode"),$GC[0][0]);
	 _setValue(_textbox("dwfrm_billing_giftCertPin"), $GC[0][1]);
	 _click(_submit("dwfrm_billing_redeemGiftCert")); 
	 if(_assertExists(_div("success giftcert-pi")))
	 {
		 var $didamt=parseFloat(_extract(_getText(_div("success giftcert-pi")),"/USD (.*) has/",true));
		 _log("Diducted amount is"+$didamt);
		  var $remamt=$OrderTotal-$didamt;
		 _log($remamt); 
	 }
	 //payment
	 PaymentDetails($Valid_Data,4);	 
	 _wait(3000,_isVisible(_submit("dwfrm_billing_save")));
	 _click(_submit("dwfrm_billing_save"));
	//address verification over lay
	addressVerificationOverlay();
	//checking whether navigated to Order review Page
	if(_isVisible(_submit("submit")))
	{
		_click(_submit("submit"));
	}
	//checking whether navigated to Order conformation Page
	if(_isVisible(_image("CloseIcon.png")))
	{
		_click(_image("CloseIcon.png"));
	}
	_wait(5000,_isVisible(_heading1($Generic[0][2])));
	_assertVisible(_heading1($Generic[0][2]));
	var $creditCardamt=parseFloat(_extract(_getText(_div("payment-amount")),"/[$](.*)/",true));
	_log("Credit card amount is"+$creditCardamt);
	//checking UI in Order Confirmation page
	//checking 1GC
	var $count=_count("_span","/Gift Certificate/");
	 _assertVisible(_div("order-payment-instruments"));
	 _assertEqual(1,$count);
	 var $exp=$GC[0][0].replace("GC","");
	 _log($exp);
	 _assertContainsText($exp,_div("orderpaymentinstrumentsgc"));
	 //checking credit card
	 var $act=_getText(_div("payment-type"));
	 _assertEqual($Valid_Data[4][9],$act); 
	//checking Order total
	 if(_getText(_row("order-total")).indexOf("Order Total: $")>-1)
	 {
		 var $orderTotalAct=_getText(_row("order-total")).replace("Order Total: $","");
	 }
	 else
	 {
		 var $orderTotalAct=_getText(_row("order-total")).replace("Order Total:$","");
	 } 
	 _assertEqual($OrderTotal,$orderTotalAct);
	 $total=$didamt+$creditCardamt;
	 _assertEqual($OrderTotal,$total);
	}
	catch($e)
	{      
	       _logExceptionAsFailure($e);
	}
	$t.end();
	cleanup();
	deleteGC($GC);
}