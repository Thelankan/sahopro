_include("../../../util.GenericLibrary/GlobalFunctions.js");
_include("../../../util.GenericLibrary/BM_Configuration.js");
_resource("BillingPage.xls");


var $item = _readExcelFile("BillingPage.xls","Item");
var $Valid_Data=_readExcelFile("BillingPage.xls","ValidData");
var $Generic=_readExcelFile("BillingPage.xls","Generic");
var $Billing_Data = _readExcelFile("BillingPage.xls","Validation");
var $Background = _readExcelFile("BillingPage.xls","Background");
var $color=$Background[0][0];
var $State_Country=_readExcelFile("BillingPage.xls","State_Country_Validation");
var $PaymentValidation=_readExcelFile("BillingPage.xls","PaymentValidation");
var $CouponValidations=_readExcelFile("BillingPage.xls","CouponValidations");
var $GC_Validation=_readExcelFile("BillingPage.xls","GC_Validation");
var $paypal=_readExcelFile("BillingPage.xls","Paypal");
var $Createcard=_readExcelFile("../../../Profile/Payment/Payment.xls","CreateCard");
var $Address=_readExcelFile("BillingPage.xls","Address");
//var $Zip = _readExcelFile("BillingPage.xls","ZipCodeValidation");
var $OrderSummuary=_readExcelFile("BillingPage.xls","OrderSummuary");

var $sheetName;
var $rowNum;


if($CheckoutType==$BMConfig[1][1])
{
if($GiftCardPayment==$BMConfig[0][3]  && $giftCertificate=="Yes")
	{
	//creating giftcard
	createGC($GC);
	//_log("created");
	}

SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();

_click(_link("Login"));
login();
//removing item from cart
ClearCartItems();
var $UserEmail=$uId;

var $billingAddress=$Valid_Data[0][1]+" "+$Valid_Data[0][2]+" "+$Valid_Data[0][3]+" "+$Valid_Data[0][4]+" "+$Valid_Data[0][7]+", "+$Valid_Data[1][6]+" "+$Valid_Data[0][8]+" "+$Valid_Data[1][5]+" "+$Valid_Data[0][9];


var $t=_testcase("126699","Verify the navigation to Checkout Billing page in Application as a Registered user.");
$t.start();
try
{
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	var $shippingMethod=_extract(_getText(_row("/order-shipping  first/")),"/Shipping(.*)[$]/",true).toString().trim();
	//shipping details
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	if($AddrVerification==$BMConfig[0][2])
		{
			if(_isVisible(_div("address-validation-dialog")))
			{
				_assertEqual($Valid_Data[0][3]+" "+$Valid_Data[0][4]+" "+$Valid_Data[0][7]+" , "+$Valid_Data[1][6]+" "+$Valid_Data[0][5]+" "+$Valid_Data[0][8], _getText(_paragraph("/(.*)/", _in(_div("original-address left-pane")))));
				_click(_submit("ship-to-original-address"));
			}
		}
	else if($AddrVerification==$BMConfig[1][2])
	{
	if(_isVisible(_submit("ship-to-original-address[1]")))
		{
		_click(_submit("ship-to-original-address[1]"));
		}
	}
	//verify the navigation to Billing page
	_assertVisible(_div("/"+$Generic[0][1]+"/"));
	_assertVisible(_fieldset("/"+$Generic[6][0]+"/"));
	_assertVisible(_submit("dwfrm_billing_save"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("126675/126676/126698","Verify the UI of 'BILLING' page,'ENTER GIFT CERTIFICATE OR COUPON/DISCOUNT CODES' section and 'SELECT PAYMENT METHOD' and 'ORDER SUMMARY' section in an Application as a Registered user.");
$t.start();
try{
	//verify the UI of Billig page 
	billingPageUI($Generic);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("124451","Verify the UI for Shipping address &amp;amp; Order Summary display in billing accordion by selecting one Shipping address for a particular product as a registered user");
$t.start();
try
{
	//verify the UI of Order Summary Section
	verifyOrderSummary("Single",$taxService,$item[0][1]);
	//Shipping Address Section
	_assertVisible(_heading3("/SHIPPING ADDRESS/"));
	var $shippingAddress=$billingAddress+" "+"Method: "+$shippingMethod;
	_assertEqual($shippingAddress,_getText(_div("details",_in(_div("/mini-shipment order-component-block  first/")))));	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("126700","Verify the UI for  Order Summary, Shipping, billing address &amp; Payment details in the right side of billing page When user navigate back to billing page from Order review page");
$t.start();
try
{
	//fill the billing details
	BillingAddress($Valid_Data,0);
	//fill the payment details
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
	{
		  if($CreditCardPayment==$BMConfig[0][4])
		  {
		  	PaymentDetails($paypal,2);
		  }
	  	  else
		  {
		  	PaymentDetails($Valid_Data,4);
		  }
	   _click(_submit("dwfrm_billing_save"));
	}
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	{
	  _click(_radio("PayPal"));
	   _click(_submit("dwfrm_billing_save"));
	   if(isMobile())
	   {
	         _setValue(_emailbox("login_email"), $paypal[0][0]);
	   }
	   else
	   {
	   _setValue(_textbox("login_email"), $paypal[0][0]);
	   }
	   _setValue(_password("login_password"), $paypal[0][1]);
	    _click(_submit("Log In"));
	    _click(_submit("Continue"));
	    _wait(10000,_isVisible(_submit("submit")));
	  }
	_wait(3000);
	//come back to billing page
	_click(_link("Edit",_in(_heading3("/BILLING ADDRESS/"))));
	//verify the UI of Order Summary Section
	verifyOrderSummary("Single",$taxService,$item[0][1]);
	//Shipping Address Section
	_assertVisible(_heading3("/SHIPPING ADDRESS/"));
	var $shippingAddress=$billingAddress+" "+"Method: "+$shippingMethod;
	_assertEqual($shippingAddress,_getText(_div("details",_in(_div("/mini-shipment order-component-block  first/")))));	
	//verify the billing address
	_assertVisible(_heading3("/BILLING ADDRESS/"));
	_assertEqual($billingAddress,_getText(_div("details",_in(_div("mini-billing-address order-component-block")))));
	//payment details
	_assertVisible(_heading3("/PAYMENT METHOD/"));
	//verifying the payment details section
	if(!_isSafari())
		{
		_assertVisible(_heading3($OrderSummuary[9][1]));
		_assertVisible(_div("/details/", _in(_div("/mini-payment-instrument/"))));
		var $payment_Details=_getText(_div("details",_in(_div("/mini-payment-instrument/"))));
		
		}
	else
		{
		_assertVisible(_heading3($OrderSummuary[9][1]));
		_assertVisible(_div("details",_near(_heading3($OrderSummuary[9][1]))));
		var $payment_Details=_getText(_div("details",_near(_heading3($OrderSummuary[9][1]))));
		}
	if($CreditCard=="Yes")
		{
		  if($CreditCardPayment==$BMConfig[0][4])
		  {
			  $sheetName=$paypal;
			  $rowNum=2;
		  }
		  else
		  {
			  $sheetName=$Valid_Data;
			  $rowNum=4;
		  }
		var $ExpCreditCard_Details=$OrderSummuary[1][1]+" "+$sheetName[$rowNum][7]+" "+$OrderSummuary[3][1]+" $"+$actOrderTotal;
		_assertEqual($ExpCreditCard_Details,$payment_Details);
		}
	else
		{
		var $ExpPayment_Details="Pay Pal "+$OrderSummuary[3][1]+" $"+$actOrderTotal;
		_assertEqual($ExpPayment_Details,$payment_Details);
		}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("149075","Verify the functionality of 'EDIT' link in SHIPPING ADDRESS Section in right nav on billing page in Application as a Registered user");
$t.start();
try
{
	//click on edit link present in shipping address sectio right nav
	_click(_link("Edit",_in(_heading3("/SHIPPING ADDRESS/"))));
	//verify the navigation
	_assertVisible(_fieldset("/SELECT OR ENTER SHIPPING ADDRESS/"));
	_assertVisible(_submit("dwfrm_singleshipping_shippingAddress_save"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("126716/126717/126718/148997/148999/149000/126687/126701/126715","Verify 'First Name','Last Name','Address 1','Address 2','City', and 'Phone'  Field validation on billing page and functionality of 'CONTINUE' button in Application as a Registered user.");
$t.start();
try
{
	//click on continue
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));	
	//address verification over lay
	addressVerificationOverlay();
    //navigate back to billing page
    _click(_link("Billing"));
	for(var $i=0;$i<$Billing_Data.length;$i++)
		{
		if($i!=3)
		{
			if($i==0)
			{
			_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_firstName"), "");
			_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_lastName"), "");
			_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address1"), "");
			_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_address2"),"");
			_setSelected(_select("dwfrm_billing_billingAddress_addressFields_country"),"");
			_setSelected(_select("dwfrm_billing_billingAddress_addressFields_states_state"), "");
			_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_city"), "");
			_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_postal"), "");
			_setValue(_textbox("dwfrm_billing_billingAddress_addressFields_phone"),"");
			_setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),"");
			}
			BillingAddress($Billing_Data,$i);
			if($i==9)
				{
				_setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),$UserEmail);
				}
			else
				{
				_setValue(_textbox("dwfrm_billing_billingAddress_email_emailAddress"),$Billing_Data[$i][10]);
				}			
			//fill payment details
			if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
				{
				  if($CreditCardPayment==$BMConfig[0][4])
        		  {
        		  	PaymentDetails($paypal,2);
        		  }
			  	  else
        		  {
        		  	PaymentDetails($Valid_Data,4);
        		  }
				}
			else if($CreditCard=="No" && $paypalNormal=="Yes")
			{
		   		_assert(_radio("PayPal").checked);
			}
			//Blank field validation
			if($i==0)
				{
				//_click(_submit("dwfrm_billing_save"));
				if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
				{
				_assert(_submit("dwfrm_billing_save").disabled);
				}               
				}
			//max characters
			else if($i==1)
		       {	
				_assertEqual($Billing_Data[0][12],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")).length);
				_assertEqual($Billing_Data[1][12],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")).length);
				_assertEqual($Billing_Data[2][12],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")).length);
				_assertEqual($Billing_Data[3][12],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address2")).length);
				_assertEqual($Billing_Data[4][12],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_city")).length);
				_assertEqual($Billing_Data[7][12],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_postal")).length);
				_assertEqual($Billing_Data[5][12],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_phone")).length);
		       }
			//special characters
			else if($i==4)
				{
					_click(_submit("dwfrm_billing_save"));
					_assertVisible(_span($Billing_Data[$i][13]));
					_assertVisible(_span($Billing_Data[$i][14]));
					_assertVisible(_span($Billing_Data[$i][15]));
				}
			else if($i==5)
				{
				_assertVisible(_div($Billing_Data[$i][13]));
				_assertEqual($Billing_Data[8][12],_style(_div($Billing_Data[$i][13]),"color"));
				}
			//valid
			else if($i==9)
				{		
				if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
				{
					_click(_submit("dwfrm_billing_save"));
				}
				if($CreditCard=="No" && $paypalNormal=="Yes")
				{
			   		_assert(_radio("PayPal").checked);
	     		//click on continue
	     		_click(_submit("dwfrm_billing_save"));	
	     		//paypal
	        		if(isMobile())
	                      {
	                            _setValue(_emailbox("login_email"), $paypal[0][0]);
	                      }
	               else
	                      {
	                      _setValue(_textbox("login_email"), $paypal[0][0]);
	                      }
	               _setValue(_password("login_password"), $paypal[0][1]);
	               _click(_submit("Log In"));
	               _click(_submit("Continue"));
	               _wait(10000,_isVisible(_submit("submit")));

				}
					//address verification over lay
					addressVerificationOverlay();
					_assert(_isVisible(_submit("Place Order")));
					_assert(_isVisible(_div("order-summary-footer")));
				}	
			//Invalid data
			else
				{
					_click(_submit("dwfrm_billing_save"));
					_assertVisible(_span($Billing_Data[$i][13]));
					if($i!=5 && $i!=8)
					{
					_assertVisible(_span($Billing_Data[$i][14]));
					}
				}			
		}
		}
}
catch($e)
{
	 _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148998/126714","Verify the validation of 'Country' and 'state' field validation on billing page in Application as a Registered user.");
$t.start();
try
{
	
	//navigating back to billing
	_click(_link("Billing"));
//	 for(var $i=0;$i<$State_Country.length;$i++)
//		{ 
		 _assertEqual($State_Country[0][11],_getText(_select("dwfrm_billing_billingAddress_addressFields_states_state")).toString());
	//	}
	 
}catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


if($CreditCard=="Yes")
{
var $t=_testcase("126721","Verify the Field validation of Expiration  Date on billing page in Application as a Registered user.");
$t.start();
try
{
	var $year=_getText(_select("dwfrm_billing_paymentMethods_creditCard_expiration_year"));
	var $length=_getText(_select("dwfrm_billing_paymentMethods_creditCard_expiration_year")).length;
	var $currentYear=parseInt(new Date().toString().split(" ")[3]);
	var $nextYear=$currentYear+1;
	var $count=0;
	for(var $i=0;$i<$length;$i++){
	if($i==0)
		{
			_assert($year[$i]==$currentYear);
		}
	else
		{
			_assert($year[$i]==$nextYear);
			$nextYear=$nextYear+1;
			$count++;
		}
	}
	if($count==10)
		{
			_assert(true,"Total No.of years are Current year+10");
		}
	else
		{
			_assert(false,"Total No.of years are not Current year+10");
		}	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("126719/126690/126691/126692","Verify the Field validation of Card Number,Security Code,'Name on Card'and 'Type' on billing page in Application as a Anonymous user.");
$t.start();
try
{
	//enter billing Address
	BillingAddress($Valid_Data,0); 
	//validating the payment fields
	for(var $i=0;$i<$PaymentValidation.length;$i++)
		{	
		if($i==0)
			{
		    _setValue(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"), "");
			 _setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"),"");
		    _setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"),"");
			}
		else
			{
			PaymentDetails($PaymentValidation,$i)
			}	
	    //blank field validation
	    if($i==0)
	    	{
	    	_click(_submit("dwfrm_billing_save"));
	    	_assertEqual($color,_style(_textbox("dwfrm_billing_paymentMethods_creditCard_owner"),"background-color"));
	    	_assertEqual($color,_style(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/"),"background-color"));
	    	_assertEqual($color,_style(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"),"background-color"));
	    	}
	    //Max
	    else  if($i==1)
	    	{
	    	_assertEqual($PaymentValidation[0][9],_getText(_textbox("dwfrm_billing_paymentMethods_creditCard_owner")).length);
	    	_assertEqual($PaymentValidation[1][9],_getText(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/")).length);
	    	_assertEqual($PaymentValidation[2][9],_getText(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/")).length);
	    	}
	    //invalid credit card number
	    else if($i>=2 && $i<=11)
	    	{
	    	_click(_submit("dwfrm_billing_save"));
	    	//verify card num error msg
	    	_assert(_isVisible(_div($PaymentValidation[$i][7])));
	    	_assertEqual($PaymentValidation[0][10],_style(_div($PaymentValidation[$i][7]),"color"));
	    	}
	    //Invalid CVV
	    else if($i>=12 && $i<=19)
			{
    		_click(_submit("dwfrm_billing_save"));
    	   	//verify text box color    		
        	_assertEqual($PaymentValidation[1][10],_style(_textbox("dwfrm_billing_paymentMethods_creditCard_cvn"), "background-color"));
			}
	    else if($i>=20 && $i<=26)
			{
			_click(_submit("dwfrm_billing_save"));
		   	//verify card num error msg
	    	_assert(_isVisible(_span($PaymentValidation[$i][7])));
	    	_assertEqual($PaymentValidation[0][10],_style(_span($PaymentValidation[$i][7]),"color"));
			}
	  //expiration year
	    else //if($i==27)
			{	
    		_click(_submit("dwfrm_billing_save"));
    	   	//verify card num error msg
        	_assert(_isVisible(_span($PaymentValidation[$i][7])));
        	_assertEqual($PaymentValidation[0][10],_style(_span($PaymentValidation[$i][7]),"color"));
			}    
		}
}
catch($e)
{      
       _logExceptionAsFailure($e);
       _click(_link("Billing"));
}
$t.end();


if(!isMobile())
{
	var $t=_testcase("128313","Verify the functionality of '( What is this?)' link on billing page in Application as a Anonymous user.");
	$t.start();
	try
	{
		_assertVisible(_link("tooltip", _in(_div("form-row cvn required"))));
		_mouseOver(_link("tooltip", _in(_div("form-row cvn required"))));
		//verifying the display of tool tip
        if(_getAttribute(_link("tooltip", _in(_div("form-row cvn required"))),"aria-describedby")!=null)
              {
                     _assert(true);
              }
        else
              {
                     _assert(false);
              }
	}
	catch($e)
	{      
	       _logExceptionAsFailure($e);
	}
	$t.end();
}

}
//navigate to billing
navigateToBillingPageWithData();


var $t=_testcase("149001","Verify the navigation of 'See Privacy Policy' link on billing page in Application as a Registered user.");
$t.start();
try
{
	_assertVisible(_link("See Privacy Policy"));
	//click on privacy policy
	_click(_link("See Privacy Policy"));
	_assertVisible(_div("dialog-container"));
	_assertVisible(_div("ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-draggable"));
	_assertVisible(_heading1($Generic[3][1]));
	_assertEqual($Generic[3][1], _getText(_heading1("content-header privacy-policy")));
	//closing overlay
	_click(_button("Close"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("149073","Verify the functionality of 'EDIT' link in order summary Section in right nav on billing page in Application as a Registered user.");
$t.start();
try
{
	//Order summary section
	_assertVisible(_heading3("/ORDER SUMMARY/"));
	_assertVisible(_link("Edit"));
	_click(_link("Edit"));
	//verify the navigation to cart page
	 _assertVisible(_table("cart-table"));
	 _assertVisible(_div("cart-actions cart-actions-top"));		
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


//navigate to billing
navigateToBillingPageWithData();


var $t=_testcase("126688/126686/126689","Verify the Field validation & functionality of 'Enter coupon code' field on billing page in Application as a Registered user");
$t.start();
try{
//validations 
	for(var $i=0;$i<$CouponValidations.length;$i++)
		{
			//Navigation till shipping page
			search($CouponValidations[$i][0]);
			_click(_submit("add-to-cart"));
			_click(_link("View Cart"));
			//navigating to shipping page
			_click(_submit("dwfrm_cart_checkoutCart"));
			_wait(4000);
			if(_isVisible(_div("login-box login-account")))
				{
				_click(_submit("dwfrm_login_unregistered"));
				}
			//shipping details
			shippingAddress($Valid_Data,0);
			_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
			if($AddrVerification==$BMConfig[0][2])
				{
					if(_isVisible(_div("address-validation-dialog")))
					{
						_assertEqual($Valid_Data[0][3]+" "+$Valid_Data[0][4]+" "+$Valid_Data[0][7]+" , "+$Valid_Data[1][6]+" "+$Valid_Data[0][5]+" "+$Valid_Data[0][8], _getText(_paragraph("/(.*)/", _in(_div("original-address left-pane")))));
						_click(_submit("ship-to-original-address"));
					}
				}	
			else if($AddrVerification==$BMConfig[1][2])
				{
				if(_isVisible(_submit("ship-to-original-address[1]")))
					{
					_click(_submit("ship-to-original-address[1]"));
					}
				}
			var $subtotalBeforeApplyingCoupon;	
			var $ordertotalBeforeApplyingCoupon;
			if($i==2)
				{
					$subtotalBeforeApplyingCoupon=parseFloat(_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true));
					$ordertotalBeforeApplyingCoupon=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true));
				}
			_setValue(_textbox("dwfrm_billing_couponCode"),$CouponValidations[$i][1]);
			_click(_span("APPLY"));
			//invalid
			if($i==0 ||$i==1 ||$i==2 || $i==3 ||$i==4)
				{
					_assertVisible(_div($CouponValidations[$i][2]));
					_assertEqual($CouponValidations[0][4],_style(_div($CouponValidations[$i][2]),"color"));
				}

			//valid coupon
			if($i==5 || $i==6)
				{
					_assert(_isVisible(_div("/redemption coupon/")));
					var $sucessMsg="Promo Code "+$CouponValidations[$i][2]+" has been added to your order and was applied.";
					//enter valid billing address
					BillingAddress($Valid_Data,0);
					//enter credit card details
					if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
						{
						  if($CreditCardPayment==$BMConfig[0][4])
		        		  {
		        		  	PaymentDetails($paypal,2);
		        		  }
					  	  else
		        		  {
		        		  	PaymentDetails($Valid_Data,4);
		        		  }
							//click on continue
							_click(_submit("dwfrm_billing_save"));	
						}
				   else if($CreditCard=="No" && $paypalNormal=="Yes")
						{
					   		_assert(_radio("PayPal").checked);
			        		//click on continue
			        		_click(_submit("dwfrm_billing_save"));	
			        		//paypal
			           		if(isMobile())
			                         {
			                            _setValue(_emailbox("login_email"), $paypal[0][0]);
			                         }
			                  else
			                         {
			                         	_setValue(_textbox("login_email"), $paypal[0][0]);
			                         }
			                  _setValue(_password("login_password"), $paypal[0][1]);
			                  _click(_submit("Log In"));
			                  _click(_submit("Continue"));
			                  _wait(10000,_isVisible(_submit("submit")));
			
						}
					//verify wehter valid Coupon got applied or not
					_assertVisible(_span("Applied"));
					_assertVisible(_div("promo  first "));
					_assertVisible(_span("Coupon Number:"));
					if(isMobile())
						{
							_assertVisible(_span("Coupon_Product",_near(_span("Coupon Number:"))));
						}
					else
						{
							_assertVisible(_span("Coupon_Product",_rightOf(_span("Coupon Number:"))));
						}
					_assertVisible(_span("value",_in(_div("discount clearfix  first "))));
					var $discountPrice=parseFloat(_extract(_getText(_span("value",_in(_div("discount clearfix  first ")))),"/[$](.*)/",true).toString());
					var $expDiscountPrice=$subtotalBeforeApplyingCoupon-$discountPrice;
					var $actDiscountPrice=parseFloat(_getText(_cell("item-total")).replace("$",""));
					//verifying the price
					_assertEqual($expDiscountPrice,$actDiscountPrice);
					//verify whether subtotal section got applied or not
					var $subTotal=_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true);
					_assertEqual($actDiscountPrice,$subTotal);		
					_click(_link("Billing"));	
				}			
			ClearCartItems();
		}
	
}catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


if($GiftCardPayment==$BMConfig[0][3] && $giftCertificate=="Yes")
{
//Navigation till shipping page
navigateToShippingPage($item[0][0],$item[0][1]);
//shipping details
shippingAddress($Valid_Data,0);
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//address verification over lay
addressVerificationOverlay();
var $t=_testcase("126683/126685/126684/126682/126693","Verify the field validations in 'Redeem Gift Certificate ' textbox, functionality by entering Valid Gift card number and pincode number,functionality whether user can remove the applied gift card, functionality of 'Check balance' link, functionality of Apply buttton in Gift card Section on billing page in Application as a Registered user.");
$t.start();
try
{
	//validations 
    for(var $i=0;$i<$GC_Validation.length;$i++)
           {
           //valid data, Apply and remove functionality
           if($i==4)
                  {
                  _setValue(_textbox("dwfrm_billing_giftCertCode"),$GC[0][0]);
                  _setValue(_textbox("dwfrm_billing_giftCertPin"),$GC[0][1]);                
                  _click(_submit("dwfrm_billing_redeemGiftCert"));
                  //valid data 
                  _assertVisible(_div("success giftcert-pi"));
                  _assertEqual("USD "+$GC[0][4]+" has been redeemed from Gift Certificate **"+$GC[0][3]+" Remove", _getText(_div("success giftcert-pi")));
                  //_assertVisible(_span("Remove"));
                  //enter valid billing address
                  BillingAddress($Valid_Data,0);
                  //enter credit card details
                  if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
                  {
                	  if($CreditCardPayment==$BMConfig[0][4])
            		  {
            		  	PaymentDetails($paypal,2);
            		  }
    			  	  else
            		  {
            		  	PaymentDetails($Valid_Data,4);
            		  }
                        //click on continue
                        _click(_submit("dwfrm_billing_save"));   
                  }
                  else if($CreditCard=="No" && $paypalNormal=="Yes")
                  {
                        _assert(_radio("PayPal").checked);
                  //click on continue
                  _click(_submit("dwfrm_billing_save"));   
                  //paypal
                  if(isMobile())
                      {
                            _setValue(_emailbox("login_email"), $paypal[0][0]);
                      }
               else
                      {
                      _setValue(_textbox("login_email"), $paypal[0][0]);
                      }
               _setValue(_password("login_password"), $paypal[0][1]);
               _click(_submit("Log In"));
               _click(_submit("Continue"));
               _wait(10000,_isVisible(_submit("submit")));

                  }
                //address verification over lay
                  addressVerificationOverlay();
                  var $ActGCAmount=parseFloat(_extract(_getText(_div("/Amount: /", _in(_div("mini-payment-instrument order-component-block  first ")))), "/Amount: [$](.*)/",true).toString());
                  _assertEqual($GC[0][2],$ActGCAmount);
                  if(isIE11())
            	  {
            	  var $OrderTotal=parseFloat(_extract(_getText(_row("order-total")),"/Order Total:[$](.*)/",true).toString());
            	  }
                  else
            	  {
            	  var $OrderTotal=parseFloat(_extract(_getText(_row("order-total")),"/Order Total: [$](.*)/",true).toString());
            	  }
                  var $ExpOrderBal=$OrderTotal-$ActGCAmount;
                  _log($ExpOrderBal);
                  $ExpOrderBal=round($ExpOrderBal,2);
                  _log($ExpOrderBal);
                  var $ActOrderBal=parseFloat(_extract(_getText(_div("/Amount: /", _in(_div("mini-payment-instrument order-component-block  last")))), "/Amount: [$](.*)/",true).toString());
                  _assertEqual($ExpOrderBal,$ActOrderBal);
                  
                  //redirecting to billing
                  _click(_link("Billing"));
                  //remove button
                  _assertVisible(_span("Remove"));
                  _click(_span("Remove"));
                  _assertNotVisible(_div("success giftcert-pi"));
                  _assertNotVisible(_span("Remove"));      
                  }
           else
                  {
                  _setValue(_textbox("dwfrm_billing_giftCertCode"),$GC_Validation[$i][1]);
                  _setValue(_textbox("dwfrm_billing_giftCertPin"),$GC_Validation[$i][2]);
                    if($i==5)
                        {
                               _click(_span("Check Balance"));
                               //Checking balance without apllying GC
                               _assertVisible(_div("Your current gift certificate balance is $"+$GC_Validation[$i][3]));
                        }
                  //Invalid
                  else if($i==0)
                        {
                               _click(_submit("dwfrm_billing_redeemGiftCert"));
                               _assertVisible(_div($GC_Validation[0][4]));
                               _assertEqual($GC_Validation[0][5],_style(_div($GC_Validation[0][4]),"color"));
                        }
                  else
                        {
                        _click(_submit("dwfrm_billing_redeemGiftCert"));
                        var $errorMsg="Gift Certificate \""+$GC_Validation[$i][1]+"\" could not be found.";
                        _assertVisible(_div($errorMsg));
                        _assertEqual($GC_Validation[0][5],_style(_div($errorMsg),"color"));
                        }
                  
                  }             
           }
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();
}
//removing item from cart
ClearCartItems();


var $t=_testcase("126677/126678","Verify the scenario if User selected and if User not selected'Use this address for Billing' checkbox on shipping page in Application as a Registered user.");
$t.start();
try
{
	//if User selected 'Use this address for Billing' checkbox
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping details
	shippingAddress($Valid_Data,0);
	//selecting use this billing address
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//address verification over lay
	addressVerificationOverlay();
	//Billing details
	//verify the address prepopulation in billing page
	_assertEqual($Valid_Data[0][1],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
	_assertEqual($Valid_Data[0][2],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
	_assertEqual($Valid_Data[0][3],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
	_assertEqual($Valid_Data[0][4],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address2")));
	_assertEqual($Valid_Data[1][5],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
	_assertEqual($Valid_Data[0][6],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
	_assertEqual($Valid_Data[0][7],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
	_assertEqual($Valid_Data[0][8],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_postal")));
	_assertEqual($Valid_Data[0][9],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_phone")));	
	//Checking email
	_assertEqual($UserEmail,_getText(_textbox("dwfrm_billing_billingAddress_email_emailAddress")));
	
	//if User not selected 'Use this address for Billing' checkbox
	_click(_link("View Cart"));
	_click(_submit("dwfrm_cart_checkoutCart"));
	
	if(!_isVisible(_link("user-account")))
	{
	_click(_submit("dwfrm_login_unregistered"));
	}
	
	//Unchecking check box
	_uncheck(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//address verification over lay
	addressVerificationOverlay();
	//assertion part Need to script after clarifacaion	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


//removing item from cart
ClearCartItems()

//deleting the addresses and card details from profile
_click(_link("user-account"));
_click(_link("My Account"));
_click(_link("Addresses"));
deleteAddress();
if($CreditCard=="Yes")
{
	DeleteCreditCard();
}


var $t=_testcase("126680/126695","Verify the functionality of 'Add to Address Book' check box and functionality of 'Save this card' checkbox on billing page in Application as a Registered user.");
$t.start();
try
{
	//if User selected 'Use this address for Billing' checkbox
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping details
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//address verification over lay
	addressVerificationOverlay();
	//Billing details
	BillingAddress($Valid_Data,0);	
	//Checking email
	_assertEqual($UserEmail,_getText(_textbox("dwfrm_billing_billingAddress_email_emailAddress")));
	//selecting check box
	_check(_checkbox("dwfrm_billing_billingAddress_addToAddressBook"));
	if($CreditCard=="Yes")
	{
		  if($CreditCardPayment==$BMConfig[0][4])
		  {
		  	PaymentDetails($paypal,2);
		  }
	  	  else
		  {
		  	PaymentDetails($Valid_Data,4);
		  }
		_check(_checkbox("dwfrm_billing_paymentMethods_creditCard_saveCard"));
		//click on continue
		_click(_submit("dwfrm_billing_save"));	
	}
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	{
		_assert(_radio("PayPal").checked);
		//click on continue
		_click(_submit("dwfrm_billing_save"));	
		//paypal
   		if(isMobile())
                 {
                       _setValue(_emailbox("login_email"), $paypal[0][0]);
                 }
          else
                 {
                 _setValue(_textbox("login_email"), $paypal[0][0]);
                 }
          _setValue(_password("login_password"), $paypal[0][1]);
          _click(_submit("Log In"));
          _click(_submit("Continue"));
          _wait(10000,_isVisible(_submit("submit")));

	}
	addressVerificationOverlay();
	
	//Click on place order button
	_click(_submit("button-fancy-large"));
	
	if(_isVisible(_image("CloseIcon.png")))
	{
	_click(_image("CloseIcon.png"));
	}
	//Checking order is placed or not
	_assertVisible(_heading1($Generic[0][2]));
	_assertVisible(_div("order-confirmation-details"));
	
	//verifying the address in account
	_click(_link("user-account"));
	_click(_link("My Account"));
	_click(_link("Addresses"));
	
	_assertEqual($Valid_Data[0][7],_getText(_div("mini-address-title")));
 	_assertEqual($Valid_Data[0][1]+" "+$Valid_Data[0][2], _getText(_div("mini-address-name")));
 	_assertEqual($Valid_Data[0][11], _getText(_div("mini-address-location")));
 	if($CreditCard=="Yes")
	{
 	//verifying the card details in account
 	_click(_link("Payment Settings"));
 	_assertContainsText($sheetName[$rowNum][7], _list("payment-list"));
	}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("126679/126722","Verify the Scenario if user select  'Choose an Address'/Verify the newly added card in profile get displayed in checkout Payment and Promotions page on billing page in Application as a Registered user.");
$t.start();
try
{
	//if User selected 'Use this address for Billing' checkbox
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping details
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//address verification over lay
	addressVerificationOverlay();
	//Billing details
	//selecting from drop down
	_setSelected(_select("dwfrm_billing_addressList"),1);
	//verify the address prepopulation in billing page
	_assertEqual($Valid_Data[0][1],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
	_assertEqual($Valid_Data[0][2],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
	_assertEqual($Valid_Data[0][3],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
	_assertEqual($Valid_Data[0][4],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address2")));
	_assertEqual($Valid_Data[1][5],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
	_assertEqual($Valid_Data[0][6],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
	_assertEqual($Valid_Data[0][7],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
	_assertEqual($Valid_Data[0][8],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_postal")));
	_assertEqual($Valid_Data[0][9],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_phone")));	
	_assertEqual($UserEmail,_getText(_textbox("dwfrm_billing_billingAddress_email_emailAddress")));
	if($CreditCard=="Yes")
	{
	//Payment section
	//selecting from drop down
	_setSelected(_select("dwfrm_billing_paymentMethods_creditCardList"),1);
	_assertEqual($sheetName[$rowNum][1],_getText(_textbox("dwfrm_billing_paymentMethods_creditCard_owner")));
	_assertEqual($sheetName[$rowNum][2],_getSelectedText(_select("dwfrm_billing_paymentMethods_creditCard_type")));
	if($CreditCardPayment!=$BMConfig[2][4])
		{
		_assertEqual($sheetName[$rowNum][8],_getText(_textbox("/dwfrm_billing_paymentMethods_creditCard_number/")));
		}	
	_assertEqual($sheetName[$rowNum][4],_getSelectedText(_select("dwfrm_billing_paymentMethods_creditCard_expiration_month")));
	_assertEqual($sheetName[$rowNum][5],_getSelectedText(_select("dwfrm_billing_paymentMethods_creditCard_expiration_year")));
	//checking cvv and entering cvv
	_assertEqual("", _getValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/")));
	_setValue(_textbox("/dwfrm_billing_paymentMethods_creditCard_cvn/"), $sheetName[$rowNum][6]);	
	_click(_submit("dwfrm_billing_save"));
	}
	else if($CreditCard=="No" && $paypalNormal=="Yes")
    {
           _click(_radio("PayPal"));
           _click(_submit("dwfrm_billing_save"));
           if(isMobile())
           	{
                 _setValue(_emailbox("login_email"), $paypal[0][0]);
           	}
           else
           	{
        	   _setValue(_textbox("login_email"), $paypal[0][0]);
           	}
    _setValue(_password("login_password"), $paypal[0][1]);
    _click(_submit("Log In"));
    _click(_submit("Continue"));
    _wait(10000,_isVisible(_submit("submit")));
    _click(_submit("dwfrm_billing_save"));
    }	
	addressVerificationOverlay();
	//Placing order
	_click(_submit("button-fancy-large"));
	
	if(_isVisible(_image("CloseIcon.png")))
	{
	_click(_image("CloseIcon.png"));
	}
	//Checking order is placed or not
	_assertVisible(_heading1($Generic[0][2]));
	_assertVisible(_div("order-confirmation-details"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


//removing item from cart
ClearCartItems()
var $t=_testcase("126723","Verify the card in checkout Payment and Promotions page once after deleting the card in profile in Application as a Registered user");
$t.start();
try
{

	
	 DeleteCreditCard();
//navigate to billing page nad verify the presence of address
//Navigation till shipping page
navigateToShippingPage($item[0][0],$item[0][1]);
//shipping details
shippingAddress($Valid_Data,0);
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//address verification over lay
addressVerificationOverlay();
//verify the presence of deleted credit card
_assertNotVisible(_select("dwfrm_billing_paymentMethods_creditCardList"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


ClearCartItems();

//Deleting addresses from the account 
deleteAddress();

var $t=_testcase("124452","Verify the UI for  different Shipping address &amp; Order Summary display in billing accordion by selecting two different Shipping address for same item with different quantity");
$t.start();
try
{
//Navigation till shipping page
navigateToShippingPage($item[0][0],"2");
var $shippingMethod=_extract(_getText(_row("/order-shipping  first/")),"/Shipping(.*)[$]/",true).toString().trim();
//click on yes button
_click(_submit("dwfrm_singleshipping_shipToMultiple"));
//adding addresses
for(var $i=0;$i<$Address.length;$i++)
	{
	_click(_span("Add/Edit Address["+$i+"]"));
	addAddressMultiShip($Address,$i);
	}

//Select Addresses from each drop down
var $j=1;
for(var $k=0;$k<$Address.length;$k++)
{
	_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$k+"_addressList"),$j);
	$j++;
}

//address save 
_click(_submit("dwfrm_multishipping_addressSelection_save"));
_wait(5000,_isVisible(_submit("dwfrm_multishipping_shippingOptions_save")));
//shipping options
_click(_submit("dwfrm_multishipping_shippingOptions_save"));
$tax=parseFloat(_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString());
$shippingTax=parseFloat(_extract(_getSelectedText(_select("/dwfrm_multishipping_shippingOptions/")),"/[$](.*)[(]/",true));
$orderTotal=parseFloat(_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());
//verify the ordersummary
verifyOrderSummary("Multishipment",$taxService,$item[0][2]);
//verify the shipping addresses
var $numOfShipments=_count("_div","/mini-shipment order-component-block/",_in(_div("secondary")));
_log($numOfShipments);
for(var $i=0;$i<$numOfShipments;$i++)
	{
		var $billingAddress=$Address[$i][2]+" "+$Address[$i][3]+" "+$Address[$i][4]+" "+$Address[$i][5]+" "+$Address[$i][8]+", "+$Address[$i][11]+" "+$Address[$i][9]+" "+$Address[$i][6]+" "+$Address[$i][10];
		var $ExpShippingAddress=$billingAddress+" "+"Method: "+$shippingMethod;
		var $ActShippingAddress=_getText(_div("details",_in(_div("/mini-shipment order-component-block  first/"))));
		if($ActShippingAddress.indexOf($Address[$i][2])>=0)
		{
		_assertEqual($ExpShippingAddress,$ActShippingAddress);
		}
		else 
		{
		var $ActShippingAddress=_getText(_div("details",_in(_div("/mini-shipment order-component-block  last/"))));
		_assertEqual($ExpShippingAddress,$ActShippingAddress);
		}
	}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


}
cleanup();

function navigateToBillingPageWithData()
{
	_click(_link("mini-cart-link-checkout"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//address verification over lay
	addressVerificationOverlay();
	_assertVisible(_fieldset("/"+$Generic[6][0]+"/"));	
}