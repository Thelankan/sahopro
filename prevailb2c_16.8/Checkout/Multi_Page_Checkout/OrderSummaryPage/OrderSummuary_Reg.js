_include("../../../util.GenericLibrary/GlobalFunctions.js");
_include("../../../util.GenericLibrary/BM_Configuration.js");
_resource("OrderSummuary.xls");

var $OrderSummuary=_readExcelFile("OrderSummuary.xls","OrderSummuary");
var $Valid_Data=_readExcelFile("OrderSummuary.xls","Valid_Data");
var $Address=_readExcelFile("OrderSummuary.xls","Address");
var $paypal=_readExcelFile("OrderSummuary.xls","Paypal");


var $sheetName;
var $rowNum;

//verifies whether five page or single page checkout
if($CheckoutType==$BMConfig[1][1])
{
	
SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();
_click(_link("Login"));
login();

var $billingAddress=$Valid_Data[0][1]+" "+$Valid_Data[0][2]+" "+$Valid_Data[0][3]+" "+$Valid_Data[0][4]+" "+$Valid_Data[0][7]+" "+$Valid_Data[1][6]+" "+$Valid_Data[0][8]+" "+$Valid_Data[0][5]+" "+$Valid_Data[0][9];
//var $billingAddress=$Valid_Data[0][1]+" "+$Valid_Data[0][2]+" "+$Valid_Data[0][3]+" "+$Valid_Data[0][4]+" "+$Valid_Data[0][7]+" "+$Valid_Data[1][6]+" "+$Valid_Data[0][8]+" "+$Valid_Data[0][5];
var $shippingAddress=$billingAddress+" "+"Method: "+$OrderSummuary[0][1];

var $t=_testcase("126608","Verify the UI of  'Summary' Page in Application as a Registered user.");
$t.start();
try
{
	//Navigation till shipping page
	navigateToShippingPage($OrderSummuary[0][7],$OrderSummuary[0][8]);
	shippingAddress($Valid_Data,0);
	if(isMobile() && !mobile.iPad())
		{
			$shippingTax=_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString();	
			$subTotal=_extract(_getText(_span("mini-cart-price",_in(_div("mini-cart-pricing")))),"/[$](.*)/",true).toString();
		}
	else
		{
			$shippingTax=_extract(_getText(_row("order-shipping  first ")),"/[$](.*)/",true).toString();
			$subTotal=_extract(_getText(_span("mini-cart-price",_in(_div("secondary")))),"/[$](.*)/",true).toString();
		}
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));	
	//Address Verification overlay
	addressVerificationOverlay();	
	BillingAddress($Valid_Data,0); 
	$tax=_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString();
	$Ordertotal=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
	//entering Credit card Information
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
	    {
			if($CreditCardPayment==$BMConfig[0][4])
			  {
			  	PaymentDetails($paypal,2);
			  }
		  	  else
			  {
			  	PaymentDetails($Valid_Data,4);
			  }
	           _click(_submit("dwfrm_billing_save"));
	    }
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	    {
	          _click(_radio("PayPal"));
	           _click(_submit("dwfrm_billing_save"));
	           if(isMobile())
	           {
	                 _setValue(_emailbox("login_email"), $paypal[0][0]);
	           }
	           else
	           {
	           _setValue(_textbox("login_email"), $paypal[0][0]);
	           }
	    _setValue(_password("login_password"), $paypal[0][1]);
	    _click(_submit("Log In"));
	    _click(_submit("Continue"));
	    _wait(10000,_isVisible(_submit("submit")));
	    }
	addressVerificationOverlay();
	if(!isMobile() || mobile.iPad())
		{
	//varify the navigation & UI of Order Summuary
	_assertVisible(_tableHeader($OrderSummuary[0][0]));
	_assertVisible(_tableHeader($OrderSummuary[1][0]));
	_assertVisible(_tableHeader($OrderSummuary[2][0]));
		}
	//image
	_assertVisible(_cell("item-image"));
	//name
	_assertVisible(_link("/(.*)/",_in(_div("name"))));
	//qty
	_assertVisible(_cell("item-quantity"));
	//item num
	_assertVisible(_span($OrderSummuary[4][0]));
	_assertVisible(_span("value",_near(_span($OrderSummuary[4][0]))));
	//total price
	_assertVisible(_cell("item-total"));
	//edit link
	_assertVisible(_link("/Edit Cart/"));
	//summuary section
	_assertVisible(_cell("/Subtotal/"));
	_assertVisible(_cell("/Shipping/"));
	_assertVisible(_cell("/Sales Tax/"));
	_assertVisible(_cell($OrderSummuary[5][0]));
	_assertVisible(_row("order-subtotal"));
	_assertVisible(_row("order-shipping"));
	_assertVisible(_row("order-sales-tax"));
	_assertVisible(_row("order-total"));
	_assertVisible(_submit("Place Order"));
	//Order Summuary 
	_assertVisible(_heading3($OrderSummuary[6][0]));
	//shipping address
	_assertVisible(_heading3($OrderSummuary[7][0]));
	_assertVisible(_div("details",_near(_heading3($OrderSummuary[7][0]))));
	//billing address
	_assertVisible(_heading3($OrderSummuary[8][0]));
	_assertVisible(_div("details",_near(_heading3($OrderSummuary[8][0]))));
	//payment details
	if(!_isSafari())
		{
		_assertVisible(_heading3($OrderSummuary[9][1]));
		_assertVisible(_div("details",_near(_heading3($OrderSummuary[9][1]))));	
		}
	else
		{
		_assertVisible(_heading3($OrderSummuary[9][0]));
		_assertVisible(_div("details",_near(_heading3($OrderSummuary[9][0]))));
		}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("126627","Verify the functionality of progress indicator on Order Review page in Application as a Registered user");
$t.start();
try
{
//verifying the progress indicators
_assertVisible(_link("Shipping"));
_assertVisible(_link("Billing"));
_assertVisible(_link("Place Order"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("149002","Verify the details to be displayed for the cart line items on Summary Page in Application as a Registered user.");
$t.start();
try
{
	//verifying the cart line items
	_assertVisible(_link("/(.*)/",_in(_div("product-list-item"))));
	_assertEqual($pName, _getText(_link("/(.*)/",_in(_div("product-list-item")))));
	//_assertVisible(_image("/(.*)/",_in(_row("cart-row  first "))));
	_assertVisible(_row("order-subtotal",_in(_div("checkout-order-totals"))));
	_assertEqual($totalPrice,parseFloat(_extract(_getText(_row("order-subtotal",_in(_div("checkout-order-totals")))),"/[$](.*)/",true)));
	if(!isMobile())
		{
			_assertVisible(_tableHeader("Qty",_in(_table("cart-table"))));
		}
	_assertVisible(_cell("item-quantity"));
	_assertEqual($quantity,_getText(_cell("item-quantity")));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("126621","Verify the 'ORDER SUMMARY'Section in the right nav on Summary Page in Application as a Registered user.");
$t.start();
try
{
	//Verify the 'ORDER SUMMARY'section
	_assertVisible(_cell("/Subtotal/",_in(_div("checkout-order-totals"))));
	_assertVisible(_cell("/Shipping/",_in(_div("checkout-order-totals"))));
	_assertVisible(_cell("/Sales Tax/",_in(_div("checkout-order-totals"))));
	_assertVisible(_cell($OrderSummuary[5][1],_in(_div("checkout-order-totals"))));
	_assertEqual($subTotal,_extract(_getText(_row("order-subtotal",_in(_div("checkout-order-totals")))),"/[$](.*)/",true).toString());
	_assertEqual($shippingTax,_extract(_getText(_row("/order-shipping/",_in(_div("checkout-order-totals")))),"/[$](.*)/",true).toString());
	_assertEqual($tax,_extract(_getText(_row("order-sales-tax",_in(_div("checkout-order-totals")))),"/[$](.*)/",true).toString());
	_assertEqual($Ordertotal,_extract(_getText(_row("order-total",_in(_div("checkout-order-totals")))),"/[$](.*)/",true).toString());
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("126629","Verify the details to be displayed in the Shipping address Section in right nav on Summary Page in Application as a Registered user.");
$t.start();
try
{
	//verifying the shipping address
	_assertVisible(_heading3($OrderSummuary[7][0]));
	_assertVisible(_div("details",_near(_heading3($OrderSummuary[7][0]))));
	var $ShippingAddress=_getText(_div("details",_near(_heading3($OrderSummuary[7][0])))).replace(",","");
	_assertEqual($shippingAddress,$ShippingAddress);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("126633","Verify the details to be displayed in the Billing address Section in right nav on Summary Page in Application as a Registered user.");
$t.start();
try
{
	//verifying the billing address
	_assertVisible(_div("details",_near(_heading3($OrderSummuary[8][0]))));
	var $BillingAddress=_getText(_div("details",_near(_heading3($OrderSummuary[8][0])))).replace(",","");
	_assertEqual($billingAddress,$BillingAddress);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("126635","Verify the details to be displayed in the Payment method Section in right nav on Summary Page in Application as a Registered user.");
$t.start();
try
{
	//verifying the payment details section
	if(!_isSafari())
		{
		_assertVisible(_heading3($OrderSummuary[9][1]));
		_assertVisible(_div("details",_near(_heading3($OrderSummuary[9][1]))));
		var $payment_Details=_getText(_div("details",_near(_heading3($OrderSummuary[9][1]))));
		}
	else
		{
		_assertVisible(_heading3($OrderSummuary[9][0]));
		_assertVisible(_div("details",_near(_heading3($OrderSummuary[9][0]))));
		var $payment_Details=_getText(_div("details",_near(_heading3("EDITPAYMENT METHOD"))));
		}
	if($CreditCard=="Yes")
	{
		if($CreditCardPayment==$BMConfig[0][4])
		  {
			  $sheetName=$paypal;
			  $rowNum=2;
		  }
		  else
		  {
			  $sheetName=$Valid_Data;
			  $rowNum=4;
		  }
		var $ExpCreditCard_Details=$OrderSummuary[1][1]+" "+$sheetName[$rowNum][1]+" "+$sheetName[$rowNum][2]+" "+$sheetName[parseFloat($rowNum)+1][3]+" "+$OrderSummuary[2][1]+". "+$sheetName[parseFloat($rowNum)+1][4]+"."+$sheetName[$rowNum][5]+" "+$OrderSummuary[3][1]+" $"+$Ordertotal;
		_assertEqual($ExpCreditCard_Details,$payment_Details);
	}
	else
	{
	var $ExpPayment_Details="Pay Pal "+$OrderSummuary[3][1]+" $"+$Ordertotal;
	_assertEqual($ExpPayment_Details,$payment_Details);
	}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("126638","Verify the Edit link functionality for shipping address Section in right nav on Summary Page in Application as a Registered user.");
$t.start();
try
{
	//clicking on edit link present in shipping address section
	_click(_link("EDIT",_in(_heading3($OrderSummuary[7][0]))));
	//verify the navigation
	_assertVisible(_fieldset("/"+$OrderSummuary[0][2]+"/"));
	//_assertVisible(_div("shippingForm"));
	_assertVisible(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//navigate to summuary page
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//Address Verification overlay
	addressVerificationOverlay();
	//verifying navigation
	_assertEqual("Step 3: Place Order", _getText(_div("step-3 active")));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("126640","Verify the Edit link functionality for the billing address Section in right nav on Summary Page in Application as a Registered user.");
$t.start();
try
{
	//click on edit link present in billing section
	_click(_link("EDIT",_in(_heading3("EDIT BILLING ADDRESS"))));
	//verify the navigation
	_assertVisible(_fieldset("/"+$OrderSummuary[0][3]+"/"));
	_assertVisible(_submit("dwfrm_billing_save"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
       //navigate back to billing page
       _click(_link("Billing"));
}
$t.end();

var $t=_testcase("126643","Verify the Edit link functionality for Payment Method Section in right nav on Summary Page in Application as a Registered user.");
$t.start();
try
{
	//navigate to summuary page
	BillingAddress($Valid_Data,0); 
	//entering Credit card Information
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
	    {
			if($CreditCardPayment==$BMConfig[0][4])
			  {
			  	PaymentDetails($paypal,2);
			  }
		  	  else
			  {
			  	PaymentDetails($Valid_Data,4);
			  }
	           _click(_submit("dwfrm_billing_save"));
	    }
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	    {
	          _click(_radio("PayPal"));
	           _click(_submit("dwfrm_billing_save"));
	           if(isMobile())
	           {
	                 _setValue(_emailbox("login_email"), $paypal[0][0]);
	           }
	           else
	           {
	           _setValue(_textbox("login_email"), $paypal[0][0]);
	           }
	    _setValue(_password("login_password"), $paypal[0][1]);
	    _click(_submit("Log In"));
	    _click(_submit("Continue"));
	    _wait(10000,_isVisible(_submit("submit")));
	    }
	addressVerificationOverlay();
	//click on edit link present in payment deatils section
	if(!_isSafari())
		{
		_click(_link("Edit",_in(_heading3($OrderSummuary[9][1]))));
		}
	else
		{
		_click(_link("EDIT",_in(_heading3("EDITPAYMENT METHOD"))));
		}
	//verify the navigation to billing page
	_assertVisible(_fieldset("/"+$OrderSummuary[0][3]+"/"));
	_assertVisible(_submit("dwfrm_billing_save"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("126646","  Verify the Edit link displayed in Order Summary Section in the SUMMARY Section for registered user");
$t.start();
try
{
	//navigate to summuary page
	_click(_link("mini-cart-link-checkout"));
	if(_isVisible(_link("nothank")))
		{
	_click(_link("nothank"));
		}
	//navigate to summuary page
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//Address Verification overlay
	addressVerificationOverlay();
	BillingAddress($Valid_Data,0); 
	//entering Credit card Information
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
	    {
			if($CreditCardPayment==$BMConfig[0][4])
			  {
			  	PaymentDetails($paypal,2);
			  }
		  	  else
			  {
			  	PaymentDetails($Valid_Data,4);
			  }
	           _click(_submit("dwfrm_billing_save"));
	    }
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	    {
	          _click(_radio("PayPal"));
	           _click(_submit("dwfrm_billing_save"));
	           if(isMobile())
	           {
	                 _setValue(_emailbox("login_email"), $paypal[0][0]);
	           }
	           else
	           {
	           _setValue(_textbox("login_email"), $paypal[0][0]);
	           }
	    _setValue(_password("login_password"), $paypal[0][1]);
	    _click(_submit("Log In"));
	    _click(_submit("Continue"));
	    _wait(10000,_isVisible(_submit("submit")));
	    }

	//click on edit link present in order Summuary section
	_click(_link("EDIT",_in(_heading3("ORDER SUMMARY EDIT"))));
	//verify the navigation to cart page
	 _assertVisible(_table("cart-table"));
	 _assertVisible(_div("cart-actions cart-actions-top"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("126611","Verify the functionality of 'Edit cart' link next to Place Order button on Summary Page in Application as a Registered user.");
$t.start();
try
{
	//navigate to summuary page
	_click(_link("mini-cart-link-checkout"));
	if(_isVisible(_link("nothank")))
		{
	_click(_link("nothank"));
		}
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//Address Verification overlay
	addressVerificationOverlay();
	BillingAddress($Valid_Data,0); 
	//entering Credit card Information
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
	    {
			if($CreditCardPayment==$BMConfig[0][4])
			  {
			  	PaymentDetails($paypal,2);
			  }
		  	  else
			  {
			  	PaymentDetails($Valid_Data,4);
			  }
	           _click(_submit("dwfrm_billing_save"));
	    }
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	    {
	          _click(_radio("PayPal"));
	           _click(_submit("dwfrm_billing_save"));
	           if(isMobile())
	           {
	                 _setValue(_emailbox("login_email"), $paypal[0][0]);
	           }
	           else
	           {
	           _setValue(_textbox("login_email"), $paypal[0][0]);
	           }
	    _setValue(_password("login_password"), $paypal[0][1]);
	    _click(_submit("Log In"));
	    _click(_submit("Continue"));
	    _wait(10000,_isVisible(_submit("submit")));
	    }
	addressVerificationOverlay();
	//click on edit cart link
	_click(_link("/Edit Cart/"));
	//verify the navigation to Cart page
	 _assertVisible(_table("cart-table"));
	 _assertVisible(_div("cart-actions cart-actions-top"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("126625","Verify the functionality of 'Place Order' button on Summary Page in Application as a Registered user.");
$t.start();
try
{
	navigateToShippingPage($OrderSummuary[0][7],$OrderSummuary[0][8]);
	//navigate to summuary page
	_click(_link("mini-cart-link-checkout"));
	if(_isVisible(_link("nothank")))
	{
		_click(_link("nothank"));
	}
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//Address Verification overlay
	addressVerificationOverlay();
	BillingAddress($Valid_Data,0); 
	//entering Credit card Information
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
	    {
			if($CreditCardPayment==$BMConfig[0][4])
			  {
			  	PaymentDetails($paypal,2);
			  }
		  	  else
			  {
			  	PaymentDetails($Valid_Data,4);
			  }
	           _click(_submit("dwfrm_billing_save"));
	    }
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	    {
	          _click(_radio("PayPal"));
	           _click(_submit("dwfrm_billing_save"));
	           if(isMobile())
	           {
	                 _setValue(_emailbox("login_email"), $paypal[0][0]);
	           }
	           else
	           {
	           _setValue(_textbox("login_email"), $paypal[0][0]);
	           }
	    _setValue(_password("login_password"), $paypal[0][1]);
	    _click(_submit("Log In"));
	    _click(_submit("Continue"));
	    _wait(10000,_isVisible(_submit("submit")));
	    }
	addressVerificationOverlay();
	//Click on Place Order
	_click(_submit("Place Order"));
	if(_isVisible(_image("CloseIcon.png")))
	{
	_click(_image("CloseIcon.png"));
	}
	//verify the navigation to thank you page
	_assertVisible(_heading1($OrderSummuary[0][5]));
	_assertVisible(_heading1("order-number"));
	_assertVisible(_div("orderdetails"));
	_assertVisible(_div("order-shipment-table"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


//Deleting addresses from the account 
deleteAddress();

var $t=_testcase("126631","Verify the details to be displayed in the Shipping address Section in right nav when registered user selects the Multiple shipping address on Summary Page in Application");
$t.start();
try
{
	navigateToShippingPage($OrderSummuary[0][7],$OrderSummuary[0][8]);
	navigateToShippingPage($OrderSummuary[1][7],$OrderSummuary[0][8]);
	//ship to multiplae addreses
	_click(_submit("dwfrm_singleshipping_shipToMultiple"));
	var $addrCount=0;
	//adding addresses
	for(var $i=0;$i<$Address.length;$i++)
	{
		_click(_span("Add/Edit Address["+$i+"]"));
		addAddressMultiShip($Address,$i);
		$addrCount++;
	}
	
	//Select Addresses from each drop down
	var $j=1;
	for(var $k=0;$k<$Address.length;$k++)
	{
		_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$k+"_addressList"),$j);
		$j++;
	}
	//selecting addresses
	var $itemCount=_count("_select", "/dwfrm_multishipping_addressSelection_quantityLineItems/",_in(_table("item-list")));
	var $c1=1;
	var $j=1;
	for(var $i=0;$i<$itemCount;$i++)
	{   
	   if($c1>$addrCount)
	    {
	      $c1=1;
	    }
	     $c1++; 
		 $j=$j+2;
	}
	//click on continue
	_click(_submit("dwfrm_multishipping_addressSelection_save"));
	//Navigating to billing
	_click(_submit("dwfrm_multishipping_shippingOptions_save"));
	BillingAddress($Valid_Data,0); 
	//entering Credit card Information
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
	    {
			if($CreditCardPayment==$BMConfig[0][4])
			  {
			  	PaymentDetails($paypal,2);
			  }
		  	  else
			  {
			  	PaymentDetails($Valid_Data,4);
			  }
	           _click(_submit("dwfrm_billing_save"));
	    }
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	    {
	          _click(_radio("PayPal"));
	           _click(_submit("dwfrm_billing_save"));
	           if(isMobile())
	           {
	                 _setValue(_emailbox("login_email"), $paypal[0][0]);
	           }
	           else
	           {
	           _setValue(_textbox("login_email"), $paypal[0][0]);
	           }
	    _setValue(_password("login_password"), $paypal[0][1]);
	    _click(_submit("Log In"));
	    _click(_submit("Continue"));
	    _wait(10000,_isVisible(_submit("submit")));
	    }

	var $shipments=_collect("_div","/mini-shipment order-component-block/",true);
	for(var $i=0;$i<$shipments.length;$i++)
		{
		var $adr=$i+1;
		_assertVisible(_div("Shipment No. "+$adr, _in($shipments[$i])));
		var $billingAddress=$Address[$i][2]+" "+$Address[$i][3]+" "+$Address[$i][4]+" "+$Address[$i][5]+" "+$Address[$i][8]+" "+$Address[$i][7]+" "+$Address[$i][9]+" "+$Address[$i][6]+" "+$Address[$i][10];
		var $ExpShippingAddress=$billingAddress+" "+"Method: "+$OrderSummuary[0][1];
		var $ActShippingAddress=_getText(_div("details",_in($shipments[0]))).replace(",","");
	       if($ActShippingAddress.indexOf($Address[$i][2])>=0)
	        {
	        _assertEqual($ExpShippingAddress,$ActShippingAddress);
	        }
	        else 
	        {
	        var $ActShippingAddress=_getText(_div("details",_in($shipments[1]))).replace(",","");
	        _assertEqual($ExpShippingAddress,$ActShippingAddress);
	        }
		}
	addressVerificationOverlay();
	//Click on Place Order
	_click(_submit("Place Order"));
	if(_isVisible(_image("CloseIcon.png")))
	{
	_click(_image("CloseIcon.png"));
	}
	//verify the navigation to thank you page
	_assertVisible(_heading1($OrderSummuary[0][5]));
	_assertVisible(_heading1("order-number"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


cleanup();
}