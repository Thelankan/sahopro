_include("../../../util.GenericLibrary/GlobalFunctions.js");
_include("../../../util.GenericLibrary/BM_Configuration.js");
_resource("SPC_Multishipping.xls");

var $SPC_Multishipping=_readExcelFile("SPC_Multishipping.xls","SPC_Multishipping");
var $addr_data=_readExcelFile("SPC_Multishipping.xls","Address");
var $addr_data1=_readExcelFile("SPC_Multishipping.xls","Address1");
var $addr_data2=_readExcelFile("SPC_Multishipping.xls","Address2");
var $Validation=_readExcelFile("SPC_Multishipping.xls","Validations");
//verifies whether five page or single page checkout
if($CheckoutType==$BMConfig[1][1])
{
SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();


var $t = _testcase("126648","Verify the UI of Multi Shipping address page - Guest user");
$t.start();
try
{
//navigate to shipping page
navigateToCart($SPC_Multishipping[0][1],$SPC_Multishipping[0][8]); 
//click on go strait to checkout link
_click(_link("mini-cart-link-checkout"));
_click(_submit("dwfrm_login_unregistered")); 
//click on yes button 'Do you want multishipping' 
_click(_submit("dwfrm_singleshipping_shipToMultiple"));	
//verify the UI
_assertVisible(_div("ship-to-single"));	
var $count=_count("_table","/item-list/",_in(_div("checkoutmultishipping")));
for(var $i=0;$i<$count;$i++)
	{
	_assertVisible(_link("/(.*)/",_in(_div("name["+$i+"]"))));
	_assertVisible(_div("/Price/",_in(_div("product-list-item["+$i+"]"))));
	_assertVisible(_div("/Color/",_in(_div("product-list-item["+$i+"]"))));
	_assertVisible(_div("/Size/",_in(_div("product-list-item["+$i+"]"))));
	_assertVisible(_div("/Width/",_in(_div("product-list-item["+$i+"]"))));
	_assertVisible(_div("/Item No/",_in(_div("product-list-item["+$i+"]"))));
	_assertVisible(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"));
	_assertVisible(_span("Add/Edit Address["+$i+"]"));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126609/126623","Verify the display of number of shipments and 'Continue button functionality' below the order total Section on Multi Shipping page in Application as a guest user");
$t.start();
try
{
	var $addrCount=0;
	//adding addresses
	for(var $i=0;$i<$addr_data.length;$i++)
	{
		_click(_span("Add/Edit Address["+$i+"]"));
		addAddressMultiShip($addr_data,$i);
		$addrCount++;
	}
	//select addresses from drop down 
	var $dropDowns=_count("_select","/dwfrm_multishipping_addressSelection_quantityLineItems/",_in(_table("item-list")));
	var $c1=1;
	var $j=1;
	for(var $i=0;$i<$dropDowns;$i++)
		{
		   if($c1>$addrCount)
		    {
		      $c1=1;
		    }
		_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"),$j);
		$j++;
		}
	_click(_submit("dwfrm_multishipping_addressSelection_save"));
_assertVisible(_cell($SPC_Multishipping[3][9]));
//verify the navigation to Select shipping method
_assertVisible(_cell("/Select Shipping Method/")); 
//verify the shipments
var $numOfShipments=_count("_table","/item-list/",_in(_div("checkoutmultishipping")));
_assertEqual($SPC_Multishipping[2][4],$numOfShipments);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("128388/126607", "Verify the functionality of Is this a Gift Section on Multi Shipping page/when user enters some text in Is this a Gift message textbox in Application as a guest user");
$t.start();
try
{

var $count=_count("_table","/item-list/",_in(_div("checkoutmultishipping")));
if($count>0)
	{
	for(var $i=0;$i<$count;$i++)
		{
		//verify the 'is this a gift' section 
		_assertVisible(_div($SPC_Multishipping[0][3]+"["+$i+"]"));
		
		//by default no should select
		_assertVisible(_radio("false["+$i+"]"));
		_assert(_radio("false["+$i+"]").checked);
		//selecting yes button
		_click(_radio("true["+$i+"]"));
		//verifying the message text field
		_assertVisible(_label($SPC_Multishipping[1][3]+"["+$i+"]"));
		_assertVisible(_textarea("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMessage"));
		//selecting no radio button
		_click(_radio("false["+$i+"]"));
		//verify the display of message box
		_assertEqual(false,_isVisible(_div("form-row gift-message-text")));
		//message field validation
		_click(_radio("true["+$i+"]"));
		//verifying the message text field
		_assertVisible(_label($SPC_Multishipping[1][3]));
		_assertVisible(_textarea("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMessage"));
		//enter more than 150 characters in textbox and verify the max length
		_setValue(_textarea("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMessage"),$SPC_Multishipping[2][3]);
		_assertEqual($SPC_Multishipping[3][3],_getText(_textarea("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_giftMessage")).length);
		_assertVisible(_div($SPC_Multishipping[4][3]));
		_assertVisible(_div("char-count"));
		//selecting no radio button
		_click(_radio("false["+$i+"]"));
		}	
	}
else
	{
	_assert(false);
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126649","Verify the UI of Multi Shipping method page - Gust user");
$t.start();
try
{
var $numOfShipments=_count("_table","/item-list/",_in(_div("checkoutmultishipping")));
var $j=1;
for(var $i=0;$i<$numOfShipments;$i++)
	{
	//verify the UI
	_assertVisible(_tableHeader("Shipment No. "+$j));
	_assertVisible(_cell($SPC_Multishipping[3][9]+"["+$i+"]"));
	_assertVisible(_row("cart-row["+$i+"]"));
	_assertVisible(_select("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_shippingMethodID"));
	//product details
	_assertVisible(_link("/(.*)/",_in(_div("name["+$i+"]"))));
	_assertVisible(_div("/Color/",_in(_div("product-list-item["+$i+"]"))));
	_assertVisible(_div("/Size/",_in(_div("product-list-item["+$i+"]"))));
	_assertVisible(_div("/Width/",_in(_div("product-list-item["+$i+"]"))));
	_assertVisible(_div("/Item No/",_in(_div("product-list-item["+$i+"]"))));
	//quantity
	_assertVisible(_cell("item-quantity"));
	//item shipping address
	_assertVisible(_cell("item-shipping-address"));
	//is this gift
	_assertVisible(_div($SPC_Multishipping[4][9]+"["+$i+"]"));
	//continue button
	_assertVisible(_submit("dwfrm_multishipping_shippingOptions_save"));
	$j++;
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126619","Verify the functionality of 'Continue' button in the Shipping methods page - guest user");
$t.start();
try
{
//click on continue button
_click(_submit("dwfrm_multishipping_shippingOptions_save"));
//verify the navigation to billing page
_assertVisible(_fieldset("/"+$SPC_Multishipping[1][9]+"/"));
_assertVisible(_fieldset("/"+$SPC_Multishipping[5][9]+"/"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


cleanup();
var $t = _testcase("149051/149050","Verify the navigation on click of 'Add/Edit Address'/and UI of 'Add/Edit Address' link on Multi Shipping page in Application as a guest  user");
$t.start();
try
{  
	//navigate to shipping page
	navigateToCart($SPC_Multishipping[0][1],$SPC_Multishipping[0][8]); 
	//click on go strait to checkout link
	_click(_link("mini-cart-link-checkout"));
	_click(_submit("dwfrm_login_unregistered"));
	//click on yes button 'Do you want multishipping' 
	_click(_submit("dwfrm_singleshipping_shipToMultiple"));		
	var $count=_count("_row","/cart-row/");
	for(var $i=0;$i<$count;$i++)
		{
		_click(_span("Add/Edit Address["+$i+"]"));
		//verify the navigation
		_assertVisible(_div("dialog-container"));
		//verify the UI
		_assert(_isVisible(_heading1("/Add or Edit Addresses/")));
		_assert(_isVisible(_button("Close")));
		_assertVisible(_label("Select an Address:"));
		_assertVisible(_select("dwfrm_multishipping_editAddress_addressList"));
		//first name	
		_assert(_isVisible(_label("/First Name/")));
		_assertVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_firstName"));
		//last name
		_assert(_isVisible(_label("/Last Name/")));
		_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_lastName")));
		//address1
		_assertVisible(_label("/Address 1/"));
		_assert(_isVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_address1")));
		//address2
		_assertVisible(_label("Address 2"));
		_assertVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_address2"));
		//Country
		_assert(_isVisible(_label("/Country/")));
		_assertVisible(_select("dwfrm_multishipping_editAddress_addressFields_country"));
		//State
		_assertVisible(_label("/State/"));
		_assertVisible(_select("dwfrm_multishipping_editAddress_addressFields_states_state"));
		//City
		_assertVisible(_label("/City/"));
		_assertVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_city"));
		//Zip Code
		_assertVisible(_label("/Zip Code/"));
		_assertVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_postal"));  
		//Phone
		_assertVisible(_label("/Phone/"));
		_assertVisible(_textbox("dwfrm_multishipping_editAddress_addressFields_phone"));
		 _assertVisible(_div("/form-caption/",_near(_textbox("dwfrm_multishipping_editAddress_addressFields_phone",_in(_div("dialog-container"))))));
	if(!isMobile())
	{
	_assert(_isVisible(_link("tooltip")));
	}
		_assertVisible(_submit("Save"));
		_assertVisible(_button("Cancel"));
		_click(_button("Cancel"));
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126652","Verify the UI of Tool tip displayed in Add/edit address overlay - Guest user");
$t.start();
try
{
	//navigate to shipping page
	navigateToCart($SPC_Multishipping[0][1],$SPC_Multishipping[0][8]); 
	//click on go strait to checkout link
	_click(_link("mini-cart-link-checkout"));
	_click(_submit("dwfrm_login_unregistered"));  
	//click on yes button 'Do you want multishipping' 
	_click(_submit("dwfrm_singleshipping_shipToMultiple"));		
var $count=_count("_row","/cart-row/");
for(var $i=0;$i<$count;$i++)
	{
	_click(_span("Add/Edit Address["+$i+"]"));
	if(_isSafari())
		{
		//mouse hover on APO / FPO tool tip
	_mouseOver(_link("APO/FPO"));
		}
	else
		{
		_mouseOver(_link("tooltip",_near(_textbox("dwfrm_multishipping_editAddress_addressFields_address1"))));
		}
	//verify the functionality
	//mouse hovering on why is this required tool tip


                     //verifying the display of tool tip
                     if(_getAttribute(_link("tooltip",_near(_textbox("dwfrm_multishipping_editAddress_addressFields_address1"))),"aria-describedby")!=null)
                           {
                                  _assert(true);
                           }
                     else
                           {
                                  _assert(false);
                           }
	if(_isSafari())
	{
	//mouse hover on why is this required tool tip
	_mouseOver(_link("/Why is this required/"));
	}
	else
		{
		_mouseOver(_link("tooltip",_rightOf(_textbox("dwfrm_multishipping_editAddress_addressFields_phone"))));
		}
	//verify the functionality
	//mouse hovering on why is this required tool tip


                     //verifying the display of tool tip
                     if(_getAttribute(_link("tooltip[1]"),"aria-describedby")!=null)
                           {
                                  _assert(true);
                           }
                     else
                           {
                                  _assert(false);
                           }	
	_click(_button("Cancel"));
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("149048","Verify the functionality of 'Save' button in the  'Add or Edit Addresses' overlay  on Multi Shipping page in Prevail application as a Anonymous user");
$t.start();
try
{
	//navigate to shipping page
	_click(_link("mini-cart-link-checkout"));
	_click(_submit("dwfrm_login_unregistered"));
	//click on yes button 'Do you want multishipping' 
	_click(_submit("dwfrm_singleshipping_shipToMultiple"));	
	//click on add/edit link
	_click(_span("Add/Edit Address"));
	//enter the address
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_firstName"), $addr_data2[0][1]);
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_lastName"),$addr_data2[0][2]);
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_address1"), $addr_data2[0][3]);
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_address2"), $addr_data2[0][4]);
	_setSelected(_select("dwfrm_multishipping_editAddress_addressFields_country"), $addr_data2[0][5]);
	_setSelected(_select("dwfrm_multishipping_editAddress_addressFields_states_state"), $addr_data2[0][6]);
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_city"), $addr_data2[0][7]);
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_postal"), $addr_data2[0][8]);  
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_phone"), $addr_data2[0][9]);
	//click on save button
	_click(_submit("dwfrm_multishipping_editAddress_save"));
	//verify the address prepopulation in dropdown
	var $count=_count("_table","/item-list/",_in(_div("checkoutmultishipping")));
	for(var $i=0;$i<$count;$i++)
		{
		var $length=_getText(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList")).length;
		var $savedAddress=_getText(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"))[$length-1].split(",");
		//verifying the address
		_assertEqual($addr_data2[0][1]+" "+$addr_data2[0][2],$savedAddress[0]);
		_assertEqual($addr_data2[0][3],$savedAddress[1]);
		_assertEqual($addr_data2[0][7],$savedAddress[2]);
		_assertEqual($addr_data2[1][6],$savedAddress[3]);
		_assertEqual($addr_data2[0][8],$savedAddress[4]);
		//_assertEqual($addr_data2[0][9],$savedAddress[5]);
		}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126622/126636","Verify the functionality of 'Select' button/cancel and close links in  'Add or Edit Addresses' overlay  on Multi Shipping page in Application as a guest user");
$t.start();
try
{
	//navigate to shipping page
	_click(_link("mini-cart-link-checkout"));
	_click(_submit("dwfrm_login_unregistered"));
	//click on yes button 'Do you want multishipping' 
	_click(_submit("dwfrm_singleshipping_shipToMultiple"));	
	_click(_span("Add/Edit Address"));
	//select the address from drop down
	_setSelected(_select("dwfrm_multishipping_editAddress_addressList"),"/"+$addr_data2[0][1]+"/");
	//click on select button
	//verify the address pre population
	_assertEqual($addr_data2[0][1],_getText(_textbox("dwfrm_multishipping_editAddress_addressFields_firstName")));
	_assertEqual($addr_data2[0][2],_getText(_textbox("dwfrm_multishipping_editAddress_addressFields_lastName")));
	_assertEqual($addr_data2[0][3],_getText(_textbox("dwfrm_multishipping_editAddress_addressFields_address1")));
	_assertEqual($addr_data2[0][4],_getText(_textbox("dwfrm_multishipping_editAddress_addressFields_address2")));
	_assertEqual($addr_data2[0][5],_getSelectedText(_select("dwfrm_multishipping_editAddress_addressFields_country")));
	_assertEqual($addr_data2[0][6],_getSelectedText(_select("dwfrm_multishipping_editAddress_addressFields_states_state")));
	_assertEqual($addr_data2[0][7],_getText(_textbox("dwfrm_multishipping_editAddress_addressFields_city")));
	_assertEqual($addr_data2[0][8],_getText(_textbox("dwfrm_multishipping_editAddress_addressFields_postal")));
	_assertEqual($addr_data2[0][9],_getText(_textbox("dwfrm_multishipping_editAddress_addressFields_phone")));
	//edit some fields
	//address1
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_address1"),$SPC_Multishipping[0][6]);
	_assertEqual($SPC_Multishipping[0][6],_getText(_textbox("dwfrm_multishipping_editAddress_addressFields_address1")));
	//city
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_city"),$SPC_Multishipping[1][6]);
	_assertEqual($SPC_Multishipping[1][6],_getText(_textbox("dwfrm_multishipping_editAddress_addressFields_city")));
	//click on cancel link
	_click(_button("dwfrm_multishipping_editAddress_cancel"));
	//verify the functionality
	_assertNotVisible(_div("dialog-container"));
	_assertNotVisible(_button("dwfrm_multishipping_editAddress_cancel"));
	_assertNotVisible(_button("Close"));
	//again click on add/edit address link
	_click(_span("Add/Edit Address"));
	//click on close icon
	_click(_button("Close"));
	//verify the functionality
	_assertNotVisible(_div("dialog-container"));
	_assertNotVisible(_button("dwfrm_multishipping_editAddress_cancel"));
	_assertNotVisible(_button("Close"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126612","Verify the navigation when user clicks on Edit link displayed in the Order Summary Section on Multi Shipping page in Application as a guest user");
$t.start();
try
{
	//click on edit link
	if(!isMobile() || mobile.iPad())
	{
	var $productName=_getText(_link("/(.*)/",_in(_div("mini-cart-name",_in(_div("checkout-mini-cart"))))));
	//clicking on edit link present in the order Summuary section
	_click(_link("EDIT",_in(_div("secondary"))));
	}
	else
	{
	var $productName=_getText(_link("/(.*)/",_in(_div("mini-cart-name",_in(_div("checkout-mini-cart"))))));
	//clicking on edit link present in the order Summuary section
	_click(_link("Edit",_in(_heading3("ORDER SUMMARY Edit"))));
	}
	//verify the navigation to Cart page
	_assertVisible(_div("cart-actions cart-actions-top"));
	_assertVisible(_table("cart-table"));
	_assertVisible(_link("/(.*)/",_in(_div("name"))));
	_assertEqual($productName,_getText(_link("/(.*)/",_in(_div("name")))));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126650","Verify the navigation on click of Product name on Multi Shipping page in Application as a guest user");
$t.start();
try
{
	//navigate to shipping page
	_click(_link("mini-cart-link-checkout"));
	_click(_submit("dwfrm_login_unregistered")); 
	//click on yes button 'Do you want multishipping' 
	_click(_submit("dwfrm_singleshipping_shipToMultiple"));
	//click on product name link
	//fetch the product name
	var $productName=_getText(_link("/(.*)/",_in(_div("name"))));
	//click on name link
	_click(_link("/(.*)/",_in(_div("name"))));
	//verify the navigation to pdp page
	_assertVisible(_span($productName,_in(_div("breadcrumb"))));
	_assertVisible(_heading1("product-name"));
	_assertEqual($productName, _getText(_heading1("product-name")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


cleanup();
var $t = _testcase("128404","Verify the display of number of shipments below the order total Section on Multi Shipping page in Application as a Anonymous user");
$t.start();
try
{
//navigate to shipping page
navigateToCart($SPC_Multishipping[0][1],$SPC_Multishipping[0][8]); 
//click on go strait to checkout link
_click(_link("mini-cart-link-checkout"));
_click(_submit("dwfrm_login_unregistered")); 
//click on yes button 'Do you want multishipping' 
_click(_submit("dwfrm_singleshipping_shipToMultiple"));	
var $addrCount=0;
//adding addresses
for(var $i=0;$i<$addr_data.length;$i++)
{
	_click(_span("Add/Edit Address["+$i+"]"));
	addAddressMultiShip($addr_data,$i);
	$addrCount++;
}
//select addresses from drop down 
var $dropDowns=_count("_select","/dwfrm_multishipping_addressSelection_quantityLineItems/",_in(_table("item-list")));
var $c1=1;
var $j=1;
for(var $i=0;$i<$dropDowns;$i++)
	{
	   if($c1>$addrCount)
	    {
	      $c1=1;
	    }
	_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"),$j);
	$j++;
	}
_click(_submit("dwfrm_multishipping_addressSelection_save"));
//click on name link
var $pName=_getText(_link("/(.*)/",_in(_div("name"))));
_click(_link("/(.*)/",_in(_div("name"))));
//verify the navigation
_assertVisible(_span($pName,_in(_div("breadcrumb"))));
_assertVisible(_heading1("product-name"));
_assertEqual($pName, _getText(_heading1("product-name")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


cleanup();
var $t = _testcase("126603","Verify the functionality of 'Select Shipping method' section  in Checkout - Mulit Shipping page");
$t.start();
try
{
//navigate to shipping page
navigateToCart($SPC_Multishipping[0][1],$SPC_Multishipping[0][8]); 
//click on go strait to checkout link
_click(_link("mini-cart-link-checkout"));
_click(_submit("dwfrm_login_unregistered"));  
//click on yes button 'Do you want multishipping' 
_click(_submit("dwfrm_singleshipping_shipToMultiple"));	
var $addrCount=0;
//adding addresses
for(var $i=0;$i<$addr_data.length;$i++)
{
	_click(_span("Add/Edit Address["+$i+"]"));
	addAddressMultiShip($addr_data,$i);
	$addrCount++;
}
//select addresses from drop down 
var $dropDowns=_count("_select","/dwfrm_multishipping_addressSelection_quantityLineItems/",_in(_table("item-list")));
var $c1=1;
var $j=1;
for(var $i=0;$i<$dropDowns;$i++)
	{
	   if($c1>$addrCount)
	    {
	      $c1=1;
	    }
	_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"),$j);
	$j++;
	}	
//click on continue
_click(_submit("dwfrm_multishipping_addressSelection_save"));
//verify the number of shipments
//selected different address and verify number of shipments
var $numOfShipments=_count("_table","/item-list/",_in(_div("checkoutmultishipping")));
_assertEqual($SPC_Multishipping[2][4],$numOfShipments);
//again navigate to same page by selecting single address
//click on go strait to checkout link
_click(_link("mini-cart-link-checkout"));
_wait(2000);
_click(_submit("dwfrm_login_unregistered"));
_wait(2000);
//click on yes button 'Do you want multishipping' 
_click(_submit("dwfrm_singleshipping_shipToMultiple"));	
for(var $i=0;$i<$dropDowns;$i++)
{
_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"),"1");
}	
//click on continue
_click(_submit("dwfrm_multishipping_addressSelection_save"));
//selected same address and verify number of shipments
var $numOfShipments=_count("_table","/item-list/",_in(_div("checkoutmultishipping")));
_assertEqual($SPC_Multishipping[1][4],$numOfShipments);	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("156078","Verify the functionality of 'Select Shipping method' section  in Checkout - single Shipping page");
$t.start();
try
{
var $numOfShipments=_count("_table","/item-list/",_in(_div("checkoutmultishipping")));
for(var $i=0;$i<$numOfShipments;$i++)
	{
//verifying the shippig methods
var $length=_getText(_select("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_shippingMethodID")).length;
	for(var $j=0;$j<$length;$j++)
		{
		_assertEqual($SPC_Multishipping[$j][10],_extract(_getText(_select("dwfrm_multishipping_shippingOptions_shipments_i"+$i+"_shippingMethodID"))[$j],"/(.*)[:]/",true).toString());
		}
	}
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126615","Verify the navigationwhen user clicks on edit link displayed in the Shipment section in Shipping method page");
$t.start();
try
{
//click on edit link present below the shipping address section
_click(_link("Edit",_in(_cell("item-shipping-address"))));
//verify the navigation
_assertVisible(_span("Add/Edit Address"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("126641","Validated the fields in 'Add or Edit Addresses' overlay in Multi Shipping page");
$t.start();
try
{
	//click on add/edit address overlay
	_click(_span("Add/Edit Address"));
	//validating the fields
	for(var $i=0;$i<$Validation.length;$i++)
		{
		if($i==0)
		{
		_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_firstName"),"");
		_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_lastName"),"");
		_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_address1"),"");
		_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_address2"),"");
		_setSelected(_select("dwfrm_multishipping_editAddress_addressFields_country"),"");
		_setSelected(_select("dwfrm_multishipping_editAddress_addressFields_states_state"), "");
		_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_city"),"");
		_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_postal"), ""); 
		_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_phone"),"");
		}
	else
		{
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_firstName"), $Validation[$i][1]);
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_lastName"), $Validation[$i][2]);
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_address1"), $Validation[$i][3]);
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_address2"),$Validation[$i][4]);
	_setSelected(_select("dwfrm_multishipping_editAddress_addressFields_country"),$Validation[$i][5]);
	_setSelected(_select("dwfrm_multishipping_editAddress_addressFields_states_state"), $Validation[$i][6]);
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_city"), $Validation[$i][7]);
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_postal"), $Validation[$i][8]); 
	_setValue(_textbox("dwfrm_multishipping_editAddress_addressFields_phone"),$Validation[$i][9]);
		}
		//click on continue button
		_click(_submit("dwfrm_multishipping_editAddress_save"));

	//with blank data
	if($i==0)
		{
		  
		_assertVisible(_span("error"));
		_assertEqual("This field is required.", _getText(_span("error")));
		}
	//checking the max length
	if($i==1)
		{
		
		//verifying the length of field's first name,last name,add1,addr2,phone number,City 
		_assertEqual($Validation[1][10],_getText(_textbox("dwfrm_multishipping_editAddress_addressFields_firstName")).length);
		_assertEqual($Validation[1][10],_getText(_textbox("dwfrm_multishipping_editAddress_addressFields_lastName")).length);
		_assertEqual($Validation[1][10],_getText(_textbox("dwfrm_multishipping_editAddress_addressFields_address1")).length);
		_assertEqual($Validation[1][10],_getText(_textbox("dwfrm_multishipping_editAddress_addressFields_address2")).length);
		_assertEqual($Validation[1][10],_getText(_textbox("dwfrm_multishipping_editAddress_addressFields_city")).length);
		_assertEqual($Validation[0][11],_getText(_textbox("dwfrm_multishipping_editAddress_addressFields_phone")).length);
		//Zip Code max length
		_assertEqual($Validation[1][11],_getText(_textbox("dwfrm_multishipping_editAddress_addressFields_postal")).length); 
		}
	//numeric,special chars,combination of both,alphabets data in city,phone number,Zip code field's
	if($i==2 || $i==3 || $i==4 || $i==5)
		{
		//Fname,Lname Should not highlight
		_assertNotEqual($Validation[0][10],_style(_textbox("dwfrm_multishipping_editAddress_addressFields_firstName"),"background-color"));
		_assertNotEqual($Validation[0][10],_style(_textbox("dwfrm_multishipping_editAddress_addressFields_lastName"),"background-color"));
		//Zip Code
		_assertEqual($Validation[6][10],_style(_textbox("dwfrm_multishipping_editAddress_addressFields_postal"),"background-color")); 
		_assertEqual($Validation[4][10],_style(_span($Validation[2][11]),"color"));
		_assertVisible(_span($Validation[2][11])); 
		//Phone Number
		_assertEqual($Validation[6][10],_style(_textbox("dwfrm_multishipping_editAddress_addressFields_phone"),"background-color"));
		_assertVisible(_span($Validation[2][10])); 
		_assertEqual($Validation[4][10],_style(_span($Validation[2][10]),"color"));  
		//City
		//In Progress
		
		}
	//state field validation
	if($i==6)
		{
		_assertVisible(_span("This field is required."));
		}
	//with valid data
	if($i==7)
		{
		_assertNotVisible(_div("dialog-container"));
		}

	//In Progress

	}
	
	
	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



var $t = _testcase("155513","In multi shipping page: Verify address drop down field of second line item when an address added/selected in first line item.");
$t.start();
try
{
	 //Check for the display of second line item Address drop down field.
	// No change must be seen in the second line item address field.
	_assertEqual("Select an Address", _getSelectedText(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i1_addressList")));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();



cleanup();
}