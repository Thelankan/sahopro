_include("../../../util.GenericLibrary/GlobalFunctions.js");
_include("../../../util.GenericLibrary/BM_Configuration.js");
_resource("IntermediateLogin_Data.xls");

var $Item=_readExcelFile("IntermediateLogin_Data.xls","Item");
var $IMLogin_Data=_readExcelFile("IntermediateLogin_Data.xls","Login_Data");
var $IMLogin_Validation=_readExcelFile("IntermediateLogin_Data.xls","Login_Validation");
var $IM_ForgotPwd=_readExcelFile("IntermediateLogin_Data.xls","Forgot_Pwd");


var $color=$Item[0][2];
var $emailID=$uId;

//for deleting user
deleteUser();
_setAccessorIgnoreCase(true); 
SiteURLs();
cleanup();

//navigate to cart page
navigateToCart($Item[0][0],$Item[0][1]);
//click on go straight to checkout
_click(_link("mini-cart-link-checkout"));
//validating IM login page
_assertVisible(_heading2("/"+$IMLogin_Data[0][0]+"/"));
_assertVisible(_heading2($IMLogin_Data[0][1]));
_assertVisible(_div("login-box login-account"));

//verifies whether five page or single page checkout
if($CheckoutType==$BMConfig[0][1])
{
var $t = _testcase("124962", "Verify the navigation to 'Checkout- Intermediate Login' page from mini cart page in Application as an Anonymous user.");
$t.start();
try
{
	//navigate to cart page
	navigateToCart($Item[0][0],$Item[0][1]);
	//click on go straight to checkout
	_click(_link("mini-cart-link-checkout"));
	//validating IM login page
	_assertVisible(_heading2("/"+$IMLogin_Data[0][0]+"/"));
	_assertVisible(_heading2($IMLogin_Data[0][1]));
	_assertVisible(_div("login-box login-account"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124963", "Verify the UI of 'Checkout- Intermediate Login' page in Application as an Anonymous user.");
$t.start();
try
{
	//Verifying the UI of returning customer section
	//Returning customer heading
	_assertVisible(_heading2("/"+$IMLogin_Data[0][0]+"/"));
	//Paragraph with static text
	_assertVisible(_paragraph("/(.*)/",_in(_div("login-box-content returning-customers clearfix"))));
	//email Address
	_assertVisible(_span($IMLogin_Data[1][0]));
	_assertVisible(_textbox("/dwfrm_login_username/"));
	_assertEqual("", _getValue(_textbox("/dwfrm_login_username/")));
	//password
	_assertVisible(_span($IMLogin_Data[2][0]));
	_assertVisible(_password("/dwfrm_login_password/"));
	_assertEqual("", _getValue(_password("/dwfrm_login_password/")));
	//remember me
	_assertVisible(_label($IMLogin_Data[3][0]));
	_assertVisible(_checkbox("dwfrm_login_rememberme"));
	_assertNotTrue(_checkbox("dwfrm_login_rememberme").checked);
	//login button
	_assertVisible(_submit("Login"));
	//forgot password
	_assertVisible(_link("password-reset"));
	_assertEqual($IMLogin_Data[4][0], _getText(_link("password-reset")));
	//social links
	_assertVisible(_div("login-oauth"));
	//_assertVisible(_paragraph($IMLogin_Data[5][0]));
	_assertVisible(_div("login-box-content returning-customers clearfix"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124968", "Verify the functionality of 'Forgot password' link on intermediate login page in Prevail application as an Anonymous user.");
$t.start();
try
{
	//clicking on forgot password link
	_click(_link("password-reset"));
	//verify whether overlay got opened or not
	_assertVisible(_div("dialog-container"));
	_assertVisible(_span("ui-dialog-title"));
	_assertVisible(_heading1($IMLogin_Data[1][2]));
	_assertVisible(_button("Close"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
 $t.end();
 
  var $t = _testcase("124964", "Verify the UI of 'forgot password?' overlay on click of 'forgot password link' on Intermediate Login page in Application  as an Anonymous user.");
 $t.start();
 try
 {
	 //verify the UI of forgot password overlay
	 _assertVisible(_heading1($IMLogin_Data[1][2]));	
	 _assertVisible(_paragraph("/(.*)/",_in(_div("dialog-container"))));
	 //email field
	 _assertVisible(_heading1($IMLogin_Data[1][2]));
	 _assertVisible(_textbox("dwfrm_requestpassword_email"));
	 _assertVisible(_submit("dwfrm_requestpassword_send"));
	 _assertVisible(_button("Close")); 
	 //closing the dialog
	 _click(_button("Close"));
 }	
 catch($e)
 {
 	_logExceptionAsFailure($e);
 }	
 $t.end(); 
 
var $t = _testcase("124965/124966/124970", "Verify the Validation of 'Email address' field in forgot password overlay,UI of forgot password confirmation overlay and functionality of 'X(Close)' button of an overlay in Application  as an Anonymous user.");
$t.start();
try
{
     _click(_link("password-reset")); 
	//validating the email id & password field's
	for(var $i=0;$i<$IM_ForgotPwd.length;$i++)
		{
		if($i==4)
			{
			_setValue(_textbox("dwfrm_requestpassword_email"),$emailID);
			}
		else
			{
			_setValue(_textbox("dwfrm_requestpassword_email"),$IM_ForgotPwd[$i][1]);
			}		
		_click(_submit("dwfrm_requestpassword_send"));
		//blank
		if($i==0)
			{
			//username
			_assertEqual($color,_style(_textbox("dwfrm_requestpassword_email"),"background-color"));
			_assertVisible(_span($IM_ForgotPwd[$i][2]));
			}
		//invalid 
		else if($i==1 || $i==2)
			{
			_assertVisible(_div($IM_ForgotPwd[$i][2]));
			}
		//Non-existing
		else if($i==3)
			{
			_assertVisible(_div($IM_ForgotPwd[$i][2]));
			}
		//valid data and Confirmation overlay UI
		else
			{
			_assertVisible(_heading1($IM_ForgotPwd[$i][2]));
			_assertVisible(_paragraph("/(.*)/",_in(_div("dialog-container"))));
			_assertVisible(_link("Go to the home page"));
			_assertVisible(_button("Close"));
			}
		}
	//close functionality of confirmation overlay
	_assertVisible(_button("Close"));
	_click(_button("Close"));
	_assertNotVisible(_div("dialog-container"));
	_assertNotVisible(_heading1($IM_ForgotPwd[4][2]));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124973","Verify the functionality of 'Checkout' button in intermediate login page in Prevail application as an Anonymous user.");
$t.start();
try
{
//click on go straight to checkout
_click(_link("mini-cart-link-checkout"));
_assertVisible(_submit("dwfrm_login_unregistered"));

_click(_submit("dwfrm_login_unregistered")); 
//verify the navigation
_assertEqual("STEP 1: Shipping", _getText(_div("step-1 active")));
_assertVisible(_fieldset("/Select or Enter Shipping Address/"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("124971/125619/124969","Verify the validation of 'Email address'/ 'Password' field and Verify the functionality of  'Login' button in the Intermediate Login page in Prevail application as an Anonymous user.");
$t.start();
try
{
//navigate to Cart page
	_click(_link("View Cart"));
	_click(_submit("dwfrm_cart_checkoutCart"));

//validating the email id & password field's
for(var $i=0;$i<$IMLogin_Validation.length;$i++)
	{
	if($i==5)
		{
		login();
		}
	else
		{
		_setValue(_textbox("/dwfrm_login_username/"),$IMLogin_Validation[$i][1]);
		_setValue(_password("/dwfrm_login_password/"),$IMLogin_Validation[$i][2]);
		_click(_submit("dwfrm_login_login"));
		}
	//blank
	if($i==0)
		{
		//username
		_assertVisible(_span($IMLogin_Validation[$i][3]));
		_assertEqual($color,_style(_textbox("/dwfrm_login_username/"),"background-color"));		
		//password
		_assertVisible(_span($IMLogin_Validation[$i][4]));
		_assertEqual($color,_style(_password("/dwfrm_login_password/"),"background-color"));
		}
	//valid
	else if($i==5)
		{
		_assertVisible(_heading2("Logged-in as "+$FName+" "+$LName));
		_assertEqual("STEP 1: Shipping", _getText(_div("step-1 active")));
		_assertVisible(_fieldset("/Select or Enter Shipping Address/"));
		}
	else if($i==2)
		{
		_assertVisible(_span($IMLogin_Validation[$i][3]));
		_assertEqual($IMLogin_Validation[1][5],_style(_span($IMLogin_Validation[$i][3]),"color"));
		_assertEqual($IMLogin_Validation[0][5],_style(_textbox("/dwfrm_login_username/"),"background-color"));
		}
	//invalid 
	else
		{
		_assertVisible(_div($IMLogin_Validation[$i][3]));
		}	
	}
ClearCartItems();
//logout from application
_click(_link("user-account"));
_click(_link("/Logout/"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("126800","Verify the functionality of 'Create an Account' button in Intemediate Login page under New Customer section for Guest user");
$t.start();
try
{
	//navigate to cart page
	navigateToCart($Item[0][0],$Item[0][1]);
	//click on go straight to checkout to navigate to IL Page
	_click(_link("mini-cart-link-checkout"));
	if(_isVisible(_submit("dwfrm_login_register")))
		{
		_click(_submit("dwfrm_login_register"));
		//verify the navigation to create account page
		_assertVisible(_heading1($IMLogin_Data[1][3]));
		_assertVisible(_link($IMLogin_Data[0][3], _in(_div("breadcrumb"))));
		//fill all the details
		createAccount();
		_assertVisible(_fieldset("/Select or Enter Shipping Address/"));
		_assertEqual($IMLogin_Data[2][3], _getText(_div("checkout-tab-head open")));
		}
	else
		{
		_assert(false,"Create account button is not there");
		}
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

}


cleanup();
//for deleting user
deleteUser();
