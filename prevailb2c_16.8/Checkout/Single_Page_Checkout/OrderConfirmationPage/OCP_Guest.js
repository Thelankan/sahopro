_include("../../../util.GenericLibrary/GlobalFunctions.js");
_include("../../../util.GenericLibrary/BM_Configuration.js");
_resource("OrderConfirmation.xls");
_resource("../BillingPage/BillingPage.xls");

deleteUser();

var $creditcard_data=_readExcelFile("OrderConfirmation.xls","Payment");
var $OCP=_readExcelFile("OrderConfirmation.xls","OCP");
var $Address=_readExcelFile("OrderConfirmation.xls","Address");
var $Account_Validation=_readExcelFile("OrderConfirmation.xls","Validation");
var $paypal=_readExcelFile("../BillingPage/BillingPage.xls","Paypal");
var $multishipAddress=_readExcelFile("../BillingPage/BillingPage.xls","Address");
var $color=$Account_Validation[0][8];
var $color1=$Account_Validation[1][8];

var $shippingTax;
var $tax;
var $subTotal;
var $Ordertotal;
var $paymentType;

var $sheetName;
var $rowNum;


//verifies whether five page or single page checkout
if($CheckoutType==$BMConfig[0][1])
{
SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();

var $Address1=$Address[0][1]+" "+$Address[0][2]+" "+$Address[0][3]+" "+$Address[0][4]+" "+$Address[0][7]+" "+$Address[1][6]+" "+$Address[0][8]+" "+$Address[0][5]+" "+$Address[0][9];
var $shippingAddress="Shipping To "+$Address1;
var $billingAddress="Billing Address "+$Address[0][1]+" "+$Address[0][2]+" "+$Address[0][3]+" "+$Address[0][4]+" "+$Address[0][7]+" "+$Address[1][6]+" "+$Address[0][8]+" "+$Address[0][5]+" "+"Phone: "+$Address[0][9];

var $t=_testcase("125787/125785","Verify the UI & navigation of the 'Order summery' section on Thank you for your order page in a application as a Guest user");
$t.start();
try{
//navigating to Shipping page
navigateToCart($OCP[0][8],1);
_click(_submit("dwfrm_cart_checkoutCart"));
_click(_submit("dwfrm_login_unregistered")); 
shippingAddress($Address,0);
//check use this for billing check box
_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
if(isMobile() && !mobile.iPad())
{
$shippingTax=_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString();	
$subTotal=_extract(_getText(_span("mini-cart-price",_in(_div("mini-cart-pricing")))),"/[$](.*)/",true).toString();
}
else
{
$shippingTax=_extract(_getText(_row("order-shipping  first ")),"/[$](.*)/",true).toString();
$subTotal=_extract(_getText(_span("mini-cart-price",_in(_div("secondary")))),"/[$](.*)/",true).toString();
}
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
//Address Verification overlay
addressVerificationOverlay();

BillingAddress($Address,0); 
$tax=_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString();
$Ordertotal=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
//entering Credit card Information
if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
    {
	if($CreditCardPayment==$BMConfig[0][4])
	  {
	  	PaymentDetails($paypal,2);
	  }
	  else
	  {
		    PaymentDetails($creditcard_data,0); 
	  }
           $paymentType="Credit Card";
           _wait(3000,_isVisible(_submit("dwfrm_billing_save")));
           _click(_submit("dwfrm_billing_save"));
           _wait(10000,_isVisible(_submit("submit")));
    }
else if($CreditCard=="No" && $paypalNormal=="Yes")
    {
           _click(_radio("is-PayPal"));
           $paymentType="Pay Pal";
           _wait(3000,_isVisible(_submit("dwfrm_billing_save")));
           _click(_submit("dwfrm_billing_save"));
           if(isMobile())
           {
                 _setValue(_emailbox("login_email"), $paypal[0][0]);
           }
           else
           {
           _setValue(_textbox("login_email"), $paypal[0][0]);
           }
    _setValue(_password("login_password"), $paypal[0][1]);
    _click(_submit("Log In"));
    _click(_submit("Continue"));
    _wait(10000,_isVisible(_submit("submit")));
    }
//Address Verification overlay
addressVerificationOverlay();
//fetch order total
var $orderTot=_getText(_cell("order-value"));
//Placing order
_click(_submit("button-fancy-large"));

if(_isVisible(_image("CloseIcon.png")))
{
_click(_image("CloseIcon.png"));
}
//verify the navigation to OCP page
_assertVisible(_heading1($OCP[0][0]));
_assertVisible(_div("content-asset"));
//verify the UI
_assertVisible(_span($OCP[0][7]));
_assertVisible(_span($OCP[1][7]));
_assertVisible(_span("value", _near(_span($OCP[0][7]))));
_assertVisible(_span("/value/",_near(_span($OCP[1][7])))); 
}catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

var $t=_testcase("125786/148929","Verify the UI of the 'Shipment' section on Thank you for your order page in a application as a Guest user");
$t.start();
try
{
	
	//verifying the UI of Shipment section
	_assertVisible(_heading2($OCP[0][4]));
//product name
_assertVisible(_link("/(.*)/",_in(_div("name",_in(_div("product-list-item"))))));
//item number
_assertVisible(_span($OCP[0][3]));
_assertVisible(_span("/value/",_in(_div("sku"))));
_assertVisible(_div("sku"));
//full product details div
_assertVisible(_div("product-list-item"));

	if(!isMobile() || mobile.iPad())
	{
	if(isIE11() || _isIE9() || _isIE10())
		{
		_assertVisible(_div("line-item-price"));
		_assertVisible(_div("line-item-quantity"));
		}
	else
		{
		//quantity
		_assertVisible(_div("line-item-quantity"));
		//price
		_assertVisible(_div("line-item-price"));
		}
	}
	//verifying the shipping address
	_assertVisible(_div("order-shipment-address"));
	_assertEqual($shippingAddress,_getText(_div("order-shipment-address")).replace(",",""));
	//return shopping
	_assertVisible(_link($OCP[5][4]));
	//shipping status and method
	//shipping status
	_assertVisible(_div($OCP[2][7]));
	_assertVisible(_div("value",_in(_div("shipping-status"))));
	//shipping method
	_assertVisible(_div($OCP[3][7]));
	_assertVisible(_div("value",_in(_div("shipping-method"))));
	//entire div
	_assertVisible(_div("order-shipment-table"));
}catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

	
var $t=_testcase("140446/148932","Verify the details to be displayed in the 'SHIPPING TO' section on Thank you for your order page in a application as a Guest user");
$t.start();
try
{
	
	//verifying the shipping address
	_assertEqual($shippingAddress,_getText(_div("order-shipment-address")).replace(",",""));
	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("140447/125785/148933","Verify the details to be displayed in the 'BILLING ADDRESS' section on Thank you for your order page in a application as a Guest user");
$t.start();
try
{
	
	//verifying the billing address
	_assertEqual($billingAddress,_getText(_div("order-billing")).replace(",",""));

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("140448/140449/125785/148934","Verify the details to be displayed in the 'PAYMENT METHOD' section on Thank you for your order page in a application as a Guest user");
$t.start();
try
{
	
	_assertVisible(_div("order-payment-instruments"));
	_assertVisible(_div("payment-type"));
	_assertEqual($paymentType, _getText(_div("payment-type")));


	if($paymentType=="Credit Card")
		{
	if($CreditCardPayment==$BMConfig[0][4])
		{
		var $exp=$creditcard_data[0][7]+"."+$paypal[3][4]+"."+$paypal[2][5];
		$sheetName=$paypal;
		 $rowNum=2;
		}
	else
		{
		var $exp=$creditcard_data[0][7]+"."+$creditcard_data[1][4]+"."+$creditcard_data[0][5];
		$sheetName=$creditcard_data;
		$rowNum=0;
		}

	var $paymentDetails=new Array();
	$paymentDetails=_getText(_div("order-payment-instruments")).split(" ");
	var $num=$paymentDetails[4].split("*")[12];
	//if($CreditCardPayment==$BMConfig[0][4])
	//{
//		_assertEqual($sheetName[3][3],$num);
	//}
	//else
	//{
	//_assertEqual($sheetName[1][3],$num);
	//}
	_assertVisible(_div("order-payment-instruments"));
	_assertEqual($creditcard_data[0][8]+" "+$orderTot, _getText(_div("order-payment-instruments")));
		}
	else if($paymentType=="Pay Pal")
		{
		_assertVisible(_span("Amount:"));
		var $OCPtotal=parseFloat(_extract(_getText(_div("payment-amount")),"/[$](.*)/",true));
		_assertEqual($Ordertotal,$OCPtotal);
		}
	
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("140450/148935","Verify the display of the 'PAYMENT TOTAL' section on Thank you for your order page in a application as a Guest user");
$t.start();
try
{
	
	//payment total heading
	_assertVisible(_div("Payment Total"));
	//all other values
	_assertVisible(_cell($OCP[1][1]));
	_assertVisible(_cell($OCP[2][1]));
	_assertVisible(_cell("/"+$OCP[3][1]+"/"));
	_assertVisible(_cell($OCP[4][1]));
	_assertEqual($subTotal,_extract(_getText(_row("order-subtotal")),"/[$](.*)/",true).toString());
	_assertEqual($shippingTax,_extract(_getText(_row("order-shipping")),"/[$](.*)/",true).toString());
	_assertEqual($tax,_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString());
	_assertEqual($Ordertotal,_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString());

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("140445/148931","Verify the details to be displayed for product line 'ITEM' on Thank you for your order page in a application as a Guest user");
$t.start();
try
{
	
//verifying the line item
_assertVisible(_link("/(.*)/",_in(_div("name",_in(_div("product-list-item"))))));
_assertEqual($pName, _getText(_link("/(.*)/",_in(_div("name",_in(_div("product-list-item")))))));
//_assertVisible(_span($OCP[0][3]));
_assertVisible(_span("/value/",_in(_div("sku")))); 
if(isMobile() && !mobile.iPad())
	{
	_assertVisible(_cell($quantity));
	_assertVisible(_cell("/"+$totalPrice+"/"));
	}
else
	{
	if(isIE11() || _isIE9() || _isIE10())
	{
		_assertVisible(_cell("line-item-price"));
		_assertEqual($totalPrice,_extract(_getText(_cell("line-item-price")),"/[$](.*)/",true).toString());
		_assertVisible(_cell("line-item-quantity"));
		_assertEqual($quantity,_getText(_cell("line-item-quantity")));
	}
else
	{
	_assertVisible(_div("line-item-quantity"));
	_assertEqual($quantity,_extract(_getText(_div("line-item-quantity")),"/Qty (.*)/",true).toString());
	_assertVisible(_div("line-item-price"));
	_assertEqual($totalPrice,_extract(_getText(_div("line-item-price")),"/[$](.*)/",true).toString());
	}
	}

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("125790/140443","Verify the navigation of the 'Product name' link on Thank you for your order page in a application as a Guest user");
$t.start();
try
{	
	//clicking on product name link
	var $pName=_getText(_link("/(.*)/",_in(_div("name",_in(_div("product-list-item"))))));
	_click(_link("/(.*)/",_in(_div("name",_in(_div("product-list-item"))))));	
	//verify the navigation to PDP
	_assertVisible(_div("breadcrumb"));
	_assertVisible(_heading1("product-name"));
	_assertEqual($pName, _getText(_heading1("product-name")));
	if(!isMobile())
		{
		//bread crumb
		_assertVisible(_span($pName,_in(_div("breadcrumb"))));
		}

}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("148937/125889/125885/125886/125887/125904/125788/125889/148936","Verify the field validations for First Name/last name/email/confirm email/password/confirm password & valid data text box in right nav & 'Order Number' on Thank you for your order page in a application as a Guest user");
$t.start();
try
{	
	//navigating to OCP 
	//navigating to Shipping page
	navigateToShippingPage($OCP[0][8],1);
	shippingAddress($Address,0);
	//check use this for billing check box
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	if(isMobile() && !mobile.iPad())
		{
		$shippingTax=_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString();
		$subTotal=_extract(_getText(_span("mini-cart-price",_in(_div("checkout-mini-cart")))),"/[$](.*)/",true).toString();
		}
	else
		{
	$shippingTax=_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString();
	$subTotal=_extract(_getText(_span("mini-cart-price",_in(_div("secondary")))),"/[$](.*)/",true).toString();
		}
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//Address Verification overlay
	addressVerificationOverlay();
	BillingAddress($Address,0);
	$tax=_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString();
	$Ordertotal=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
	//entering Credit card Information
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
	    {
		 //PaymentDetails($Valid_Data,4);
	    PaymentDetails($creditcard_data,0); 
	    _wait(3000,_isVisible(_submit("dwfrm_billing_save")));
	           _click(_submit("dwfrm_billing_save"));
	           _wait(10000,_isVisible(_submit("submit")));
	    }
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	    {
	           _click(_radio("is-PayPal"));
	           _wait(3000,_isVisible(_submit("dwfrm_billing_save")));
	           _click(_submit("dwfrm_billing_save"));
	           if(isMobile())
	           {
	                 _setValue(_emailbox("login_email"), $paypal[0][0]);
	           }
	           else
	           {
	           _setValue(_textbox("login_email"), $paypal[0][0]);
	           }
	    _setValue(_password("login_password"), $paypal[0][1]);
	    _click(_submit("Log In"));
	    _click(_submit("Continue"));
	    _wait(10000,_isVisible(_submit("submit")));
	    }
	//Address Verification overlay
	addressVerificationOverlay();
	
	
	//click on Place Order
	_click(_submit("button-fancy-large"));
	
	if(_isVisible(_image("CloseIcon.png")))
	{
	_click(_image("CloseIcon.png"));
	}
	
	//verify the Order Number
	_assertVisible(_span("/value/",_near(_span($OCP[1][7])))); 
	var $orderNum=_getText(_span("/value/",_near(_span($OCP[1][7]))));
	_log($orderNum);

//validate the fields
for(var $i=0;$i<$Account_Validation.length;$i++)
	{
		if($i==10)
		{
			createAccount();
			//verify whether account got created or not
	 	   var $exp=$userData[$user][1]+" "+$userData[$user][2];
	 	   _assertEqual($exp, _extract(_getText(_heading1("/My Account/")),"/[|] (.*) [(]/",true).toString());
	 	   _click(_link("user-account"));
	 	   _click(_link("Logout"));
	 	  _click(_submit("dwfrm_login_register"));
		}
	else
		{
		 _setValue(_textbox("dwfrm_profile_customer_firstname"), $Account_Validation[$i][1]);
	 	 _setValue(_textbox("dwfrm_profile_customer_lastname"), $Account_Validation[$i][2]);
	 	 _setValue(_textbox("dwfrm_profile_customer_email"), $Account_Validation[$i][3]);
	 	 _setValue(_textbox("dwfrm_profile_customer_emailconfirm"), $Account_Validation[$i][4]);
	 	 _setValue(_password("/dwfrm_profile_login_password/"), $Account_Validation[$i][5]);
	 	 _setValue(_password("/dwfrm_profile_login_passwordconfirm/"), $Account_Validation[$i][6]);
	 	 if($i!=1)
	 		 {
	 		_click(_submit("dwfrm_profile_confirm"));
	 		 }	
	 	 //blank field validation
	 	 if($i==0)
	 		 {
	 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_firstname"), "background-color"));
	 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_lastname"), "background-color"));
	 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_email"), "background-color"));
	 		  _assertEqual($color, _style(_textbox("dwfrm_profile_customer_emailconfirm"), "background-color"));
	 		  _assertEqual($color, _style(_password("/dwfrm_profile_login_password/"), "background-color"));
	 	  	  _assertEqual($color, _style(_password("/dwfrm_profile_login_passwordconfirm/"), "background-color"));
	 		 }
	 	 //Max characters
	 	 else if($i==1)
	 		 {
	 		_assertEqual($Account_Validation[0][9],_getText(_textbox("dwfrm_profile_customer_firstname")).length);
	 		_assertEqual($Account_Validation[1][9],_getText(_textbox("dwfrm_profile_customer_lastname")).length);
	 		_assertEqual($Account_Validation[2][9],_getText(_textbox("dwfrm_profile_customer_email")).length);
	 		_assertEqual($Account_Validation[3][9],_getText(_textbox("dwfrm_profile_customer_emailconfirm")).length);
	 		_assertEqual($Account_Validation[4][9],_getText(_password("/dwfrm_profile_login_password/")).length);
	 		_assertEqual($Account_Validation[5][9],_getText(_password("/dwfrm_profile_login_passwordconfirm/")).length);
	 		 }
	 	 //invalid data
	 	 else if($i==2 || $i==3 ||$i==4 || $i==5)
	 		 {
	 		 _assertVisible(_span($Account_Validation[$i][7], _in(_div("/Email/"))));
	 		 //_assertEqual($color, _style(_textbox("dwfrm_profile_customer_email"), "background-color"));	 		  
	 		 _assertVisible(_span($Account_Validation[$i][7], _in(_div("/Confirm Email/"))));
	 		 //_assertEqual($color, _style(_textbox("dwfrm_profile_customer_emailconfirm"), "background-color"));
	 		 }	 	 
	 	 //password less than 8 characters
	 	 else if($i==6)
	 		 {
	 		_assertVisible(_div($Account_Validation[$i][7]));
	 		_assertEqual($color1,_style(_span($Account_Validation[1][10]), "color"));			 		 
	 		_assertVisible(_div($Account_Validation[$i][7], _in(_div("/ Confirm Password /"))));
	 		_assertEqual($color1,_style(_span($Account_Validation[2][10]), "color"));
	 		 }
	 	 else if($i==7 || $i==8 || $i==9)
	 		 {			 		
	 		 	if($i==7)
	 		 		{
	 		 		 _assertEqual($color1, _style(_span($Account_Validation[0][10]), "color"));
	 		 		}
	 		 	if($i==8)
	 		 		{
	 		 		_assertEqual($color1, _style(_span($Account_Validation[2][10]), "color"));
	 		 		}
	 		_assertVisible(_div($Account_Validation[$i][7]));
	 		 }
		}
}
//verify the Order Number in Order History
_click(_link("user-account"));
_click(_link("My Account"));
_click(_link("Orders"));
//verifying the order details
_assertVisible(_span("/value/", _in(_div("order-number")))); 
_assertEqual($orderNum,_getText(_span("/value/", _in(_div("order-number"))))); 
//logout from the application
_click(_link("user-account"));
_click(_link("Logout"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t=_testcase("125784","Verify the functionality of 'Return to Shopping' button on Thank you for your order page in a application as a Guest user");
$t.start();
try
{
	
	//navigating to OCP 
	//navigating to Shipping page
	navigateToShippingPage($OCP[0][8],1);
	shippingAddress($Address,0);
	//check use this for billing check box
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	if(isMobile() && !mobile.iPad())
		{
			$shippingTax=_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString();
			$subTotal=_extract(_getText(_span("mini-cart-price",_in(_div("checkout-mini-cart")))),"/[$](.*)/",true).toString();
		}
	else
		{
			$shippingTax=_extract(_getText(_row("/order-shipping/")),"/[$](.*)/",true).toString();
			$subTotal=_extract(_getText(_span("mini-cart-price",_in(_div("secondary")))),"/[$](.*)/",true).toString();
		}
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//Address Verification overlay
	addressVerificationOverlay();
	BillingAddress($Address,0);
	$tax=_extract(_getText(_row("order-sales-tax")),"/[$](.*)/",true).toString();
	$Ordertotal=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
	//entering Credit card Information
	if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
	    {
			if($CreditCardPayment==$BMConfig[0][4])
			  {
			  	PaymentDetails($paypal,2);
			  }
			  else
			  {
				    PaymentDetails($creditcard_data,0); 
			  }
		    _wait(3000,_isVisible(_submit("dwfrm_billing_save")));
	           _click(_submit("dwfrm_billing_save"));
	           _wait(10000,_isVisible(_submit("submit")));
	    }
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	    {
	           _click(_radio("is-PayPal"));
	           _wait(3000,_isVisible(_submit("dwfrm_billing_save")));
	           _click(_submit("dwfrm_billing_save"));
	           if(isMobile())
	           {
	                 _setValue(_emailbox("login_email"), $paypal[0][0]);
	           }
	           else
	           {
	           _setValue(_textbox("login_email"), $paypal[0][0]);
	           }
	    _setValue(_password("login_password"), $paypal[0][1]);
	    _click(_submit("Log In"));
	    _click(_submit("Continue"));
	    _wait(10000,_isVisible(_submit("submit")));
	    }
	//Address Verification overlay
	addressVerificationOverlay();
	
	//click on Place Order
	_click(_submit("button-fancy-large"));
	
	//verify the navigation to OCP page
	if(_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
	
	//click on returning customer link
	_click(_link("Return to Shopping"));
	//verify the navigation to recently visited PLP
	_assertVisible(_div("breadcrumb"));
	_assertVisible(_div("search-result-content"));
	_assertVisible(_div("refinements"));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


cleanup();
var $t=_testcase("125924","Verifying multiple shipping functionality in order confirmation page for guest user");
$t.start();
try
{
//navigating to Shipping page
navigateToShippingPage($OCP[0][8],2);
var $shippingMethod=_extract(_getText(_row("/order-shipping  first/")),"/Shipping(.*)[$]/",true).toString().trim();
//click on yes button
_click(_submit("dwfrm_singleshipping_shipToMultiple"));
//adding addresses
for(var $i=0;$i<$multishipAddress.length;$i++)
{
	_click(_span("Add/Edit Address["+$i+"]"));
	addAddressMultiShip($multishipAddress,$i);
}

var $j=1;
	for(var $k=0;$k<$multishipAddress.length;$k++)
	{
		_setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$k+"_addressList"),$j);
		$j++;
	}
	
_click(_submit("dwfrm_multishipping_addressSelection_save"));
_click(_submit("dwfrm_multishipping_shippingOptions_save"));
//fill billing details
BillingAddress($Address,0); 
//entering Credit card Information
if(($CreditCard=="Yes" && $paypalNormal=="Yes") || ($CreditCard=="Yes" && $paypalNormal=="No"))
    {
	
		if($CreditCardPayment==$BMConfig[0][4])
		  {
		  	PaymentDetails($paypal,2);
		  }
		  else
		  {
			    PaymentDetails($creditcard_data,0); 
		  }
           $paymentType="Credit Card";
           _wait(3000,_isVisible(_submit("dwfrm_billing_save")));
           _click(_submit("dwfrm_billing_save"));
           _wait(10000,_isVisible(_submit("submit")));
    }
else if($CreditCard=="No" && $paypalNormal=="Yes")
    {
           _click(_radio("is-PayPal"));
           $paymentType="Pay Pal";
           _wait(3000,_isVisible(_submit("dwfrm_billing_save")));
           _click(_submit("dwfrm_billing_save"));
           if(isMobile())
           {
                 _setValue(_emailbox("login_email"), $paypal[0][0]);
           }
           else
           {
           _setValue(_textbox("login_email"), $paypal[0][0]);
           }
    _setValue(_password("login_password"), $paypal[0][1]);
    _click(_submit("Log In"));
    _click(_submit("Continue"));
    _wait(10000,_isVisible(_submit("submit")));
    }
//click on Place Order
	_click(_submit("button-fancy-large"));
	
	//verify the navigation to OCP page
	if(_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
//verify the addresses
	var $count=_count("_table","/order-shipment-table/");
	_log($count);
	for(var $i=0;$i<$count;$i++)
		{
		
		var $ExpShippingAddress=$multishipAddress[$i][2]+" "+$multishipAddress[$i][3]+" "+$multishipAddress[$i][4]+" "+$multishipAddress[$i][5]+" "+$multishipAddress[$i][8]+", "+$multishipAddress[$i][11]+" "+$multishipAddress[$i][9]+" "+$multishipAddress[$i][6]+" "+$multishipAddress[$i][10];
		var $ActShippingAddress=_getText(_div("order-shipment-address[0]"));
		_log($ActShippingAddress);
			if($ActShippingAddress.indexOf($multishipAddress[$i][2])>=0)
			{
			_assertEqual($ExpShippingAddress,$ActShippingAddress);
			}
			else 
			{
			var $ActShippingAddress=_getText(_div("order-shipment-address[1]"));
			//var $ActShippingAddress=_getText(_div("order-shipment-address["+$i+"]"));	
			_assertEqual($ExpShippingAddress,$ActShippingAddress);
			}
		}
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();	


cleanup();
}