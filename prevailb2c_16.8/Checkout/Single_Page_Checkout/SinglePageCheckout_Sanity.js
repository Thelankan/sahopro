_include("../../util.GenericLibrary/GlobalFunctions.js");
_include("../../util.GenericLibrary/BM_Configuration.js");
_resource("IntermediateLoginPage/IntermediateLogin_Data.xls");
_resource("Shipping Page/Shipping Page.xls");
_resource("BillingPage/BillingPage.xls");


//Intermediate
var $Item=_readExcelFile("IntermediateLoginPage/IntermediateLogin_Data.xls","Item");
//Shipping
var $Shipping_Page=_readExcelFile("Shipping Page/Shipping Page.xls","Shipping_Page");
var $Shipping_addr=_readExcelFile("Shipping Page/Shipping Page.xls","Address2");
//Billing
var $item = _readExcelFile("BillingPage/BillingPage.xls","Item");
var $Valid_Data=_readExcelFile("BillingPage/BillingPage.xls","ValidData");
var $paypal=_readExcelFile("BillingPage/BillingPage.xls","Paypal");
var $Generic=_readExcelFile("BillingPage/BillingPage.xls","Generic");
//Order summary
var $addr_data=_readExcelFile("Shipping Page/Shipping Page.xls","Address");
var $UserEmail=$uId;
SiteURLs();
cleanup();
_setAccessorIgnoreCase(true); 


var $t = _testcase("124969","Verify the functionality of  'Login' button in the Intermediate Login page in Prevail application as an Anonymous user.");
$t.start();
try
{
	
	//navigate to cart page
	navigateToCart($Item[0][0],$Item[0][1]);
	//click on go straight to checkout
	_click(_link("mini-cart-link-checkout"));
	login();
	_assertVisible(_link("user-account"));
	_assertEqual("STEP 1: Shipping", _getText(_div("step-1 active")));
	_assertVisible(_fieldset("/SELECT OR ENTER A SHIPPING ADDRESS /"));	
	ClearCartItems();
	//logout from application
	_click(_link("Account"));
	_click(_link("Logout"));
}	
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


if($CheckoutType==$BMConfig[0][1])
{
var $t = _testcase("148949","Verify the functionality of radio buttons under 'SELECT SHIPPING METHOD' Section on shipping page in Prevail application as a guest user");
$t.start();
try
{
    //add items to cart
	navigateToCart($Shipping_Page[0][1],1);
	_click(_submit("dwfrm_cart_checkoutCart"));	
	_click(_submit("dwfrm_login_unregistered")); 
	//verify the navigation
	_assertVisible(_heading2("Shipping"));
	_assertVisible(_fieldset("/SELECT OR ENTER A SHIPPING ADDRESS /"));
	//verify the functionality of radio buttons
	var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_fieldset($Shipping_Page[3][0])));
	var $count=0;
	for(var $i=0;$i<$totalShippingMethods;$i++)
		{
			//selecting radio buttons
			_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
			//verify the selection
			_assert(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked);
			//uncheck the checkbox
			_uncheck(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
			//verify the deselection
			_assert(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked);
			for(var $j=0;$j<$totalShippingMethods;$j++)
				{
					if(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked)
						{
							$count++;
						}
				}
		}		
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t=_testcase("148952","Verify the functionality of 'CONTINUE >' button on shipping page in Prevail application as a Anonymous user");
$t.start();
try
{
shippingAddress($Shipping_addr,0);
_wait(3000,_isVisible(_submit("dwfrm_singleshipping_shippingAddress_save")));
//click on continue button
_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
if($AddrVerification==$BMConfig[0][2])
{
	if(_isVisible(_div("address-validation-dialog")))
		{
			_click(_submit("ship-to-original-address"));
		}
}
//verify the navigation to billing page
_assertVisible(_fieldset("/"+$Shipping_Page[0][3]+"/"));
_assertVisible(_submit("dwfrm_billing_save"));
}catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();


var $t = _testcase("148952","Verify the functionality of 'CONTINUE>' button on shipping page in Prevail application as a guest user");
$t.start();
try
{	
	//enter the shipping address
	shippingAddress($Shipping_addr,0);	
	//click on continue button
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//UPS verification
	if($AddrVerification==$BMConfig[0][2])
		{
			if(_isVisible(_div("address-validation-dialog")))
			{
				_click(_submit("ship-to-original-address"));
			}
			//directly should navigate to billing page
			_assertVisible(_fieldset("/"+$Shipping_Page[0][3]+"/"));
			_assertVisible(_submit("dwfrm_billing_save"));
		}
	else
		{
			_log("Enable the UPS in BM");
			//directly should navigate to billing page
			_assertVisible(_fieldset("/"+$Shipping_Page[0][3]+"/"));
			_assertVisible(_submit("dwfrm_billing_save"));
		}	
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

cleanup();


//-------------------------------------------------------------------------------Registered
//login to the application
_click(_link("Login"));
login();


var $t = _testcase("124486","Verify the functionality of radio buttons under 'SELECT SHIPPING METHOD' Section on shipping page in Prevail application as a Registered user");
$t.start();
try
{
	//add items to cart
	navigateToCart($Shipping_Page[0][1],1);
	_click(_submit("dwfrm_cart_checkoutCart"));	
	//verify the navigation
	_assertVisible(_heading2("Shipping"));
	//verify the functionality of radio buttons
	var $totalShippingMethods=_count("_div","form-row form-indent label-inline",_in(_fieldset($Shipping_Page[3][0])));
	var $count=0;
	for(var $i=0;$i<$totalShippingMethods;$i++)
		{
		//selecting radio buttons
		_check(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
		//verify the selection
		_assert(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked);
		//uncheck the checkbox
		_uncheck(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]"));
		//verify the deselection
		_assert(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked);
		for(var $j=0;$j<$totalShippingMethods;$j++)
			{
			if(_radio("dwfrm_singleshipping_shippingAddress_shippingMethodID["+$i+"]").checked)
				{
				$count++;
				}
			}
		}		
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();


var $t = _testcase("148869/148870","Verify the functionality of 'CONTINUE>' button/verify the contents of verify your shipping address page on shipping page in Prevail application as a Registered user");
$t.start();
try
{
	//enter the shipping address
	shippingAddress($Shipping_addr,0);	
	//click on continue button
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//UPS verification
	if($AddrVerification==$BMConfig[0][2])
	{
		//verify the UI contents of dialog box
		_assertVisible(_heading1($Shipping_Page[1][2]));
		_assertVisible(_div("address-validation-dialog"));
		_assertVisible(_paragraph("/(.*)/",_in(_div("address-validation-dialog"))));
		//original address
		_assertVisible(_heading2($Shipping_Page[2][2]));
		_assertVisible(_div("original-address left-pane"));
		_assertVisible(_submit("original-address-edit"));
		_assertVisible(_submit("ship-to-original-address"));
		//suggested address
		_assertVisible(_heading2($Shipping_Page[3][2]));
		_assertVisible(_div("suggested-addresses origin"));
		_assertVisible(_submit("Edit",_in(_div("suggested-addresses origin"))));
		_assertVisible(_submit("ship-to-address-selected-1"));
		
		//click on link edit address
		_click(_submit("Edit",_in(_div("suggested-addresses origin"))));
		//again click on continue
		_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
		if(_isVisible(_div("address-validation-dialog")))
		{
			_click(_submit("ship-to-original-address"));
		}	
	}
	else
	{
		_log("Enable the UPS in BM");
	}
	//directly should navigate to billing page
	_assertVisible(_fieldset("/"+$Shipping_Page[0][3]+"/"));
	_assertVisible(_submit("dwfrm_billing_save"));
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();

ClearCartItems();

var $t=_testcase("124434","Verify the scenario if User selected 'Use this address for Billing' checkbox on shipping page in Application as a Registered user.");
$t.start();
try
{
	//if User selected 'Use this address for Billing' checkbox
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping details
	shippingAddress($Valid_Data,0);
	//selecting use this billing address
	_check(_checkbox("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"));
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//address verification over lay
	addressVerificationOverlay();
	//Billing details
	//verify the address prepopulation in billing page
	_assertEqual($Valid_Data[0][1],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_firstName")));
	_assertEqual($Valid_Data[0][2],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_lastName")));
	_assertEqual($Valid_Data[0][3],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address1")));
	_assertEqual($Valid_Data[0][4],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_address2")));
	_assertEqual($Valid_Data[1][5],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_country")));
	_assertEqual($Valid_Data[0][6],_getSelectedText(_select("dwfrm_billing_billingAddress_addressFields_states_state")));
	_assertEqual($Valid_Data[0][7],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_city")));
	_assertEqual($Valid_Data[0][8],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_postal")));
	_assertEqual($Valid_Data[0][9],_getText(_textbox("dwfrm_billing_billingAddress_addressFields_phone")));	
	//Checking email
	_assertEqual($UserEmail,_getText(_textbox("dwfrm_billing_billingAddress_email_emailAddress")));
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

//removing item from cart
ClearCartItems()

if($paypalNormal=="Yes")
{
var $t=_testcase("128312"," Verify whether user is able to place the order using Paypal alone  on billing page in Application as a Anonymous user.");
$t.start();
try
{
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping details
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	if($AddrVerification==$BMConfig[0][2])
	{
		if(_isVisible(_div("address-validation-dialog")))
			{
				_click(_submit("ship-to-original-address"));
			}
	}
	//Billing details
	BillingAddress($Valid_Data,0);
	//Payment section through paypal
	if(($CreditCard=="Yes" && $paypalNormal=="Yes"))
	{
		_click(_radio("PayPal"));	
	}
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	{
		_assert(_radio("PayPal").checked);
	}
	_click(_submit("dwfrm_billing_save"));
	if(isMobile())
		 {
		 	_setValue(_emailbox("login_email"), $paypal[0][0]);
		 }
	 else
		 {
		 _setValue(_textbox("login_email"), $paypal[0][0]);
		 }
	 _setValue(_password("login_password"), $paypal[0][1]);
	 _click(_submit("Log In"));
	 _click(_submit("Continue"));
	 _wait(10000,_isVisible(_submit("submit")));
	//Placing order
	_click(_submit("submit"));
	if(_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
	//Checking order is placed or not
	_assertVisible(_heading1($Generic[0][2]));
	_assertVisible(_div("order-confirmation-details"));
	
	//checking Paypal
	 var $paymentType=_getText(_div("payment-type"));
	 _assertEqual($paypal[0][2],$paymentType);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();
}


ClearCartItems();
var $t = _testcase("125961","Verify the 'Continue button functionality' below the order total Section on Multi Shipping page in Application as a Registered  user");
$t.start();
try
{
	//navigate to shipping page
	navigateToCart($SPC_Multishipping[0][1],$SPC_Multishipping[0][8]); 
	//click on go strait to checkout link
	_click(_link("mini-cart-link-checkout"));
	//click on yes button 'Do you want multishipping' 
	_click(_submit("dwfrm_singleshipping_shipToMultiple"));
	var $addrCount=0;
	//adding addresses
	for(var $i=0;$i<$addr_data.length;$i++)
	{
		_click(_link("Add/Edit Address["+$i+"]"));
		addAddressMultiShip($addr_data,$i);
		$addrCount++;
	}
	//select addresses from drop down 
	var $dropDowns=_count("_select","/dwfrm_multishipping_addressSelection_quantityLineItems/",_in(_table("item-list")));
	var $c1=1;
	var $j=1;
	for(var $i=0;$i<$dropDowns;$i++)
		{
		   if($c1>$addrCount)
		    {
		      $c1=1;
		    }
		   _setSelected(_select("dwfrm_multishipping_addressSelection_quantityLineItems_i"+$i+"_addressList"),$j);
		   $j++;	
		}	
	//click on continue
	_click(_submit("dwfrm_multishipping_addressSelection_save"));
	//verify the number of shipments
	//selected different address and verify number of shipments
	var $numOfShipments=_count("_table","/item-list/");
	_assertEqual($SPC_Multishipping[2][4],$numOfShipments);
}
catch($e)
{
	_logExceptionAsFailure($e);
}	
$t.end();
}

_include("EndToEndScenarios/CreditCard/OrderWithPromotion1GCandCreditCard.js");
_include("EndToEndScenarios/Paypal/OrderWithPromotion1GCandPaypal.js");
_include("EndToEndScenarios/Paypal/Multishipment1GCandPaypal.js");
_include("EndToEndScenarios/CreditCard/MultishipPaymentWith1GCandCreditcard.js");
