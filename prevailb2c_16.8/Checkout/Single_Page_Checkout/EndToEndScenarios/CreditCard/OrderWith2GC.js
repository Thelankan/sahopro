_include("../../../../util.GenericLibrary/GlobalFunctions.js");
_include("../../../../util.GenericLibrary/BM_Configuration.js");
_resource("BillingPage.xls");

var $item = _readExcelFile("BillingPage.xls","Item");
var $Valid_Data=_readExcelFile("BillingPage.xls","ValidData");
var $Address=_readExcelFile("BillingPage.xls","Address");

if($CheckoutType==$BMConfig[0][1] && $GiftCardPayment==$BMConfig[0][3] && $giftCertificate=="Yes")
{
//creating giftcard
createGC($GC);

SiteURLs()
_setAccessorIgnoreCase(true); 
cleanup();

_click(_link("Login"));
login();

 var $t=_testcase("126816","Verify order placement with 2 Gift cards  on billing page in Application as a Registered  user");
 $t.start();
 try
 {
 _click(_link("Addresses"));
 var $c=_count("_div", "/mini-address-location/");
  for(var $i=0;$i<$c;$i++)
 {
   _click(_link("Delete"));
 } 
 _log($Address.length);
 for(var $i=0;$i<$Address.length;$i++)
 {	
	 _log($i);
 	_wait(2000,_isVisible(_link("Create New Address"))); 		
 	_click(_link("Create New Address")); 	
 	addAddress($Address,$i);
 	_click(_submit("Apply"));
 	
 }  
 _wait(3000);
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping details
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	if(_isVisible(_div("address-validation-dialog")))
		{
			_assertEqual($Valid_Data[0][3]+" "+$Valid_Data[0][4]+" "+$Valid_Data[0][7]+" , "+$Valid_Data[1][6]+" "+$Valid_Data[0][5]+" "+$Valid_Data[0][8], _getText(_paragraph("/(.*)/", _in(_div("original-address left-pane")))));
			_click(_submit("ship-to-original-address"));
		}
	
	var $OrderTotal=round(_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString(),2);
	 _setValue(_textbox("dwfrm_billing_giftCertCode"),$GC[1][0]);
	 _setValue(_textbox("dwfrm_billing_giftCertPin"), $GC[1][1]);
	 _click(_submit("dwfrm_billing_redeemGiftCert"));
	 
	 if(_assertExists(_div("success giftcert-pi")))
	 {
	 var $didamt=round(parseFloat(_extract(_getText(_div("success giftcert-pi")),"/USD (.*) has/",true)), 2);
	 _log("Diducted amount is"+$didamt);
	  var $remamt=$OrderTotal-$didamt;
	 _log($remamt);
	 
	 //for(var $i=1;$remamt>0;$i++){
	 _setValue(_textbox("dwfrm_billing_giftCertCode"),$GC[2][0]);
	 _setValue(_textbox("dwfrm_billing_giftCertPin"), $GC[2][1]);
	 _click(_submit("dwfrm_billing_redeemGiftCert"));
	 if(_assertExists(_div("success giftcert-pi[1]")))
	 {
	 var $didamt1=round(parseFloat(_extract(_getText(_div("success giftcert-pi[1]")),"/USD (.*) has/",true)),2);
	  $remamt=$remamt-$didamt1;
	  _log($remamt);
	 }
	// }
	 var $Temp=$didamt+$didamt1;
	var $total=round($Temp,2);		
	
	 _assertEqual($OrderTotal,$total);
	 }
	 else
		 {
		 _assert(false,"successful message is not displayed");
		 }
	 
	 _click(_submit("dwfrm_billing_save"));
	 
	 //checking whether navigated to Order review Page
	 if(_isVisible(_submit("submit"))){
	 _click(_submit("submit"));
	 }
	 
	//checking whether navigated to Order conformation Page
	 if(_isVisible(_image("CloseIcon.png"))){
	 _click(_image("CloseIcon.png"));
	 }
	 _assertExists(_heading1("Thank you for your order."));
	 
	 var $count=_count("_span","/Gift Certificate/");
	 _assert(_isVisible(_div("order-payment-instruments")));
	 _assertEqual(2,$count);
	 var $exp=$GC[1][0].replace("GC","");
	 _log($exp);
	 _assertContainsText($exp,_div("orderpaymentinstrumentsgc"));
	 var $exp1=$GC[2][0].replace("GC","");
	 _log($exp1);
	 _assertContainsText($exp1,_div("orderpaymentinstrumentsgc[1]"));
	 
	//checking Order total
	 if(_getText(_row("order-total")).indexOf("Order Total: $")>-1){
		 var $orderTotalAct=_getText(_row("order-total")).replace("Order Total: $","");
	 }else{
	 var $orderTotalAct=_getText(_row("order-total")).replace("Order Total:$","");
	 }
	 _assertEqual($OrderTotal,$orderTotalAct);	 
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();
	 
cleanup();
deleteGC($GC);
}