_include("../../../../util.GenericLibrary/GlobalFunctions.js");
_include("../../../../util.GenericLibrary/BM_Configuration.js");
_resource("BillingPage.xls");

var $item = _readExcelFile("BillingPage.xls","Item");
var $Valid_Data=_readExcelFile("BillingPage.xls","ValidData");
var $Generic=_readExcelFile("BillingPage.xls","Generic");

if($CheckoutType==$BMConfig[0][1])
{
if($GiftCardPayment==$BMConfig[0][3]  && $giftCertificate=="Yes")
	{
	//creating gift card
	//createGC($GC);
	//_log("created");
	}

SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();

//Login()
_click(_link("Login"));
login();
//removing item from cart
ClearCartItems();

if($CreditCard=="Yes")
{
	if($CreditCardPayment!=$BMConfig[0][4])
	{  
		var $t=_testcase("148959/148960/148961/148962/148966","Verify whether user is able to place order using VISA, MASTERCARD, AMERICAN EXPRESS, DISCOVER as credit card type  and Credit card alone on billing page in Application as a Registered user.");
		$t.start();
		try
		{
			for(var $i=4;$i<$Valid_Data.length;$i++)
				{
					_click(_link("user-account"));
					_click(_link("My Account"));
					_click(_link("Payment Settings"));
					while(_isVisible(_submit("Delete Card")))
					{
					  _click(_submit("Delete Card"));
					  _wait(1000)
					  window.location.reload();
					 // _call(location.reload());
					  _expectConfirm("/Do you want/",true);
					}	
					//Navigation till shipping page
					navigateToShippingPage($item[0][0],$item[0][1]);
					//shipping details
					shippingAddress($Valid_Data,0);
					_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
					//address verification over lay
					addressVerificationOverlay();
					//Billing details
					BillingAddress($Valid_Data,0);
					//Payment details
					PaymentDetails($Valid_Data,$i);
					_click(_submit("dwfrm_billing_save"));
					//Placing order
					_click(_submit("submit"));
					if(_isVisible(_image("CloseIcon.png")))
						{
						_click(_image("CloseIcon.png"));
						}
					//Checking order is placed or not
					_assertVisible(_heading1($Generic[0][2]));
					_assertVisible(_div("order-confirmation-details"));
				}
		}
		catch($e)
		{      
		       _logExceptionAsFailure($e);
		}
		$t.end();
	}
}

//removing item from cart
ClearCartItems();
if($GiftCardPayment==$BMConfig[0][3] && $giftCertificate=="Yes")
{
var $t=_testcase("148963/168349","Verify whether user is able to place the order using Gift Card alone on billing page in Application as a Anonymous user.");
$t.start();
try
{
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping details
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//address verification over lay
	addressVerificationOverlay();
	//applying GC
	_setValue(_textbox("dwfrm_billing_giftCertCode"),$GC[2][0]);
	_setValue(_textbox("dwfrm_billing_giftCertPin"),$GC[2][1]);
	_click(_submit("dwfrm_billing_redeemGiftCert"));
	//Billing details
	BillingAddress($Valid_Data,0);
	_click(_submit("dwfrm_billing_save"));
	//Placing order
	_click(_submit("submit"));
	if(_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
	//Checking order is placed or not
	_assertVisible(_heading1($Generic[0][2]));
	_assertVisible(_div("order-confirmation-details"));
	
	//checking GC
	 _assert(_isVisible(_div("order-payment-instruments")));
	 var $exp=$GC[2][0].replace("GC","**");
	 var $act=_extract(_getText(_div("order-payment-instruments")),"/Gift Certificate (.*) Amount/",true).toString();
	 _assertEqual($exp,$act);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();
}
}