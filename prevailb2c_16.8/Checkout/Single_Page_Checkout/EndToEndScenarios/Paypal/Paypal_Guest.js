_include("../../../../util.GenericLibrary/GlobalFunctions.js");
_include("../../../../util.GenericLibrary/BM_Configuration.js");
_resource("BillingPage.xls");

var $item = _readExcelFile("BillingPage.xls","Item");
var $Valid_Data=_readExcelFile("BillingPage.xls","ValidData");
var $Generic=_readExcelFile("BillingPage.xls","Generic");
var $paypal=_readExcelFile("BillingPage.xls","Paypal");
var $Address=_readExcelFile("BillingPage.xls","Address");
var $OrderSummuary=_readExcelFile("BillingPage.xls","OrderSummuary");


SiteURLs();
_setAccessorIgnoreCase(true); 
cleanup();


if($paypalNormal=="Yes")
{
var $t=_testcase("128312"," Verify whether user is able to place the order using Paypal alone  on billing page in Application as a Anonymous user.");
$t.start();
try
{
	//Navigation till shipping page
	navigateToShippingPage($item[0][0],$item[0][1]);
	//shipping details
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	if($AddrVerification==$BMConfig[0][2])
	{
		if(_isVisible(_div("address-validation-dialog")))
			{
				_click(_submit("ship-to-original-address"));
			}
	}
	//Billing details
	BillingAddress($Valid_Data,0);
	//Payment section through paypal
	if(($CreditCard=="Yes" && $paypalNormal=="Yes"))
	{
		_click(_radio("PayPal"));	
	}
	else if($CreditCard=="No" && $paypalNormal=="Yes")
	{
		_assert(_radio("PayPal").checked);
	}
	_click(_submit("dwfrm_billing_save"));
	if(isMobile())
		 {
		 	_setValue(_emailbox("login_email"), $paypal[0][0]);
		 }
	 else
		 {
		 _setValue(_textbox("login_email"), $paypal[0][0]);
		 }
	 _setValue(_password("login_password"), $paypal[0][1]);
	 _click(_submit("Log In"));
	 _click(_submit("Continue"));
	 _wait(10000,_isVisible(_submit("submit")));
	//Placing order
	_click(_submit("submit"));
	if(_isVisible(_image("CloseIcon.png")))
		{
		_click(_image("CloseIcon.png"));
		}
	//Checking order is placed or not
	_assertVisible(_heading1($Generic[0][2]));
	_assertVisible(_div("order-confirmation-details"));
	
	//checking Paypal
	 var $paymentType=_getText(_div("payment-type"));
	 _assertEqual($paypal[0][2],$paymentType);
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();
}


ClearCartItems();
var $t=_testcase("148927","Verify the display of Payment information Section when user have used Paypal as their payment method on Summary Page in Application as a Anonymous user.");
$t.start();
try
{
	navigateToShippingPage($OrderSummuary[0][7],$OrderSummuary[0][8]);
	//shipping address
	shippingAddress($Valid_Data,0);
	_click(_submit("dwfrm_singleshipping_shippingAddress_save"));
	//UPS verification
	if($AddrVerification==$BMConfig[0][2])
	{
		_click(_submit("ship-to-original-address"));
	}
	//Billing address
	BillingAddress($Valid_Data,0); 
	$Ordertotal=_extract(_getText(_row("order-total")),"/[$](.*)/",true).toString();
	//Payment section through paypal
	_click(_radio("PayPal"));
	_click(_submit("dwfrm_billing_save"));
	if(isMobile())
		 {
		 	_setValue(_emailbox("login_email"), $paypal[0][0]);
		 	
		 }
	 else
		 {
		 _setValue(_textbox("login_email"), $paypal[0][0]);
		 }
	 _setValue(_password("login_password"), $paypal[0][1]);
	 _click(_submit("Log In"));
	 if(!isMobile())
		 {
	 _assertEqual($paypal[0][0], _getText(_span("confidential")));
		 }
	 else
		 {
		 _click(_span("/Funding/"));
		 _assertVisible(_heading4("Change funding to:"));
		 _assertVisible(_span("/Bank/"));
		 _assertVisible(_span("/Card/"));
		 _click(_submit("Cancel"));
		 }
	 _click(_submit("Continue"));
	 _assertEqual($OrderSummuary[4][1]+" $"+$Ordertotal, _getText(_div("details", _in(_div("/mini-payment-instrument/")))));
	 
}
catch($e)
{      
       _logExceptionAsFailure($e);
}
$t.end();

